-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.23 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных vpalette
CREATE DATABASE IF NOT EXISTS `vpalette` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `vpalette`;

-- Дамп структуры для таблица vpalette.attributes
CREATE TABLE IF NOT EXISTS `attributes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frontend_type` enum('select','checkbox','radio','text','textarea','range','color','file','number','email','date') COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_filterable` tinyint(1) NOT NULL DEFAULT '0',
  `is_required` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.attributes: ~2 rows (приблизительно)
DELETE FROM `attributes`;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
INSERT INTO `attributes` (`id`, `code`, `name`, `frontend_type`, `is_filterable`, `is_required`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'size', 'Size', 'select', 1, 1, '2019-10-04 10:44:41', '2019-10-04 10:44:41', NULL),
	(2, 'color', 'Color', 'select', 1, 1, '2019-10-04 10:44:41', '2019-10-04 10:44:41', NULL);
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.attribute_values
CREATE TABLE IF NOT EXISTS `attribute_values` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` bigint(20) unsigned NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attribute_values_attribute_id_foreign` (`attribute_id`),
  CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.attribute_values: ~7 rows (приблизительно)
DELETE FROM `attribute_values`;
/*!40000 ALTER TABLE `attribute_values` DISABLE KEYS */;
INSERT INTO `attribute_values` (`id`, `attribute_id`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'small', '2019-10-04 10:44:53', '2019-10-04 10:44:53', NULL),
	(2, 1, 'medium', '2019-10-04 10:44:53', '2019-10-04 10:44:53', NULL),
	(3, 1, 'large', '2019-10-04 10:44:53', '2019-10-04 10:44:53', NULL),
	(4, 2, 'black', '2019-10-04 10:44:53', '2019-10-04 10:44:53', NULL),
	(5, 2, 'blue', '2019-10-04 10:44:53', '2019-10-04 10:44:53', NULL),
	(6, 2, 'red', '2019-10-04 10:44:53', '2019-10-04 10:44:53', NULL),
	(7, 2, 'orange', '2019-10-04 10:44:53', '2019-10-04 10:44:53', NULL);
/*!40000 ALTER TABLE `attribute_values` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preview_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.brands: ~0 rows (приблизительно)
DELETE FROM `brands`;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image_src` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_icon_src` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail_image_src` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) unsigned DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.categories: ~11 rows (приблизительно)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `active`, `name`, `slug`, `description`, `image_src`, `menu_icon_src`, `detail_image_src`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'Вверхний уровень', 'vverkhniy-uroven', 'Это корневая категория, не удаляйте её', '', '', '', NULL, '2019-10-04 10:45:24', '2019-10-04 10:45:24', NULL),
	(2, 1, 'Артемьев Гавриил Львович', 'artemev-gavriil-lvovich', 'Чичиков поднял несколько бровь, услышав такое отчасти греческое имя, которому, неизвестно почему.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(3, 1, 'Полина Борисовна Мишина', 'polina-borisovna-mishina', 'Может быть, вы имеете какие-нибудь сомнения? — О! это была хозяйка. Он надел рубаху; платье, уже.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(4, 1, 'Илья Евгеньевич Смирнов', 'ilya-evgenevich-smirnov', 'Эк уморила как проклятая старуха» — «сказал он, немного отдохнувши, и отпер шкатулку. Автор.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(5, 1, 'Капустин Никодим Борисович', 'kapustin-nikodim-borisovich', 'И что всего страннее, что может только на твоей стороне счастие, ты можешь заплатить мне после.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(6, 1, 'Ярослава Львовна Уварова', 'yaroslava-lvovna-uvarova', 'Ведь у — тебя, чай, место есть на козлах, где бы ни случилось с ним; но судьбам угодно было спасти.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(7, 1, 'Данила Максимович Суханов', 'danila-maksimovich-sukhanov', 'Суворов, он лезет на — которую он совершенно обиделся. — Ей-богу, повесил бы, — повторил Ноздрев.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(8, 1, 'Прохор Алексеевич Копылов', 'prokhor-alekseevich-kopylov', 'Но все это en gros[[1 - В большом — количестве (франц.)]]. В фортунку крутнул: выиграл две банки.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(9, 1, 'Журавлёва Искра Сергеевна', 'zhuravleva-iskra-sergeevna', 'Увидев гостя, он сказал какой-то комплимент, весьма приличный для человека средних лет, имеющего.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(10, 1, 'Рената Борисовна Суворова', 'renata-borisovna-suvorova', 'Трактир был что-то вроде русской избы, несколько в сторону председателя и почтмейстера. Несколько.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(11, 1, 'Зиновьев Витольд Максимович', 'zinovev-vitold-maksimovich', 'Поцелуй меня, — душа, смерть люблю тебя! Мижуев, смотри, вот судьба свела: ну что бы тебе стоило.', '', '', '', 1, '2019-10-04 10:45:25', '2019-10-04 10:45:25', NULL),
	(12, 1, 'Нитки', 'nitki', '<p>Описание для категории нитки редактированные</p>', 'categories/wHRwhkZ5Kxu7XNdeZTmeHTWSK.png', 'categories/XeJ92btwxvwrZvZru8aWHod2H.png', 'categories/9H4cjc3iBYkvuEombBuQse2U4.png', 1, '2019-10-04 10:47:02', '2019-10-04 11:01:40', NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.deliveries
CREATE TABLE IF NOT EXISTS `deliveries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_period_max` int(10) unsigned NOT NULL,
  `delivery_period_min` int(10) unsigned NOT NULL,
  `delivery_total_price` double(8,2) NOT NULL,
  `delivery_cdek_print_receipts` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_pochta_russian_f7_form` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_pochta_russian_f112_form` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_pochta_russian_print_receipts` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_cdek_print_labels` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.deliveries: ~0 rows (приблизительно)
DELETE FROM `deliveries`;
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.evaluations
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Model name',
  `parent_id` int(11) NOT NULL COMMENT 'Model ID',
  `type_id` int(11) NOT NULL COMMENT 'Evaluation type ID',
  `user_id` int(11) NOT NULL COMMENT 'User ID',
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'IP address',
  `user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'User-Agent',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.evaluations: ~0 rows (приблизительно)
DELETE FROM `evaluations`;
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.faqs
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` longtext COLLATE utf8mb4_unicode_ci,
  `answer` longtext COLLATE utf8mb4_unicode_ci,
  `product_id` bigint(20) unsigned DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faqs_product_id_foreign` (`product_id`),
  KEY `faqs_user_id_foreign` (`user_id`),
  CONSTRAINT `faqs_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `faqs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.faqs: ~0 rows (приблизительно)
DELETE FROM `faqs`;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.followables
CREATE TABLE IF NOT EXISTS `followables` (
  `user_id` bigint(20) unsigned NOT NULL,
  `followable_id` int(10) unsigned NOT NULL,
  `followable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'follow' COMMENT 'follow/like/subscribe/favorite/upvote/downvote',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `followables_user_id_foreign` (`user_id`),
  KEY `followables_followable_type_index` (`followable_type`),
  CONSTRAINT `followables_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.followables: ~0 rows (приблизительно)
DELETE FROM `followables`;
/*!40000 ALTER TABLE `followables` DISABLE KEYS */;
/*!40000 ALTER TABLE `followables` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.linked_social_accounts
CREATE TABLE IF NOT EXISTS `linked_social_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `linked_social_accounts_user_id_foreign` (`user_id`),
  CONSTRAINT `linked_social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.linked_social_accounts: ~0 rows (приблизительно)
DELETE FROM `linked_social_accounts`;
/*!40000 ALTER TABLE `linked_social_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `linked_social_accounts` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.migrations: ~48 rows (приблизительно)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(8, '2016_08_03_081210_create_evaluations_table', 1),
	(9, '2018_06_29_032244_create_laravel_follow_tables', 1),
	(10, '2019_04_13_122456_create_notifications_table', 1),
	(11, '2019_04_13_124848_create_permission_tables', 1),
	(12, '2019_04_13_165922_add_avatar_field_to_users_table', 1),
	(13, '2019_05_30_105141_create_wishlists_table', 1),
	(14, '2019_06_24_094325_create_partners_table', 1),
	(15, '2019_06_24_102553_create_categories_table', 1),
	(16, '2019_06_24_102553_create_products_table', 1),
	(17, '2019_06_24_110554_create_product_category_table', 1),
	(18, '2019_09_23_171555_create_attributes_table', 2),
	(19, '2019_09_23_222856_create_attribute_values_table', 2),
	(20, '2019_08_28_102757_create_views_table', 3),
	(21, '2019_09_19_102849_add_column_menu_icon_src', 4),
	(22, '2019_09_19_102904_add_column_detail_image_src', 4),
	(23, '2019_09_24_103805_create_settings_table', 5),
	(24, '2019_08_20_152654_create_faqs_table', 6),
	(25, '2019_08_09_095415_create_news_table', 7),
	(26, '2019_08_09_095824_create_news_category_table', 7),
	(27, '2019_09_02_125539_create_partner_shops_table', 8),
	(28, '2019_06_25_072721_create_partner_offers_table', 9),
	(29, '2019_08_18_085720_add_popular_column_in_products_table', 10),
	(30, '2019_08_18_093047_add_active_column_in_products_table', 10),
	(31, '2019_08_18_093055_add_sort_column_in_products_table', 10),
	(32, '2019_08_18_174611_add_gallery_images_column_in_products_table', 10),
	(33, '2019_08_19_212204_create_reviews_table', 11),
	(34, '2019_08_21_101326_add_column_username_in_reviews_table', 11),
	(35, '2019_08_02_092253_create_seos_table', 12),
	(36, '2019_07_24_083837_create_sliders_table', 13),
	(37, '2019_08_05_061204_create_stocks_table', 14),
	(38, '2019_08_27_122138_add_count_view_in_stocks_table', 14),
	(39, '2019_08_29_150734_add_date_column_in_stocks_table', 14),
	(40, '2019_08_15_111440_create_linked_social_accounts_table', 15),
	(41, '2019_08_15_112010_make_password_and_email_fields_nullable_in_users_table', 15),
	(42, '2019_08_20_202112_add_phone_column_in_users_table', 15),
	(43, '2019_08_21_145157_create_ratings_table', 15),
	(44, '2019_09_03_143735_add_first_name_column_in_table_user', 15),
	(45, '2019_09_03_143956_add_last_name_column_in_table_user', 15),
	(46, '2019_09_06_114504_make_field_name_nullable_in_users_table', 15),
	(47, '2019_09_18_170035_create_deliveries_table', 15),
	(48, '2019_09_19_122134_create_shoppingcart_table', 15),
	(51, '2019_10_04_143849_create_brands_table', 16);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.model_has_permissions: ~0 rows (приблизительно)
DELETE FROM `model_has_permissions`;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.model_has_roles: ~3 rows (приблизительно)
DELETE FROM `model_has_roles`;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\User', 1),
	(2, 'App\\User', 2),
	(3, 'App\\User', 3);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` smallint(5) unsigned DEFAULT NULL,
  `activity_end_at` datetime DEFAULT NULL,
  `count_view` int(10) unsigned NOT NULL DEFAULT '0',
  `preview_text` text COLLATE utf8mb4_unicode_ci,
  `preview_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail_text` longtext COLLATE utf8mb4_unicode_ci,
  `detail_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_user_id_foreign` (`user_id`),
  CONSTRAINT `news_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.news: ~5 rows (приблизительно)
DELETE FROM `news`;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `name`, `slug`, `sort`, `activity_end_at`, `count_view`, `preview_text`, `preview_image`, `detail_text`, `detail_image`, `user_id`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(4, 'ОЧЕНЬ ИНТЕРЕСНАЯ НОВОСТЬ', 'ochen-interesnaya-novost', 100, NULL, 0, '<p>qweqweqweЙЦУЙЦУЙЦУ</p>', 'https://vpalette-bucket.s3.amazonaws.com/news/nJI99UKLJhtn4DtyKJgPnHqnm2zrCEyiuBC0s1BH.jpeg', '<p>qeqweqeЙЦУЙЦУЙЙУЙУЙ</p>', 'https://vpalette-bucket.s3.amazonaws.com/news/XiqBJMgF8RhT8uWLgunF5OxCkhqUKrhl6rFUyAbP.jpeg', NULL, 1, '2019-10-07 13:54:13', '2019-10-07 19:25:54', '2019-10-07 19:25:54'),
	(5, 'qweqweqweqqeqweqweqw', 'qweqweqweqqeqweqweqw', 100, NULL, 0, '<p>qweqeq</p>', 'https://vpalette-bucket.s3.amazonaws.com/news/ySq0WxTeGvdLEAA2Y9bl1rOADBp2WV356BKfMRll.jpeg', '<p>qeeqweq</p>', 'https://vpalette-bucket.s3.amazonaws.com/news/G4k9wE6mIhria5igoVkJsUjrnXn1Ug2YVLDK7G7e.jpeg', NULL, 1, '2019-10-07 16:26:58', '2019-10-07 18:08:22', '2019-10-07 18:08:22'),
	(6, 'Новая новость', 'novaya-novost', 100, '2019-10-07 12:00:00', 0, '<p>Новая новость описание</p>', 'news/BcVFsfYYJDcpSh8lGoUoFQkwbjv3kFITHVmygtQX.jpeg', '<p>Детальное описание новой новости.</p>', 'news/jEptcKbcLUNB54Kh7wSYC8cZxKxAPZHEW1QNxrN4.jpeg', NULL, 1, '2019-10-07 19:36:24', '2019-10-07 19:59:29', NULL),
	(7, 'Новая новость 2', 'novaya-novost-2', 200, NULL, 0, '<p>Тест&nbsp;</p>', 'news/dkLqS6YcQs3G9RQ2Pneq8OEPVxwlrIT4HZRn1Mdq.jpeg', '<p>тест</p>', 'news/JlfSzIRa1458Q9UHKpj4UbVvltgoQ9b3F65y4yDB.jpeg', NULL, 1, '2019-10-07 19:59:00', '2019-10-07 19:59:01', NULL),
	(8, 'Серия qweqwe', 'seriya-qweqwe', 200, NULL, 0, '<p>qwewqeqeq</p>', 'news/Qp32glgJGfkYAE3jI86IOmoQK4X36uvNLMfS3MRx.jpeg', '<p>wqeqeqweq</p>', 'news/DiC5siHetObXht1g9dVjjg9Wr2wfif6bkq9P2x8a.jpeg', 1, 1, '2019-10-09 10:16:38', '2019-10-09 10:16:39', NULL),
	(9, 'Василий Пивоваров', 'vasiliy-pivovarov', 200, NULL, 0, '<p>qweqwe</p>', 'news/YLSWMyqTFVvA0WNdbalxgxCRZUrEJoSHZi8xi8HO.jpeg', '<p>qweqweqew</p>', 'news/8FxzLxonAOhAwmCLjMKsUHvc94RtXzzmuAdEpkra.jpeg', NULL, 1, '2019-10-09 10:18:51', '2019-10-09 10:18:51', NULL),
	(10, 'Ниткиqweqw', 'nitkiqweqw', 100, '2019-11-01 12:00:00', 0, '<p>ewqq</p>', 'news/gWW4W6elPpke3iQyjtEhxQOKsGbsPGqhXHcEHU5Y.jpeg', '<p>qweqe</p>', 'news/PypdiTKLjUdwGblnvzY2nfFoRAWLMrk1d3QsVzCO.jpeg', NULL, 1, '2019-10-09 10:21:38', '2019-10-09 10:21:38', NULL),
	(11, 'qweqwewqewqewqeqweqwqwewqeqw', 'qweqwewqewqewqeqweqwqwewqeqw', 100, NULL, 0, '<p>wqeqe</p>', 'news/FlQyYx3tcdZmkgQtjaFDVxIy7vIjbIqGpwh82US0.jpeg', '<p>ewqeqweqw</p>', 'news/ZX2chGXPbe6U1tsbhXQh2kLSWjMC6kWHBLXDYWWh.jpeg', NULL, 1, '2019-10-09 10:28:21', '2019-10-09 10:28:22', NULL),
	(12, 'ПИЗДАТАЯ НОВОСТЬ', 'pizdataya-novost', 100, NULL, 0, '<p>wqeqe</p>', 'news/bil1YS5d2O8JjFYknTo9s4LYFR3I00tF5uX5sSPV.jpeg', '<p>ewqeqweqw</p>', 'news/mjyyJisxCdR4AApltETc05dxalV8ujfFUSukRrGj.jpeg', NULL, 1, '2019-10-09 10:29:07', '2019-10-09 10:30:23', NULL);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.news_category
CREATE TABLE IF NOT EXISTS `news_category` (
  `news_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `news_category_news_id_foreign` (`news_id`),
  KEY `news_category_category_id_foreign` (`category_id`),
  CONSTRAINT `news_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `news_category_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.news_category: ~3 rows (приблизительно)
DELETE FROM `news_category`;
/*!40000 ALTER TABLE `news_category` DISABLE KEYS */;
INSERT INTO `news_category` (`news_id`, `category_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
	(4, 12, NULL, '2019-10-07 17:54:41', '2019-10-07 17:54:41'),
	(7, 12, NULL, '2019-10-07 19:59:01', '2019-10-07 19:59:01'),
	(6, 12, NULL, '2019-10-07 19:59:30', '2019-10-07 19:59:30'),
	(8, 4, NULL, '2019-10-09 10:16:39', '2019-10-09 10:16:39'),
	(9, 1, NULL, '2019-10-09 10:18:51', '2019-10-09 10:18:51'),
	(10, 12, NULL, '2019-10-09 10:21:38', '2019-10-09 10:21:38'),
	(11, 1, NULL, '2019-10-09 10:28:22', '2019-10-09 10:28:22'),
	(12, 1, NULL, '2019-10-09 10:29:07', '2019-10-09 10:29:07');
/*!40000 ALTER TABLE `news_category` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.notifications: ~0 rows (приблизительно)
DELETE FROM `notifications`;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.oauth_access_tokens: ~0 rows (приблизительно)
DELETE FROM `oauth_access_tokens`;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.oauth_auth_codes: ~0 rows (приблизительно)
DELETE FROM `oauth_auth_codes`;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.oauth_clients: ~0 rows (приблизительно)
DELETE FROM `oauth_clients`;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.oauth_personal_access_clients: ~0 rows (приблизительно)
DELETE FROM `oauth_personal_access_clients`;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.oauth_refresh_tokens: ~0 rows (приблизительно)
DELETE FROM `oauth_refresh_tokens`;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.partners
CREATE TABLE IF NOT EXISTS `partners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partners_user_id_foreign` (`user_id`),
  CONSTRAINT `partners_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.partners: ~0 rows (приблизительно)
DELETE FROM `partners`;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.partner_offers
CREATE TABLE IF NOT EXISTS `partner_offers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `article` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `price` int(10) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `partner_shop_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partner_offers_partner_shop_id_foreign` (`partner_shop_id`),
  KEY `partner_offers_product_id_foreign` (`product_id`),
  CONSTRAINT `partner_offers_partner_shop_id_foreign` FOREIGN KEY (`partner_shop_id`) REFERENCES `partner_shops` (`id`),
  CONSTRAINT `partner_offers_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.partner_offers: ~0 rows (приблизительно)
DELETE FROM `partner_offers`;
/*!40000 ALTER TABLE `partner_offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `partner_offers` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.partner_shops
CREATE TABLE IF NOT EXISTS `partner_shops` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `law_shop_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inn` bigint(20) unsigned NOT NULL,
  `kpp` bigint(20) unsigned NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_index` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `partner_shops_inn_unique` (`inn`),
  UNIQUE KEY `partner_shops_kpp_unique` (`kpp`),
  KEY `partner_shops_partner_id_foreign` (`partner_id`),
  CONSTRAINT `partner_shops_partner_id_foreign` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.partner_shops: ~0 rows (приблизительно)
DELETE FROM `partner_shops`;
/*!40000 ALTER TABLE `partner_shops` DISABLE KEYS */;
/*!40000 ALTER TABLE `partner_shops` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.password_resets: ~0 rows (приблизительно)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.permissions: ~5 rows (приблизительно)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'manage-users', 'web', '2019-10-04 10:43:59', '2019-10-04 10:43:59'),
	(2, 'view-users', 'web', '2019-10-04 10:43:59', '2019-10-04 10:43:59'),
	(3, 'create-users', 'web', '2019-10-04 10:43:59', '2019-10-04 10:43:59'),
	(4, 'edit-users', 'web', '2019-10-04 10:43:59', '2019-10-04 10:43:59'),
	(5, 'delete-users', 'web', '2019-10-04 10:43:59', '2019-10-04 10:43:59');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `article` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_src` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_price` decimal(10,2) unsigned DEFAULT NULL,
  `popular` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sort` smallint(5) unsigned DEFAULT NULL,
  `gallery_images` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_article_unique` (`article`),
  UNIQUE KEY `products_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.products: ~0 rows (приблизительно)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `product_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `product_category_product_id_foreign` (`product_id`),
  KEY `product_category_category_id_foreign` (`category_id`),
  CONSTRAINT `product_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_category_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.product_category: ~0 rows (приблизительно)
DELETE FROM `product_category`;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.ratings
CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rating` int(11) NOT NULL,
  `ratingable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ratingable_id` bigint(20) unsigned NOT NULL,
  `author_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ratings_ratingable_type_ratingable_id_index` (`ratingable_type`,`ratingable_id`),
  KEY `ratings_author_type_author_id_index` (`author_type`,`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.ratings: ~0 rows (приблизительно)
DELETE FROM `ratings`;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `virtues` mediumtext COLLATE utf8mb4_unicode_ci,
  `limitations` longtext COLLATE utf8mb4_unicode_ci,
  `comments` mediumtext COLLATE utf8mb4_unicode_ci,
  `make_anonymous` tinyint(1) NOT NULL DEFAULT '0',
  `photos` json DEFAULT NULL,
  `rating` int(10) unsigned NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reviews_user_id_foreign` (`user_id`),
  KEY `reviews_product_id_foreign` (`product_id`),
  CONSTRAINT `reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.reviews: ~0 rows (приблизительно)
DELETE FROM `reviews`;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.roles: ~2 rows (приблизительно)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'web', '2019-10-04 10:43:59', '2019-10-04 10:43:59'),
	(2, 'Editor', 'web', '2019-10-04 10:43:59', '2019-10-04 10:43:59'),
	(3, 'User', 'web', '2019-10-04 10:44:00', '2019-10-04 10:44:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.role_has_permissions: ~7 rows (приблизительно)
DELETE FROM `role_has_permissions`;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(1, 2),
	(2, 2);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.seos
CREATE TABLE IF NOT EXISTS `seos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seoble_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seoble_id` bigint(20) unsigned DEFAULT NULL,
  `canonical_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_tag_manager` text COLLATE utf8mb4_unicode_ci,
  `ya_direct` text COLLATE utf8mb4_unicode_ci,
  `google_analytics` text COLLATE utf8mb4_unicode_ci,
  `ya_metrica` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.seos: ~7 rows (приблизительно)
DELETE FROM `seos`;
/*!40000 ALTER TABLE `seos` DISABLE KEYS */;
INSERT INTO `seos` (`id`, `title`, `description`, `keywords`, `seoble_type`, `seoble_id`, `canonical_url`, `google_tag_manager`, `ya_direct`, `google_analytics`, `ya_metrica`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Новая новость - ВПалитре', NULL, NULL, 'Modules\\News\\Models\\News', 1, NULL, NULL, NULL, NULL, NULL, '2019-10-07 13:46:26', '2019-10-07 13:46:26', NULL),
	(2, 'Новая новость - ВПалитре', NULL, NULL, 'Modules\\News\\Models\\News', 2, NULL, NULL, NULL, NULL, NULL, '2019-10-07 13:48:35', '2019-10-07 13:48:35', NULL),
	(3, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 3, NULL, NULL, NULL, NULL, NULL, '2019-10-07 13:50:34', '2019-10-07 13:50:34', NULL),
	(4, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 4, NULL, NULL, NULL, NULL, NULL, '2019-10-07 13:54:15', '2019-10-07 19:25:54', '2019-10-07 19:25:54'),
	(5, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 5, NULL, NULL, NULL, NULL, NULL, '2019-10-07 16:27:01', '2019-10-07 18:08:22', '2019-10-07 18:08:22'),
	(6, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 6, NULL, NULL, NULL, NULL, NULL, '2019-10-07 19:36:24', '2019-10-07 19:59:30', NULL),
	(7, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 7, NULL, NULL, NULL, NULL, NULL, '2019-10-07 19:59:01', '2019-10-07 19:59:01', NULL),
	(8, NULL, NULL, NULL, 'Modules\\Stock\\Models\\Stock', 1, NULL, NULL, NULL, NULL, NULL, '2019-10-08 17:03:24', '2019-10-08 17:03:24', NULL),
	(9, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 8, NULL, NULL, NULL, NULL, NULL, '2019-10-09 10:16:39', '2019-10-09 10:16:39', NULL),
	(10, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 9, NULL, NULL, NULL, NULL, NULL, '2019-10-09 10:18:51', '2019-10-09 10:18:51', NULL),
	(11, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 10, NULL, NULL, NULL, NULL, NULL, '2019-10-09 10:21:38', '2019-10-09 10:21:38', NULL),
	(12, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 11, NULL, NULL, NULL, NULL, NULL, '2019-10-09 10:28:22', '2019-10-09 10:28:22', NULL),
	(13, NULL, NULL, NULL, 'Modules\\News\\Models\\News', 12, NULL, NULL, NULL, NULL, NULL, '2019-10-09 10:29:07', '2019-10-09 10:30:47', NULL),
	(14, NULL, NULL, NULL, 'Modules\\Stock\\Models\\Stock', 2, NULL, NULL, NULL, NULL, NULL, '2019-10-09 10:32:05', '2019-10-09 10:32:05', NULL);
/*!40000 ALTER TABLE `seos` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.settings: ~21 rows (приблизительно)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `key`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'default_email_address', 'vpalitre@gmail.com', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(2, 'currency_code', 'RUB', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(3, 'currency_symbol', '₽', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(4, 'site_name', 'ВПалитре', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(5, 'site_title', 'ВПалитре - Интернет-портал', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(6, 'site_logo', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(7, 'site_favicon', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(8, 'site_phone', '8 800 900-00-00', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(9, 'site_address', '115172, город Астрахань, улица Свердлова, дом 4', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(10, 'site_work_time', 'пн-вс: с 10:00 до 24:00', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(11, 'site_requisites', 'Индивидуальный предприниматель Иванов Иван Иванович', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(12, 'site_requisites_ogrnip', '317774600300812', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(13, 'site_requisites_inn', '772706085275', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(14, 'footer_copyright_text', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(15, 'social_facebook', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(16, 'social_twitter', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(17, 'social_instagram', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(18, 'social_vkontakte', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(19, 'google_analytics', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(20, 'yandex_metrica', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57'),
	(21, 'facebook_pixels', '', '2019-10-04 10:45:57', '2019-10-04 10:45:57');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.shoppingcart
CREATE TABLE IF NOT EXISTS `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`,`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.shoppingcart: ~0 rows (приблизительно)
DELETE FROM `shoppingcart`;
/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.sliders
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Смотреть',
  `button_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '100',
  `banner_src` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.sliders: ~0 rows (приблизительно)
DELETE FROM `sliders`;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.stocks
CREATE TABLE IF NOT EXISTS `stocks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` smallint(5) unsigned NOT NULL DEFAULT '500',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `preview_text` text COLLATE utf8mb4_unicode_ci,
  `preview_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail_text` text COLLATE utf8mb4_unicode_ci,
  `detail_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `count_view` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.stocks: ~0 rows (приблизительно)
DELETE FROM `stocks`;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
INSERT INTO `stocks` (`id`, `name`, `sort`, `slug`, `active`, `preview_text`, `preview_image`, `detail_text`, `detail_image`, `date_start`, `date_end`, `date`, `created_at`, `updated_at`, `deleted_at`, `count_view`) VALUES
	(1, 'Новая акция', 100, 'novaya-aktsiya', 1, 'превью екст акции', 'stocks/JjeO6zq7WIymtiXtBYtzlhwR3OQb0Bbkx83GWQwJ.jpeg', 'пкопуокзп зщпукзщ укзщ укзщеокущзоу', 'stocks/BVAl9oR0nLfZEyh11BjjKc9hDrvpk5VxPjwsO3qw.jpeg', '2019-10-08 05:01:00', '2019-11-25 12:00:00', NULL, '2019-10-08 17:03:24', '2019-10-08 17:03:24', NULL, 0),
	(2, 'КРУТАЯ АКЦИЯ', 100, 'krutaya-aktsiya', 1, 'ЙЦУЙЦУЦЙУЦЙУЦЙУЙ', 'stocks/VnPRjw1nWrDux8HzwiG51wA1d7Jntz7NdwSet8fo.jpeg', 'УЦЙУЙЦУЙЦ', 'stocks/bcpBrUOoRg8OtTJHu4xtBoTW2cQThwx22vorSoZz.jpeg', '2019-10-26 10:31:00', '2019-10-30 12:00:00', NULL, '2019-10-09 10:32:05', '2019-10-09 10:32:05', NULL, 0);
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.users: ~3 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `avatar`, `phone`, `first_name`, `last_name`) VALUES
	(1, 'Admin', 'admin@example.com', NULL, '$2y$10$pZUQu.zf5DUmoRbaN30wr.g.BH3u3ko0tDwTkgKQj4I9D8BJjEkMa', 'fC7zz9qJ5FlUVTmu6zgWIosqiipHpHmXxRd9GYHHJu5dCVawDIqSNw4mwYtF', '2019-10-04 10:43:59', '2019-10-04 10:43:59', NULL, '+79990001234', 'Pivovarov', 'Vasiliy'),
	(2, 'Editor', 'editor@example.com', NULL, '$2y$10$sHy1i7EszTnrIfMWu9tJDOyqK1PiZqi0GGXp3UsIkVnj08P9x/GFu', NULL, '2019-10-04 10:43:59', '2019-10-04 10:43:59', NULL, '+79990001234', 'Ivanov', 'Ivan'),
	(3, 'User', 'user@example.com', NULL, '$2y$10$ZpVZ88TFyCKuZizJU/t/nuM0LpdzOqMsdYN./s.z09aSjH8ZL5agK', NULL, '2019-10-04 10:44:00', '2019-10-04 10:44:00', NULL, '+79990001234', 'Smirnov', 'Andrey');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.views
CREATE TABLE IF NOT EXISTS `views` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(10) unsigned NOT NULL DEFAULT '0',
  `viewable_id` bigint(20) unsigned NOT NULL,
  `viewable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `views_user_id_unique` (`user_id`),
  UNIQUE KEY `views_ip_unique` (`ip`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.views: ~7 rows (приблизительно)
DELETE FROM `views`;
/*!40000 ALTER TABLE `views` DISABLE KEYS */;
INSERT INTO `views` (`id`, `user_id`, `ip`, `views`, `viewable_id`, `viewable_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, NULL, NULL, 0, 1, 'Modules\\News\\Models\\News', '2019-10-07 13:46:26', '2019-10-07 13:46:26', NULL),
	(2, NULL, NULL, 0, 2, 'Modules\\News\\Models\\News', '2019-10-07 13:48:35', '2019-10-07 13:48:35', NULL),
	(3, NULL, NULL, 0, 3, 'Modules\\News\\Models\\News', '2019-10-07 13:50:34', '2019-10-07 13:50:34', NULL),
	(4, NULL, NULL, 0, 4, 'Modules\\News\\Models\\News', '2019-10-07 13:54:15', '2019-10-07 13:54:15', NULL),
	(5, NULL, NULL, 0, 5, 'Modules\\News\\Models\\News', '2019-10-07 16:27:01', '2019-10-07 16:27:01', NULL),
	(6, NULL, NULL, 0, 6, 'Modules\\News\\Models\\News', '2019-10-07 19:36:24', '2019-10-07 19:36:24', NULL),
	(7, NULL, NULL, 0, 7, 'Modules\\News\\Models\\News', '2019-10-07 19:59:01', '2019-10-07 19:59:01', NULL),
	(8, NULL, NULL, 0, 8, 'Modules\\News\\Models\\News', '2019-10-09 10:16:39', '2019-10-09 10:16:39', NULL),
	(9, NULL, NULL, 0, 9, 'Modules\\News\\Models\\News', '2019-10-09 10:18:51', '2019-10-09 10:18:51', NULL),
	(10, NULL, NULL, 0, 10, 'Modules\\News\\Models\\News', '2019-10-09 10:21:38', '2019-10-09 10:21:38', NULL),
	(11, NULL, NULL, 0, 11, 'Modules\\News\\Models\\News', '2019-10-09 10:28:22', '2019-10-09 10:28:22', NULL),
	(12, NULL, NULL, 0, 12, 'Modules\\News\\Models\\News', '2019-10-09 10:29:07', '2019-10-09 10:29:07', NULL);
/*!40000 ALTER TABLE `views` ENABLE KEYS */;

-- Дамп структуры для таблица vpalette.wishlists
CREATE TABLE IF NOT EXISTS `wishlists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `admin_product_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы vpalette.wishlists: ~0 rows (приблизительно)
DELETE FROM `wishlists`;
/*!40000 ALTER TABLE `wishlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `wishlists` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
