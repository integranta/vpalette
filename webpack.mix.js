const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.webpackConfig({
    resolve: {
        extensions: ['*', '.js', '.jsx', '.vue'],
        alias: {
            '@': __dirname + '/resources'
        }
    }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.mergeManifest();

mix.autoload({
    'jquery': ['jQuery', '$'],
});

mix.options({
    outputStyle: 'compressed',
    extractVueStyles: false,
    processCssUrls: true,
    purifyCss: false,
    terser: {
        cache: true,
        parallel: true,
        extractComments: false,
        terserOptions: {
            ecma: 2015,
            toplevel: true,
            ie8: false,
            safari10: true,
            output: {
                comments: false
            }
        },

    }
})

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css', {
        implementation: require('node-sass')
    })
    .js('resources/js/common.js', 'public/js')
    .styles([
        'public/libs/css/bootstrap-tagsinput.css',
        'public/libs/css/bootstrap-daterangepicker/daterangepicker.css',
        'public/libs/css/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
        'public/libs/css/select2/dist/css/select2.min.css',
        'public/libs/css/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        'public/libs/css/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
        'public/libs/css/dataTables.bootstrap4.min.css',
        'public/libs/css/select.bootstrap4.min.css',
        'public/libs/css/flatpickr/themes/dark.css',
        'public/libs/css/flatpickr/flatpickr.min.css'
    ], 'public/css/libs.css')
    .minify(['public/js/app.js', 'public/css/app.css', 'public/css/libs.css'])
   /* .styles('node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css', 'public/libs/css/bootstrap-tagsinput.css')
    .styles('node_modules/bootstrap-daterangepicker/daterangepicker.css', 'public/libs/css/bootstrap-daterangepicker/daterangepicker.css')
    .styles('node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css', 'public/libs/css/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')
    .styles('node_modules/select2/dist/css/select2.min.css', 'public/libs/css/select2/dist/css/select2.min.css')

    .styles('node_modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css', 'public/libs/css/bootstrap-timepicker/css/bootstrap-timepicker.min.css')
    .styles('node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css', 'public/libs/css/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')
    .styles('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css', 'public/libs/css/dataTables.bootstrap4.min.css')
    .styles('node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css', 'public/libs/css/select.bootstrap4.min.css')
    .styles('node_modules/flatpickr/dist/themes/dark.css', 'public/libs/css/flatpickr/themes/dark.css')
    .styles('node_modules/flatpickr/dist/flatpickr.min.css', 'public/libs/css/flatpickr/flatpickr.min.css')*/
    // JS
    .copy('resources/js/tinymce/langs/ru.js', 'public/libs/js/tinymce/langs/ru.js')

    .copy('node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js', 'public/libs/js/bootstrap-tagsinput.min.js')
    .copy('node_modules/chart.js/dist/Chart.min.js', 'public/libs/js/Chart.min.js')
    .copy('node_modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js', 'public/libs/js/bootstrap-timepicker.min.js')
    .copy('node_modules/datatables/media/js/jquery.dataTables.min.js', 'public/libs/js/jquery.dataTables.min.js')
    .copy('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js', 'public/libs/js/dataTables.bootstrap4.min.js')
    .copy('node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js', 'public/libs/js/select.bootstrap4.min.js')
    .copy('node_modules/jquery-mask-plugin/dist/jquery.mask.min.js', 'public/libs/js/jquery-mask-plugin/jquery.mask.min.js')
    .copy('node_modules/moment/min/moment-with-locales.min.js', 'public/libs/js/moment/moment-with-locales.min.js')
    .copy('node_modules/moment/min/moment.min.js', 'public/libs/js/moment/moment.min.js')
    .copy('node_modules/nicescroll/dist/jquery.nicescroll.min.js', 'public/libs/js/nicescroll/jquery.nicescroll.min.js')


    .copy('node_modules/bootstrap-daterangepicker/daterangepicker.js', 'public/libs/js/bootstrap-daterangepicker/daterangepicker.js')
    .copy('node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js', 'public/libs/js/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')
    .copy('node_modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js', 'public/libs/js/bootstrap-timepicker/js/bootstrap-timepicker.min.js')
    .copy('node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js', 'public/libs/js/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')
    .copy('node_modules/select2/dist/js/select2.full.min.js', 'public/libs/js/select2/dist/js/select2.full.min.js')

    .copy('node_modules/flatpickr/dist/flatpickr.min.js', 'public/libs/js/flatpickr/flatpickr.min.js')
    .copy('resources/js/shared', 'public/js/shared', false)
    .version();
