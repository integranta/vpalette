<?php

return [
    'name' => 'Sitemap',
    'name_tables' => [
        'brand' => 'brands',
        'category' => 'categories',
        'page' => 'pages',
        'news' => 'news',
        'product' => 'products',
        'stock' => 'stocks'
    ],
    'urls' => [
        'base_url' => 'http://www.vpalette.loc/admin/',
        'brands' => 'http://www.vpalette.loc/admin/sitemap.xml/brands/',
        'categories' => 'http://www.vpalette.loc/admin/sitemap.xml/categories/',
        'posts' => 'http://www.vpalette.loc/admin/sitemap.xml/posts/',
        'pages' => 'http://www.vpalette.loc/admin/sitemap.xml/pages/',
        'products' => 'http://www.vpalette.loc/admin/sitemap.xml/products/',
        'stocks' => 'http://www.vpalette.loc/admin/sitemap.xml/brands/'
    ],
    'max_items' => env('SITEMAP_MAX_ITEMS', 50000),
    'timezone' => env('SITEMAP_TIMEZONE', 'Europe/Samara'),
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all sitemaps as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('SITEMAP_CACHE_KEY', 'index_sitemaps'),
    'cache_ttl' => env('SITEMAP_CACHE_TTL', 360000),
];
