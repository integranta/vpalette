<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Sitemap\Http\Controllers\API\V1\SitemapAPIController;

Route::prefix('v1')->group(function () {
	Route::get('/sitemap', [SitemapAPIController::class, 'index']);
	Route::get('/sitemap/brands/{id}', [SitemapAPIController::class, 'brands']);
	Route::get('/sitemap/categories/{id}', [SitemapAPIController::class, 'categories']);
	Route::get('/sitemap/pages/{id}', [SitemapAPIController::class, 'pages']);
	Route::get('/sitemap/posts/{id}', [SitemapAPIController::class, 'posts']);
	Route::get('/sitemap/products/{id}', [SitemapAPIController::class, 'products']);
	Route::get('/sitemap/stocks/{id}', [SitemapAPIController::class, 'stocks']);
});
