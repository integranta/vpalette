<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Sitemap\Http\Controllers\SitemapController;

Route::name('admin.')
	->prefix('admin')
	->group(function () {
		Route::get('/sitemap.xml', [SitemapController::class, 'index'])
			->name('sitemap');
		Route::get('/sitemap.xml/brands/{id}', [SitemapController::class, 'brands'])
			->name('sitemap.brands');
		Route::get('/sitemap.xml/categories/{id}', [SitemapController::class, 'categories'])
			->name('sitemap.categories');
		Route::get('/sitemap.xml/pages/{id}', [SitemapController::class, 'pages'])
			->name('sitemap.pages');
		Route::get('/sitemap.xml/posts/{id}', [SitemapController::class, 'posts'])
			->name('sitemap.posts');
		Route::get('/sitemap.xml/products/{id}', [SitemapController::class, 'products'])
			->name('sitemap.products');
		Route::get('/sitemap.xml/stocks/{id}', [SitemapController::class, 'stocks'])
			->name('sitemap.stocks');
	});
