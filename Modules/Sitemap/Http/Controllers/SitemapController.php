<?php

namespace Modules\Sitemap\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Sitemap\Services\SitemapWebService;


/**
 * Class SitemapController
 *
 * @package Modules\Sitemap\Http\Controllers
 */
class SitemapController extends Controller
{
    /** @var SitemapWebService */
    private $sitemapWebService;

    /**
     * SitemapController constructor.
     *
     * @param  SitemapWebService  $service
     */
    public function __construct(SitemapWebService $service)
    {
        $this->sitemapWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->sitemapWebService->index();
    }

    /**
     * Generate sitemap.xml for model Brand
     *
     * @param  int  $id
     *
     * @return Response|void
     */
    public function brands(int $id): Response
    {
        return $this->sitemapWebService->brands($id);
    }

    /**
     * Generate sitemap.xml for model Category
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function categories(int $id): Response
    {
        return $this->sitemapWebService->categories($id);
    }


    /**
     * Generate sitemap.xml for model News
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function posts(int $id): Response
    {
        return $this->sitemapWebService->posts($id);
    }

    /**
     * Generate sitemap.xml for model Page
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function pages(int $id): Response
    {
        return $this->sitemapWebService->pages($id);
    }


    /**
     * Generate sitemap.xml for model Product
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function products(int $id): Response
    {
        return $this->sitemapWebService->products($id);
    }

    /**
     * Generate sitemap.xml for model Stock
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function stocks(int $id): Response
    {
        return $this->sitemapWebService->stocks($id);
    }
}
