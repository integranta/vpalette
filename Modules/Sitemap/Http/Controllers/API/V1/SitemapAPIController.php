<?php

namespace Modules\Sitemap\Http\Controllers\API\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Sitemap\Services\API\V1\SitemapApiService;

/**
 * Class SitemapAPIController
 *
 * @package Modules\Sitemap\Http\Controllers\API\V1
 */
class SitemapAPIController extends Controller
{
    /** @var SitemapApiService */
    private $sitemapApiService;

    /**
     * SitemapController constructor.
     *
     * @param  SitemapApiService  $service
     */
    public function __construct(SitemapApiService $service)
    {
        $this->sitemapApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->sitemapApiService->index();
    }

    /**
     * Generate sitemap.xml for model Brand
     *
     * @param  int  $id
     *
     * @return JsonResponse|void
     */
    public function brands(int $id): JsonResponse
    {
        return $this->sitemapApiService->brands($id);
    }

    /**
     * Generate sitemap.xml for model Category
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function categories(int $id): JsonResponse
    {
        return $this->sitemapApiService->categories($id);
    }


    /**
     * Generate sitemap.xml for model News
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function posts(int $id): JsonResponse
    {
        return $this->sitemapApiService->posts($id);
    }

    /**
     * Generate sitemap.xml for model Page
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function pages(int $id): JsonResponse
    {
        return $this->sitemapApiService->pages($id);
    }


    /**
     * Generate sitemap.xml for model Product
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function products(int $id): JsonResponse
    {
        return $this->sitemapApiService->products($id);
    }

    /**
     * Generate sitemap.xml for model Stock
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function stocks(int $id): JsonResponse
    {
        return $this->sitemapApiService->stocks($id);
    }
}
