<?php

return [
    'success_retrieve' => 'Data successfully retrieved to build a Sitemap.',
    
    'success_retrieve_brands' => 'Part of the data for building a Sitemap for brands was successfully extracted',
    'not_found_part_brands' => 'Part of the data for building a Sitemap for brands was not found',
    
    'success_retrieve_categories' => 'Part of the data for building a Sitemap for categories was successfully extracted',
    'not_found_part_categories' => 'Part of the data for building a Sitemap for categories was not found',
    
    'success_retrieve_posts' => 'Part of the data for building a Sitemap for posts was successfully extracted',
    'not_found_part_posts' => 'Part of the data for building a Sitemap for posts was not found',
    
    'success_retrieve_pages' => 'Part of the data for building a Sitemap for pages was successfully extracted',
    'not_found_part_pages' => 'Part of the data for building a Sitemap for pages was not found',
    
    'success_retrieve_products' => 'Part of the data for building a Sitemap for products was successfully extracted',
    'not_found_part_products' => 'Part of the data for building a Sitemap for products was not found',
    
    'success_retrieve_stocks' => 'Part of the data for building a Sitemap for stocks was successfully extracted',
    'not_found_part_stocks' => 'Part of the data for building a Sitemap for stocks was not found',

];
