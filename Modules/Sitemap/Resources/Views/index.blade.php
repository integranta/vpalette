<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
        http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd"
>
    @foreach($brands as $key => $brand)
        <sitemap>
            <loc>{{ config('sitemap.urls.brands') . $key }}</loc>
        </sitemap>
    @endforeach

    @foreach($categories as $key => $category)
        <sitemap>
            <loc>{{ config('sitemap.urls.categories') . $key }}</loc>
        </sitemap>
    @endforeach

    @foreach($posts as $key => $post)
        <sitemap>
            <loc>{{ config('sitemap.urls.posts') . $key }}</loc>
        </sitemap>
    @endforeach

    @foreach($pages as $key => $page)
        <sitemap>
            <loc>{{ config('sitemap.urls.pages') . $key }}</loc>
        </sitemap>
    @endforeach

    @foreach($products as $key => $product)
        <sitemap>
            <loc>{{ config('sitemap.urls.products') . $key }}</loc>
        </sitemap>
    @endforeach

    @foreach($stocks as $key => $stock)
        <sitemap>
            <loc>{{ config('sitemap.urls.stocks') . $key }}</loc>
        </sitemap>
    @endforeach
</sitemapindex>
