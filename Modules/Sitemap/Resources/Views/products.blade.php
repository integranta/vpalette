<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
>
    @foreach ($arProducts as $product)
        @if ($product->full_paths !== null)
            @foreach(json_decode(json_decode($product->full_paths, true, 512, JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR) as $path)
                <url>
                    <loc>{{ config('sitemap.base_url'). $path }}</loc>
                    <lastmod>{{ $product->updated_at }}</lastmod>
                    <changefreq>weekly</changefreq>
                    <priority>0.6</priority>
                </url>
            @endforeach
        @else
            <url>
                <loc>{{ config('sitemap.base_url'). 'catalog/products/' . $product->slug }}</loc>
                <lastmod>{{ $product->updated_at }}</lastmod>
                <changefreq>weekly</changefreq>
                <priority>0.6</priority>
            </url>
        @endif
    @endforeach
</urlset>
