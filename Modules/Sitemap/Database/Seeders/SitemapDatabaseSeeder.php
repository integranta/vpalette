<?php

namespace Modules\Sitemap\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

/**
 * Class SitemapDatabaseSeeder
 *
 * @package Modules\Sitemap\Database\Seeders
 */
class SitemapDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        // $this->call("OthersTableSeeder");
    }
}
