<?php

namespace Modules\Sitemap\Services;

use App\Services\BaseService;
use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Schema;

/**
 * Class SitemapWebService
 *
 * @package Modules\Sitemap\Services
 */
class SitemapWebService extends BaseService
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        
        $brands = $this->getCollectionBrands();
        $categories = $this->getCollectionCategories();
        $pages = $this->getCollectionPages();
        $posts = $this->getCollectionNews();
        $products = $this->getCollectionProducts();
        $stocks = $this->getCollectionStocks();
        
        return response()->view('sitemap::index', compact(
            'brands',
            'categories',
            'pages',
            'posts',
            'products',
            'stocks'
        ))->header('Content-Type', 'text/xml');
    }
    
    /**
     * Return array limit max items for model Brand
     *
     * @return array
     */
    private function getCollectionBrands(): array
    {
        return collect($this->getLatest(config('sitemap.name_tables.brand')))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return latest records sorted by updated_at in specify table
     *
     * @param  string  $nameTable
     *
     * @return null|Collection
     */
    private function getLatest(string $nameTable): ?Collection
    {
        if ($nameTable !== '' && Schema::hasTable($nameTable)) {
            return Cache::remember(
                config('sitemap.cache_key').$nameTable,
                config('sitemap.cache_ttl'),
                function () use ($nameTable) {
                    return DB::table($nameTable)->latest('updated_at')->get();
                }
            );
        }
        
        return null;
    }
    
    /**
     * Return array limit max items for model Category
     *
     * @return array
     */
    private function getCollectionCategories(): array
    {
        return collect($this->getLatest(config('sitemap.name_tables.category')))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model Page
     *
     * @return array
     */
    private function getCollectionPages(): array
    {
        return collect($this->getLatest(config('sitemap.name_tables.page')))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model News
     *
     * @return array
     */
    private function getCollectionNews(): array
    {
        return collect($this->getLatest(config('sitemap.name_tables.news')))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model Product
     *
     * @return array
     */
    private function getCollectionProducts(): array
    {
        return collect($this->getLatest(config('sitemap.name_tables.product')))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model Stock
     *
     * @return array
     */
    private function getCollectionStocks(): array
    {
        return collect($this->getLatest(config('sitemap.name_tables.stock')))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Generate sitemap.xml for model Brand
     *
     * @param  int  $id
     *
     * @return Response|void
     */
    public function brands(int $id): Response
    {
        $brands = $this->getCollectionBrands();
        $arBrands = $this->getPartSitemap($id, $brands);
        
        return response()->view('sitemap::brands', compact('arBrands'))
            ->header('Content-Type', 'text/xml');
    }
    
    /**
     * @param  int    $id
     * @param  array  $items
     *
     * @return array
     */
    private function getPartSitemap(int $id, array $items): array
    {
        $arResult = [];
        if ($id !== null) {
            foreach ($items as $key => $item) {
                $this->getTransformUpdatedAtAttribute($item, $key);
                
                if ($key === $id) {
                    $arResult = $item;
                }
            }
        }
        return $arResult;
    }
    
    /**
     * Return transformed field updated_at record to Atom string format
     *
     * @param  array  $item
     * @param  int    $key
     */
    private function getTransformUpdatedAtAttribute(array $item, int $key): void
    {
        $item[$key]->updated_at = Carbon::createFromTimeString($item[$key]->updated_at, config('sitemap.timezone'))
            ->toAtomString();
    }
    
    /**
     * Generate sitemap.xml for model Category
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function categories(int $id): Response
    {
        $categories = $this->getCollectionCategories();
        $arCategories = $this->getPartSitemap($id, $categories);
        
        return response()->view('sitemap::categories', compact('arCategories'))
            ->header('Content-Type', 'text/xml');
    }
    
    /**
     * Generate sitemap.xml for model News
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function posts(int $id): Response
    {
        $posts = $this->getCollectionNews();
        $arPosts = $this->getPartSitemap($id, $posts);
        
        return response()->view('sitemap::posts', compact('arPosts'))
            ->header('Content-Type', 'text/xml');
    }
    
    /**
     * Generate sitemap.xml for model Page
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function pages(int $id): Response
    {
        $pages = $this->getCollectionPages();
        $arPages = $this->getPartSitemap($id, $pages);
        
        return response()->view('sitemap::pages', compact('arPages'))
            ->header('Content-Type', 'text/xml');
    }
    
    /**
     * Generate sitemap.xml for model Product
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function products(int $id): Response
    {
        $products = $this->getCollectionProducts();
        $arProducts = $this->getPartSitemap($id, $products);
        
        return response()->view('sitemap::products', compact('arProducts'))
            ->header('Content-Type', 'text/xml');
    }
    
    /**
     * Generate sitemap.xml for model Stock
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function stocks(int $id): Response
    {
        $stocks = $this->getCollectionStocks();
        $arStocks = $this->getPartSitemap($id, $stocks);
        
        return response()->view('sitemap::pages', compact('arStocks'))
            ->header('Content-Type', 'text/xml');
    }
}
