<?php

namespace Modules\Sitemap\Services\API\V1;

use App\Services\BaseService;
use Carbon\Carbon;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Class SitemapApiService
 *
 * @package Modules\Sitemap\Services\API\V1
 */
class SitemapApiService extends BaseService
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $brands = $this->getCollectionBrands();
        $categories = $this->getCollectionCategories();
        $pages = $this->getCollectionPages();
        $posts = $this->getCollectionNews();
        $products = $this->getCollectionProducts();
        $stocks = $this->getCollectionStocks();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('sitemap::messages.success_retrieve'),
            [
                'brands' => $brands,
                'categories' => $categories,
                'pages' => $pages,
                'posts' => $posts,
                'products' => $products,
                'stocks' => $stocks
            ]
        );
    }
    
    /**
     * Return array limit max items for model Brand
     *
     * @return array
     */
    private function getCollectionBrands(): array
    {
        return collect(DB::table(config('sitemap.name_tables.brand'))
            ->latest('updated_at')
            ->get(['id', 'slug', 'updated_at']))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model Category
     *
     * @return array
     */
    private function getCollectionCategories(): array
    {
        return collect(DB::table(config('sitemap.name_tables.category'))
            ->latest('updated_at')
            ->get(['id', 'slug', 'full_path', 'updated_at']))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model Page
     *
     * @return array
     */
    private function getCollectionPages(): array
    {
        return collect(DB::table(config('sitemap.name_tables.page'))
            ->latest('updated_at')
            ->get(['id', 'slug', 'updated_at']))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model News
     *
     * @return array
     */
    private function getCollectionNews(): array
    {
        return collect(DB::table(config('sitemap.name_tables.news'))
            ->latest('updated_at')
            ->get(['id', 'slug', 'updated_at']))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model Product
     *
     * @return array
     */
    private function getCollectionProducts(): array
    {
        return collect(DB::table(config('sitemap.name_tables.product'))
            ->latest('updated_at')
            ->get(['id', 'full_path', 'full_paths', 'updated_at']))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Return array limit max items for model Stock
     *
     * @return array
     */
    private function getCollectionStocks(): array
    {
        return collect(DB::table(config('sitemap.name_tables.stock'))
            ->latest('updated_at')
            ->get(['id', 'slug', 'updated_at']))
            ->chunk(config('sitemap.max_items'))
            ->toArray();
    }
    
    /**
     * Generate sitemap.xml for model Brand
     *
     * @param  int  $id
     *
     * @return JsonResponse|void
     */
    public function brands(int $id): JsonResponse
    {
        $brands = $this->getCollectionBrands();
        
        $arBrands = $this->getPartSitemap($id, $brands);
        
        if (!$arBrands) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('sitemap::messages.not_found_part_brands')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_FOUND,
            __('sitemap::messages.success_retrieve_brands'),
            $arBrands
        );
    }
    
    /**
     * @param  int    $id
     * @param  array  $items
     *
     * @return array
     */
    private function getPartSitemap(int $id, array $items): array
    {
        $arResult = [];
        if ($id !== null) {
            foreach ($items as $key => $arItem) {
                foreach ($arItem as $item) {
                    $this->getTransformUpdatedAtAttribute($item);
                    
                    if ($key === $id) {
                        $arResult = $arItem;
                    }
                }
            }
        }
        return $arResult;
    }
    
    /**
     * Return transformed field updated_at record to Atom string format
     *
     * @param  object  $item
     *
     * @return void
     */
    private function getTransformUpdatedAtAttribute(object $item): void
    {
        $item->updated_at = Carbon::createFromTimeString(
            $item->updated_at,
            config('sitemap.timezone'))
            ->toAtomString();
    }
    
    /**
     * Generate sitemap.xml for model Category
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function categories(int $id): JsonResponse
    {
        $categories = $this->getCollectionCategories();
        $arCategories = $this->getPartSitemap($id, $categories);
        
        if (!$arCategories) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('sitemap::messages.not_found_part_categories')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_FOUND,
            __('sitemap::messages.success_retrieve_categories'),
            $arCategories
        );
    }
    
    /**
     * Generate sitemap.xml for model News
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function posts(int $id): JsonResponse
    {
        $posts = $this->getCollectionNews();
        $arPosts = $this->getPartSitemap($id, $posts);
        
        if (!$arPosts) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('sitemap::messages.not_found_part_posts')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_FOUND,
            __('sitemap::messages.success_retrieve_posts'),
            $arPosts
        );
    }
    
    /**
     * Generate sitemap.xml for model Page
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function pages(int $id): JsonResponse
    {
        $pages = $this->getCollectionPages();
        $arPages = $this->getPartSitemap($id, $pages);
        
        if (!$arPages) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('sitemap::messages.not_found_part_pages')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_FOUND,
            __('sitemap::messages.success_retrieve_pages'),
            $arPages
        );
    }
    
    /**
     * Generate sitemap.xml for model Product
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function products(int $id): JsonResponse
    {
        $products = $this->getCollectionProducts();
        $arProducts = $this->getPartSitemap($id, $products);
        
        if (!$arProducts) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('sitemap::messages.not_found_part_products')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_FOUND,
            __('sitemap::messages.success_retrieve_products'),
            $arProducts
        );
    }
    
    /**
     * Generate sitemap.xml for model Stock
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function stocks(int $id): JsonResponse
    {
        $stocks = $this->getCollectionStocks();
        $arStocks = $this->getPartSitemap($id, $stocks);
        
        if (!$arStocks) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('sitemap::messages.not_found_part_stocks')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_FOUND,
            __('sitemap::messages.success_retrieve_stocks'),
            $arStocks
        );
    }
}
