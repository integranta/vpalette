<?php

namespace Modules\Banner\Models;

use App\Enums\BannerType;
use App\Models\BaseModel;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Storage;


/**
 * Class Banner
 *
 * @package Modules\Banner\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $slug
 * @property string $name
 * @property string|null $text
 * @property string|null $buttonText
 * @property string|null $buttonLink
 * @property null|string $bannerSrc
 * @property string $typeBanner
 * @property string|null $bannerLink
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Banner\Models\Banner onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereBannerLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereBannerSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereButtonLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereButtonText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereTypeBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Banner\Models\Banner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Banner\Models\Banner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Banner\Models\Banner withoutTrashed()
 * @mixin \Eloquent
 */
class Banner extends BaseModel
{
    use SoftDeletes;
    use CastsEnums;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'banners';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'slug',
        'name',
        'text',
        'button_text',
        'button_link',
        'sort',
        'banner_src',
        'type_banner',
        'banner_link',
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'text' => 'string',
        'button_text' => 'string',
        'button_link' => 'string',
        'banner_src' => 'string',
        'type_banner' => 'string',
        'banner_link' => 'string',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'type_banner' => BannerType::class
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required|boolean',
        'name' => 'required|string|max:191',
        'text' => 'nullable|string|max:191',
        'button_text' => 'nullable|string',
        'button_link' => 'nullable|string',
        'sort' => 'nullable|integer|min:1',
        'banner_src' => 'required',
        'type_banner' => 'required',
        'banner_link' => 'nullable|string'
    ];


    /**
     * Возвращает список значений типов баннеров из enum класса для select тега.
     *
     * @return array
     */
    public static function getTypeBanners(): array
    {
        return BannerType::toSelectArray();
    }

    /**
     * Получить ссылку на баннер.
     *
     * @param  string|null  $value
     *
     * @return null|string
     */
    public function getBannerSrcAttribute(?string $value): ?string
    {
        return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
    }
}
