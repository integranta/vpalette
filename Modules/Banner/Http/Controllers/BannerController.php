<?php

namespace Modules\Banner\Http\Controllers;

use App\DataTables\BannersDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Banner\Http\Requests\CreateBannerRequest;
use Modules\Banner\Http\Requests\UpdateBannerRequest;
use Modules\Banner\Services\BannerWebService;

/**
 * Class BannerController
 *
 * @package Modules\Banner\Http\Controllers
 */
class BannerController extends Controller
{
    /*** @var BannerWebService */
    private $bannerWebService;

    /**
     * BannerController constructor.
     *
     * @param  BannerWebService  $service
     */
    public function __construct(BannerWebService $service)
    {
        $this->bannerWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Banner.
	 *
	 * @param  \App\DataTables\BannersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(BannersDataTable $dataTable)
    {
        return $this->bannerWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Banner.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->bannerWebService->create();
    }

    /**
     * Store a newly created Banner in storage.
     *
     * @param  CreateBannerRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateBannerRequest $request): RedirectResponse
    {
        return $this->bannerWebService->store($request);
    }

    /**
     * Display the specified Banner.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->bannerWebService->show($id);
    }

    /**
     * Show the form for editing the specified Banner.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->bannerWebService->edit($id);
    }

    /**
     * Update the specified Banner in storage.
     *
     * @param  int                  $id
     * @param  UpdateBannerRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateBannerRequest $request): RedirectResponse
    {
        return $this->bannerWebService->update($id, $request);
    }

    /**
     * Remove the specified Banner from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->bannerWebService->destroy($id);
    }
}
