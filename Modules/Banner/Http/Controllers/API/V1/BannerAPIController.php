<?php

namespace Modules\Banner\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Banner\Http\Requests\API\V1\CreateBannerAPIRequest;
use Modules\Banner\Http\Requests\API\V1\UpdateBannerAPIRequest;
use Modules\Banner\Services\API\V1\BannerApiService;

/**
 * Class BannerAPIController
 *
 * @package Modules\Banner\Http\Controllers\API\V1
 */
class BannerAPIController extends Controller
{
    /** @var BannerApiService */
    private $bannerApiService;

    /**
     * BannerAPIController constructor.
     *
     * @param  BannerApiService  $service
     *
     */
    public function __construct(BannerApiService $service)
    {
        $this->bannerApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->bannerApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBannerAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateBannerAPIRequest $request): JsonResponse
    {
        return $this->bannerApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->bannerApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBannerAPIRequest  $request
     * @param  int                     $id
     *
     * @return JsonResponse
     */
    public function update(UpdateBannerAPIRequest $request, int $id): JsonResponse
    {
        return $this->bannerApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->bannerApiService->destroy($id);
    }

    /**
     * Return the enum values types banner
     *
     * @return JsonResponse
     */
    public function getBannerTypes(): JsonResponse
    {
        return $this->bannerApiService->getBannerTypes();
    }
}
