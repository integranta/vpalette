<?php

namespace Modules\Banner\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateBannerAPIRequest
 *
 * @package Modules\Banner\Http\Requests\API\V1
 */
class UpdateBannerAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'bannerFields.name' => 'required|string|max:191',
            'bannerFields.text' => 'nullable|string|max:191',
            'bannerFields.button_text' => 'nullable|string',
            'bannerFields.button_link' => 'nullable|string',
            'bannerFields.sort' => 'nullable|integer|min:1',
            'bannerFields.banner_src' => 'ext:jpg,jpeg,png,bmp,svg|max:1000',
            'bannerFields.type_banner' => 'required',
            'bannerFields.banner_link' => 'nullable|string'
        ];
    }
}
