<?php

namespace Modules\Banner\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Banner\Models\Banner;

/**
 * Class UpdateBannerRequest
 *
 * @package Modules\Banner\Http\Requests
 */
class UpdateBannerRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
        
        ];
    }
}
