<?php

namespace Modules\Banner\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Banner\Models\Banner;

/**
 * Class CreateBannerRequest
 *
 * @package Modules\Banner\Http\Requests
 */
class CreateBannerRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Banner::$rules;
    }
}
