<?php

namespace Modules\Banner\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BannerResource
 *
 * @package Modules\Banner\Transformers
 */
class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'isActive' => $this->active,
            'name' => $this->name,
            'sort' => $this->sort,
            'text' => $this->text,
            'bannerImageSrc' => $this->banner_src,
            'bannerType' => $this->type_banner,
            'bannerUrl' => $this->banner_link,
            'buttonText' => $this->button_text,
            'buttonUrl' => $this->button_link,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
