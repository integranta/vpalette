<?php

namespace Modules\Banner\Observers;

use App\Traits\Cacheable;
use Modules\Banner\Models\Banner;
use Storage;

/**
 * Class BannerObserver
 *
 * @package Modules\Banner\Observers
 */
class BannerObserver
{
    use Cacheable;
    
    /**
     * Handle the banner "created" event.
     *
     * @param  Banner  $banner
     *
     * @return void
     */
    public function created(Banner $banner): void
    {
        $this->incrementCountItemsInCache($banner, config('banner.cache_key'));
    }
    
    /**
     * Handle the banner "updated" event.
     *
     * @param  Banner  $banner
     *
     * @return void
     */
    public function updated(Banner $banner): void
    {
        //
    }
    
    /**
     * Handle the banner "deleting" event.
     *
     * @param  Banner  $banner
     */
    public function deleting(Banner $banner): void
    {
        $path = 'banners/'.$banner->slug;
        
        if (Storage::disk('uploads')->exists($path)) {
            Storage::disk('uploads')->deleteDirectory($path);
        }
    }
    
    /**
     * Handle the banner "deleted" event.
     *
     * @param  Banner  $banner
     *
     * @return void
     */
    public function deleted(Banner $banner): void
    {
        $this->decrementCountItemsInCache($banner, config('banner.cache_key'));
    }
    
    /**
     * Handle the banner "restored" event.
     *
     * @param  Banner  $banner
     *
     * @return void
     */
    public function restored(Banner $banner): void
    {
        $this->incrementCountItemsInCache($banner, config('banner.cache_key'));
    }
    
    /**
     * Handle the banner "force deleted" event.
     *
     * @param  Banner  $banner
     *
     * @return void
     */
    public function forceDeleted(Banner $banner): void
    {
        $this->decrementCountItemsInCache($banner, config('banner.cache_key'));
    }
}
