<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Banner\Http\Controllers\API\V1\BannerAPIController;

Route::prefix('v1')->group(function () {
    Route::apiResource('banners', BannerAPIController::class);
    Route::get('banners/get/banner-types', [BannerAPIController::class, 'getBannerTypes']);
});
