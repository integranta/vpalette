<?php

namespace Modules\Banner\Database\Seeders;

use App\Enums\BannerType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Banner\Models\Banner;
use Storage;
use Str;

/**
 * Class BannerDatabaseSeeder
 *
 * @package Modules\Banner\Database\Seeders
 */
class BannerDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $randomName = Str::random();


        $fromSlidersImage = "faker/banners/sliders/top-slider.jpg";
        $fromSmallBanners = 'faker/banners/small/stock-block-img.jpg';


        $bannerSlug = Str::of('Товары для вашего творчества')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            $fromSlidersImage,
            "banners/${bannerSlug}/slider/${randomName}.jpg"
        );

        Banner::create([
            'active' => 1,
            'name' => 'Товары для вашего творчества',
            'text' => 'Используйте все возможности сервиса для достижения ваших целей',
            'button_text' => 'Смотреть',
            'button_link' => '/',
            'sort' => 500,
            'banner_src' => "banners/${bannerSlug}/slider/${randomName}.jpg",
            'type_banner' => BannerType::SLIDER,
            'banner_link' => null,
            'created_at' => Carbon::now(env('APP_TIMEZONE')),
            'updated_at' => Carbon::now(env('APP_TIMEZONE'))
        ]);

        $bannerSlug = Str::of('Лучшие товары недели')->lower()->trim()->slug('_', 'ru');

        Storage::disk('uploads')->copy(
            'faker/banners/big/best-img-1.jpg',
            "banners/${bannerSlug}/big/${randomName}.jpg"
        );

        Banner::create([
            'active' => 1,
            'name' => 'Лучшие товары недели',
            'text' => null,
            'button_text' => 'Смотреть',
            'button_link' => '/',
            'sort' => 500,
            'banner_src' => "banners/${bannerSlug}/big/${randomName}.jpg",
            'type_banner' => BannerType::BIG,
            'banner_link' => null,
            'created_at' => Carbon::now(env('APP_TIMEZONE')),
            'updated_at' => Carbon::now(env('APP_TIMEZONE'))
        ]);

        $bannerSlug = Str::of('Самые низкие цены')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            'faker/banners/big/best-img-2.jpg',
            "banners/${bannerSlug}/big/${randomName}.jpg"
        );

        Banner::create([
            'active' => 1,
            'name' => 'Самые низкие цены',
            'text' => null,
            'button_text' => 'Смотреть',
            'button_link' => '/',
            'sort' => 500,
            'banner_src' => "banners/${bannerSlug}/big/${randomName}.jpg",
            'type_banner' => BannerType::BIG,
            'banner_link' => null,
            'created_at' => Carbon::now(env('APP_TIMEZONE')),
            'updated_at' => Carbon::now(env('APP_TIMEZONE'))
        ]);

        $bannerSlug = Str::of('Подарки')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            $fromSmallBanners,
            "banners/${bannerSlug}/small/${randomName}.jpg"
        );

        Banner::create([
            'active' => 1,
            'name' => 'Подарки',
            'text' => null,
            'button_text' => null,
            'button_link' => '/',
            'sort' => 500,
            'banner_src' => "banners/${bannerSlug}/small/${randomName}.jpg",
            'type_banner' => BannerType::SMALL,
            'banner_link' => null,
            'created_at' => Carbon::now(env('APP_TIMEZONE')),
            'updated_at' => Carbon::now(env('APP_TIMEZONE'))
        ]);

        // factory(Banner::class, 5)->create();
    }
}
