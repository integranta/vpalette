<?php

use App\Enums\BannerType;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Http\UploadedFile;
use Modules\Banner\Models\Banner;


/** @var Factory $factory */
$factory->define(Banner::class,
    function (Faker $faker) {
        
        $name = $faker->words(2, true);
        $slug = Str::of($name)->lower()->trim()->slug();
        $path = 'storage/app/public/uploads/banners/'.$slug;
        
        if (!File::exists($path)) {
            File::makeDirectory($path);
        }
        
        $typeBanner = $faker->randomElement(BannerType::getValues());
        
        $imageSrc = 'banners/'.$slug.'/';
        $bannerSrc = null;
        
        $random_int = random_int(1, 50);
        
        if (BannerType::fromValue($typeBanner)->value === BannerType::BIG) {
            $url = "https://i.picsum.photos/id/{$random_int}/1000/838.jpg";
            $file = $path.'/'.Str::of(Str::random().'.jpg')->lower();
            $contents = Image::make($url)->save($file, 95);
            $bannerSrc .= $imageSrc.$contents->basename;
        } elseif (BannerType::fromValue($typeBanner)->value === BannerType::SMALL) {
            $url = "https://i.picsum.photos/id/{$random_int}/305/345.jpg";
            $file = $path.'/'.Str::of(Str::random().'.jpg')->lower();
            $contents = Image::make($url)->save($file, 95);
            $bannerSrc .= $imageSrc.$contents->basename;
        } elseif (BannerType::fromValue($typeBanner)->value === BannerType::SLIDER) {
            $url = "https://i.picsum.photos/id/{$random_int}/1313/395.jpg";
            $file = $path.'/'.Str::of(Str::random().'.jpg')->lower();
            $contents = Image::make($url)->save($file, 95);
            $bannerSrc .= $imageSrc.$contents->basename;
        }
        
        return [
            'active' => true,
            'name' => $name,
            'text' => $faker->realText(100),
            'button_text' => 'Смотреть',
            'button_link' => $faker->url,
            'sort' => 500,
            'banner_src' => $bannerSrc,
            'type_banner' => $typeBanner,
            'banner_link' => $faker->url,
            'created_at' => $faker->date('Y-m-d H:i:s'),
            'updated_at' => $faker->date('Y-m-d H:i:s')
        ];
    });
