<?php

namespace Modules\Banner\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Banner\Models\Banner;

/**
 * Class BannerPolicy
 *
 * @package Modules\Banner\Policies
 */
class BannerPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the banner.
     *
     * @param  User|null  $user
     * @param  Banner     $banner
     *
     * @return mixed
     */
    public function view(?User $user, Banner $banner)
    {
        if ($banner->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished banners')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create banners.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create banners')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the banner.
     *
     * @param  User    $user
     * @param  Banner  $banner
     *
     * @return mixed
     */
    public function update(User $user, Banner $banner)
    {
        if ($user->can('edit own banners')) {
            return $user->id == $banner->user_id;
        }
        
        if ($user->can('edit all banners')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the banner.
     *
     * @param  User    $user
     * @param  Banner  $banner
     *
     * @return mixed
     */
    public function delete(User $user, Banner $banner)
    {
        if ($user->can('delete any banner')) {
            return true;
        }
        
        return false;
    }
}
