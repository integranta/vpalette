<!-- Active field -->
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('banner::templates.fields.labels.active') }}
    </span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        {!! Form::hidden('active', 0) !!}
        {!!
            Form::checkbox(
                'active',
                true,
                old('active', true),
                ['class' => 'custom-control-input', 'id' => 'active']
            )
        !!}
        <label class="custom-control-label ml-lg-3" for="active"></label>
    </div>
</div>

<!-- Name Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'name',
            __('banner::templates.fields.labels.name'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'name',
                old('name', ''),
                [
                    'class' => 'form-control',
                    'placeholder' => __('banner::templates.fields.placeholder.name')
                ]
            )
        !!}
    </div>
</div>

<!-- Text Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'text',
            __('banner::templates.fields.labels.text'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::textarea(
                'text',
                old('text', ''),
                [
                    'class' => 'form-control tinyMCE',
                    'placeholder' => __('banner::templates.fields.placeholder.text')
                ]
            )
        !!}
    </div>
</div>

<!-- Button Text Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'button_text',
            __('banner::templates.fields.labels.button_text'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'button_text',
                old('button_text', ''),
                [
                    'class' => 'form-control',
                    'placeholder' => __('banner::templates.fields.placeholder.button_text')
                ]
            )
        !!}
    </div>
</div>

<!-- Button Link Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'button_link',
            __('banner::templates.fields.labels.button_link'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'button_link',
                old('button_link', ''),
                [
                    'class' => 'form-control',
                    'placeholder' => __('banner::templates.fields.placeholder.button_link')
                ]
            )
        !!}
    </div>
</div>

<!-- Sort Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'sort',
            __('banner::templates.fields.labels.sort'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'sort',
                old('sort', 500),
                [
                    'class' => 'form-control',
                    'placeholder' => __('banner::templates.fields.placeholder.sort')
                ]
            )
        !!}
    </div>
</div>

<!-- Banner Src Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'banner_src',
            __('banner::templates.fields.labels.banner_src'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::file('banner_src') !!}
    </div>
</div>
<div class="clearfix"></div>

<!-- Type Banner Field -->
<div class="form-group row col-md-12 mb-4">
    {!!
        Form::label(
            'type_banner',
            __('banner::templates.fields.labels.type_banner'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    @php
        $types = [
            'slider'    =>  'Слайдер',
            'big'       =>  'Большой',
            'small'     =>  'Маленький',
        ]
    @endphp
    <div class="col-sm-12 col-md-7">
        <select name="type_banner" id="type_banner" class="form-control">
            <option disabled selected>{{ __('banner::templates.fields.placeholder.type_banner') }}</option>
            @foreach($types as $key => $label)
                <option value="{{ $key }}">{{ $label }}</option>
            @endforeach
        </select>
    </div>
</div>

<!-- Banner Link Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'banner_link',
            __('banner::templates.fields.labels.banner_link'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'banner_link',
                old('banner_link', ''),
                [
                    'class' => 'form-control',
                    'placeholder' => __('banner::templates.fields.placeholder.banner_link')
                ]
            )
        !!}
    </div>
</div>
