<?php


namespace Modules\Banner\Repositories;


use App\Repositories\BaseRepository;
use App\Traits\UploadAble;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Str;
use Modules\Banner\Contracts\BannerContract;
use Modules\Banner\Models\Banner;
use Storage;

/**
 * Class BannerRepository
 *
 * @package Modules\Banner\Repositories
 */
class BannerRepository extends BaseRepository implements BannerContract
{
    use UploadAble;

    /**
     * BannerRepository constructor.
     *
     * @param  Banner  $model
     */
    public function __construct(Banner $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function listBanners(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('banner.cache_key').$postfix,
            config('banner.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }

    /**
     * @inheritDoc
     */
    public function findBannerById(int $id): ?Banner
    {
        return $this->find($id);
    }

    /**
     * @inheritDoc
     */
    public function createBanner(array $params): Banner
    {

        $path = 'banners/'.Str::of($params['name'])
                ->trim()
                ->lower()
                ->slug('_', 'ru');

        return $this->create([
            'active' => filter_var($params['active'], FILTER_VALIDATE_BOOLEAN),
            'name' => $params['name'],
            'text' => $params['text'],
            'button_text' => $params['button_text'],
            'button_link' => $params['button_link'],
            'sort' => $params['sort'],
            'type_banner' => $params['type_banner'],
            'banner_link' => $params['banner_link'],
            'banner_src' => $this->makeCachedImage($params['banner_src'], $path)
        ]);
    }

    /**
     * @inheritDoc
     */
    public function updateBanner(array $params, int $id): bool
    {
        $path = 'banners/'.Str::of($params['name'])
                ->trim()
                ->lower()
                ->slug('_', 'ru');

        $bannerData = [
            'active' => filter_var($params['active'], FILTER_VALIDATE_BOOLEAN),
            'name' => $params['name'],
            'text' => $params['text'],
            'button_text' => $params['button_text'],
            'button_link' => $params['button_link'],
            'sort' => $params['sort'],
            'type_banner' => $params['type_banner'],
            'banner_link' => $params['banner_link'],
        ];

        if (isset($params['banner_src'])) {
            $bannerData['banner_src'] = $this->makeCachedImage($params['banner_src'], $path);
        }

        return $this->update($bannerData, $id);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function deleteBanner(int $id): bool
    {
        return $this->delete($id);
    }
}
