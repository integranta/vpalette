<?php


namespace Modules\Banner\Services\API\V1;

use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Banner\Contracts\BannerContract;
use Modules\Banner\Http\Requests\API\V1\CreateBannerAPIRequest;
use Modules\Banner\Http\Requests\API\V1\UpdateBannerAPIRequest;
use Modules\Banner\Models\Banner;
use Modules\Banner\Transformers\BannerResource;

/**
 * Class BrandApiService
 *
 * @package Modules\Banner\Services
 */
class BannerApiService extends BaseService
{
    /** @var BannerContract */
    private $bannerRepository;
    
    
    /**
     * BannerAPIController constructor.
     *
     * @param  BannerContract  $bannerRepo
     *
     * @return void
     */
    public function __construct(BannerContract $bannerRepo)
    {
        $this->bannerRepository = $bannerRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $banners = BannerResource::collection($this->bannerRepository->listBanners());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('banner::messages.success_retrieve'),
            $banners
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBannerAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateBannerAPIRequest $request): JsonResponse
    {
        $banner = $this->bannerRepository->createBanner($request->all());
        
        if (!$banner) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('banner::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('banner::messages.success_create'),
            $banner
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $banner = BannerResource::make($this->bannerRepository->findBannerById($id));
        
        if ($banner === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('banner::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('banner::messages.success_found', ['id' => $id]),
            $banner
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBannerAPIRequest  $request
     * @param  int                     $id
     *
     * @return JsonResponse
     */
    public function update(UpdateBannerAPIRequest $request, int $id): JsonResponse
    {
        $banner = $this->bannerRepository->findBannerById($id);
        
        if ($banner === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('banner::messages.not_found', ['id' => $id])
            );
        }
        
        $banner = $this->bannerRepository->updateBanner($request->all(), $id);
        
        if (!$banner) {
            return $this->responseJson(
                false,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('banner::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('banner::messages.success_update'),
            $banner
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $banner = $this->bannerRepository->findBannerById($id);
        
        if ($banner === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('banner::messages.not_found', ['id' => $id])
            );
        }
        
        $banner = $this->bannerRepository->deleteBanner($id);
        
        if (!$banner) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('banner::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('banner::messages.success_delete'),
            $banner
        );
    }
    
    /**
     * @return JsonResponse
     */
    public function getBannerTypes(): JsonResponse
    {
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('banner::messages.success_retrieve_banner_types'),
            Banner::getTypeBanners()
        );
    }
}
