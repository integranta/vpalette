<?php


namespace Modules\Banner\Services;


use App\DataTables\BannersDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Banner\Contracts\BannerContract;
use Modules\Banner\Http\Requests\CreateBannerRequest;
use Modules\Banner\Http\Requests\UpdateBannerRequest;

/**
 * Class BrandWebService
 *
 * @package Modules\Banner\Services
 */
class BannerWebService extends BaseService
{
	/*** @var BannerContract */
	private $bannerRepository;
	
	/**
	 * BannerController constructor.
	 *
	 * @param  BannerContract  $bannerRepo
	 */
	public function __construct(BannerContract $bannerRepo)
	{
		$this->bannerRepository = $bannerRepo;
	}
	
	/**
	 * Display a listing of the Banner.
	 *
	 * @param  \App\DataTables\BannersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(BannersDataTable $dataTable)
	{
		$this->setPageTitle(
			__('banner::messages.title'),
			__('banner::messages.index_subtitle'),
			__('banner::messages.index_leadtext')
		);
		
		return $dataTable->render('banner::banners.index');
	}
	
	/**
	 * Show the form for creating a new Banner.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$this->setPageTitle(
			__('banner::messages.title'),
			__('banner::messages.create_subtitle'),
			__('banner::messages.create_leadtext')
		);
		return view('banner::banners.create');
	}
	
	/**
	 * Store a newly created Banner in storage.
	 *
	 * @param  CreateBannerRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateBannerRequest $request): RedirectResponse
	{
		$banner = $this->bannerRepository->createBanner($request->all());
		
		if (!$banner) {
			return $this->responseRedirectBack(
				__('banner::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.banners.index',
			__('banner::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified Banner.
	 *
	 * @param  int  $id
	 *
	 * @return View|RedirectResponse|Factory
	 */
	public function show(int $id): View
	{
		$banner = $this->bannerRepository->findBannerById($id);
		
		if (!$banner) {
			return $this->responseRedirect(
				'admin.banners.index',
				__('banner::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$targetTypeBanner = $this->getNameTypeBanner($banner->type_banner);
		
		
		$this->setPageTitle(
			__('banner::messages.title'),
			__('banner::messages.show_subtitle', ['name' => $banner->name]),
			__('banner::messages.show_leadtext', ['name' => $banner->name])
		);
		return view('banner::banners.show', compact(
				'banner',
				'targetTypeBanner'
			)
		);
	}
	
	/**
	 * Get translated name type banner
	 *
	 * @param  string  $type
	 *
	 * @return string
	 */
	private function getNameTypeBanner(string $type): string
	{
		switch ($type) {
			case 'slider':
				return __('banner::banner_type.slider');
				break;
			case 'big':
				return __('banner::banner_type.big');
				break;
			case 'small':
				return __('banner::banner_type.slider.small');
				break;
		}
		
		return $type;
	}
	
	/**
	 * Show the form for editing the specified Banner.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$banner = $this->bannerRepository->findBannerById($id);
		
		if (!$banner) {
			return $this->responseRedirect(
				'admin.banners.index',
				__('banner::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$targetTypeBanner = $this->getNameTypeBanner($banner->type_banner);
		
		
		$this->setPageTitle(
			__('banner::messages.title'),
			__('banner::messages.edit_subtitle', ['name' => $banner->name]),
			__('banner::messages.edit_leadtext', ['name' => $banner->name])
		);
		return view('banner::banners.edit', compact(
				'banner',
				'targetTypeBanner'
			)
		);
	}
	
	/**
	 * Update the specified Banner in storage.
	 *
	 * @param  int                  $id
	 * @param  UpdateBannerRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function update(int $id, UpdateBannerRequest $request): RedirectResponse
	{
		$banner = $this->bannerRepository->findBannerById($id);
		
		if (!$banner) {
			return $this->responseRedirect(
				'admin.banners.index',
				__('banner::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$banner = $this->bannerRepository->updateBanner($request->all(), $id);
		
		if (!$banner) {
			return $this->responseRedirectBack(
				__('banner::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirectBack(
			__('banner::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified Banner from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 *
	 */
	public function destroy(int $id): RedirectResponse
	{
		$banner = $this->bannerRepository->findBannerById($id);
		
		if (!$banner) {
			return $this->responseRedirect(
				'admin.banners.index',
				__('banner::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$banner = $this->bannerRepository->deleteBanner($id);
		
		if (!$banner) {
			return $this->responseRedirectBack(
				__('banner::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.banners.index',
			__('banner::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
