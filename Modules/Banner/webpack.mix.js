const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/banner.js')
    .js(__dirname + '/Resources/Assets/js/index.js', 'js/banner-index.js')
    .js(__dirname + '/Resources/Assets/js/show.js', 'js/banner-show.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/banner.css', {
        implementation: require('node-sass')
    });

if (mix.inProduction()) {
    mix.version();
}
