<?php

return [
    'name' => 'Banner',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all banners as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('BANNER_CACHE_KEY','index_banners'),
    'cache_ttl' => env('BANNER_CACHE_TTL',360000),
];
