<?php


namespace Modules\Banner\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Banner\Models\Banner;

/**
 * Interface BannerContract
 *
 * @package Modules\Banner\Contracts
 */
interface BannerContract
{
    /**
     * Получить список всех баннеров.
     *
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listBanners(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection;
    
    /**
     * Получить баннер по указанному ID.
     *
     * @param  int  $id
     *
     * @return Banner
     */
    public function findBannerById(int $id): ?Banner;
    
    /**
     * Создать новый баннер.
     *
     * @param  array  $params
     *
     * @return Banner
     */
    public function createBanner(array $params): Banner;
    
    /**
     * Обновить баннер по указанному ID.
     *
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateBanner(array $params, int $id): bool;
    
    /**
     * Удалить баннер по указанному ID.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteBanner(int $id): bool;
}
