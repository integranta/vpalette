<?php


namespace Modules\Cart\Services\API\V1;

use App\Services\BaseService;
use Cart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cart\Contracts\CartContract;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;

/**
 * Class CartApiService
 *
 * @package Modules\Cart\Services\API\V1
 */
class CartApiService extends BaseService
{
    /** @var PartnerOfferContract */
    private $offerRepository;
    
    /** @var CartContract */
    private $cartRepository;
    
    /**
     * CartAPIController constructor.
     *
     * @param  PartnerOfferContract  $offerRepo
     * @param  CartContract          $cartRepo
     */
    public function __construct(
        PartnerOfferContract $offerRepo,
        CartContract $cartRepo
    ) {
        $this->offerRepository = $offerRepo;
        $this->cartRepository = $cartRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function getCart(Request $request): JsonResponse
    {
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('cart::messages.success_retrieve'),
            $this->cartRepository->getCart($request->input('user_id'))
        );
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function addToCart(Request $request): JsonResponse
    {
        $offer = $this->offerRepository->findOfferById($request->input('offer_id'));
        
        if ($offer !== null) {
            $offer->load('product');
            $productName = $offer->product->name;
            
            $params = [
                'user_id' => $request->input('user_id'),
                'product_id' => $offer->product->id,
                'offer_id' => $offer->id,
                'product_name' => $productName,
                'price' => $offer->price,
                'qty' => $request->input('qty'),
                'attributes' => [
                    'offer_id' => $offer->id,
                    $request->input('attributes')
                ]
            ];
            
            $total = $this->cartRepository->addToCart($params);
            
            if ($total === null) {
                return $this->responseJson(
                    true,
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    __('cart::messages.error_create', ['product' => $productName])
                );
            }
            return $this->responseJson(
                false,
                Response::HTTP_OK,
                __('cart::messages.success_create', ['product' => $productName]),
                $total
            );
        }
        
        return $this->responseJson(
            true,
            Response::HTTP_NOT_FOUND,
            __('partneroffer::messages.not_found')
        );
    }
    
    /**
     * @param  Request     $request
     * @param  string|int  $id
     *
     * @return JsonResponse
     */
    public function removeItem(Request $request, string $id): JsonResponse
    {
        $item = $this->cartRepository->removeItem($request->input('user_id'), $id);
        
        if (!$item) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart::messages.error_remove_item')
            );
        }
        
        if (Cart::isEmpty()) {
            return $this->responseJson(
                false,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart:messages.empty_cart')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('cart::messages.success_delete_item'),
            $this->cartRepository->getCart($request->input('user_id'))
        );
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function clearCart(Request $request): JsonResponse
    {
        $clearCart = $this->cartRepository->clearCart($request->input('user_id'));
        
        if (!$clearCart) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart::messages.error_clear')
            );
        }
        
        return $this->responseJson(false,
            Response::HTTP_OK,
            __('cart::messages.success_clear'),
            $this->cartRepository->getCart($request->input('user_id'))
        );
    }
    
    /**
     * @param  Request  $request
     * @param  string   $id
     *
     * @return JsonResponse
     */
    public function updateCountCartItem(Request $request, string $id): JsonResponse
    {
        $item = $this->cartRepository->updateCountCartItem($request->all(), $id);
        
        if (!$item) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart::messages.error_update_count')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('cart::messages.success_update_count'),
            $item
        );
    }
    
    /**
     * @param  Request  $request
     */
    public function applyCoupon(Request $request): JsonResponse
    {
        $isSuccess = $this->cartRepository->applyCoupon($request->all());
        
        if (!$isSuccess) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart::messages.error_apply_coupon')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('cart::messages.success_apply_coupon'),
            $isSuccess
        );
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function removeCoupon(Request $request): JsonResponse
    {
        $isRemove = $this->cartRepository->removeCoupon($request->all());
        
        if (!$isRemove) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart::messages.error_remove_coupon')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('cart::messages.success_remove_coupon'),
            $isRemove
        );
    }
}
