<?php


namespace Modules\Cart\Services\API\V1;


use App\Services\BaseService;
use Cart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cart\Contracts\CartCookieContract;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;

/**
 * Class CartCookieService
 *
 * @package Modules\Cart\Services\API\V1
 */
class CartCookieService extends BaseService
{
    /** @var CartCookieContract */
    private $cookieCartRepository;
    
    /** @var PartnerOfferContract */
    private $offerRepository;
    
    /**
     * CartCookieService constructor.
     *
     * @param  CartCookieContract    $cookieCartRepository
     * @param  PartnerOfferContract  $offerRepository
     */
    public function __construct(CartCookieContract $cookieCartRepository, PartnerOfferContract $offerRepository)
    {
        $this->cookieCartRepository = $cookieCartRepository;
        $this->offerRepository = $offerRepository;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function getCart(Request $request): JsonResponse
    {
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('cart::messages.cookie.success_retrieve'),
            $this->cookieCartRepository->getCartItems($request->cookie('shopBasket'))
        );
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse|Response
     */
    public function addToCart(Request $request): Response
    {
        $offer = $this->offerRepository->findOfferById($request->input('offer_id'));
        
        if ($offer !== null) {
            $offer->load('product');
            $productName = $offer->product->name;
            
            $params = [
                'cart_id' => uniqid('cart_', false),
                'product_id' => $offer->product->id,
                'offer_id' => $offer->id,
                'product_name' => $productName,
                'price' => $offer->price,
                'qty' => $request->input('qty'),
                'attributes' => [
                    'offer_id' => $offer->id,
                    $request->input('attributes')
                ]
            ];
            
            return $this->cookieCartRepository->addToCartItem($params);
        }
        
        return $this->responseJson(
            true,
            Response::HTTP_NOT_FOUND,
            __('partneroffer::messages.not_found')
        );
    }
    
    /**
     * @param  Request     $request
     * @param  string|int  $id
     *
     * @return Response|JsonResponse
     */
    public function removeItem(Request $request, string $id): Response
    {
        $item = $this->cookieCartRepository->removeCartItem($request->cookie('shopBasket'), $id);
        
        if (!$item) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart::messages.error_remove_item')
            );
        }
        
        if (Cart::isEmpty()) {
            return $this->responseJson(
                false,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart:messages.empty_cart')
            );
        }
        
        return $item;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function clearCart(Request $request): JsonResponse
    {
        $clearCart = $this->cookieCartRepository->clearCart($request->input('user_id'));
        
        if (!$clearCart) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('cart::messages.error_clear')
            );
        }
        
        return $this->responseJson(false,
            Response::HTTP_OK,
            __('cart::messages.success_clear'),
            $this->cookieCartRepository->getCartItems($request->input('user_id'))
        );
    }
    
    /**
     * @param  Request  $request
     * @param  string   $cartItemId
     *
     * @return Response
     */
    public function updateCountCartItem(Request $request, string $cartItemId): Response
    {
        return $this->cookieCartRepository->updateCountCartItem($cartItemId, $request->input('quantity'));
    }
}
