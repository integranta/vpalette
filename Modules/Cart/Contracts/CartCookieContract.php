<?php


namespace Modules\Cart\Contracts;


use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Interface CartCookieContract
 *
 * @package Modules\Cart\Contracts
 */
interface CartCookieContract
{
    /**
     * @param  string  $cookie
     *
     * @return array
     */
    public function getCartItems(string $cookie): array;
    
    /**
     * @param  array  $params
     *
     * @return Response
     */
    public function addToCartItem(array $params): Response;
    
    /**
     * @param  string  $cartItemId
     * @param  int     $quantity
     *
     * @return Response
     */
    public function updateCountCartItem(string $cartItemId, int $quantity): Response;
    
    /**
     * @param  string  $cookie
     * @param  string  $cartItem
     *
     * @return Response
     */
    public function removeCartItem(string $cookie, string $cartItem): Response;
    
    /**
     * @param  string  $cookie
     *
     * @return bool
     */
    public function clearCart(string $cookie): bool;
}
