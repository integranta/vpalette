<?php


namespace Modules\Cart\Contracts;


/**
 * Interface CartContract
 *
 * @package Modules\Cart\Contracts
 */
interface CartContract
{
    /**
     * @param  array  $params
     *
     * @return null|array
     */
    public function addToCart(array $params): ?array;
    
    /**
     * @param  int  $userId
     *
     * @return array
     */
    public function getCart(int $userId): array;
    
    /**
     * @param  array   $params
     * @param  string  $cartItemId
     *
     * @return null|array
     */
    public function updateCountCartItem(array $params, string $cartItemId): ?array;
    
    /**
     * @param  int     $userId
     * @param  string  $id
     *
     * @return bool
     */
    public function removeItem(int $userId, string $id): bool;
    
    /**
     * @param  int  $userId
     *
     * @return bool
     */
    public function clearCart(int $userId): bool;
    
    /**
     * @param  array  $params
     *
     * @return bool
     */
    public function removeCoupon(array $params): bool;
    
    /**
     * @param  array  $params
     *
     * @return bool
     */
    public function applyCoupon(array $params): bool;
    
}
