<?php

return [
    'name' => 'Cart',
    'current_timezone' => 'Europe/Samara',
    'datatime_format' => 'Y-m-d H:i:s'
];
