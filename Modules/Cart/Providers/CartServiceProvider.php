<?php

namespace Modules\Cart\Providers;

use Config;
use Illuminate\Auth\AuthManager;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Cart\Contracts\CartContract;
use Modules\Cart\Http\Controllers\API\CartAPIController;
use Modules\Cart\Services\CartApiService;
use Modules\Cart\Services\CartCookieService;

/**
 * Class CartServiceProvider
 *
 * @package Modules\Cart\Providers
 */
class CartServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->when(CartAPIController::class)
            ->needs(CartContract::class)
            ->give(static function (AuthManager $authManager) {
                if ($authManager->guard()->user()) {
                    return CartApiService::class;
                }
                
                return CartCookieService::class;
            });
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('cart.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'cart'
        );
    }
    
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/cart');
        
        $sourcePath = __DIR__.'/../Resources/Views';
        
        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');
        
        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path.'/modules/cart';
        }, Config::get('view.paths')), [$sourcePath]), 'cart');
    }
    
    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/cart');
        
        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'cart');
        } else {
            $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'cart');
        }
    }
    
    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__.'/../Database/Factories');
        }
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
