<?php

namespace Modules\Cart\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Cart\Services\API\V1\CartApiService;
use Modules\Cart\Services\API\V1\CartCookieService;


/**
 * Class CartAPIController
 *
 * @package Modules\Cart\Http\Controllers\API\V1
 */
class CartAPIController extends Controller
{
    /** @var CartApiService */
    private $cartApiService;
    
    /** @var CartCookieService */
    private $cartCookieService;
    
    /**
     * CartAPIController constructor.
     *
     * @param  CartApiService     $cartApiService
     * @param  CartCookieService  $cartCookieService
     */
    public function __construct(CartApiService $cartApiService, CartCookieService $cartCookieService)
    {
        $this->cartApiService = $cartApiService;
        $this->cartCookieService = $cartCookieService;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function getCart(Request $request): JsonResponse
    {
        if (Auth::guard('api')->user()) {
            return $this->cartApiService->getCart($request);
        }
        
        return $this->cartCookieService->getCart($request);
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse|Response
     */
    public function addToCart(Request $request)
    {
        if (Auth::guard('api')->user()) {
            return $this->cartApiService->addToCart($request);
        }
        
        return $this->cartCookieService->addToCart($request);
    }
    
    /**
     * @param  Request     $request
     * @param  string|int  $id
     *
     * @return JsonResponse|Response
     */
    public function removeItem(Request $request, string $id): Response
    {
        if (Auth::guard('api')->user()) {
            return $this->cartApiService->removeItem($request, $id);
        }
        
        return $this->cartCookieService->removeItem($request, $id);
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function clearCart(Request $request): JsonResponse
    {
        if (Auth::guard('api')->user()) {
            return $this->cartApiService->clearCart($request);
        }
        
        return $this->cartCookieService->clearCart($request);
    }
    
    /**
     * @param  Request  $request
     * @param  string   $id
     *
     * @return Response|JsonResponse
     */
    public function updateQuantityCartId(Request $request, string $id)
    {
        if (Auth::guard('api')->user()) {
            return $this->cartApiService->updateCountCartItem($request, $id);
        }
        
        return $this->cartCookieService->updateCountCartItem($request, $id);
    }
    
    /**
     * @param  Request  $request
     *
     * @return mixed
     */
    public function applyCoupon(Request $request)
    {
        if (Auth::guard('api')->user()) {
            return $this->cartApiService->applyCoupon($request);
        }
        
        return $this->cartCookieService->applyCoupon($request);
    }
    
    /**
     * @param  Request  $request
     *
     * @return mixed
     */
    public function removeCoupon(Request $request)
    {
        if (Auth::guard('api')->user()) {
            return $this->cartApiService->removeCoupon($request);
        }
        
        return $this->cartCookieService->removeCoupon($request);
    }
}
