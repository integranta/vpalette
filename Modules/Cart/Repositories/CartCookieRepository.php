<?php


namespace Modules\Cart\Repositories;


use Cart;
use Cookie;
use Crypt;
use Exception;
use Illuminate\Http\Response;
use Modules\Cart\Contracts\CartCookieContract;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Class CartCookieRepository
 *
 * @package Modules\Cart\Repositories
 */
class CartCookieRepository implements CartCookieContract
{
    
    /**
     * @inheritDoc
     */
    public function getCartItems(string $cookie): array
    {
        return Crypt::decrypt($cookie, true);
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function addToCartItem(array $params): Response
    {
        $response = new Response(array(
            'error' => false,
            'status' => Response::HTTP_CREATED,
            'message' => __('cart::messages.create-cookie')
        ));
        
        if ($this->decrementAmountOfPartnerOffer($params['offer_id'], $params['qty']) < 0) {
            return new Response(array(
                'error' => true,
                'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => __('cart::messages.cookie.negative_amount')
            ));
        }
        
        if (request()->hasCookie('shopBasket')) {
            $cookieData = Crypt::decrypt(request()->cookie('shopBasket'), true);
            
            $cart = $this->addToCart($params, $cookieData['cart_id']);
            
            $cartResult = $this->getArrCartResult($cookieData['cart_id'], $cart);
            
            $value = $this->getEncryptArrCartResult($cartResult);
            
            
            return $response->withCookie(
                Cookie::forever(
                    'shopBasket',
                    $value,
                    null,
                    null,
                    false,
                    true,
                    false,
                    null
                )
            );
        }
        
        $cart = $this->addToCart($params, $params['cart_id']);
        
        $cartResult = $this->getArrCartResult($params['cart_id'], $cart);
        
        $value = $this->getEncryptArrCartResult($cartResult);
        
        
        return $response->withCookie(Cookie::forever(
            'shopBasket',
            $value,
            null,
            null,
            false,
            true,
            false,
            null
        ));
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function removeCartItem(string $cookie, string $cartItemId): Response
    {
        $response = new Response(array(
            'error' => false,
            'status' => Response::HTTP_OK,
            'message' => __('cart::messages.cookie.success-remove')
        ));
        
        $cookieData = Crypt::decrypt($cookie, true);
        $cartId = $cookieData['cart_id'];
        $cart = Cart::session($cartId);
        
        $offerId = $cookieData['items'][$cartItemId]['attributes']['offer_id'];
        $quantity = $cookieData['items'][$cartItemId]['quantity'];
        
        $this->incrementAmountOfPartnerOffer($offerId, $quantity);
        
        $cartItemIsRemove = Cart::remove($cartItemId);
        
        if ($cartItemIsRemove) {
            
            $cartResult = $this->getArrCartResult($cartId, $cart);
            
            return $response->withCookie(
                Cookie::forever(
                    'shopBasket',
                    $this->getEncryptArrCartResult($cartResult),
                    null,
                    null,
                    false,
                    true,
                    false,
                    null
                )
            );
        }
        
        $response = new Response(array(
            'error' => true,
            'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => __('cart::messages.cookie.error-remove')
        ));
        
        return $response;
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function clearCart(string $cookie): bool
    {
        $cookieData = Crypt::decrypt($cookie, true);
        $cartId = $cookieData['cart_id'];
        
        Cart::session($cartId);
        
        $cartItems = Cart::getContent();
        
        foreach ($cartItems as $key => $cartItem) {
            $this->incrementAmountOfPartnerOffer($cartItem['attributes']['offer_id'], $cartItem->quantity);
        }
        
        return Cart::session($cartId)->clear();
    }
    
    /**
     * @param  array   $params
     * @param  string  $cartId
     *
     * @return \Darryldecode\Cart\Cart
     * @throws Exception
     */
    private function addToCart(array $params, string $cartId): \Darryldecode\Cart\Cart
    {
        return Cart::session($cartId)->add(
            uniqid("{$cartId}_", false),
            $params['product_name'],
            $params['price'],
            $params['qty'],
            $params['attributes']
        );
    }
    
    /**
     * @param  string                        $cartId
     * @param  \Darryldecode\Cart\Cart|Cart  $cart
     *
     * @return array
     */
    private function getArrCartResult(string $cartId, \Darryldecode\Cart\Cart $cart): array
    {
        return [
            'cart_id' => $cartId,
            'priceTotal' => $cart->getTotal(),
            'quantity' => $cart->getTotalQuantity(),
            'items' => $cart->getContent()->each(static function ($item) {
                return $item;
            })
        ];
    }
    
    /**
     * @param  array  $cartResult
     *
     * @return false|string
     */
    private function getEncryptArrCartResult(array $cartResult): string
    {
        return json_encode(Crypt::encrypt($cartResult, true));
    }
    
    /**
     * @param  int  $offerId
     * @param  int  $quantity
     *
     * @return int
     */
    private function decrementAmountOfPartnerOffer(int $offerId, int $quantity): int
    {
        PartnerOffer::find($offerId)->decrement('amount', $quantity);
        return PartnerOffer::find($offerId)->amount;
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function updateCountCartItem(string $cartItemId, int $quantity): Response
    {
        $cookieData = Crypt::decrypt(request()->cookie('shopBasket'));
        $cartId = $cookieData['cart_id'];
        $offerId = $cookieData['items'][$cartItemId]['attributes']['offer_id'];
        $oldQuantity = $cookieData['items'][$cartItemId]['quantity'];
        
        $cartIsUpdate = Cart::session($cartId)->update($cartItemId, array(
            'quantity' => array(
                'relative' => false,
                'value' => $quantity
            )
        ));
        
        if ($cartIsUpdate) {
            $newQuantity = $cookieData['items'][$cartItemId]['quantity'];
            
            if ($oldQuantity < $newQuantity) {
                $diffQuantity = $newQuantity - $oldQuantity;
                if ($this->decrementAmountOfPartnerOffer($offerId, $diffQuantity) < 0) {
                    return new Response(array(
                        'error' => true,
                        'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'message' => __('cart::messages.cookie.negative_amount')
                    ));
                }
            } else {
                $diffQuantity = $oldQuantity - $newQuantity;
                $this->incrementAmountOfPartnerOffer($offerId, $diffQuantity);
            }
            
            $cart = Cart::session($cartId);
            
            $value = $this->getArrCartResult($cartId, $cart);
            
            $response = new Response(array(
                'error' => false,
                'status' => Response::HTTP_OK,
                'message' => __('cart::messages.cookie.success-update')
            ));
            
            return $response->withCookie(
                Cookie::forever(
                    'shopBasket',
                    $this->getEncryptArrCartResult($value),
                    null,
                    null,
                    false,
                    true,
                    false,
                    null
                )
            );
        }
        
        $response = new Response(array(
            'error' => true,
            'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => __('cart::messages.cookie.error-update')
        ));
        
        return $response;
    }
    
    /**
     * @param  int  $offerId
     * @param  int  $quantity
     *
     * @return int
     */
    private function incrementAmountOfPartnerOffer(int $offerId, int $quantity): int
    {
        PartnerOffer::find($offerId)->increment('amount', $quantity);
        return PartnerOffer::find($offerId)->amount;
    }
}
