<?php


namespace Modules\Cart\Repositories;


use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use Carbon\Carbon;
use Cart;
use Darryldecode\Cart\CartCondition;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Cart\Contracts\CartContract;
use Modules\Coupon\Models\Coupon;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Class CartRepository
 *
 * @package Modules\Cart\Repositories
 */
class CartRepository implements CartContract
{
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function addToCart(array $params): ?array
    {
        Cart::session($params['user_id']);
        
        if ($this->decrementAmountOfPartnerOffer($params['offer_id'], $params['qty']) < 0) {
            return null;
        }
        
        Cart::add(
            uniqid("cart_{$params['user_id']}_", false),
            $params['product_name'],
            $params['price'],
            $params['qty'],
            $params['attributes']
        );
        
        return $this->getCart($params['user_id']);
    }
    
    /**
     * @param  int  $offerId
     * @param  int  $quantity
     *
     * @return int
     */
    private function decrementAmountOfPartnerOffer(int $offerId, int $quantity): int
    {
        PartnerOffer::find($offerId)->decrement('amount', $quantity);
        return PartnerOffer::find($offerId)->amount;
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getCart(int $userId): array
    {
        Cart::session($userId);
        return [
            'priceTotal' => Cart::getTotal(),
            'quantity' => Cart::getTotalQuantity(),
            'items' => Cart::getContent()->each(static function ($item) {
                return $item;
            })
        ];
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function updateCountCartItem(array $params, string $cartItemId): ?array
    {
        Cart::session($params['user_id']);
        
        $oldQuantity = (int) Cart::get($cartItemId)->quantity;
        $offerId = Cart::get($cartItemId)->attributes->offer_id;
        
        $cartItemIsSuccessUpdate = Cart::update($cartItemId,
            array(
                'quantity' => array(
                    'value' => $params['qty'],
                    'relative' => false
                )
            ));
        
        if ($cartItemIsSuccessUpdate) {
            $newQuantity = Cart::get($cartItemId)->quantity;
            
            if ($oldQuantity < $newQuantity) {
                $diffQuantity = $newQuantity - $oldQuantity;
                if ($this->decrementAmountOfPartnerOffer($offerId, $diffQuantity) < 0) {
                    return null;
                }
            } else {
                $diffQuantity = $oldQuantity - $newQuantity;
                $this->incrementAmountOfPartnerOffer($offerId, $diffQuantity);
            }
            
            return $this->getCart($params['user_id']);
        }
        
        return null;
    }
    
    /**
     * @param  int  $offerId
     * @param  int  $quantity
     *
     * @return int
     */
    private function incrementAmountOfPartnerOffer(int $offerId, int $quantity): int
    {
        PartnerOffer::find($offerId)->increment('amount', $quantity);
        return PartnerOffer::find($offerId)->amount;
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function removeItem(int $userId, string $id): bool
    {
        Cart::session($userId);
        
        $quantity = (int) Cart::get($id)->quantity;
        $offerId = (int) Cart::get($id)->attributes->offer_id;
        $this->incrementAmountOfPartnerOffer($offerId, $quantity);
        
        return Cart::session($userId)->remove($id);
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function clearCart(int $userId): bool
    {
        Cart::session($userId);
        
        $cartItems = Cart::getContent();
        
        foreach ($cartItems as $key => $cartItem) {
            $this->incrementAmountOfPartnerOffer($cartItem['attributes']['offer_id'], $cartItem->quantity);
        }
        
        return Cart::session($userId)->clear();
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function removeCoupon(array $params): bool
    {
        Cart::session($params['user_id']);
        
        $cartItems = Cart::getContent();
        
        // Нашли дорогой товар в корзине.
        /** @var int $expensivePrice */
        $expensivePrice = $cartItems->max('price');
        $expensiveGoods = $cartItems->filter(static function ($item) use ($expensivePrice) {
            return $item->price === $expensivePrice;
        })->first();
        
        // Очищаем все условия на общую стоимость/предытоговую в корзине.
        Cart::clearCartConditions();
        
        // Удаляем у него применённую ранее скидку.
        $isClear = Cart::clearItemConditions($expensiveGoods->id);
        
        if ($isClear) {
            return true;
        }
        
        return false;
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function applyCoupon(array $params): bool
    {
        Cart::session($params['user_id']);
        
        $cartItems = Cart::getContent();
        $coupon = $this->findCoupon($params['coupon']);
        
        $currentDataTime = Carbon::now(config('cart.current_timezone'))->format(config('cart.datatime_format'));
        
        if ($coupon !== null) {
            $discount = $coupon->discount;
            
            // Если у купона и скидки иcтёк срок использования и период активности у них интервал.
            if ($currentDataTime > $coupon->expires_at
                && $currentDataTime > $discount->expires_at
                && DiscountPeriodActivity::INTERVAL === DiscountPeriodActivity::fromValue($discount->period_activity)
                    ->description
                && CouponPeriodActivity::INTERVAL === CouponPeriodActivity::fromValue($coupon->period_activity)
                    ->description) {
                return false;
            }
            
            // Если купон не принадлежит владельцу.
            if ($coupon->user_id !== null && $coupon->user_id !== (int) $params['user_id']) {
                return false;
            }
            
            // Если у купона кончилось количество использований.
            if ($coupon->uses_user_count < 0) {
                return false;
            }
            
            /**
             * Если тип купона на одну позицию заказа.
             *
             */
            if (CouponTypes::fromValue($coupon->type)->value === CouponTypes::SINGLE_ITEM_ORDER) {
                // Нашли дорогой товар в корзине.
                /** @var int $expensivePrice */
                $expensivePrice = $cartItems->max('price');
                $expensiveGoods = $cartItems->filter(static function ($item) use ($expensivePrice) {
                    return $item->price === $expensivePrice;
                })->first();
                
                // Если скидка типа выражена в процентах.
                if (DiscountAmountTypes::fromValue($discount->amount_type)->value === DiscountAmountTypes::PERCENT) {
                    $couponCondition = new CartCondition(array(
                        'name' => $discount->name,
                        'type' => 'coupon',
                        'value' => $discount->getFormatedDiscountInPercentValue()
                    ));
                    
                    $couponIsAccept = Cart::addItemCondition($expensiveGoods->id, $couponCondition);
                    
                    if ($couponIsAccept) {
                        return true;
                    }
                    
                    return false;
                }
                
                // Если скидка выражается в фиксированной цене (деньгах).
                if (DiscountAmountTypes::fromValue($discount->amount_type)->value === DiscountAmountTypes::FIXED) {
                    $couponCondition = new CartCondition(array(
                        'name' => $discount->name,
                        'type' => 'coupon',
                        'value' => $discount->getFormatedDiscountInMoneyValue()
                    ));
                    
                    $couponIsAccept = Cart::addItemCondition($expensiveGoods->id, $couponCondition);
                    
                    if ($couponIsAccept) {
                        return true;
                    }
                    
                    return false;
                }
                
                // Если скидка должна указать фиксированную стоимость товару.
                if (DiscountAmountTypes::fromValue($discount->amount_type)->value === DiscountAmountTypes::SET_PRICE) {
                    $isUpdate = Cart::update($expensiveGoods->id, array(
                        'price' => $discount->amount
                    ));
                    
                    if ($isUpdate) {
                        return true;
                    }
                    
                    return false;
                }
            }
            
            // Если купон на один заказ или многоразовый.
            if (CouponTypes::fromValue($coupon->type)->value === CouponTypes::SINGLE_ORDER || CouponTypes::REUSABLE) {
                // Если скидка в процентах вычитаем от общей стоимости заказа процент скидки.
                if (DiscountAmountTypes::fromValue($discount->amount_type)->value === DiscountAmountTypes::PERCENT) {
                    $cartCondition = new CartCondition(array(
                        'name' => $discount->name,
                        'type' => 'sale',
                        'target' => 'total',
                        'value' => $discount->getFormatedDiscountInPercentValue(),
                        'order' => 1
                    ));
                    
                    $couponIsAccept = Cart::condition($cartCondition);
                    
                    if ($couponIsAccept) {
                        return true;
                    }
                    
                    return false;
                }
                
                
                // Если скидка в виде фиксированной цены, то устанавливаем новую стоимость на весь заказ.
                if (DiscountAmountTypes::fromValue($discount->type)->value === DiscountAmountTypes::SET_PRICE) {
                    $cartCondition = new CartCondition(array(
                        'name' => $discount->name,
                        'type' => 'sale',
                        'target' => 'total',
                        'value' => $discount->getFormatedDiscountInMoneyValue(),
                        'order' => 1
                    ));
                    
                    $couponIsAccept = Cart::condition($cartCondition);
                    
                    if ($couponIsAccept) {
                        return true;
                    }
                    
                    return false;
                }
            }
        }
        return false;
    }
    
    /**
     * Find coupon by used coupon code.
     *
     * @param  string  $coupon
     *
     * @return Builder|Model|object|null
     */
    private function findCoupon(string $coupon): Model
    {
        return Coupon::with('discount')->where(['code' => $coupon])->first();
    }
}
