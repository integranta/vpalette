<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Cart\Http\Controllers\API\V1\CartAPIController;

Route::prefix('v1')->group(function () {
	Route::post('cart/get', [CartAPIController::class, 'getCart']);
	Route::post('cart/add', [CartAPIController::class, 'addToCart']);
	Route::post('cart/update/{id}', [CartAPIController::class, 'updateQuantityCartId']);
	Route::delete('cart/remove/{id}', [CartAPIController::class, 'removeItem']);
	Route::post('cart/clear', [CartAPIController::class, 'clearCart']);
	Route::post('cart/add/coupon', [CartAPIController::class, 'applyCoupon']);
	Route::post('cart/remove/coupon', [CartAPIController::class, 'removeCoupon']);
});
