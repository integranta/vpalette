<!-- Active field -->
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('advantages::templates.fields.labels.active') }}
    </span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        {!! Form::hidden('active', 0) !!}
        {!!
            Form::checkbox(
                'active',
                true,
                $model->active,
                ['class' => 'custom-control-input', 'id' => 'active']
            )
        !!}
        <label class="custom-control-label ml-lg-3" for="active"></label>
    </div>
</div>

<!-- Name Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'title',
            __('advantages::templates.fields.labels.title'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'title',
                $model->title,
                [
                    'class' => 'form-control',
                    'placeholder' => __('advantages::templates.fields.placeholder.title')
                ]
            )
        !!}
    </div>
</div>

<!-- Sort Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'sort',
            __('advantages::templates.fields.labels.sort'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'sort',
                $model->sort,
                [
                    'class' => 'form-control',
                    'placeholder' => __('advantages::templates.fields.placeholder.sort')
                ]
            )
        !!}
    </div>
</div>

<!-- Text Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'text',
            __('advantages::templates.fields.labels.text'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::textarea(
                'text',
                $model->text,
                [
                    'class' => 'form-control form-control-lg',
                    'placeholder' => __('advantages::templates.fields.placeholder.text')
                ]
            )
        !!}
    </div>
</div>
<!-- Icon Field -->
<div class="form-group row col-md-12 mb-4">
    {!!
        Form::label(
            'icon_name',
            __('advantages::templates.fields.labels.icon_name'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}

    <div class="col-sm-12 col-md-7">
        <select name="icon_name" id="icon_name" class="form-control">
            <option disabled>{{ __('advantages::templates.fields.placeholder.icon_name') }}</option>
            @foreach($types as $key => $label)
                @if($targetIcon === $label)
                    <option value="{{ $key }}" selected>{{ $label }}</option>
                @else
                    <option value="{{ $key }}">{{ $label }}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
