<?php

namespace Modules\Advantages\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Advantages\Models\Advantage;

/**
 * Class AdvantagesPolicy
 *
 * @package Modules\Advantage\Policies
 */
class AdvantagesPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the advantage.
     *
     * @param  User|null   $user
     * @param  Advantage  $advantages
     *
     * @return mixed
     */
    public function view(?User $user, Advantage $advantages)
    {
        if ($advantages->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished advantages')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create advantages.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create advantages')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the advantage.
     *
     * @param  User        $user
     * @param  Advantage  $advantages
     *
     * @return mixed
     */
    public function update(User $user, Advantage $advantages)
    {
        if ($user->can('edit all advantages')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the advantage.
     *
     * @param  User        $user
     * @param  Advantage  $advantages
     *
     * @return mixed
     */
    public function delete(User $user, Advantage $advantages)
    {
        if ($user->can('delete any advantages')) {
            return true;
        }
        
        return false;
    }
}
