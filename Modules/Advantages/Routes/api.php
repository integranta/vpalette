<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Advantages\Http\Controllers\API\V1\AdvantagesAPIController;

Route::prefix('v1')->group(function () {
	Route::apiResource('advantages', AdvantagesAPIController::class);
	Route::get('advantages/get/icon-name', [AdvantagesAPIController::class, 'getIconName']);
	Route::get('advantages/get/icon-types', [AdvantagesAPIController::class, 'getIconTypes']);
});
