<?php

namespace Modules\Advantages\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Advantages\Http\Requests\API\V1\CreateAdvantagesAPIRequest;
use Modules\Advantages\Http\Requests\API\V1\GetIconNameAdvantagesAPIRequest;
use Modules\Advantages\Http\Requests\API\V1\UpdateAdvantagesAPIRequest;
use Modules\Advantages\Services\API\V1\AdvantagesApiService;

/**
 * Class AdvantagesController
 *
 * @package Modules\Advantage\Http\Controllers\API\V1
 */
class AdvantagesAPIController extends Controller
{
    /** @var  AdvantagesApiService */
    private $advantagesApiService;

    /**
     * AdvantagesAPIController constructor.
     *
     * @param  AdvantagesApiService  $service
     */
    public function __construct(AdvantagesApiService $service)
    {
        $this->advantagesApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Получить список всех тизеров-преимуществ.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->advantagesApiService->index($request);
    }

    /**
     * Создать новый тизер-преимущество.
     *
     * @param  CreateAdvantagesAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateAdvantagesAPIRequest $request): JsonResponse
    {
        return $this->advantagesApiService->store($request);
    }


    /**
     * Показать тизер-преимущество по полученному ID.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->advantagesApiService->show($id);
    }

    /**
     * Обновить информацию об тизере-преимуществе по полученному ID.
     *
     * @param  int                         $id
     * @param  UpdateAdvantagesAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateAdvantagesAPIRequest $request): JsonResponse
    {
        return $this->advantagesApiService->update($id, $request);
    }

    /**
     * Удалить тизер-преимуществ по полученному ID.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->advantagesApiService->destroy($id);
    }

    /**
     * Получить список названии иконок.
     *
     * @param  GetIconNameAdvantagesAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function getIconName(GetIconNameAdvantagesAPIRequest $request): JsonResponse
    {
        return $this->advantagesApiService->getIconName($request);
    }

    /**
     * Получить список типов иконок.
     *
     * @return JsonResponse
     */
    public function getIconTypes(): JsonResponse
    {
        return $this->advantagesApiService->getIconTypes();
    }
}
