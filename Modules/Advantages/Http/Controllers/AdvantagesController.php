<?php

namespace Modules\Advantages\Http\Controllers;

use App\DataTables\AdvantagesDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Advantages\Http\Requests\CreateAdvantagesRequest;
use Modules\Advantages\Http\Requests\UpdateAdvantagesRequest;
use Modules\Advantages\Services\AdvantagesWebService;

/**
 * Class AdvantagesController
 *
 * @package Modules\Advantage\Http\Controllers
 */
class AdvantagesController extends Controller
{
    /**
     * @var AdvantagesWebService
     */
    private $advantagesService;


    /**
     * AdvantagesController constructor.
     *
     * @param  AdvantagesWebService  $advantagesService
     */
    public function __construct(AdvantagesWebService $advantagesService)
    {
        $this->advantagesService = $advantagesService;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Показать список из тизеров-преимуществ.
	 *
	 * @param  \App\DataTables\AdvantagesDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(AdvantagesDataTable $dataTable)
    {
        return $this->advantagesService->index($dataTable);
    }

    /**
     * Показать форму для создания нового тизера-преимущества.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create(): View
    {
        return $this->advantagesService->create();
    }

    /**
     * Создать новый тизер-преимущество.
     *
     * @param  CreateAdvantagesRequest  $request
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(CreateAdvantagesRequest $request): RedirectResponse
    {
        return $this->advantagesService->store($request);
    }

    /**
     * Показать выбранный тизер-преимущество.
     *
     * @param  int  $id
     *
     * @return Factory|View|RedirectResponse
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        return $this->advantagesService->show($id);
    }

    /**
     * Показать форму для редактирования тизер-преимущества.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        return $this->advantagesService->edit($id);
    }

    /**
     * Обновить выбранный тизер-преимущство.
     *
     * @param  int                      $id
     * @param  UpdateAdvantagesRequest  $request
     *
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(int $id, UpdateAdvantagesRequest $request): RedirectResponse
    {
        return $this->advantagesService->update($id, $request);
    }

    /**
     * Удалить выбранный тизер-преимущество.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->advantagesService->destroy($id);
    }
}
