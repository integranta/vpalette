<?php

namespace Modules\Advantages\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Advantages\Models\Advantage;

/**
 * Class CreateAdvantagesRequest
 *
 * @package Modules\Advantage\Http\Requests
 */
class CreateAdvantagesRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Advantage::$rules;
    }
}
