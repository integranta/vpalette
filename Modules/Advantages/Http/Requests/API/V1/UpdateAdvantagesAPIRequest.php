<?php

namespace Modules\Advantages\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Advantages\Models\Advantage;

/**
 * Class UpdateAdvantagesAPIRequest
 *
 * @package Modules\Advantage\Http\Requests\API\V1
 */
class UpdateAdvantagesAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Advantage::$rules;
    }
}
