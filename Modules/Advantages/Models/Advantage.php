<?php

namespace Modules\Advantages\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Advantage
 *
 * @package Modules\Advantage\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $title
 * @property string $text
 * @property string $iconName
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @method static Builder|Advantage newModelQuery()
 * @method static Builder|Advantage newQuery()
 * @method static \Illuminate\Database\Query\Builder|Advantage onlyTrashed()
 * @method static Builder|Advantage query()
 * @method static Builder|Advantage whereActive($value)
 * @method static Builder|Advantage whereCreatedAt($value)
 * @method static Builder|Advantage whereDeletedAt($value)
 * @method static Builder|Advantage whereIconName($value)
 * @method static Builder|Advantage whereId($value)
 * @method static Builder|Advantage whereSort($value)
 * @method static Builder|Advantage whereText($value)
 * @method static Builder|Advantage whereTitle($value)
 * @method static Builder|Advantage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Advantage withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Advantage withoutTrashed()
 * @mixin \Eloquent
 */
class Advantage extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'advantages';


    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'title',
        'text',
        'icon_name'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'text' => 'string',
        'icon_name' => 'string',
        'sort' => 'integer',
        'active' => 'boolean'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:150',
        'text' => 'required|string',
        'icon_name' => 'nullable',
        'sort' => 'required|integer',
        'active' => 'required|boolean'
    ];

}
