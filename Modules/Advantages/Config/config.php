<?php

return [
    'name' => 'Advantage',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all advantages as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('ADVANTAGES_CACHE_KEY', 'index_advantages'),
    'cache_ttl' => env('ADVANTAGES_CACHE_TTL', 360000)
];
