<?php


namespace Modules\Advantages\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Advantages\Contracts\AdvantagesContract;
use Modules\Advantages\Http\Requests\API\V1\CreateAdvantagesAPIRequest;
use Modules\Advantages\Http\Requests\API\V1\GetIconNameAdvantagesAPIRequest;
use Modules\Advantages\Http\Requests\API\V1\UpdateAdvantagesAPIRequest;
use Modules\Advantages\Transformers\AdvantagesResource;

/**
 * Class AdvantagesApiService
 *
 * @package Modules\Advantage\Services\API\V1
 */
class AdvantagesApiService extends BaseService
{
    /** @var  AdvantagesContract */
    private $advantagesRepository;

    /**
     * AdvantagesAPIController constructor.
     *
     * @param  AdvantagesContract  $advantagesRepo
     */
    public function __construct(AdvantagesContract $advantagesRepo)
    {
        $this->advantagesRepository = $advantagesRepo;
    }

    /**
     * Обработчик для показа списка из тизеров-преимуществ, через REST API.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $advantages = AdvantagesResource::collection($this->advantagesRepository->listAdvantages());

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('advantages::messages.success_retrieve'),
            $advantages
        );
    }

    /**
     * Обработчик для создания нового тизер-преимущество, через REST API.
     *
     * @param  CreateAdvantagesAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateAdvantagesAPIRequest $request): JsonResponse
    {
        $advantage = $this->advantagesRepository->createAdvantages($request->all());

        if (!$advantage) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('advantages::messages.error_create')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('advantages::messages.success_create'),
            $advantage
        );
    }


    /**
     * Обработчик для показа выбранного тизер-преимущество, через REST API.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $advantage = AdvantagesResource::make($this->advantagesRepository->findAdvantagesById($id));

        if ($advantage === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('advantages::messages.not_found', ['id' => $id])
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('advantages::messages.success_found', ['id' => $id]),
            $advantage
        );
    }

    /**
     * Обработчик для обновления выбранного тизер-преимущество, через REST API.
     *
     * @param  int                         $id
     * @param  UpdateAdvantagesAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateAdvantagesAPIRequest $request): JsonResponse
    {
        $advantage = $this->advantagesRepository->findAdvantagesById($id);

        if ($advantage === null) {
            return $this->responseJson(
                false,
                Response::HTTP_NOT_FOUND,
                __('advantages::messages.not_found', ['id' => $id])
            );
        }

        $advantage = $this->advantagesRepository->updateAdvantages($request->all(), $id);

        if (!$advantage) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('advantages::messages.error_update')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('advantages::messages.success_update'),
            $advantage
        );
    }

    /**
     * Обработчик для удаления выбранный тизер-преимущество, через REST API.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $advantage = $this->advantagesRepository->findAdvantagesById($id);

        if ($advantage === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('advantages::messages.not_found', ['id' => $id])
            );
        }

        $advantage = $this->advantagesRepository->deleteAdvantages($id);

        if (!$advantage) {
            $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('advantages::messages.error_delete')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('advantages::messages.success_delete'),
            $advantage
        );
    }

    /**
     * Обработчик для получения типов иконок, через REST API.
     *
     * @return JsonResponse
     */
    public function getIconTypes(): JsonResponse
    {
        $types = [
            'adv-1' => 'Упаковка',
            'adv-2' => 'Корзина',
            'adv-3' => 'Палец вверх',
            'adv-4' => 'Награда',
        ];

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('advantages::messages.success_icon_types'),
            $types
        );
    }

    /**
     * Обработчик для получения перевода типа иконки, через REST API.
     *
     * @param  GetIconNameAdvantagesAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function getIconName(GetIconNameAdvantagesAPIRequest $request): JsonResponse
    {
        $name = null;

        switch ($request->get('name')) {
            case 'adv-1':
                $name = 'Упаковка';
                break;
            case 'adv-2':
                $name = 'Корзина';
                break;
            case 'adv-3':
                $name = 'Палец вверх';
                break;
            case 'adv-4':
                $name = 'Награда';
                break;
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('advantages::messages.success_icon_name'),
            $name
        );
    }
}
