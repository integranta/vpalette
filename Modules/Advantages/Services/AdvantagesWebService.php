<?php


namespace Modules\Advantages\Services;

use App\DataTables\AdvantagesDataTable;
use App\Services\BaseService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Advantages\Contracts\AdvantagesContract;
use Modules\Advantages\Http\Requests\CreateAdvantagesRequest;
use Modules\Advantages\Http\Requests\UpdateAdvantagesRequest;
use Modules\Advantages\Models\Advantage;

/**
 * Class AdvantagesWebService
 *
 * @package Modules\Advantage\Services
 */
class AdvantagesWebService extends BaseService
{
	/**
	 * @var AdvantagesContract
	 */
	private $advantagesRepository;
	
	
	/**
	 * AdvantagesController constructor.
	 *
	 * @param  AdvantagesContract  $advantagesRepo
	 */
	public function __construct(AdvantagesContract $advantagesRepo)
	{
		$this->advantagesRepository = $advantagesRepo;
	}
	
	/**
	 * Обработчик для показа списка из тизеров-преимуществ.
	 *
	 * @param  \App\DataTables\AdvantagesDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(AdvantagesDataTable $dataTable)
	{
		$this->setPageTitle(
			__('advantages::messages.title'),
			__('advantages::messages.index_subtitle'),
			__('advantages::messages.index_leadtext')
		);
		
		return $dataTable->render('advantages::advantages.index');
	}
	
	/**
	 * Обработчик для показа формы создания нового тизера-преимущества.
	 *
	 * @return View
	 * @throws AuthorizationException
	 */
	public function create(): View
	{
		$this->authorize('create', Advantage::class);
		$types = $this->getTypeIcon();
		
		$this->setPageTitle(
			__('advantages::messages.title'),
			__('advantages::messages.create_subtitle'),
			__('advantages::messages.create_leadtext')
		);
		
		return view('advantages::advantages.create', compact('types'));
	}
	
	/**
	 * Обработчик для получения типов иконок.
	 *
	 * @return array
	 */
	private function getTypeIcon(): array
	{
		return [
			'adv-1' => 'Упаковка',
			'adv-2' => 'Корзина',
			'adv-3' => 'Палец вверх',
			'adv-4' => 'Награда',
		];
	}
	
	/**
	 * Обработчик для создания нового тизер-преимущество.
	 *
	 * @param  CreateAdvantagesRequest  $request
	 *
	 * @return RedirectResponse
	 * @throws AuthorizationException
	 */
	public function store(CreateAdvantagesRequest $request): RedirectResponse
	{
		$this->authorize('create', Advantage::class);
		$advantages = $this->advantagesRepository->createAdvantages($request->all());
		
		if (!$advantages) {
			return $this->responseRedirectBack(
				__('advantages::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.advantages.index',
			__('advantages::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Обработчик для показа формы просмотра выбранного тизер-преимущество.
	 *
	 * @param  int  $id
	 *
	 * @return View|RedirectResponse
	 * @throws AuthorizationException
	 */
	public function show(int $id)
	{
		$model = $this->advantagesRepository->findAdvantagesById($id);
		$this->authorize('view', $model);
		
		if ($model === null) {
			return $this->responseRedirect(
				'admin.advantages.index',
				__('advantages::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$targetIcon = $this->getIconName($model->icon_name);
		$types = $this->getTypeIcon();
		
		$this->setPageTitle(
			__('advantages::messages.title'),
			__('advantages::messages.show_subtitle', ['title' => $model->title]),
			__('advantages::messages.show_leadtext', ['title' => $model->title])
		);
		
		return view('advantages::advantages.show', compact('model', 'targetIcon', 'types'));
	}
	
	/**
	 * Обработчик для получения перевода типа иконки.
	 *
	 * @param  string  $value
	 *
	 * @return string
	 */
	private function getIconName(string $value): string
	{
		
		$name = null;
		
		switch ($value) {
			case 'adv-1':
				$name = 'Упаковка';
				break;
			case 'adv-2':
				$name = 'Корзина';
				break;
			case 'adv-3':
				$name = 'Палец вверх';
				break;
			case 'adv-4':
				$name = 'Награда';
				break;
		}
		
		return $name;
	}
	
	/**
	 * Показать форму для редактирования тизер-преимущества.
	 *
	 * @param  int  $id
	 *
	 * @return View|RedirectResponse
	 * @throws AuthorizationException
	 */
	public function edit(int $id)
	{
		$model = $this->advantagesRepository->findAdvantagesById($id);
		$this->authorize('update', $model);
		
		if (!$model) {
			return $this->responseRedirect(
				'admin.advantages.index',
				__('advantages::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$types = $this->getTypeIcon();
		$targetIcon = null;
		
		$targetIcon = $this->getIconName($model->icon_name);
		
		$this->setPageTitle(
			__('advantages::messages.title'),
			__('advantages::messages.edit_subtitle', ['title' => $model->title]),
			__('advantages::messages.edit_leadtext', ['title' => $model->title])
		);
		return view('advantages::advantages.edit', compact('model', 'targetIcon', 'types'));
	}
	
	/**
	 * Обработчик для обновления выбранного тизер-преимущство.
	 *
	 * @param  int                      $id
	 * @param  UpdateAdvantagesRequest  $request
	 *
	 * @return RedirectResponse
	 * @throws AuthorizationException
	 */
	public function update(int $id, UpdateAdvantagesRequest $request): RedirectResponse
	{
		$this->authorize('update', Advantage::find($id));
		
		$advantages = $this->advantagesRepository->findAdvantagesById($id);
		
		if (!$advantages) {
			return $this->responseRedirect(
				'admin.advantages.index',
				__('advantages::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$advantage = $this->advantagesRepository->updateAdvantages($request->all(), $id);
		
		if (!$advantage) {
			return $this->responseRedirectBack(
				__('advantages::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirectBack(
			__('advantages::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Обработчик для удаления выбранный тизер-преимущество.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws AuthorizationException
	 */
	public function destroy(int $id): RedirectResponse
	{
		$this->authorize('delete', Advantage::find($id));
		
		$advantages = $this->advantagesRepository->findAdvantagesById($id);
		
		if (!$advantages) {
			return $this->responseRedirect(
				'admin.advantages.index',
				__('advantages::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$advantages = $this->advantagesRepository->deleteAdvantages($id);
		
		if (!$advantages) {
			return $this->responseRedirectBack(
				__('advantages::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.advantages.index',
			__('advantages::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
