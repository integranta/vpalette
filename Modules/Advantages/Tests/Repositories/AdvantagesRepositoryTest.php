<?php namespace Modules\Advantages\Tests\Repositories;

use App;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Advantages\Models\Advantage;
use Modules\Advantages\Repositories\AdvantagesRepository;
use Modules\Advantages\Tests\ApiTestTrait;
use Modules\Advantages\Tests\TestCase;

/**
 * Class AdvantagesRepositoryTest
 *
 * @package Modules\Advantage\Tests\Repositories
 */
class AdvantagesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;
    
    /**
     * @var AdvantagesRepository
     */
    protected $advantagesRepo;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->advantagesRepo = App::make(AdvantagesRepository::class);
    }
    
    /**
     * @test create
     */
    public function test_create_advantages()
    {
        $advantages = factory(Advantage::class)->make()->toArray();
        
        $createdAdvantages = $this->advantagesRepo->create($advantages);
        
        $createdAdvantages = $createdAdvantages->toArray();
        $this->assertArrayHasKey('id', $createdAdvantages);
        $this->assertNotNull($createdAdvantages['id'], 'Created Advantage must have id specified');
        $this->assertNotNull(Advantage::find($createdAdvantages['id']), 'Advantage with given id must be in DB');
        $this->assertModelData($advantages, $createdAdvantages);
    }
    
    /**
     * @test read
     */
    public function test_read_advantages()
    {
        $advantages = factory(Advantage::class)->create();
        
        $dbAdvantages = $this->advantagesRepo->find($advantages->id);
        
        $dbAdvantages = $dbAdvantages->toArray();
        $this->assertModelData($advantages->toArray(), $dbAdvantages);
    }
    
    /**
     * @test update
     */
    public function test_update_advantages()
    {
        $advantages = factory(Advantage::class)->create();
        $fakeAdvantages = factory(Advantage::class)->make()->toArray();
        
        $updatedAdvantages = $this->advantagesRepo->update($fakeAdvantages, $advantages->id);
        
        $this->assertModelData($fakeAdvantages, $updatedAdvantages->toArray());
        $dbAdvantages = $this->advantagesRepo->find($advantages->id);
        $this->assertModelData($fakeAdvantages, $dbAdvantages->toArray());
    }
    
    /**
     * @test delete
     */
    public function test_delete_advantages()
    {
        $advantages = factory(Advantage::class)->create();
        
        $resp = $this->advantagesRepo->delete($advantages->id);
        
        $this->assertTrue($resp);
        $this->assertNull(Advantage::find($advantages->id), 'Advantage should not exist in DB');
    }
}
