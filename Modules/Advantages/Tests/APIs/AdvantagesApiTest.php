<?php namespace Modules\Advantages\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Advantages\Models\Advantage;
use Modules\Advantages\Tests\ApiTestTrait;
use Modules\Advantages\Tests\TestCase;

/**
 * Class AdvantagesApiTest
 *
 * @package Modules\Advantage\Tests\APIs
 */
class AdvantagesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_advantages()
    {
        $advantages = factory(Advantage::class)->make()->toArray();
        
        $this->response = $this->json(
            'POST',
            '/api/advantages', $advantages
        );
        
        $this->assertApiResponse($advantages);
    }
    
    /**
     * @test
     */
    public function test_read_advantages()
    {
        $advantages = factory(Advantage::class)->create();
        
        $this->response = $this->json(
            'GET',
            '/api/advantages/'.$advantages->id
        );
        
        $this->assertApiResponse($advantages->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_advantages()
    {
        $advantages = factory(Advantage::class)->create();
        $editedAdvantages = factory(Advantage::class)->make()->toArray();
        
        $this->response = $this->json(
            'PUT',
            '/api/advantages/'.$advantages->id,
            $editedAdvantages
        );
        
        $this->assertApiResponse($editedAdvantages);
    }
    
    /**
     * @test
     */
    public function test_delete_advantages()
    {
        $advantages = factory(Advantage::class)->create();
        
        $this->response = $this->json(
            'DELETE',
            '/api/advantages/'.$advantages->id
        );
        
        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/advantages/'.$advantages->id
        );
        
        $this->response->assertStatus(404);
    }
}
