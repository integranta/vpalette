<?php

namespace Modules\Advantages\Observers;

use App\Traits\Cacheable;
use Modules\Advantages\Models\Advantage;

/**
 * Class AdvantageObserver
 *
 * @package Modules\Advantages\Observers
 */
class AdvantageObserver
{
    use Cacheable;
    
    /**
     * Handle the advantage "created" event.
     *
     * @param  Advantage  $advantage
     *
     * @return void
     */
    public function created(Advantage $advantage): void
    {
        $this->incrementCountItemsInCache($advantage, config('advantages.cache_key'));
    }
    
    /**
     * Handle the advantage "updated" event.
     *
     * @param  Advantage  $advantage
     *
     * @return void
     */
    public function updated(Advantage $advantage): void
    {
        //
    }
    
    /**
     * Handle the advantage "deleted" event.
     *
     * @param  Advantage  $advantage
     *
     * @return void
     */
    public function deleted(Advantage $advantage): void
    {
        $this->decrementCountItemsInCache($advantage, config('advantages.cache_key'));
    }
    
    /**
     * Handle the advantage "restored" event.
     *
     * @param  Advantage  $advantage
     *
     * @return void
     */
    public function restored(Advantage $advantage): void
    {
        $this->incrementCountItemsInCache($advantage, config('advantages.cache_key'));
    }
    
    /**
     * Handle the advantage "force deleted" event.
     *
     * @param  Advantage  $advantage
     *
     * @return void
     */
    public function forceDeleted(Advantage $advantage): void
    {
        $this->decrementCountItemsInCache($advantage, config('advantages.cache_key'));
    }
}
