<?php


namespace Modules\Advantages\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Advantages\Models\Advantage;

/**
 * Interface AdvantagesContract
 *
 * @package Modules\Advantage\Contracts
 */
interface AdvantagesContract
{
    /**
     * Получить список всех тизеров-преимуществ.
     *
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listAdvantages(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection;

    /**
     * Найти тизер-преимущество по ID.
     *
     * @param  int  $id
     *
     * @return Advantage
     */
    public function findAdvantagesById(int $id): ?Advantage;

    /**
     * Создать новый тизер-преимущество.
     *
     * @param  array  $params
     *
     * @return Advantage
     */
    public function createAdvantages(array $params): Advantage;

    /**
     * Обновить тизер-преимущество по указанному ID.
     *
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateAdvantages(array $params, int $id): bool;

    /**
     * Удалить тизер-преимущество по указанному ID.
     *
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteAdvantages(int $id): bool;
}
