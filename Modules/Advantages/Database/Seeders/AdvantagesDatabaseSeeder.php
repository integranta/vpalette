<?php

namespace Modules\Advantages\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Advantages\Models\Advantage;

/**
 * Class AdvantagesDatabaseSeeder
 *
 * @package Modules\Advantage\Database\Seeders
 */
class AdvantagesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Advantage::create([
            'active' => true,
            'title' => 'Доставка по России',
            'text' => 'Вы можете посмотреть все способы доставки в вашем городе',
            'icon_name' => 'adv-1',
            'sort' => 500
        ]);
        
        Advantage::create([
            'active' => true,
            'title' => 'Все в наличии',
            'text' => 'Вы можете посмотреть все способы доставки в вашем городе',
            'icon_name' => 'adv-2',
            'sort' => 500
        ]);
        
        Advantage::create([
            'active' => true,
            'title' => 'Новые поступления',
            'text' => 'Вы можете посмотреть все способы доставки в вашем городе',
            'icon_name' => 'adv-3',
            'sort' => 500
        ]);
        
        Advantage::create([
            'active' => true,
            'title' => 'Все бренды',
            'text' => 'Вы можете посмотреть все способы доставки в вашем городе',
            'icon_name' => 'adv-4',
            'sort' => 500
        ]);
    }
}
