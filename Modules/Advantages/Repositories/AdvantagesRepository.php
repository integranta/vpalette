<?php

namespace Modules\Advantages\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Advantages\Contracts\AdvantagesContract;
use Modules\Advantages\Models\Advantage;
use Str;

/**
 * Class AdvantagesRepository
 *
 * @package Modules\Advantage\Repositories
 * @version October 15, 2019, 4:34 pm MSK
 */
class AdvantagesRepository extends BaseRepository implements AdvantagesContract
{
    /**
     * AdvantagesRepository constructor.
     *
     * @param  Advantage  $model
     */
    public function __construct(Advantage $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listAdvantages(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('advantages.cache_key').$postfix,
            config('advantages.cache_ttl'),
            function () use ($columns, $order, $sort) {
                return $this->all($columns, $order, $sort);
            });
    }
    
    /**
     * @param  int  $id
     *
     * @return Advantage
     */
    public function findAdvantagesById(int $id): ?Advantage
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return Advantage
     */
    public function createAdvantages(array $params): Advantage
    {
        return $this->create($params);
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateAdvantages(array $params, int $id): bool
    {
        return $this->update($params, $id);
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteAdvantages(int $id): bool
    {
        return $this->delete($id);
    }
}
