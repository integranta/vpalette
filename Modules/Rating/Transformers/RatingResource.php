<?php

namespace Modules\Rating\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RatingResource
 *
 * @package Modules\Rating\Transformers
 */
class RatingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rating' => $this->rating,
            'ratingableType' => $this->ratingable_type,
            'ratingableId' => $this->ratinable_id,
            'userId' => $this->author_id,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
