<?php


namespace Modules\Rating\Traits;


use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Rating\Models\Rating;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Ratingable
 *
 * @package Modules\Rating\Traits
 */
trait Ratingable
{
    /**
     * Setting polymorphic relationship with model Rating for ratingable model.
     *
     * @return MorphMany
     */
    public function ratings(): MorphMany
    {
        return $this->morphMany(Rating::class, 'ratingable');
    }
    
    /**
     * Return Average value rating for ratingable model.
     *
     * @return float|null
     */
    public function avgRating(): ?float
    {
        return $this->ratings()->avg('rating');
    }
    
    /**
     * Return total sum rating for ratingable model.
     *
     * @return float|null
     */
    public function sumRating(): ?float
    {
        return $this->ratings()->sum('rating');
    }
    
    /**
     * Return minimal value rating for ratingable model.
     *
     * @return float|null
     */
    public function minRating(): ?float
    {
        return $this->ratings()->min('rating');
    }
    
    /**
     * Return maximum value rating for ratingable model.
     *
     * @return float|null
     */
    public function maxRating(): ?float
    {
        return $this->ratings()->max('rating');
    }
    
    /**
     * Return value rating like percent value for ratingable model.
     *
     * @param  int  $max
     *
     * @return float|int|null
     */
    public function percentRating(int $max = 5)
    {
        $quantity = $this->ratings()->count();
        $total = $this->sumRating();
        
        return $quantity * $max > 0 ? $total / ($quantity * $max / 100) : 0;
    }
    
    /**
     * Add new value for ratingable model.
     *
     * @param              $data
     * @param  Model       $author
     * @param  Model|null  $parent
     *
     * @return mixed
     */
    public function setRating(Model $parent, Model $author, $data)
    {
        return (new Rating())->createRating($parent, $author, $data);
    }
    
    /**
     * Update value rating for ratingable model.
     *
     * @param         $id
     * @param         $data
     * @param  Model| $parent
     *
     * @return mixed
     */
    public function updateRating($id, $data, Model $parent = null)
    {
        return (new Rating())->updateRating($id, $data);
    }
    
    /**
     * Delete value rating for ratingable model.
     *
     * @param $id
     *
     * @return mixed
     */
    public function deleteRating($id)
    {
        return (new Rating())->deleteRating($id);
    }
    
    /**
     * An accessor for get the average value rating for ratingable model.
     *
     * @return float|null
     */
    public function getAvgRatingAttribute(): ?float
    {
        return $this->avgRating();
    }
    
    /**
     * An accessor for get the total sum value rating for ratingable model.
     *
     * @return float|null
     */
    public function getSumRatingAttribute(): ?float
    {
        return $this->sumRating();
    }
    
    /**
     * An accessor for get minimal value rating for ratingable model.
     *
     * @return float|null
     */
    public function getMinRatingAttribute(): ?float
    {
        return $this->minRating();
    }
    
    
    /**
     * An accessor for get maximum value rating for ratingable model.
     *
     * @return float|null
     */
    public function getMaxRatingAttribute(): ?float
    {
        return $this->maxRating();
    }
    
    /**
     * An accessor for get value rating like percent for ratingable model.
     *
     * @return float|int|null
     */
    public function getPercentRatingAttribute()
    {
        return $this->percentRating();
    }
}
