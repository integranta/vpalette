<?php

namespace Modules\Rating\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Rating
 *
 * @package Modules\Rating\Models
 * @property int $id
 * @property int $active
 * @property float $rating
 * @property string $ratingableType
 * @property int $ratingableId
 * @property string $authorType
 * @property int $authorId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $author
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $rateable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Rating\Models\Rating onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereAuthorType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereRatingableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereRatingableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Rating\Models\Rating whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Rating\Models\Rating withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Rating\Models\Rating withoutTrashed()
 * @mixin \Eloquent
 */
class Rating extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'ratings';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'rating',
        'ratingable_id',
        'ratingable_type',
        'author_id',
        'author_type'
    ];

    /**
     * Получить все модели, владеющие reteable.
     *
     * @return MorphTo
     */
    public function rateable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Получить все модели, владеющие author.
     *
     * @return MorphTo
     */
    public function author(): MorphTo
    {
        return $this->morphTo('author');
    }

    /**
     * Создаёт или обновляет существующий рейтинг.
     *
     * @param  Model  $ratingable
     * @param  Model  $author
     * @param  float  $mark
     *
     * @return Model
     */
    public function createRating(Model $ratingable, Model $author, float $mark): Model
    {
        $rating = [
            'active' => true,
            'author_id' => $author->id,
            'author_type' => get_class($author),
            'ratingable_type' => get_class($ratingable),
            'ratingable_id' => $ratingable->id,
            'rating' => $mark
        ];

        return Rating::updateOrCreate($rating);
    }

    /**
     * Обновляет информацию об рейтинге.
     *
     * @param  int    $id
     * @param  array  $data
     *
     * @return bool
     */
    public function updateRating(int $id, array $data): bool
    {
        $rating = static::find($id);
        $rating->update($data);

        return $rating;
    }

    /**
     * Удаляет информацию об рейтинге.
     *
     * @param  int  $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteRating(int $id): bool
    {
        return static::find($id)->delete();
    }
}
