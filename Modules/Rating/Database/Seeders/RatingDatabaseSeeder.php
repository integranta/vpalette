<?php

namespace Modules\Rating\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Rating\Models\Rating;

/**
 * Class RatingDatabaseSeeder
 *
 * @package Modules\Rating\Database\Seeders
 */
class RatingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Rating::class, 10)->make();
    }
}
