<?php


namespace Modules\Import\Repositories;


use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Modules\Import\Contracts\ImportProfileContract;
use Modules\Import\Models\ImportProfile;

/**
 * Class ImportProfileRepository
 *
 * @package Modules\Import\Repositories
 */
class ImportProfileRepository extends BaseRepository implements ImportProfileContract
{
	/**
	 * ImportProfileRepository constructor.
	 *
	 * @param  \Modules\Import\Models\ImportProfile  $model
	 */
	public function __construct(ImportProfile $model)
	{
		parent::__construct($model);
		$this->model = $model;
	}
	
	
	/**
     * @inheritDoc
     */
    public function listImportProfiles(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        return $this->all($columns, $order, $sort);
    }
    
    /**
     * @inheritDoc
     */
    public function findImportProfileById(int $id): ?ImportProfile
    {
        $profile = $this->find($id);
        
        if (!$profile) {
        	return null;
		}
        
        return $profile;
    }
    
    /**
     * @inheritDoc
     */
    public function createImportProfile(array $params): ImportProfile
    {
        return $this->create($params);
    }
    
    /**
     * @inheritDoc
     */
    public function updateImportProfile(array $params, int $id): bool
    {
        return $this->update($params, $id);
    }
	
	/**
	 * @inheritDoc
	 * @throws Exception
	 */
    public function deleteImportProfile($id): bool
    {
        return $this->delete($id);
    }
}