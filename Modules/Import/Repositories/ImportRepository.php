<?php


namespace Modules\Import\Repositories;


use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Modules\Import\Contracts\ImportContract;
use Modules\Import\Models\Import;

/**
 * Class ImportRepository
 *
 * @package Modules\Import\Repositories
 */
class ImportRepository extends BaseRepository implements ImportContract
{
	/**
	 * ImportRepository constructor.
	 *
	 * @param  \Modules\Import\Models\Import  $model
	 */
	public function __construct(Import $model)
	{
		parent::__construct($model);
		$this->model = $model;
	}
	
	
	/**
	 * @param  string          $order
	 * @param  string          $sort
	 * @param  array|string[]  $columns
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function listImports(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
	{
		return $this->all($columns, $order, $sort);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return \Modules\Import\Models\Import|null
	 */
	public function findImportById(int $id): ?Import
	{
		$import = $this->find($id);
		
		if (!$import) {
			return null;
		}
		
		return $import;
	}
	
	/**
	 * @param  array  $params
	 *
	 * @return mixed
	 */
	public function createImport(array $params)
	{
		// TODO: Implement createImport() method.
	}
	
	/**
	 * @param  array  $params
	 * @param  int    $id
	 *
	 * @return bool
	 */
	public function updateImport(array $params, int $id): bool
	{
		// TODO: Implement updateImport() method.
	}
	
	/**
	 * @param $id
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function deleteImport($id): bool
	{
		return $this->delete($id);
}}