const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/import.js')
	.js(__dirname + '/Resources/Assets/js/create.js', 'js/import_create.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/import.css');

if (mix.inProduction()) {
    mix.version();
}
