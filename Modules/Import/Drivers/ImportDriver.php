<?php


namespace Modules\Import\Drivers;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Exception;
use Illuminate\Support\Facades\File;

/**
 * Class ImportDriver
 *
 * @package Modules\Import\Drivers
 */
class ImportDriver
{
	public $path;
	
	/**
	 * ImportDriver constructor.
	 *
	 * @param $path
	 */
	public function __construct($path)
	{
		$this->path = $path;
	}
	
	
	public function start()
	{
		try {
			$extension = File::extension($this->path);
			$reader = ReaderEntityFactory::createReaderFromFile($this->path);
			$reader->open($this->path);
			
			foreach ($reader->getSheetIterator() as $sheet) {
				foreach ($sheet->getRowIterator() as $row) {
					print_r($row->toArray());
				}
			}
			
		} catch (Exception $exception) {}
		
	}
}