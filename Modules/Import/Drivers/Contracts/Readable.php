<?php


namespace Modules\Import\Drivers\Contracts;


/**
 * Interface Readable
 *
 * @package Modules\Import\Drivers\Contracts
 */
interface Readable
{
    public function parse(string $file);
    
    public function hasError();
}