<?php


namespace Modules\Import\Drivers\Readers;


use Modules\Import\Drivers\Contracts\Readable;

/**
 * Class Xlsx
 *
 * Xlsx adapter
 *
 * @package Modules\Import\Drivers\Readers
 */
class Xlsx implements Readable
{
	/** @var \SimpleXLSX */
	public $xlsx;
	
	/**
	 * @param  string  $file
	 *
	 * @return \SimpleXLSX|bool
	 */
	public function parse(string $file)
	{
		return $this->xlsx = \SimpleXLSX::parse($file);
	}
	
	
	/**
	 * @return mixed
	 */
	public function hasError()
	{
		return $this->xlsx::parseError();
	}
}