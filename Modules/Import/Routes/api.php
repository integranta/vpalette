<?php

use Modules\Import\Http\Controllers\API\V1\ImportAPIController;
use Modules\Import\Http\Controllers\API\V1\ImportProfileAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
	Route::apiResource('imports', ImportAPIController::class);
	Route::apiResource('import-profiles', ImportProfileAPIController::class);
});