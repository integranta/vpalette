<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Import\Http\Controllers\ImportController;
use Modules\Import\Http\Controllers\ImportProfileController;

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		Route::get('import', [ImportController::class, 'index'])
			->name('import.index');
		Route::get('import/create', [ImportController::class, 'create'])
			->name('import.create');
		Route::post('import/parse', [ImportController::class, 'parse'])
			->name('import.import_parse');
		Route::post('import/process', [ImportController::class, 'process'])
			->name('import.import_process');
		
		Route::resource('import-profiles', ImportProfileController::class);
	});
