<?php

namespace Modules\Import\Jobs;

use App\Imports\ImportCatalog;
use Illuminate\Bus\Queueable;
use Illuminate\Console\OutputStyle;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $outputStyle;
	
	/**
	 * Create a new job instance.
	 *
	 * @param  \Illuminate\Console\OutputStyle  $outputStyle
	 */
    public function __construct(OutputStyle $outputStyle)
    {
        $this->outputStyle = $outputStyle;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $files = glob(base_path('export_catalog-*.xlsx'));
        
        foreach ($files as $file) {
        	
        	$this->outputStyle->title('Начал импортировать файл '. $file);
        	
			(new ImportCatalog)->withOutput($this->outputStyle)->import($file);
	
			$this->outputStyle->title('Закончил импортировать файл '. $file);
		}
	
		$this->outputStyle->title('Закончил работу');
    }
}
