<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateImportProfilesTable
 */
class CreateImportProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('name');
			$table->json('params');
			$table->unsignedBigInteger('import_id');
			$table->foreign('import_id')
				->on('imports')
				->references('id')
				->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_profiles');
    }
}
