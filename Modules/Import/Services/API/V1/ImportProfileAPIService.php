<?php


namespace Modules\Import\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Import\Contracts\ImportProfileContract;

/**
 * Class ImportProfileAPIService
 *
 * @package Modules\Import\Services\API\V1
 */
class ImportProfileAPIService extends BaseService
{
	/**
	 * @var \Modules\Import\Contracts\ImportProfileContract
	 */
	private $importProfileRepository;
	
	/**
	 * ImportProfileAPIService constructor.
	 *
	 * @param  \Modules\Import\Contracts\ImportProfileContract  $importProfileRepository
	 */
	public function __construct(ImportProfileContract $importProfileRepository)
	{
		$this->importProfileRepository = $importProfileRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 * @return JsonResponse
	 */
	public function index(): ?JsonResponse
	{
		$profiles = $this->importProfileRepository->listImportProfiles();
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('import::api_profiles_messages.success_retrieve'),
			$profiles
		);
	}
	
	/**
	 * Store a newly created resource in storage.
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function store(Request $request): ?JsonResponse
	{
		$importProfile = $this->importProfileRepository->createImportProfile($request->all());
		
		if (!$importProfile) {
			return $this->responseJson(
				true,
				Response::HTTP_BAD_REQUEST,
				__('import::api_profiles_messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('import::api_profiles_messages.success_create'),
			$importProfile
		);
	}
	
	/**
	 * Show the specified resource.
	 * @param int $id
	 * @return JsonResponse
	 */
	public function show($id): ?JsonResponse
	{
		$importProfile = $this->importProfileRepository->findImportProfileById($id);
		
		if (!$importProfile) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('import::api_profiles_messages.not_found')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_FOUND,
			__('import::api_profiles_messages.found'),
			$importProfile
		);
	}
	
	/**
	 * Update the specified resource in storage.
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse
	 */
	public function update(Request $request, $id): ?JsonResponse
	{
		$importProfile = $this->importProfileRepository->findImportProfileById($id);
		
		if (!$importProfile) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('import::api_profiles_messages.not_found')
			);
		}
		
		$importProfile = $this->importProfileRepository->updateImportProfile($request->all(), $id);
		
		if (!$importProfile) {
			return $this->responseJson(
				true,
				Response::HTTP_BAD_REQUEST,
				__('import::api_profiles_messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('import::api_profiles_messages.success_update'),
			$importProfile
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 * @param int $id
	 * @return JsonResponse
	 */
	public function destroy($id): ?JsonResponse
	{
		$importProfile = $this->importProfileRepository->findImportProfileById($id);
		
		if (!$importProfile) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('import::api_profiles_messages.not_found')
			);
		}
		
		$importProfile = $this->importProfileRepository->deleteImportProfile($id);
		
		if (!$importProfile) {
			return $this->responseJson(
				true,
				Response::HTTP_BAD_REQUEST,
				__('import::api_profiles_messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('import::api_profiles_messages.success_delete'),
			$importProfile
		);
	}
}