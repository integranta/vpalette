<?php


namespace Modules\Import\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Import\Contracts\ImportContract;

/**
 * Class ImportAPIService
 *
 * @package Modules\Import\Services\API\V1
 */
class ImportAPIService extends BaseService
{
	/**
	 * @var \Modules\Import\Contracts\ImportContract
	 */
	private $importRepository;
	
	/**
	 * ImportAPIService constructor.
	 *
	 * @param  \Modules\Import\Contracts\ImportContract  $importRepository
	 */
	public function __construct(ImportContract $importRepository)
	{
		$this->importRepository = $importRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
	public function index(): JsonResponse
	{
		$imports = $this->importRepository->listImports();
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('import::api_messages.success_receive'),
			$imports
		);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse
	 */
	public function store(Request $request): JsonResponse
	{
		$import = $this->importRepository->createImport($request->all());
		
		if (!$import) {
			return $this->responseJson(
				true,
				Response::HTTP_BAD_REQUEST,
				__('import::api_messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('import::api_messages.success_create'),
			$import
		);
	}
	
	/**
	 * Show the specified resource.
	 * @param int $id
	 * @return JsonResponse
	 */
	public function show(int $id): JsonResponse
	{
		$import = $this->importRepository->findImportById($id);
		
		if (!$import) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('import::api_messages.not_found')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_FOUND,
			__('import::api_messages.found'),
			$import
		);
	}
	
	/**
	 * Update the specified resource in storage.
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse
	 */
	public function update(Request $request, int $id): ?JsonResponse
	{
		$import = $this->importRepository->findImportById($id);
		
		if (!$import) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('import::api_messages.not_found')
			);
		}
		
		$import = $this->importRepository->updateImport($request->all(), $id);
		
		if (!$import) {
			return $this->responseJson(
				true,
				Response::HTTP_BAD_REQUEST,
				__('import::api_messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('import::api_messages.success_update'),
			$import
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 * @param int $id
	 * @return JsonResponse
	 */
	public function destroy(int $id): ?JsonResponse
	{
		$import = $this->importRepository->findImportById($id);
		
		if (!$import) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('import::api_messages.not_found')
			);
		}
		
		$import = $this->importRepository->deleteImport($id);
		
		if (!$import) {
			return $this->responseJson(
				true,
				Response::HTTP_BAD_REQUEST,
				__('import::api_messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('import::api_messages.success_delete'),
			$import
		);
	}
}