<?php


namespace Modules\Import\Services;


use App\Services\BaseService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Modules\Import\Contracts\ImportProfileContract;
use function __;

/**
 * Class ImportProfileWebService
 *
 * @package Modules\Import\Services
 */
class ImportProfileWebService extends BaseService
{
	/**
	 * @var \Modules\Import\Contracts\ImportProfileContract
	 */
	private $importProfileRepository;
	
	/**
	 * ImportProfileWebService constructor.
	 *
	 * @param  \Modules\Import\Contracts\ImportProfileContract  $importProfileRepository
	 */
	public function __construct(ImportProfileContract $importProfileRepository)
	{
		$this->importProfileRepository = $importProfileRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$profiles = $this->importProfileRepository->listImportProfiles();
		return view('import::index', compact('profiles'));
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		return view('import::create');
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request): RedirectResponse
	{
		dd(__METHOD__, $request->all());
		
		$profile = $this->importProfileRepository->createImportProfile($request->all());
		
		if (!$profile) {
			return $this->responseRedirectBack(
				__('import::profiles_messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		//return $this->responseRedirect();
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show($id)
	{
		return view('import::show');
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id)
	{
		return view('import::edit');
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id): RedirectResponse
	{
		$profile = $this->importProfileRepository->findImportProfileById($id);
		
		if (!$profile) {
			return $this->responseRedirectBack(
				__('import::profiles_messages.not_found'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$profile = $this->importProfileRepository->updateImportProfile($request->all(), $id);
		
		if (!$profile) {
			return $this->responseRedirectBack(
				__('import::profiles_messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirectBack(
			__('import::profiles_messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id): RedirectResponse
	{
		$profile = $this->importProfileRepository->findImportProfileById($id);
		
		if (!$profile) {
			return $this->responseRedirectBack(
				__('import::profiles_messages.not_found'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$profile = $this->importProfileRepository->deleteImportProfile($id);
		
		if (!$profile) {
			return $this->responseRedirectBack(
				__('import::profiles_messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.import.create',
			__('import::profiles_messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}