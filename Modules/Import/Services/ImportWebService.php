<?php


namespace Modules\Import\Services;


use App\Services\BaseService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;
use Modules\Import\Contracts\ImportContract;
use Modules\Import\Contracts\ImportProfileContract;
use Modules\Import\Drivers\ImportDriver;
use Modules\Import\Http\Requests\ParseUploadedFileRequest;
use Modules\Import\Http\Requests\ProcessUploadedFileRequest;
use Modules\Import\Http\Requests\UploadFileToImportRequest;
use Str;

/**
 * Class ImportWebService
 *
 * @package Modules\Import\Services
 */
class ImportWebService extends BaseService
{
	/**
	 * @var \Modules\Import\Contracts\ImportContract
	 */
	private $repository;
	
	
	/**
	 * ImportWebService constructor.
	 *
	 * @param  \Modules\Import\Contracts\ImportContract  $repository
	 */
	public function __construct(ImportContract $repository)
	{
		$this->repository = $repository;
	}
	
	public function index(): View
	{
		$imports = $this->repository->listImports();
		
		$this->setPageTitle(
			__('import::messages.title'),
			__('import::messages.index_subtitle'),
			__('import::messages.index_leadtext')
		);
		
		return view('import::import_catalog.index', compact('imports'));
	}
	
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @param  \Illuminate\Http\Request                         $request
	 * @param  \Modules\Import\Contracts\ImportProfileContract  $importProfileRepository
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(Request $request, ImportProfileContract $importProfileRepository): View
	{
		$profiles = $importProfileRepository->listImportProfiles();
		
		$this->setPageTitle(
			__('import::messages.title'),
			__('import::messages.create_subtitle'),
			__('import::messages.create_leadtext')
		);
		return view('import::import_catalog.create', compact('profiles'));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Modules\Import\Http\Requests\UploadFileToImportRequest  $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(UploadFileToImportRequest $request): RedirectResponse
	{
		$uploadedFile = $request->file('file');
		
		$fileName = Str::of($uploadedFile->getClientOriginalName())
			->trim()
			->beforeLast('.')
			->append(Str::random(), '_', date('y-m-d-H-i-s'), '.')
			->finish($uploadedFile->getClientOriginalExtension());
		
		$path = resource_path('pending-files/'.$fileName);
		File::copy($uploadedFile, $path);
		
		$import = new ImportDriver($path);
		$import->start();
		
		session()->flash('status', __('import::messages.success'));
		
		return back();
	}
	
	public function parse(ParseUploadedFileRequest $request)
	{
	
	}
	
	public function process(ProcessUploadedFileRequest $request)
	{
	
	}
}