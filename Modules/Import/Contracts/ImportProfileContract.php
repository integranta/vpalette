<?php


namespace Modules\Import\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Import\Models\ImportProfile;

/**
 * Interface ImportProfileContract
 *
 * @package Modules\Import\Contracts
 */
interface ImportProfileContract
{
	/**
	 * Получить список профилей импорта
	 *
	 * @param  string  $order
	 * @param  string  $sort
	 * @param  array   $columns
	 *
	 * @return Collection
	 */
	public function listImportProfiles(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
	
	/**
	 * Найти профиль импорта по указанному ID
	 *
	 * @param  int  $id
	 *
	 * @return ImportProfile|null
	 */
	public function findImportProfileById(int $id): ?ImportProfile;
	
	/**
	 * Создать новый профиль
	 *
	 * @param  array  $params
	 *
	 * @return ImportProfile
	 */
	public function createImportProfile(array $params): ImportProfile;
	
	/**
	 * Обновить существующий профиль по указанному ID
	 *
	 * @param  array  $params
	 * @param  int    $id
	 *
	 * @return bool
	 */
	public function updateImportProfile(array $params, int $id): bool;
	
	/**
	 * Удалить профиль по указанному ID
	 *
	 * @param $id
	 *
	 * @return bool
	 */
	public function deleteImportProfile($id): bool;
}