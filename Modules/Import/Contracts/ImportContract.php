<?php


namespace Modules\Import\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Import\Models\Import;

/**
 * Interface ImportContract
 *
 * @package Modules\Import\Contracts
 */
interface ImportContract
{
	/**
	 * @param  string  $order
	 * @param  string  $sort
	 * @param  array   $columns
	 *
	 * @return Collection
	 */
	public function listImports(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
	
	/**
	 * @param  int  $id
	 *
	 * @return Import|null
	 */
	public function findImportById(int $id): ?Import;
	
	/**
	 * @param  array  $params
	 *
	 * @return Import
	 */
	public function createImport(array $params);
	
	/**
	 * @param  array  $params
	 * @param  int    $id
	 *
	 * @return bool
	 */
	public function updateImport(array $params, int $id): bool;
	
	/**
	 * @param $id
	 *
	 * @return bool
	 */
	public function deleteImport($id): bool;
}