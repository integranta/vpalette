<?php

namespace Modules\Import\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class ImportProfile
 *
 * @package Modules\Import\Models
 * @property int $id
 * @property string $name
 * @property object $params
 * @property int $importId
 * @property-read \Modules\Import\Models\Import $imports
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\ImportProfile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\ImportProfile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\ImportProfile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\ImportProfile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\ImportProfile whereImportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\ImportProfile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\ImportProfile whereParams($value)
 * @mixin \Eloquent
 */
class ImportProfile extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
	/**
	 * Связанная с моделью таблица.
	 *
	 * @var string
	 */
	protected $table = 'import_profiles';
	
	/**
	 * Определяет необходимость отметок времени для модели.
	 *
	 * @var bool
	 */
	public $timestamps = false;
	
	/**
	 * Атрибуты, для которых разрешено массовое назначение.
	 *
	 * @var array
	 */
    protected $fillable = [
    	'name',
		'params',
		'import_id'
	];
	
	/**
	 * Атрибуты, которые нужно преобразовать в нативный тип.
	 *
	 * @var array
	 */
	protected $casts = [
		'name' => 'string',
		'params' => 'object',
		'import_id' => 'int'
	];
 
	/**
	 * Получить импорты с этим профилем.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function imports(): BelongsTo
	{
		return $this->belongsTo(Import::class);
	}
}
