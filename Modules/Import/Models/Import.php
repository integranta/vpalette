<?php

namespace Modules\Import\Models;

use App\Enums\ImportStatus;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Import
 *
 * @package Modules\Import\Models
 * @property int $id
 * @property int $userId
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $importStartAt
 * @property \Illuminate\Support\Carbon|null $importFinishAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Import\Models\ImportProfile[] $profiles
 * @property-read int|null $profilesCount
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import whereImportFinishAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import whereImportStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Import\Models\Import whereUserId($value)
 * @mixin \Eloquent
 */
class Import extends Model
{
	use CastsEnums;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
	/**
	 * Связанная с моделью таблица.
	 *
	 * @var string
	 */
	protected $table = 'imports';
	
	/**
	 * Определяет необходимость отметок времени для модели.
	 *
	 * @var bool
	 */
	public $timestamps = false;
	
	/**
	 * Формат хранения отметок времени модели.
	 *
	 * @var string
	 */
	protected $dateFormat = 'Y-m-d H:i:s';
	
	/**
	 * Атрибуты, которые должны быть преобразованы в даты.
	 *
	 * @var array
	 */
	protected $dates = [
		'import_start_at',
		'import_finish_at'
	];
	
	/**
	 * Атрибуты, которые нужно связать с enum классом.
	 *
	 * @var array
	 */
	protected $enumCasts = [
		'status' => ImportStatus::class
	];
	
	/**
	 * Атрибуты, для которых разрешено массовое назначение.
	 *
	 * @var array
	 */
    protected $fillable = [
		'status',
		'imported_at',
		'user_id',
		'import_start_at',
		'import_finish_at'
	];
	
	/**
	 * Атрибуты, которые нужно преобразовать в нативный тип.
	 *
	 * @var array
	 */
	protected $casts = [
		'status' => 'int',
		'user_id' => 'int'
	];
	
	/**
	 * Получить профили импортов
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function profiles(): HasMany
	{
		return $this->hasMany(ImportProfile::class);
	}
}
