<?php
return [
	'table' => [
		'id' => 'ID',
		'status' => 'Статус',
		'user_id' => 'Импортировал',
		'actions' => [
			'title' => 'Действия'
		]
	],
	'create' => [
		'title_steps' => [
			'first' => 'Выбор профиля',
			'second' => 'Загрузка файла',
			'three' => 'Настройка выгрузки',
			'four' => 'Результат',
		]
	]
];