<div class="form-group row col-md-12 mb-4">
	<label
		class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
		for="name">
		Выберите профиль:
	</label>
	<div class="col-sm-12 col-md-7">
		<select
			id="name"
			class="form-control"
			name="name"
			required
		>
			<option value="0">
				Не выбран
			</option>
			@if (count($profiles) > 0)
				@foreach($profiles as $profile)
					<option value={{ $profile->id }}> {{ $profile->name }}</option>
				@endforeach
			@endif
			<option value="new">Создать новый</option>
		</select>
	</div>
</div>


