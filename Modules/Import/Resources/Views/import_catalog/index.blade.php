@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('libs/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/import.css') }}">
@endsection
@section('content')
    <div class="row">
        @include('admin.partials.flash')
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $subTitle }}</h4>
                    <div class="card-header-action">
                        <a
                            class="btn btn-primary"
                            href="{!! route('admin.import.create') !!}"
                        ><span class="text-white">Добавить <i class="text-white fas fa-plus"></i></span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    @include('import::import_catalog.table')
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('libs/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('libs/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('libs/js/select.bootstrap4.min.js') }}"></script>
    <script>
        $(function () {
            $('#importcatalog-table').DataTable({
                "language": {
                    "url": "/assets/configs/DataTables/Lang/ru/Russian.json"
                }
            });
        });
    </script>
@endpush
