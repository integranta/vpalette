<div class="table-responsive">
    <table class="table" id="importcatalog-table">
        <thead>
        <tr>
            <th>{{ __('import::templates.table.id') }}</th>
            <th>{{ __('import::templates.table.status') }}</th>
            <th>{{ __('import::templates.table.user_id') }}</th>
            <th style="width:100px; min-width:100px;" class="text-center">
				{{ __('import::templates.table.actions.title') }}
			</th>
        </tr>
        </thead>
        <tbody>
        @foreach($imports as $import)
            <tr>
                <td>{{ $import->id }}</td>
                <td>{{ $import->status }}</td>
                <td>{{ $import->imported_at }}</td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="CRUD операции">
                        {!! Form::open(['route' => ['admin.import.destroy', $import->id], 'method' => 'delete']) !!}
                        <a
                            href="{{ route('admin.import.edit', $import->id) }}"
                            class="btn btn-sm btn-primary">
                            <i class="fa fa-edit"></i>
                        </a>
                        {!! Form::button(
                            '<i class="fa fa-trash"></i>',
                            [
                                'type' => 'submit',
                                'class' => 'btn btn-sm btn-danger',
                                'title' => 'Удалить',
                                'onclick' => "return confirm('Вы уверены?')"
                            ])
                        !!}
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
