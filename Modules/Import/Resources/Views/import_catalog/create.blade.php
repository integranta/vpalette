@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/import.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-sm-6 col-lg-12">
            @include('admin.partials.flash')
            <div class="card">
                <div class="card-header">
                    <h4>{{ $subTitle }}</h4>
                </div>
                <div class="card-body">
					@include('import::includes.steps')

                    <!-- Form -->
                    <form
                        method="POST"
                        action="{{ route('admin.import-profiles.store') }}"
                        enctype="multipart/form-data"
                        class="wizard-content mt-2"
                    >
                        @csrf
						@include('import::import_catalog.select_profile')
                        <div class="card-footer">
                            <!-- Submit Field -->
                            <div class="form-group row col-mb-12">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-2"></label>
                                <div class="col-sm-4 col-md-4">
                                    <button class="btn btn-danger">Отмена</button>
                                </div>
                                <div class="col-sm-4 col-md-4 text-right">

                                    <button
                                        type="submit"
                                        class="btn btn-icon icon-right btn-primary">
                                        Далее <i class="fas fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Close Form -->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ asset('js/import_create.js') }}"></script>
@endspush
