<div class="form-group row col-md-12 mb-4">
	<label
		class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
		for="name">
		Выберите профиль:
	</label>
	<div class="col-sm-12 col-md-7">
		<select
			id="name"
			class="form-control"
			name="name"
			required
		>
			<option value="0">
				Не выбран
			</option>
			@foreach($profiles as $profile)
				<option value="{{ $profile->id }}"> {{ $profile->name }} </option>
			@endforeach
			<option value="new">Создать новый</option>
		</select>
	</div>
</div>


<div class="form-group row col-md-12">
	<label
		class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
		for="csv_file">Импортируемый файл (csv, xls, xlsx):</label>
	<div class="col-sm-12 col-md-7">
		<input type="file" name="csv_file" id="csv_file" class="form-control" required>
	</div>
</div>

<div class="form-group row col-md-12">
	<span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Файл содержит строку с заголовками:</span>
	<div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
		<input type="checkbox" name="header" id="header" class="custom-control-input" checked>
		<label class="custom-control-label ml-lg-3" for="header"></label>
	</div>
</div>

<!-- Start owner (partner_id) field -->
{{--<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="partner_shop_id">
        От какого магазина будет производиться импорт:
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="partner_shop_id"
            class="form-control"
            name="partner_shop_id"
            required
        >
            <option value="0" disabled>
                Выберите из списка свой магазин
            </option>
            @foreach($owners as $owner)
                @foreach($owner->partnerShops as $shop)
                    <option value="{{ $shop->id }}"> {{ $shop->shop_name }} </option>
                @endforeach
            @endforeach
        </select>
    </div>
</div>--}}
<!-- End owner (partner_id) field -->

<!-- File separator field -->
<div class="form-group row col-md-12">
	<label
		class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
		for="csv_file_delimiter">Разделитель в файле:</label>
	<div class="col-sm-12 col-md-7">
		<input
			type="text"
			name="csv_file_delimiter"
			id="csv_file_delimiter"
			class="form-control"
			required
		>
	</div>
</div>
<!-- End file separator field -->
