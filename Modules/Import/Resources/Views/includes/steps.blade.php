<!-- Step block -->
<div class="row mt-4">
	<div class="col-12 col-lg-8 offset-lg-2">
		<div class="wizard-steps">
			<div
				class="wizard-step wizard-step-active"
			>
				<div class="wizard-step-icon">
					<i class="fas fa-user-cog"></i>
				</div>
				<div class="wizard-step-label">
					{{ __('import::templates.create.title_steps.first') }}
				</div>
			</div>
			<div
				class="wizard-step wizard-step"
			>
				<div class="wizard-step-icon">
					<i class="fas fa-file-upload"></i>
				</div>
				<div class="wizard-step-label">
					{{ __('import::templates.create.title_steps.second') }}
				</div>
			</div>

			<div
				class="wizard-step"
			>
				<div class="wizard-step-icon">
					<i class="fas fa-cog"></i>
				</div>
				<div class="wizard-step-label">
					{{ __('import::templates.create.title_steps.three') }}
				</div>
			</div>

			<div
				class="wizard-step"
			>
				<div class="wizard-step-icon">
					<i class="fas fa-file-import"></i>
				</div>
				<div class="wizard-step-label">
					{{ __('import::templates.create.title_steps.four') }}
				</div>
			</div>

		</div>
	</div>
</div>
<!-- Close Step block -->