<?php

namespace Modules\Import\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Import\Services\ImportProfileWebService;

/**
 * Class ImportProfileController
 *
 * @package Modules\Import\Http\Controllers
 */
class ImportProfileController extends Controller
{
	/**
	 * @var \Modules\Import\Services\ImportProfileWebService
	 */
	private $service;
	
	/**
	 * ImportProfileController constructor.
	 *
	 * @param  \Modules\Import\Services\ImportProfileWebService  $service
	 */
	public function __construct(ImportProfileWebService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		return $this->service->index();
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		return $this->service->create();
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request)
	{
		return $this->service->store($request);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show($id)
	{
		return $this->service->show($id);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id)
	{
		return $this->service->edit($id);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request  $request
	 * @param  int      $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, $id)
	{
		return $this->service->update($request, $id);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		return $this->service->destroy($id);
	}
}
