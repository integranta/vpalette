<?php

namespace Modules\Import\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Import\Services\API\V1\ImportAPIService;

/**
 * Class ImportAPIController
 *
 * @package Modules\Import\Http\Controllers\API\V1
 */
class ImportAPIController extends Controller
{
	/**
	 * @var \Modules\Import\Services\API\V1\ImportAPIService
	 */
	private $importAPIService;
	
	/**
	 * ImportAPIController constructor.
	 *
	 * @param  \Modules\Import\Services\API\V1\ImportAPIService  $importAPIService
	 */
	public function __construct(ImportAPIService $importAPIService)
	{
		$this->importAPIService = $importAPIService;
	}
	
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function index(): JsonResponse
	{
        return $this->importAPIService->index();
	}
	
	/**
	 * Store a newly created resource in storage.
	 * @param  Request  $request
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function store(Request $request): JsonResponse
	{
        return $this->importAPIService->store($request);
	}
	
	/**
	 * Show the specified resource.
	 * @param  int  $id
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function show($id): JsonResponse
	{
        return $this->importAPIService->show($id);
	}
	
	/**
	 * Update the specified resource in storage.
	 * @param  Request  $request
	 * @param  int      $id
	 * @return \Illuminate\Http\JsonResponse|null
	 */
    public function update(Request $request, $id): ?JsonResponse
	{
        return $this->importAPIService->update($request, $id);
	}
	
	/**
	 * Remove the specified resource from storage.
	 * @param  int  $id
	 * @return \Illuminate\Http\JsonResponse|null
	 */
    public function destroy($id): ?JsonResponse
	{
        return $this->importAPIService->destroy($id);
    }
}
