<?php

namespace Modules\Import\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Import\Services\API\V1\ImportProfileAPIService;

/**
 * Class ImportProfileAPIController
 *
 * @package Modules\Import\Http\Controllers\API\V1
 */
class ImportProfileAPIController extends Controller
{
	/**
	 * @var \Modules\Import\Services\API\V1\ImportProfileAPIService
	 */
	private $importProfileAPIService;
	
	/**
	 * ImportProfileAPIController constructor.
	 *
	 * @param  \Modules\Import\Services\API\V1\ImportProfileAPIService  $importProfileAPIService
	 */
	public function __construct(ImportProfileAPIService $importProfileAPIService)
	{
		$this->importProfileAPIService = $importProfileAPIService;
	}
	
	
	/**
     * Display a listing of the resource.
     * @return JsonResponse
     */
    public function index(): JsonResponse
	{
        return $this->importProfileAPIService->index();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
	{
        return $this->importProfileAPIService->store($request);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
	{
        return $this->importProfileAPIService->show($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
	{
        return $this->importProfileAPIService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
	{
        return $this->importProfileAPIService->destroy($id);
    }
}
