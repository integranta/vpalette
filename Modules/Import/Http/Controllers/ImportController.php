<?php

namespace Modules\Import\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Modules\Import\Contracts\ImportProfileContract;
use Modules\Import\Http\Requests\ParseUploadedFileRequest;
use Modules\Import\Http\Requests\ProcessUploadedFileRequest;
use Modules\Import\Http\Requests\UploadFileToImportRequest;
use Modules\Import\Services\ImportWebService;

/**
 * Class ImportController
 *
 * @package Modules\Import\Http\Controllers
 */
class ImportController extends Controller
{
	/**
	 * @var \Modules\Import\Services\ImportWebService
	 */
	private $service;
	
	/**
	 * ImportController constructor.
	 *
	 * @param  \Modules\Import\Services\ImportWebService  $service
	 */
	public function __construct(ImportWebService $service)
	{
		$this->service = $service;
	}
	
	/**
	 * @return \Illuminate\View\View
	 */
	public function index(): View
	{
		return $this->service->index();
	}
	
	/**
	 * @param  \Illuminate\Http\Request                         $request
	 * @param  \Modules\Import\Contracts\ImportProfileContract  $importProfileRepository
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(Request $request, ImportProfileContract $importProfileRepository): View
	{
		return $this->service->create($request, $importProfileRepository);
	}
	
	public function parse(ParseUploadedFileRequest $request)
	{
		return $this->service->parse($request);
	}
	
	public function process(ProcessUploadedFileRequest $request)
	{
		return $this->service->process($request);
	}
}
