<?php

namespace Modules\Delivery\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Delivery
 *
 * @package Modules\Delivery\Models
 * @property int $id
 * @property int $deliveryPeriodMax
 * @property int $deliveryPeriodMin
 * @property float $deliveryTotalPrice
 * @property string $deliveryCdekPrintReceipts
 * @property string $deliveryPochtaRussianF7Form
 * @property string $deliveryPochtaRussianF112Form
 * @property string $deliveryPochtaRussianPrintReceipts
 * @property string $deliveryCdekPrintLabels
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Delivery\Models\Delivery onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryCdekPrintLabels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryCdekPrintReceipts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryPeriodMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryPeriodMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryPochtaRussianF112Form($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryPochtaRussianF7Form($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryPochtaRussianPrintReceipts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereDeliveryTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Delivery\Models\Delivery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Delivery\Models\Delivery withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Delivery\Models\Delivery withoutTrashed()
 * @mixin \Eloquent
 */
class Delivery extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'deliveries';


    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'delivery_period_max',
        'delivery_period_min',
        'delivery_total_price',
        'delivery_cdek_print_receipts',
        'delivery_pochta_russian_f7_form',
        'delivery_pochta_russian_f112_form',
        'delivery_pochta_russian_print_receipts',
        'delivery_cdek_print_labels'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'delivery_total_price' => 'double',
        'delivery_cdek_print_receipts' => 'string',
        'delivery_pochta_russian_f7_form' => 'string',
        'delivery_pochta_russian_f112_form' => 'string',
        'delivery_pochta_russian_print_receipts' => 'string',
        'delivery_cdek_print_labels' => 'string'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'delivery_period_max' => 'required|integer',
        'delivery_period_min' => 'requried|integer',
        'delivery_total_price' => 'required|integer',
        'delivery_cdek_print_receipts' => 'nullable|string|mimes:pdf',
        'delivery_pochta_russian_f7_form' => 'nullable|string|mimes:pdf',
        'delivery_pochta_russian_f112_form' => 'nullable|string|mimes:pdf',
        'delivery_pochta_russian_print_receipts' => 'required|string|mimes:pdf',
        'delivery_cdek_print_labels' => 'nullable|string|mimes:pdf'
    ];


}
