<?php


namespace Modules\Delivery\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Delivery\Models\Delivery;

/**
 * Interface DeliveryContract
 *
 * @package Modules\Delivery\Contracts
 */
interface DeliveryContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listDeliveries(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Delivery
     */
    public function findDeliveryById(int $id): ?Delivery;
    
    /**
     * @param  array  $params
     *
     * @return Delivery
     */
    public function createDelivery(array $params): Delivery;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return mixed
     */
    public function updateDelivery(array $params, int $id): Delivery;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteDelivery(int $id): bool;
}
