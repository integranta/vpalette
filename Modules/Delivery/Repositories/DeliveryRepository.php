<?php

namespace Modules\Delivery\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Modules\Delivery\Contracts\DeliveryContract;
use Modules\Delivery\Models\Delivery;

/**
 * Class DeliveryRepository
 *
 * @package Modules\Delivery\Repositories
 * @version September 18, 2019, 5:00 pm MSK
 */
class DeliveryRepository extends BaseRepository implements DeliveryContract
{
    /**
     * DeliveryRepository constructor.
     *
     * @param  Delivery  $model
     */
    public function __construct(Delivery $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listDeliveries(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        return $this->all($columns, $order, $sort);
    }
    
    /**
     * @inheritDoc
     */
    public function findDeliveryById(int $id): ?Delivery
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function createDelivery(array $params): Delivery
    {
        return $this->create($params);
    }
    
    /**
     * @inheritDoc
     */
    public function updateDelivery(array $params, int $id): Delivery
    {
        return $this->update($params, $id);
    }
    
    /**
     * @inheritDoc
     */
    public function deleteDelivery(int $id): bool
    {
        return $this->delete($id);
    }
}
