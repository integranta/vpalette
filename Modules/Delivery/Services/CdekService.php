<?php


namespace Modules\Delivery\Services;

use App\User;
use CdekSDK\CdekClient;
use CdekSDK\Common;
use CdekSDK\Common\Location;
use CdekSDK\Common\Order;
use CdekSDK\Requests;
use App\Enums\DeliveriesTariffsCdek;
use Log;
use Modules\Order\Traits\GetInfoAboutCityFromCdek;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Обработчик регистрации заказа от Интернет-магазина для СДЭК.
 *
 * Class CdekService
 *
 * @package Modules\Delivery\Services
 */
class CdekService
{
	use GetInfoAboutCityFromCdek;
	
	/** @var \CdekSDK\Common\Order */
	private $order;
	
	/** @var \CdekSDK\CdekClient */
	private $cdekClient;
	
	/** @var array */
	private $orderData;
	
	/** @var \CdekSDK\Common\Location */
	private $senderAddress;
	
	/** @var \Modules\PartnerShop\Models\PartnerShop */
	private $partnerShop;
	
	/** @var array */
	private $cart;
	
	/** @var \App\User */
	private $recipient;
	
	/**
	 * CdekService constructor.
	 *
	 * @param  CdekClient                               $cdekClient
	 * @param  array                                    $orderData
	 * @param  \CdekSDK\Common\Location                 $senderAddress
	 * @param  \Modules\PartnerShop\Models\PartnerShop  $partnerShop
	 * @param  \App\User                                $recipient
	 * @param  array                                    $cart
	 */
	public function __construct(
		CdekClient $cdekClient,
		array $orderData,
		Location $senderAddress,
		PartnerShop $partnerShop,
		User $recipient,
		array $cart
	) {
		$this->cdekClient = $cdekClient;
		$this->orderData = $orderData;
		$this->senderAddress = $senderAddress;
		$this->partnerShop = $partnerShop;
		$this->recipient = $recipient;
		$this->cart = $cart;
		
		$this->makeOrder();
	}
	
	private function makeOrder(): void
	{
		
		$orderCDEK = new Order([
			'Number'           => $this->orderData['order_number'],
			'SendCityPostCode' => $this->partnerShop->postIndex,
			'SendCityCode'     => $this->senderAddress->getCityCode(),
			'RecCityPostCode'  => $this->orderData['post_code'],
			'RecipientName'    => "{$this->recipient->firstName} {$this->recipient->lastName}",
			'RecipientEmail'   => $this->recipient->email,
			'Phone'            => $this->recipient->phone,
			'TariffTypeCode'   => DeliveriesTariffsCdek::EXPRESS_LITE_DOOR_DOOR,
			'Comment'          => $this->orderData['comments']
		]);
		
		$orderCDEK->setSender(Common\Sender::create([
			'Company' => $this->partnerShop->shopName,
			'Name'    => $this->partnerShop->partner->user->full_name,
			'Phone'   => $this->partnerShop->partner->phone
		])->setAddress(Common\Address::create([
			'Street' => $this->partnerShop->street,
			'House'  => $this->partnerShop->house,
		])));
		
		$package = Common\Package::create([
			'Number'  => $this->orderData['order_number'],
			'BarCode' => $this->orderData['order_number']
		]);
		
		
		foreach ($this->cart as $item) {
			$offer = PartnerOffer::whereId($item['offerId'])->first();
			if ($offer) {
				$package->addItem(Common\Item::create([
					'WareKey' => $offer->product->article,
					'Cost'    => $item['price'],
					'Payment' => 0, // Оплата за товар при получение (за единицу товара)
					'Weight'  => $offer->product->weight,
					'Amount'  => $item['countBuy'],
					'Comment' => $offer->product->name
				]));
			}
		}
		
		$orderCDEK->addPackage($package);
		$cdekDeliveryRequest = new Requests\DeliveryRequest([
			'Number' => $this->orderData['order_number']
		]);
		$cdekDeliveryRequest->addOrder($orderCDEK);
		
		$cdekDeliveryResponse = $this->cdekClient->sendDeliveryRequest($cdekDeliveryRequest);
		
		if ($cdekDeliveryResponse->hasErrors()) {
			// обработка ошибок
			
			foreach ($cdekDeliveryResponse->getErrors() as $order) {
				// заказы с ошибками
				$order->getMessage();
				$order->getErrorCode();
				$order->getNumber();
			}
			
			foreach ($cdekDeliveryResponse->getMessages() as $message) {
				// Сообщения об ошибках
				Log::debug($message->getMessage());
			}
		}
		
		foreach ($cdekDeliveryResponse->getOrders() as $order) {
			/** @var \CdekSDK\Common\Order order */
			$this->order = $order;
			break;
		}
	}
	
	/**
	 * @return Common\Order
	 */
	public function getOrder(): ?Order
	{
		return $this->order;
	}
}