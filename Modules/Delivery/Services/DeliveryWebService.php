<?php


namespace Modules\Delivery\Services;


use App\Services\BaseService;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\Factory;
use Illuminate\View\View;
use Modules\Delivery\Contracts\DeliveryContract;
use Modules\Delivery\Http\Requests\CreateDeliveryRequest;
use Modules\Delivery\Http\Requests\UpdateDeliveryRequest;

/**
 * Class DeliveryWebService
 *
 * @package Modules\Delivery\Services
 */
class DeliveryWebService extends BaseService
{
    /** @var  DeliveryContract */
    private $deliveryRepository;
    
    /**
     * DeliveryController constructor.
     *
     * @param  DeliveryContract  $deliveryRepo
     */
    public function __construct(DeliveryContract $deliveryRepo)
    {
        $this->deliveryRepository = $deliveryRepo;
    }
    
    /**
     * Display a listing of the Delivery.
     *
     * @param  Request  $request
     *
     * @return View
     */
    public function index(Request $request): View
    {
        $deliveries = $this->deliveryRepository->listDeliveries();
        
        $this->setPageTitle(
            __('delivery::messages.title'),
            __('delivery::messages.index_subtitle'),
            __('delivery::messages.index_leadtext')
        );
        
        return view('delivery::deliveries.index', compact('deliveries'));
    }
    
    /**
     * Show the form for creating a new Delivery.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $this->setPageTitle(
            __('advantages::messages.title'),
            __('advantages::messages.create_subtitle'),
            __('advantages::messages.create_leadtext')
        );
        return view('delivery::deliveries.create');
    }
    
    /**
     * Store a newly created Delivery in storage.
     *
     * @param  CreateDeliveryRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateDeliveryRequest $request): RedirectResponse
    {
        $delivery = $this->deliveryRepository->createDelivery($request->all());
        
        if (!$delivery) {
            return $this->responseRedirectBack(
                __('delivery::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.delivery.index',
            __('delivery::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Display the specified Delivery.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $id): View
    {
        $delivery = $this->deliveryRepository->findDeliveryById($id);
        
        if ($delivery === null) {
            return $this->responseRedirect(
                'admin.delivery.index',
                __('delivery::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }
        
        $this->setPageTitle(
            __('advantages::messages.title'),
            __('advantages::messages.create_subtitle', ['id' => $id]),
            __('advantages::messages.create_leadtext', ['id' => $id])
        );
        
        return view('delivery::deliveries.show', compact('delivery'));
    }
    
    /**
     * Show the form for editing the specified Delivery.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        $delivery = $this->deliveryRepository->findDeliveryById($id);
        
        if ($delivery === null) {
            return $this->responseRedirect(
                'admin.delivery.index',
                __('delivery::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }
        $this->setPageTitle(
            __('advantages::messages.title'),
            __('advantages::messages.create_subtitle', ['id' => $id]),
            __('advantages::messages.create_leadtext', ['id' => $id])
        );
        return view('deliveries.edit', compact('delivery'));
    }
    
    /**
     * Update the specified Delivery in storage.
     *
     * @param  int                    $id
     * @param  UpdateDeliveryRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateDeliveryRequest $request): RedirectResponse
    {
        $delivery = $this->deliveryRepository->findDeliveryById($id);
        
        if ($delivery === null) {
            return $this->responseRedirect(
                'admin.delivery.index',
                __('delivery::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }
        
        $delivery = $this->deliveryRepository->updateDelivery($request->all(), $id);
        
        if (!$delivery) {
            return $this->responseRedirectBack(
                __('delivery::messages.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.delivery.index',
            __('delivery::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified Delivery from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        $delivery = $this->deliveryRepository->findDeliveryById($id);
        
        if ($delivery === null) {
            return $this->responseRedirect(
                'admin.delivery.index',
                __('delivery::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                false,
                );
        }
        
        $delivery = $this->deliveryRepository->deleteDelivery($id);
        
        if (!$delivery) {
            return $this->responseRedirectBack(
                __('delivery::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.delivery.index',
            __('delivery::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
}
