<?php


namespace Modules\Delivery\Services\API\V1;

use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Delivery\Contracts\DeliveryContract;
use Modules\Delivery\Http\Requests\API\V1\CreateDeliveryAPIRequest;
use Modules\Delivery\Http\Requests\API\V1\UpdateDeliveryAPIRequest;

/**
 * Class DeliveryApiService
 *
 * @package Modules\Delivery\Services\API\V1
 */
class DeliveryApiService extends BaseService
{
    /** @var  DeliveryContract */
    private $deliveryRepository;
    
    /**
     * DeliveryAPIController constructor.
     *
     * @param  DeliveryContract  $deliveryRepo
     */
    public function __construct(DeliveryContract $deliveryRepo)
    {
        $this->deliveryRepository = $deliveryRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $deliveries = $this->deliveryRepository->listDeliveries();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('delivery::messages.success_retrieve'),
            $deliveries
        );
    }
    
    /**
     * @param  CreateDeliveryAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateDeliveryAPIRequest $request): JsonResponse
    {
        $delivery = $this->deliveryRepository->createDelivery($request->all());
        
        if (!$delivery) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('delivery::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('delivery::messages.success_create'),
            $delivery
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $delivery = $this->deliveryRepository->findDeliveryById($id);
        
        if ($delivery === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('delivery::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('delivery::messages.success_found', ['id' => $id]),
            $delivery
        );
    }
    
    /**
     * @param  int                       $id
     * @param  UpdateDeliveryAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateDeliveryAPIRequest $request): JsonResponse
    {
        $delivery = $this->deliveryRepository->findDeliveryById($id);
        
        if ($delivery === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('delivery::messages.not_found', ['id' => $id])
            );
        }
        
        $delivery = $this->deliveryRepository->updateDelivery($request->all(), $id);
        
        if (!$delivery) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('delivery::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('delivery::messages.success_update'),
            $delivery
        );
    }
    
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $delivery = $this->deliveryRepository->findDeliveryById($id);
        
        if ($delivery === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('delivery::messages.not_found', ['id' => $id])
            );
        }
        
        $delivery = $this->deliveryRepository->deleteDelivery($id);
        
        if (!$delivery) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('delivery::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('delivery::messages.success_delete'),
            $delivery
        );
    }
}
