<?php


namespace Modules\Delivery\Services;


use CdekSDK\Common\Location;
use App\User;
use Exception;
use Modules\PartnerShop\Models\PartnerShop;
use Modules\Product\Models\Product;
use WildTuna\BoxberrySdk\Client;
use WildTuna\BoxberrySdk\Entity\Customer;
use WildTuna\BoxberrySdk\Entity\Item;
use WildTuna\BoxberrySdk\Entity\Order;
use WildTuna\BoxberrySdk\Entity\Place;
use WildTuna\BoxberrySdk\Entity\RussianPostParams;
use WildTuna\BoxberrySdk\Exception\BoxBerryException;

/**
 * Обработчик регистрации заказа от Интернет-магазина для Boxberry.
 *
 * Class BoxberryService
 *
 * @package Modules\Delivery\Services
 */
class BoxberryService
{
	/** @var \WildTuna\BoxberrySdk\Client */
	private $boxberryClient;
	
	/** @var \WildTuna\BoxberrySdk\Entity\Order */
	private $order;
	
	/** @var array */
	private $orderData;
	
	/** @var \Modules\PartnerShop\Models\PartnerShop */
	private $partnerShop;
	
	/** @var Location */
	private $addressSender;
	
	/** @var array */
	private $cart;
	
	/** @var \App\User */
	private $recipient;
	
	/**
	 * BoxberryService constructor.
	 *
	 * @param  array                                    $orderData
	 * @param  \Modules\PartnerShop\Models\PartnerShop  $partnerShop
	 * @param  Location                                 $addressSender
	 * @param  \App\User                                $buyer
	 * @param  array                                    $cart
	 */
	public function __construct(
		array $orderData,
		PartnerShop $partnerShop,
		Location $addressSender,
		User $buyer,
		array $cart
	) {
		$this->orderData = $orderData;
		$this->partnerShop = $partnerShop;
		$this->addressSender = $addressSender; // TODO: Заменить на независимые данные.
		$this->recipient = $buyer;
		$this->cart = $cart;
		$this->setup();
		$this->makeOrder();
	}
	
	/**
	 * Подготовляет Http-клиент для работы со SDK
	 */
	private function setup(): void
	{
		$this->boxberryClient = new Client(
			config('boxberry.BOXBERRY_TIMEOUT'),
			config('boxberry.BOXBERRY_BASE_URI')
		);
		$this->boxberryClient->setToken('main', config('boxberry.BOXBERRY_SDK_API_TOKEN'));
		$this->boxberryClient->setCurrentToken('main');
	}
	
	public function makeOrder(): void
	{
		//dd($this->orderData, $this->partnerShop, $this->addressSender, $this->recipient, $this->cart);
		
		try {
			$order = new Order();
			if ($this->orderData['city'] === 'Москва' || $this->orderData['city'] === 'Санкт-Петербург') {
				$order->setDeliveryDate(date('Y-m-d')); // Дата доставки от +1 день до +5 дней от текущий даты (только для доставки по Москве, МО и Санкт-Петербургу)
			}
			$order->setOrderId($this->orderData['order_number']); // ID заказа в ИМ
			$order->setPaymentAmount($this->orderData['grand_total']); // Сумма к оплате
			$order->setDeliveryAmount(300); // Стоимость доставки TODO: Заменить на полученные значения от BoxBerry
			$order->setComment($this->orderData['comments']); // Комментарий к заказу
			$order->setVid(Order::RUSSIAN_POST); // Тип доставки (1 - ПВЗ, 2 - КД, 3 - Почта России)
			
			$customer = new Customer();
			$customer->setFio($this->recipient->name); // ФИО получателя (TODO: Добавить поле отчество у пользователя и написать accessor)
			$customer->setPhone($this->recipient->phone); // Контактный номер телефона
			$customer->setEmail($this->recipient->email); // E-mail для оповещений
			
			$customer->setIndex($this->orderData['post_code']); // Почтовый индекс получателя (не заполянется, если в ПВЗ)
			$customer->setCity($this->orderData['city']); // (не заполянется, если в ПВЗ)
			$customer->setAddress($this->orderData['address']); // Адрес доставки (не заполянется, если в ПВЗ)
			
			
			// Поля ниже заполняются для организации (не обязательные)
			$customer->setOrgName($this->partnerShop->lawShopName); // Наименование организации
			$customer->setOrgAddress($this->partnerShop->address); // Арес организации
			$customer->setOrgInn($this->partnerShop->inn); // ИНН организации
			$customer->setOrgKpp($this->partnerShop->kpp); // КПП организации
			
			$order->setCustomer($customer);
			
			foreach ($this->cart as $cartItem) {
				$_product = Product::whereId($cartItem['productId'])->first();
				// Создаем места в заказе
				$place = new Place();
				$place->setWeight($_product->weight !== 0 ? $_product->weight : 50); // Вес места в граммах
				$order->setPlaces($place);
				
				// Создаем товары
				$item = new Item();
				$item->setId($_product->id); // ID товара в БД ИМ'
				$item->setName($_product->name); // Название товара
				$item->setAmount($cartItem['price']); // Цена единицы товара
				$item->setQuantity($cartItem['countBuy']); // Количество
				$item->setVat(20); // Ставка НДС
				$item->setUnit('шт'); // Единица измерения
				$order->setItems($item);
				$order->setSenderName($this->partnerShop->shopName); // Наименование отправителя
				$order->setIssue(Order::TOI_DELIVERY_WITH_OPENING_AND_VERIFICATION); // вид выдачи (см. константы класса)
			}
			
			// Для отправления Почтой России необходимо заполнить дополнительные параметры
			$russianPostParams = new RussianPostParams();
			$russianPostParams->setType(RussianPostParams::PT_POSILKA); // Тип отправления (см. константы класса)
			$russianPostParams->setFragile(true); // Хрупкая посылка
			$russianPostParams->setStrong(true); // Строгий тип
			$russianPostParams->setOptimize(true); // Оптимизация тарифа
			$russianPostParams->setPackingType(RussianPostParams::PACKAGE_IM_MORE_160); // Тип упаковки (см. константы класса)
			$russianPostParams->setPackingStrict(false); // Строгая упаковка
			
			// Габариты тарного места (см) Обязательны для доставки Почтой России.
			$russianPostParams->setLength(10);
			$russianPostParams->setWidth(10);
			$russianPostParams->setHeight(10);
			
			$order->setRussianPostParams($russianPostParams);
			$result = $this->boxberryClient->createOrder($order);
			
			$this->setOrder($result);
			
			/*
			 array(
			   'track'=>'DUD15224387', // Трек-номер BB
			   'label'=>'URI' // Ссылка на скачивание PDF файла с этикетками
			 );
			 */
		} catch (BoxBerryException $e) {
			// Обработка ошибки вызова API BB
			\Log::debug($e->getMessage()); // текст ошибки
			\Log::debug($e->getCode()); // http код ответа сервиса BB
			\Log::debug($e->getRawResponse()); // ответ сервера BB как есть (http request body)
		} catch (Exception $e) {
			// Обработка исключения
			\Log::debug($e->getMessage());
		}
	}
	
	/**
	 * @return \WildTuna\BoxberrySdk\Entity\Order
	 */
	public function getOrder(): ?Order
	{
		return $this->order;
	}
	
	/**
	 * @param  array  $orderResult
	 */
	public function setOrder(array $orderResult): void
	{
		$this->order = $orderResult;
	}
}