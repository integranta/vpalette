<?php

namespace Modules\Delivery\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Delivery\Models\Delivery;

/**
 * Class UpdateDeliveryAPIRequest
 *
 * @package Modules\Delivery\Http\Requests\API\V1
 */
class UpdateDeliveryAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Delivery::$rules;
    }
}
