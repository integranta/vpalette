<?php

namespace Modules\Delivery\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Delivery\Models\Delivery;

/**
 * Class CreateDeliveryRequest
 *
 * @package Modules\Delivery\Http\Requests
 */
class CreateDeliveryRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Delivery::$rules;
    }
}
