<?php

namespace Modules\Delivery\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Delivery\Http\Requests\CreateDeliveryRequest;
use Modules\Delivery\Http\Requests\UpdateDeliveryRequest;
use Modules\Delivery\Services\DeliveryWebService;

/**
 * Class DeliveryController
 *
 * @package Modules\Delivery\Http\Controllers
 */
class DeliveryController extends Controller
{
    /** @var  DeliveryWebService */
    private $deliveryWebService;

    /**
     * DeliveryController constructor.
     *
     * @param  DeliveryWebService  $service
     */
    public function __construct(DeliveryWebService $service)
    {
        $this->deliveryWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the Delivery.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function index(Request $request): View
    {
        return $this->deliveryWebService->index($request);
    }

    /**
     * Show the form for creating a new Delivery.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->deliveryWebService->create();
    }

    /**
     * Store a newly created Delivery in storage.
     *
     * @param  CreateDeliveryRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateDeliveryRequest $request): RedirectResponse
    {
        return $this->deliveryWebService->store($request);
    }

    /**
     * Display the specified Delivery.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $id): View
    {
        return $this->deliveryWebService->show($id);
    }

    /**
     * Show the form for editing the specified Delivery.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->deliveryWebService->edit($id);
    }

    /**
     * Update the specified Delivery in storage.
     *
     * @param  int                    $id
     * @param  UpdateDeliveryRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateDeliveryRequest $request): RedirectResponse
    {
        return $this->deliveryWebService->update($id, $request);
    }

    /**
     * Remove the specified Delivery from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->deliveryWebService->destroy($id);
    }
}
