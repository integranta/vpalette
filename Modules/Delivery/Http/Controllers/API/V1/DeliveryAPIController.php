<?php

namespace Modules\Delivery\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Delivery\Http\Requests\API\V1\CreateDeliveryAPIRequest;
use Modules\Delivery\Http\Requests\API\V1\UpdateDeliveryAPIRequest;
use Modules\Delivery\Services\API\V1\DeliveryApiService;

/**
 * Class DeliveryController
 *
 * @package Modules\Delivery\Http\Controllers\API\V1
 */
class DeliveryAPIController extends Controller
{
    /** @var  DeliveryApiService */
    private $deliveryApiService;

    /**
     * DeliveryAPIController constructor.
     *
     * @param  DeliveryApiService  $service
     */
    public function __construct(DeliveryApiService $service)
    {
        $this->deliveryApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->deliveryApiService->index($request);
    }

    /**
     * @param  CreateDeliveryAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateDeliveryAPIRequest $request): JsonResponse
    {
        return $this->deliveryApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->deliveryApiService->show($id);
    }

    /**
     * @param  int                       $id
     * @param  UpdateDeliveryAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateDeliveryAPIRequest $request): JsonResponse
    {
        return $this->deliveryApiService->update($id, $request);
    }


    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->deliveryApiService->destroy($id);
    }
}
