<!-- Delivery Period Max Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_period_max', 'Delivery Period Max:') !!}
    {!! Form::text('delivery_period_max', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Period Min Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_period_min', 'Delivery Period Min:') !!}
    {!! Form::text('delivery_period_min', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Total Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_total_price', 'Delivery Total Price:') !!}
    {!! Form::text('delivery_total_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Cdek Print Receipts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_cdek_print_receipts', 'Delivery Cdek Print Receipts:') !!}
    {!! Form::file('delivery_cdek_print_receipts') !!}
</div>
<div class="clearfix"></div>

<!-- Delivery Pochta Russian F7 Form Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_pochta_russian_f7_form', 'Delivery Pochta Russian F7 Form:') !!}
    {!! Form::file('delivery_pochta_russian_f7_form') !!}
</div>
<div class="clearfix"></div>

<!-- Delivery Pochta Russian F112 Form Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_pochta_russian_f112_form', 'Delivery Pochta Russian F112 Form:') !!}
    {!! Form::file('delivery_pochta_russian_f112_form') !!}
</div>
<div class="clearfix"></div>

<!-- Delivery Pochta Russian Print Receipts Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_pochta_russian_print_receipts', 'Delivery Pochta Russian Print Receipts:') !!}
    {!! Form::file('delivery_pochta_russian_print_receipts') !!}
</div>
<div class="clearfix"></div>

<!-- Delivery Cdek Print Labels Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_cdek_print_labels', 'Delivery Cdek Print Labels:') !!}
    {!! Form::file('delivery_cdek_print_labels') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('deliveries.index') !!}" class="btn btn-default">Cancel</a>
</div>
