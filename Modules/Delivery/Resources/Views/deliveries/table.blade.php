<div class="table-responsive">
    <table class="table" id="deliveries-table">
        <thead>
        <tr>
            <th>Delivery Period Max</th>
            <th>Delivery Period Min</th>
            <th>Delivery Total Price</th>
            <th>Delivery Cdek Print Receipts</th>
            <th>Delivery Pochta Russian F7 Form</th>
            <th>Delivery Pochta Russian F112 Form</th>
            <th>Delivery Pochta Russian Print Receipts</th>
            <th>Delivery Cdek Print Labels</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($deliveries as $delivery)
            <tr>
                <td>{!! $delivery->delivery_period_max !!}</td>
                <td>{!! $delivery->delivery_period_min !!}</td>
                <td>{!! $delivery->delivery_total_price !!}</td>
                <td>{!! $delivery->delivery_cdek_print_receipts !!}</td>
                <td>{!! $delivery->delivery_pochta_russian_f7_form !!}</td>
                <td>{!! $delivery->delivery_pochta_russian_f112_form !!}</td>
                <td>{!! $delivery->delivery_pochta_russian_print_receipts !!}</td>
                <td>{!! $delivery->delivery_cdek_print_labels !!}</td>
                <td>
                    {!! Form::open(['route' => ['deliveries.destroy', $delivery->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('deliveries.show', [$delivery->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('deliveries.edit', [$delivery->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
