<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $delivery->id !!}</p>
</div>

<!-- Delivery Period Max Field -->
<div class="form-group">
    {!! Form::label('delivery_period_max', 'Delivery Period Max:') !!}
    <p>{!! $delivery->delivery_period_max !!}</p>
</div>

<!-- Delivery Period Min Field -->
<div class="form-group">
    {!! Form::label('delivery_period_min', 'Delivery Period Min:') !!}
    <p>{!! $delivery->delivery_period_min !!}</p>
</div>

<!-- Delivery Total Price Field -->
<div class="form-group">
    {!! Form::label('delivery_total_price', 'Delivery Total Price:') !!}
    <p>{!! $delivery->delivery_total_price !!}</p>
</div>

<!-- Delivery Cdek Print Receipts Field -->
<div class="form-group">
    {!! Form::label('delivery_cdek_print_receipts', 'Delivery Cdek Print Receipts:') !!}
    <p>{!! $delivery->delivery_cdek_print_receipts !!}</p>
</div>

<!-- Delivery Pochta Russian F7 Form Field -->
<div class="form-group">
    {!! Form::label('delivery_pochta_russian_f7_form', 'Delivery Pochta Russian F7 Form:') !!}
    <p>{!! $delivery->delivery_pochta_russian_f7_form !!}</p>
</div>

<!-- Delivery Pochta Russian F112 Form Field -->
<div class="form-group">
    {!! Form::label('delivery_pochta_russian_f112_form', 'Delivery Pochta Russian F112 Form:') !!}
    <p>{!! $delivery->delivery_pochta_russian_f112_form !!}</p>
</div>

<!-- Delivery Pochta Russian Print Receipts Field -->
<div class="form-group">
    {!! Form::label('delivery_pochta_russian_print_receipts', 'Delivery Pochta Russian Print Receipts:') !!}
    <p>{!! $delivery->delivery_pochta_russian_print_receipts !!}</p>
</div>

<!-- Delivery Cdek Print Labels Field -->
<div class="form-group">
    {!! Form::label('delivery_cdek_print_labels', 'Delivery Cdek Print Labels:') !!}
    <p>{!! $delivery->delivery_cdek_print_labels !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $delivery->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $delivery->updated_at !!}</p>
</div>

