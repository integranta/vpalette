<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateDeliveriesTable
 */
class CreateDeliveriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('delivery_period_max');
            $table->unsignedInteger('delivery_period_min');
            $table->double('delivery_total_price', 8, 2);
            $table->string('delivery_cdek_print_receipts');
            $table->string('delivery_pochta_russian_f7_form');
            $table->string('delivery_pochta_russian_f112_form');
            $table->string('delivery_pochta_russian_print_receipts');
            $table->string('delivery_cdek_print_labels');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deliveries');
    }
}
