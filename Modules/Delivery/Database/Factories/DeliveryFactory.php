<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\Delivery\Models\Delivery;
use Faker\Generator as Faker;

$factory->define(Delivery::class, function (Faker $faker) {
    
    return [
        'delivery_period_max' => $faker->word,
        'delivery_period_min' => $faker->word,
        'delivery_total_price' => $faker->word,
        'delivery_cdek_print_receipts' => $faker->word,
        'delivery_pochta_russian_f7_form' => $faker->word,
        'delivery_pochta_russian_f112_form' => $faker->word,
        'delivery_pochta_russian_print_receipts' => $faker->word,
        'delivery_cdek_print_labels' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
