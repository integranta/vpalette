<?php

namespace Modules\Brand\Observers;

use App\Traits\Cacheable;
use Modules\Brand\Models\Brand;

/**
 * Class BrandObserver
 *
 * @package Modules\Brand\Observers
 */
class BrandObserver
{
    use Cacheable;
    
    /**
     * Handle the brand "creating" event.
     *
     * @param  Brand  $brand
     *
     * @return void
     */
    public function creating(Brand $brand): void
    {
        //
    }
    
    /**
     * Handle the brand "created" event.
     *
     * @param  Brand  $brand
     *
     * @return void
     */
    public function created(Brand $brand): void
    {
        $this->incrementCountItemsInCache($brand, config('brand.cache_key'));
    }
    
    /**
     * Handle the brand "updated" event.
     *
     * @param  Brand  $brand
     *
     * @return void
     */
    public function updated(Brand $brand): void
    {
        //
    }
    
    /**
     * Handle the brand "deleted" event.
     *
     * @param  Brand  $brand
     *
     * @return void
     */
    public function deleted(Brand $brand): void
    {
        $this->decrementCountItemsInCache($brand, config('brand.cache_key'));
    }
    
    /**
     * Handle the brand "restored" event.
     *
     * @param  Brand  $brand
     *
     * @return void
     */
    public function restored(Brand $brand): void
    {
        $this->incrementCountItemsInCache($brand, config('brand.cache_key'));
    }
    
    /**
     * Handle the brand "force deleted" event.
     *
     * @param  Brand  $brand
     *
     * @return void
     */
    public function forceDeleted(Brand $brand): void
    {
        $this->decrementCountItemsInCache($brand, config('brand.cache_key'));
    }
}
