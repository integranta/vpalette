<?php

return [
    'name' => 'Brand',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all brands as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('BRAND_CACHE_KEY','index_brands'),
    'cache_ttl' => env('BRAND_CACHE_TTL',360000),
];
