<?php

namespace Modules\Brand\Http\Controllers;

use App\DataTables\BrandsDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Brand\Http\Requests\CreateBrandRequest;
use Modules\Brand\Http\Requests\UpdateBrandRequest;
use Modules\Brand\Services\BrandWebService;

/**
 * Class BrandController
 *
 * @package Modules\Brand\Http\Controllers
 */
class BrandController extends Controller
{
  /** @var BrandWebService */
  protected $brandWebService;
  
  /**
   * BrandController constructor.
   *
   * @param  BrandWebService  $service
   */
  public function __construct(BrandWebService $service)
  {
	$this->brandWebService = $service;
	$this->middleware('role:Owner|Admin');
	$this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
  }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\BrandsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
  public function index(BrandsDataTable $dataTable)
  {
	return $this->brandWebService->index($dataTable);
  }
  
  /**
   * Show the form for creating a new resource.
   *
   * @return Factory|View
   */
  public function create(): View
  {
	return $this->brandWebService->create();
  }
  
  /**
   * Store a newly created resource in storage.
   *
   * @param  CreateBrandRequest  $request
   *
   * @return RedirectResponse
   */
  public function store(CreateBrandRequest $request): RedirectResponse
  {
	return $this->brandWebService->store($request);
  }
  
  /**
   * Display the specified Brand.
   *
   * @param  int  $id
   *
   * @return Factory|View
   */
  public function show(int $id): View
  {
	return $this->brandWebService->show($id);
  }
  
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   *
   * @return Factory|View
   */
  public function edit(int $id): View
  {
	return $this->brandWebService->edit($id);
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  UpdateBrandRequest  $request
   * @param                      $id
   *
   * @return RedirectResponse
   */
  public function update(int $id, UpdateBrandRequest $request): RedirectResponse
  {
	return $this->brandWebService->update($id, $request);
  }
  
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   *
   * @return RedirectResponse
   */
  public function destroy(int $id): RedirectResponse
  {
	return $this->brandWebService->destroy($id);
  }
  
  public function import()
  {
	return $this->brandWebService->import();
  }
}
