<?php

namespace Modules\Brand\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Brand\Http\Requests\API\V1\{CreateBrandAPIRequest, UpdateBrandAPIRequest};
use Modules\Brand\Services\API\V1\BrandApiService;

/**
 * Class BrandAPIController
 *
 * @package Modules\Brand\Http\Controllers\API\V1
 */
class BrandAPIController extends Controller
{
    /** @var BrandApiService */
    private $brandApiService;


    /**
     * BrandAPIController constructor.
     *
     * @param  BrandApiService  $service
     */
    public function __construct(BrandApiService $service)
    {
        $this->brandApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->brandApiService->index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBrandAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateBrandAPIRequest $request): JsonResponse
    {
        return $this->brandApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->brandApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBrandAPIRequest  $request
     * @param  int                    $id
     *
     * @return JsonResponse
     */
    public function update(UpdateBrandAPIRequest $request, int $id): JsonResponse
    {
        return $this->brandApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->brandApiService->destroy($id);
    }
}
