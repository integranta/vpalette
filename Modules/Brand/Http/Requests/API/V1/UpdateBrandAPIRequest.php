<?php

namespace Modules\Brand\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateBrandAPIRequest
 *
 * @package Modules\Brand\Http\Requests\API\V1
 */
class UpdateBrandAPIRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'slug' => 'unique:brands,id,'.$this->input('id'),
            'description' => 'nullable|string',
            'logo' => 'mimes:jpg,jpeg,png|max:1000',
            'file' => 'nullable|mimes:pdf,doc,docx|max:1000',
            'preview_file' => 'nullable|mimes:jpg'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
