<?php

namespace Modules\Brand\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateBrandAPIRequest
 *
 * @package Modules\Brand\Http\Requests\API\V1
 */
class CreateBrandAPIRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'slug' => 'unique:brands|string|',
            'description' => 'nullable|string',
            'logo' => 'mimes:jpg,jpeg,png|max:1000',
            'file' => 'nullable|mimes:pdf,doc,docx|max:5000',
            'preview_file' => 'nullable|mimes:jpg|max:1000'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
