<?php


namespace Modules\Brand\Repositories;


use App\Repositories\BaseRepository;
use App\Traits\UploadAble;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Modules\Brand\Contracts\BrandContract;
use Modules\Brand\Models\Brand;
use Str;

/**
 * Class BrandRepository
 *
 * @package Modules\Brand\Repositories
 */
class BrandRepository extends BaseRepository implements BrandContract
{

    use UploadAble;

    /**
     * BrandRepository constructor.
     *
     * @param  Brand  $model
     */
    public function __construct(Brand $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listBrands(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('brand.cache_key').$postfix,
            config('brand.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }

    /**
     * @param  int  $id
     *
     * @return Brand
     */
    public function findBrandById(int $id): ?Brand
    {
        return $this->find($id);
    }

    /**
     * @param  array  $params
     *
     * @return Brand
     */
    public function createBrand(array $params): Brand
    {
        $path = $this->getPathStr($params['name']);

        $brandFields = [
            'name' => $params['name'],
            'description' => $params['description'],
            'url' => $params['url'],
            'preview_file' => null
        ];

        $logoPath = null;

        if (isset($params['logo'])) {
            $logoPath = $this->makeCachedImage($params['logo'], $path);
        }
        $brandFields['logo'] = $logoPath;

        $filePath = null;
        if (isset($params['file'])) {
            $filePath = Storage::disk('uploads')->putFile($path, $params['file']);
        }
        $brandFields['file'] = $filePath;

        return $this->create($brandFields);
    }

    /**
     * @param  array  $params
     * @param         $id
     *
     * @return bool
     */
    public function updateBrand(array $params, int $id): bool
    {
        $logo = null;
        $brandField = [
            'name' => $params['name'],
            'description' => $params['description'],
            'url' => $params['url']
        ];

        $path = $this->getPathStr($params['name']);


        if (isset($params['logo'])) {
            $logo = $this->makeCachedImage($params['logo'], $path);
        }

        $brandField['logo'] = $logo;

        return $this->update($brandField, $id);
    }

    /**
     * @param  int  $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteBrand(int $id): bool
    {
        $brand = $this->findBrandById($id);

        if (!$brand)
        {
            return false;
        }

        $path = $this->getPathStr($brand->name);

        if (Storage::disk('uploads')->exists($path)) {
            Storage::disk('uploads')->deleteDirectory($path);
        }

        return $this->delete($id);
    }

    /**
     * @param  string  $name
     *
     * @return string
     */
    private function getPathStr(string $name): string
    {
        return 'brands/'.Str::of($name)
                ->trim()
                ->lower()
                ->slug('_', 'ru');
    }
}
