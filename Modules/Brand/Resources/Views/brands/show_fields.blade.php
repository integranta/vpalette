<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Name Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'name',
                    'Название:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!! Form::text('name', $brand->name, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <!-- Logo Image Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'logo',
                    'Логотип бренда:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                <div class="col-sm-2">
                    <img src="{{ asset($brand->logo) }}" alt="">
                </div>
                {!! Form::hidden('preview_image', $brand->logo) !!}
            </div>
        </div>
        <!-- Catalog Files Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'file',
                    'Каталог бренда:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                @if(!is_null($brand->file))
                    <div class="col-md-2">
                        <a href="{{ asset($brand->file) }}">
                            Файл каталога
                        </a>
                    </div>
                @endif
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Preview Text Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'description',
                    'Текст для анонса:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea(
                        'description',
                        $brand->description,
                        ['class' => 'form-control tinyMCE', 'id' => 'description']
                    )
                !!}
            </div>
        </div>
    </div>
</div>
