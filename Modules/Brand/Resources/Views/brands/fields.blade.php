<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Name Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'name',
                    'Название:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!! Form::text('name', $model->name, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <!-- Logo Image Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'logo',
                    'Логотип бренда:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!! Form::file('logo') !!}
            </div>
        </div>
        <!-- Catalog Files Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'file',
                    'Каталог бренда:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!! Form::file('file') !!}
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Preview Text Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'description',
                    'Текст для анонса:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea(
                        'description',
                        $model->description,
                        ['class' => 'form-control tinyMCE', 'id' => 'description']
                    )
                !!}
            </div>
        </div>
    </div>
</div>
