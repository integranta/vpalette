<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Name Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'name',
                    __('brand::templates.fields.labels.name'),
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::text(
                        'name',
                        $brand->name,
                        [
                            'class' => 'form-control',
                            'placeholder' => __('brand::templates.fields.placeholder.name')
                        ]
                    )
                !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <!-- Logo Image Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'logo',
                    __('brand::templates.fields.labels.logo'),
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                @if ($brand->logo !== null)
                    <div class="col-sm-2">
                        <img src="{{ url($brand->logo) }}" alt="">
                    </div>
                @else
                    {!! Form::hidden('logo', $brand->logo) !!}
                    {!! Form::file('logo') !!}
                @endif
            </div>
        </div>
        <!-- Catalog Files Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'file',
                    __('brand::templates.fields.labels.file'),
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                @if(!is_null($brand->file))
                    <div class="col-md-2">
                        <a href="{{ asset($brand->file) }}">
                            Файл каталога
                        </a>
                    </div>
                @else
                    {!! Form::file('file') !!}
                @endif
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Description Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'description',
                    __('brand::templates.fields.labels.description'),
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea(
                        'description',
                        $brand->description,
                        [
                            'class' => 'form-control tinyMCE',
                            'id' => 'description',
                            'placeholder' => __('brand::templates.fields.placeholder.description')
                        ]
                    )
                !!}
            </div>
        </div>
    </div>
</div>
