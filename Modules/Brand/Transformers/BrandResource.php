<?php

namespace Modules\Brand\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Product\Transformers\ProductResource;

/**
 * Class BrandResource
 *
 * @package Modules\Brand\Transformers
 */
class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'logo' => $this->logo,
            'description' => $this->description,
            'urlBrand' => $this->url,
            'catalogBrand' => $this->file,
            'previewCatalogBrand' => $this->preview_file,
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at,
        ];
    }
}
