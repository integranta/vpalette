<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Brand\Models\Brand;

/** @var Factory $factory */
$factory->define(Brand::class,
    function (Faker $faker) {
        return [
            'active' => true,
            'sort' => $faker->numberBetween(1, 500),
            'name' => $faker->sentence,
            'description' => $faker->realText(),
            'url' => $faker->url,
            'logo' => null
        ];
    });
