<?php

namespace Modules\Brand\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Brand\Models\Brand;
use Storage;
use Str;

/**
 * Class BrandDatabaseSeeder
 *
 * @package Modules\Brand\Database\Seeders
 */
class BrandDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $randomName = Str::random();

        $slug = Str::of('Fimo')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            "faker/brands/{$slug}.png",
            "brands/{$slug}/${randomName}.png"
        );

        Brand::create([
            'active' => true,
            'sort' => 1,
            'name' => 'Fimo',
            'logo' => "brands/{$slug}/${randomName}.png",
            'description' => '',
            'url' => '',
            'file' => null,
            'preview_file' => null
        ]);

        $slug = Str::of('ALEA-Mosaik')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            "faker/brands/{$slug}.png",
            "brands/{$slug}/${randomName}.png"
        );

        Brand::create([
            'active' => true,
            'sort' => 2,
            'name' => 'ALEA-Mosaik',
            'logo' => "brands/{$slug}/${randomName}.png",
            'description' => '',
            'url' => '',
            'file' => null,
            'preview_file' => null
        ]);

        $slug = Str::of('Sennelier')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            "faker/brands/{$slug}.png",
            "brands/{$slug}/${randomName}.png"
        );

        Brand::create([
            'active' => true,
            'sort' => 3,
            'name' => 'Sennelier',
            'logo' => "brands/{$slug}/${randomName}.png",
            'description' => '',
            'url' => '',
            'file' => null,
            'preview_file' => null
        ]);

        $slug = Str::of('Canson')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            "faker/brands/{$slug}.png",
            "brands/{$slug}/${randomName}.png"
        );

        Brand::create([
            'active' => true,
            'sort' => 4,
            'name' => 'Canson',
            'logo' => "brands/{$slug}/${randomName}.png",
            'description' => '',
            'url' => '',
            'file' => null,
            'preview_file' => null
        ]);

        $slug = Str::of('Mungyo')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            "faker/brands/{$slug}.png",
            "brands/{$slug}/${randomName}.png"
        );

        Brand::create([
            'active' => true,
            'sort' => 5,
            'name' => 'Mungyo',
            'logo' => "brands/{$slug}/${randomName}.png",
            'description' => '',
            'url' => '',
            'file' => null,
            'preview_file' => null
        ]);

        $slug = Str::of('Viva Decor')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            "faker/brands/{$slug}.png",
            "brands/{$slug}/${randomName}.png"
        );

        Brand::create([
            'active' => true,
            'sort' => 6,
            'name' => 'Viva Decor',
            'logo' => "brands/{$slug}/${randomName}.png",
            'description' => '',
            'url' => '',
            'file' => null,
            'preview_file' => null
        ]);

        $slug = Str::of('SKETCHMARKER')->lower()->trim()->slug('_', 'ru');
        Storage::disk('uploads')->copy(
            "faker/brands/{$slug}.png",
            "brands/{$slug}/${randomName}.png"
        );

        Brand::create([
            'active' => true,
            'sort' => 7,
            'name' => 'SKETCHMARKER',
            'logo' => "brands/{$slug}/${randomName}.png",
            'description' => '',
            'url' => '',
            'file' => null,
            'preview_file' => null
        ]);
    }
}
