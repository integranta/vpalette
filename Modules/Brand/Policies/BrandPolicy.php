<?php

namespace Modules\Brand\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Brand\Models\Brand;

/**
 * Class BrandPolicy
 *
 * @package Modules\Brand\Policies
 */
class BrandPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the brand.
     *
     * @param  User|null  $user
     * @param  Brand      $brand
     *
     * @return mixed
     */
    public function view(?User $user, Brand $brand)
    {
        if ($brand->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished brands')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create brands.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create brands')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the brand.
     *
     * @param  User   $user
     * @param  Brand  $brand
     *
     * @return mixed
     */
    public function update(User $user, Brand $brand)
    {
        if ($user->can('edit all brands')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the brand.
     *
     * @param  User   $user
     * @param  Brand  $brand
     *
     * @return mixed
     */
    public function delete(User $user, Brand $brand)
    {
        if ($user->can('delete any brand')) {
            return true;
        }
        
        return false;
    }
}
