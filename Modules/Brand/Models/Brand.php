<?php

namespace Modules\Brand\Models;

use App\Models\BaseModel;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Modules\Product\Models\Product;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Brand
 *
 * @package Modules\Brand\Models
 * @property int                                                                             $id
 * @property bool                                                                            $active
 * @property int                                                                             $sort
 * @property string                                                                          $name
 * @property string                                                                          $slug
 * @property null|string                                                                     $logo
 * @property string|null                                                                     $description
 * @property string|null                                                                     $url
 * @property string|null                                                                     $file
 * @property string|null                                                                     $previewFile
 * @property \Illuminate\Support\Carbon|null                                                 $createdAt
 * @property \Illuminate\Support\Carbon|null                                                 $updatedAt
 * @property \Illuminate\Support\Carbon|null                                                 $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Models\Product[] $products
 * @property-read int|null                                                                   $productsCount
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Brand\Models\Brand onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand wherePreviewFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Brand\Models\Brand whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Brand\Models\Brand withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Brand\Models\Brand withoutTrashed()
 * @mixin \Eloquent
 */
class Brand extends BaseModel
{
	use SoftDeletes;
	use SoftCascadeTrait;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
	/**
	 * Связанная с моделью таблица.
	 *
	 * @var string
	 */
	protected $table = 'brands';
	
	/**
	 * Атрибуты, для которых разрешено массовое назначение.
	 *
	 * @var array
	 */
	protected $fillable = [
		'active',
		'sort',
		'name',
		'slug',
		'url',
		'description',
		'logo',
		'file',
		'preview_file'
	];
	
	/**
	 * Атрибуты, которые должны быть преобразованы в даты.
	 *
	 * @var array
	 */
	protected $dates = [
		'deleted_at'
	];
	
	/**
	 * Атрибуты, которые нужно преобразовать в нативный тип.
	 *
	 * @var array
	 */
	protected $casts = [
		'name'         => 'string',
		'slug'         => 'string',
		'url'          => 'string',
		'description'  => 'string',
		'logo'         => 'string',
		'file'         => 'json',
		'preview_file' => 'string',
		'active'       => 'boolean',
		'sort'         => 'integer'
	];
	
	
	/**
	 * Получить ссылку на логотип.
	 *
	 * @param  string|null  $value
	 *
	 * @return null|string
	 */
	public function getLogoAttribute(?string $value): ?string
	{
		return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
	}
	
	public function getFileAttribute(?string $value): ?string
	{
		$files = json_decode($value);
		
		$arFiles = null;
		
		if ($files !== null) {
			foreach ($files as $file) {
				$arFiles[] = Storage::disk('uploads')->url('uploads/'.$file);
			}
			return json_encode($arFiles);
		}
		
		return null;
	}
	
	/**
	 * Получить товары от бренда.
	 *
	 * @return HasMany
	 */
	public function products(): HasMany
	{
		return $this->hasMany(Product::class);
	}
}
