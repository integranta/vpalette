const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/brand.js')
    .js(__dirname + '/Resources/Assets/js/app-create.js', 'js/brand-create.js')
    .js(__dirname + '/Resources/Assets/js/app-show.js', 'js/brand-show.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/brand.css', {
        implementation: require('node-sass')
    });

if (mix.inProduction()) {
    mix.version();
}
