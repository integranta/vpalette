<?php


namespace Modules\Brand\Services\API\V1;

use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Brand\Contracts\BrandContract;
use Modules\Brand\Http\Requests\API\V1\{CreateBrandAPIRequest, UpdateBrandAPIRequest};
use Modules\Brand\Transformers\BrandResource;

/**
 * Class BrandApiService
 *
 * @package Modules\Brand\Services\API\V1
 */
class BrandApiService extends BaseService
{
    /** @var BrandContract */
    private $brandRepository;
    
    
    /**
     * BrandAPIController constructor.
     *
     * @param  BrandContract  $brandRepo
     */
    public function __construct(BrandContract $brandRepo)
    {
        $this->brandRepository = $brandRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $brands = BrandResource::collection($this->brandRepository->listBrands());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('brand::messages.success_retrieve'),
            $brands
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBrandAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateBrandAPIRequest $request): JsonResponse
    {
        $brand = $this->brandRepository->createBrand($request->all());
        
        if (!$brand) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('brand::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('brand::messages.success_create'),
            $brand
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $brand = BrandResource::make($this->brandRepository->findBrandById($id));
        
        if (!$brand) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('brand::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('brand::messages.success_found', ['id' => $id]),
            $brand
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBrandAPIRequest  $request
     * @param  int                    $id
     *
     * @return JsonResponse
     */
    public function update(UpdateBrandAPIRequest $request, int $id): JsonResponse
    {
        $brand = $this->brandRepository->findBrandById($id);
        
        if (!$brand) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('brand::messages.not_found', ['id' => $id])
            );
        }
        
        $brand = $this->brandRepository->updateBrand($request->all(), $id);
        
        if (!$brand) {
            return $this->responseJson(
                false,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('brand::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('brand::messages.success_update'),
            $brand
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $brand = $this->brandRepository->findBrandById($id);
        
        if (!$brand) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('brand::messages.not_found', ['id' => $id])
            );
        }
        
        $brand = $this->brandRepository->deleteBrand($id);
        
        if (!$brand) {
            return $this->responseJson(
                false,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('brand::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('brand::messages.success_delete'),
            $brand
        );
    }
}
