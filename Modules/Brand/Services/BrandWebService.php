<?php


namespace Modules\Brand\Services;


use App\DataTables\BrandsDataTable;
use App\Imports\VendorsImport;
use App\Services\BaseService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Brand\Contracts\BrandContract;
use Modules\Brand\Http\Requests\CreateBrandRequest;
use Modules\Brand\Http\Requests\UpdateBrandRequest;
use Modules\Brand\Models\Brand;

/**
 * Class BrandWebService
 *
 * @package Modules\Brand\Services
 */
class BrandWebService extends BaseService
{
	/**
	 * @var BrandContract
	 */
	protected $brandRepository;
	
	/**
	 * BrandController constructor.
	 *
	 * @param  BrandContract  $brandRepository
	 */
	public function __construct(BrandContract $brandRepository)
	{
		$this->brandRepository = $brandRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\BrandsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(BrandsDataTable $dataTable)
	{
		$this->setPageTitle(
			__('brand::messages.title'),
			__('brand::messages.index_subtitle'),
			__('brand::messages.index_leadtext')
		);
		return $dataTable->render('brand::brands.index');
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$model = new Brand();
		
		$this->setPageTitle(
			__('brand::messages.title'),
			__('brand::messages.create_subtitle'),
			__('brand::messages.create_leadtext')
		);
		return view('brand::brands.create', compact('model'));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  CreateBrandRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateBrandRequest $request): RedirectResponse
	{
		
		$brand = $this->brandRepository->createBrand($request->all());
		
		
		if (!$brand) {
			return $this->responseRedirectBack(
				__('brand::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.brands.index',
			__('brand::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified Brand.
	 *
	 * @param  int  $id
	 *
	 * @return View|RedirectResponse|Factory
	 */
	public function show(int $id): View
	{
		$brand = $this->brandRepository->findBrandById($id);
		
		if (!$brand) {
			return $this->responseRedirect(
				'admin.brand.index',
				__('brand::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('brand::messages.title'),
			__('brand::messages.show_subtitle', ['name' => $brand->name]),
			__('brand::messages.show_leadtext', ['name' => $brand->name])
		);
		return view('brand::brands.show', compact('brand'));
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return View|RedirectResponse|Factory
	 */
	public function edit(int $id): View
	{
		$brand = $this->brandRepository->findBrandById($id);
		
		if (!$brand) {
			return $this->responseRedirect(
				'admin.brand.index',
				__('brand::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('brand::messages.title'),
			__('brand::messages.edit_subtitle', ['name' => $brand->name]),
			__('brand::messages.edit_leadtext', ['name' => $brand->name])
		);
		
		return view('brand::brands.edit', compact('brand'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  UpdateBrandRequest  $request
	 * @param                      $id
	 *
	 * @return RedirectResponse
	 */
	public function update(int $id, UpdateBrandRequest $request): RedirectResponse
	{
		$brand = $this->brandRepository->findBrandById($id);
		
		if (!$brand) {
			return $this->responseRedirect(
				'admin.brand.index',
				__('brand::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$brand = $this->brandRepository->updateBrand($request->all(), $id);
		
		if (!$brand) {
			return $this->responseRedirectBack(
				__('brand::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirectBack(
			__('brand::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 */
	public function destroy(int $id): RedirectResponse
	{
		$brand = $this->brandRepository->findBrandById($id);
		
		if (!$brand) {
			return $this->responseRedirect(
				'admin.brand.index',
				__('brand::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$brand = $this->brandRepository->deleteBrand($id);
		
		if (!$brand) {
			return $this->responseRedirectBack(
				__('brand::messages.error_delete'),
				self::FAIL_RESPONSE,
				true, true
			);
		}
		return $this->responseRedirect(
			'admin.brands.index',
			__('brand::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	public function import()
	{
		(new VendorsImport)->import(base_path('export_vendors.xlsx'));
	}
}
