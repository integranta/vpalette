<?php


namespace Modules\Brand\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Brand\Models\Brand;

/**
 * Interface BrandContract
 *
 * @package Modules\Brand\Contracts
 */
interface BrandContract
{
    /**
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listBrands(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Brand
     */
    public function findBrandById(int $id): ?Brand;
    
    /**
     * @param  array  $params
     *
     * @return Brand
     */
    public function createBrand(array $params): Brand;
    
    /**
     * @param  array  $params
     * @param         $id
     *
     * @return bool
     */
    public function updateBrand(array $params, int $id): bool;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteBrand(int $id): bool;
}
