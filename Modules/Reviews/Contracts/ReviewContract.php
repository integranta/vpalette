<?php


namespace Modules\Reviews\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Reviews\Models\Review;

/**
 * Interface ReviewContract
 *
 * @package Modules\Reviews\Contracts
 */
interface ReviewContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return mixed
     */
    public function listReviews(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return mixed
     */
    public function findReviewById(int $id): ?Review;
    
    /**
     * @param  array  $data
     *
     * @return mixed
     */
    public function findReviewBy(array $data): ?Review;
    
    /**
     * @param  array  $params
     *
     * @return mixed
     */
    public function createReview(array $params): Review;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return mixed
     */
    public function updateReview(array $params, int $id): Review;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteReview(int $id): bool;
}
