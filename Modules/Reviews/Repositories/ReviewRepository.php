<?php

namespace Modules\Reviews\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Reviews\Contracts\ReviewContract;
use Modules\Reviews\Models\Review;
use Storage;
use Str;

/**
 * Class ReviewRepository
 *
 * @package Modules\Reviews\Repositories
 * @version August 19, 2019, 9:22 pm UTC
 */
class ReviewRepository extends BaseRepository implements ReviewContract
{
    /**
     * ReviewRepository constructor.
     *
     * @param  Review  $model
     */
    public function __construct(Review $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listReviews(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('reviews.cache_key').$postfix,
            config('reviews.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @inheritDoc
     */
    public function findReviewById(int $id): ?Review
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function findReviewBy(array $data): ?Review
    {
        return $this->findBy($data);
    }
    
    /**
     * @inheritDoc
     */
    public function createReview(array $params): Review
    {
        $params['photos'] = $this->uploadPicture($params);
        
        $review = $this->create($params);
        
        $review->setRating($review, $review->user, $params['rating']);
        
        return $review;
    }
    
    /**
     * @inheritDoc
     */
    public function updateReview(array $params, int $id): Review
    {
        $params['photos'] = $this->uploadPicture($params);
        $review = $this->update($params, $id);
        
        // Обновляем рейтинг
        $ratingId = $this->model::find($id)->ratings->where('ratingable_id', '=', $id)->first()->id;
        $this->model->updateRating($ratingId, ['rating' => $params['rating']]);
        
        return $review;
    }
    
    /**
     * @inheritDoc
     */
    public function deleteReview(int $id): bool
    {
        // Находим рейтинг и удаляем его.
        $ratingId = $this->find($id)->ratings->where('ratingable_id', '=', $id)->first()->id;
        $this->model->deleteRating($ratingId);
        
        return $this->delete($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return array
     */
    private function uploadPicture(array $params): array
    {
        $photosPaths = array();
        
        if (isset($params['photos'])) {
            foreach ($params['photos'] as $key => $photo) {
                $photosPaths[$key]['src'] = Storage::put(
                    "reviews/{$params['product_id']}/{$params['user_id']}",
                    $photo
                );
            }
        } else {
            return [];
        }
        
        return $photosPaths;
    }
}
