<?php


namespace Modules\Reviews\Services;


use App\DataTables\ReviewsDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Product\Contracts\ProductContract;
use Modules\Reviews\Contracts\ReviewContract;
use Modules\Reviews\Http\Requests\CreateReviewRequest;
use Modules\Reviews\Http\Requests\UpdateReviewRequest;
use Modules\User\Contracts\UserContract;

/**
 * Class ReviewWebService
 *
 * @package Modules\Reviews\Services
 */
class ReviewWebService extends BaseService
{
    /** @var  ReviewContract */
    private $reviewRepository;
    
    /**
     * @var ProductContract
     */
    private $productRepository;
    
    /**
     * @var UserContract
     */
    private $userRepository;
    
    /**
     * ReviewController constructor.
     *
     * @param  ReviewContract   $reviewRepo
     * @param  ProductContract  $productRepo
     * @param  UserContract     $userRepo
     */
    public function __construct(
        ReviewContract $reviewRepo,
        ProductContract $productRepo,
        UserContract $userRepo
    ) {
        $this->reviewRepository = $reviewRepo;
        $this->productRepository = $productRepo;
        $this->userRepository = $userRepo;
    }
	
	/**
	 * Display a listing of the Review.
	 *
	 * @param  \App\DataTables\ReviewsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(ReviewsDataTable $dataTable)
    {
        $this->setPageTitle(
            __('reviews::messages.title'),
            __('reviews::messages.index_subtitle'),
            __('reviews::messages.index_leadtext')
        );
        return $dataTable->render('reviews::reviews.index');
    }
    
    /**
     * Show the form for creating a new Review.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $products = $this->productRepository->listProducts();
        $users = $this->userRepository->listUsers();
        $this->setPageTitle(
            __('reviews::messages.title'),
            __('reviews::messages.create_subtitle'),
            __('reviews::messages.create_leadtext')
        );
        return view('reviews::reviews.create', compact('products', 'users'));
    }
    
    /**
     * Store a newly created Review in storage.
     *
     * @param  CreateReviewRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateReviewRequest $request): RedirectResponse
    {
        $review = $this->reviewRepository->createReview($request->all());
        
        if (!$review) {
            return $this->responseRedirectBack(
                __('reviews::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.reviews.index',
            __('reviews::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Display the specified Review.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        $review = $this->reviewRepository->findReviewById($id);
        $products = $this->productRepository->listProducts();
        $targetProduct = $review->product->id;
        $targetUser = $this->userRepository->findUserById($review->user->id);
        $users = $this->userRepository->listUsers();
        
        $this->setPageTitle(
            __('reviews::messages.title'),
            __('reviews::messages.show_subtitle', ['id' => $review->id]),
            __('reviews::messages.show_leadtext', ['id' => $review->id])
        );
        return view('reviews::reviews.show', compact('review',
            'products', 'targetProduct', 'targetUser', 'users'));
    }
    
    /**
     * Show the form for editing the specified Review.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        $review = $this->reviewRepository->findReviewById($id);
        $products = $this->productRepository->listProducts();
        $targetProduct = $review->product->id;
        $targetUser = $this->userRepository->findUserById($review->user->id);
        $users = $this->userRepository->listUsers();
        
        $this->setPageTitle(
            __('reviews::messages.title'),
            __('reviews::messages.edit_subtitle', ['id' => $id]),
            __('reviews::messages.edit_leadtext', ['id' => $id])
        );
        return view('reviews::reviews.edit', compact('review',
            'products', 'targetProduct', 'targetUser', 'users'));
    }
    
    /**
     * Update the specified Review in storage.
     *
     * @param  int                  $id
     * @param  UpdateReviewRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateReviewRequest $request): RedirectResponse
    {
        $review = $this->reviewRepository->updateReview($request->all(), $id);
        
        if (!$review) {
            return $this->responseRedirectBack(
                __('reviews::messages.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        return $this->responseRedirect(
            'admin.reviews.index',
            __('reviews::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified Review from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        $review = $this->reviewRepository->deleteReview($id);
        
        if (!$review) {
            return $this->responseRedirectBack(
                __('reviews::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        return $this->responseRedirect(
            'admin.reviews.index',
            __('reviews::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
}
