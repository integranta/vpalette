<?php


namespace Modules\Reviews\Services\API\V1;


use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Reviews\Contracts\ReviewContract;
use Modules\Reviews\Http\Requests\API\V1\CreateReviewAPIRequest;
use Modules\Reviews\Http\Requests\API\V1\UpdateReviewAPIRequest;
use Modules\Reviews\Transformers\ReviewsResource;

/**
 * Class ReviewApiService
 *
 * @package Modules\Reviews\Services\API\V1
 */
class ReviewApiService extends BaseService
{
	/** @var  ReviewContract */
	private $reviewRepository;
	
	/**
	 * ReviewAPIController constructor.
	 *
	 * @param  ReviewContract  $reviewRepo
	 */
	public function __construct(ReviewContract $reviewRepo)
	{
		$this->reviewRepository = $reviewRepo;
	}
	
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function index(Request $request): JsonResponse
	{
		$reviews = ReviewsResource::collection($this->reviewRepository->listReviews());
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('reviews::messages.success_retrieve'),
			$reviews
		);
	}
	
	/**
	 * @param  CreateReviewAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function store(CreateReviewAPIRequest $request): JsonResponse
	{
		$review = $this->reviewRepository->createReview($request->all());
		
		if (!$review) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('reviews::messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('reviews::messages.success_create'),
			$review
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function show(int $id): JsonResponse
	{
		$review = ReviewsResource::make($this->reviewRepository->findReviewById($id));
		
		if ($review === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('reviews::messages.not_found', ['id' => $id])
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('reviews::messages.success_found', ['id' => $id]),
			$review
		);
	}
	
	/**
	 * @param  int                     $id
	 * @param  UpdateReviewAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function update(int $id, UpdateReviewAPIRequest $request): JsonResponse
	{
		$review = $this->reviewRepository->findReviewById($id);
		
		if ($review === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('reviews::messages.not_found', ['id' => $id])
			);
		}
		
		$review = $this->reviewRepository->updateReview($request->all(), $id);
		
		if (!$review) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('reviews::messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('reviews::messages.success_update'),
			$review
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function destroy(int $id): JsonResponse
	{
		$review = $this->reviewRepository->findReviewById($id);
		
		if ($review === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('reviews::messages.not_found', ['id' => $id])
			);
		}
		
		$review = $this->reviewRepository->deleteReview($id);
		
		if (!$review) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('reviews::messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('reviews::messages.success_delete'),
			$review
		);
	}
}
