<?php

namespace Modules\Reviews\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Reviews\Models\Review;

/**
 * Class ReviewsDatabaseSeeder
 *
 * @package Modules\Reviews\Database\Seeders
 */
class ReviewsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Review::class, 20)->create();
    }
}
