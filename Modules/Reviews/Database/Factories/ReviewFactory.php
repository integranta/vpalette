<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Reviews\Models\Review;

$factory->define(Review::class,
    function (Faker $faker) {
        
        return [
            'active' => true,
            'sort' => $faker->numberBetween(1, 500),
            'virtues' => $faker->realText(150, 1),
            'limitations' => $faker->realText(100, 1),
            'comments' => $faker->text(350),
            'make_anonymous' => $faker->numberBetween(0, 1),
            'user_id' => factory(\App\User::class),
            'product_id' => $faker->numberBetween(1, 20),
            'rating' => $faker->numberBetween(1, 5)
        ];
    });
