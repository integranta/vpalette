<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Username Field -->
        <div class="form-group row col-md-12">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                for="user_id"
            >
                Автор:
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="user_id"
                    class="form-control"
                    name="user_id"
                    disabled
                >
                    <option value="0">Выберите пользователя</option>
                    @foreach($users as $user)
                        @if ($targetUser->id == $user->id)
                            <option value="{{ $user->id }}" selected> {{ $user->name }} </option>
                        @else
                            <option value="{{ $user->id }}"> {{ $user->name }} </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <!-- End Username Field -->


        <!-- Rating Field -->
        <div class="form-group row col-sm-12">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                for="rating"
            >
                Оценка:
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="rating"
                    class="form-control"
                    name="rating"
                    required
                    disabled
                >
                    <option value="0">Выберите оценку</option>
                    @foreach(range(1, 5) as $id => $rate)
                        @if (ceil($review->rating) == $rate)
                            <option value="{{ $id + 1 }}" selected> {{ $rate }} </option>
                        @else
                            <option value="{{ $id + 1 }}"> {{ $rate }} </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <!-- Anonymous field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Оставить анонимным:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('make_anonymous', 0) !!}
                {!!
                    Form::checkbox(
                        'make_anonymous',
                        true,
                        $review->make_anonymous === true ? true : false,
                        ['class' => 'custom-control-input', 'id' => 'make_anonymous']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="make_anonymous"></label>
            </div>
        </div>
        <!-- End Anonymous Field -->
    </div>
    <div class="tab-pane fade show" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <!-- Virtues Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'virtues',
                    'Преимущества:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea
                    (
                        'virtues',
                        $review->virtues,
                        ['class' => 'form-control', 'id' => 'virtues']
                    )
                !!}
            </div>
        </div>


        <!-- Limitations Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'limitations',
                    'Недостатки:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea
                    (
                        'limitations',
                        $review->limitations,
                        ['class' => 'form-control', 'id' => 'limitations']
                    )
                !!}
            </div>
        </div>


        <!-- Comments Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'comments',
                    'Комментарий:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea
                    (
                        'comments',
                        $review->comments,
                        ['class' => 'form-control', 'id' => 'comments']
                    )
                !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
        <!-- Images Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'photos',
                    'Фотографии:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                <div class="gallery">
                    @foreach($review->photos as $photo)
                        <div
                            class="gallery-item"
                            data-image="{{ asset($photo['src']) }}"
                            data-title="Image 1"
                            href="{{ asset($photo['src']) }}"
                            title="Image 1"
                            style="background-image: url({{ asset($photo['src']) }});"
                        >
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade show" id="detail" role="tabpanel" aria-labelledby="detail-tab">
        <!-- Product_id Field -->
        <div class="form-group row col-md-12 mb-4">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
                for="product_id">Товар <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="product_id"
                    class="form-control"
                    name="product_id"
                    disabled
                >
                    <option value="0">Выберите товар</option>
                    @foreach($products as $product)
                        @if ($targetProduct == $product->id)
                            <option value="{{ $product->id }}" selected> {{ $product->name }} </option>
                        @else
                            <option value="{{ $product->id }}"> {{ $product->name }} </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <!-- End Product_Id Field -->
    </div>
</div>
