@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/reviews.css') }}">

@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.reviews.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>

            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')

                    {!! Form::model($review,
                        [
                            'route' => ['admin.reviews.show', $review->id],
                            'method' => 'get',
                            'files' => 'true'
                        ])
                    !!}
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="element-tab" data-toggle="tab" href="#element"
                                       role="tab"
                                       aria-controls="element" aria-selected="true">Элемент</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="preview-tab" data-toggle="tab" href="#preview" role="tab"
                                       aria-controls="description" aria-selected="false">Отзыв</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab"
                                       aria-controls="images" aria-selected="false">Картинки</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab"
                                       aria-controls="detail" aria-selected="false">Товар</a>
                                </li>
                            </ul>
                            @include('reviews::reviews.show_fields')
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    {{-- JS Libraies --}}
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('libs/js/tinymce/langs/ru.js') }}"></script>
    {{--  Module JS File  --}}
    <script src="{{ asset('js/reviews-show.js') }}"></script>
@endpush
