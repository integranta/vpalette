import { tinyMCEConfiguration } from "../../../../../resources/js/shared/tinyMCEConfig";

$(document).ready(function () {
    tinymce.init({...tinyMCEConfiguration, selector: "textarea#virtues"});
    tinymce.init({...tinyMCEConfiguration, selector: "textarea#limitations"});
    tinymce.init({...tinyMCEConfiguration, selector: "textarea#comments"});
});
