<?php

return [
    'name' => 'Reviews',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all reviews as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('REVIEWS_CACHE_KEY','index_reviews'),
    'cache_ttl' => env('REVIEWS_CACHE_TTL',360000),
];
