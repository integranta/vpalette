<?php

namespace Modules\Reviews\Models;

use App\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Product\Models\Product;
use Modules\Rating\Traits\Ratingable;
use Modules\Reaction\Traits\Dislikeable;
use Modules\Reaction\Traits\Likeable;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Review
 *
 * @package Modules\Reviews\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property int $userId
 * @property int $productId
 * @property string|null $virtues
 * @property string|null $limitations
 * @property string|null $comments
 * @property bool $makeAnonymous
 * @property array|null $photos
 * @property float $rating
 * @property string|null $username
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Modules\Reaction\Models\DislikeCounter|null $dislikeCounter
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Reaction\Models\Dislike[] $dislikes
 * @property-read int|null $dislikesCount
 * @property-read float|null $avgRating
 * @property-read int $dislikeCount
 * @property-read bool $disliked
 * @property-read int $likeCount
 * @property-read bool $liked
 * @property-read float|null $maxRating
 * @property-read float|null $minRating
 * @property-read float|int|null $percentRating
 * @property-read float|null $sumRating
 * @property-read \Modules\Reaction\Models\LikeCounter|null $likeCounter
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Reaction\Models\Like[] $likes
 * @property-read int|null $likesCount
 * @property-read \Modules\Product\Models\Product $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Rating\Models\Rating[] $ratings
 * @property-read int|null $ratingsCount
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Reviews\Models\Review onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereDislikedBy($userId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereLikedBy($userId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereLimitations($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereMakeAnonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review wherePhotos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reviews\Models\Review whereVirtues($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Reviews\Models\Review withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Reviews\Models\Review withoutTrashed()
 * @mixin \Eloquent
 * @property-read mixed $hasDislike
 * @property-read mixed $hasLike
 */
class Review extends Model
{
    use SoftDeletes;
    use Ratingable;
    use Likeable;
    use Dislikeable;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'reviews';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'user_id',
        'product_id',
        'virtues',
        'limitations',
        'comments',
        'make_anonymous',
        'photos',
        'rating',
        'username',
        'total_count_likes',
        'total_count_dislikes',
		'has_like',
		'has_dislike'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'make_anonymous' => 'boolean',
        'photos' => 'array',
        'total_count_likes' => 'integer',
        'total_count_dislikes' => 'integer',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'nullable|exists:users,id',
        'product_id' => 'required|exists:products,id',
        'virtues' => 'nullable|string',
        'limitations' => 'nullable|string',
        'comments' => 'nullable|string',
        'make_anonymous' => 'boolean',
        'photos' => 'nullable|',
        'username' => 'string',
        'total_count_likes' => 'integer|min:0',
        'total_count_dislikes' => 'integer|min:0',
        'active' => 'required|boolean',
        'sort' => 'required|integer'
    ];

    /**
     * Отношение, которые всегда должны быть загружены.
     *
     * @var array
     */
    protected $with = [
        'product'
    ];
	
	protected $appends = [
		'has_like',
		'has_dislike'
	];
	
	public function getHasLikeAttribute()
	{
		return $this->liked;
	}
	
	public function getHasDislikeAttribute()
	{
		return $this->disliked;
	}

    /**
     * Установить флаг "Сделать анонимным" отзыв.
     *
     * @param $value
     *
     * @return void
     */
    public function setMakeAnonymousAttribute($value): void
    {
        $this->attributes['make_anonymous'] = $value === "1";
    }


    /***
     * Получить автора отзыва.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получить товар, на который был сделан отзыв.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

}
