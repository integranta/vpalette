const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/reviews.js')
    .js(__dirname + '/Resources/Assets/js/detail.js', 'js/reviews-detail.js')
    .js(__dirname + '/Resources/Assets/js/show.js', 'js/reviews-show.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/reviews.css', {
        implementation: require('node-sass')
    });

if (mix.inProduction()) {
    mix.version();
}
