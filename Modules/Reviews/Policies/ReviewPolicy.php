<?php

namespace Modules\Reviews\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Reviews\Models\Review;

/**
 * Class ReviewPolicy
 *
 * @package Modules\Reviews\Policies
 */
class ReviewPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the review.
     *
     * @param  User|null  $user
     * @param  Review     $review
     *
     * @return mixed
     */
    public function view(?User $user, Review $review)
    {
        if ($review->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished reviews')) {
            return true;
        }
        
        // authors can view their own unpublished reviews
        return $user->id == $review->user_id;
    }
    
    /**
     * Determine whether the user can create reviews.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create reviews')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the review.
     *
     * @param  User    $user
     * @param  Review  $review
     *
     * @return mixed
     */
    public function update(User $user, Review $review)
    {
        if ($user->can('edit own reviews')) {
            return $user->id == $review->user_id;
        }
        
        if ($user->can('edit all reviews')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the review.
     *
     * @param  User    $user
     * @param  Review  $review
     *
     * @return mixed
     */
    public function delete(User $user, Review $review)
    {
        if ($user->can('delete own reviews')) {
            return $user->id == $review->user_id;
        }
        
        if ($user->can('delete any review')) {
            return true;
        }
        
        return false;
    }
}
