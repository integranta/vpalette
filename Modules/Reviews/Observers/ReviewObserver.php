<?php

namespace Modules\Reviews\Observers;

use App\Traits\Cacheable;
use Modules\Reviews\Models\Review;

/**
 * Class ReviewObserver
 *
 * @package Modules\Reviews\Observers
 */
class ReviewObserver
{
    use Cacheable;
    
    /**
     * Handle the review "created" event.
     *
     * @param  Review  $review
     *
     * @return void
     */
    public function created(Review $review): void
    {
        if (filter_var($review->make_anonymous, FILTER_VALIDATE_BOOLEAN)) {
            $review->update([
                'username' => 'Анонимно'
            ]);
        }
        
        $this->incrementCountItemsInCache($review, config('reviews.cache_key'));
    }
    
    /**
     * Handle the review "updated" event.
     *
     * @param  Review  $review
     *
     * @return void
     */
    public function updated(Review $review): void
    {
        if (filter_var($review->make_anonymous, FILTER_VALIDATE_BOOLEAN)) {
            $review->update([
                'username' => 'Анонимно'
            ]);
        }
        
        $review->update([
            'username' => $review->user()->name
        ]);
    }
    
    /**
     * Handle the review "deleted" event.
     *
     * @param  Review  $review
     *
     * @return void
     */
    public function deleted(Review $review): void
    {
        $this->decrementCountItemsInCache($review, config('reviews.cache_key'));
    }
    
    /**
     * Handle the review "restored" event.
     *
     * @param  Review  $review
     *
     * @return void
     */
    public function restored(Review $review): void
    {
        $this->incrementCountItemsInCache($review, config('reviews.cache_key'));
    }
    
    /**
     * Handle the review "force deleted" event.
     *
     * @param  Review  $review
     *
     * @return void
     */
    public function forceDeleted(Review $review): void
    {
        $this->decrementCountItemsInCache($review, config('reviews.cache_key'));
    }
}
