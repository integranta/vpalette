<?php

namespace Modules\Reviews\Http\Controllers;

use App\DataTables\ReviewsDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Reviews\Http\Requests\CreateReviewRequest;
use Modules\Reviews\Http\Requests\UpdateReviewRequest;
use Modules\Reviews\Services\ReviewWebService;

/**
 * Class ReviewController
 *
 * @package Modules\Reviews\Http\Controllers
 */
class ReviewController extends Controller
{
    /** @var ReviewWebService */
    private $reviewWebService;

    /**
     * ReviewController constructor.
     *
     * @param  ReviewWebService  $service
     */
    public function __construct(
        ReviewWebService $service
    ) {
        $this->reviewWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Review.
	 *
	 * @param  \App\DataTables\ReviewsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(ReviewsDataTable $dataTable)
    {
        return $this->reviewWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Review.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->reviewWebService->create();
    }

    /**
     * Store a newly created Review in storage.
     *
     * @param  CreateReviewRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateReviewRequest $request): RedirectResponse
    {
        return $this->reviewWebService->store($request);
    }

    /**
     * Display the specified Review.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->reviewWebService->show($id);
    }

    /**
     * Show the form for editing the specified Review.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->reviewWebService->edit($id);
    }

    /**
     * Update the specified Review in storage.
     *
     * @param  int                  $id
     * @param  UpdateReviewRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateReviewRequest $request): RedirectResponse
    {
        return $this->reviewWebService->update($id, $request);
    }

    /**
     * Remove the specified Review from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->reviewWebService->destroy($id);
    }
}
