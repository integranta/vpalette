<?php

namespace Modules\Reviews\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Reviews\Http\Requests\API\V1\CreateReviewAPIRequest;
use Modules\Reviews\Http\Requests\API\V1\UpdateReviewAPIRequest;
use Modules\Reviews\Services\API\V1\ReviewApiService;

/**
 * Class ReviewController
 *
 * @package Modules\Reviews\Http\Controllers\API\V1
 */
class ReviewAPIController extends Controller
{
    /** @var  ReviewApiService */
    private $reviewApiService;

    /**
     * ReviewAPIController constructor.
     *
     * @param  ReviewApiService  $service
     */
    public function __construct(ReviewApiService $service)
    {
        $this->reviewApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }


    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->reviewApiService->index($request);
    }

    /**
     * @param  CreateReviewAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateReviewAPIRequest $request): JsonResponse
    {
        return $this->reviewApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->reviewApiService->show($id);
    }

    /**
     * @param                          $id
     * @param  UpdateReviewAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateReviewAPIRequest $request): JsonResponse
    {
        return $this->reviewApiService->update($id, $request);
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->reviewApiService->destroy($id);
    }
}
