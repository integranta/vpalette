<?php

namespace Modules\Reviews\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Reviews\Models\Review;

/**
 * Class UpdateReviewRequest
 *
 * @package Modules\Reviews\Http\Requests
 */
class UpdateReviewRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Review::$rules;
    }
}
