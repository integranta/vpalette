<?php


namespace Modules\PartnerShop\Repositories;


use App\Repositories\BaseRepository;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\PartnerShop\Contracts\PartnerShopContract;
use Modules\PartnerShop\Models\PartnerShop;
use Str;

/**
 * Class PartnerShopRepository
 *
 * @package Modules\PartnerShop\Repositories
 */
class PartnerShopRepository extends BaseRepository implements PartnerShopContract
{
    
    /**
     * PartnerShopRepository constructor.
     *
     * @param  PartnerShop  $model
     */
    public function __construct(PartnerShop $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listPartnerShops(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('partnershop.cache_key').$postfix,
            config('partnershop.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->model::with(['partner.user:id,name'])
                    ->orderBy($order, $sort)
                    ->get($columns);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return PartnerShop
     */
    public function findPartnerShopById(int $id): ?PartnerShop
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return PartnerShop
     */
    public function createPartnerShop(array $params): PartnerShop
    {
        return $this->create($params);
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return PartnerShop
     */
    public function updatePartnerShop(array $params, int $id): PartnerShop
    {
        return $this->update($params, $id);
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deletePartnerShop(int $id): bool
    {
        return $this->delete($id);
    }
}
