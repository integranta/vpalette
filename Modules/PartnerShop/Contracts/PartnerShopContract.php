<?php


namespace Modules\PartnerShop\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Interface PartnerShopContract
 *
 * @package Modules\PartnerShop\Contract
 */
interface PartnerShopContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listPartnerShops(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return PartnerShop
     */
    public function findPartnerShopById(int $id): ?PartnerShop;
    
    /**
     * @param  array  $params
     *
     * @return PartnerShop
     */
    public function createPartnerShop(array $params): PartnerShop;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return PartnerShop
     */
    public function updatePartnerShop(array $params, int $id): PartnerShop;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deletePartnerShop(int $id): bool;
}
