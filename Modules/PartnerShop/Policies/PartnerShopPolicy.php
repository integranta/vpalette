<?php

namespace Modules\PartnerShop\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Class PartnerShopPolicy
 *
 * @package Modules\PartnerShop\Policies
 */
class PartnerShopPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the partner shop.
     *
     * @param  User|null    $user
     * @param  PartnerShop  $partnerShop
     *
     * @return mixed
     */
    public function view(?User $user, PartnerShop $partnerShop)
    {
        if ($partnerShop->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished partner shops')) {
            return true;
        }
        
        // authors can view their own unpublished partner shops
        return $user->id == $user->is($partnerShop->partner->user);
    }
    
    /**
     * Determine whether the user can create partner shops.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create partner shops')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the partner shop.
     *
     * @param  User         $user
     * @param  PartnerShop  $partnerShop
     *
     * @return mixed
     */
    public function update(User $user, PartnerShop $partnerShop)
    {
        if ($user->can('edit own partner shops')) {
            return $user->id == $user->is($partnerShop->partner->user);
        }
        
        if ($user->can('edit all partner shops')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the partner shop.
     *
     * @param  User         $user
     * @param  PartnerShop  $partnerShop
     *
     * @return mixed
     */
    public function delete(User $user, PartnerShop $partnerShop)
    {
        if ($user->can('delete own partner shops')) {
            return $user->id == $user->is($partnerShop->partner->user);
        }
        
        if ($user->can('delete any partner shop')) {
            return true;
        }
        
        return false;
    }
}
