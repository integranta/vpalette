<?php


namespace Modules\PartnerShop\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\PartnerShop\Contracts\PartnerShopContract;
use Modules\PartnerShop\Http\Requests\API\V1\CreatePartnerShopAPIRequest;
use Modules\PartnerShop\Http\Requests\API\V1\UpdatePartnerShopAPIRequest;
use Modules\PartnerShop\Transformers\PartnerShopResource;

/**
 * Class PartnerShopApiService
 *
 * @package Modules\PartnerShop\Services\API\V1
 */
class PartnerShopApiService extends BaseService
{
    /** @var PartnerShopContract */
    private $partnerShopRepository;
    
    /**
     * PartnerShopAPIController constructor.
     *
     * @param  PartnerShopContract  $partnerShopRepo
     */
    public function __construct(PartnerShopContract $partnerShopRepo)
    {
        $this->partnerShopRepository = $partnerShopRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $partnerShops = PartnerShopResource::collection($this->partnerShopRepository->listPartnerShops());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('partnershop::messages.success_retrieve'),
            $partnerShops
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePartnerShopAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreatePartnerShopAPIRequest $request): JsonResponse
    {
        $partnerShop = $this->partnerShopRepository->createPartnerShop($request->all());
        
        if (!$partnerShop) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('partnershop::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('partnershop::messages.success_create'),
            $partnerShop
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $partnerShop = PartnerShopResource::make($this->partnerShopRepository->findPartnerShopById($id));
        
        if ($partnerShop === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('partnershop::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('partnershop::messages.success_found', ['id' => $id]),
            $partnerShop
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePartnerShopAPIRequest  $request
     * @param  int                          $id
     *
     * @return JsonResponse
     */
    public function update(UpdatePartnerShopAPIRequest $request, int $id): JsonResponse
    {
        $partnerShop = $this->partnerShopRepository->findPartnerShopById($id);
        
        if ($partnerShop === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('partnershop::messages.not_found', ['id' => $id])
            );
        }
        
        $partnerShop = $this->partnerShopRepository->updatePartnerShop($request->all(), $id);
        
        if (!$partnerShop) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('partnershop::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('partnershop::messages.success_update'),
            $partnerShop
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $partnerShop = $this->partnerShopRepository->findPartnerShopById($id);
        
        if ($partnerShop === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('partnershop::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('partnershop::messages.success_delete'),
            $partnerShop
        );
    }
}
