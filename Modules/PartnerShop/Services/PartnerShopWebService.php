<?php


namespace Modules\PartnerShop\Services;


use App\DataTables\PartnerShopsDataTable;
use App\Services\BaseService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Partner\Contracts\PartnerContract;
use Modules\PartnerShop\Contracts\PartnerShopContract;
use Modules\PartnerShop\Http\Requests\CreatePartnerShopRequest;
use Modules\PartnerShop\Http\Requests\UpdatePartnerShopRequest;

/**
 * Class PartnerShopWebService
 *
 * @package Modules\PartnerShop\Services
 */
class PartnerShopWebService extends BaseService
{
	/** @var PartnerShopContract */
	private $partnerShopRepository;
	
	/** @var PartnerContract */
	private $partnerRepository;
	
	/**
	 * PartnerShopController constructor.
	 *
	 * @param  PartnerShopContract  $partnerShopRepo
	 * @param  PartnerContract      $partnerRepo
	 *
	 * @return void
	 */
	public function __construct(
		PartnerShopContract $partnerShopRepo,
		PartnerContract $partnerRepo
	) {
		$this->partnerShopRepository = $partnerShopRepo;
		$this->partnerRepository = $partnerRepo;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\PartnerShopsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(PartnerShopsDataTable $dataTable)
	{
		$this->setPageTitle(
			__('partnershop::messages.title'),
			__('partnershop::messages.index_subtitle'),
			__('partnershop::messages.index_leadtext')
		);
		
		return $dataTable->render('partnershop::partnershops.index');
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$owners = $this->partnerRepository->listPartners();
		
		$this->setPageTitle(
			__('partnershop::messages.title'),
			__('partnershop::messages.create_subtitle'),
			__('partnershop::messages.create_leadtext')
		);
		
		return view('partnershop::partnershops.create', compact('owners'));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  CreatePartnerShopRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreatePartnerShopRequest $request): RedirectResponse
	{
		$partnerShop = $this->partnerShopRepository->createPartnerShop($request->all());
		
		if (!$partnerShop) {
			return $this->responseRedirectBack(
				__('partnershop::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partner-shop.index',
			__('partnershop::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(int $id): View
	{
		$partnerShop = $this->partnerShopRepository->findPartnerShopById($id);
		
		if ($partnerShop === null) {
			return $this->responseRedirect(
				'admin.partner-shop.index',
				__('partnershop::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		$owners = $this->partnerRepository->listPartners();
		$targetOwner = $this->partnerRepository->findPartnerById($partnerShop->partner->id);
		
		$this->setPageTitle(
			__('partnershop::messages.title'),
			__('partnershop::messages.show_subtitle', ['name' => $partnerShop->shop_name]),
			__('partnershop::messages.show_leadtext', ['name' => $partnerShop->shop_name])
		);
		
		return view('partnershop::partnershops.show', compact(
				'partnerShop',
				'owners',
				'targetOwner'
			)
		);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$partnerShop = $this->partnerShopRepository->findPartnerShopById($id);
		
		if ($partnerShop === null) {
			return $this->responseRedirect(
				'admin.partner-shop.index',
				__('partnershop::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$owners = $this->partnerRepository->listPartners();
		$targetOwner = $this->partnerRepository->findPartnerById($partnerShop->partner->id);
		
		$this->setPageTitle(
			__('partnershop::messages.title'),
			__('partnershop::messages.edit_subtitle', ['name' => $partnerShop->shop_name]),
			__('partnershop::messages.edit_leadtext', ['name' => $partnerShop->shop_name])
		);
		
		return view('partnershop::partnershops.edit', compact(
				'partnerShop',
				'owners',
				'targetOwner'
			)
		);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  UpdatePartnerShopRequest  $request
	 * @param  int                       $id
	 *
	 * @return RedirectResponse
	 */
	public function update(UpdatePartnerShopRequest $request, int $id): RedirectResponse
	{
		$partnerShop = $this->partnerShopRepository->updatePartnerShop($request->all(), $id);
		
		if (!$partnerShop) {
			return $this->responseRedirectBack(
				__('partnershop::messages.error_edit'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partner-shop.index',
			__('partnershop::messages.success_edit'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 */
	public function destroy(int $id): RedirectResponse
	{
		$partnerShop = $this->partnerShopRepository->deletePartnerShop($id);
		
		if (!$partnerShop) {
			return $this->responseRedirectBack(
				__('partnershop::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partner-shop.index',
			__('partnershop::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}