<!-- Start shop_name field -->
<div class="form-group row mb-4">
    <label
        for="shop_name"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('partnershop::templates.shop_name') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            name="shop_name"
            id="shop_name"
            type="text"
            value="{{ $partnerShop->shop_name }}"
            class="form-control"
            disabled
        >
    </div>
</div>
<!-- End shop_name field -->

<!-- Start law_shop_name field -->
<div class="form-group row mb-4">
    <label
        for="law_shop_name"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('partnershop::templates.law_shop_name') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            id="law_shop_name"
            name="law_shop_name"
            type="text"
            value="{{ $partnerShop->law_shop_name }}"
            class="form-control"
            disabled
        >
    </div>
</div>
<!-- End law_shop_name field -->

<!-- Start inn field -->
<div class="form-group row mb-4">
    <label
        for="inn"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('partnershop::templates.inn') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="inn"
            id="inn"
            class="form-control"
            value="{{ $partnerShop->inn }}"
            disabled
        >
    </div>
</div>
<!-- End inn field -->

<!-- Start kpp field -->
<div class="form-group row mb-4">
    <label
        for="kpp"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('partnershop::templates.kpp') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="kpp"
            id="kpp"
            class="form-control"
            value="{{ $partnerShop->kpp }}"
            disabled
        >
    </div>
</div>
<!-- End kpp field -->

<!-- Start city field -->
<div class="form-group row mb-4">
    <label
        for="city"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('partnershop::templates.city') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="city"
            id="city"
            class="form-control"
            value="{{ $partnerShop->city }}"
            disabled
        >
    </div>
</div>
<!-- End city field -->

<!-- Start address field -->
<div class="form-group row mb-4">
    <label
        for="address"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('partnershop::templates.address') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <textarea
            type="text"
            name="address"
            id="address"
            class="form-control"
            disabled
        >
            {{ $partnerShop->address }}
        </textarea>
    </div>
</div>
<!-- End address field -->

<!-- Start post_index field -->
<div class="form-group row mb-4">
    <label
        for="post_index"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('partnershop::templates.post_index') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="post_index"
            id="post_index"
            class="form-control"
            value="{{ $partnerShop->post_index }}"
            disabled
        >
    </div>
</div>
<!-- End post_index field -->

<!-- Start owner (partner_id) field -->
<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="partner_id">
        {{ __('partnershop::templates.owner') }}
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="partner_id"
            class="form-control"
            name="partner_id"
            disabled
        >
            <option value="0" disabled>{{ __('partnershop::templates.select_owner') }}</option>
            @foreach($owners as $owner)
                @if ($targetOwner->id === $owner->id)
                    <option value="{{ $owner->id }}" selected>
                        {{ $owner->user->name }}
                    </option>
                @else
                    <option value="{{ $owner->id }}">
                        {{ $owner->user->name }}
                    </option>
                @endif
            @endforeach
        </select>
    </div>
</div>
<!-- End owner (partner_id) field -->
