<?php

namespace Modules\PartnerShop\Observers;

use App\Traits\Cacheable;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Class PartnerShopObserver
 *
 * @package Modules\PartnerShop\Observers
 */
class PartnerShopObserver
{
    use Cacheable;
    
    /**
     * Handle the partner shop "created" event.
     *
     * @param  PartnerShop  $partnerShop
     *
     * @return void
     */
    public function created(PartnerShop $partnerShop): void
    {
        $this->incrementCountItemsInCache($partnerShop, config('partnershop.cache_key'));
    }
    
    /**
     * Handle the partner shop "updated" event.
     *
     * @param  PartnerShop  $partnerShop
     *
     * @return void
     */
    public function updated(PartnerShop $partnerShop): void
    {
        //
    }
    
    /**
     * Handle the partner shop "deleted" event.
     *
     * @param  PartnerShop  $partnerShop
     *
     * @return void
     */
    public function deleted(PartnerShop $partnerShop): void
    {
        $this->incrementCountItemsInCache($partnerShop, config('partnershop.cache_key'));
    }
    
    /**
     * Handle the partner shop "restored" event.
     *
     * @param  PartnerShop  $partnerShop
     *
     * @return void
     */
    public function restored(PartnerShop $partnerShop): void
    {
        $this->incrementCountItemsInCache($partnerShop, config('partnershop.cache_key'));
    }
    
    /**
     * Handle the partner shop "force deleted" event.
     *
     * @param  PartnerShop  $partnerShop
     *
     * @return void
     */
    public function forceDeleted(PartnerShop $partnerShop): void
    {
        $this->incrementCountItemsInCache($partnerShop, config('partnershop.cache_key'));
    }
}
