<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreatePartnerShopsTable
 */
class CreatePartnerShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->string('shop_name');
            $table->string('law_shop_name');
            $table->unsignedBigInteger('inn')->unique();
            $table->unsignedBigInteger('kpp')->unique();
            $table->string('city');
            $table->string('address');
            $table->string('post_index');
            $table->unsignedBigInteger('partner_id');
            $table->foreign('partner_id')
                ->references('id')
                ->on('partners');
            $table->softDeletes();
            $table->timestamps();
            $table->index(['active', 'sort', 'inn', 'kpp', 'partner_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partner_shops');
    }
}
