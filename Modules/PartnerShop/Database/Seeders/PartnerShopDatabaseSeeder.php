<?php

namespace Modules\PartnerShop\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Class PartnerShopDatabaseSeeder
 *
 * @package Modules\PartnerShop\Database\Seeders
 */
class PartnerShopDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(PartnerShop::class, 10)->create();
    }
}
