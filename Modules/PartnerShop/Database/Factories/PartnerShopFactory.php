<?php


use Faker\Generator as Faker;
use Faker\Provider\ru_RU\Company;
use Illuminate\Database\Eloquent\Factory;
use Modules\Partner\Models\Partner;
use Modules\PartnerShop\Models\PartnerShop;

/** @var Factory $factory */
$factory->define(PartnerShop::class, function (Faker $faker) {
    $faker->addProvider(new Company($faker));
    return [
        'active' => true,
        'sort' => $faker->numberBetween(1, 500),
        'shop_name' => $faker->unique()->company,
        'law_shop_name' => "{$faker->companySuffix} {$faker->company}",
        'inn' => $faker->unique()->inn,
        'kpp' => $faker->unique()->kpp,
        'city' => $faker->unique()->city,
		'street' => $faker->streetName,
		'house' => $faker->buildingNumber,
        'address' => $faker->unique()->address,
        'post_index' => $faker->unique()->postcode,
        'partner_id' => factory(Partner::class)
    ];
});
