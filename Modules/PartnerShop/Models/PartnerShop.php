<?php

namespace Modules\PartnerShop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Partner\Models\Partner;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\Rating\Traits\Ratingable;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class PartnerShop
 *
 * @package Modules\PartnerShop\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $shopName
 * @property string $lawShopName
 * @property string $inn
 * @property string $kpp
 * @property string $city
 * @property string $address
 * @property string $postIndex
 * @property int $partnerId
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerOffer\Models\PartnerOffer[] $allowDiscountFromPortal
 * @property-read int|null $allowDiscountFromPortalCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerOffer\Models\PartnerOffer[] $disallowDiscountFromPortal
 * @property-read int|null $disallowDiscountFromPortalCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerOffer\Models\PartnerOffer[] $offers
 * @property-read int|null $offersCount
 * @property-read \Modules\Partner\Models\Partner $partner
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerShop\Models\PartnerShop onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereKpp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereLawShopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop wherePartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop wherePostIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereShopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerShop\Models\PartnerShop withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerShop\Models\PartnerShop withoutTrashed()
 * @mixin \Eloquent
 * @property string $street
 * @property string $house
 * @property-read float|null $avgRating
 * @property-read float|null $maxRating
 * @property-read float|null $minRating
 * @property-read float|int|null $percentRating
 * @property-read float|null $sumRating
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Rating\Models\Rating[] $ratings
 * @property-read int|null $ratingsCount
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerShop\Models\PartnerShop whereStreet($value)
 */
class PartnerShop extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	use Ratingable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'partner_shops';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'shop_name',
        'law_shop_name',
        'inn',
        'kpp',
        'city',
		'street',
		'house',
        'address',
        'post_index',
        'partner_id'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'shop_name' => 'string',
        'law_shop_name' => 'string',
        'inn' => 'string',
        'kpp' => 'string',
        'city' => 'string',
        'address' => 'string',
		'street' => 'string',
		'house' => 'string',
        'post_index' => 'string',
        'partner_shop_id' => 'integer',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'shop_name' => 'required|string',
        'law_shop_name' => 'required|string',
        'inn' => 'required|string|min:12|max:12',
        'kpp' => 'required|string|min:9|max:9',
        'city' => 'required|string',
        'address' => 'required|string',
		'street' => 'required|string',
		'house' => 'required|string',
        'post_index' => 'required|integer',
        'partner_id' => 'required|integer',
        'active' => 'required|boolean',
        'sort' => 'required|integer'
    ];

    /**
     * Получить партнёра (владельца) этого магазина.
     *
     * @return BelongsTo
     */
    public function partner(): BelongsTo
    {
        return $this->belongsTo(Partner::class);
    }

    /**
     * Предложение, которые предлагает этот магазин.
     *
     * @return HasMany
     */
    public function offers(): HasMany
    {
        return $this->hasMany(PartnerOffer::class);
    }

    /**
     * Получить список предложений, на которых разрешено применение скидкок от портала.
     *
     * @return HasMany
     */
    public function allowDiscountFromPortal(): HasMany
    {
        return $this->hasMany(PartnerOffer::class)
            ->where('allow_discount_from_portal', '=', 1)
            ->orderByDesc('id');
    }

    /**
     * Получить список предложений, на которых не разрешено применение скидкок от портала.
     *
     * @return HasMany
     */
    public function disallowDiscountFromPortal(): HasMany
    {
        return $this->hasMany(PartnerOffer::class)
            ->where('allow_discount_from_portal', '=', 0)
            ->orderByDesc('id');
    }
}
