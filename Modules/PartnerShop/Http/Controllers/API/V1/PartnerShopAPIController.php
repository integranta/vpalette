<?php

namespace Modules\PartnerShop\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\PartnerShop\Http\Requests\API\V1\CreatePartnerShopAPIRequest;
use Modules\PartnerShop\Http\Requests\API\V1\UpdatePartnerShopAPIRequest;
use Modules\PartnerShop\Services\API\V1\PartnerShopApiService;

/**
 * Class PartnerShopAPIController
 *
 * @package Modules\PartnerShop\Http\Controllers\API\V1
 */
class PartnerShopAPIController extends Controller
{
    /** @var PartnerShopApiService */
    private $partnerShopApiService;

    /**
     * PartnerShopAPIController constructor.
     *
     * @param  PartnerShopApiService  $service
     */
    public function __construct(PartnerShopApiService $service)
    {
        $this->partnerShopApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->partnerShopApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePartnerShopAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreatePartnerShopAPIRequest $request): JsonResponse
    {
        return $this->partnerShopApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->partnerShopApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePartnerShopAPIRequest  $request
     * @param  int                          $id
     *
     * @return JsonResponse
     */
    public function update(UpdatePartnerShopAPIRequest $request, int $id): JsonResponse
    {
        return $this->partnerShopApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->partnerShopApiService->destroy($id);
    }
}
