<?php

namespace Modules\PartnerShop\Http\Controllers;

use App\DataTables\PartnerShopsDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\PartnerShop\Http\Requests\CreatePartnerShopRequest;
use Modules\PartnerShop\Http\Requests\UpdatePartnerShopRequest;
use Modules\PartnerShop\Services\PartnerShopWebService;

/**
 * Class PartnerShopController
 *
 * @package Modules\PartnerShop\Http\Controllers
 */
class PartnerShopController extends Controller
{
    /** @var PartnerShopWebService */
    private $partnerShopWebService;

    /**
     * PartnerShopController constructor.
     *
     * @param  PartnerShopWebService  $service
     */
    public function __construct(
        PartnerShopWebService $service
    ) {
        $this->partnerShopWebService = $service;
        $this->middleware('role:Owner|Admin|');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\PartnerShopsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(PartnerShopsDataTable $dataTable)
    {
        return $this->partnerShopWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->partnerShopWebService->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePartnerShopRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreatePartnerShopRequest $request): RedirectResponse
    {
        return $this->partnerShopWebService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->partnerShopWebService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->partnerShopWebService->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePartnerShopRequest  $request
     * @param  int                       $id
     *
     * @return RedirectResponse
     */
    public function update(UpdatePartnerShopRequest $request, int $id): RedirectResponse
    {
        return $this->partnerShopWebService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->partnerShopWebService->destroy($id);
    }
}
