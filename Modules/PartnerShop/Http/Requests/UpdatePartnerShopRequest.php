<?php

namespace Modules\PartnerShop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePartnerShopRequest
 *
 * @package Modules\PartnerShop\Http\Requests
 */
class UpdatePartnerShopRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
