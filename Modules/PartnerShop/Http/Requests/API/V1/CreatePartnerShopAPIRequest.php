<?php

namespace Modules\PartnerShop\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreatePartnerShopAPIRequest
 *
 * @package Modules\PartnerShop\Http\Requests\API\V1
 */
class CreatePartnerShopAPIRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
