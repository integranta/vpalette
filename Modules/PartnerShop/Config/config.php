<?php

return [
    'name' => 'PartnerShop',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all partner shops as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('PARTNER_SHOPS_CACHE_KEY','index_partner_shops'),
    'cache_ttl' => env('PARTNER_SHOPS_CACHE_TTL',360000),
];
