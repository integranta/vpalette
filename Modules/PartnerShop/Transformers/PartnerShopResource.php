<?php

namespace Modules\PartnerShop\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PartnerShopResource
 *
 * @package Modules\PartnerShop\Transformers
 */
class PartnerShopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
