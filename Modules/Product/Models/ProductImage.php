<?php

namespace Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Storage;

/**
 * Class ProductImage
 *
 * @package Modules\Product\Models
 * @property int $id
 * @property int|null $productId
 * @property null|string $full
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Modules\Product\Models\Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage whereFull($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductImage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProductImage extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'product_images';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'full'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer'
    ];


    /**
     * Получить ссылку на полный путь к картинке.
     *
     * @param  null|string  $value
     *
     * @return null|string
     */
    public function getFullAttribute(?string $value): ?string
    {
        return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
    }

    /**
     * Получить товар, которому принадлежит эта картинка.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
