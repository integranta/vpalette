<?php

namespace Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Attribute\Models\Attribute;
use Modules\Attribute\Models\AttributeValue;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class ProductAttribute
 *
 * @package Modules\Product\Models
 * @property int $id
 * @property int $attributeId
 * @property int $productId
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Modules\Attribute\Models\Attribute $attribute
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Attribute\Models\AttributeValue[] $attributesValues
 * @property-read int|null $attributesValuesCount
 * @property-read \Modules\Product\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\ProductAttribute whereValue($value)
 * @mixin \Eloquent
 */
class ProductAttribute extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'product_attributes';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'attribute_id',
        'value'
    ];

    /**
     * Получить товар, которому принадлежит этот атрибут.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Значение атрибутов, которые принадлежат этому товару.
     *
     * @return BelongsToMany
     */
    public function attributesValues(): BelongsToMany
    {
        return $this->belongsToMany(
        	AttributeValue::class,
			'product_attributes',
			'product_id',
			'attribute_id'
		);
    }

    /**
     * Получить атрибут, которой принадлежит этот товар.
     *
     * @return BelongsTo
     */
    public function attribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class);
    }
}
