<?php

namespace Modules\Product\Models;


use App\Enums\ProductType;
use App\Models\BaseModel;
use BenSampo\Enum\Traits\CastsEnums;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Brand\Models\Brand;
use Modules\Category\Models\Category;
use Modules\Discount\Models\Discount;
use Modules\Faq\Models\Faq;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerProduct\Models\PartnerProduct;
use Modules\Rating\Traits\Ratingable;
use Modules\Reviews\Models\Review;
use Modules\Seo\Models\Seo;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Product
 *
 * @package Modules\Product\Models
 * @property int $id
 * @property mixed $sections
 * @property string $article
 * @property string $barcode
 * @property string $name
 * @property string $slug
 * @property string $typeProduct
 * @property bool $isNew
 * @property string|null $fullPath
 * @property object|null $fullPaths
 * @property string|null $about
 * @property float|null $weight
 * @property int|null $minPrice
 * @property int $sort
 * @property bool $active
 * @property bool $isPopular
 * @property int $brandId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Models\ProductAttribute[] $attributes
 * @property-read int|null $attributesCount
 * @property-read \Modules\Brand\Models\Brand $brand
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Category\Models\Category[] $categories
 * @property-read int|null $categoriesCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Discount\Models\Discount[] $discounts
 * @property-read int|null $discountsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Faq\Models\Faq[] $faqs
 * @property-read int|null $faqsCount
 * @property-read float|null $avgRating
 * @property-read float|null $maxRating
 * @property-read float|null $minRating
 * @property-read float|int|null $percentRating
 * @property-read float|null $sumRating
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Models\ProductImage[] $images
 * @property-read int|null $imagesCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerOffer\Models\PartnerOffer[] $partnerOffers
 * @property-read int|null $partnerOffersCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerProduct\Models\PartnerProduct[] $partnerProducts
 * @property-read int|null $partnerProductsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Rating\Models\Rating[] $ratings
 * @property-read int|null $ratingsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Reviews\Models\Review[] $reviews
 * @property-read int|null $reviewsCount
 * @property-read \Modules\Seo\Models\Seo|null $seo
 * @property-write mixed $isActive
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Product\Models\Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereArticle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereFullPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereFullPaths($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereIsNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereIsPopular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereMinPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereSections($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereTypeProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Product\Models\Product whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Product\Models\Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Product\Models\Product withoutTrashed()
 * @mixin \Eloquent
 */
class Product extends BaseModel
{
    use SoftDeletes;
    use Ratingable;
    use CastsEnums;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'products';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Отношение, которые всегда должны быть загружены.
     *
     * @var array
     */
    protected $with = [];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'sections',
        'article',
        'barcode',
        'name',
        'slug',
        'about',
        'weight',
        'min_price',
        'sort',
        'type_product',
        'active',
        'is_popular',
        'is_new',
        'brand_id',
        'full_path',
        'full_paths'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'article' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'about' => 'string',
        'weight' => 'float',
        'barcode' => 'string',
        'active' => 'boolean',
        'is_popular' => 'boolean',
        'is_new' => 'boolean',
        'type_product' => 'string',
        'full_paths' => 'object'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'article' => 'required',
        'barcode' => 'nullable|required',
        'name' => 'required',
        'about' => 'nullable',
        'weight' => 'required',
        'active' => 'required|',
        'is_popular' => 'required|',
        'is_new' => 'required|'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'type_product' => ProductType::class,
    ];

    /**
     * Установить флаг активности для товара.
     *
     * @param $value
     *
     * @return bool
     */
    public function setIsActiveAttribute(string $value): bool
    {
        return $this->attributes['active'] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Установить флаг "Популярный" для товара.
     *
     * @param $value
     *
     * @return bool
     */
    public function setIsPopularAttribute(string $value): bool
    {
        return $this->attributes['is_popular'] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Установить флаг "Новинка" для товара.
     *
     * @param $value
     *
     * @return bool
     */
    public function setIsNewAttribute(string $value): bool
    {
        return $this->attributes['is_new'] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Установить вес для товара.
     *
     * @param  float  $value
     *
     * @return string
     */
    public function setWeightAttribute(float $value): string
    {
        return $this->attributes['weight'] = number_format($value, 2, '.', '');
    }

    /**
     * Получить бренд товара.
     *
     * @return BelongsTo
     */
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Получить картинки для этого товара.
     *
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * Получить атрибуты для этого товара.
     *
     * @return HasMany
     */
    public function attributes(): HasMany
    {
        return $this->hasMany(ProductAttribute::class);
    }

    /**
     * Найти товар по значению артикла.
     *
     * @param  string  $article
     *
     * @return Builder|Model|object|null
     */
    public function findByArticle(string $article): Model
    {
        return self::where('article', $article)->first();
    }

    /**
     * Найти товар по значению штрих-кода.
     *
     * @param  string  $barcode
     *
     * @return Builder|Model|object|null
     */
    public function findByBarcode(string $barcode): Model
    {
        return self::where('barcode', $barcode)->first();
    }

    /**
     * Категории, принадлежащие товару.
     *
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            Category::class,
            'category_product',
            'product_id',
            'category_id'
        )->withPivot(['category_id', 'product_id']);
    }

    /**
     * Получить связанные SEO параметры для товара.
     *
     * @return MorphOne
     */
    public function seo(): MorphOne
    {
        return $this->morphOne(Seo::class, 'seoble');
    }

    /**
     * Партнёрские предложение, принадлежащие товару.
     *
     * @return HasMany
     */
    public function partnerOffers(): HasMany
    {
        return $this->hasMany(PartnerOffer::class);
    }

    /**
     * Партнёрские предложенные товары, принадлежащие товару.
     *
     * @return HasMany
     */
    public function partnerProducts(): HasMany
    {
        return $this->hasMany(PartnerProduct::class);
    }

    /**
     * Скидки, принадлежащие товару.
     *
     * @return BelongsToMany
     */
    public function discounts(): BelongsToMany
    {
        return $this->belongsToMany(Discount::class, 'product_discount')
            ->withPivot('product_id', 'discount_id')
            ->withTimestamps();
    }

    /**
     * Получить вопросы и ответы для товара.
     *
     * @return HasMany
     */
    public function faqs(): HasMany
    {
        return $this->hasMany(Faq::class);
    }

    /**
     * Получить отзывы для товара.
     *
     * @return HasMany
     */
    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }
}
