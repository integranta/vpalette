<?php

namespace Modules\Product\Repositories;

use App\Enums\ProductType;
use App\Repositories\BaseRepository;
use Arr;
use Cache;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Models\Category;
use Modules\Product\Contracts\ProductContract;
use Modules\Product\Models\Product;
use Storage;
use Str;

/**
 * Class ProductRepository
 *
 * @package Modules\Product\Repositories
 * @version October 21, 2019, 11:37 am MSK
 */
class ProductRepository extends BaseRepository implements ProductContract
{
	/**
	 * @var array
	 */
	protected $seoData = [
		'article',
		'barcode',
		'name',
		'slug',
		'about',
		'weight',
		'sort',
		'is_popular',
		'active',
		'_method',
		'_token',
		'brand_id',
		'categories',
		'product_id'
	];
	
	/**
	 * @var array
	 */
	protected $productData = [
		'title',
		'description',
		'keywords',
		'canonical_url',
		'google_tag_manager',
		'google_analytics',
		'ya_direct',
		'ya_metrica',
		'_method',
		'_token',
		'categories',
		'product_id'
	];
	
	/**
	 * ProductRepository constructor.
	 *
	 * @param  Product  $model
	 */
	public function __construct(Product $model)
	{
		parent::__construct($model);
		$this->model = $model;
	}
	
	/**
	 * @param  array   $columns
	 * @param  string  $order
	 * @param  string  $sort
	 *
	 * @return Collection
	 */
	public function listProducts(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
	{
		$postfix = Str::of($this->getLastId() ? : 0)->start('_');
		
		return Cache::remember(
			config('product.products.cache_key').$postfix,
			config('product.products.cache_ttl'),
			function () use ($order, $sort, $columns) {
				return $this->model::with(['categories:name,parent_id,category_id,product_id', 'brand:id,name'])
					->orderBy($order, $sort)
					->get($columns);
			}
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return Product
	 */
	public function findProductById(int $id): ?Product
	{
		return $this->find($id);
	}
	
	/**
	 * @param  array  $params
	 *
	 * @return Product
	 */
	public function createProduct(array $params): Product
	{
		$fullImagePaths = array();
		
		if ($params['pictures'] !== null && isset($params['pictures'])) {
			foreach ($params['pictures'] as $key => $picture) {
				$fullImagePaths[$key]['full'] = Storage::disk('uploads')
					->put("products/{$params['name']}", $picture);
			}
		}
		
		$product = Product::create([
			'article'      => $params['article'],
			'barcode'      => $params['barcode'],
			'name'         => $params['name'],
			'about'        => $params['about'],
			'weight'       => $params['weight'],
			'sort'         => $params['sort'],
			'active'       => $params['active'],
			'is_popular'   => $params['is_popular'],
			'is_new'       => $params['is_new'],
			'brand_id'     => $params['brand_id'],
			'type_product' => ProductType::PORTAL_PRODUCT,
			'sections'     => json_encode($this->getCategoriesIds($params))
		]);
		$product->seo()->create([
			'title'              => $params['title'],
			'description'        => $params['description'],
			'keywords'           => $params['keywords'],
			'canonical_url'      => $params['canonical_url'],
			'google_tag_manager' => $params['google_tag_manager'],
			'ya_metrica'         => $params['ya_metrica'],
			'google_analytics'   => $params['google_analytics'],
			'ya_direct'          => $params['ya_direct']
		]);
		$product->images()->createMany($fullImagePaths);
		$product->save();
		
		$product->load('categories', 'images');
		
		$category = Category::find($this->getCategoriesIds($params));
		$catPath = $category->first()->full_path;
		
		$catPaths = [];
		
		foreach ($category as $cat) {
			$full = $cat->full_path.'/'.$product->slug;
			$catPaths[] = $full;
		}
		
		
		$product->categories()->attach($category);
		$product->save();
		
		$product->update([
			'full_path'  => $catPath.'/'.$product->slug,
			'full_paths' => $catPaths
		]);
		
		return $product;
	}
	
	/**
	 * @param  array  $params
	 *
	 * @return array
	 */
	private function getCategoriesIds(array $params): array
	{
		return Arr::get($params, 'categories');
	}
	
	/**
	 * @param  array  $params
	 * @param  int    $id
	 *
	 * @return Product
	 */
	public function updateProduct(array $params, int $id): Product
	{
		$product = $this->update($this->getProductFields($params), $id);
		$product->load('seo');
		$product->seo()->update($this->getSeoFields($params));
		
		$product->load('categories');
		$category = Category::find($this->getCategoriesIds($params));
		$catPath = $category->first()->full_path;
		
		$catPaths = [];
		
		
		foreach ($category as $cat) {
			$full = $cat->full_path.'/'.$product->slug;
			$catPaths[] = $full;
		}
		
		$catPaths = array_unique($catPaths);
		
		
		$product->categories()->sync($this->getCategoriesIds($params));
		
		$product->update([
			'full_path'  => $catPath.'/'.$product->slug,
			'full_paths' => json_encode($catPaths, JSON_FORCE_OBJECT, 512)
		]);
		$product->save();
		
		return $product;
	}
	
	/**
	 * @param  array  $params
	 *
	 * @return array
	 */
	private function getProductFields(array $params): array
	{
		return Arr::except($params, $this->productData);
	}
	
	/**
	 * @param  array  $params
	 *
	 * @return array
	 */
	private function getSeoFields(array $params): array
	{
		return Arr::except($params, $this->seoData);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function deleteProduct(int $id): bool
	{
		return $this->delete($id);
	}
	
	/**
	 * @param  array  $data
	 *
	 * @return Builder|Model|object
	 */
	public function findProductBy(array $data): Model
	{
		return $this->model::where($data)->first();
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
	 */
	public function getProduct(int $id)
	{
		$cacheKey = 'get.product_'.$id;
		
		return Cache::remember($cacheKey, 36000, static function () use ($id) {
			return Product::where('id', '=', $id)
				->with([
					'brand'      => static function ($query) {
						$query->select(['id', 'name']);
					},
					'categories' => static function ($query) {
						$query->select(['name'])->withPivot(['id', 'category_id']);
					},
					'seo'        => static function ($query) {
						$query->select([
							'id',
							'title',
							'description',
							'keywords',
							'canonical_url',
							'google_tag_manager',
							'ya_direct',
							'google_analytics',
							'ya_metrica',
							'og_title',
							'og_description',
							'og_image',
							'og_type',
							'og_url'
						]);
					},
					'attributes' => static function ($query) {
						$query->select([
							'id',
							'product_id',
							'attribute_id',
							'value'
						]);
					}
				])
				->first([
					'id',
					'name',
					'brand_id',
					'article',
					'barcode',
					'sort',
					'weight',
					'active',
					'is_popular',
					'is_new',
					'about',
					'sections'
				])->load('images');
		});
	}
	
	/**
	 * Set full URL from first & all full URL attach categories.
	 *
	 * @param  array  $params
	 * @param         $product
	 *
	 * @return void
	 */
	private function setProductFullPath(array $params, Product $product): void
	{
		$category = Category::find($this->getCategoriesIds($params));
		$catPath = $category->first()->full_path;
		
		$catPaths = [];
		
		foreach ($category as $cat) {
			$full = $cat->full_path.'/'.$product->slug;
			$catPaths[] = $full;
		}
		
		$product->categories()->sync($category);
		
		$product->update([
			'full_path'  => $catPath.'/'.$product->slug,
			'full_paths' => json_encode($catPaths)
		]);
		$product->save();
	}
}
