<?php


namespace Modules\Product\Repositories;


use App\Repositories\BaseRepository;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Product\Contracts\ProductAttributeContract;
use Modules\Product\Models\ProductAttribute;
use Str;

/**
 * Class ProductAttributeRepository
 *
 * @package Modules\Product\Repositories
 */
class ProductAttributeRepository extends BaseRepository implements ProductAttributeContract
{
    /**
     * ProductAttributeRepository constructor.
     *
     * @param  ProductAttribute  $model
     */
    public function __construct(ProductAttribute $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listProductAttributes(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): Collection {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('product.product_attributes.cache_key').$postfix,
            config('product.product_attributes.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @inheritDoc
     */
    public function findProductAttributesById(int $id): ?ProductAttribute
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function createProductAttributes(array $params): ProductAttribute
    {
        return $this->create($params);
    }
    
    /**
     * @inheritDoc
     */
    public function updateProductAttributes(array $params, int $id): ProductAttribute
    {
        return $this->update($params, $id);
    }
    
    /**
     * @inheritDoc
     */
    public function deleteProductAttributes(int $id): bool
    {
        return $this->delete($id);
    }
}
