<?php

return [
    'name'               => 'Product',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all products as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'products'           => [
        'cache_key' => env('PRODUCT_CACHE_KEY', 'index_products'),
        'cache_ttl' => env('PRODUCT_CACHE_TTL', 360000),
    ],
    'product_attributes' => [
        'cache_key' => env('PRODUCT_ATTRIBUTES_CACHE_KEY', 'index_product-attributes'),
        'cache_ttl' => env('PRODUCT_ATTRIBUTES_CACHE_TTL', 360000),
    ]
];
