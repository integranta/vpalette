<?php

namespace Modules\Product\Observers;

use Carbon\Carbon;
use Modules\Category\Models\Category;
use Modules\Dashboard\Models\Dashboard;
use Modules\Product\Models\Product;

/**
 * Class ProductObserver
 *
 * @package Modules\Product\Observers
 */
class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  Product  $product
     *
     * @return void
     */
    public function creating(Product $product): void
    {
        //
    }
    
    /**
     * Handle the product "created" event.
     *
     * @param  Product  $product
     *
     * @return void
     */
    public function created(Product $product): void
    {
        $product->load('categories');
        $categoryIds = json_decode($product->sections, true);
		//dd(__METHOD__, $category->first());
	
		$category = Category::find($categoryIds);
        $catPath = $category->first()->full_path;
        
        $catPaths = [];
        
        foreach ($category as $cat) {
            $full = $cat->full_path.'/'.$product->slug;
            $catPaths[] = $full;
        }
        
        //dd(__METHOD__, $catPath, $catPaths);
        
        $product->update([
            'full_path'  => $catPath.'/'.$product->slug,
            'full_paths' => $catPaths
        ]);
        
        
        // Если прошло 4 месяца с момента создания, убираем метку "Новинка".
        if ($product->created_at > Carbon::now()->subMonths(4)) {
            $product->update([
                'is_new' => false
            ]);
        }
        Dashboard::increment('count_products',1);
    }
    
    /**
     * Handle the product "updated" event.
     *
     * @param  Product  $product
     *
     * @return void
     */
    public function updated(Product $product): void
    {
        //
    }
    
    /**
     * Handle the product "deleted" event.
     *
     * @param  Product  $product
     *
     * @return void
     */
    public function deleted(Product $product): void
    {
        Dashboard::decrement('count_products',
            1);
    }
    
    /**
     * Handle the product "restored" event.
     *
     * @param  Product  $product
     *
     * @return void
     */
    public function restored(Product $product): void
    {
        Dashboard::increment('count_products',
            1);
    }
    
    /**
     * Handle the product "force deleted" event.
     *
     * @param  Product  $product
     *
     * @return void
     */
    public function forceDeleted(Product $product): void
    {
        Dashboard::decrement('count_products',
            1);
    }
}
