<?php

namespace Modules\Product\Observers;

use App\Traits\Cacheable;
use Modules\Product\Models\ProductAttribute;

/**
 * Class ProductAttributeObserver
 *
 * @package Modules\Product\Observers
 */
class ProductAttributeObserver
{
    use Cacheable;
    
    /**
     * Handle the product "created" event.
     *
     * @param  ProductAttribute  $product
     *
     * @return void
     */
    public function created(ProductAttribute $product): void
    {
        $this->incrementCountItemsInCache($product, config('product.product_attributes.cache_key'));
    }
    
    /**
     * Handle the product "updated" event.
     *
     * @param  ProductAttribute  $product
     *
     * @return void
     */
    public function updated(ProductAttribute $product): void
    {
        //
    }
    
    /**
     * Handle the product "deleted" event.
     *
     * @param  ProductAttribute  $product
     *
     * @return void
     */
    public function deleted(ProductAttribute $product): void
    {
        $this->decrementCountItemsInCache($product, config('product.product_attributes.cache_key'));
    }
    
    /**
     * Handle the product "restored" event.
     *
     * @param  ProductAttribute  $product
     *
     * @return void
     */
    public function restored(ProductAttribute $product): void
    {
        $this->incrementCountItemsInCache($product, config('product.product_attributes.cache_key'));
    }
    
    /**
     * Handle the product "force deleted" event.
     *
     * @param  ProductAttribute  $product
     *
     * @return void
     */
    public function forceDeleted(ProductAttribute $product): void
    {
        $this->decrementCountItemsInCache($product, config('product.product_attributes.cache_key'));
    }
}
