const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/product.js')
    .js(__dirname + '/Resources/Assets/js/create-app.js', 'js/create-product.js')
    .js(__dirname + '/Resources/Assets/js/app-show.js', 'js/show-product.js')
    .js(__dirname + '/Resources/Assets/js/edit-app.js', 'js/edit-product.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/product.css', {
        implementation: require('node-sass')
    });

if (mix.inProduction()) {
    mix.version();
}
