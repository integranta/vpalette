<?php


namespace Modules\Product\Services;


use App\DataTables\ProductsDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Brand\Contracts\BrandContract;
use Modules\Category\Contracts\CategoryContract;
use Modules\Product\Contracts\ProductContract;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\ImportCsvFileRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;
use Modules\Product\Models\Product;

/**
 * Class ProductWebService
 *
 * @package Modules\Product\Services
 */
class ProductWebService extends BaseService
{
    /** @var  ProductContract */
    protected $productRepository;
    
    /*** @var CategoryContract */
    protected $categoryRepository;
    
    /*** @var BrandContract */
    protected $brandRepository;
    
    /**
     * ProductController constructor.
     *
     * @param  ProductContract   $productRepo
     * @param  CategoryContract  $categoryRepo
     * @param  BrandContract     $brandRepo
     */
    public function __construct(
        ProductContract $productRepo,
        CategoryContract $categoryRepo,
        BrandContract $brandRepo
    ) {
        $this->productRepository = $productRepo;
        $this->categoryRepository = $categoryRepo;
        $this->brandRepository = $brandRepo;
    }
	
	/**
	 * Display a listing of the Product.
	 *
	 * @param  \App\DataTables\ProductsDataTable  $dataTable
	 *
	 *
	 * @return mixed
	 */
    public function index(ProductsDataTable $dataTable)
    {
		$this->setPageTitle(
			__('product::messages.title'),
			__('product::messages.index_subtitle'),
			__('product::messages.index_leadtext')
		);
		return $dataTable->render('product::products.index');
    }
    
    /**
     * Show the form for creating a new Product.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $model = new Product;
        $seoInfo = $model->load('seo');
        
        $brands = $this->brandRepository->listBrands();
        $categories = $this->categoryRepository->listCategories();
        
        $this->setPageTitle(
            __('product::messages.title'),
            __('product::messages.create_subtitle'),
            __('product::messages.create_leadtext')
        );
        return view('product::products.create', compact(
                'model',
                'categories',
                'seoInfo',
                'brands'
            )
        );
    }
	
	/**
	 * Store a newly created Product in storage.
	 *
	 * @param  \Modules\Product\Http\Requests\CreateProductRequest  $request
	 *
	 * @return RedirectResponse
	 */
    public function store(CreateProductRequest $request): RedirectResponse
    {
        $product = $this->productRepository->createProduct($request->all());
        
        if (!$product) {
            return $this->responseRedirectBack(
                __('product::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.products.index',
            __('product::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Display the specified Product.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
		$brands = $this->brandRepository->listBrands();
		$categories = $this->categoryRepository->listCategories();
	
		$model = $this->productRepository->getProduct($id);
        
        $this->setPageTitle(
            __('product::messages.title'),
            __('product::messages.show_subtitle', ['name' => $model->name]),
            __('product::messages.show_leadtext', ['name' => $model->name])
        );
        return view('product::products.show', compact(
                'model',
                'categories',
                'brands'
            )
        );
    }
    
    /**
     * Show the form for editing the specified Product.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(int $id)
	{
		$brands = $this->brandRepository->listBrands(['id', 'name']);
		$categories = $this->categoryRepository->listCategories(['id', 'name']);
		
		$model = $this->productRepository->getProduct($id);
		
		if ($model === null) {
			return $this->responseRedirect(
				'admin.products.index',
				__('product::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
        $this->setPageTitle(
            __('product::messages.title'),
            __('product::messages.edit_subtitle', ['name' => $model->name]),
            __('product::messages.edit_leadtext', ['name' => $model->name])
        );
        
        return view('product::products.edit', compact('model', 'brands', 'categories'));
    }
    
    /**
     * Update the specified Product in storage.
     *
     * @param  int                   $id
     * @param  UpdateProductRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateProductRequest $request): RedirectResponse
    {
        $product = $this->productRepository->updateProduct($request->all(), $id);
        
        if (!$product) {
            return $this->responseRedirectBack(
                __('product::messages.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        return $this->responseRedirectBack(
            __('product::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified Product from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        $product = $this->productRepository->deleteProduct($id);
        
        if (!$product) {
            return $this->responseRedirectBack(
                __('product::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        return $this->responseRedirect(
            'admin.products.index',
            __('product::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
	
	/**
	 * Вызов обработчика импорта Excel файала полученного с формы.
	 *
	 * @param  \Modules\Product\Http\Requests\ImportCsvFileRequest  $request
	 *
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function importHandler(ImportCsvFileRequest $request)
    {
    	$file = $request->file('file');
    	
    	
    	
		session()->flash('status', 'Файл был добавлен в очередь на импорт');
		
		return redirect(route('admin.products.import.form'));
    }
    
    public function import(): View
	{
		return view('product::import.index');
	}
}