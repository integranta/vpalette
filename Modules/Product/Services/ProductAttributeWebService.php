<?php


namespace Modules\Product\Services;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attribute\Contracts\AttributeContract;
use Modules\Product\Contracts\ProductAttributeContract;
use Modules\Product\Contracts\ProductContract;
use function __;

/**
 * Class ProductAttributeWebService
 *
 * @package Modules\Product\Services
 */
class ProductAttributeWebService extends BaseService
{
	/** @var ProductAttributeContract */
	private $productAttributeRepository;
	/**
	 * @var AttributeContract
	 */
	private $attributeRepository;
	/**
	 * @var ProductContract
	 */
	private $productRepository;
	
	/**
	 * ProductAttributeWebService constructor.
	 *
	 * @param  ProductAttributeContract  $productAttributeRepository
	 * @param  AttributeContract         $attributeRepository
	 * @param  ProductContract           $productRepository
	 */
	public function __construct(
		ProductAttributeContract $productAttributeRepository,
		AttributeContract $attributeRepository,
		ProductContract $productRepository
	) {
		$this->productAttributeRepository = $productAttributeRepository;
		$this->attributeRepository = $attributeRepository;
		$this->productRepository = $productRepository;
	}
	
	/**
	 * @return JsonResponse
	 */
	public function loadAttributes(): JsonResponse
	{
		$attributes = $this->attributeRepository->listAttributes();
		
		if (!$attributes) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('product::attributes.error_load_all')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('product::attributes.success_load_all'),
			$attributes
		);
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function productAttributes(Request $request): JsonResponse
	{
		$product = $this->productRepository->findProductById($request->id);
		
		if (!$product) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('product::attributes.error_load_product_attributes')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_FOUND,
			__('product::attributes.success_load_product_attributes'),
			$product->load(['attributes'])->attributes
		);
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function loadValues(Request $request): JsonResponse
	{
		$attribute = $this->attributeRepository->findAttributeById($request->id);
		
		if (!$attribute) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('product::attributes.error_load_attr_values', ['attribute', $attribute->name])
			);
		}
		
		return $this->responseJson(
			true,
			Response::HTTP_NOT_FOUND,
			__('product::attributes.success_load_attr_values', ['attribute', $attribute->name]),
			$attribute->values
		);
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function addAttribute(Request $request): JsonResponse
	{
		$productAttribute = $this->productAttributeRepository->createProductAttributes($request->data);
		
		if ($productAttribute) {
			return $this->responseJson(
				false,
				Response::HTTP_CREATED,
				__('product::attributes.success_add_attribute', ['attribute', $productAttribute->attribute->name]),
				$productAttribute
			);
		}
		
		return $this->responseJson(
			true,
			Response::HTTP_UNPROCESSABLE_ENTITY,
			__('product::attributes.error_add_attribute', ['attribute', $productAttribute->attribute->name])
		);
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function deleteAttribute(Request $request): JsonResponse
	{
		$productAttribute = $this->productAttributeRepository->findProductAttributesById($request->id);
		
		if (!$productAttribute) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('product::attributes.not_found', ['id' => $request->id])
			);
		}
		$productAttribute = $this->productAttributeRepository->deleteProductAttributes($request->id);
		
		if (!$productAttribute) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('product::attributes.error_delete_attribute')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('product::attributes.success_delete_attribute')
		);
	}
}
