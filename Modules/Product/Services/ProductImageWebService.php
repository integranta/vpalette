<?php


namespace Modules\Product\Services;


use App\Services\BaseService;
use App\Traits\UploadAble;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Modules\Product\Contracts\ProductContract;
use Modules\Product\Models\ProductImage;
use Storage;

/**
 * Class ProductImageWebService
 *
 * @package Modules\Product\Services
 */
class ProductImageWebService extends BaseService
{
    use UploadAble;
    
    /** @var ProductContract */
    protected $productRepository;
    
    /**
     * ProductImageController constructor.
     *
     * @param  ProductContract  $productRepo
     */
    public function __construct(ProductContract $productRepo)
    {
        $this->productRepository = $productRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function upload(Request $request): JsonResponse
    {
        $product = $this->productRepository->findProductById($request->product_id);
        
        if ($request->has('image')) {
            
            $image = $this->uploadOne($request->image, 'products');
            
            $productImage = new ProductImage([
                'full' => $image,
            ]);
            
            $product->images()->save($productImage);
        }
        
        return response()->json(['status' => 'Success']);
        
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function dropzone(Request $request): JsonResponse
    {
        
        $productImagesId = null;
        
        if ($request->has('pictures')) {
            $fullImagePath = Storage::put('products', $request->file('pictures'));
            
            if ($fullImagePath !== null) {
                $productImagesId = ProductImage::create([$fullImagePath]);
            }
        }
        return response()->json(
            array(
                'success' => 200,
                'message' => 'Картинки успешно загружены',
                'productImagesIds' => $productImagesId
            ),
            200);
    }
    
    /**
     * @param $id
     *
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        $image = ProductImage::findOrFail($id);
        
        if ($image->full !== '') {
            $this->deleteOne($image->full);
        }
        $image->delete();
        
        return redirect()->back();
    }
}