<?php


namespace Modules\Product\Services\API\V1;


use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Contracts\ProductContract;
use Modules\Product\Http\Requests\API\V1\CreateProductAPIRequest;
use Modules\Product\Http\Requests\API\V1\UpdateProductAPIRequest;
use Modules\Product\Transformers\ProductResource;

/**
 * Class ProductApiService
 *
 * @package Modules\Product\Services\API\V1
 */
class ProductApiService extends BaseService
{
    /** @var  ProductContract */
    private $productRepository;
    
    /**
     * ProductAPIController constructor.
     *
     * @param  ProductContract  $productRepo
     */
    public function __construct(ProductContract $productRepo)
    {
        $this->productRepository = $productRepo;
    }
    
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $products = ProductResource::collection($this->productRepository->listProducts());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('product::messages.success_retrieve'),
            $products
        );
    }
    
    
    /**
     * @param  CreateProductAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateProductAPIRequest $request): JsonResponse
    {
        $product = $this->productRepository->createProduct($request->all());
        
        if (!$product) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('product::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('product::message.success_create'),
            $product
        );
    }
    
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $product = ProductResource::make($this->productRepository->findProductById($id));
        
        if ($product === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('product::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('product::messages.success_found', ['id' => $id]),
            $product
        );
    }
    
    
    /**
     * @param  UpdateProductAPIRequest  $request
     * @param  int                      $id
     *
     * @return JsonResponse
     */
    public function update(UpdateProductAPIRequest $request, int $id): JsonResponse
    {
        $product = $this->productRepository->findProductById($id);
        
        if ($product === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('product::messages.not_found', ['id' => $id])
            );
        }
        
        $product = $this->productRepository->updateProduct($request->all(), $id);
        
        if (!$product) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('product::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('product::messages.success_update'),
            $product
        );
    }
    
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $product = $this->productRepository->findProductById($id);
        
        if ($product === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('product::messages.not_found', ['id' => $id])
            );
        }
        
        $product = $this->productRepository->deleteProduct($id);
        
        if (!$product) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('product::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('product::messages.success_delete'),
            $product
        );
    }
}
