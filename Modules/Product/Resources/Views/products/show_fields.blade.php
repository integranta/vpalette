<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">

        <!-- Active field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Активность:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('active', 0) !!}
                {!!
                    Form::checkbox(
                        'active',
                        true,
                        $model->active == 1 ? true : false,
                        ['class' => 'custom-control-input', 'id' => 'active']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="active"></label>
            </div>
        </div>

        <!-- Popular field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Популярное:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('is_popular', 0) !!}
                {!!
                    Form::checkbox(
                        'is_popular',
                        true,
                        $model->active == 1 ? true : false,
                        ['class' => 'custom-control-input', 'id' => 'is_popular']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="is_popular"></label>
            </div>
        </div>

        <!-- Name Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'name',
                    'Название:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!! Form::text('name', $model->name, [
                    'class' => 'form-control',
                    'placeholder' => 'Введите название товара'
                ]) !!}
            </div>
        </div>

        <!-- Sort Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                'sort',
                'Сортировка:',
                [
                    'class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3',
                ])
            !!}
            <div class="col-sm-12 col-md-7">
                {!! Form::text('sort', $model->sort, [
                'placeholder' => 'Укажите число для сортировки',
                'class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group row col-md-12 mb-4">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
                for="categories">Категория <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="categories"
                    class="form-control"
                    name="categories[]"
                    multiple
                >
                    @foreach($categories as $category)
                        @php $check = in_array($category->id, $model->categories->pluck('id')->toArray()) ? 'selected' : ''@endphp
                        <option value="{{ $category->id }}" {{ $check }}> {{ $category->name }} </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row col-md-12 mb-4">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
                for="brand_id">Бренд <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="brand_id"
                    class="form-control"
                    name="brand_id"
                >
                    <option value="0">Выберите бренд</option>
                    @foreach($brands as $brand)
                        @if ($model->brand_id == $brand->id)
                            <option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
                        @else
                            <option value="{{ $brand->id }}"> {{ $brand->name }} </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <!-- Article Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'article',
                    'Артикул:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::text(
                        'article',
                        $model->article,
                        [
                        'placeholder' => 'Укажите артикул товара',
                        'class' => 'form-control', 'id' => 'article']
                    )
                !!}
            </div>
        </div>

        <!-- Barcode Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'barcode',
                    'Штрих-код:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::text(
                        'barcode',
                        $model->barcode,
                        [
                        'placeholder' => 'Укажите номер штрих-код товара',
                        'class' => 'form-control', 'id' => 'barcode']
                    )
                !!}
            </div>
        </div>

        <!-- Weight Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'weight',
                    'Вес:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::text(
                        'weight',
                        $model->weight,
                        [
                        'placeholder' => 'Укажите вес товара',
                        'class' => 'form-control', 'id' => 'weight']
                    )
                !!}
            </div>
        </div>

        <!-- Description Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'about',
                    'Описание:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea(
                        'about',
                        $model->about,
                        [
                        'placeholder' => 'Укажите описание для товара',
                        'class' => 'form-control tinyMCE', 'id' => 'about']
                    )
                !!}
            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="spec" role="tabpanel" aria-labelledby="spec-tab">
        <product-attributes :productid="{{ $model->id }}"></product-attributes>
    </div>
    <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">

        <!-- Meta Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'title',
                    'Meta title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'title',
                        old('title', $model->seo->title),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>

        <!-- Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'description',
                    'Meta Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'description',
                        old('description', $model->seo->description),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- Keywords Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'keywords',
                    'Keywords:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'keywords',
                        old('keywords', $model->seo->keywords),
                        ['class' => 'form-control codeeditor', 'id' => 'keywords']
                    )
                !!}
            </div>
        </div>
        <!-- Canonical URL Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'canonical_url',
                    'Canonical URL:',
                    [
                        'class' => 'form-control-label col-sm-3 text-md-right',
                        'id' => 'canonical_url'
                    ]
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'canonical_url',
                        old('canonical_url', $model->seo->canonical_url),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>

        <!-- Og Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_title',
                    'Open Graph Title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_title',
                        $model->seo->og_title ?? null,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End title Field -->

        <!-- Og Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_description',
                    'Og Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'og_description',
                        $model->seo->og_description ?? null,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End description Field -->


        <!-- Og Image Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_image',
                    'Open Graph Image:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_image',
                        $model->seo->og_image ?? null,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End og image Field -->


        <!-- Og type Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_type',
                    'Open Graph type:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_type',
                        $model->seo->og_type ?? null,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End og type Field -->


        <!-- Og url Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_url',
                    'Open Graph url:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_url',
                        $model->seo->og_url ?? null,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End og url Field -->

        <div class="card-header">
            <h4>Реклама</h4>
        </div>

        <div class="form-group row">
            {!!
                Form::label(
                    'ya_direct',
                    'Яндекс.Директ:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_direct',
                        old('ya_direct', $model->seo->ya_direct),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_direct'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="card-header">
            <h4>Счётчики:</h4>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'google_tag_manager',
                    'Google Tag Manager:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_tag_manager',
                        old('google_tag_manager', $model->seo->google_tag_manager),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_tag_manager'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'google_analytics',
                    'Google Analytics:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_analytics',
                        old('google_anlytics', $model->seo->google_analytics),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_analytics'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'ya_metrica',
                    'Яндекс.Метрика:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_metrica',
                        old('ya_metrica', $model->seo->ya_metrica),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_metrica'
                        ]
                    )
                !!}
            </div>
        </div>
    </div>
</div>
