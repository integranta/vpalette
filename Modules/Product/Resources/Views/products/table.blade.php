{{--
<table class="table" id="products-table">
    <thead>
    <tr>
        <th>ID</th>
        <th>Артикул</th>
        <th>Название</th>
        <th>Бренд</th>
        <th>Категорий</th>
        <th>Символьный.код</th>
        <th class="text-center">Активное</th>
        <th class="text-center">Популярное</th>
        <th style="width:100px; min-width:100px;" class="text-center">Действия</th>
    </tr>
    </thead>
    --}}
{{--<tbody>
    @foreach($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->article }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->brand->name ?? null }}</td>
            <td>
                @foreach($product->categories as $category)
                    <span class="badge badge-info">{{ $category->name }}</span>
                @endforeach
            </td>
            <td style="width: 100%; max-width: 100px;">{{ $product->slug }}</td>
            <td class="text-center">
                @if ($product->active == 1)
                    <span class="badge badge-success">Да</span>
                @else
                    <span class="badge badge-danger">Нет</span>
                @endif
            </td>
            <td class="text-center">
                @if ($product->is_popular == 1)
                    <span class="badge badge-success">Да</span>
                @else
                    <span class="badge badge-danger">Нет</span>
                @endif
            </td>
            <td class="text-center">
                <div class="btn-group" role="group" aria-label="CRUD операции">
                    {!! Form::open(['route' => ['admin.products.destroy', $product->id], 'method' => 'delete']) !!}
                    <a
                        href="{{ route('admin.products.show', $product->id) }}"
                        class="btn btn-sm btn-success">
                        <i class="fa fa-eye"></i>
                    </a>
                    @can('edit all products')
                        <a
                            href="{{ route('admin.products.edit', $product->id) }}"
                            class="btn btn-sm btn-primary">
                            <i class="fa fa-edit"></i>
                        </a>
                    @endcan
                    @can('delete all products')
                        {!! Form::button(
                            '<i class="fa fa-trash"></i>',
                            [
                                'type' => 'submit',
                                'class' => 'btn btn-sm btn-danger',
                                'title' => 'Удалить',
                                'onclick' => "return confirm('Вы уверены?')"
                            ])
                        !!}
                    @endcan
                    {!! Form::close() !!}
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>--}}{{--

</table>
--}}

{{ $dataTable->table() }}