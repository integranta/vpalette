@extends('layouts.admin-master')

@section('title')
	{{ $pageTitle }}
@endsection

@section('plugin_css')
	<link rel="stylesheet" href="{{ asset('libs/css/select2/dist/css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/dropzone/basic.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/dropzone/dropzone.min.css') }}">
	<link rel="stylesheet" href="{{ mix('css/product.css') }}">
@endsection

@section('content')
	<section class="section">
		<div class="section-header">
			<div class="section-header-back">
				<a href="{{ route('admin.products.index') }}" class="btn btn-icon">
					<i class="fas fa-arrow-left"></i>
				</a>
			</div>
			<h1>{{ $pageTitle }}</h1>
		</div>

		<div class="section-body">
			<h2 class="section-title">{{ $subTitle }}</h2>
			<p class="section-lead">
				{{ $leadText }}
			</p>
			<div class="row">
				<div class="col-12 col-sm-6 col-lg-12">
					@include('admin.partials.flash')

					<div class="card">
						<div class="card-header">
							<h4>{{ $subTitle }}</h4>
						</div>
						<div class="card-body">
							<ul class="nav nav-tabs" id="myTab2" role="tablist">
								@include('product::includes.edit-tabs')
							</ul>

							{!!
								Form::open([
									'route' => ['admin.products.show', $model->id],
									'method' => 'GET',
									'files' => true
								])
							!!}
							<div class="tab-content tab-bordered" id="myTab3Content">
								<div
									class="tab-pane fade show active"
									id="element"
									role="tabpanel"
									aria-labelledby="element-tab"
								>
									<h3 class="tile-title">Информация о товаре</h3>
									<hr>
									<div class="tile-body">
										<div class="form-group">
											<label class="control-label" for="name">Название</label>
											<input
												class="form-control @error('name') is-invalid @enderror"
												type="text"
												placeholder="Введите название товара"
												id="name"
												name="name"
												value="{{ $model->name }}"
												disabled
											/>
											<input type="hidden" name="product_id" value="{{ $model->id }}">
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="article">Артикул</label>
													<input
														class="form-control @error('article') is-invalid @enderror"
														type="text"
														placeholder="Введите артикул товара"
														id="article"
														name="article"
														value="{{ $model->article }}"
														disabled
													/>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="barcode">Штрих-код</label>
													<input
														class="form-control @error('barcode') is-invalid @enderror"
														type="text"
														placeholder="Введите штрих-код товара"
														id="barcode"
														name="barcode"
														value="{{ $model->barcode }}"
														disabled
													/>
													<div class="invalid-feedback active">
														<i class="fa fa-exclamation-circle fa-fw"></i> @error('barcode')
														<span>{{ $message }}</span> @enderror
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="brand_id">Бренд</label>
													<select
														name="brand_id"
														id="brand_id"
														class="form-control @error('brand_id') is-invalid @enderror"
														disabled
													>
														<option value="0">Выберите бренд</option>
														@foreach($brands as $brand)
															@if ($model->brand_id === $brand->id)
																<option value="{{ $brand->id }}"
																		selected>{{ $brand->name }}</option>
															@else
																<option
																	value="{{ $brand->id }}">{{ $brand->name }}</option>
															@endif
														@endforeach
													</select>
													<div class="invalid-feedback active">
														<i class="fa fa-exclamation-circle fa-fw"></i> @error('brand_id')
														<span>{{ $message }}</span> @enderror
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="sort">Сортировка</label>
													<input
														class="form-control @error('sort') is-invalid @enderror"
														type="text"
														placeholder="Введите сортировку товара"
														id="sort"
														name="sort"
														value="{{ $model->sort }}"
														disabled
													/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="categories">Категория</label>
													<select name="categories[]" id="categories" class="form-control"
															multiple>
														@foreach($categories as $id => $categoryName)
															@foreach(json_decode($model->sections, true) as $selectedCategory)

																@if ($id === $selectedCategory)
																	<option value="{{ $id }}" selected>
																		{{ $categoryName }}
																	</option>
																@else
																	<option value="{{ $id }}">
																		{{ $categoryName }}
																	</option>
																@endif

															@endforeach
														@endforeach
													</select>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="weight">Вес</label>
													<input
														class="form-control"
														type="text"
														placeholder="Введите вес товара"
														id="weight"
														name="weight"
														value="{{ $model->weight }}"
														disabled
													/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<!-- Active field -->
												<div class="form-group row col-md-12">
													<span class="col-form-label text-md-right">Активность:</span>
													<div
														class="custom-control custom-checkbox d-flex align-self-center">
														{!! Form::hidden('active', 0) !!}
														{!!
															Form::checkbox(
																'active',
																true,
																$model->active === 1 ? true : false,
																[
																	'class' => 'custom-control-input',
																	'id' => 'active',
																	'disabled'
																]
															)
														!!}
														<label class="custom-control-label ml-lg-3"
															   for="active"></label>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<!-- Popular field -->
												<div class="form-group row col-md-12">
													<span class="col-form-label text-md-right">Популярное:</span>
													<div
														class="custom-control custom-checkbox d-flex align-self-center">
														{!! Form::hidden('is_popular', 0) !!}
														{!!
															Form::checkbox(
																'is_popular',
																true,
																$model->active === 1 ? true : false,
																[
																	'class' => 'custom-control-input',
																	'id' => 'is_popular',
																	'disabled'
																]
															)
														!!}
														<label class="custom-control-label ml-lg-3"
															   for="is_popular"></label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
									<div class="row">
										<div class="col-sm-12 col-lg-12">
											<div class="form-group">
												<label class="control-label" for="about">Описание товара</label>
												<textarea
													name="about"
													id="about"
													rows="8"
													class="form-control tinyMCE"
													disabled
												>
                                           {{ $model->about }}
                                       </textarea>
											</div>
										</div>
									</div>
								</div>

								<div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
									<!-- Meta Title Field -->
									<div class="form-group row align-items-center">
										{!!
											Form::label(
												'title',
												'Meta title:',
												['class' => 'form-control-label col-sm-3 text-md-right']
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::text(
													'title',
													!empty($model->seo->title) ? $model->seo->title : '',
													['class' => 'form-control', 'disabled']
												)
											!!}
										</div>
									</div>

									<!-- Description Field -->
									<div class="form-group row align-items-center">
										{!!
											Form::label(
												'description',
												'Meta Description:',
												['class' => 'form-control-label col-sm-3 text-md-right']
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::textarea(
													'description',
													!empty($model->seo->description) ? $model->seo->description : '',
													['class' => 'form-control', 'disabled']
												)
											!!}
										</div>
									</div>
									<!-- Keywords Field -->
									<div class="form-group row align-items-center">
										{!!
											Form::label(
												'keywords',
												'Keywords:',
												['class' => 'form-control-label col-sm-3 text-md-right']
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::textarea(
													'keywords',
													!empty($model->seo->keywords) ? $model->seo->keywords : '',
													['class' => 'form-control codeeditor', 'id' => 'keywords', 'disabled']
												)
											!!}
										</div>
									</div>
									<!-- Canonical URL Field -->
									<div class="form-group row align-items-center">
										{!!
											Form::label(
												'canonical_url',
												'Canonical URL:',
												[
													'class' => 'form-control-label col-sm-3 text-md-right',
													'id' => 'canonical_url'
												]
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::text(
													'canonical_url',
													!empty($model->seo->canonical_url) ? $model->seo->cannonical_url : '',
													['class' => 'form-control', 'disabled']
												)
											!!}
										</div>
									</div>

									<div class="card-header">
										<h4>Реклама</h4>
									</div>

									<div class="form-group row">
										{!!
											Form::label(
												'ya_direct',
												'Яндекс.Директ:',
												['class' => 'form-control-label col-sm-3 text-md-right']
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::textarea(
													'ya_direct',
													!empty($model->seo->ya_direct) ? $model->seo->ya_direct : '',
													[
														'class' => 'form-control codeeditor',
														'id' => 'ya_direct',
														'disabled'
													]
												)
											!!}
										</div>
									</div>
									<div class="card-header">
										<h4>Счётчики:</h4>
									</div>
									<div class="form-group row">
										{!!
											Form::label(
												'google_tag_manager',
												'Google Tag Manager:',
												['class' => 'form-control-label col-sm-3 text-md-right']
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::textarea(
													'google_tag_manager',
													!empty($model->seo->google_tag_manager) ? $model->seo->google_tag_manager : '',
													[
														'class' => 'form-control codeeditor',
														'id' => 'google_tag_manager',
														'disabled'
													]
												)
											!!}
										</div>
									</div>
									<div class="form-group row">
										{!!
											Form::label(
												'google_analytics',
												'Google Analytics:',
												['class' => 'form-control-label col-sm-3 text-md-right']
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::textarea(
													'google_analytics',
													!empty($model->seo->google_analytics) ? $model->seo->google_analytics : '',
													[
														'class' => 'form-control codeeditor',
														'id' => 'google_analytics',
														'disabled'
													]
												)
											!!}
										</div>
									</div>
									<div class="form-group row">
										{!!
											Form::label(
												'ya_metrica',
												'Яндекс.Метрика:',
												['class' => 'form-control-label col-sm-3 text-md-right']
											)
										!!}
										<div class="col-sm-6 col-md-9">
											{!!
												Form::textarea(
													'ya_metrica',
													!empty($model->seo->ya_metrica) ? $model->seo->ya_metrica : '',
													[
														'class' => 'form-control codeeditor',
														'id' => 'ya_metrica',
														'disabled'
													]
												)
											!!}
										</div>
									</div>
								</div>

								<div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
									@if ($model->images->isNotEmpty())
										<div class="tab-pane" id="images">
											<div class="tile">

												<h3 class="tile-title">Загруженные изображения</h3>
												<hr>
												<div class="tile-body">

													<hr>
													<div class="row">
														@foreach($model->images as $image)
															<div class="col-md-3">
																<div class="card">
																	<div class="card-body">
																		<img src="{{ asset($image->full) }}"
																			 id="brandLogo"
																			 class="img-fluid" alt="img">
																		<a class="card-link float-right text-danger"
																		   href="{{ route('admin.products.images.delete', $image->id) }}">
																			<i class="fa fa-fw fa-lg fa-trash"></i>
																		</a>
																	</div>
																</div>
															</div>
														@endforeach
													</div>
												</div>
											</div>
										</div>
									@endif
									<h3>Картинок нет</h3>
								</div>
								<div class="tab-pane fade" id="attributes" role="tabpanel"
									 aria-labelledby="attributes-tab">
									<div class="tile-body">
										<div class="table-responsive table-striped">
											<table class="table table-sm">
												<thead>
												<tr class="text-center">
													<th>Значение</th>
													<th>Количество</th>
													<th>Цена</th>
												</tr>
												</thead>
												<tbody>
												@forelse($model->attributes as $field => $value)
													<tr>
														<td style="width: 25%"
															class="text-center">{{ $value->value }}</td>
														<td style="width: 25%"
															class="text-center">{{ $value->quantity }}</td>
														<td style="width: 25%"
															class="text-center">{{ $value->price }}</td>
													</tr>
												@empty
													<tr>
														<td class="text-center">Атрибутов у товара не заполнены</td>
													</tr>
												@endforelse
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" id="product_id" name="product_id" value="{{ $model->id }}">
							{{ csrf_field() }}
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@push('scripts')
	{{-- JS Libraies --}}
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="{{ asset('libs/js/tinymce/langs/ru.js') }}"></script>
	<script src="{{ asset('libs/js/select2/dist/js/select2.full.min.js') }}"></script>
	{{--  Module JS File  --}}
	<script src="{{ mix('js/show-product.js') }}"></script>
@endpush
