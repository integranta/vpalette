<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Импорт каталога</title>

       {{-- Laravel Mix - CSS File --}}
        <link rel="stylesheet" href="{{ asset('css/app.min.css') }}">

    </head>
    <body>
        @yield('content')

        {{-- Laravel Mix - JS File --}}
        {{-- <script src="{{ asset('js/product.js') }}"></script> --}}
    </body>
</html>
