@extends('product::layouts.master')

@section('content')
	<div class="container">
		<div class="row my-5">
			<div class="col-12">
				<h1>Импорт каталога</h1>
				@if(session('status'))
					<div class="alert alert-info">{{ session('status') }}</div>
				@endif
				<form
					action="{{ route('admin.import.handler') }}"
					method="POST"
					accept-charset="UTF-8"
					enctype="multipart/form-data"
				>
					@csrf
					<div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}"
					>
						<label
							class="control-label"
							for="file">Импортируемый файл</label>
						<input
							class="form-control"
							type="file"
							name="file"
							id="file"
						>
						@if ($errors->has('file'))
							<span class="help-block">
								<strong>{{ $errors->first('file') }}</strong>
							</span>
						@endif
					</div>
					<button
						class="btn btn-success"
						type="submit">Импортировать
					</button>
				</form>
			</div>
		</div>
	</div>
@endsection