<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Заголовок (meta title) для всех страниц.
	|--------------------------------------------------------------------------
	*/
	'title'            => 'Товары',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы списка всех товаров.
	|--------------------------------------------------------------------------
	*/
	'index_subtitle'   => 'Список всех товаров.',
	'index_leadtext'   => 'Здесь вы можете увидеть список всех товаров.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы создания товара.
	|--------------------------------------------------------------------------
	*/
	'create_subtitle'  => 'Добавление нового товара.',
	'create_leadtext'  => 'На этой странице вы можете создать нового товара и заполнить все поля.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об создания товара.
	|--------------------------------------------------------------------------
	*/
	'error_create'     => 'Произошла ошибка при добавлении товара.',
	'success_create'   => 'Товар успешно был добавлен.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы просмотра информации об товаре.
	|--------------------------------------------------------------------------
	*/
	'show_subtitle'    => 'Просмотр информации об товаре: :name.',
	'show_leadtext'    => 'Здесь вы можете просмотреть информацию об товаре: :name.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы редактирования товаре.
	|--------------------------------------------------------------------------
	*/
	'edit_subtitle'    => 'Редактирование информации об товаре: :name.',
	'edit_leadtext'    => 'Здесь вы можете заполнить поля для редактирования товара: :name.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об обновление товара.
	|--------------------------------------------------------------------------
	*/
	'error_update'     => 'Произошла ошибка при обновлении информации товаре.',
	'success_update'   => 'Информация об товаре успешно обновлена.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об удаления товара.
	|--------------------------------------------------------------------------
	*/
	'error_delete'     => 'Произошла ошибка при удалении товара.',
	'success_delete'   => 'Информация об товаре успешно удалена.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об извлечения товара, через REST API.
	|--------------------------------------------------------------------------
	*/
	'not_found'        => 'Товар с таким ID не был найден в системе.',
	'success_found'    => 'Страница с ID :id успешно извлечена.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об извлечения товаров, через REST API.
	|--------------------------------------------------------------------------
	*/
	'success_retrieve' => 'Товары были успешно извлечены.'
];
