import {tinyMCEConfiguration} from '../../../../../resources/js/shared/tinyMCEConfig';

$(document).ready(function () {
    $('#categories').select2();
    tinymce.init({
        ...tinyMCEConfiguration,
        readonly: false,
        selector: ".tinyMCE"
    });
});
