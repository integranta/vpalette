<?php

namespace Modules\Product\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Attribute\Transformers\AttributeResource;
use Modules\Brand\Transformers\BrandResource;
use Modules\Category\Transformers\CategoryResource;
use Modules\PartnerOffer\Transformers\PartnerOfferResource;
use Modules\Rating\Transformers\RatingResource;
use Modules\Seo\Transformers\SeoResource;

/**
 * Class ProductResource
 *
 * @package Modules\Product\Transformers
 */
class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'isActive' => $this->active,
            'isPopular' => $this->is_popular,
            'isNew' => $this->is_new,
            'article' => $this->article,
            'barcode' => $this->barcode,
            'name' => $this->name,
            'sort' => (int) $this->sort,
            'slug' => $this->slug,
            'about' => $this->about,
            'weight' => (float) $this->weight,
            'minPrice' => (int) $this->min_price,
            'brandId' => $this->brand_id,
            'URI' => $this->full_path,
            'URIs' => $this->full_paths,
            'brand' => BrandResource::make($this->whenLoaded('brand')),
            'images' => ProductImageResource::collection($this->whenLoaded('images')),
            'attributes' => AttributeResource::collection($this->whenLoaded('attributes')),
            'offers' => PartnerOfferResource::collection($this->whenLoaded('partnerOffers')),
            'ratings' => RatingResource::collection($this->whenLoaded('ratings')),
            'seo' => SeoResource::make($this->whenLoaded('seo')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories'))
        ];
    }
}
