<?php

namespace Modules\Product\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Product\Http\Requests\API\V1\CreateProductAPIRequest;
use Modules\Product\Http\Requests\API\V1\UpdateProductAPIRequest;
use Modules\Product\Services\API\V1\ProductApiService;
use Illuminate\Http\Response;

/**
 * Class ProductAPIController
 *
 * @package Modules\Product\Http\Controllers\API\V1
 */
class ProductAPIController extends Controller
{
    /** @var ProductApiService */
    private $productApiService;

    /**
     * ProductAPIController constructor.
     *
     * @param  ProductApiService  $service
     */
    public function __construct(ProductApiService $service)
    {
        $this->productApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }


    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->productApiService->index($request);
    }


    /**
     * @param  CreateProductAPIRequest  $request
     *
     * @return Response
     */
    public function store(CreateProductAPIRequest $request): JsonResponse
    {
        return $this->productApiService->store($request);
    }


    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->productApiService->show($id);
    }


    /**
     * @param  UpdateProductAPIRequest  $request
     * @param  int                      $id
     *
     * @return JsonResponse
     */
    public function update(UpdateProductAPIRequest $request, int $id): JsonResponse
    {
        return $this->productApiService->update($request, $id);
    }


    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->productApiService->destroy($id);
    }
}
