<?php

namespace Modules\Product\Http\Controllers;

use App\DataTables\ProductsDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\ImportCsvFileRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;
use Modules\Product\Services\ProductWebService;

/**
 * Class ProductController
 *
 * @package Modules\Product\Http\Controllers
 */
class ProductController extends Controller
{
    /** @var ProductWebService */
    private $productWebService;

    /**
     * ProductController constructor.
     *
     * @param  ProductWebService  $service
     */
    public function __construct(
        ProductWebService $service
    ) {
        $this->productWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Product.
	 *
	 * @param  \App\DataTables\ProductsDataTable  $dataTable
	 *
	 *
	 * @return mixed
	 */
    public function index(ProductsDataTable $dataTable)
    {
        return $this->productWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->productWebService->create();
    }
	
	/**
	 * Store a newly created Product in storage.
	 *
	 * @param  \Modules\Product\Http\Requests\CreateProductRequest  $request
	 *
	 * @return RedirectResponse
	 */
    public function store(CreateProductRequest $request): RedirectResponse
    {
        return $this->productWebService->store($request);
    }

    /**
     * Display the specified Product.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->productWebService->show($id);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->productWebService->edit($id);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int                   $id
     * @param  UpdateProductRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateProductRequest $request): RedirectResponse
    {
        return $this->productWebService->update($id, $request);
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->productWebService->destroy($id);
    }
    
    public function importHandler(ImportCsvFileRequest $request)
	{
		return $this->productWebService->importHandler($request);
	}
	
	public function import()
	{
		return $this->productWebService->import();
	}
	
	public function importProcess()
	{
		return $this->productWebService->importProcess();
	}
}
