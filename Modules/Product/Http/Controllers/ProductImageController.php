<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Services\ProductImageWebService;

/**
 * Class ProductImageController
 *
 * @package Modules\Product\Http\Controllers
 */
class ProductImageController extends Controller
{
    /** @var ProductImageWebService */
    private $productImageWebService;

    /**
     * ProductImageController constructor.
     *
     * @param  ProductImageWebService  $service
     */
    public function __construct(ProductImageWebService $service)
    {
        $this->productImageWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function upload(Request $request): JsonResponse
    {
        return $this->productImageWebService->upload($request);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function dropzone(Request $request): JsonResponse
    {
        return $this->productImageWebService->dropzone($request);
    }

    /**
     * @param $id
     *
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        return $this->productImageWebService->delete($id);
    }
}
