<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Services\ProductAttributeWebService;

/**
 * Class ProductAttributeController
 *
 * @package Modules\Product\Http\Controllers
 */
class ProductAttributeController extends Controller
{
    /**
     * @var ProductAttributeWebService
     */
    private $productAttributeWebService;

    /**
     * ProductAttributeController constructor.
     *
     * @param  ProductAttributeWebService  $service
     */
    public function __construct(ProductAttributeWebService $service)
    {
        $this->productAttributeWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @return JsonResponse
     */
    public function loadAttributes(): JsonResponse
    {
        return $this->productAttributeWebService->loadAttributes();
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function productAttributes(Request $request): JsonResponse
    {
        return $this->productAttributeWebService->productAttributes($request);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function loadValues(Request $request): JsonResponse
    {
        return $this->productAttributeWebService->loadValues($request);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function addAttribute(Request $request): JsonResponse
    {
        return $this->productAttributeWebService->addAttribute($request);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function deleteAttribute(Request $request): JsonResponse
    {
        return $this->productAttributeWebService->deleteAttribute($request);
    }
}
