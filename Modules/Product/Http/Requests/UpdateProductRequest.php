<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Product\Models\Product;

/**
 * Class UpdateProductRequest
 *
 * @package Modules\Product\Http\Requests
 */
class UpdateProductRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'article' => 'required',
            'barcode' => 'required',
            'sort' => 'required|integer',
            'active' => 'boolean',
            'is_popular' => 'boolean',
            'brand_id' => 'required|not_in:0',
            'about' => 'string'
        ];
    }
}
