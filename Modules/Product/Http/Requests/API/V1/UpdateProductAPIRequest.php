<?php

namespace Modules\Product\Http\Requests\API\V1;

use Modules\Product\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateProductAPIRequest
 *
 * @package Modules\Product\Http\Requests\API\V1
 */
class UpdateProductAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Product::$rules;
    }
}
