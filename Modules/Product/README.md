Модуль Product
============

Этот модуль для управления списоком доступных товаров, которые получат предложения от магазинов-партнёров.


#### Начало работы

```bash
php artisan module:migrate Product
```


