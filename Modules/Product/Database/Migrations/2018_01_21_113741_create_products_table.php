<?php

use App\Enums\ProductType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateProductsTable
 */
class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('sections');
            $table->string('article');
            $table->string('barcode')->nullable()->default(null);
            $table->string('name');
            $table->string('slug');
            $table->string('type_product')->default(ProductType::PORTAL_PRODUCT);
            $table->boolean('is_new')->default(true);
            $table->string('full_path')->nullable();
            $table->json('full_paths')->nullable();
            $table->text('about')->nullable();
            $table->decimal('weight', 8, 2)->nullable();
            $table->unsignedInteger('min_price')->nullable();
            $table->unsignedInteger('sort')->default(500);
            $table->boolean('active')->default(true);
            $table->boolean('is_popular')->default(0);
            $table->unsignedBigInteger('brand_id')->nullable()->default(null);
            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['article', 'barcode', 'slug', 'is_new', 'brand_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
