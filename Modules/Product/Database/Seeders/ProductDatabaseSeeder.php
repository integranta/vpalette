<?php

namespace Modules\Product\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Storage;
use Modules\Category\Models\Category;
use Modules\Product\Models\Product;
use Str;

/**
 * Class ProductDatabaseSeeder
 *
 * @package Modules\Product\Database\Seeders
 */
class ProductDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        Model::unguard();
        
        factory(Product::class, 20)
            ->create()
            ->each(function ($product) {
                $product->seo()->create([
                    'title' => $product->name.' ВПалитре - Интернет-портал',
                    'description' => $product->name.'Оптимальный интернет-магазин',
                    'keywords' => 'интернет-магазин, заказать, купить',
                ])->make();
                $product->categories()->sync(Category::find(json_decode($product->sections)));
                
                $fullImagePaths = [];
                $path = 'products/'.Str::of($product->name)->lower()->trim()->slug('_', 'ru');
                for ($i = 0; $i < 6; $i++) {
                    $image = 'public/storage/uploads/faker/products/'.Str::of(random_int(1, 6)).'.png';
                    
                    $fullImagePaths[$i]['full'] = Storage::disk('uploads')->putFile($path, $image);
                }
                $product->images()->createMany($fullImagePaths);
            });
    }
}
