<?php

/** @var Factory $factory */

use App\Enums\ProductType;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Brand\Models\Brand;
use Modules\Product\Models\Product;

$factory->define(Product::class,
    function (Faker $faker) {


        $categories = null;
        $random_int = random_int(1, 3);
        for ($i = 0; $i < $random_int; $i++) {
            $categories[] = random_int(1, 226);
        }

        return [
            'article' => $faker->ean8,
            'barcode' => $faker->ean13,
            'name' => $faker->sentence,
            'type_product' => ProductType::PORTAL_PRODUCT,
            'about' => $faker->realText(300),
            'weight' => $faker->randomFloat(true, 1, 9999.99),
            'sort' => $faker->numberBetween(1, 500),
            'active' => true,
            'is_popular' => $faker->numberBetween(0, 1),
            'is_new' => $faker->numberBetween(0, 1),
            'brand_id' => \factory(Brand::class),
            'sections' => json_encode($categories)
        ];
    });
