<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Product\Http\Controllers\ProductAttributeController;
use Modules\Product\Http\Controllers\ProductController;
use Modules\Product\Http\Controllers\ProductImageController;

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		
		Route::resource('products', ProductController::class);
		Route::post('images/upload', [ProductImageController::class, 'upload'])
			->name('products.images.upload');
		Route::get('images/{id}/delete', [ProductImageController::class, 'delete'])
			->name('products.images.delete');
		Route::post('images/dropzone', [ProductImageController::class, 'dropzone'])
			->name('products.images.dropzone');
		
		// Load attributes on the page load
		Route::get('products/attributes/load', [ProductAttributeController::class, 'loadAttributes']);
		// Load product attributes on the page load
		Route::post('products/attributes', [ProductAttributeController::class, 'productAttributes']);
		// Load option values for a attribute
		Route::post('products/attributes/values', [ProductAttributeController::class, 'loadValues']);
		// Add product attribute to the current product
		Route::post('products/attributes/add', [ProductAttributeController::class, 'addAttribute']);
		// Delete product attribute from the current product
		Route::post('products/attributes/delete', [ProductAttributeController::class, 'deleteAttribute']);
	});
