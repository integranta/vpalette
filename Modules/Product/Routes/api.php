<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Product\Http\Controllers\API\V1\ProductAPIController;

Route::prefix('v1')->group(function () {
	Route::apiResource('products', ProductAPIController::class);
	Route::get('products/search', [ProductAPIController::class, 'search']);
});
