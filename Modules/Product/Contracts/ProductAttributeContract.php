<?php


namespace Modules\Product\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Product\Models\ProductAttribute;

/**
 * Interface ProductAttributeContract
 *
 * @package Modules\Product\Contracts
 */
interface ProductAttributeContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return mixed
     */
    public function listProductAttributes(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): Collection;
    
    /**
     * @param  int  $id
     *
     * @return ProductAttribute|null
     */
    public function findProductAttributesById(int $id): ?ProductAttribute;
    
    /**
     * @param  array  $params
     *
     * @return ProductAttribute
     */
    public function createProductAttributes(array $params): ProductAttribute;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return ProductAttribute
     */
    public function updateProductAttributes(array $params, int $id): ProductAttribute;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteProductAttributes(int $id): bool;
}
