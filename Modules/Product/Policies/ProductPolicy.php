<?php

namespace Modules\Product\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Product\Models\Product;

/**
 * Class ProductPolicy
 *
 * @package Modules\Product\Policies
 */
class ProductPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the product.
     *
     * @param  User|null  $user
     * @param  Product    $product
     *
     * @return mixed
     */
    public function view(?User $user, Product $product)
    {
        if ($product->active) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished products')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create products.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create products')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the product.
     *
     * @param  User     $user
     * @param  Product  $product
     *
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        if ($user->can('edit all products')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the product.
     *
     * @param  User     $user
     * @param  Product  $product
     *
     * @return mixed
     */
    public function delete(User $user, Product $product)
    {
        if ($user->can('delete any product')) {
            return true;
        }
        
        return false;
    }
}
