<?php

namespace Modules\Partner\Emails;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class RegistrationEmail
 *
 * @package Modules\Partner\Emails
 */
class RegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * @var User
     */
    public $user;
    
    /**
     * @var User
     */
    public $password;
    
    /**
     * Create a new message instance.
     *
     * @param  User  $user
     * @param  User  $password
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('partner::partners.email.mail')
            ->subject(config('app.name').": Registration new Partner Notification")
            ->from(config('mail.from.address'));
    }
}
