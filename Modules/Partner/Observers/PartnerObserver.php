<?php

namespace Modules\Partner\Observers;

use App\Traits\Cacheable;
use Modules\Partner\Models\Partner;

/**
 * Class PartnerObserver
 *
 * @package Modules\Partner\Observers
 */
class PartnerObserver
{
    use Cacheable;
    
    /**
     * Handle the partner "created" event.
     *
     * @param  Partner  $partner
     *
     * @return void
     */
    public function created(Partner $partner): void
    {
        $this->incrementCountItemsInCache($partner, config('partner.cache_key'));
    }
    
    /**
     * Handle the partner "updated" event.
     *
     * @param  Partner  $partner
     *
     * @return void
     */
    public function updated(Partner $partner): void
    {
        //
    }
    
    /**
     * Handle the partner "deleted" event.
     *
     * @param  Partner  $partner
     *
     * @return void
     */
    public function deleted(Partner $partner): void
    {
        $this->decrementCountItemsInCache($partner, config('partner.cache_key'));
    }
    
    /**
     * Handle the partner "restored" event.
     *
     * @param  Partner  $partner
     *
     * @return void
     */
    public function restored(Partner $partner): void
    {
        $this->incrementCountItemsInCache($partner, config('partner.cache_key'));
    }
    
    /**
     * Handle the partner "force deleted" event.
     *
     * @param  Partner  $partner
     *
     * @return void
     */
    public function forceDeleted(Partner $partner): void
    {
        $this->decrementCountItemsInCache($partner, config('partner.cache_key'));
    }
}
