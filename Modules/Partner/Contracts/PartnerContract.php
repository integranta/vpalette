<?php


namespace Modules\Partner\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Partner\Models\Partner;

/**
 * Interface PartnerContract
 *
 * @package Modules\Partner\Contracts
 */
interface PartnerContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listPartners(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Partner
     */
    public function findPartnerById(int $id): ?Partner;
    
    /**
     * @param  array  $params
     *
     * @return Partner
     */
    public function createPartner(array $params): Partner;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Partner
     */
    public function updatePartner(array $params, int $id): Partner;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deletePartner(int $id): bool;
    
    /**
     * Get all offers by the given partner ID
     *
     * @param  int  $id
     *
     * @return array
     */
    public function getOffers(int $id): array;
}
