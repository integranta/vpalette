@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.partners.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card-body">
                        <edit-partner-component
                            :partner="{{ $model }}"
                            subtitle="{{ $subTitle }}"
                        ></edit-partner-component>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
