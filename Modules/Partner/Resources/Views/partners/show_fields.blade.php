<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $partner->id !!}</p>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:') !!}
    <p>{!! $partner->company !!}</p>
</div>

<!-- Law Shop Name Field -->
<div class="form-group">
    {!! Form::label('law_shop_name', 'Law Shop Name:') !!}
    <p>{!! $partner->law_shop_name !!}</p>
</div>

<!-- Inn Field -->
<div class="form-group">
    {!! Form::label('inn', 'Inn:') !!}
    <p>{!! $partner->inn !!}</p>
</div>

<!-- Kpp Field -->
<div class="form-group">
    {!! Form::label('kpp', 'Kpp:') !!}
    <p>{!! $partner->kpp !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $partner->address !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $partner->city !!}</p>
</div>

<!-- Post Index Field -->
<div class="form-group">
    {!! Form::label('post_index', 'Post Index:') !!}
    <p>{!! $partner->post_index !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $partner->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $partner->phone !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $partner->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $partner->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $partner->updated_at !!}</p>
</div>

