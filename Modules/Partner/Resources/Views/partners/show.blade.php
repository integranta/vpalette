@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.partners.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>

            <show-partner-component
                subtitle="{{ $subTitle }}"
                :partner="{{ $model->toJson() }}"
            >
            </show-partner-component>
        </div>
    </section>
@endsection
