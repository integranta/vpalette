@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('css/partner.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.partners.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>

            <register-partner-component
                :partner="{{ $model->toJson() }}"
                subtitle="{{ $subTitle }}"
            ></register-partner-component>
        </div>
    </section>
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ asset('js/partner.js') }}"></script>
@endpush
