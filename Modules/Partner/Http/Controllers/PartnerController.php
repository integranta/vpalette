<?php

namespace Modules\Partner\Http\Controllers;

use App\DataTables\PartnersDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Partner\Http\Requests\CreatePartnerRequest;
use Modules\Partner\Http\Requests\UpdatePartnerRequest;
use Modules\Partner\Services\PartnerWebService;


/**
 * Class PartnerController
 *
 * @package Modules\Partner\Http\Controllers
 */
class PartnerController extends Controller
{
    /** @var  PartnerWebService */
    private $partnerWebService;

    /**
     * PartnerController constructor.
     *
     * @param  PartnerWebService  $service
     */
    public function __construct(PartnerWebService $service)
    {
        $this->partnerWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Partner.
	 *
	 * @param  \App\DataTables\PartnersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(PartnersDataTable $dataTable)
    {
        return $this->partnerWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Partner.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->partnerWebService->create();
    }

    /**
     * Store a newly created Partner in storage.
     *
     * @param  CreatePartnerRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreatePartnerRequest $request): RedirectResponse
    {
        return $this->partnerWebService->store($request);
    }

    /**
     * Display the specified Partner.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->partnerWebService->show($id);
    }

    /**
     * Show the form for editing the specified Partner.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->partnerWebService->edit($id);
    }

    /**
     * Update the specified Partner in storage.
     *
     * @param  int                   $id
     * @param  UpdatePartnerRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdatePartnerRequest $request): RedirectResponse
    {
        return $this->partnerWebService->update($id, $request);
    }

    /**
     * Remove the specified Partner from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->partnerWebService->destroy($id);
    }
}
