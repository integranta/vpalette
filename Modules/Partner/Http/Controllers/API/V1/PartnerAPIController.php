<?php

namespace Modules\Partner\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Partner\Http\Requests\API\V1\CreatePartnerAPIRequest;
use Modules\Partner\Http\Requests\API\V1\UpdatePartnerAPIRequest;
use Modules\Partner\Services\API\V1\PartnerApiService;

/**
 * Class PartnerController
 *
 * @package Modules\Partner\Http\Controllers\API\V1
 */
class PartnerAPIController extends Controller
{
    /** @var  PartnerApiService */
    private $partnerApiService;

    /**
     * PartnerAPIController constructor.
     *
     * @param  PartnerApiService  $service
     */
    public function __construct(PartnerApiService $service)
    {
        $this->partnerApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->partnerApiService->index($request);
    }

    /**
     * @param  CreatePartnerAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreatePartnerAPIRequest $request): JsonResponse
    {
        return $this->partnerApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->partnerApiService->show($id);
    }

    /**
     * @param  UpdatePartnerAPIRequest  $request
     * @param  int                      $id
     *
     * @return JsonResponse
     */
    public function update(UpdatePartnerAPIRequest $request, int $id): JsonResponse
    {
        return $this->partnerApiService->update($request, $id);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->partnerApiService->destroy($id);
    }

    /**
     * @param  int  $partner
     *
     * @return JsonResponse
     */
    public function offers(int $partner): JsonResponse
    {
        return $this->partnerApiService->offers($partner);
    }
}
