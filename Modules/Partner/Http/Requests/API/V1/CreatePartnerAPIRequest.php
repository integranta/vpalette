<?php

namespace Modules\Partner\Http\Requests\API\V1;

use Modules\Partner\Models\Partner;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreatePartnerAPIRequest
 *
 * @package Modules\Partner\Http\Requests\API\V1
 */
class CreatePartnerAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Partner::$rules;
    }
}
