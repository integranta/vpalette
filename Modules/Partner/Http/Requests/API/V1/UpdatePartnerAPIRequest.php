<?php

namespace Modules\Partner\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePartnerAPIRequest
 *
 * @package Modules\Partner\Http\Requests\API\V1
 */
class UpdatePartnerAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'string|email|unique:users,id,'.$this->input('user_id'),
            'inn' => 'required|string|unique:partner_shops,id,'.$this->input('partner_shop_id'),
            'kpp' => 'required|string|unique:partner_shops,id,'.$this->input('partner_shop_id')
        ];
    }
}
