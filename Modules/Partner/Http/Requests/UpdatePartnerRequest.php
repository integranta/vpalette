<?php

namespace Modules\Partner\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePartnerRequest
 *
 * @package Modules\Partner\Http\Requests
 */
class UpdatePartnerRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'users.email' => 'string|email|unique:users,id,'.$this->input('user_id'),
            'partner.email' => 'string|email|unique:users,id,'.$this->input('user_id'),
            'partnerShops.*.inn' => 'required|string|unique:partner_shops,id,'.$this->input('partner_shop_id'),
            'partnerShops.*.kpp' => 'required|string|unique:partner_shops,id,'.$this->input('partner_shop_id')
        ];
        
        return $rules;
    }
}
