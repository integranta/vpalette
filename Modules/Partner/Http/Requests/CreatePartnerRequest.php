<?php

namespace Modules\Partner\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreatePartnerRequest
 *
 * @package Modules\Partner\Http\Requests
 */
class CreatePartnerRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'email' => 'required|email|string|unique:users,email',
            'phone' => 'required',
            'shop_name' => 'required|string',
            'law_shop_name' => 'required|string',
            'inn' => 'required|integer|min:12',
            'kpp' => 'required|integer|min:9',
            'city' => 'required|string',
            'address' => 'required|string',
            'post_index' => 'required|integer|min:6'
        ];
    }
}
