<?php namespace Modules\Partner\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Partner\Tests\ApiTestTrait;
use Modules\Partner\Tests\TestCase;
use Modules\Partner\Tests\Traits\MakePartnerTrait;

/**
 * Class PartnerApiTest
 *
 * @package Modules\Partner\Tests\APIs
 */
class PartnerApiTest extends TestCase
{
    use MakePartnerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_partner()
    {
        $partner = $this->fakePartnerData();
        $this->response = $this->json('POST', '/api/partners', $partner);
        
        $this->assertApiResponse($partner);
    }
    
    /**
     * @test
     */
    public function test_read_partner()
    {
        $partner = $this->makePartner();
        $this->response = $this->json('GET', '/api/partners/'.$partner->id);
        
        $this->assertApiResponse($partner->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_partner()
    {
        $partner = $this->makePartner();
        $editedPartner = $this->fakePartnerData();
        
        $this->response = $this->json('PUT', '/api/partners/'.$partner->id, $editedPartner);
        
        $this->assertApiResponse($editedPartner);
    }
    
    /**
     * @test
     */
    public function test_delete_partner()
    {
        $partner = $this->makePartner();
        $this->response = $this->json('DELETE', '/api/partners/'.$partner->id);
        
        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/partners/'.$partner->id);
        
        $this->response->assertStatus(404);
    }
}
