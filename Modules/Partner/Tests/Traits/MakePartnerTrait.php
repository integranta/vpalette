<?php namespace Modules\Partner\Tests\Traits;

use App;
use Faker\Factory as Faker;
use Modules\Partner\Models\Partner;
use Modules\Partner\Repositories\PartnerRepository;

/**
 * Trait MakePartnerTrait
 *
 * @package Modules\Partner\Tests\Traits
 */
trait MakePartnerTrait
{
    /**
     * Create fake instance of Partner and save it in database
     *
     * @param  array  $partnerFields
     *
     * @return Partner
     */
    public function makePartner($partnerFields = [])
    {
        /** @var PartnerRepository $partnerRepo */
        $partnerRepo = App::make(PartnerRepository::class);
        $theme = $this->fakePartnerData($partnerFields);
        return $partnerRepo->create($theme);
    }
    
    /**
     * Get fake instance of Partner
     *
     * @param  array  $partnerFields
     *
     * @return Partner
     */
    public function fakePartner($partnerFields = [])
    {
        return new Partner($this->fakePartnerData($partnerFields));
    }
    
    /**
     * Get fake data of Partner
     *
     * @param  array  $partnerFields
     *
     * @return array
     */
    public function fakePartnerData($partnerFields = [])
    {
        $fake = Faker::create();
        
        return array_merge([
            'company' => $fake->word,
            'law_shop_name' => $fake->word,
            'inn' => $fake->word,
            'kpp' => $fake->word,
            'address' => $fake->word,
            'city' => $fake->word,
            'post_index' => $fake->word,
            'email' => $fake->word,
            'phone' => $fake->word,
            'user_id' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $partnerFields);
    }
}
