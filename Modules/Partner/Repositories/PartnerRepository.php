<?php

namespace Modules\Partner\Repositories;

use App\Repositories\BaseRepository;
use App\User;
use Arr;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Partner\Contracts\PartnerContract;
use Modules\Partner\Models\Partner;
use Str;

/**
 * Class PartnerRepository
 *
 * @package Modules\Partner\Repositories
 * @version June 24, 2019, 9:43 am UTC
 */
class PartnerRepository extends BaseRepository implements PartnerContract
{
    /**
     * PartnerRepository constructor.
     *
     * @param  Partner  $model
     */
    public function __construct(Partner $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listPartners(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('partner.cache_key').$postfix,
            config('partner.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return Partner
     */
    public function findPartnerById(int $id): ?Partner
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return Partner
     */
    public function createPartner(array $params): Partner
    {
        $partner = User::create($this->getUserFields($params));
        $partner->assignRole('Partner');
        $partner->partner()->create($this->getPartnerFields($params));
        $partner->partner->partnerShops()->create($this->getPartnerShopsFields($params), 'id');
        $partner->save();
        
        /**
         * Поджигаем событие для отправки письма
         * для нового зарегистрированного партнёра (пользователя)
         * на его электронную почту со сгенерированным паролем.
         *
         * А также запускаем новую Job для отправки уведомления администратору.
         */
        // Event::dispatch(new PartnerRegistered($partner, $params['password']));
        $admin = User::role('admin')->get();
        // Event::dispatch(new NotifyRegisterNewPartner($admin, $partner));
        return $partner;
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Partner
     */
    public function updatePartner(array $params, int $id): Partner
    {
        $userId = $this->model::find($id)->user->id;
        $partner = User::with(['partner', 'partner.partnerShops'])->find($userId);
        $partner->update(Arr::get($params, 'users'));
        $partner->save();
        
        $partner = $this->update(Arr::get($params, 'partner'), $id);
        
        return $partner;
    }
    
    /**
     * @param $id
     *
     * @return bool
     */
    public function deletePartner(int $id): bool
    {
        return $this->delete($id);
    }
    
    /**
     * @var array
     */
    protected $partnerData = [
        'first_name',
        'last_name',
        'shops',
        'password',
        'shop_name'
    ];
    
    /**
     * @var array
     */
    protected $userData = [
        'shops'
    ];
    
    
    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getPartnerFields(array $params): array
    {
        return Arr::except($params, $this->partnerData);
    }
    
    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getUserFields(array $params): array
    {
        return Arr::except($params, $this->userData);
    }
    
    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getIssuesPartnerShopsFields(array $params): array
    {
        return [
            'id' => Arr::get($params, 'partner_shop_id'),
            'shop_name' => Arr::get($params, 'shop_name'),
            'law_shop_name' => Arr::get($params, 'law_shop_name'),
            'inn' => Arr::get($params, 'inn'),
            'kpp' => Arr::get($params, 'kpp'),
            'address' => Arr::get($params, 'address'),
            'city' => Arr::get($params, 'city'),
            'post_index' => Arr::get($params, 'post_index'),
        ];
    }
    
    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getPartnerShopsFields(array $params): array
    {
        return [
            'shop_name' => Arr::get($params, 'shop_name'),
            'law_shop_name' => Arr::get($params, 'law_shop_name'),
            'inn' => Arr::get($params, 'inn'),
            'kpp' => Arr::get($params, 'kpp'),
            'address' => Arr::get($params, 'address'),
            'city' => Arr::get($params, 'city'),
            'post_index' => Arr::get($params, 'post_index'),
        ];
    }
    
    
    /**
     * @param  int  $id
     *
     * @return array
     */
    public function getOffers(int $id): array
    {
        $partner = $this->find($id);
        
        $shopOffers = [];
        
        foreach ($partner->partnerShops as $partnerShop) {
            $shopOffers[$partnerShop['shop_name']] = $partnerShop->offers;
            foreach ($partnerShop->offers as $key => $offer) {
                $shopOffers[$partnerShop['shop_name']][$key]['name'] = $offer->product->name;
                Arr::except($shopOffers[$partnerShop['shop_name']][$key], 'product');
            }
        }
        
        return $shopOffers;
    }
}
