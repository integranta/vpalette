<?php

namespace Modules\Partner\Models;

use App\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerProduct\Models\PartnerProduct;
use Modules\PartnerShop\Models\PartnerShop;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Partner
 *
 * @package Modules\Partner\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $email
 * @property string $phone
 * @property int|null $userId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerProduct\Models\PartnerProduct[] $partnerProducts
 * @property-read int|null $partnerProductsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerShop\Models\PartnerShop[] $partnerShops
 * @property-read int|null $partnerShopsCount
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Partner\Models\Partner onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Partner\Models\Partner whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Partner\Models\Partner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Partner\Models\Partner withoutTrashed()
 * @mixin \Eloquent
 */
class Partner extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'partners';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Отношение, которые всегда должны быть загружены.
     *
     * @var array
     */
    protected $with = [
        'user',
        'partnerShops'
    ];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'phone',
        'active',
        'sort'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'email' => 'string',
        'phone' => 'string',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|string|email|',
        'phone' => 'required|string|phone:RU',
        'active' => 'required|boolean',
        'sort' => 'required|integer'
    ];

    /**
     * Получить пользователя, котором принадлежит роль "Партнёр".
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получить список магазинов принадлежащие партнёру.
     *
     * @return HasMany
     */
    public function partnerShops(): HasMany
    {
        return $this->hasMany(PartnerShop::class);
    }

    /**
     * "Загружащий" метод модели.
     *
     * @return void
     */
    public static function boot(): void
    {
        parent::boot();
        self::deleting(function ($partner) {
            $partner->partnerShops()->each(function ($shop) {
                $shop->delete();
            });
        });
    }

    /**
     * Получить партнёрские товары, которые предложил добавить партнёр.
     *
     * @return HasMany
     */
    public function partnerProducts(): HasMany
    {
        return $this->hasMany(PartnerProduct::class);
    }
}
