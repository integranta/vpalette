<?php

namespace Modules\Partner\Listeners;

use Mail;
use Modules\Partner\Emails\RegistrationEmail;
use Modules\Partner\Events\PartnerRegistered;

/**
 * Class SendRegisterNotification
 *
 * @package Modules\Partner\Listeners
 */
class SendRegisterNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Handle the event.
     *
     * @param  PartnerRegistered  $event
     *
     * @return void
     */
    public function handle(PartnerRegistered $event)
    {
        Mail::to($event->user->email)->send(new RegistrationEmail($event->user, $event->password));
    }
}
