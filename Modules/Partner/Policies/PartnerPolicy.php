<?php

namespace Modules\Partner\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Partner\Models\Partner;

/**
 * Class PartnerPolicy
 *
 * @package Modules\Partner\Policies
 */
class PartnerPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can create partners.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if ($user->can('show any partners')) {
            return true;
        }
    }
    
    /**
     * Determine whether the user can view the partner.
     *
     * @param  User|null  $user
     * @param  Partner    $partner
     *
     * @return mixed
     */
    public function view(?User $user, Partner $partner)
    {
        if ($partner->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished partners')) {
            return true;
        }
        
        return $user->id == $partner->user_id;
    }
    
    /**
     * Determine whether the user can create partners.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create partners')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the partner.
     *
     * @param  User     $user
     * @param  Partner  $partner
     *
     * @return mixed
     */
    public function update(User $user, Partner $partner)
    {
        if ($user->can('edit all partners')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the partner.
     *
     * @param  User     $user
     * @param  Partner  $partner
     *
     * @return mixed
     */
    public function delete(User $user, Partner $partner)
    {
        if ($user->can('delete any partner')) {
            return true;
        }
        
        return false;
    }
}
