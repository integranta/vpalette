<?php

namespace Modules\Partner\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Partner\Models\Partner;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Class PartnerDatabaseSeeder
 *
 * @package Modules\Partner\Database\Seeders
 */
class PartnerDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Partner::class, 10)
            ->create()
            ->each(function ($partner) {
                $partner->partnerShops()->save(factory(PartnerShop::class)->make());
            });
    }
}
