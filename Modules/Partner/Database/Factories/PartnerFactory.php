<?php

/** @var Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Partner\Models\Partner;

$factory->define(Partner::class,
    function (Faker $faker) {
        return [
            'active' => true,
            'sort' => $faker->numberBetween(1, 500),
            'email' => $faker->safeEmail,
            'phone' => $faker->phoneNumber,
            'user_id' => \factory(User::class)
        ];
    });
