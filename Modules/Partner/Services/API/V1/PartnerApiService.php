<?php


namespace Modules\Partner\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Partner\Contracts\PartnerContract;
use Modules\Partner\Http\Requests\API\V1\CreatePartnerAPIRequest;
use Modules\Partner\Http\Requests\API\V1\UpdatePartnerAPIRequest;
use Modules\Partner\Transformers\PartnerResource;
use Str;

/**
 * Class PartnerApiService
 *
 * @package Modules\Partner\Services\API\V1
 */
class PartnerApiService extends BaseService
{
	/** @var  PartnerContract */
	private $partnerRepository;
	
	/**
	 * PartnerAPIController constructor.
	 *
	 * @param  PartnerContract  $partnerRepo
	 */
	public function __construct(PartnerContract $partnerRepo)
	{
		$this->partnerRepository = $partnerRepo;
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function index(Request $request): JsonResponse
	{
		$partners = PartnerResource::collection($this->partnerRepository->listPartners());
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partner::messages.success_retrieve'),
			$partners
		);
	}
	
	/**
	 * @param  CreatePartnerAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function store(CreatePartnerAPIRequest $request): JsonResponse
	{
		$input = $request->all();
		$input['password'] = Str::random();
		
		$partner = $this->partnerRepository->createPartner($input);
		
		if (!$partner) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('partner::messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('partner::messages.success_create'),
			$partner
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function show(int $id): JsonResponse
	{
		$partner = PartnerResource::make($this->partnerRepository->findPartnerById($id));
		
		if ($partner === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('partner::messages.not_found', ['id' => $id])
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partner::messages.success_found', ['id' => $id]),
			$partner
		);
	}
	
	/**
	 * @param  UpdatePartnerAPIRequest  $request
	 * @param  int                      $id
	 *
	 * @return JsonResponse
	 */
	public function update(UpdatePartnerAPIRequest $request, int $id): JsonResponse
	{
		$partner = $this->partnerRepository->findPartnerById($id);
		
		if ($partner === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('partner::messages.not_found', ['id' => $id])
			);
		}
		
		$partner = $this->partnerRepository->updatePartner($request->all(), $id);
		
		if (!$partner) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('partner::messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partner::messages.success_update'),
			$partner
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function destroy(int $id): JsonResponse
	{
		$partner = $this->partnerRepository->findPartnerById($id);
		
		if ($partner === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('partner::messages.not_found', ['id' => $id])
			);
		}
		
		$partner = $this->partnerRepository->deletePartner($id);
		
		if (!$partner) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('partner::messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partner::messages.success_delete'),
			$partner
		);
	}
	
	/**
	 * @param  int  $partnerId
	 *
	 * @return JsonResponse
	 */
	public function offers(int $partnerId): JsonResponse
	{
		$partner = $this->partnerRepository->findPartnerById($partnerId);
		
		if (!$partner) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('partner::messages.not_found', ['id' => $partnerId])
			);
		}
		
		$offers = $this->partnerRepository->getOffers($partnerId);
		
		
		if (!$offers) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('partner::messages.error_load_offers')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partner::messages.success_load_offers'),
			$offers
		);
	}
}
