<?php


namespace Modules\Partner\Services;


use App\DataTables\PartnersDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Partner\Contracts\PartnerContract;
use Modules\Partner\Http\Requests\CreatePartnerRequest;
use Modules\Partner\Http\Requests\UpdatePartnerRequest;
use Modules\Partner\Models\Partner;
use Str;

/**
 * Class PartnerWebService
 *
 * @package Modules\Partner\Services
 */
class PartnerWebService extends BaseService
{
	/** @var  PartnerContract */
	private $partnerRepository;
	
	/**
	 * PartnerController constructor.
	 *
	 * @param  PartnerContract  $partnerRepo
	 */
	public function __construct(PartnerContract $partnerRepo)
	{
		$this->partnerRepository = $partnerRepo;
	}
	
	/**
	 * Display a listing of the Partner.
	 *
	 * @param  \App\DataTables\PartnersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(PartnersDataTable $dataTable)
	{
		$this->setPageTitle(
			__('partner::messages.title'),
			__('partner::messages.index_subtitle'),
			__('partner::messages.index_leadtext')
		);
		return $dataTable->render('partner::partners.index');
	}
	
	/**
	 * Show the form for creating a new Partner.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$model = new Partner();
		
		$this->setPageTitle(
			__('partner::messages.title'),
			__('partner::messages.create_subtitle'),
			__('partner::messages.create_leadtext')
		);
		return view('partner::partners.create', compact('model'));
	}
	
	/**
	 * Store a newly created Partner in storage.
	 *
	 * @param  CreatePartnerRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreatePartnerRequest $request): RedirectResponse
	{
		$params = $request->all();
		$params['password'] = Str::random();
		
		$partner = $this->partnerRepository->createPartner($params);
		
		if (!$partner) {
			return $this->responseRedirectBack(
				__('partner::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partners.index',
			__('partner::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified Partner.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(int $id): View
	{
		$model = $this->partnerRepository->findPartnerById($id);
		
		if ($model === null) {
			return $this->responseRedirect(
				'admin.partners.index',
				__('partner::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('partner::messages.title'),
			__('partner::messages.show_subtitle', ['name' => $model->user->name]),
			__('partner::messages.show_leadtext', ['name' => $model->user->name])
		);
		return view('partner::partners.show', compact('model'));
	}
	
	/**
	 * Show the form for editing the specified Partner.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$model = $this->partnerRepository->findPartnerById($id);
		
		if ($model === null) {
			return $this->responseRedirect(
				'admin.partners.index',
				__('partner::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('partner::messages.title'),
			__('partner::messages.edit_subtitle', ['name' => $model->user->name]),
			__('partner::messages_edit_leadtext', ['name' => $model->user->name])
		);
		return view('partner::partners.edit', compact('model'));
	}
	
	/**
	 * Update the specified Partner in storage.
	 *
	 * @param  int                   $id
	 * @param  UpdatePartnerRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function update(int $id, UpdatePartnerRequest $request): RedirectResponse
	{
		$partner = $this->partnerRepository->updatePartner($request->all(), $id);
		
		if (!$partner) {
			return $this->responseRedirectBack(
				__('partner::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.partners.index',
			__('partner::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified Partner from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 *
	 */
	public function destroy(int $id): RedirectResponse
	{
		$partner = $this->partnerRepository->deletePartner($id);
		
		if (!$partner) {
			return $this->responseRedirectBack(
				__('partner::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.partners.index',
			__('partner::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}