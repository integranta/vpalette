<?php

namespace Modules\Partner\Events;

use App\User;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class PartnerRegistered
 *
 * @package Modules\Partner\Events
 */
class PartnerRegistered
{
    use SerializesModels, Dispatchable;
    
    /**
     * @var User
     */
    public $user;
    
    /**
     * @var
     */
    public $password;
    
    /**
     * Create a new event instance.
     *
     * @param  User  $user
     * @param        $password
     */
    public function __construct(User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }
    
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
