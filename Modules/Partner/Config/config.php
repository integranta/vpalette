<?php

return [
    'name' => 'Partner',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all partners as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('PARTNER_CACHE_KEY','index_partners'),
    'cache_ttl' => env('PARTNER_CACHE_TTL',360000),
];
