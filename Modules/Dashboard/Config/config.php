<?php

return [
	'name'      => 'Dashboard',
	
	/*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all brands as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
	'cache_key' => env('DASHBOARD_CACHE_KEY', 'index_dashboard'),
	'cache_ttl' => env('DASHBOARD_CACHE_TTL', 360000),
];
