<?php


namespace Modules\Dashboard\Services;


use App\Services\BaseService;
use Cache;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Dashboard\Events\ChartJsDataEvent;
use Modules\Dashboard\Models\Dashboard;
use Modules\News\Contracts\NewsContract;
use Modules\Partner\Contracts\PartnerContract;
use Modules\Product\Contracts\ProductContract;
use Modules\User\Contracts\UserContract;

class DashboardWebService extends BaseService
{
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Factory|View
	 */
	public function index(): View
	{
		$this->setPageTitle(
			__('dashboard::messages.title'),
			__('dashboard::messages.subtitle'),
			__('dashboard::messages.index_leadtext')
		);
		
		
		$postfix = Carbon::parse(Dashboard::orderByDesc('updated_at')
			->first(['updated_at'])
			->updated_at)
			->toDateString();
		
		$dashboard = Cache::remember(
			config('dashboard.cache_key').$postfix,
			config('dashboard.cache_ttl'),
			static function () {
				return Dashboard::first([
					'id',
					'count_admins',
					'count_news',
					'count_products',
					'count_partners',
					'count_orders'
				]);
			});
		
		
		return view('dashboard::index', compact('dashboard'));
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return array
	 */
	public function chartjsData(Request $request)
	{
		$result = [
			'labels'   => [
				'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь',
				'Декабрь'
			],
			'datasets' => array(
				[
					'label'           => 'Продажи',
					'backgroundColor' => '#6777ef',
					'data'            => [12500, 55000, 1000, 3000, 1500, 5000, 1000, 30400, 1500, 54000, 1000, 30400]
				]
			)
		];
		return $result;
		
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return array
	 */
	public function realtimeData(Request $request)
	{
		$result = [
			'labels'   => [
				'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь',
				'Декабрь'
			],
			'datasets' => array(
				[
					'label'           => 'Продажи',
					'backgroundColor' => '#6777ef',
					'data'            => [500, 530, 3100, 5304, 1040, 2400, 100, 300, 180, 500, 200, 300]
				]
			)
		];
		
		if ($request->has('isRealtime')) {
			if ($request->input('isRealtime') === 'Y') {
				$result = [
					'labels'   => [
						'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь',
						'Ноябрь', 'Декабрь'
					],
					'datasets' => array(
						[
							'label'           => 'Продажи',
							'backgroundColor' => '#6777ef',
							'data'            => [21420, 7661, 3210, 5444, 1740, 2470, 170, 350, 580, 550, 250, 350]
						]
					)
				];
				event(new ChartJsDataEvent($result));
			}
		}
		
		return $result;
	}
}
