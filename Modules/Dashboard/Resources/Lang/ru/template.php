<?php

return [
	'total_admins'   => 'Всего администраторов',
	'total_news'     => 'Всего новостей',
	'total_products' => 'Всего продуктов',
	'total_partners' => 'Всего партнеров',
	'notification'   => [
		'title'         => 'Уведомления',
		'not_found'     => 'Уведомления не найдены',
		'check_as_read' => 'Отменить как прочитанное'
	],
	'user'           => [
		'short_greet_message' => 'Привет, ',
		'greet_message'       => 'Добро пожаловать, ',
		'settings_profile'    => 'Настройки профиля',
		'logout'              => 'Выйти'
	]
];
