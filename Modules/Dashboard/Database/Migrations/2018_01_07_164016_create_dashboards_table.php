<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboards', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('count_admins')->default(0);
                $table->unsignedInteger('count_news')->default(0);
                $table->unsignedInteger('count_products')->default(0);
                $table->unsignedInteger('count_partners')->default(0);
                $table->unsignedInteger('count_orders')->default(0);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboards');
    }
}
