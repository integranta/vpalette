<?php

namespace Modules\Dashboard\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

/**
 * Class ChartJsDataEvent
 *
 * @package Modules\Dashboard\Events
 */
class ChartJsDataEvent implements ShouldBroadcast
{
    use SerializesModels;
    
    /**
     * @var
     */
    public $result;
    
    /**
     * Create a new event instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->result = $data;
    }
    
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['chartJs-event'];
    }
}
