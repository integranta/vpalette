<?php

namespace Modules\Dashboard\Http\Controllers;

use Modules\Dashboard\Services\DashboardWebService;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class DashboardController
 *
 * @package Modules\Dashboard\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * @var DashboardWebService
     */
    private $service;

    public function __construct(DashboardWebService $service)
    {
        $this->service = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'chartjsData']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        return $this->service->index();
    }

    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function chartjsData(Request $request)
    {
        return $this->service->chartjsData($request);
    }

    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function realtimeData(Request $request)
    {
        return $this->service->realtimeData($request);
    }
}
