<?php

namespace Modules\Dashboard\Models;

use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Dashboard
 *
 * @package Modules\Dashboard\Models
 * @property int $id
 * @property int $countAdmins
 * @property int $countNews
 * @property int $countProducts
 * @property int $countPartners
 * @property int $countOrders
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereCountAdmins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereCountNews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereCountOrders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereCountPartners($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereCountProducts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Dashboard\Models\Dashboard whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Dashboard extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'dashboards';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'count_admins',
        'count_news',
        'count_products',
        'count_partners',
        'count_orders'
    ];
}
