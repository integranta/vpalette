<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Dashboard\Http\Controllers\DashboardController;

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		Route::get('/dashboard', [DashboardController::class, 'index'])
			->name('dashboard.index');
		Route::get('/dashboard/json', [DashboardController::class, 'chartjsData'])
			->name('dashboard.json');
		Route::get('/dashboard/realtime', [DashboardController::class, 'realtimeData'])
			->name('dashboard.realtime');
	});
