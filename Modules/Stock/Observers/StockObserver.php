<?php

namespace Modules\Stock\Observers;

use App\Traits\Cacheable;
use Exception;
use Modules\Stock\Models\Stock;

/**
 * Class StockObserver
 *
 * @package Modules\Stock\Observers
 */
class StockObserver
{
    use Cacheable;
    
    /**
     * Handle the stock "created" event.
     *
     * @param  Stock  $stock
     *
     * @return void
     * @throws Exception
     */
    public function created(Stock $stock): void
    {
        $this->incrementCountItemsInCache($stock, config('stock.cache_key'));
    }
    
    /**
     * Handle the stock "updated" event.
     *
     * @param  Stock  $stock
     *
     * @return void
     */
    public function updated(Stock $stock): void
    {
        //
    }
    
    /**
     * Handle the stock "deleted" event.
     *
     * @param  Stock  $stock
     *
     * @return void
     */
    public function deleted(Stock $stock): void
    {
        $this->decrementCountItemsInCache($stock, config('stock.cache_key'));
    }
    
    /**
     * Handle the stock "restored" event.
     *
     * @param  Stock  $stock
     *
     * @return void
     */
    public function restored(Stock $stock): void
    {
        $this->incrementCountItemsInCache($stock, config('stock.cache_key'));
    }
    
    /**
     * Handle the stock "force deleted" event.
     *
     * @param  Stock  $stock
     *
     * @return void
     */
    public function forceDeleted(Stock $stock): void
    {
        $this->decrementCountItemsInCache($stock, config('stock.cache_key'));
    }
}
