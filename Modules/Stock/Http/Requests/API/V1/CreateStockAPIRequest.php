<?php

namespace Modules\Stock\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Stock\Models\Stock;

/**
 * Class CreateStockAPIRequest
 *
 * @package Modules\Stock\Http\Requests\API\V1
 */
class CreateStockAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Stock::$rules;
    }
}
