<?php

namespace Modules\Stock\Http\Controllers;

use App\DataTables\StocksDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Stock\Http\Requests\CreateStockRequest;
use Modules\Stock\Http\Requests\UpdateStockRequest;
use Modules\Stock\Services\StockWebService;


/**
 * Class StockController
 *
 * @package Modules\Stock\Http\Controllers
 */
class StockController extends Controller
{
    /** @var StockWebService */
    private $stockWebService;

    /**
     * StockController constructor.
     *
     * @param  StockWebService  $service
     */
    public function __construct(StockWebService $service)
    {
        $this->stockWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Stocks.
	 *
	 * @param  \App\DataTables\StocksDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(StocksDataTable $dataTable)
    {
        return $this->stockWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Stock.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->stockWebService->create();
    }

    /**
     * Store a newly created Stock in storage.
     *
     * @param  CreateStockRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateStockRequest $request): RedirectResponse
    {
        return $this->stockWebService->store($request);
    }

    /**
     * Display the specified Stock.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->stockWebService->show($id);
    }

    /**
     * Show the form for editing the specified Stock.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->stockWebService->edit($id);
    }

    /**
     * Update the specified Stock in storage.
     *
     * @param  int                 $id
     * @param  UpdateStockRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateStockRequest $request): RedirectResponse
    {
        return $this->stockWebService->update($id, $request);
    }

    /**
     * Remove the specified Stock from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->stockWebService->destroy($id);
    }
}
