<?php

namespace Modules\Stock\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Stock\Http\Requests\API\V1\CreateStockAPIRequest;
use Modules\Stock\Http\Requests\API\V1\UpdateStockAPIRequest;
use Modules\Stock\Services\API\V1\StockApiService;

/**
 * Class StockController
 *
 * @package Modules\Stock\Http\Controllers\API\V1
 */
class StockAPIController extends Controller
{
    /** @var  StockApiService */
    private $stockApiService;

    /**
     * StockAPIController constructor.
     *
     * @param  StockApiService  $service
     */
    public function __construct(StockApiService $service)
    {
        $this->stockApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }


    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->stockApiService->index($request);
    }

    /**
     * @param  CreateStockAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateStockAPIRequest $request): JsonResponse
    {
        return $this->stockApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->stockApiService->show($id);
    }

    /**
     * @param  int                    $id
     * @param  UpdateStockAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateStockAPIRequest $request): JsonResponse
    {
        return $this->stockApiService->update($id, $request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->stockApiService->destroy($id);
    }
}
