import setDateRangePicker from "../../../../../resources/js/shared/daterangepicker";
import { tinyMCEConfiguration } from "../../../../../resources/js/shared/tinyMCEConfig";

$(document).ready(() => {
    tinymce.init({
        ...tinyMCEConfiguration,
        selector: ".tinyMCE"
    });

    setDateRangePicker('#date_start');
    setDateRangePicker('#date_end');
});
