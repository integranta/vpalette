@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/stock.css') }}">

@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.stocks.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')

                    {!! Form::model($model,
                        [
                            'route' => ['admin.stocks.update', $model->id],
                            'method' => 'patch',
                            'files' => 'true'
                        ])
                    !!}
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                <li class="nav-item">
                                    <a
                                        class="nav-link active"
                                        id="element-tab"
                                        data-toggle="tab"
                                        href="#element"
                                        role="tab"
                                        aria-controls="element"
                                        aria-selected="true"
                                    >
                                        Элемент
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a
                                        class="nav-link"
                                        id="preview-tab"
                                        data-toggle="tab"
                                        href="#preview"
                                        role="tab"
                                        aria-controls="preview"
                                        aria-selected="false">
                                        Описание
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a
                                        class="nav-link"
                                        id="detail-tab"
                                        data-toggle="tab"
                                        href="#detail"
                                        role="tab"
                                        aria-controls="detail"
                                        aria-selected="false"
                                    >
                                        Детальная
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a
                                        class="nav-link"
                                        id="seo-tab"
                                        data-toggle="tab"
                                        href="#seo"
                                        role="tab"
                                        aria-controls="seo"
                                        aria-selected="false"
                                    >
                                        SEO
                                    </a>
                                </li>
                            </ul>
                            @include('stock::stocks.edit_fields')
                        </div>
                        <div class="card-footer">
                            <!-- Submit Field -->
                            <div class="form-group row col-mb-12">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-4 col-md-4">
                                    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('libs/js/tinymce/langs/ru.js') }}"></script>

    {{-- JS Libraies --}}
    <script src="{{ asset('libs/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('libs/js/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>

    {{--  Module JS File  --}}
    <script src="{{ asset('js/stock.js') }}"></script>
@endpush
