<?php

namespace Modules\Stock\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StockResource
 *
 * @package Modules\Stock\Transformers
 */
class StockResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'sort' => $this->sort,
            'slug' => $this->slug,
            'isActive' => $this->active,
            'previewText' => $this->preview_text,
            'previewImageSrc' => $this->preview_image,
            'detailText' => $this->detail_text,
            'detailImageSrc' => $this->detail_image,
            'dateStartAt' => $this->date_start,
            'dateEndAt' => $this->date_end,
            'humanDateAt' => $this->date,
            'countView' => $this->count_view,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
