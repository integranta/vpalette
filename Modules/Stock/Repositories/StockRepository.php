<?php

namespace Modules\Stock\Repositories;

use App\Repositories\BaseRepository;
use App\Traits\UploadAble;
use Arr;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Stock\Contracts\StockContract;
use Modules\Stock\Models\Stock;
use Str;

/**
 * Class StockRepository
 *
 * @package Modules\Stock\Repositories
 * @version August 5, 2019, 6:12 am UTC
 */
class StockRepository extends BaseRepository implements StockContract
{
    use UploadAble;
    
    /**
     * StockRepository constructor.
     *
     * @param  Stock  $model
     */
    public function __construct(Stock $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @var array $stockData
     */
    protected $stockData = [
        'title',
        'description',
        'keywords',
        'canonical_url',
        'google_tag_manager',
        'google_analytics',
        'ya_direct',
        'ya_metrica',
        '_method',
        '_token'
    ];
    
    /**
     * @var array $seoData
     */
    protected $seoData = [
        'name',
        'slug',
        'sort',
        'active',
        'preview_text',
        'preview_image',
        'detail_text',
        'detail_image',
        'date_start',
        'date_end',
        '_method',
        '_token'
    ];
    
    
    /**
     * Return stock fields
     *
     * @param  array  $input
     *
     * @return array
     */
    private function getStockFields(array $input): array
    {
        return Arr::except($input, $this->stockData);
    }
    
    /**
     * Return seo fields
     *
     * @param  array  $input
     *
     * @return array
     */
    private function getSeoFields(array $input): array
    {
        return Arr::except($input, $this->seoData);
    }
    
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listStock(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('stock.cache_key').$postfix,
            config('stock.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return Stock
     */
    public function findStockById(int $id): ?Stock
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return Stock
     */
    public function createStock(array $params): Stock
    {
        $stock = Stock::create($this->getStockFields($params));
        $stock->seo()->create($this->getSeoFields($params));
        $stock->save();
        
        return $stock;
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Stock
     */
    public function updateStock(array $params, int $id): Stock
    {
        $stock = Stock::find($id);
        $stock->update($this->getStockFields($params));
        $stock->seo()->update($this->getSeoFields($params));
        
        return $stock;
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteStock(int $id): bool
    {
        $stock = $this->findStockById($id);
        
        if ($stock->preview_image !== null) {
            $this->deleteOne($stock->preview_image);
        }
        
        if ($stock->detail_image !== null) {
            $this->deleteOne($stock->detail_image);
        }
        
        return $this->delete($id);
    }
}
