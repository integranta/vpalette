<?php

namespace Modules\Stock\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Stock\Models\Stock;

/**
 * Class StockPolicy
 *
 * @package Modules\Stock\Policies
 */
class StockPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the stock.
     *
     * @param  User|null  $user
     * @param  Stock      $stock
     *
     * @return mixed
     */
    public function view(?User $user, Stock $stock)
    {
        if ($stock->active) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished stocks')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create stocks.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create stocks')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the stock.
     *
     * @param  User   $user
     * @param  Stock  $stock
     *
     * @return mixed
     */
    public function update(User $user, Stock $stock)
    {
        if ($user->can('edit all stocks')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the stock.
     *
     * @param  User   $user
     * @param  Stock  $stock
     *
     * @return mixed
     */
    public function delete(User $user, Stock $stock)
    {
        if ($user->can('delete any stock')) {
            return true;
        }
        
        return false;
    }
}
