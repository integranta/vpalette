<?php

namespace Modules\Stock\Models;

use App\Models\BaseModel;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Modules\Seo\Models\Seo;
use Rennokki\QueryCache\Traits\QueryCacheable;


/**
 * Class Stock
 *
 * @package Modules\Stock\Models
 * @property int $id
 * @property string|null $name
 * @property int $sort
 * @property string|null $slug
 * @property bool $active
 * @property string|null $previewText
 * @property null|string $previewImage
 * @property string|null $detailText
 * @property null|string $detailImage
 * @property mixed|null $dateStart
 * @property mixed|null $dateEnd
 * @property string $date
 * @property int $countView
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Builder|\Illuminate\Database\Eloquent\Model|object|null $next
 * @property-read \Builder|\Illuminate\Database\Eloquent\Model|\Stock|object|null $prev
 * @property-read \Modules\Seo\Models\Seo|null $seo
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Stock\Models\Stock onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereCountView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereDetailImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereDetailText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock wherePreviewImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock wherePreviewText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Stock\Models\Stock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Stock\Models\Stock withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Stock\Models\Stock withoutTrashed()
 * @mixin \Eloquent
 */
class Stock extends BaseModel
{
    use SoftDeletes;
    use SoftCascadeTrait;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'stocks';

    /**
     * Записи из указанных отношении должны быть каскадно "мягко" удалиться.
     * Удаляються с помощью стороннего пакета Askedio\SoftCascade.
     *
     * @link https://github.com/Askedio/laravel-soft-cascade#usage
     *
     * @return array
     */
    protected $softCascade = ['seo'];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'date_start',
        'date_end',
    ];

    /**
     * Отношение, которые всегда должны быть загружены.
     *
     * @var array
     */
    protected $with = ['seo'];

    /**
     * Формат хранения отметок времени модели.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'sort',
        'slug',
        'active',
        'preview_text',
        'preview_image',
        'detail_text',
        'date_start',
        'date_end',
        'detail_image',
        'count_view',
        'date'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'sort' => 'integer',
        'slug' => 'string',
        'active' => 'boolean',
        'preview_text' => 'string',
        'preview_image' => 'string',
        'detail_text' => 'string',
        'date_start' => 'datetime:Y-m-d H:i:s',
        'date_end' => 'datetime:Y-m-d H:i:s',
        'detail_image' => 'string',
        'count_view' => 'integer',
        'date' => 'string'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string',
        'sort' => 'integer',
        'slug' => 'unique:stocks|string',
        'active' => 'boolean',
        'preview_text' => 'nullable|string',
        'preview_image' => 'nullable|',
        'detail_text' => 'required|string',
        'detail_image' => 'nullable|',
        'date_start' => 'date',
        'date_end' => 'nullable|date',
        'count_view' => 'min:0|integer',
        'date' => 'string'
    ];



    /**
     * Получить отформатированную дату начало активности.
     *
     * @param  string|null  $date
     *
     * @return string
     */
    /*public function getDateStartAttribute(?string $date): ?string
    {
        return $date !== null ? Carbon::parse($date)->format('Y-m-d H:i:s') : null;
    }*/

    /**
     * Установить дату начала активности.
     *
     * @param  string|null  $value
     *
     * @return void
     */
    /*public function setDateStartAttribute(?string $value): void
    {
        $this->attributes['date_start'] = Carbon::createFromFormat('Y-m-d H:i:s', $value)->toString();
    }*/

    /**
     * @param $date
     *
     * @return string|null
     */
    /*public function getDateEndAttribute($date)
    {
        return !empty($date) ? Carbon::parse($date)
            ->format('Y-m-d H:i:s') : null;
    }*/

    /**
     * @param $value
     *
     * @return string|null
     */
    /*public function setDateEndAttribute($value)
    {
        return $this->attributes['date_end'] = !empty($value) ? Carbon::createFromFormat('Y-m-d H:i:s', $value)
            ->format('Y-m-d H:i:s') : null;
    }*/

    /**
     * Установить флаг активности.
     *
     * @param  string  $value
     *
     * @return void
     */
    public function setActiveAttribute(string $value): void
    {
        $this->attributes['active'] = $value == '1';
    }

    /**
     * Получить значение флага активности.
     *
     * @param  string  $value
     *
     * @return bool
     */
    public function getActiveAttribute(string $value): bool
    {
        return $value == "1";
    }

    /**
     * Получить полную подготовленную дату.
     *
     * @return string
     */
    public function getDateAttribute(): string
    {
        $startDate = Carbon::parse($this->date_start)->locale('ru');
        $endDate = Carbon::parse($this->date_end)->locale('ru');

        if (!empty($this->date_end)) {
            return 'С '.$startDate->format('d').' по '.$endDate->isoFormat('DD MMMM YYYY');
        }

        return 'C '.$startDate->isoFormat('DD MMMM YYYY');
    }


    /**
     * Получить следующую акцию.
     *
     * @return Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function getNextAttribute(): Model
    {
        $result = self::query()
            ->where('id', '>', $this->id)
            ->orderBy('id', 'asc')
            ->first();

        if ($result) {
            $this->attributes['current'] = 'nullable';
            return $result;
        }

        return self::query()->orderBy('id', 'desc')->first();
    }

    /**
     * Получить ссылку на картинку анонса.
     *
     * @param  string|null  $value
     *
     * @return null|string
     */
    public function getPreviewImageAttribute(?string $value): ?string
    {
        return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
    }

    /**
     * Получить ссылку на детальную картинку.
     *
     * @param  string|null  $value
     *
     * @return null|string
     */
    public function getDetailImageAttribute(?string $value): ?string
    {
        return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
    }

    /**
     * Получить предыдущую акцию.
     *
     * @return Builder|\Illuminate\Database\Eloquent\Model|Stock|object|null
     */
    public function getPrevAttribute(): Model
    {
        $result = self::query()
            ->where('id', '<', $this->id)
            ->orderBy('id', 'desc')
            ->first();
        if ($result) {
            $this->attributes['current'] = 'null';
            return $result;
        }

        return self::first();
    }


    /**
     * Получить связанные SEO параметры для акций.
     *
     * @return MorphOne
     */
    public function seo(): MorphOne
    {
        return $this->morphOne(Seo::class, 'seoble');
    }
}
