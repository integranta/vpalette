const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/stock.js')
    .js(__dirname + '/Resources/Assets/js/detail.js', 'js/stock-detail.js')
    .js(__dirname + '/Resources/Assets/js/index.js', 'js/stock-index.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/stock.css', {
        implementation: require('node-sass')
    });

if (mix.inProduction()) {
    mix.version();
}
