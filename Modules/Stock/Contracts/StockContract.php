<?php


namespace Modules\Stock\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Stock\Models\Stock;

/**
 * Interface StockContract
 *
 * @package Modules\Stock\Contracts
 */
interface StockContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listStock(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Stock
     */
    public function findStockById(int $id): ?Stock;
    
    /**
     * @param  array  $params
     *
     * @return Stock
     */
    public function createStock(array $params): Stock;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Stock
     */
    public function updateStock(array $params, int $id): Stock;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteStock(int $id): bool;
}
