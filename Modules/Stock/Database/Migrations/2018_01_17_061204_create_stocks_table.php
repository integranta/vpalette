<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateStocksTable
 */
class CreateStocksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->unsignedSmallInteger('sort', false)->default(500);
            $table->string('slug')->nullable()->index();
            $table->boolean('active')
                ->default(true)
                ->nullable();
            $table->text('preview_text')->nullable();
            $table->string('preview_image')->nullable();
            $table->text('detail_text')->nullable();
            $table->string('detail_image')->nullable();
            $table->dateTime('date_start')->nullable();
            $table->dateTime('date_end')->nullable();
            $table->string('date')->nullable();
            $table->unsignedBigInteger('count_view')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stocks');
    }
}
