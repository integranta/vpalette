<?php

use Faker\Generator as Faker;
use Modules\Stock\Models\Stock;
use Illuminate\Http\File;

$factory->define(Stock::class,
    function (Faker $faker) {
        
        $name = $faker->sentence(10, false);
        $path = 'stocks/'.Str::of($name)->lower()->trim()->slug('_', 'ru');
        
        $image = 'public/storage/uploads/faker/stock/'.Str::of(random_int(1, 6)).'.jpg';
        $imageFile = new File($image);
        
        return [
            'name' => $name,
            'active' => true,
            'sort' => $faker->numberBetween(1, 500),
            'preview_text' => $faker->realText(100),
            'preview_image' => Storage::disk('uploads')->putFile($path, $imageFile),
            'detail_text' => $faker->realText(500),
            'detail_image' => Storage::disk('uploads')->putFile($path, $imageFile),
        ];
    });
