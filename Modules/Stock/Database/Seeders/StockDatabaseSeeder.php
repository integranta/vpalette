<?php

namespace Modules\Stock\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Stock\Models\Stock;

/**
 * Class StockDatabaseSeeder
 *
 * @package Modules\Stock\Database\Seeders
 */
class StockDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Stock::class, 20)->create();
    }
}
