<?php namespace Modules\Stock\Tests\Traits;

use App;
use Faker\Factory as Faker;
use Modules\Stock\Models\Stock;
use Modules\Stock\Repositories\StockRepository;

/**
 * Trait MakeStockTrait
 *
 * @package Modules\Stock\Tests\Traits
 */
trait MakeStockTrait
{
    /**
     * Create fake instance of Stock and save it in database
     *
     * @param  array  $stockFields
     *
     * @return Stock
     */
    public function makeStock($stockFields = [])
    {
        /** @var StockRepository $stockRepo */
        $stockRepo = App::make(StockRepository::class);
        $theme = $this->fakeStockData($stockFields);
        return $stockRepo->create($theme);
    }
    
    /**
     * Get fake instance of Stock
     *
     * @param  array  $stockFields
     *
     * @return Stock
     */
    public function fakeStock($stockFields = [])
    {
        return new Stock($this->fakeStockData($stockFields));
    }
    
    /**
     * Get fake data of Stock
     *
     * @param  array  $stockFields
     *
     * @return array
     */
    public function fakeStockData($stockFields = [])
    {
        $fake = Faker::create();
        
        return array_merge([
            'name' => $fake->word,
            'preview_text' => $fake->text,
            'preview_image' => $fake->word,
            'detail_text' => $fake->text,
            'date_start' => $fake->word,
            'date_end' => $fake->word,
            'detail_image' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $stockFields);
    }
}
