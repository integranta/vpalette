<?php namespace Modules\Stock\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Stock\Tests\ApiTestTrait;
use Modules\Stock\Tests\TestCase;
use Modules\Stock\Tests\Traits\MakeStockTrait;

/**
 * Class StockApiTest
 *
 * @package Modules\Stock\Tests\APIs
 */
class StockApiTest extends TestCase
{
    use MakeStockTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_stock()
    {
        $stock = $this->fakeStockData();
        $this->response = $this->json('POST', '/api/stocks', $stock);
        
        $this->assertApiResponse($stock);
    }
    
    /**
     * @test
     */
    public function test_read_stock()
    {
        $stock = $this->makeStock();
        $this->response = $this->json('GET', '/api/stocks/'.$stock->id);
        
        $this->assertApiResponse($stock->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_stock()
    {
        $stock = $this->makeStock();
        $editedStock = $this->fakeStockData();
        
        $this->response = $this->json('PUT', '/api/stocks/'.$stock->id, $editedStock);
        
        $this->assertApiResponse($editedStock);
    }
    
    /**
     * @test
     */
    public function test_delete_stock()
    {
        $stock = $this->makeStock();
        $this->response = $this->json('DELETE', '/api/stocks/'.$stock->id);
        
        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/stocks/'.$stock->id);
        
        $this->response->assertStatus(404);
    }
}
