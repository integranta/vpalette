<?php

return [
    'name' => 'Stock',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all stocks as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('STOCK_CACHE_KEY','index_stocks'),
    'cache_ttl' => env('STOCK_CACHE_TTL',360000),
];
