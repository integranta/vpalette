<?php


namespace Modules\Stock\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Stock\Contracts\StockContract;
use Modules\Stock\Http\Requests\API\V1\CreateStockAPIRequest;
use Modules\Stock\Http\Requests\API\V1\UpdateStockAPIRequest;
use Modules\Stock\Transformers\StockResource;

/**
 * Class StockApiService
 *
 * @package Modules\Stock\Services\API\V1
 */
class StockApiService extends BaseService
{
    /** @var  StockContract */
    private $stockRepository;
    
    /**
     * StockAPIController constructor.
     *
     * @param  StockContract  $stockRepo
     */
    public function __construct(StockContract $stockRepo)
    {
        $this->stockRepository = $stockRepo;
    }
    
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $stocks = StockResource::collection($this->stockRepository->listStock());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('stock::messages.success_retrieve'),
            $stocks
        );
    }
    
    /**
     * @param  CreateStockAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateStockAPIRequest $request): JsonResponse
    {
        $stock = $this->stockRepository->createStock($request->all());
        
        if (!$stock) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('stock::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('stock::messages.success_create'),
            $stock
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        
        $stock = StockResource::make($this->stockRepository->findStockById($id));
        
        if ($stock === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('stock::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('stock::messages.success_found', ['id' => $id]),
            $stock
        );
    }
    
    /**
     * @param  int                    $id
     * @param  UpdateStockAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateStockAPIRequest $request): JsonResponse
    {
        $stock = $this->stockRepository->findStockById($id);
        
        if ($stock === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('stock::messages.not_found', ['id' => $id])
            );
        }
        
        $stock = $this->stockRepository->updateStock($request->all(), $id);
        
        if (!$stock) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('stock::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('stock::messages.success_update'),
            $stock
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $stock = $this->stockRepository->findStockById($id);
        
        if ($stock === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('stock::messages.not_found', ['id' => $id])
            );
        }
        
        $stock = $this->stockRepository->deleteStock($id);
        
        if (!$stock) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('stock::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('stock::messages.success_delete')
        );
    }
}
