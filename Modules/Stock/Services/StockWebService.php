<?php


namespace Modules\Stock\Services;


use App\DataTables\StocksDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Stock\Contracts\StockContract;
use Modules\Stock\Http\Requests\CreateStockRequest;
use Modules\Stock\Http\Requests\UpdateStockRequest;
use Modules\Stock\Models\Stock;

/**
 * Class StockWebService
 *
 * @package Modules\Stock\Services
 */
class StockWebService extends BaseService
{
    /*** @var StockContract */
    protected $stockRepository;
    
    /**
     * StockController constructor.
     *
     * @param  StockContract  $stockRepo
     */
    public function __construct(StockContract $stockRepo)
    {
        $this->stockRepository = $stockRepo;
    }
	
	/**
	 * Display a listing of the Stocks.
	 *
	 * @param  \App\DataTables\StocksDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(StocksDataTable $dataTable)
    {
        $this->setPageTitle(
            __('stock::messages.title'),
            __('stock::messages.index_subtitle'),
            __('stock::messages.index_leadtext')
        );
        
        return $dataTable->render('stock::stocks.index');
    }
    
    /**
     * Show the form for creating a new Stock.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $model = new Stock();
        $seoInfo = $model->load('seo');
        
        $this->setPageTitle(
            __('stock::messages.title'),
            __('stock::messages.create_subtitle'),
            __('stock::messages.create_leadtext')
        );
        return view('stock::stocks.create', compact('model', 'seoInfo'));
    }
    
    /**
     * Store a newly created Stock in storage.
     *
     * @param  CreateStockRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateStockRequest $request): RedirectResponse
    {
        $stock = $this->stockRepository->createStock($request->all());
        
        if (!$stock) {
            return $this->responseRedirectBack(
                __('stock::message.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.stocks.index',
            __('stock::message.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Display the specified Stock.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        $model = $this->stockRepository->findStockById($id);
        
        if ($model === null) {
        	$this->responseRedirect(
        		'admin.stocks.index',
				__('stock::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
        
        $this->setPageTitle(
            __('stock::messages.title'),
            __('stock::messages.show_subtitle', ['name' => $model->name]),
            __('stock::messages.show_leadtext', ['name' => $model->name])
        );
        return view('stock::stocks.show', compact('model'));
    }
    
    /**
     * Show the form for editing the specified Stock.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        $model = $this->stockRepository->findStockById($id);
	
		if ($model === null) {
			$this->responseRedirect(
				'admin.stocks.index',
				__('stock::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
        
        $this->setPageTitle(
            __('stock::messages.title'),
            __('stock::messages.edit_subtitle', ['name' => $model->name]),
            __('stock::messages.edit_leadtext', ['name' => $model->name])
        );
        return view('stock::stocks.edit', compact('model'));
    }
    
    /**
     * Update the specified Stock in storage.
     *
     * @param  int                 $id
     * @param  UpdateStockRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateStockRequest $request): RedirectResponse
    {
        $stock = $this->stockRepository->updateStock($request->all(), $id);
        
        if (!$stock) {
            return $this->responseRedirectBack(
                __('stock::message.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirectBack(
            __('stock::message.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified Stock from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        $stock = $this->stockRepository->deleteStock($id);
        
        if (!$stock) {
            return $this->responseRedirectBack(
                __('stock::message.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.stocks.index',
            __('stock::message.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
}