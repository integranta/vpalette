const dotenvExpand = require('dotenv-expand');
dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/user.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/user.css', {
        implementation: require('node-sass')
    });

if (mix.inProduction()) {
    mix.version();
}
