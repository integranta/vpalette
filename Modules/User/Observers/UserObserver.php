<?php

namespace Modules\User\Observers;

use App\Traits\Cacheable;
use App\User;
use Exception;
use Hash;
use DB;

/**
 * Class UserObserver
 *
 * @package Modules\User\Observers
 */
class UserObserver
{
    use Cacheable;
    
    /**
     * Handle the user "creating" event.
     *
     * @param  User  $user
     *
     * @return void
     */
    public function creating(User $user): void
    {
        $user->name = "{$user->first_name} {$user->last_name}";
        $user->password = Hash::needsRehash($user->password) ? Hash::make($user->password) : $user->password;
    }
    
    /**
     * Handle the user "created" event.
     *
     * @param  User  $user
     *
     * @return void
     */
    public function created(User $user): void
    {
        if ($user->hasRole('Admin')) {
            DB::table('dashboards')->increment('count_admins', 1);
        }
        
        if ($user->hasRole('Partner')) {
            DB::table('dashboards')->increment('count_partners', 1);
        }
        
        $this->incrementCountItemsInCache($user, config('user.cache_key'));
    }
    
    /**
     * Handle the user "updating" event.
     *
     * @param  User  $user
     */
    public function updating(User $user): void
    {
        $user->name = "{$user->first_name} {$user->last_name}";
    }
    
    /**
     * Handle the user "updated" event.
     *
     * @param  User  $user
     *
     * @return void
     */
    public function updated(User $user): void
    {
        //
    }
    
    /**
     * Handle the user "deleting" event.
     *
     * @param  User  $user
     *
     * @throws Exception
     */
    public function deleting(User $user): void
    {
        $user->load(array(
                'partner',
                'partner.partnerShops'
            )
        );
        
        if ($user->partner !== null) {
            $user->partner()->each(static function ($partner) {
                $partner->delete();
                $partner->partnerShops()->each(static function ($shop) {
                    $shop->delete();
                });
            });
        }
    }
    
    /**
     * Handle the user "deleted" event.
     *
     * @param  User  $user
     *
     * @return void
     */
    public function deleted(User $user): void
    {
        if ($user->hasRole('Admin')) {
            DB::table('dashboards')->decrement('count_admins', 1);
        }
        
        if ($user->hasRole('Partner')) {
            DB::table('dashboards')->decrement('count_partners', 1);
        }
        
        $this->decrementCountItemsInCache($user, config('user.cache_key'));
    }
    
    /**
     * Handle the user "restored" event.
     *
     * @param  User  $user
     *
     * @return void
     */
    public function restored(User $user): void
    {
        if ($user->hasRole('Admin')) {
            DB::table('dashboards')->increment('count_admins', 1);
        }
        
        if ($user->hasRole('Partner')) {
            DB::table('dashboards')->increment('count_partners', 1);
        }
        
        
        $this->incrementCountItemsInCache($user, config('user.cache_key'));
    }
    
    /**
     * Handle the user "force deleted" event.
     *
     * @param  User  $user
     *
     * @return void
     */
    public function forceDeleted(User $user): void
    {
        if ($user->hasRole('Admin')) {
            DB::table('dashboards')->decrement('count_admins', 1);
        }
        
        if ($user->hasRole('Partner')) {
            DB::table('dashboards')->decrement('count_partners', 1);
        }
        
        $this->decrementCountItemsInCache($user, config('user.cache_key'));
    }
}
