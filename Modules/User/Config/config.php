<?php

return [
    'name' => 'User',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all users as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('USER_CACHE_KEY', 'index_users'),
    'cache_ttl' => env('USER_CACHE_TTL', 360000),
];
