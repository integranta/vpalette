<?php


namespace Modules\User\Service;


use App\DataTables\UsersDataTable;
use App\Services\BaseService;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\User\Contracts\UserContract;
use Spatie\Permission\Models\Role;

/**
 * Class UserWebService
 *
 * @package Modules\User\Service
 */
class UserWebService extends BaseService
{
    /**
     * @var UserContract
     */
    private $userRepository;
    
    /**
     * UserWebService constructor.
     *
     * @param  UserContract  $userRepository
     */
    public function __construct(UserContract $userRepository)
    {
        $this->userRepository = $userRepository;
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\UsersDataTable  $dataTable
	 *
	 * @return View|JsonResponse
	 */
    public function index(UsersDataTable $dataTable)
    {
        $this->setPageTitle(
            __('user::messages.title'),
            __('user::messages.index_subtitle'),
            __('user::messages.index_leadtext')
        );
        
        return $dataTable->render('user::users.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $roles = Role::get();
        
        
        $this->setPageTitle(
            __('user::messages.title'),
            __('user::messages.create_subtitle'),
            __('user::messages.create_leadtext')
        );
        
        return view('user::users.create', compact('roles'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $user = $this->userRepository->createUser($request->all());
        
        if (!$user) {
            return $this->responseRedirectBack(
                __('user::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.users.index',
            __('user::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|View
     */
    public function show(int $id): View
    {
        $user = $this->userRepository->findUserById($id);
        $roles = Role::get();
        $targetRoles = null;
        if ($user) {
            $targetRoles = $user->roles()->get();
        }
        
        
        if (!$user) {
            return $this->responseRedirect(
                'admin.users.index',
                __('user::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }
        
        $this->setPageTitle(
            __('user::messages.title'),
            __('user::messages.show_subtitle').$user->name,
            __('user::messages.show_leadtext').$user->name
        );
        
        return view('user::users.show', compact('user', 'roles', 'targetRoles'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Application|Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        $user = $this->userRepository->findUserById($id);
        $roles = Role::get();
        $targetRoles = null;
        if ($user) {
            $targetRoles = $user->roles()->get();
        }
        
        if (!$user) {
            return $this->responseRedirect(
                'admin.users.index',
                __('user::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }
        
        $this->setPageTitle(
            __('user::messages.title'),
            __('user::messages.edit_subtitle').$user->name,
            __('user::messages.edit_leadtext').$user->name
        );
        
        return view('user::users.edit', compact('user', 'roles', 'targetRoles'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $user = $this->userRepository->findUserById($id);
        
        if (!$user) {
            return $this->responseRedirectBack(
                __('user::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        $user = $this->userRepository->updateUser($request->all(), $id);
        
        if (!$user) {
            return $this->responseRedirectBack(
                __('user::messages.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.users.index',
            __('user::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $user = $this->userRepository->findUserById($id);
        
        if (!$user) {
            return $this->responseRedirectBack(
                __('user::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        $user = $this->userRepository->deleteUser($id);
        
        if (!$user) {
            return $this->responseRedirectBack(
                __('user::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.users.index',
            __('user::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Return list all available roles.
     *
     * @return JsonResponse
     */
    public function roles(): JsonResponse
    {
        return response()->json(Role::get());
    }
    
    /**
     * Return list all notifications for user.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function listNotifications(int $id): View
    {
        $listNotifications = User::find($id)->notifications()->get()->toArray();
        $userId = $id;
        return view('admin.users.notifications', compact('listNotifications', 'userId'));
    }
}
