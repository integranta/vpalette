<?php

return [
	'id'                    => 'ID',
	'active'                => 'Активность:',
	'name'                  => 'Полное имя',
	'first_name'            => 'Имя:',
	'last_name'             => 'Фамилия:',
	'email'                 => 'Email:',
	'phone'                 => 'Номер телефон:',
	'role'                  => 'Роль:',
	'password'              => 'Пароль:',
	'password_confirmation' => 'Подтверждение пароля:',
	'current_password'      => 'Текущий пароль:',
	'created_at'            => 'Зарегистрирован:',
	'action'               => 'Действия',
	
	'placeholder' => [
		'first_name'            => 'Введите Имя:',
		'last_name'             => 'Введите Фамилия:',
		'email'                 => 'Введите Email:',
		'phone'                 => 'Введите Номер телефон:',
		'role'                  => 'Выберите Роль:',
		'password'              => 'Введите Пароль:',
		'change_password'       => 'Введите пароль (Только если вы хотите изменить пароль)',
		'password_confirmation' => 'Подтвердите пароль:',
	]
];
