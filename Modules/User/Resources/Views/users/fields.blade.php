<!-- Active field -->
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('user::templates.active') }}
    </span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        {!! Form::hidden('active', 0) !!}
        {!!
            Form::checkbox(
                'active',
                true,
                old('active', true),
                [
                  'class' => "custom-control-input",
                  'id' => 'active',
                ]
            )
        !!}
        <label class="custom-control-label ml-lg-3" for="active"></label>
    </div>
</div>

<!-- First name Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'first_name',
            __('user::templates.first_name'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'first_name',
                old('first_name', ''),
                ['class' => 'form-control']
            )
        !!}
    </div>
</div>

<!-- Last name Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'last_name',
            __('user::templates.last_name'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'last_name',
                old('last_name', ''),
                ['class' => 'form-control']
            )
        !!}
    </div>
</div>

<!-- E-mail Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'email',
            __('user::templates.email'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::email(
                'email',
                old('email', ''),
                ['class' => 'form-control']
            )
        !!}
    </div>
</div>

<!-- Phone Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'phone',
            __('user::templates.phone'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::tel(
                'phone',
                old('phone', ''),
                ['class' => 'form-control']
            )
        !!}
    </div>
</div>

@can('edit-users')
    <!-- Role Field -->
    <div class="form-group row col-md-12 mb-4">
        {!!
            Form::label(
                'role',
                __('user::templates.role'),
                ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
            )
        !!}

        <div class="col-sm-12 col-md-7">
            <select name="role" id="role" class="form-control">
                @foreach($roles as $key => $role)
                    <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
@endcan

<!-- Password field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'password',
            __('user::templates.password'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'password',
                old('password', ''),
                ['class' => 'form-control']
            )
        !!}
    </div>
</div>

@can('edit-users')
    <div class="form-group row col-md-12">
        {!!
            Form::label(
                'password_confirmation',
                __('user::templates.password_confirmation'),
                ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
            )
        !!}
        <div class="col-sm-12 col-md-7">
            {!!
                Form::text(
                    'password',
                    old('password_confirmation'),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>
@endcan
