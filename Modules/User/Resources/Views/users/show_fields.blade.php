<!-- Active field -->
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        {{ __('user::templates.active') }}
    </span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        {!! Form::hidden('active', 0) !!}
        {!!
            Form::checkbox(
                'active',
                true,
                $user->active,
                [
                  'class' => "custom-control-input",
                  'id' => 'active',
                  'disabled'
                ]
            )
        !!}
        <label class="custom-control-label ml-lg-3" for="active"></label>
    </div>
</div>

<!-- First name Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'first_name',
            __('user::templates.first_name'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'first_name',
               $user->first_name,
                [
                    'class' => 'form-control',
                    'placeholder' => __('user::templates.placeholder.first_name'),
                    'disabled'
                ]
            )
        !!}
    </div>
</div>

<!-- Last name Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'last_name',
            __('user::templates.last_name'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'last_name',
                $user->last_name,
                [
                    'class' => 'form-control',
                    'placeholder' => __('user::templates.placeholder.last_name'),
                    'disabled'
                ]
            )
        !!}
    </div>
</div>

<!-- E-mail Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'email',
            __('user::templates.email'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::email(
                'email',
                $user->email,
                [
                    'class' => 'form-control',
                    'placeholder' => __('user::templates.placeholder.email'),
                    'disabled'
                ]
            )
        !!}
    </div>
</div>

<!-- Phone Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'phone',
            __('user::templates.phone'),
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::tel(
                'phone',
                $user->phone,
                [
                    'class' => 'form-control',
                    'disabled',
                    'placeholder' => __('user::templates.placeholder.phone')
                ]
            )
        !!}
    </div>
</div>

@can('edit-users')
    <!-- Role Field -->
    <div class="form-group row col-md-12 mb-4">
        {!!
            Form::label(
                'role',
                __('user::templates.role'),
                ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold']
            )
        !!}

        <div class="col-sm-12 col-md-7">
            <select name="role" id="role" class="form-control" disabled>
                @foreach($roles as $key => $role)
                    @foreach ($targetRoles as $targetRole)
                        @if ($role->name === $targetRole->name)
                            <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                        @else
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endif
                    @endforeach
                @endforeach
            </select>
        </div>
    </div>
@endcan

