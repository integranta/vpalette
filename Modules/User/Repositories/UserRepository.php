<?php


namespace Modules\User\Repositories;

use Hash;
use Modules\User\Contracts\UserContract;
use App\Repositories\BaseRepository;
use App\User;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Permission\Models\Role;
use Str;

/**
 * Class UserRepository
 *
 * @package Modules\User\Repositories
 */
class UserRepository extends BaseRepository implements UserContract
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listUsers(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('user.cache_key').$postfix,
            config('user.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @inheritDoc
     */
    public function findUserById(int $id): ?User
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function findUserBy(array $data): ?User
    {
        return $this->findBy($data);
    }
    
    /**
     * @inheritDoc
     */
    public function createUser(array $params): User
    {
        $user = $this->create($params);
        $role = Role::findById($params['role']);
        
        if ($role) {
            $user->assignRole($role);
        }
        
        return $user;
    }
    
    /**
     * @inheritDoc
     */
    public function updateUser(array $params, int $id): bool
    {
        
        $user = $this->findUserById($id);
        
        if (!$user) {
            return false;
        }
        
        $this->update([
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'email' => $params['email'],
            'phone' => $params['phone']
        ], $id);
        
        if (isset($params['password'])) {
            $this->update([
                'password' => Hash::make($params['password'])
            ], $id);
        }
        
        
        if (isset($params['role']) && $user->can('edit-users') && !$user->isme) {
            $role = Role::findById($params['role']);
            if ($role) {
                $user->syncRoles([$role]);
            }
        }
        
        return true;
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function deleteUser(int $id): bool
    {
        return $this->delete($id);
    }
}
