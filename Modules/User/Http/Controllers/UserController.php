<?php

namespace Modules\User\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\User\Service\UserWebService;

/**
 * Class UserController
 *
 * @package Modules\User\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserWebService
     */
    private $service;

    /**
     * UserController constructor.
     *
     * @param  UserWebService  $service
     */
    public function __construct(UserWebService $service)
    {
        $this->authorizeResource(User::class);
        $this->service = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\UsersDataTable  $dataTable
	 *
	 * @return View|JsonResponse
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
    public function index(UsersDataTable $dataTable)
    {
        $this->authorize(User::class, 'index');
        return $this->service->index($dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return $this->service->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        return $this->service->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|View
     */
    public function show(int $id)
    {
        return $this->service->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Application|Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->service->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        return $this->service->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->service->destroy($id);
    }

    /**
     * Return list all available roles.
     *
     * @return JsonResponse
     */
    public function roles(): JsonResponse
    {
        return $this->service->roles();
    }

    /**
     * Return list all notifications for user.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function listNotifications(int $id): View
    {
        return $this->service->listNotifications($id);
    }
}
