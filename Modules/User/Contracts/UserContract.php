<?php


namespace Modules\User\Contracts;

use App\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface UserContract
 *
 * @package Modules\User\Contracts
 */
interface UserContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listUsers(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return User
     */
    public function findUserById(int $id): ?User;
    
    /**
     * @param  array  $data
     *
     * @return User
     */
    public function findUserBy(array $data): ?User;
    
    /**
     * @param  array  $params
     *
     * @return User
     */
    public function createUser(array $params): User;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateUser(array $params, int $id): bool;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteUser(int $id): bool;
}
