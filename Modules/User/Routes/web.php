<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\User\Http\Controllers\UserController;

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		Route::get('users/roles', [UserController::class, 'roles'])
			->name('users.roles');
		Route::get('users/{id}/notify/list', [UserController::class, 'listNotifications'])
			->name('users.list.notify');
		Route::resource('users', UserController::class);
	});
