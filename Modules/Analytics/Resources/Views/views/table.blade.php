<div class="table-responsive">
    <table class="table" id="views-table">
        <thead>
        <tr>
            <th>User Id</th>
            <th>Ip</th>
            <th>Views</th>
            <th>Viewable Type</th>
            <th>Viewable Type</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($views as $view)
            <tr>
                <td>{!! $view->user_id !!}</td>
                <td>{!! $view->ip !!}</td>
                <td>{!! $view->views !!}</td>
                <td>{!! $view->viewable_type !!}</td>
                <td>{!! $view->viewable_type !!}</td>
                <td>
                    {!! Form::open(['route' => ['views.destroy', $view->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('views.show', [$view->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('views.edit', [$view->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
