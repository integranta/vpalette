<?php


namespace Modules\Analytics\Traits;


use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\Analytics\Models\ViewCount;

/**
 * Trait Viewable
 *
 * @package Modules\Analytics\Traits
 */
trait Viewable
{
    /***
     * Relationship with model Views
     *
     * @return MorphMany
     */
    public function views(): MorphMany
    {
        return $this->morphMany(ViewCount::class, 'viewable');
    }
}
