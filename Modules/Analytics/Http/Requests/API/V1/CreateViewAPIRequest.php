<?php

namespace Modules\Analytics\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Analytics\Models\ViewCount;

/**
 * Class CreateViewAPIRequest
 *
 * @package Modules\Analytics\Http\Requests\API\V1
 */
class CreateViewAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return ViewCount::$rules;
    }
}
