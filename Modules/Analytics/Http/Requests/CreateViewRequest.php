<?php

namespace Modules\Analytics\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Analytics\Models\View;

/**
 * Class CreateViewRequest
 *
 * @package Modules\Analytics\Http\Requests
 */
class CreateViewRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return View::$rules;
    }
}
