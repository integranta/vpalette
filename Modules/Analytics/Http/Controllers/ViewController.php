<?php

namespace Modules\Analytics\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Analytics\Http\Requests\CreateViewRequest;
use Modules\Analytics\Http\Requests\UpdateViewRequest;
use Modules\Analytics\Services\ViewWebService;
use Response;

/**
 * Class ViewController
 *
 * @package Modules\Analytics\Http\Controllers
 */
class ViewController extends Controller
{
    /** @var  ViewWebService */
    private $viewWebService;

    /**
     * ViewController constructor.
     *
     * @param  ViewWebService  $service
     */
    public function __construct(ViewWebService $service)
    {
        $this->viewWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the View.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function index(Request $request): View
    {
        return $this->viewWebService->index($request);
    }

    /**
     * Show the form for creating a new View.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->viewWebService->create();
    }

    /**
     * Store a newly created View in storage.
     *
     * @param  CreateViewRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateViewRequest $request): RedirectResponse
    {
        return $this->viewWebService->store($request);
    }

    /**
     * Display the specified View.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $id): View
    {
        return $this->viewWebService->show($id);
    }

    /**
     * Show the form for editing the specified View.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->viewWebService->show($id);
    }

    /**
     * Update the specified View in storage.
     *
     * @param  int                $id
     * @param  UpdateViewRequest  $request
     *
     * @return RedirectResponse|Response
     */
    public function update(int $id, UpdateViewRequest $request): RedirectResponse
    {
        return $this->viewWebService->update($id, $request);
    }

    /**
     * Remove the specified View from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->viewWebService->destroy($id);
    }
}
