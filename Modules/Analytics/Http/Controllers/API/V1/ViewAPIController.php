<?php

namespace Modules\Analytics\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Analytics\Http\Requests\API\V1\CreateViewAPIRequest;
use Modules\Analytics\Http\Requests\API\V1\UpdateViewAPIRequest;
use Modules\Analytics\Services\API\V1\ViewApiService;

/**
 * Class ViewController
 *
 * @package Modules\Analytics\Http\Controllers\API\V1
 */
class ViewAPIController extends Controller
{
    /** @var  ViewApiService */
    private $viewApiService;

    /**
     * ViewAPIController constructor.
     *
     * @param  ViewApiService  $service
     */
    public function __construct(ViewApiService $service)
    {
        $this->viewApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->viewApiService->index($request);
    }

    /**
     * @param  CreateViewAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateViewAPIRequest $request): JsonResponse
    {
        return $this->viewApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->viewApiService->show($id);
    }

    /**
     * @param  int                   $id
     * @param  UpdateViewAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateViewAPIRequest $request): JsonResponse
    {
        return $this->viewApiService->update($id, $request);
    }


    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->viewApiService->destroy($id);
    }
}
