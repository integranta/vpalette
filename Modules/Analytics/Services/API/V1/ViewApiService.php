<?php


namespace Modules\Analytics\Services\API\V1;

use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Analytics\Contracts\ViewContract;
use Modules\Analytics\Http\Requests\API\V1\CreateViewAPIRequest;
use Modules\Analytics\Http\Requests\API\V1\UpdateViewAPIRequest;
use Modules\Analytics\Transformers\ViewResource;

/**
 * Class ViewApiService
 *
 * @package Modules\Analytics\Services\API\V1
 */
class ViewApiService extends BaseService
{
    /** @var  ViewContract */
    private $viewRepository;

    /**
     * ViewAPIController constructor.
     *
     * @param  ViewContract  $viewRepo
     */
    public function __construct(ViewContract $viewRepo)
    {
        $this->viewRepository = $viewRepo;
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $views = ViewResource::collection($this->viewRepository->listViews());

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('analytics::messages.success_retrieve'),
            $views
        );
    }

    /**
     * @param  CreateViewAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateViewAPIRequest $request): JsonResponse
    {
        $view = $this->viewRepository->createViews($request->all());

        if (!$view) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('analytics::messages.error_create')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('analytics::messages.success_create'),
            $view
        );
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $view = ViewResource::make($this->viewRepository->findViewsById($id));

        if ($view === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('analytics::messages.not_found')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('analytics::messages.success_found'),
            $view
        );
    }

    /**
     * @param  int                   $id
     * @param  UpdateViewAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateViewAPIRequest $request): JsonResponse
    {
        $view = $this->viewRepository->findViewsById($id);

        if ($view === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('analytics::messages.not_found')
            );
        }

        $view = $this->viewRepository->updateViews($request->all(), $id);

        if (!$view) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('analytics::messages.error_update')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('analytics::messages.success_update'),
            $view
        );
    }


    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $view = $this->viewRepository->findViewsById($id);

        if ($view === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('analytics::messages.not_found')
            );
        }

        $view = $this->viewRepository->deleteViews($id);

        if (!$view) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('analytics::messages.error_delete')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('analytics::messages.success_delete'),
            $view
        );
    }
}
