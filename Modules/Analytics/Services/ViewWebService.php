<?php


namespace Modules\Analytics\Services;


use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Analytics\Contracts\ViewContract;
use Modules\Analytics\Http\Requests\CreateViewRequest;
use Modules\Analytics\Http\Requests\UpdateViewRequest;
use Response;

/**
 * Class ViewWebService
 *
 * @package Modules\Analytics\Services
 */
class ViewWebService extends BaseService
{
    /** @var  ViewContract */
    private $viewRepository;

    /**
     * ViewController constructor.
     *
     * @param  ViewContract  $viewRepo
     */
    public function __construct(ViewContract $viewRepo)
    {
        $this->viewRepository = $viewRepo;
    }

    /**
     * Display a listing of the View.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function index(Request $request): View
    {
        $views = $this->viewRepository->listViews();

        $this->setPageTitle(
            __('analytics::messages.title'),
            __('analytics::messages.index_subtitle'),
            __('analytics::messages.index_leadtext')
        );

        return view('analytics::views.index', compact('views'));
    }

    /**
     * Show the form for creating a new View.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $this->setPageTitle(
            __('analytics::messages.title'),
            __('analytics::messages.index_subtitle'),
            __('analytics::messages.index_leadtext')
        );

        return view('analytics::views.create');
    }

    /**
     * Store a newly created View in storage.
     *
     * @param  CreateViewRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateViewRequest $request): RedirectResponse
    {
        $view = $this->viewRepository->createViews($request->all());

        if (!$view) {
            return $this->responseRedirectBack(
                __('analytics::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }

        return $this->responseRedirect(
            'admin.views.index',
            __('analytics::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }

    /**
     * Display the specified View.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $id): View
    {
        $view = $this->viewRepository->findViewsById($id);

        if ($view === null) {
            return $this->responseRedirect(
                'admin.views.index',
                __('analytics::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }

        $this->setPageTitle(
            __('analytics::messages.title'),
            __('analytics::messages.show_subtitle'),
            __('analytics::messages.show_leadtext')
        );
        return view('analytics::views.show', compact('view'));
    }

    /**
     * Show the form for editing the specified View.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        $view = $this->viewRepository->findViewsById($id);

        if ($view === null) {
            return $this->responseRedirect(
                'admin.views.index',
                __('analytics::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }

        $this->setPageTitle(
            __('analytics::messages.title'),
            __('analytics::messages.edit_subtitle'),
            __('analytics::messages.edit_leadtext')
        );
        return view('analytics::views.edit', compact('view'));
    }

    /**
     * Update the specified View in storage.
     *
     * @param  int                $id
     * @param  UpdateViewRequest  $request
     *
     * @return RedirectResponse|Response
     */
    public function update(int $id, UpdateViewRequest $request): RedirectResponse
    {
        $view = $this->viewRepository->findViewsById($id);

        if ($view === null) {
            return $this->responseRedirect(
                'admin.views.index',
                __('analytics::message.not_found'),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }

        $view = $this->viewRepository->updateViews($request->all(), $id);

        if (!$view) {
            return $this->responseRedirectBack(
                __('analytics::messages.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }

        return $this->responseRedirect(
            'admin.views.index',
            __('analytics::message.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }

    /**
     * Remove the specified View from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        $view = $this->viewRepository->findViewsById($id);

        if ($view === null) {
            return $this->responseRedirect(
                'admin.views.index',
                __('analytics::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                false
            );
        }

        $view = $this->viewRepository->deleteViews($id);

        if (!$view) {
            return $this->responseRedirectBack(
                __('analytics::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }

        return $this->responseRedirect(
            'admin.views.index',
            __('analytics::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
}
