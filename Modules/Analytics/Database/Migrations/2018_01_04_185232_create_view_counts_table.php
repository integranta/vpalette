<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateViewCountsTable
 */
class CreateViewCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('view_counts', function (Blueprint $table) {
                $table->bigIncrements('id')->index();
                $table->unsignedBigInteger('user_id')->nullable()->index();
                $table->ipAddress('ip')->nullable()->index();
                $table->unsignedInteger('views')->default(0);
                $table->string('viewable_type', 100)->index();
                $table->unsignedBigInteger('viewable_id');
                $table->timestamp('viewed_at')->useCurrent();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_counts');
    }
}
