<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Analytics\Models\ViewCount;

$factory->define(ViewCount::class, function (Faker $faker) {
    return [
        'user_id' => \factory(\App\User::class),
        'ip' => $faker->ipv4,
        'viewable_type' => '\Modules\News\Models\News',
        'viewable_id' => $faker->numberBetween(1, 20),
        'viewed_at' => \Carbon\Carbon::tomorrow(env('APP_TIMEZONE'))
    ];
});
