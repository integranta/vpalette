<?php

namespace Modules\Analytics\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ViewResource
 *
 * @package Modules\Analytics\Transformers
 */
class ViewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'userId' => $this->user_id,
            'ipAddress' => $this->ip,
            'countView' => $this->views,
            'viewableId' => $this->viewable_id,
            'viewableType' => $this->viewable_type,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
