<?php

namespace Modules\Analytics\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class ViewCount
 *
 * @package Modules\Analytics\Models
 * @property int $id
 * @property int|null $userId
 * @property string|null $ip
 * @property int $views
 * @property string $viewableType
 * @property int $viewableId
 * @property string $viewedAt
 * @property string|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $viewable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereViewableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereViewableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereViewedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Analytics\Models\ViewCount whereViews($value)
 * @mixin \Eloquent
 */
class ViewCount extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'view_counts';

    /**
     * Определяет необходимость отметок времени для модели.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var string
     */
    protected $fillable = [
        'user_id',
        'ip',
        'views',
        'viewable_id',
        'viewable_type'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ip' => 'string',
        'views' => 'integer',
        'viewable_id' => 'integer',
        'viewable_type' => 'string'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'nullable|integer|exists:users,id',
        'ip' => 'required|ip',
        'views' => 'min:0|integer'
    ];

    /**
     * Получить все модели, владеющие viewable.
     *
     * @return MorphTo
     */
    public function viewable(): MorphTo
    {
        return $this->morphTo();
    }
}
