<?php

namespace Modules\Analytics\Repositories;

use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Modules\Analytics\Contracts\ViewContract;
use Modules\Analytics\Models\ViewCount;

/**
 * Class ViewRepository
 *
 * @package Modules\Analytics\Repositories
 * @version August 28, 2019, 10:27 am MSK
 */
class ViewRepository extends BaseRepository implements ViewContract
{
    /**
     * BaseRepository constructor.
     *
     * @param  ViewCount  $model
     */
    public function __construct(ViewCount $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function listViews(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @inheritDoc
     */
    public function findViewsById(int $id): ?ViewCount
    {
        return $this->find($id);
    }

    /**
     * @inheritDoc
     */
    public function createViews(array $params): ViewCount
    {
        return $this->create($params);
    }

    /**
     * @inheritDoc
     */
    public function updateViews(array $params, int $id): bool
    {
        return $this->update($params, $id);
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function deleteViews(int $id): bool
    {
        return $this->delete($id);
    }
}
