<?php


namespace Modules\Analytics\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Analytics\Models\ViewCount;

/**
 * Interface ViewContract
 *
 * @package Modules\Analytics\Contracts
 */
interface ViewContract
{
    /**
     * Получить список записей об просмотрах.
     *
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listViews(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection;

    /**
     * Найти запись об просмотре по указанному ID.
     *
     * @param  int  $id
     *
     * @return ViewCount
     */
    public function findViewsById(int $id): ?ViewCount;

    /**
     * Создать новую запись об просмотре.
     *
     * @param  array  $params
     *
     * @return ViewCount
     */
    public function createViews(array $params): ViewCount;

    /**
     * Обновить запись об просмотре по указанному ID.
     *
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateViews(array $params, int $id): bool;

    /**
     * Удалить запись об просмотре по указанному ID.
     *
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteViews(int $id): bool;
}
