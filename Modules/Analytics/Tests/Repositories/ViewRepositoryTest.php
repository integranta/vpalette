<?php namespace Modules\Analytics\Tests\Repositories;

use App;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Analytics\Models\View;
use Modules\Analytics\Repositories\ViewRepository;
use Modules\Analytics\Tests\ApiTestTrait;
use Modules\Analytics\Tests\TestCase;

/**
 * Class ViewRepositoryTest
 *
 * @package Modules\Analytics\Tests\Repositories
 */
class ViewRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;
    
    /**
     * @var ViewRepository
     */
    protected $viewRepo;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->viewRepo = App::make(ViewRepository::class);
    }
    
    /**
     * @test create
     */
    public function test_create_view()
    {
        $view = factory(View::class)->make()->toArray();
        
        $createdView = $this->viewRepo->create($view);
        
        $createdView = $createdView->toArray();
        $this->assertArrayHasKey('id', $createdView);
        $this->assertNotNull($createdView['id'], 'Created View must have id specified');
        $this->assertNotNull(View::find($createdView['id']), 'View with given id must be in DB');
        $this->assertModelData($view, $createdView);
    }
    
    /**
     * @test read
     */
    public function test_read_view()
    {
        $view = factory(View::class)->create();
        
        $dbView = $this->viewRepo->find($view->id);
        
        $dbView = $dbView->toArray();
        $this->assertModelData($view->toArray(), $dbView);
    }
    
    /**
     * @test update
     */
    public function test_update_view()
    {
        $view = factory(View::class)->create();
        $fakeView = factory(View::class)->make()->toArray();
        
        $updatedView = $this->viewRepo->update($fakeView, $view->id);
        
        $this->assertModelData($fakeView, $updatedView->toArray());
        $dbView = $this->viewRepo->find($view->id);
        $this->assertModelData($fakeView, $dbView->toArray());
    }
    
    /**
     * @test delete
     */
    public function test_delete_view()
    {
        $view = factory(View::class)->create();
        
        $resp = $this->viewRepo->delete($view->id);
        
        $this->assertTrue($resp);
        $this->assertNull(View::find($view->id), 'View should not exist in DB');
    }
}
