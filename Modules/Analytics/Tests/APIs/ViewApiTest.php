<?php namespace Modules\Analytics\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Analytics\Models\View;
use Modules\Analytics\Tests\ApiTestTrait;
use Modules\Analytics\Tests\TestCase;

/**
 * Class ViewApiTest
 *
 * @package Modules\Analytics\Tests\APIs
 */
class ViewApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_view()
    {
        $view = factory(View::class)->make()->toArray();
        
        $this->response = $this->json(
            'POST',
            '/api/views', $view
        );
        
        $this->assertApiResponse($view);
    }
    
    /**
     * @test
     */
    public function test_read_view()
    {
        $view = factory(View::class)->create();
        
        $this->response = $this->json(
            'GET',
            '/api/views/'.$view->id
        );
        
        $this->assertApiResponse($view->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_view()
    {
        $view = factory(View::class)->create();
        $editedView = factory(View::class)->make()->toArray();
        
        $this->response = $this->json(
            'PUT',
            '/api/views/'.$view->id,
            $editedView
        );
        
        $this->assertApiResponse($editedView);
    }
    
    /**
     * @test
     */
    public function test_delete_view()
    {
        $view = factory(View::class)->create();
        
        $this->response = $this->json(
            'DELETE',
            '/api/views/'.$view->id
        );
        
        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/views/'.$view->id
        );
        
        $this->response->assertStatus(404);
    }
}
