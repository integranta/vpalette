<?php namespace Modules\Faq\Tests\Repositories;

use App;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Faq\Models\Faq;
use Modules\Faq\Repositories\FaqRepository;
use Modules\Faq\Tests\ApiTestTrait;
use Modules\Faq\Tests\TestCase;

/**
 * Class FaqRepositoryTest
 *
 * @package Modules\Faq\Tests\Repositories
 */
class FaqRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;
    
    /**
     * @var FaqRepository
     */
    protected $faqRepo;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->faqRepo = App::make(FaqRepository::class);
    }
    
    /**
     * @test create
     */
    public function test_create_faq()
    {
        $faq = factory(Faq::class)->make()->toArray();
        
        $createdFaq = $this->faqRepo->create($faq);
        
        $createdFaq = $createdFaq->toArray();
        $this->assertArrayHasKey('id', $createdFaq);
        $this->assertNotNull($createdFaq['id'], 'Created Faq must have id specified');
        $this->assertNotNull(Faq::find($createdFaq['id']), 'Faq with given id must be in DB');
        $this->assertModelData($faq, $createdFaq);
    }
    
    /**
     * @test read
     */
    public function test_read_faq()
    {
        $faq = factory(Faq::class)->create();
        
        $dbFaq = $this->faqRepo->find($faq->id);
        
        $dbFaq = $dbFaq->toArray();
        $this->assertModelData($faq->toArray(), $dbFaq);
    }
    
    /**
     * @test update
     */
    public function test_update_faq()
    {
        $faq = factory(Faq::class)->create();
        $fakeFaq = factory(Faq::class)->make()->toArray();
        
        $updatedFaq = $this->faqRepo->update($fakeFaq, $faq->id);
        
        $this->assertModelData($fakeFaq, $updatedFaq->toArray());
        $dbFaq = $this->faqRepo->find($faq->id);
        $this->assertModelData($fakeFaq, $dbFaq->toArray());
    }
    
    /**
     * @test delete
     */
    public function test_delete_faq()
    {
        $faq = factory(Faq::class)->create();
        
        $resp = $this->faqRepo->delete($faq->id);
        
        $this->assertTrue($resp);
        $this->assertNull(Faq::find($faq->id), 'Faq should not exist in DB');
    }
}
