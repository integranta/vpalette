<!-- Username Field -->
<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="user_id"
    >
        Автор:
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="user_id"
            class="form-control"
            name="user_id"
        >
            <option value="0">Выберите пользователя</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}"> {{ $user->name }} </option>
            @endforeach
        </select>
    </div>
</div>
<!-- End Username Field -->

<!-- Product_id Field -->

<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="product_id">Товар
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="product_id"
            class="form-control"
            name="product_id"
        >
            <option value="0">Выберите товар</option>
            @foreach($products as $product)
                <option value="{{ $product->id }}"> {{ $product->name }} </option>
            @endforeach
        </select>
    </div>
</div>
<!-- End Product_Id Field -->

<!-- Question Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label
        (
            'question',
            'Вопрос:',
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::textarea
            (
                'question',
                old('question'),
                ['class' => 'form-control', 'id' => 'question']
            )
        !!}
    </div>
</div>

<div class="form-group row col-md-12">
    {!!
        Form::label
        (
            'answer',
            'Ответ:',
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::textarea
            (
                'answer',
                old('answer'),
                ['class' => 'form-control', 'id' => 'answer']
            )
        !!}
    </div>
</div>
