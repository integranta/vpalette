<!-- Username Field -->
<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="user_id"
    >
        Автор:
    </label>
    <div class="col-sm-12 col-md-7">

        <select
            id="user_id"
            class="form-control"
            name="user_id"
            disabled
        >
            <option value="0" disabled>Выберите пользователя</option>
            @foreach($users as $user)
                @if ($user->id === $targetUser->id)
                    <option value="{{ $user->id }}" selected> {{ $user->name }} </option>
                @else
                    <option value="{{ $user->id }}"> {{ $user->name }} </option>
                @endif
            @endforeach
        </select>
    </div>
</div>
<!-- End Username Field -->

<!-- Product_id Field -->

<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="product_id">Товар
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="product_id"
            class="form-control"
            name="product_id"
            disabled
        >
            <option value="0" disabled>Выберите товар</option>
            @foreach($products as $product)
                @if ($product->id === $targetProduct->id)
                    <option value="{{ $product->id }}" selected> {{ $product->name }} </option>
                @else
                    <option value="{{ $product->id }}"> {{ $product->name }} </option>
                @endif
            @endforeach
        </select>
    </div>
</div>
<!-- End Product_Id Field -->

<!-- Question Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label
        (
            'question',
            'Вопрос:',
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::textarea
            (
                'question',
                $faq->question,
                ['class' => 'form-control', 'id' => 'question', 'disabled']
            )
        !!}
    </div>
</div>

<div class="form-group row col-md-12">
    {!!
        Form::label
        (
            'answer',
            'Ответ:',
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::textarea
            (
                'answer',
                $faq->answer,
                ['class' => 'form-control', 'id' => 'answer', 'disabled']
            )
        !!}
    </div>
</div>
