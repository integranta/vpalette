<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Faq\Http\Controllers\API\V1\FaqAPIController;

Route::prefix('v1')->group(function () {
    Route::apiResource('faqs', FaqAPIController::class);
});
