<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Faq\Http\Controllers\FaqController;

Route::name('admin.')
    ->prefix('admin')
    ->middleware('auth')
    ->group(function() {
    	Route::get('faqs/import', [FaqController::class, 'import']);
        Route::resource('faqs', FaqController::class);
    });
