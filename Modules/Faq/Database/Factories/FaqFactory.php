<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Faq\Models\Faq;

$factory->define(Faq::class,
    function (Faker $faker) {
        
        return [
            'active' => true,
            'sort' => $faker->numberBetween(1, 500),
            'question' => $faker->text(200),
            'answer' => $faker->text(150),
            'user_id' => random_int(1, 3),
            'product_id' => random_int(1, 5)
        ];
    });
