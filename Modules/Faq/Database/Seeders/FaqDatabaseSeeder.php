<?php

namespace Modules\Faq\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Faq\Models\Faq;

/**
 * Class FaqDatabaseSeeder
 *
 * @package Modules\Faq\Database\Seeders
 */
class FaqDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Faq::class, 50)->create();
    }
}
