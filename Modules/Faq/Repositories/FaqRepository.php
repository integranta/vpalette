<?php

namespace Modules\Faq\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Modules\Faq\Contracts\FaqContract;
use Modules\Faq\Models\Faq;
use Str;

/**
 * Class FaqRepository
 *
 * @package Modules\Faq\Repositories
 * @version August 20, 2019, 3:26 pm MSK
 */
class FaqRepository extends BaseRepository implements FaqContract
{
    /**
     * FaqRepository constructor.
     *
     * @param  Faq  $model
     */
    public function __construct(Faq $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listAnswerAndQuestions(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): Collection {
        
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('faq.cache_key').$postfix,
            config('faq.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return Faq
     */
    public function findAnswerAndQuestionById(int $id): ?Faq
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return Faq
     */
    public function createAnswerAndQuestion(array $params): Faq
    {
        return $this->create($params);
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Faq
     */
    public function updateAnswerAndQuestion(array $params, int $id): Faq
    {
        return $this->update($params, $id);
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteAnswerAndQuestion(int $id): bool
    {
        return $this->delete($id);
    }
}
