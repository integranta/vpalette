<?php

namespace Modules\Faq\Http\Controllers;

use App\DataTables\QuestionsDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\{Factory, View};
use Illuminate\Http\RedirectResponse;
use Modules\Faq\Http\Requests\{CreateFaqRequest, UpdateFaqRequest};
use Modules\Faq\Services\FaqWebService;

/**
 * Class FaqController
 *
 * @package Modules\Faq\Http\Controllers
 */
class FaqController extends Controller
{
    /** @var FaqWebService */
    private $faqWebService;

    /**
     * FaqController constructor.
     *
     * @param  FaqWebService  $service
     */
    public function __construct(FaqWebService $service)
    {
        $this->faqWebService = $service;

        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Faq.
	 *
	 * @param  \App\DataTables\QuestionsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(QuestionsDataTable $dataTable)
    {
        return $this->faqWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Faq.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->faqWebService->create();
    }

    /**
     * Store a newly created Faq in storage.
     *
     * @param  CreateFaqRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateFaqRequest $request): RedirectResponse
    {
        return $this->faqWebService->store($request);
    }

    /**
     * Display the specified Faq.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->faqWebService->show($id);
    }

    /**
     * Show the form for editing the specified Faq.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->faqWebService->edit($id);
    }

    /**
     * Update the specified Faq in storage.
     *
     * @param  int               $id
     * @param  UpdateFaqRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateFaqRequest $request): RedirectResponse
    {
        return $this->faqWebService->update($id, $request);
    }

    /**
     * Remove the specified Faq from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->faqWebService->destroy($id);
    }
	
	
    public function import()
	{
		return $this->faqWebService->import();
	}
}
