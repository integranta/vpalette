<?php

namespace Modules\Faq\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Faq\Http\Requests\API\V1\CreateFaqAPIRequest;
use Modules\Faq\Http\Requests\API\V1\UpdateFaqAPIRequest;
use Modules\Faq\Services\API\V1\FaqApiService;


/**
 * Class FaqController
 *
 * @package Modules\Faq\Http\Controllers\API\V1
 */
class FaqAPIController extends Controller
{
    /** @var FaqApiService */
    private $faqApiService;
    
    /**
     * FaqAPIController constructor.
     *
     * @param  FaqApiService  $service
     */
    public function __construct(FaqApiService $service)
    {
        $this->faqApiService = $service;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->faqApiService->index($request);
    }
    
    
    /**
     * @param  CreateFaqAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateFaqAPIRequest $request): JsonResponse
    {
        return $this->faqApiService->store($request);
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->faqApiService->show($id);
    }
    
    /**
     * @param  int                  $id
     * @param  UpdateFaqAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateFaqAPIRequest $request): JsonResponse
    {
        return $this->faqApiService->update($id, $request);
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->faqApiService->destroy($id);
    }
}
