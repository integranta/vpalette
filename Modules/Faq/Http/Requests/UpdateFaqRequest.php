<?php

namespace Modules\Faq\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Faq\Models\Faq;

/**
 * Class UpdateFaqRequest
 *
 * @package Modules\Faq\Http\Requests
 */
class UpdateFaqRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Faq::$rules;
    }
}
