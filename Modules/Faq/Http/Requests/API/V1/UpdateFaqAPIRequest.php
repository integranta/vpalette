<?php

namespace Modules\Faq\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Faq\Models\Faq;

/**
 * Class UpdateFaqAPIRequest
 *
 * @package Modules\Faq\Http\Requests\API\V1
 */
class UpdateFaqAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Faq::$rules;
    }
}
