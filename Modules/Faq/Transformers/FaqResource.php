<?php

namespace Modules\Faq\Transformers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Product\Transformers\ProductResource;

/**
 * Class FaqResource
 *
 * @package Modules\Faq\Transformers
 */
class FaqResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'question' => $this->question,
            'answer' => $this->answer,
            'productId' => $this->product_id,
            'userId' => $this->user_id,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at,
            'products' => ProductResource::collection($this->whenLoaded('product')),
            'users' => UserResource::collection($this->whenLoaded('user'))
        ];
    }
}
