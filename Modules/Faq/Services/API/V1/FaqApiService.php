<?php


namespace Modules\Faq\Services\API\V1;

use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Faq\Contracts\FaqContract;
use Modules\Faq\Http\Requests\API\V1\CreateFaqAPIRequest;
use Modules\Faq\Http\Requests\API\V1\UpdateFaqAPIRequest;
use Modules\Faq\Transformers\FaqResource;

/**
 * Class BrandApiService
 *
 * @package Modules\Banner\Services\API\V1
 */
class FaqApiService extends BaseService
{
	/** @var  FaqContract */
	private $faqRepository;
	
	/**
	 * FaqAPIController constructor.
	 *
	 * @param  FaqContract  $faqRepo
	 */
	public function __construct(FaqContract $faqRepo)
	{
		$this->faqRepository = $faqRepo;
	}
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function index(Request $request): JsonResponse
	{
		$faqs = FaqResource::collection($this->faqRepository->listAnswerAndQuestions());
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('faq::messages.success_retrieve'),
			$faqs
		);
	}
	
	
	/**
	 * @param  CreateFaqAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function store(CreateFaqAPIRequest $request): JsonResponse
	{
		$faq = $this->faqRepository->createAnswerAndQuestion($request->all());
		
		if (!$faq) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('faq::messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('faq::messages.success_create'),
			$faq
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function show(int $id): JsonResponse
	{
		$faq = FaqResource::make($this->faqRepository->findAnswerAndQuestionById($id));
		
		if ($faq === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('faq::messages.not_found', ['id' => $id])
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('faq::messages.success_found', ['id' => $id]),
			$faq
		);
	}
	
	/**
	 * @param  int                  $id
	 * @param  UpdateFaqAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function update(int $id, UpdateFaqAPIRequest $request): JsonResponse
	{
		$faq = $this->faqRepository->findAnswerAndQuestionById($id);
		
		if ($faq === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('faq::messages.not_found', ['id' => $id])
			);
		}
		
		$faq = $this->faqRepository->updateAnswerAndQuestion($request->all(), $id);
		
		if (!$faq) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('faq::messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('faq::messages.success_update'),
			$faq
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function destroy(int $id): JsonResponse
	{
		$faq = $this->faqRepository->findAnswerAndQuestionById($id);
		
		if ($faq === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('faq::messages.not_found', ['id' => $id])
			);
		}
		
		$faq = $this->faqRepository->deleteAnswerAndQuestion($id);
		
		if (!$faq) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('faq::messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('faq::messages.success_delete'),
			$faq
		);
	}
}
