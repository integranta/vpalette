<?php


namespace Modules\Faq\Services;

use App\DataTables\QuestionsDataTable;
use App\Imports\ImportFaq;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Faq\Contracts\FaqContract;
use Modules\Faq\Http\Requests\{CreateFaqRequest, UpdateFaqRequest};
use Modules\Product\Contracts\ProductContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Repositories\UserRepository;

/**
 * Class BrandWebService
 *
 * @package Modules\Banner\Services
 */
class FaqWebService extends BaseService
{
	/** @var FaqContract */
	private $faqRepository;
	
	/** @var UserRepository */
	private $userRepository;
	
	/** @var ProductContract */
	private $productRepository;
	
	
	/**
	 * FaqController constructor.
	 *
	 * @param  FaqContract      $faqRepo
	 * @param  UserContract     $userRepo
	 * @param  ProductContract  $productRepo
	 */
	public function __construct(
		FaqContract $faqRepo,
		UserContract $userRepo,
		ProductContract $productRepo
	) {
		$this->faqRepository = $faqRepo;
		$this->userRepository = $userRepo;
		$this->productRepository = $productRepo;
	}
	
	/**
	 * Display a listing of the Faq.
	 *
	 * @param  \App\DataTables\QuestionsDataTable  $dataTable
	 *
	 * @return \Illuminate\Contracts\View\Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(QuestionsDataTable $dataTable)
	{
		$this->setPageTitle(
			__('faq::messages.title'),
			__('faq::messages.index_subtitle'),
			__('faq::messages.index_leadtext')
		);
		
		return $dataTable->render('faq::faqs.index');
	}
	
	/**
	 * Show the form for creating a new Faq.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$users = $this->userRepository->listUsers();
		$products = $this->productRepository->listProducts();
		
		$this->setPageTitle(
			__('faq::messages.title'),
			__('faq::messages.create_subtitle'),
			__('faq::messages.create_leadtext')
		);
		return view('faq::faqs.create', compact('users', 'products'));
	}
	
	/**
	 * Store a newly created Faq in storage.
	 *
	 * @param  CreateFaqRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateFaqRequest $request): RedirectResponse
	{
		$faq = $this->faqRepository->createAnswerAndQuestion($request->all());
		
		if (!$faq) {
			return $this->responseRedirectBack(
				__('faq::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.faqs.index',
			__('faq::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified Faq.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|View
	 */
	public function show(int $id): View
	{
		$faq = $this->faqRepository->findAnswerAndQuestionById($id);
		$users = $this->userRepository->listUsers();
		$targetUser = $this->userRepository->findUserById($faq->user_id);
		$products = $this->productRepository->listProducts();
		$targetProduct = $this->productRepository->findProductById($faq->product_id);
		
		$this->setPageTitle(
			__('faq::messages.title'),
			__('faq::messages.create_subtitle', ['question' => $faq->question]),
			__('faq::messages.create_leadtext', ['question' => $faq->question])
		);
		return view('faq::faqs.show', compact(
				'faq',
				'users',
				'targetUser',
				'targetProduct',
				'products'
			)
		);
	}
	
	/**
	 * Show the form for editing the specified Faq.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$faq = $this->faqRepository->findAnswerAndQuestionById($id);
		$users = $this->userRepository->listUsers();
		$targetUser = $this->userRepository->findUserById($faq->user_id);
		$products = $this->productRepository->listProducts();
		$targetProduct = $this->productRepository->findProductById($faq->product_id);
		
		
		$this->setPageTitle(
			__('faq::messages.title'),
			__('faq::messages.edit_subtitle', ['question' => $faq->question]),
			__('faq::messages.edit_leadtext', ['question' => $faq->question])
		);
		return view('faq::faqs.edit', compact(
				'faq',
				'users',
				'targetUser',
				'targetProduct',
				'products'
			)
		);
	}
	
	/**
	 * Update the specified Faq in storage.
	 *
	 * @param  int               $id
	 * @param  UpdateFaqRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function update(int $id, UpdateFaqRequest $request): RedirectResponse
	{
		$faq = $this->faqRepository->updateAnswerAndQuestion($request->all(), $id);
		
		if (!$faq) {
			return $this->responseRedirectBack(
				__('faq::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirectBack(
			__('faq::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified Faq from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 *
	 */
	public function destroy(int $id): RedirectResponse
	{
		$faq = $this->faqRepository->deleteAnswerAndQuestion($id);
		
		if (!$faq) {
			return $this->responseRedirectBack(
				__('faq::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.faqs.index',
			__('faq::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	public function import()
	{
		(new ImportFaq)->import(base_path('export_faq.xlsx'));
	}
}
