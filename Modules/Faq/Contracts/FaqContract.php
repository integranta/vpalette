<?php


namespace Modules\Faq\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Faq\Models\Faq;

/**
 * Interface FaqContract
 *
 * @package Modules\Faq\Contracts
 */
interface FaqContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listAnswerAndQuestions(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Faq
     */
    public function findAnswerAndQuestionById(int $id): ?Faq;
    
    /**
     * @param  array  $params
     *
     * @return Faq
     */
    public function createAnswerAndQuestion(array $params): Faq;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Faq
     */
    public function updateAnswerAndQuestion(array $params, int $id): Faq;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteAnswerAndQuestion(int $id): bool;
}
