<?php

return [
    'name' => 'Faq',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all FAQs as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('FAQ_CACHE_KEY','index_faqs'),
    'cache_ttl' => env('FAQ_CACHE_TTL',360000),
];
