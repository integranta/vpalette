<?php

namespace Modules\Faq\Observers;

use App\Traits\Cacheable;
use Modules\Faq\Models\Faq;

/**
 * Class FaqObserver
 *
 * @package Modules\Faq\Observers
 */
class FaqObserver
{
    use Cacheable;
    
    /**
     * Handle the faq "created" event.
     *
     * @param  Faq  $faq
     *
     * @return void
     */
    public function created(Faq $faq): void
    {
        $this->incrementCountItemsInCache($faq, config('faq.cache_key'));
    }
    
    /**
     * Handle the faq "updated" event.
     *
     * @param  Faq  $faq
     *
     * @return void
     */
    public function updated(Faq $faq): void
    {
        //
    }
    
    /**
     * Handle the faq "deleted" event.
     *
     * @param  Faq  $faq
     *
     * @return void
     */
    public function deleted(Faq $faq): void
    {
        $this->decrementCountItemsInCache($faq, config('faq.cache_key'));
    }
    
    /**
     * Handle the faq "restored" event.
     *
     * @param  Faq  $faq
     *
     * @return void
     */
    public function restored(Faq $faq): void
    {
        $this->incrementCountItemsInCache($faq, config('faq.cache_key'));
    }
    
    /**
     * Handle the faq "force deleted" event.
     *
     * @param  Faq  $faq
     *
     * @return void
     */
    public function forceDeleted(Faq $faq): void
    {
        $this->decrementCountItemsInCache($faq, config('faq.cache_key'));
    }
}
