<?php

namespace Modules\Faq\Models;

use App\User;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Product\Models\Product;
use Modules\Rating\Traits\Ratingable;
use Modules\Reaction\Traits\Dislikeable;
use Modules\Reaction\Traits\Likeable;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Faq
 *
 * @package Modules\Faq\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string|null $question
 * @property string|null $answer
 * @property int|null $productId
 * @property int|null $userId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Modules\Reaction\Models\DislikeCounter|null $dislikeCounter
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Reaction\Models\Dislike[] $dislikes
 * @property-read int|null $dislikesCount
 * @property-read float|null $avgRating
 * @property-read int $dislikeCount
 * @property-read bool $disliked
 * @property-read int $likeCount
 * @property-read bool $liked
 * @property-read float|null $maxRating
 * @property-read float|null $minRating
 * @property-read float|int|null $percentRating
 * @property-read float|null $sumRating
 * @property-read \Modules\Reaction\Models\LikeCounter|null $likeCounter
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Reaction\Models\Like[] $likes
 * @property-read int|null $likesCount
 * @property-read \Modules\Product\Models\Product|null $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Rating\Models\Rating[] $ratings
 * @property-read int|null $ratingsCount
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Faq\Models\Faq onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereDislikedBy($userId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereLikedBy($userId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Faq\Models\Faq whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Faq\Models\Faq withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Faq\Models\Faq withoutTrashed()
 * @mixin \Eloquent
 */
class Faq extends Model
{
    use SoftDeletes;
    use Likeable;
    use Dislikeable;
    use Ratingable;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds


    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'faqs';


    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'question',
        'answer',
        'product_id',
        'user_id'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'boolean',
        'sort' => 'integer',
        'product_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required|boolean',
        'sort' => 'required|integer',
        'question' => 'string|max:500',
        'answer' => 'string',
        'product_id' => 'nullable|integer',
        'user_id' => 'nullable|integer'
    ];

    /**
     * Получить товар, которой принадлежит этому вопрос.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Получить пользователя, который задал вопрос.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
