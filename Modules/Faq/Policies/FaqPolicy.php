<?php

namespace Modules\Faq\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Faq\Models\Faq;

/**
 * Class FaqPolicy
 *
 * @package Modules\Faq\Policies
 */
class FaqPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the faq.
     *
     * @param  User|null  $user
     * @param  Faq        $faq
     *
     * @return mixed
     */
    public function view(?User $user, Faq $faq)
    {
        if ($faq->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished faqs')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create faqs.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create faqs')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the faq.
     *
     * @param  User  $user
     * @param  Faq   $faq
     *
     * @return mixed
     */
    public function update(User $user, Faq $faq)
    {
        if ($user->can('edit all faqs')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the faq.
     *
     * @param  User  $user
     * @param  Faq   $faq
     *
     * @return mixed
     */
    public function delete(User $user, Faq $faq)
    {
        if ($user->can('delete any faq')) {
            return true;
        }
        
        return false;
    }
}
