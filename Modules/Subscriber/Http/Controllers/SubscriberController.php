<?php

namespace Modules\Subscriber\Http\Controllers;

use App\DataTables\SubscriberDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Modules\Subscriber\Http\Requests\AcceptSubscriberRequest;
use Modules\Subscriber\Http\Requests\CreateSubscriberRequest;
use Modules\Subscriber\Http\Requests\UnSubscriptionRequest;
use Modules\Subscriber\Http\Requests\UpdateSubscriberRequest;
use Modules\Subscriber\Services\SubscriberWebService;


/**
 * Class SubscriberController
 *
 * @package Modules\Subscriber\Http\Controllers
 */
class SubscriberController extends Controller
{
    /**
     * @var SubscriberWebService
     */
    private $subscriberWebService;

    /**
     * SubscriberController constructor.
     *
     * @param  SubscriberWebService  $service
     */
    public function __construct(SubscriberWebService $service)
    {
        $this->subscriberWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\SubscriberDataTable  $dataTable
	 *
	 *
	 */
    public function index(SubscriberDataTable $dataTable)
    {
        return $this->subscriberWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->subscriberWebService->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSubscriberRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateSubscriberRequest $request): RedirectResponse
    {
        return $this->subscriberWebService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->subscriberWebService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->subscriberWebService->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateSubscriberRequest  $request
     * @param  int                      $id
     *
     * @return RedirectResponse
     */
    public function update(UpdateSubscriberRequest $request, int $id): RedirectResponse
    {
        return $this->subscriberWebService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->subscriberWebService->destroy($id);
    }

    /**
     * Handle activation Newsletters subscription for subscriber.
     *
     * @param  AcceptSubscriberRequest  $request
     *
     * @return RedirectResponse
     */
    public function accept(AcceptSubscriberRequest $request): RedirectResponse
    {
        return $this->subscriberWebService->accept($request);
    }

    /**
     * Handler unsubscription subscriber from newsletters
     *
     * @param  UnSubscriptionRequest  $request
     *
     * @return RedirectResponse
     */
    public function unsubscription(UnSubscriptionRequest $request): RedirectResponse
    {
        return $this->subscriberWebService->unsubscription($request);
    }
}
