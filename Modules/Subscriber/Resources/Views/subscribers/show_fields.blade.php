<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Active field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Активность:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('active', 0) !!}
                {!!
                    Form::checkbox(
                        'active',
                        true,
                        $model->active == 1 ? true : false,
                        ['class' => 'custom-control-input', 'id' => 'active', 'disabled']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="active"></label>
            </div>
        </div>

        <!-- Email Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'email',
                    'Email:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!! Form::email('email', $model->email, ['class' => 'form-control', 'disabled']) !!}
            </div>
        </div>
        <!-- End Email Field -->
    </div>
</div>
