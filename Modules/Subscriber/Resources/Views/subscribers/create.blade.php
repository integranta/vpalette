@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ mix('css/subscriber.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.subscriber.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>

            <div class="row">
                <div class="col-12">
                    @include('admin.partials.flash')
                    {!! Form::open(
                        [
                            'route' => 'admin.subscriber.store',
                            'method' => 'POST'
                        ])
                    !!}
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                <li class="nav-item">
                                    <a
                                        class="nav-link active"
                                        id="element-tab"
                                        data-toggle="tab"
                                        href="#element"
                                        role="tab"
                                        aria-controls="element"
                                        aria-selected="true"
                                    >
                                        Подписчик
                                    </a>
                                </li>
                            </ul>
                            @include('subscriber::subscribers.fields')
                        </div>
                        <div class="card-footer">
                            <!-- Submit Field -->
                            <div class="form-group row col-mb-12">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-4 col-md-4">
                                    {!! Form::submit('Создать', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ mix('js/subscriber.js') }}"></script>
@endspush
