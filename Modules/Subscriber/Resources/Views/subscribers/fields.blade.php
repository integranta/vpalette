<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Is Active field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Активность:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('active', 0) !!}
                {!!
                    Form::checkbox(
                        'active',
                        true,
                        false,
                        ['class' => 'custom-control-input', 'id' => 'active']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="active"></label>
            </div>
        </div>
        <!-- End Is Active field -->

        <!-- Email Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'email',
                    'Email подписчика:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::text(
                        'email',
                         null,
                         [
                            'class' => 'form-control',
                            'placeholder' => 'Введите E-mail для E-mail-рассылки'
                         ]
                    )
                !!}
            </div>
        </div>
        <!-- End Email field -->
    </div>
</div>
