<?php

return [
	'action' => 'Действия',
    'id' => 'ID',
	'email' => 'E-mail',
	'active' => 'Активность',
	'created_at' => 'Дата подписки',
	'updated_at' => 'Дата обновления',
	'deleted_at' => 'Дата отписки'
];
