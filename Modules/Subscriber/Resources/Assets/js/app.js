$(document).ready(function () {
    $('#subscribers-table').DataTable({
        language: {
            url: "/assets/configs/DataTables/Lang/ru/Russian.json"
        },
		processing: true,
		serverSize: true,
		ajax: "{{ route('admin.subscriber.index') }}",
		columns: [{
        		data: 'id',
				name: 'ID'
			},
			{
				data: 'email',
				name: 'E-mail'
			}
		]
    });
});
