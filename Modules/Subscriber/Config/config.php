<?php

return [
    'name' => 'Subscriber',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all Email subscribers as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('SUBSCRIBER_CACHE_KEY','index_subsribers'),
    'cache_ttl' => env('SUBSCRIBER_CACHE_TTL',720000),
];
