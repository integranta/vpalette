<?php


namespace Modules\Subscriber\Services;


use App\DataTables\SubscriberDataTable;
use App\Services\BaseService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Subscriber\Contracts\SubscriberContract;
use Modules\Subscriber\Http\Requests\AcceptSubscriberRequest;
use Modules\Subscriber\Http\Requests\CreateSubscriberRequest;
use Modules\Subscriber\Http\Requests\UnSubscriptionRequest;
use Modules\Subscriber\Http\Requests\UpdateSubscriberRequest;

/**
 * Class SubscriberWebService
 *
 * @package Modules\Subscriber\Services
 */
class SubscriberWebService extends BaseService
{
    /**
     * @var SubscriberContract
     */
    private $subscriberRepository;
    
    /**
     * SubscriberController constructor.
     *
     * @param  SubscriberContract  $subscriberRepo
     */
    public function __construct(SubscriberContract $subscriberRepo)
    {
        $this->subscriberRepository = $subscriberRepo;
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\SubscriberDataTable  $dataTable
	 *
	 *
	 */
    public function index(SubscriberDataTable $dataTable)
    {
        $this->setPageTitle(
            __('subscriber::messages.title'),
            __('subscriber::messages.index_subtitle'),
            __('subscriber::messages.index_leadtext')
        );
        
        return $dataTable->render('subscriber::subscribers.index');
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $this->setPageTitle(
            __('subscriber::messages.title'),
            __('subscriber::messages.create_subtitle'),
            __('subscriber::messages.create_leadtext')
        );
        return view('subscriber::subscribers.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSubscriberRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateSubscriberRequest $request): RedirectResponse
    {
        $subscriber = $this->subscriberRepository->createSubscriber($request->all());
        
        if (!$subscriber) {
            return $this->responseRedirectBack(
                __('subscriber::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.subscriber.index',
            __('subscriber::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        $subscriber = $this->subscriberRepository->findSubscriberById($id);
        $this->setPageTitle(
            __('subscriber::messages.title'),
            __('subscriber::messages.show_subtitle').$subscriber->email,
            __('subscriber::messages.show_leadtext')
        );
        return view('subscriber::subscribers.show', compact('subscriber'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        $subscriber = $this->subscriberRepository->findSubscriberById($id);
        $this->setPageTitle(
            __('subscriber::messages.title'),
            __('subscriber::messages.edit_subtitle').$subscriber->email,
            __('subscriber::messages.edit_leadtext')
        );
        return view('subscriber::subscribers.edit', compact('subscriber'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateSubscriberRequest  $request
     * @param  int                      $id
     *
     * @return RedirectResponse
     */
    public function update(UpdateSubscriberRequest $request, int $id): RedirectResponse
    {
        $subscriber = $this->subscriberRepository->updateSubscriber($request->all(), $id);
        
        if (!$subscriber) {
            return $this->responseRedirectBack(
                __('subscriber::messages.not_found'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.subscriber.index',
            __('subscriber::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $subscriber = $this->subscriberRepository->deleteSubscriber($id);
        
        if (!$subscriber) {
            return $this->responseRedirectBack(
                __('subscriber::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.subscriber.index',
            __('subscriber::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Handle activation Newsletters subscription for subscriber.
     *
     * @param  AcceptSubscriberRequest  $request
     *
     * @return RedirectResponse
     */
    public function accept(AcceptSubscriberRequest $request): RedirectResponse
    {
        $subscriberId = $this->subscriberRepository
            ->findSubscriberBy(['email' => $request->get('email')])->id;
        $subscriber = $this->subscriberRepository
            ->updateSubscriber($request->all(), $subscriberId);
        
        if (!$subscriber) {
            return $this->responseRedirectBack(
                __('subscriber::messages.error_active'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.subscriber.index',
            __('subscriber::messages.success_active'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Handler unsubscription subscriber from newsletters
     *
     * @param  UnSubscriptionRequest  $request
     *
     * @return RedirectResponse
     */
    public function unsubscription(UnSubscriptionRequest $request): RedirectResponse
    {
        $subscriberId = $this->subscriberRepository->findSubscriberBy(['email' => $request->get('email')])->id;
        $subscriber = $this->subscriberRepository->deleteSubscriber($subscriberId);
        
        if (!$subscriber) {
            return $this->responseRedirectBack(
                __('subscriber::messages.error_unsubscribe'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.subscriber.index',
            __('subscriber::messages.success_unsubscribe'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
}