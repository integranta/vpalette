<?php

namespace Modules\Subscriber\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Subscriber\Models\Subscriber;

/**
 * Class SendUnsubscriptionEmail
 *
 * @package Modules\Subscriber\Emails
 */
class SendUnsubscriptionEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * The Subscriber instance.
     *
     * @var Subscriber
     */
    public $subscriber;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('MAIL_FROM'))
            ->subject('Отписка от E-mail-рассылки на Интернет-портале ВПалитре')
            ->view('emails.subscription.unsubscription')
            ->with([
                'email' => $this->subscriber->email
            ]);
    }
}
