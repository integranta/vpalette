<?php

namespace Modules\Subscriber\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Subscriber\Models\Subscriber;

/**
 * Class SendAcceptEmailSubscription
 *
 * @package Modules\Subscriber\Emails
 */
class SendAcceptEmailSubscription extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * The Subscriber instance.
     *
     * @var Subscriber
     */
    public $subscriber;
    
    /**
     * Create a new message instance.
     *
     * @param  Subscriber  $subscriber
     *
     * @return void
     */
    public function __construct(Subscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('MAIL_FROM'))
            ->subject('Подтвержение E-mail-рассылки на Интернет-портале ВПалитре')
            ->view('emails.subscription.accept')
            ->with([
                'email' => $this->subscriber->email
            ]);
    }
}
