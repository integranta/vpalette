<?php

namespace Modules\Subscriber\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Subscriber\Models\Subscriber;

/**
 * Class SubscriberPolicy
 *
 * @package Modules\Subscriber\Policies
 */
class SubscriberPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the subscriber.
     *
     * @param  User|null   $user
     * @param  Subscriber  $subscriber
     *
     * @return mixed
     */
    public function view(?User $user, Subscriber $subscriber)
    {
        if ($subscriber->active) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished subscribers')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create subscribers.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create subscribers')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the subscriber.
     *
     * @param  User        $user
     * @param  Subscriber  $subscriber
     *
     * @return mixed
     */
    public function update(User $user, Subscriber $subscriber)
    {
        if ($user->can('edit all subscribers')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the subscriber.
     *
     * @param  User        $user
     * @param  Subscriber  $subscriber
     *
     * @return mixed
     */
    public function delete(User $user, Subscriber $subscriber)
    {
        if ($user->can('delete any subscriber')) {
            return true;
        }
        
        return false;
    }
}
