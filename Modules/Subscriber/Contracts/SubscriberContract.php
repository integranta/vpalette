<?php


namespace Modules\Subscriber\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Subscriber\Models\Subscriber;

/**
 * Interface SubscriberContract
 *
 * @package Modules\Subscriber\Contracts
 */
interface SubscriberContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listSubscribers(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Subscriber
     */
    public function findSubscriberById(int $id): ?Subscriber;
    
    /**
     * @param  array  $data
     *
     * @return Subscriber
     */
    public function findSubscriberBy(array $data): ?Subscriber;
    
    /**
     * @param  array  $params
     *
     * @return Subscriber
     */
    public function createSubscriber(array $params): Subscriber;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Subscriber
     */
    public function updateSubscriber(array $params, int $id): Subscriber;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteSubscriber(int $id): bool;
}
