<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Subscriber\Http\Controllers\SubscriberController;

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		Route::resource('subscriber', SubscriberController::class);
		Route::get('accept', [SubscriberController::class, 'accept'])
			->name('accept.subscription');
		Route::get('unsubscription', [SubscriberController::class, 'unsubscription'])
			->name('accept.unsubscription');
	});

