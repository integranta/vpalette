<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Subscriber\Models\Subscriber;

$factory->define(Subscriber::class,
    function (Faker $faker) {
        return [
            'email' => $faker->safeEmail,
            'active' => true,
            'sort' => $faker->numberBetween(1, 500)
        ];
    });
