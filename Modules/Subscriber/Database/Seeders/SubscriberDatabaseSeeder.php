<?php

namespace Modules\Subscriber\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Subscriber\Models\Subscriber;

/**
 * Class SubscriberDatabaseSeeder
 *
 * @package Modules\Subscriber\Database\Seeders
 */
class SubscriberDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Subscriber::class, 100)->create();
    }
}
