<?php

namespace Modules\Subscriber\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Subscriber
 *
 * @package Modules\Subscriber\Models
 * @property int $id
 * @property mixed $email
 * @property bool $active
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property bool $isActive
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Subscriber\Models\Subscriber onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Subscriber\Models\Subscriber whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Subscriber\Models\Subscriber withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Subscriber\Models\Subscriber withoutTrashed()
 * @mixin \Eloquent
 */
class Subscriber extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'subscribers';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = ['email', 'active', 'sort'];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'text',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Установить флаг активности.
     *
     * @param  string  $value
     *
     * @return void
     */
    public function setIsActiveAttribute(string $value): void
    {
        $this->attributes['active'] = $value === "1";
    }

    /**
     * Получить флаг активности.
     *
     * @param  string  $value
     *
     * @return bool
     */
    public function getIsActiveAttribute(string $value): bool
    {
        return $value === "1";
    }
}
