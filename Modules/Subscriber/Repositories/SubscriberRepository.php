<?php


namespace Modules\Subscriber\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Subscriber\Contracts\SubscriberContract;
use Modules\Subscriber\Models\Subscriber;
use Str;

/**
 * Class SubscriberRepository
 *
 * @package Modules\Subscriber\Repositories
 */
class SubscriberRepository extends BaseRepository implements SubscriberContract
{
    
    /**
     * SubscriberRepository constructor.
     *
     * @param  Subscriber  $model
     */
    public function __construct(Subscriber $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listSubscribers(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('subscriber.cache_key').$postfix,
            config('subscriber.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return Subscriber
     */
    public function findSubscriberById(int $id): ?Subscriber
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return Subscriber
     */
    public function createSubscriber(array $params): Subscriber
    {
        return $this->create($params);
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Subscriber
     */
    public function updateSubscriber(array $params, int $id): Subscriber
    {
        return $this->update($params, $id);
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteSubscriber(int $id): bool
    {
        return $this->delete($id);
    }
    
    /**
     * @param  array  $data
     *
     * @return Subscriber
     */
    public function findSubscriberBy(array $data): ?Subscriber
    {
        return $this->findBy($data);
    }
}
