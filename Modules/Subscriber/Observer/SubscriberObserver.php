<?php


namespace Modules\Subscriber\Observer;

use App;
use App\Traits\Cacheable;
use Mail;
use Modules\Subscriber\Emails\SendAcceptEmailSubscription;
use Modules\Subscriber\Emails\SendUnsubscriptionEmail;
use Modules\Subscriber\Models\Subscriber;

/**
 * Class SubscriberObserver
 *
 * @package Modules\Subscriber\Observer
 */
class SubscriberObserver
{
    use Cacheable;
    
    /**
     * Handle the subscriber "created" event.
     *
     * @param  Subscriber  $subscriber
     *
     * @return void
     */
    public function created(Subscriber $subscriber): void
    {
        if (App::environment('production')) {
            Mail::to($subscriber->email)->send(new SendAcceptEmailSubscription($subscriber));
        }
        
        $this->incrementCountItemsInCache($subscriber, config('subscriber.cache_key'));
    }
    
    /**
     * Handle the subscriber "deleted" event.
     *
     * @param  Subscriber  $subscriber
     *
     * @return void
     */
    public function deleted(Subscriber $subscriber): void
    {
        if (App::environment('production')) {
            Mail::to($subscriber->email)->send(new SendUnsubscriptionEmail($subscriber));
        }
        
        $this->decrementCountItemsInCache($subscriber, config('subscriber.cache_key'));
    }
}
