<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateCategoriesTable
 */
class CreateCategoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true)->index();
            $table->unsignedInteger('sort', false)->default(500);
            $table->string('name', 255);
            $table->string('slug', 255)->index();
            $table->longText('seo_text')->nullable();
            $table->longText('dropdown_text')->nullable();
            $table->longText('full_path')->nullable();
            $table->longText('dropdown_image_src')->nullable();
            $table->longText('section_image_src')->nullable();
            $table->longText('menu_icon_src')->nullable();
            $table->unsignedBigInteger('parent_id')
                ->default(1)
                ->nullable()
                ->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
