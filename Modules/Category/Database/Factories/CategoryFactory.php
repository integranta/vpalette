<?php

use Faker\Generator as Faker;
use Modules\Category\Models\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'seo_text' => $faker->text(50),
        'parent_id' => null,
        'active' => true,
        'menu_icon_src' => '',
        'image_src' => '',
        'detail_image_src' => '',
        'sort' => $faker->numberBetween(1, 500)
    ];
});
