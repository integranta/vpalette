<?php

namespace Modules\Category\Observers;

use App\Traits\Cacheable;
use Cache;
use Modules\Category\Models\Category;
use Storage;
use Str;

/**
 * Class CategoryObserver
 *
 * @package Modules\Category\Observers
 */
class CategoryObserver
{
    use Cacheable;
    
    /**
     * Handle the category "created" event.
     *
     * @param  Category  $category
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function creating(Category $category): void
    {
        $category->full_path = $category->getUrl();
        $this->createDirectory($category->full_path);
        $this->prependImages($category);
        $this->prependSEOText($category);
    }
    
    /**
     * Создаёт папку для категории.
     *
     * @param  string  $categorySlug
     */
    private function createDirectory(string $categorySlug): void
    {
        Storage::disk('uploads')->makeDirectory("categories/{$categorySlug}");
    }
    
    /**
     * Подготавливает картинки и иконки для категории.
     *
     * @param  Category  $category
     */
    private function prependImages(Category $category): void
    {
        $randName = Str::random();
        
        $fromSectionImage = "faker/catalog/{$category->slug}".'.png';
        $fromIconImage = "faker/catalog-dropdown-list/{$category->slug}".'.svg';
        $fromMenuImage = 'faker/catalog-dropdown-list/category-dropdown-image.png';
        
        $toSectionImage = "categories{$category->full_path}/section/$randName".'.png';
        $toIconImage = "categories{$category->full_path}/icon/$randName".'.svg';
        $toMenuImage = "categories{$category->full_path}/menu/$randName".'.png';
        
        if (Storage::disk('uploads')->exists($fromSectionImage)) {
            Storage::disk('uploads')->copy($fromSectionImage, $toSectionImage);
            $category->section_image_src = $toSectionImage;
        } else {
            $fromSectionImage = null;
            $category->section_image_src = null;
        }
        
        if (Storage::disk('uploads')->exists($fromIconImage)) {
            Storage::disk('uploads')->copy($fromIconImage, $toIconImage);
            $category->menu_icon_src = $toIconImage;
        } else {
            $fromIconImage = null;
            $category->menu_icon_src = null;
        }
        
        if (Storage::disk('uploads')->exists($fromMenuImage)) {
            Storage::disk('uploads')->copy($fromMenuImage, $toMenuImage);
            $category->dropdown_image_src = $toMenuImage;
        } else {
            $fromMenuImage = null;
            $category->dropdown_image_src = null;
        }
    }
    
    /**
     * Подготавливает SEO текста для категории.
     *
     * @param  Category  $category
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function prependSEOText(Category $category): void
    {
        $sourceText = "faker/catalog/texts/{$category->slug}.txt";
        
        if (Storage::disk('uploads')->exists($sourceText)) {
            $category->seo_text = Storage::disk('uploads')->get($sourceText);
        } else {
            $category->seo_text = null;
        }
    }
    
    /**
     * Handle the category "created" event.
     *
     * @param  Category  $category
     *
     * @return void
     */
    public function created(Category $category): void
    {
        $this->incrementCountItemsInCache($category, config('category.cache_key'));
    }
    
    /**
     * Handle the category "updating" event.
     *
     * @param  Category  $category
     *
     * @return void
     */
    public function updating(Category $category): void
    {
        $category->full_path = $category->getUrl();
        //$this->createDirectory($category->full_path);
    }
    
    /**
     * Handle the category "updated" event.
     *
     * @param  Category  $category
     *
     * @return void
     */
    public function updated(Category $category): void
    {
        Cache::clear();
    }
    
    /**
     * Handle the category "deleted" event.
     *
     * @param  Category  $category
     *
     * @return void
     */
    public function deleted(Category $category): void
    {
        Storage::disk('uploads')->deleteDirectory('categories/'.$category->full_path);
        $this->decrementCountItemsInCache($category, config('category.cache_key'));
    }
    
    /**
     * Handle the category "restored" event.
     *
     * @param  Category  $category
     *
     * @return void
     */
    public function restored(Category $category): void
    {
        $this->incrementCountItemsInCache($category, config('category.cache_key'));
    }
    
    /**
     * Handle the category "force deleted" event.
     *
     * @param  Category  $category
     *
     * @return void
     */
    public function forceDeleted(Category $category): void
    {
        $this->decrementCountItemsInCache($category, config('category.cache_key'));
    }
}
