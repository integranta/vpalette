<?php

return [
    'name' => 'Category',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all categories as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('CATEGORY_CACHE_KEY','index_categories'),
    'cache_ttl' => env('CATEGORY_CACHE_TTL',3600),

    /*
    |--------------------------------------------------------------------------
    | Cache settings for tree categories
    |--------------------------------------------------------------------------
    |
    | This option controls the default cache connection that gets used while
    | getting a list of all categories in the form of a tree.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key_tree' => env('CATEGORY_CACHE_KEY_TREE','index.category-tree'),
    'cache_ttl_tree' => env('CATEGORY_CACHE_TTL_TREE',3600)
];
