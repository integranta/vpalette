<?php

namespace Modules\Category\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Category\Models\Category;

/**
 * Class CategoryPolicy
 *
 * @package Modules\Category\Policies
 */
class CategoryPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view the category.
     *
     * @param  User|null  $user
     * @param  Category   $category
     *
     * @return mixed
     */
    public function view(?User $user, Category $category)
    {
        if ($category->active) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished categories')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create categories.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create categories')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the category.
     *
     * @param  User      $user
     * @param  Category  $category
     *
     * @return mixed
     */
    public function update(User $user, Category $category)
    {
        if ($user->can('edit all categories')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the category.
     *
     * @param  User      $user
     * @param  Category  $category
     *
     * @return mixed
     */
    public function delete(User $user, Category $category)
    {
        if ($user->can('delete any category')) {
            return true;
        }
        
        return false;
    }
}
