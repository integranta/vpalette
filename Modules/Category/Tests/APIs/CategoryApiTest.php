<?php namespace Modules\Category\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Category\Tests\ApiTestTrait;
use Modules\Category\Tests\TestCase;
use Modules\Category\Tests\Traits\MakeCategoryTrait;

/**
 * Class CategoryApiTest
 *
 * @package Modules\Category\Tests\APIs
 */
class CategoryApiTest extends TestCase
{
    use MakeCategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_category()
    {
        $category = $this->fakeCategoryData();
        $this->response = $this->json('POST', '/api/categories', $category);
        
        $this->assertApiResponse($category);
    }
    
    /**
     * @test
     */
    public function test_read_category()
    {
        $category = $this->makeCategory();
        $this->response = $this->json('GET', '/api/categories/'.$category->id);
        
        $this->assertApiResponse($category->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_category()
    {
        $category = $this->makeCategory();
        $editedCategory = $this->fakeCategoryData();
        
        $this->response = $this->json('PUT', '/api/categories/'.$category->id, $editedCategory);
        
        $this->assertApiResponse($editedCategory);
    }
    
    /**
     * @test
     */
    public function test_delete_category()
    {
        $category = $this->makeCategory();
        $this->response = $this->json('DELETE', '/api/categories/'.$category->id);
        
        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/categories/'.$category->id);
        
        $this->response->assertStatus(404);
    }
}
