<?php


namespace Modules\Category\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Category\Models\Category;

/**
 * Interface CategoryContract
 *
 * @package Modules\Category\Contracts
 */
interface CategoryContract
{
    /**
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection|array
     */
    public function listCategories(array $columns = ['*'], string $order = 'id', string $sort = 'desc');
    
    /**
     * @param  int  $id
     *
     * @return Category
     */
    public function findCategoryById(int $id): ?Category;
    
    /**
     * @param  array  $params
     *
     * @return Category
     */
    public function createCategory(array $params): Category;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Category
     */
    public function updateCategory(array $params, int $id): Category;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteCategory(int $id): bool;
}
