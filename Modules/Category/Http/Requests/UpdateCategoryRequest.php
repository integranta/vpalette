<?php

namespace Modules\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateCategoryRequest
 *
 * @package Modules\Category\Http\Requests
 */
class UpdateCategoryRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:191',
            'parent_id' => 'required|not_in:0',
            'slug' => 'unique:categories,id,'.$this->input('id'),
            'section_image_src' => 'mimes:jpg,jpeg,png|max:1000',
            'menu_icon_src' => 'nullable|mimes:jpg,jpeg,png,svg|max:1000',
            'dropdown_image_src' => 'nullable|mimes:jpg,jpeg,png|max:1000'
        ];
    }
}
