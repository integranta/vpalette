<?php

namespace Modules\Category\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Modules\Category\Http\Requests\API\V1\CreateCategoryAPIRequest;
use Modules\Category\Http\Requests\API\V1\UpdateCategoryAPIRequest;
use Modules\Category\Models\Category;
use Modules\Category\Services\API\V1\CategoryApiService;

/**
 * Class CategoryAPIController
 *
 * @package Modules\Category\Http\Controllers\API\V1
 */
class CategoryAPIController extends Controller
{
    /** @var CategoryApiService */
    private $categoryApiService;

    /**
     * CategoryAPIController constructor.
     *
     * @param  CategoryApiService  $service
     */
    public function __construct(CategoryApiService $service)
    {
        $this->categoryApiService = $service;
        $this->middleware('auth:api')->except(['index', 'show']);
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->categoryApiService->index();
    }

    /**
     * @param  CreateCategoryAPIRequest  $request
     *
     * @return JsonResponse
     * @throws AuthorizationException
     *
     */
    public function store(CreateCategoryAPIRequest $request): JsonResponse
    {
        $this->authorize('create', Category::class);
        return $this->categoryApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws AuthorizationException
     *
     */
    public function show(int $id): JsonResponse
    {
        $this->authorize('view', Category::find($id));
        return $this->categoryApiService->show($id);
    }

    /**
     * @param  int                       $id
     * @param  UpdateCategoryAPIRequest  $request
     *
     * @return JsonResponse
     *
     * @throws AuthorizationException
     *
     */
    public function update(int $id, UpdateCategoryAPIRequest $request): JsonResponse
    {
        $this->authorize('update', Category::find($id));
        return $this->categoryApiService->update($id, $request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws AuthorizationException
     *
     */
    public function destroy(int $id): JsonResponse
    {
        $this->authorize('delete', Category::find($id));
        return $this->categoryApiService->destroy($id);
    }
}
