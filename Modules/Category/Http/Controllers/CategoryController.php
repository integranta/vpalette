<?php

namespace Modules\Category\Http\Controllers;

use App\DataTables\CategoriesDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Modules\Category\Http\Requests\CreateCategoryRequest;
use Modules\Category\Http\Requests\UpdateCategoryRequest;
use Modules\Category\Services\CategoryWebService;


/**
 * Class CategoryController
 *
 * @package Modules\Category\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryWebService
     */
    private $categoryWebService;

    /**
     * CategoryController constructor.
     *
     * @param  CategoryWebService  $service
     */
    public function __construct(CategoryWebService $service)
    {
        $this->categoryWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Category.
	 *
	 * @param  \App\DataTables\CategoriesDataTable  $dataTable
	 *
	 * @return View|\Illuminate\Http\JsonResponse
	 */
    public function index(CategoriesDataTable $dataTable)
    {
        return $this->categoryWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return View
     */
    public function create(): View
    {
        return $this->categoryWebService->create();
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  CreateCategoryRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateCategoryRequest $request): RedirectResponse
    {
        return $this->categoryWebService->store($request);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->categoryWebService->edit($id);
    }
	
	/**
	 * Show the form for display the specified Category.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|View
	 */
	public function show(int $id): View
	{
		return $this->categoryWebService->show($id);
	}

    /**
     * Update the specified Category in storage.
     *
     * @param  UpdateCategoryRequest  $request
     * @param  int                    $id
     *
     * @return RedirectResponse
     */
    public function update(UpdateCategoryRequest $request, int $id): RedirectResponse
    {
        return $this->categoryWebService->update($request, $id);
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->categoryWebService->destroy($id);
    }
}
