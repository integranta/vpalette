import { tinyMCEConfiguration } from "../../../../../resources/js/shared/tinyMCEConfig";

$(document).ready(() => {
    tinymce.init({
        ...tinyMCEConfiguration,
        selector: "textarea#description"
    });
});
