<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- 'bootstrap / Toggle Switch Is Active Field' -->
        <!-- Active field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Активность:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('active', 0) !!}
                {!!
                    Form::checkbox(
                        'active',
                        true,
                        $targetCategory->active,
                        [
                        	'class' => 'custom-control-input',
                        	'id' => 'active',
                        	'disabled'
                        ]
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="active"></label>
            </div>
        </div>

        <!-- Name Field -->
        <div class="form-group row mb-4">
            <label
                for="name"
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
            >
                Название
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите название"
                    id="name"
                    name="name"
                    value="{{ $targetCategory->name }}"
					disabled
                />
            </div>
        </div>

        <!-- Description Field -->
        <div class="form-group row mb-4">
            <label
                for="seo_text"
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
            >
                Описание
            </label>
            <div class="col-sm-12 col-md-7">
                <textarea
                    class="form-control form-control-lg"
                    type="text"
                    placeholder="Введите описание для картегории"
                    id="seo_text"
                    name="seo_text"
					disabled
                >
                    {{ $targetCategory->seo_text }}
                </textarea>
            </div>
        </div>

        <!-- Dropdown text Field -->
        <div class="form-group row mb-4">
            <label
                for="dropdown_text"
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
            >
                Текст для выпадающего меню
            </label>
            <div class="col-sm-12 col-md-7">
                <textarea
                    class="form-control"
                    type="text"
                    placeholder="Введите описание для выпадающего меню"
                    id="dropdown_text"
                    name="dropdown_text"
					disabled
                >
                    {{ $targetCategory->dropdown_text }}
                </textarea>
            </div>
        </div>

        <!-- Parent Id -->
        <div class="form-group row mb-4">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
                for="parent">Корневая категория <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="parent"
                    class="form-control custom-select mt-15"
                    name="parent_id"
					disabled
                >
                    <option value="0">Выберите родительскую категорию</option>

                    @foreach($categories as $key => $category)
                        @if ($targetCategory->parent_id === $key)
                            <option selected value="{{ $key }}">
                                {{ $category }}
                            </option>
                        @else
                            <option value="{{ $key }}">{{ $category }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row mb-4">
            <label
                for="section_image_src"
                class="control-label col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold">
                Картинка раздела в каталоге
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                @if ($targetCategory->section_image_src)
                    <img class="img-fluid"
                         src="{{ asset($targetCategory->section_image_src) }}"
                         alt="{{ $targetCategory->name }}"
                    >
                @endif
            </div>
        </div>

        <div class="form-group row mb-4">
            <label
                for="menu_icon_src"
                class="control-label col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold">
                Иконка раздела
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                @if ($targetCategory->menu_icon_src)
                    <img
                        src="{{ asset($targetCategory->menu_icon_src) }}"
                        alt="{{ $targetCategory->name }}"
                        width="64" height="64"
                    >
                @endif
            </div>
        </div>

        <div class="form-group row mb-4">
            <label
                for="dropdown_image_src"
                class="control-label col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold">
                Картинка для SEO описания подкатегорий
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                @if ($targetCategory->dropdown_image_src)
                    <img class="img-fluid"
                         src="{{ asset($targetCategory->dropdown_image_src) }}"
                         alt="{{ $targetCategory->name }}"
                    >
                @endif
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">

        <!-- Meta Title Field -->
        <div class="form-group row align-items-center">
			<label for="title" class="form-control-label col-sm-3 text-md-right">Meta title:</label>
            <div class="col-sm-6 col-md-9">
				<input
					type="text"
					name="title"
					id="title"
					class="form-control"
					value="{{ $targetCategory->seo->title ?? '' }}"
					disabled
				>
            </div>
        </div>

        <!-- Description Field -->
        <div class="form-group row align-items-center">
			<label for="description" class="form-control-label col-sm-3 text-md-right">Meta description:</label>
			<div class="col-sm-6 col-md-9">
				<input
					type="text"
					name="description"
					id="description"
					class="form-control"
					value="{{ $targetCategory->seo->description ?? '' }}"
					disabled
				>
			</div>
        </div>
        <!-- Keywords Field -->
        <div class="form-group row align-items-center">
			<label for="keywords" class="form-control-label col-sm-3 text-md-right">Meta keywords:</label>
            <div class="col-sm-6 col-md-9">
				<textarea name="keywords" id="keywords" class="form-control codeeditor">
					{{ $targetCategory->seo->keywords ?? '' }}
				</textarea>
            </div>
        </div>
        <!-- Canonical URL Field -->
        <div class="form-group row align-items-center">
			<label for="canonical_url" class="form-control-label col-sm-3 text-md-right">canonical_url:</label>
			<div class="col-sm-6 col-md-9">
				<input
					type="text"
					name="canonical_url"
					id="canonical_url"
					class="form-control"
					value="{{ $targetCategory->seo->canonical_url ?? '' }}"
					disabled
				>
			</div>
        </div>

        <!-- Og Title Field -->
        <div class="form-group row align-items-center">
			<label for="og_title" class="form-control-label col-sm-3 text-md-right">og_title:</label>
			<div class="col-sm-6 col-md-9">
				<input
					type="text"
					name="og_title"
					id="og_title"
					class="form-control"
					value="{{ $targetCategory->seo->og_title ?? '' }}"
					disabled
				>
			</div>
        </div>
        <!-- End title Field -->

        <!-- Og Description Field -->
        <div class="form-group row align-items-center">
			<label for="og_description" class="form-control-label col-sm-3 text-md-right">Meta og_description:</label>
			<div class="col-sm-6 col-md-9">
				<textarea name="og_description" id="og_description" class="form-control codeeditor">
					{{ $targetCategory->seo->og_description ?? '' }}
				</textarea>
			</div>
        </div>
        <!-- End description Field -->


        <!-- Og Image Field -->
        <div class="form-group row align-items-center">
			<label for="og_image" class="form-control-label col-sm-3 text-md-right">Meta og_image:</label>
            <div class="col-sm-6 col-md-9">
				@if (@isset($targetCategory->seo->og_image))
					<img class="img-fluid"
						 src="{{ asset($targetCategory->seo->og_image) }}"
						 alt="{{ $targetCategory->name  }}"
					>
				@endif
            </div>
        </div>
        <!-- End og image Field -->


        <!-- Og type Field -->
        <div class="form-group row align-items-center">
			<label for="og_type" class="form-control-label col-sm-3 text-md-right">Meta og_type:</label>
			<div class="col-sm-6 col-md-9">
				<input
					type="text"
					name="og_type"
					id="og_type"
					class="form-control"
					value="{{ $targetCategory->seo->og_type ?? '' }}"
					disabled
				>
			</div>
        </div>
        <!-- End og type Field -->


        <!-- Og url Field -->
        <div class="form-group row align-items-center">
			<label for="og_url" class="form-control-label col-sm-3 text-md-right">Meta og_url:</label>
			<div class="col-sm-6 col-md-9">
				<input
					type="text"
					name="og_url"
					id="og_url"
					class="form-control"
					value="{{ $targetCategory->seo->og_url ?? '' }}"
					disabled
				>
			</div>
        </div>
        <!-- End og url Field -->

        <div class="card-header">
            <h4>Реклама</h4>
        </div>

        <div class="form-group row">
			<label for="ya_direct" class="form-control-label col-sm-3 text-md-right">Яндекс.Директ:</label>
			<div class="col-sm-6 col-md-9">
				<textarea name="ya_direct" id="ya_direct" class="form-control codeeditor">
					{{ $targetCategory->seo->ya_direct ?? '' }}
				</textarea>
			</div>
        </div>
        <div class="card-header">
            <h4>Счётчики:</h4>
        </div>
        <div class="form-group row">
			<label for="google_tag_manager" class="form-control-label col-sm-3 text-md-right">Google Tag Manager:</label>
			<div class="col-sm-6 col-md-9">
				<textarea name="google_tag_manager" id="google_tag_manager" class="form-control codeeditor">
					{{ $targetCategory->seo->google_tag_manager ?? '' }}
				</textarea>
			</div>
        </div>
        <div class="form-group row">
			<label for="google_analytics" class="form-control-label col-sm-3 text-md-right">Google Analytics:</label>
			<div class="col-sm-6 col-md-9">
				<textarea name="google_analytics" id="google_analytics" class="form-control codeeditor">
					{{ $targetCategory->seo->google_analytics ?? '' }}
				</textarea>
			</div>
        </div>
        <div class="form-group row">
			<label for="ya_metrica" class="form-control-label col-sm-3 text-md-right">Яндекс.Метрика:</label>
			<div class="col-sm-6 col-md-9">
				<textarea name="ya_metrica" id="ya_metrica" class="form-control codeeditor">
					{{ $targetCategory->seo->ya_metrica ?? '' }}
				</textarea>
			</div>
        </div>
    </div>
</div>
