@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('css/category.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.categories.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')

                    {!! Form::model($targetCategory,
                        [
                            'route' => ['admin.categories.show', $targetCategory->id],
                            'method' => 'get',
                            'files' => 'true'
                        ])
                    !!}
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                <li class="nav-item">
                                    <a
                                        class="nav-link active"
                                        id="element-tab"
                                        data-toggle="tab"
                                        href="#element"
                                        role="tab"
                                        aria-controls="element"
                                        aria-selected="true"
                                    >
                                        Элемент
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a
                                        class="nav-link"
                                        id="seo-tab"
                                        data-toggle="tab"
                                        href="#seo"
                                        role="tab"
                                        aria-controls="seo"
                                        aria-selected="false"
                                    >
                                        SEO
                                    </a>
                                </li>
                            </ul>
                            @include('category::categories.show_fields')
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('libs/js/tinymce/langs/ru.js') }}"></script>

    {{--  Module JS File  --}}
    <script src="{{ asset('js/category.js') }}"></script>
@endpush
