<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- 'bootstrap / Toggle Switch Is Active Field' -->
        <!-- Active field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Активность:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('active', 0) !!}
                {!!
                    Form::checkbox(
                        'active',
                        true,
                        old('active', true),
                        ['class' => 'custom-control-input', 'id' => 'active']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="active"></label>
            </div>
        </div>

        <!-- Name Field -->
        <div class="form-group row mb-4">
            <label
                for="name"
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
            >
                Название
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <input
                    class="form-control @error('name') is-invalid @enderror"
                    type="text"
                    placeholder="Введите название"
                    id="name"
                    name="name"
                    value="{{ old('name', '') }}"
                />
                @error('name') {{ $message }} @enderror
            </div>
        </div>

        <!-- Seo text Field -->
        <div class="form-group row mb-4">
            <label
                for="seo_text"
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
            >
                Описание
            </label>
            <div class="col-sm-12 col-md-7">
                <textarea
                    class="form-control"
                    type="text"
                    placeholder="Введите описание для категории"
                    id="seo_text"
                    name="seo_text"
                >
                    {{ old('seo_text', '') }}
                </textarea>
            </div>
        </div>

        <!-- Dropdown text Field -->
        <div class="form-group row mb-4">
            <label
                for="dropdown_text"
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
            >
                Текст для выпадающего меню
            </label>
            <div class="col-sm-12 col-md-7">
                <textarea
                    class="form-control"
                    type="text"
                    placeholder="Введите описание для выпадающего меню"
                    id="dropdown_text"
                    name="dropdown_text"
                >
                    {{ old('dropdown_text', '') }}
                </textarea>
            </div>
        </div>

        <!-- Parent Id -->
        <div class="form-group row mb-4">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
                for="parent_id">Корневая категория <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="parent_id"
                    class="form-control custom-select mt-15 @error('parent_id') is-invalid @enderror"
                    name="parent_id"
                >
                    <option value="0">Выберите родительскую категорию</option>

                    @foreach($categories as $key => $category)
                        <option value="{{ $key }}"> {{ $category }} </option>
                    @endforeach
                </select>
                @error('parent_id') {{ $message }} @enderror
            </div>
        </div>


        <div class="form-group row mb-4">
            <label
                for="section_image_src"
                class="control-label col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold">
                Картинка раздела в каталоге
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <input
                    class="form-control @error('section_image_src') is-invalid @enderror"
                    type="file"
                    id="section_image_src"
                    name="section_image_src"
                />
                @error('section_image_src') {{ $message }} @enderror
            </div>
        </div>

        <div class="form-group row mb-4">
            <label
                for="menu_icon_src"
                class="control-label col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold">
                Иконка раздела
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <input
                    class="form-control @error('menu_icon_src') is-invalid @enderror"
                    type="file"
                    id="menu_icon_src"
                    name="menu_icon_src"
                />
                @error('menu_icon_src') {{ $message }} @enderror
            </div>
        </div>


        <div class="form-group row mb-4">
            <label
                for="dropdown_image_src"
                class="control-label col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold">
                Картинка для SEO описания подкатегорий
                <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <input
                    class="form-control @error('dropdown_image_src') is-invalid @enderror"
                    type="file"
                    id="dropdown_image_src"
                    name="dropdown_image_src"
                />
                @error('dropdown_image_src') {{ $message }} @enderror
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">

        <!-- Meta Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'title',
                    'Meta title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'title',
                        old('title', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>

        <!-- Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'description',
                    'Meta Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'description',
                        old('description', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- Keywords Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'keywords',
                    'Keywords:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'keywords',
                        old('keywords', ''),
                        ['class' => 'form-control codeeditor', 'id' => 'keywords']
                    )
                !!}
            </div>
        </div>
        <!-- Canonical URL Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'canonical_url',
                    'Canonical URL:',
                    [
                        'class' => 'form-control-label col-sm-3 text-md-right',
                        'id' => 'canonical_url'
                    ]
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'canonical_url',
                        old('canonical_url', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>

        <!-- Og Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_title',
                    'Open Graph Title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_title',
                        old('og_title', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End title Field -->

        <!-- Og Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_description',
                    'Og Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'og_description',
                        old('og_description', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End description Field -->


        <!-- Og Image Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_image',
                    'Open Graph Image:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::file(
                        'og_image',
                        old('og_image', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End og image Field -->


        <!-- Og type Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_type',
                    'Open Graph type:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_type',
                        old('og_type', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End og type Field -->


        <!-- Og url Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_url',
                    'Open Graph url:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_url',
                        old('og_url', ''),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End og url Field -->

        <div class="card-header">
            <h4>Реклама</h4>
        </div>

        <div class="form-group row">
            {!!
                Form::label(
                    'ya_direct',
                    'Яндекс.Директ:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_direct',
                        old('ya_direct', ''),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_direct'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="card-header">
            <h4>Счётчики:</h4>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'google_tag_manager',
                    'Google Tag Manager:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_tag_manager',
                        old('google_tag_manager', ''),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_tag_manager'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'google_analytics',
                    'Google Analytics:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_analytics',
                        old('google_anlytics', ''),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_analytics'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'ya_metrica',
                    'Яндекс.Метрика:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_metrica',
                        old('ya_metrica', ''),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_metrica'
                        ]
                    )
                !!}
            </div>
        </div>
    </div>
</div>
