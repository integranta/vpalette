<?php

namespace Modules\Category\Models;

use App\Models\BaseModel;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Attribute\Models\Attribute;
use Modules\Product\Models\Product;
use Modules\Seo\Models\Seo;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Storage;
use TypiCMS\NestableTrait;

/**
 * Class Category
 *
 * @package Modules\Category\Models
 * @property int                                                                                 $id
 * @property bool                                                                                $active
 * @property int                                                                                 $sort
 * @property string                                                                              $name
 * @property string                                                                              $slug
 * @property string|null                                                                         $seoText
 * @property string|null                                                                         $dropdownText
 * @property string|null                                                                         $fullPath
 * @property string|null                                                                         $dropdownImageSrc
 * @property string|null                                                                         $sectionImageSrc
 * @property string|null                                                                         $menuIconSrc
 * @property int|null                                                                            $parentId
 * @property \Illuminate\Support\Carbon|null                                                     $createdAt
 * @property \Illuminate\Support\Carbon|null                                                     $updatedAt
 * @property \Illuminate\Support\Carbon|null                                                     $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Attribute\Models\Attribute[] $attributes
 * @property-read int|null                                                                       $attributesCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Category\Models\Category[]   $children
 * @property-read int|null                                                                       $childrenCount
 * @property-read \Modules\Category\Models\Category|null                                         $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Models\Product[]     $products
 * @property-read int|null                                                                       $productsCount
 * @property-read \Modules\Seo\Models\Seo|null                                                   $seo
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|Category onlyTrashed()
 * @method static Builder|Category query()
 * @method static Builder|Category whereActive($value)
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereDeletedAt($value)
 * @method static Builder|Category whereDropdownImageSrc($value)
 * @method static Builder|Category whereDropdownText($value)
 * @method static Builder|Category whereFullPath($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereMenuIconSrc($value)
 * @method static Builder|Category whereName($value)
 * @method static Builder|Category whereParentId($value)
 * @method static Builder|Category whereSectionImageSrc($value)
 * @method static Builder|Category whereSeoText($value)
 * @method static Builder|Category whereSlug($value)
 * @method static Builder|Category whereSort($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Category withoutTrashed()
 * @mixin \Eloquent
 * @method static \TypiCMS\NestableCollection|static[] all($columns = ['*'])
 * @method static \TypiCMS\NestableCollection|static[] get($columns = ['*'])
 */
class Category extends BaseModel
{
	use SoftDeletes;
	use NestableTrait;
	use SoftCascadeTrait;
	use QueryCacheable;
	
		/**
	 * Контанта для хранение ЧПУ корневой категорий.
	 */
	const ROOT_LEVEL = 'vverhnij_uroven'; // cache time for package, in seconds
	/**
	 * Правила для валидации атрибутов.
	 *
	 * @var array
	 */
	public static $rules = [
		'active'             => 'nullable|boolean',
		'name'               => 'required|string|max:191',
		'sort'               => 'required|integer',
		'parent_id'          => 'required|not_in:0',
		'slug'               => 'nullable|string',
		'seo_text'           => 'nullable|string',
		'dropdown_text'      => 'nullable|string',
		'section_image_src'  => 'nullable|mimes:jpg,png,svg,bmp,jpeg|max:1000',
		'menu_icon_src'      => 'nullable|mimes:jpg,png,svg,bmp,jpeg|max:1000',
		'dropdown_image_src' => 'nullable|mimes:jpg,png,svg,bmp,jpeg|max:1000',
	];
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600;
	
	/**
	 * Связанная с моделью таблица.
	 *
	 * @var string
	 */
	public $table = 'categories';
	
	/**
	 * Отношение, которые всегда должны быть загружены.
	 *
	 * @var array
	 */
	//protected $with = ['children', 'children.products'];
	
	/**
	 * Атрибуты, которые должны быть преобразованы в даты.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];
	/**
	 * Атрибуты, для которых разрешено массовое назначение.
	 *
	 * @var array
	 */
	protected $fillable = [
		'active',
		'sort',
		'name',
		'slug',
		'dropdown_text',
		'seo_text',
		'section_image_src',
		'menu_icon_src',
		'dropdown_image_src',
		'parent_id',
		'full_path'
	];
	/**
	 * Атрибуты, которые должны быть всегда скрыты.
	 *
	 * @var array
	 */
	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at'
	];
	/**
	 * Атрибуты, которые нужно преобразовать в нативный тип.
	 *
	 * @var array
	 */
	protected $casts = [
		'id'                 => 'integer',
		'active'             => 'boolean',
		'sort'               => 'integer',
		'name'               => 'string',
		'slug'               => 'string',
		'dropdown_text'      => 'string',
		'seo_text'           => 'string',
		'section_image_src'  => 'string',
		'menu_icon_src'      => 'string',
		'dropdown_image_src' => 'string',
		'parent_id'          => 'integer'
	];
	
	/**
	 * Возвращает полный путь до корневой категории.
	 *
	 * @return string
	 */
	public function getUrl(): string
	{
		$url = $this->slug;
		
		$category = $this;
		
		while ($category = $category->parent) {
			if ($category->slug === self::ROOT_LEVEL) {
				continue;
			}
			$url = $category->slug.'/'.$url;
		}
		
		return '/catalog/'.$url;
	}
	
	/**
	 * Получить ссылку на картинку категорий.
	 *
	 * @param  string|null  $value
	 *
	 * @return string|null
	 */
	public function getSectionImageSrcAttribute(?string $value): ?string
	{
		return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
	}
	
	/**
	 * Получить ссылку на иконку категорий.
	 *
	 * @param  string|null  $value
	 *
	 * @return string|null
	 */
	public function getMenuIconSrcAttribute(?string $value): ?string
	{
		return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
	}
	
	
	/**
	 * Получить ссылку на картинку-обложку для выпадающего меню.
	 *
	 * @param  string|null  $value
	 *
	 * @return string|null
	 */
	public function getDropdownImageSrcAttribute(?string $value): ?string
	{
		return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
	}
	
	/**
	 * Получить родительскую категорию, которая принадлежит этой категорий.
	 *
	 * @return BelongsTo
	 */
	public function parent(): BelongsTo
	{
		return $this->belongsTo(self::class, 'parent_id');
	}
	
	
	/**
	 * Получить дочерние категории.
	 *
	 * @return HasMany
	 */
	public function children(): HasMany
	{
		return $this->hasMany(self::class, 'parent_id');
	}
	
	
	/**
	 * Товары, принадлежащие этой категорий.
	 *
	 * @return BelongsToMany
	 */
	public function products(): BelongsToMany
	{
		return $this->belongsToMany(
			Product::class,
			'category_product',
			'category_id',
			'product_id'
		)->withPivot(['category_id', 'product_id']);
	}
	
	
	/**
	 * Получить связанные SEO параметры для категорий.
	 *
	 * @return MorphOne
	 */
	public function seo(): MorphOne
	{
		return $this->morphOne(Seo::class, 'seoble');
	}
	
	/**
	 * Атрибуты, принадлежащие этой категорий.
	 *
	 * @return BelongsToMany
	 */
	public function attributes(): BelongsToMany
	{
		return $this->belongsToMany(Attribute::class);
	}
}
