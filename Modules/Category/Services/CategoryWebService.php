<?php


namespace Modules\Category\Services;


use App\DataTables\CategoriesDataTable;
use App\Services\BaseService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Modules\Category\Contracts\CategoryContract;
use Modules\Category\Http\Requests\CreateCategoryRequest;
use Modules\Category\Http\Requests\UpdateCategoryRequest;

/**
 * Class CategoryWebService
 *
 * @package Modules\Category\Services
 */
class CategoryWebService extends BaseService
{
	/**
	 * @var CategoryContract
	 */
	protected $categoryRepository;
	
	/**
	 * CategoryController constructor.
	 *
	 * @param  CategoryContract  $categoryRepo
	 */
	public function __construct(CategoryContract $categoryRepo)
	{
		$this->categoryRepository = $categoryRepo;
	}
	
	/**
	 * Display a listing of the Category.
	 *
	 * @return View|\Illuminate\Http\JsonResponse
	 */
	public function index(CategoriesDataTable $dataTable)
	{
		$this->setPageTitle(
			__('category::messages.title'),
			__('category::messages.index_subtitle'),
			__('category::messages.index_leadtext')
		);
		return $dataTable->render('category::categories.index');
	}
	
	/**
	 * Show the form for creating a new Category.
	 *
	 * @return View
	 */
	public function create(): View
	{
		$categories = $this->categoryRepository->treeList();
		
		$this->setPageTitle(
			__('category::messages.title'),
			__('category::messages.create_subtitle'),
			__('category::messages.create_leadtext')
		);
		return view('category::categories.create', compact('categories'));
	}
	
	/**
	 * Store a newly created Category in storage.
	 *
	 * @param  CreateCategoryRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateCategoryRequest $request): RedirectResponse
	{
		$category = $this->categoryRepository->createCategory($request->all());
		
		if (!$category) {
			return $this->responseRedirectBack(
				__('category::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.categories.index',
			__('category::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Show the form for editing the specified Category.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
	public function edit(int $id)
	{
		$targetCategory = $this->categoryRepository->findCategoryById($id);
		
		if ($targetCategory === null) {
			return $this->responseRedirect(
				'admin.categories.index',
				__('category::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				false,
				true
			);
		}
		
		$categories = $this->categoryRepository->treeList();
		
		$this->setPageTitle(
			__('category::messages.title'),
			__('category::messages.edit_subtitle', ['name' => $targetCategory->name]),
			__('category::messages.edit_leadtext', ['name' => $targetCategory->name])
		);
		return view('category::categories.edit', compact('categories', 'targetCategory'));
	}
	
	/**
	 * Update the specified Category in storage.
	 *
	 * @param  UpdateCategoryRequest  $request
	 * @param  int                    $id
	 *
	 * @return RedirectResponse
	 */
	public function update(UpdateCategoryRequest $request, int $id): RedirectResponse
	{
		$category = $this->categoryRepository->findCategoryById($id);
		
		if ($category === null) {
			return $this->responseRedirect(
				'admin.categories.index',
				__('category::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				false,
				true
			);
		}
		
		$category = $this->categoryRepository->updateCategory($request->all(), $id);
		if (!$category) {
			return $this->responseRedirectBack(
				__('category::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirectBack(
			__('category::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	public function show(int $id)
	{
		$targetCategory = $this->categoryRepository->findCategoryById($id);
		
		if ($targetCategory === null) {
			return $this->responseRedirect(
				'admin.categories.index',
				__('category::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				false,
				true
			);
		}
		
		$categories = $this->categoryRepository->treeList();
		
		$this->setPageTitle(
			__('category::messages.title'),
			__('category::messages.show_subtitle', ['name' => $targetCategory->name]),
			__('category::messages.show_leadtext', ['name' => $targetCategory->name])
		);
		return view('category::categories.show', compact('categories', 'targetCategory'));
	}
	
	/**
	 * Remove the specified Category from storage.
	 *
	 * @param $id
	 *
	 * @return RedirectResponse
	 */
	public function destroy(int $id): RedirectResponse
	{
		$category = $this->categoryRepository->deleteCategory($id);
		if (!$category) {
			return $this->responseRedirectBack(
				__('category::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.categories.index',
			__('category::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
