<?php


namespace Modules\Category\Services\API\V1;

use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Category\Contracts\CategoryContract;
use Modules\Category\Http\Requests\API\V1\CreateCategoryAPIRequest;
use Modules\Category\Http\Requests\API\V1\UpdateCategoryAPIRequest;
use Modules\Category\Transformers\CategoryResource;

/**
 * Class CategoryApiService
 *
 * @package Modules\Category\Services\API\V1
 */
class CategoryApiService extends BaseService
{
    /** @var  CategoryContract */
    private $categoryRepository;
    
    /**
     * CategoryAPIController constructor.
     *
     * @param  CategoryContract  $categoryRepo
     */
    public function __construct(CategoryContract $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }
    
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $categories = CategoryResource::collection($this->categoryRepository->listCategories());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('category::messages.success_retrieve'),
            $categories
        );
    }
    
    /**
     * @param  CreateCategoryAPIRequest  $request
     *
     * @return JsonResponse
     *
     */
    public function store(CreateCategoryAPIRequest $request): JsonResponse
    {
        $category = $this->categoryRepository->createCategory($request->all());
        
        if (!$category) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('category::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('category::messages.success_create'),
            $category
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     *
     */
    public function show(int $id): JsonResponse
    {
        $category = CategoryResource::make($this->categoryRepository->findCategoryById($id));
        
        if ($category === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('category::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('category::messages.success_found', ['id' => $id]),
            $category
        );
    }
    
    /**
     * @param  int                       $id
     * @param  UpdateCategoryAPIRequest  $request
     *
     * @return JsonResponse
     *
     *
     */
    public function update(int $id, UpdateCategoryAPIRequest $request): JsonResponse
    {
        $category = $this->categoryRepository->findCategoryById($id);
        
        if ($category === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('category::messages.not_found', ['id' => $id])
            );
        }
        
        $category = $this->categoryRepository->updateCategory($request->all(), $id);
        
        if (!$category) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('category::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('category::messages.success_update'),
            $category
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     *
     */
    public function destroy(int $id): JsonResponse
    {
        $category = $this->categoryRepository->findCategoryById($id);
        
        if ($category === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('category::messages.not_found', ['id' => $id])
            );
        }
        
        $category = $this->categoryRepository->deleteCategory($id);
        
        if (!$category) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('category::messages.error_delete')
            );
        }
        
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('category::messages.success_delete'),
            $category
        );
    }
}
