<?php

namespace Modules\Category\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CategoryResource
 *
 * @package Modules\Category\Transformers
 */
class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request): array
    {
        
        return [
            'isActive' => $this->active,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'fullPath' => $this->full_path,
            'imageSrc' => $this->image_src,
            'menuIconSrc' => $this->menu_icon_src,
            'detailImageSrc' => $this->detail_image_src,
            'parentId' => $this->parent_id,
            'createAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at,
        ];
    }
}
