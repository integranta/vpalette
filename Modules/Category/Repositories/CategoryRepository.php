<?php

namespace Modules\Category\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Storage;
use Modules\Category\Models\Category;
use App\Traits\UploadAble;
use Modules\Category\Contracts\CategoryContract;
use \Illuminate\Database\Eloquent\Collection;
use Str;


/**
 * Class CategoryRepository
 *
 * @package Modules\Category\Repositories
 * @version June 24, 2019, 10:25 am UTC
 */
class CategoryRepository extends BaseRepository implements CategoryContract
{
    use UploadAble;

    /**
     * CategoryRepository constructor.
     *
     * @param  Category  $model
     */
    public function __construct(Category $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection|array
     */
    public function listCategories(array $columns = ['*'], string $order = 'id', string $sort = 'desc')
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('category.cache_key').$postfix,
            config('category.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return Category::with('parent')
                    ->orderBy($order, $sort)
					->get($columns)
					->noCleaning()
					->nest()
					->listsFlattened('name');
            }
        );
    }

    /**
     * @param  array  $params
     *
     * @return Category
     */
    public function createCategory(array $params): Category
    {
        $path = null;

        $parentCategory = $this->model::find($params['parent_id']);

        if ($parentCategory !== null) {
            $path = $parentCategory->full_path.'/'.Str::of($params['name'])
                    ->trim()
                    ->lower()
                    ->slug('_', 'ru');
        }


        $sectionImageSrc = null;

        if (isset($params['section_image_src'])) {
            $sectionImageSrc = $this->makeCachedImage(
                $params['section_image_src'],
                'categories/'.$path.'/section'
            );
        }

        $menuIconSrc = null;
        if (isset($params['menu_icon_src'])) {
			$path = 'categories/'.$path.'/icon/'.Str::random(15).'.svg';
			$icon = $params['menu_icon_src'];
			$menuIconSrc = $this->uploadOne($icon, $path, 'uploads');
        }

        $dropdownImageSrc = null;
        if (isset($params['dropdown_image_src'])) {
            $dropdownImageSrc = $this->makeCachedImage(
                $params['dropdown_image_src'],
                'categories/'.$path.'/menu'
            );
        }

        $category = $this->create($this->getArrCategoryFields(
            $params,
            $sectionImageSrc,
            $menuIconSrc,
            $dropdownImageSrc
        )
        );
        $category->seo()->create($this->getArrSeoFields($params));

        $category->save();

        return $category;
    }

    /**
     * Return array filled params for work with category.
     *
     * @param  array        $params
     * @param  null|string  $sectionImageSrc
     * @param  null|string  $menuIconSrc
     * @param  null|string  $dropdownImageSrc
     *
     * @return array
     */
    private function getArrCategoryFields(
        array $params,
        ?string $sectionImageSrc,
        ?string $menuIconSrc,
        ?string $dropdownImageSrc
    ): array {
        return [
            'active' => $params['active'],
            'name' => $params['name'],
            'seo_text' => $params['seo_text'],
            'dropdown_text' => $params['dropdown_text'],
            'parent_id' => $params['parent_id'],
            'section_image_src' => $sectionImageSrc,
            'menu_icon_src' => $menuIconSrc,
            'dropdown_image_src' => $dropdownImageSrc
        ];
    }

    /**
     * Return array filled params for work with SEO.
     *
     * @param  array  $params
     *
     * @return array
     */
    private function getArrSeoFields(array $params): array
    {
        return [
            "title" => $params['title'],
            'description' => $params['description'],
            "keywords" => $params['keywords'],
            "canonical_url" => $params['canonical_url'],
            "ya_direct" => $params['ya_direct'],
            "google_tag_manager" => $params['google_tag_manager'],
            "google_analytics" => $params['google_analytics'],
            "ya_metrica" => $params['ya_metrica'],
        ];
    }

    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Category
     */
    public function updateCategory(array $params, int $id): Category
    {
        $category = $this->find($id);

        $path = null;
        $oldPath = null;

        // Если нашли категорию.
        if ($category !== null) {
            $path = 'categories/'.$category->full_path; // Текущий старый полный путь.
            $oldPath = 'categories/'.$category->full_path; // Текущий старый полный путь.
        }

        // Так как срабатывает Accessor удаляем лишнюю часть из пути до картинок и иконки.
        $sectionImageSrc = Str::of($category->section_image_src)->after('uploads/');
        $menuIconSrc = Str::of($category->menu_icon_src)->after('uploads/');
        $dropdownImageSrc = Str::of($category->dropdown_image_src)->after('uploads/');

        // Если поменялась родительская категория, переопредели полный путь.
        if ((int) $params['parent_id'] !== $category->parent_id) {
            $parentCategory = $this->find($params['parent_id']);
            $path = 'categories/'.$parentCategory->full_path.'/'.Str::of($params['name'])
                    ->lower()
                    ->trim()
                    ->slug('_', 'ru');

            $newSectionImageSrcPath = Str::of($sectionImageSrc)->afterLast($category->slug)->start(Str::of($path));
            $newMenuIconSrcPath = Str::of($menuIconSrc)->afterLast($category->slug)->start(Str::of($path));
            $newDropdownImageSrcPath = Str::of($dropdownImageSrc)->afterLast($category->slug)->start(Str::of($path));

            // Перемещаем существующие файлы в новую папку.
            Storage::disk('uploads')->move($sectionImageSrc, $newSectionImageSrcPath);
            Storage::disk('uploads')->move($menuIconSrc, $newMenuIconSrcPath);
            Storage::disk('uploads')->move($dropdownImageSrc, $newDropdownImageSrcPath);
            // Удаляем старую папку.
            Storage::disk('uploads')->deleteDirectory($oldPath);

            $sectionImageSrc = $newSectionImageSrcPath;
            $menuIconSrc = $newMenuIconSrcPath;
            $dropdownImageSrc = $newDropdownImageSrcPath;
        }

        if (isset($params['section_image_src'])) {
            $sectionImageSrc = $this->makeCachedImage($params['section_image_src'], $path.'/section');
        }

        if (isset($params['menu_icon_src'])) {
            $menuIconSrc = $this->uploadOne($params['menu_icon_src'], $path.'/icon/', 'uploads');
        }

        if (isset($params['dropdown_image_src'])) {
            $dropdownImageSrc = $this->makeCachedImage($params['dropdown_image_src'], $path.'/menu');
        }


        $this->update($this->getArrCategoryFields(
            $params,
            $sectionImageSrc,
            $menuIconSrc,
            $dropdownImageSrc
        ), $id);

        $category = $this->find($id);
        $category->seo()->update($this->getArrSeoFields($params));

        return $category;
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteCategory(int $id): bool
    {
        return $this->delete($id);
    }

    /**
     * @param  int  $id
     *
     * @return Category
     */
    public function findCategoryById(int $id): ?Category
    {
        return $this->find($id);
    }

    /**
     *
     * @return array
     */
    public function treeList(): array
    {
        return Cache::remember(
            config('category.cache_key_tree'),
            config('category.cache_ttl_tree'),
            function () {
                return Category::orderByRaw('-name ASC')
                    ->get()
                    ->nest()
                    ->setIndent('|–– ')
                    ->listsFlattened('name');
            });
    }

    /**
     * @param  array|string  $relations
     * @param  string        $slug
     *
     * @return Builder|Model|object
     */
    public function findBySlug(array $relations, string $slug): Model
    {
        return Category::with($relations)
            ->where('slug', $slug)
            ->first();
    }
}
