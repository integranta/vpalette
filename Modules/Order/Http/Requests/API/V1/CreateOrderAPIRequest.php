<?php

namespace Modules\Order\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateOrderAPIRequest
 *
 * @package Modules\Order\Http\Requests\API\V1
 */
class CreateOrderAPIRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'post_code' => 'required|integer|min:6',
            'phone_number' => 'required|integer|min:12',
            'comments' => 'nullable|string|max:500'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
