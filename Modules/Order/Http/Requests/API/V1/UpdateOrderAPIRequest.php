<?php

namespace Modules\Order\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateOrderAPIRequest
 *
 * @package Modules\Order\Http\Requests\API\V1
 */
class UpdateOrderAPIRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'orderFields.first_name' => 'required|string',
            'orderFields.last_name' => 'required|string',
            'orderFields.address' => 'required|string',
            'orderFields.city' => 'required|string',
            'orderFields.country' => 'required|string',
            'orderFields.post_code' => 'required|integer|min:6',
            'orderFields.phone_number' => 'required|integer|min:12',
            'orderFields.comments' => 'nullable|string|max:500'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
