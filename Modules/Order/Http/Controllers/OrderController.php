<?php

namespace Modules\Order\Http\Controllers;

use App\DataTables\OrdersDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Order\Http\Requests\CreateOrderRequest;
use Modules\Order\Http\Requests\UpdateOrderRequest;
use Modules\Order\Services\OrderWebService;

/**
 * Class OrderController
 *
 * @package Modules\Order\Http\Controllers
 */
class OrderController extends Controller
{
    /** @var OrderWebService */
    private $orderWebService;

    /**
     * OrderController constructor.
     *
     * @param  OrderWebService  $service
     */
    public function __construct(OrderWebService $service)
    {
        $this->orderWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\OrdersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(OrdersDataTable $dataTable)
    {
        return $this->orderWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->orderWebService->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateOrderRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateOrderRequest $request): RedirectResponse
    {
        return $this->orderWebService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  string  $orderNumber
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(string $orderNumber): View
    {
        return $this->orderWebService->show($orderNumber);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $orderNumber
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(string $orderNumber): View
    {
        return $this->orderWebService->edit($orderNumber);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateOrderRequest  $request
     * @param  string              $orderNumber
     *
     * @return RedirectResponse
     */
    public function update(UpdateOrderRequest $request, string $orderNumber): RedirectResponse
    {
        return $this->orderWebService->update($request,
            $orderNumber);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $orderNumber
     *
     * @return RedirectResponse
     */
    public function destroy(string $orderNumber): RedirectResponse
    {
        return $this->orderWebService->destroy($orderNumber);
    }
}
