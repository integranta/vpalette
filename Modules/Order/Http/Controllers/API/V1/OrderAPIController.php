<?php

namespace Modules\Order\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Order\Http\Requests\API\V1\CreateOrderAPIRequest;
use Modules\Order\Http\Requests\API\V1\UpdateOrderAPIRequest;
use Modules\Order\Services\API\V1\OrderApiService;

/**
 * Class OrderAPIController
 *
 * @package Modules\Order\Http\Controllers\API\V1
 */
class OrderAPIController extends Controller
{
    /** @var OrderApiService */
    private $orderApiService;

    /**
     * OrderAPIController constructor.
     *
     * @param  OrderApiService  $service
     */
    public function __construct(OrderApiService $service)
    {
        $this->orderApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
        //$this->middleware('auth:api')->except('show');
    }


    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->orderApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateOrderAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateOrderAPIRequest $request): JsonResponse
    {
        return $this->orderApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  string  $orderNumber
     *
     * @return JsonResponse
     */
    public function show(string $orderNumber): JsonResponse
    {
        return $this->orderApiService->show($orderNumber);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateOrderAPIRequest  $request
     * @param  string                 $orderNumber
     *
     * @return JsonResponse
     */
    public function update(UpdateOrderAPIRequest $request, string $orderNumber): JsonResponse
    {
        return $this->orderApiService->update($request, $orderNumber);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $orderNumber
     *
     * @return JsonResponse
     */
    public function destroy(string $orderNumber): JsonResponse
    {
        return $this->orderApiService->destroy($orderNumber);
    }
}
