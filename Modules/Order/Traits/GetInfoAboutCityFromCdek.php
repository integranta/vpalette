<?php


namespace Modules\Order\Traits;

use CdekSDK\CdekClient;
use CdekSDK\Requests;
use CdekSDK\Common\Location;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

/**
 * Возвращает информацию о городе от СДЭК
 *
 * Trait GetInfoAboutCityFromCdek
 *
 * @package Modules\Order\Traits
 */
trait GetInfoAboutCityFromCdek
{
	private $cdekClient;
	
	/**
	 * GetInfoAboutCityFromCdek constructor.
	 *
	 */
	public function __construct()
	{
		$this->setup();
	}
	
	
	/**
	 * @param  string  $city
	 *
	 * @return array|null
	 */
	public function getInfoAboutCityFromCdekAutoComplete(string $city): ?array
	{
		$http = Http::get('https://api.cdek.ru/city/getListByTerm/jsonp.php?q=' . urlencode($city));
		
		if ($http->successful()) {
			return $http->json();
		}
		
		return null;
	}
	
	/**
	 * Возвращает полную информацию о городе отправителя от СДЭК.
	 *
	 * @param  int  $cdekCodeCity
	 *
	 * @return \CdekSDK\Common\Location|null
	 */
	public function getSenderAddressFromCdek(int $cdekCodeCity): ?Location
	{
		$addressSender = null;
		
		$request = new Requests\CitiesRequest();
		$request
			->setPage(0)
			->setSize(1)
			->setCountryCode('ru')
			->setCityCode($cdekCodeCity);
		//dd(__METHOD__, $this->cdekClient);
		
		$response = $this->cdekClient->sendCitiesRequest($request);
		
		if ($response->hasErrors()) {
			return null;
		}
		
		foreach ($response as $location) {
			$addressSender = $location;
		}
		
		return $addressSender;
	}
	
	/**
	 * Подготовляет Http-клиент для работы со SDK
	 */
	private function setup(): void
	{
		if (config('cdek.test_mode_enable')) {
			$this->cdekClient = new CdekClient(
				config('cdek.test_mode.account'),
				config('cdek.test_mode.password'),
				new Client([
					'base_uri' => config('cdek.test_mode.base_uri'),
					'timeout'  => config('cdek.test_mode.timeout')
				])
			);
		}
		
		$this->cdekClient = new CdekClient(
			config('cdek.prod_mode.account'),
			config('cdek.prod_mode.password'),
			new Client([
				'base_uri' => config('cdek.prod_mode.base_uri'),
				'timeout'  => config('cdek.prod_mode.timeout')
			])
		);
	}
}