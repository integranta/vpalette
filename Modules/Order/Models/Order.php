<?php

namespace Modules\Order\Models;

use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Order
 *
 * @package Modules\Order\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $orderNumber
 * @property int $userId
 * @property string $status
 * @property float $grandTotal
 * @property int $itemCount
 * @property string $paymentStatus
 * @property string|null $paymentMethod
 * @property string $firstName
 * @property string $lastName
 * @property string $address
 * @property string $city
 * @property string $country
 * @property string $postCode
 * @property string $phoneNumber
 * @property string|null $comments
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Order\Models\OrderItem[] $items
 * @property-read int|null $itemsCount
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereGrandTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereItemCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereOrderNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order wherePaymentStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order wherePostCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\Order whereUserId($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'order_number',
        'user_id',
        'status',
        'grand_total',
        'item_count',
        'payment_status',
        'payment_method',
        'first_name',
        'last_name',
        'address',
        'city',
        'country',
        'post_code',
        'phone_number',
        'comments'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'string',
        'payment_status' => 'string',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'status' => OrderStatus::class,
        'payment_status' => PaymentStatus::class
    ];

    /**
     * Возвращает список значений статусов заказа из enum класса для select тега.
     *
     * @return array
     */
    public static function getOrderStatus(): array
    {
        return OrderStatus::toSelectArray();
    }

    /**
     * Возвращает список значений статусов платежей из enum класса для select тега.
     *
     * @return array
     */
    public static function getPaymentStatus(): array
    {
        return PaymentStatus::toSelectArray();
    }

    /**
     * Получить пользователя, кто заказывал.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получить содержимое заказа.
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }
}
