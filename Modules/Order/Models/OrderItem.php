<?php

namespace Modules\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\PartnerOffer\Models\PartnerOffer;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class OrderItem
 *
 * @package Modules\Order\Models
 * @property int $id
 * @property int $orderId
 * @property int $offerId
 * @property int $quantity
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Modules\PartnerOffer\Models\PartnerOffer $offer
 * @property-read \Modules\Order\Models\Order $order
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Order\Models\OrderItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderItem extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'offer_id',
        'quantity',
        'price'
    ];

    /**
     * Получить партнёрское предложение, который принадлежит этот элемент заказа.
     *
     * @return BelongsTo
     */
    public function offer(): BelongsTo
    {
        return $this->belongsTo(PartnerOffer::class);
    }

    /**
     * Получить заказ, который принадлежит этот элемент заказа.
     *
     * @return BelongsTo
     */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
