<?php


namespace Modules\Order\Repositories;


use App\Enums\DeliveriesTariffsCdek;
use App\Enums\DeliveryServices;
use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use App\Repositories\BaseRepository;
use Cart;
use CdekSDK\CdekClient;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use CdekSDK\Common;
use CdekSDK\Requests;
use Modules\Order\Contracts\OrderContract;
use Modules\Order\Models\Order;
use Modules\Order\Models\OrderItem;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Class OrderRepository
 *
 * @package Modules\Order\Repositories
 */
class OrderRepository extends BaseRepository implements OrderContract
{
	/** @var \CdekSDK\CdekClient */
	private $cdekClient;
	
    /**
     * OrderRepository constructor.
     *
     * @param  Order  $model
     */
    public function __construct(Order $model)
    {
        parent::__construct($model);
        $this->model = $model;
        $this->cdekClient = new CdekClient(
			config('cdek.test_mode.account'),
			config('cdek.test_mode.password')
		);
	}
    
    /**
     * @inheritDoc
     */
    public function listOrders(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        return $this->all($columns,
            $order,
            $sort);
    }
    
    /**
     * @inheritDoc
     */
    public function findOrderByNumber(string $orderNumber): ?Order
    {
        return $this->findOneBy(['order_number' => $orderNumber]);
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function storeOrderDetails(array $params): Order
    {
        $cart = $this->getCart($params);
        
        $orderData = [
            'order_number' => strtoupper(uniqid('ORD-',
                false)),
            'user_id' => $params['user_id'],
            'status' => OrderStatus::PENDING,
            'grand_total' => $cart->getSubTotal(),
            'item_count' => $cart->getTotalQuantity(),
            'payment_status' => PaymentStatus::NOT_PAID,
            'payment_method' => null,
            'first_name' => $params['first_name'],
            'last_name' => $params['last_name'],
            'address' => $params['address'],
            'city' => $params['city'],
            'country' => $params['country'],
            'post_code' => $params['post_code'],
            'phone_number' => $params['phone_number'],
            'comments' => $params['comments']
        ];
        
        $partnerShop = PartnerShop::whereId(1);
        
        // TODO: Вынести в отдельные классы-сервисы под разные службы доставок
		if (DeliveryServices::CDEK === $params['delivery_service'])
		{
			$orderCDEK = new Common\Order([
				'Number' => $orderData['order_number'],
				'SendCityPostCode' => $partnerShop->postIndex,
				'SendCityCode' => 44, // TODO: Нужно подставлять код города/индекс магазина-партнёра у которого покупается товар
				'RecCityPostCode' => '630001', // TODO: Заменить на полученные с frontend
				'RecipientName' => "${orderData['first_name']} ${orderData['last_name']}",
				'RecipientEmail' => $orderData['email'],
				'Phone' => $orderData['phone_number'],
				'TariffTypeCode' => DeliveriesTariffsCdek::EXPRESS_LITE_DOOR_DOOR,
				'RecipientCompany' => $orderData['company'],
				'Comment' => $orderData['comments']
			]);
			
			$orderCDEK->setSender(Common\Sender::create([
				'Company' => $partnerShop->shopName,
				'Name' => $partnerShop->partner->user->full_name,
				'Phone' => $partnerShop->partner->phone
			])->setAddress(Common\Address::create([
				'Street' => $partnerShop->street,
				'House' => $partnerShop->house,
			])));
			
			$package = Common\Package::create([
				'Number' => $orderData['order_number'],
				'Barcode' => $orderData['order_number']
			]);
			
			$items = $cart->getContent();
			
			foreach ($items as $item) {
				$offer = PartnerOffer::where('id', $item->attributes->offer_id)->first();
				if ($offer) {
					$package->addItem(Common\Item::create([
						'WareKey' => $offer->product->article,
						'Cost' => $offer->price,
						'Payment' => 0, // Оплата за товар при получение (за единицу товара)
						'Weight' => $offer->product->weight,
						'Amount' => $item->quantity,
						'Comment' => $offer->product->name
					]));
				}
			}
			
			$orderCDEK->addPackage($package);
			$cdekDeliveryRequest = new Requests\DeliveryRequest([
				'Number' => $orderData['order_number']
			]);
			$cdekDeliveryRequest->addOrder($orderCDEK);
			
			$cdekDeliveryResponse = $this->cdekClient->sendDeliveryRequest($cdekDeliveryRequest);
			
			if ($cdekDeliveryResponse->hasErrors()) {
				// обработка ошибок
				
				foreach ($cdekDeliveryResponse->getErrors() as $order) {
					// заказы с ошибками
					$order->getMessage();
					$order->getErrorCode();
					$order->getNumber();
				}
				
				foreach ($cdekDeliveryResponse->getMessages() as $message) {
					// Сообщения об ошибках
					\Log::debug($message->getMessage());
				}
			}
			
			foreach ($cdekDeliveryResponse->getOrders() as $order) {
				// сверяем данные заказа, записываем номер
				$order->getNumber(); // Номер заказа Интернет-магазина
				$order->getDispatchNumber(); // Номер заказа СДЭК
				break;
			}
		}
        
        $order = $this->create($orderData);
        
        if ($order) {
            $items = $cart->getContent();
            
            foreach ($items as $item) {
                $offer = PartnerOffer::where('id',
                    $item->attributes->offer_id)->first();
                
                if ($offer) {
                    $orderItem = new OrderItem([
                        'offer_id' => $offer->id,
                        'quantity' => $item->quantity,
                        'price' => $item->getPriceSum()
                    ]);
                    
                    $order->items()->save($orderItem);
                }
            }
        }
        
        return $order;
    }
    
    /**
     * @inheritDoc
     */
    public function deleteOrder(string $orderNumber): bool
    {
        return $this->model::where(['order_number' => $orderNumber])->delete();
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function updateOrder(array $params, string $orderNumber): bool
    {
        $orderId = $this->findOneBy(['order_number' => $orderNumber])->id;
        
        $orderData = [
            'order_number' => $params['orderFields']['order_number'],
            'user_id' => $params['orderFields']['user_id'],
            'status' => $params['orderFields']['status'],
            'grand_total' => $params['orderFields']['grand_total'],
            'item_count' => $params['orderFields']['item_count'],
            'payment_status' => $params['orderFields']['payment_status'],
            'payment_method' => null,
            'first_name' => $params['orderFields']['first_name'],
            'last_name' => $params['orderFields']['last_name'],
            'address' => $params['orderFields']['address'],
            'city' => $params['orderFields']['city'],
            'country' => $params['orderFields']['country'],
            'post_code' => $params['orderFields']['post_code'],
            'phone_number' => $params['orderFields']['phone_number'],
            'comments' => $params['orderFields']['comments']
        ];
        $order = $this->update($orderData, $orderId);
        
        if ($order) {
            foreach ($params['orderItems'] as $item) {
                $offer = OrderItem::where('id', $item['id'])->first();
                
                if ($offer) {
                    $orderItem = [
                        'order_id' => $offer['order_id'],
                        'offer_id' => $offer['offer_id'],
                        'quantity' => $item['quantity'],
                        'price' => $item['price']
                    ];
                    $offer->update($orderItem);
                }
            }
        }
        
        return $order;
    }
    
    /**
     * @param  array  $params
     *
     * @return \Darryldecode\Cart\Cart|bool
     * @throws Exception
     */
    private function getCart(array $params): \Darryldecode\Cart\Cart
    {
        return Cart::session($params['user_id']);
    }
}
