<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="params" role="tabpanel" aria-labelledby="params-tab">
        <!-- Activity End at Field -->
        <div class="form-group row col-md-12">
            <label for="created_at" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                Создан:
            </label>
            <div class="col-sm-12 col-md-7">
                {{ $order->created_at }}
            </div>
        </div>

        <!-- Activity End at Field -->
        <div class="form-group row col-md-12">
            <label for="updated_at" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                Последнее изменение:
            </label>
            <div class="col-sm-12 col-md-7">
                {{ $order->updated_at }}
            </div>
        </div>

        <div class="form-group row col-md-12">
            <label for="status" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                Статус заказа:
            </label>

            <div class="col-sm-12 col-md-7">
                <select
                    class="form-control custom-select mt-15 @error('status') is-invalid @enderror"
                    name="status"
                    id="status"
                >
                    <option disabled>Выберите статус заказа</option>
                    @foreach($orderStatuses as $key => $status)
                        @if ($order->status === $key)
                            <option value="{{ $key }}" selected>{{ $status }}</option>
                        @else
                            <option value="{{ $key }}">{{ $status }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="tab-pane fade show" id="buyer" role="tabpanel" aria-labelledby="buyer-tab">
        <!-- Username Field -->
        <div class="form-group row col-md-12">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                for="user_id"
            >
                Покупатель:
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="user_id"
                    class="form-control"
                    name="user_id"
                >
                    <option value="0">Выберите пользователя</option>
                    @foreach($users as $user)
                        @if ($targetUser->user->id === $user->id)
                            <option value="{{ $user->id }}" selected> {{ $user->name }} </option>
                        @else
                            <option value="{{ $user->id }}"> {{ $user->name }} </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <!-- Comment Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'comments',
                    'Комментарии к заказу:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea(
                        'comments',
                        $order->comments,
                        ['class' => 'form-control h-100', 'id' => 'comments']
                    )
                !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade show" id="shipping" role="tabpanel" aria-labelledby="shipping-tab"></div>
    <div class="tab-pane fade show" id="payinfo" role="tabpanel" aria-labelledby="payinfo-tab">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>К оплате</h4>
                        </div>
                        <div class="card-body">
                            <h5>
                                {{ $order->grand_total }}{{ config('settings.currency_symbol') }}
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade show" id="pay" role="tabpanel" aria-labelledby="pay-tab">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Cпособ оплаты</h4>
                        </div>
                        <div class="card-body">
                            <p>{{ $order->payment_method }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Cумма к оплате</h4>
                        </div>
                        <div class="card-body">
                            <p>{{ $order->grand_total }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-4">
                    <div class="card card-primary">
                        <div class="card-header">
                            <label for="payment_status">Статус</label>
                        </div>
                        <div class="card-body">
                            <select name="payment_status" id="payment_status" class="form-control">
                                @foreach($paymentStatuses as $key => $pay_status)
                                    @if ($key === $order->payment_status)
                                        <option value="{{ $key }}" selected>{{ $pay_status }}</option>
                                    @else
                                        <option value="{{ $key }}">{{ $pay_status }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade show" id="orderitems" role="tabpanel" aria-labelledby="orderitems-tab">
        <table class="table">
            <tr>
                <td>Изображение</td>
                <td>Название</td>
                <td>Количество</td>
                <td>Остаток</td>
                <td>Цена</td>
                <td>Сумма</td>
            </tr>
            <tbody>
            @foreach($order->items as $item)
                <tr>
                    <td>
                        <img
                            src="{{ $item->offer->product->images->first()->full }}"
                            alt="{{ $item->offer->product->name }}"
                            width="70" height="70"
                            style="object-fit: cover;"
                            class="my-2"
                        >
                    </td>
                    <td>{{ $item->offer->product->name }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ $item->offer->amount }}</td>
                    <td>{{ $item->offer->price }}{{ config('settings.currency_symbol') }}</td>
                    <td>{{ $item->price }}{{ config('settings.currency_symbol') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
