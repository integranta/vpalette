<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Username Field -->
        <div class="form-group row col-md-12">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                for="user_id"
            >
                Покупатель:
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="user_id"
                    class="form-control"
                    name="user_id"
                >
                    <option value="0">Выберите пользователя</option>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}"> {{ $user->name }} </option>
                    @endforeach
                </select>
            </div>
        </div>
        <!-- End Username Field -->

        <!-- Product_id Field -->
        <div class="form-group row col-md-12 mb-4">
            <label
                class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
                for="product_id">Предложение <span class="m-l-5 text-danger"> *</span>
            </label>
            <div class="col-sm-12 col-md-7">
                <select
                    id="product_id"
                    class="form-control"
                    name="product_id"
                >
                    <option value="0">Выберите предложение</option>
                    @foreach($offers as $offer)
                        <option value="{{ $offer->id }}">
                            {{ $offer->product->name }} - {{ $offer->price }}{{ config('settings.currency_symbol') }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <!-- End Product_Id Field -->


        <!-- Anonymous field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Оставить анонимным:</span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('make_anonymous', 0) !!}
                {!!
                    Form::checkbox(
                        'make_anonymous',
                        true,
                        false,
                        ['class' => 'custom-control-input', 'id' => 'make_anonymous']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="make_anonymous"></label>
            </div>
        </div>
        <!-- End Anonymous Field -->
    </div>
    <div class="tab-pane fade show" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <!-- Virtues Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'virtues',
                    'Преимущества:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea
                    (
                        'virtues',
                        null,
                        ['class' => 'form-control', 'id' => 'virtues']
                    )
                !!}
            </div>
        </div>


        <!-- Limitations Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'limitations',
                    'Недостатки:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea
                    (
                        'limitations',
                        null,
                        ['class' => 'form-control', 'id' => 'limitations']
                    )
                !!}
            </div>
        </div>


        <!-- Comments Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'comments',
                    'Комментарий:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea
                    (
                        'comments',
                        null,
                        ['class' => 'form-control', 'id' => 'comments']
                    )
                !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
        <!-- Images Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label
                (
                    'photos',
                    'Фотографии:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::file
                    (
                        'photos[]',
                        ['class' => 'form-control', 'id' => 'photos', 'multiple']
                    )
                !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade show" id="detail" role="tabpanel" aria-labelledby="detail-tab">

    </div>
</div>
