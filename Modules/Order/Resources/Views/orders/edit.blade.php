@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('libs/css/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/order.css') }}">
@endsection

@section('content')
    <order-edit
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        order-number="{{ $order->order_number }}"
    ></order-edit>
@endsection

@push('scripts')
    {{-- JS Libraies --}}
    <script src="{{ asset('libs/js/select2/dist/js/select2.full.min.js') }}"></script>
    {{--  Module JS File  --}}
    <script src="{{ asset('js/order.js') }}"></script>
@endpush
