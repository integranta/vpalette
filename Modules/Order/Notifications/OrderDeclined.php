<?php

namespace Modules\Order\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Modules\Order\Models\Order;

/**
 * Class OrderDeclined
 *
 * @package Modules\Order\Notifications
 */
class OrderDeclined extends Notification
{
    use Queueable;
    
    /** @var Order */
    private $order;
    
    /**
     * Create a new notification instance.
     *
     * @param  Order  $order
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail', 'database', 'broadcast'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject("Заказ {$this->order->order_number} был отменён")
            ->greeting('Привет !')
            ->line("Хотим сообщить Вам что заказ {$this->order->order_number} был отменён")
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'message' => "Заказ {$this->order->order_number} был отменён",
            'typeNotify' => 'order_declined',
            'url' => route('admin.orders.show', ['order' => $this->order->id])
        ];
    }
    
    /**
     * @param $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'message' => "Заказ {$this->order->order_number} был отменён",
            'typeNotify' => 'order_declined',
            'url' => route('admin.orders.show', ['order' => $this->order->id])
        ]);
    }
    
    /**
     * @param $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable): array
    {
        return [
            'message' => "Заказ {$this->order->order_number} был отменён",
            'typeNotify' => 'order_declined',
            'url' => route('admin.orders.show', ['order' => $this->order->id])
        ];
    }
}
