<?php

namespace Modules\Order\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Order\Models\Order;
use Modules\Order\Notifications\OrderDeclined;

/**
 * Class NotifyOrderDeclined
 *
 * @package Modules\Order\Jobs
 */
class NotifyOrderDeclined implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /** @var User */
    public $user;
    
    /** @var Order */
    public $order;
    
    /**
     * Create a new job instance.
     *
     * @param  User   $user
     * @param  Order  $order
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->user->notify(new OrderDeclined($this->order));
    }
}
