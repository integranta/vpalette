<?php

namespace Modules\Order\Observers;

use App\Enums\OrderStatus;
use App\User;
use Modules\Order\Jobs\NotifyOrderCompleted;
use Modules\Order\Jobs\NotifyOrderCreated;
use Modules\Order\Jobs\NotifyOrderDeclined;
use Modules\Order\Jobs\NotifyOrderDelivered;
use Modules\Order\Jobs\NotifyOrderProcessing;
use Modules\Order\Models\Order;

/**
 * Class OrderObserver
 *
 * @package Modules\Order\Observers
 */
class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param  Order  $order
     *
     * @return void
     */
    public function created(Order $order): void
    {
        $customer = User::find($order->user_id);
        NotifyOrderCreated::dispatch($customer, $order);
    }
    
    /**
     * Handle the order "updated" event.
     *
     * @param  Order  $order
     *
     * @return void
     */
    public function updated(Order $order): void
    {
        $customer = User::find($order->user_id);
        
        /** @var string $orderStatus */
        $orderStatus = OrderStatus::fromValue($order->status)->value;
        
        if ($orderStatus === OrderStatus::PROCESSING) {
            NotifyOrderProcessing::dispatch($customer, $order);
        }
        
        if ($orderStatus === OrderStatus::DELIVERED) {
            NotifyOrderDelivered::dispatch($customer, $order);
        }
        
        if ($orderStatus === OrderStatus::COMPLETED) {
            NotifyOrderCompleted::dispatch($customer, $order);
        }
        
        if ($orderStatus === OrderStatus::DECLINE) {
            NotifyOrderDeclined::dispatch($customer, $order);
        }
    }
    
    /**
     * Handle the order "deleted" event.
     *
     * @param  Order  $order
     *
     * @return void
     */
    public function deleted(Order $order): void
    {
        //
    }
    
    /**
     * Handle the order "restored" event.
     *
     * @param  Order  $order
     *
     * @return void
     */
    public function restored(Order $order): void
    {
        //
    }
    
    /**
     * Handle the order "force deleted" event.
     *
     * @param  Order  $order
     *
     * @return void
     */
    public function forceDeleted(Order $order): void
    {
        //
    }
}
