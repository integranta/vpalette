<?php

namespace Modules\Order\Observers;

use Modules\Order\Jobs\NotifyOrderCreated;
use Modules\Order\Models\OrderItem;

/**
 * Class OrderItemObserver
 *
 * @package Modules\Order\Observers
 */
class OrderItemObserver
{
    /**
     * Handle the order item "created" event.
     *
     * @param  OrderItem  $orderItem
     *
     * @return void
     */
    public function created(OrderItem $orderItem): void
    {
        $orderItem->load(['offer.partnerShop.partner.user', 'order']);
        $order = $orderItem->order;
        $partner = $orderItem->offer->partnerShop->partner->user;
        NotifyOrderCreated::dispatch($partner, $order);
        $this->updateAmountOffer($orderItem);
    }
    
    /**
     * @param  OrderItem  $orderItem
     */
    private function updateAmountOffer(OrderItem $orderItem): void
    {
        $orderItem->load(['offer', 'order']);
        $amount = $orderItem->offer->amount - $orderItem->quantity;
        $orderItem->offer->update([
            'amount' => $amount
        ]);
    }
    
    /**
     * Handle the order item "updated" event.
     *
     * @param  OrderItem  $orderItem
     *
     * @return void
     */
    public function updated(OrderItem $orderItem): void
    {
        $orderItem->load(['offer.partnerShop.partner.user', 'order']);
        $this->updateAmountOffer($orderItem);
    }
    
    /**
     * Handle the order item "deleted" event.
     *
     * @param  OrderItem  $orderItem
     *
     * @return void
     */
    public function deleted(OrderItem $orderItem): void
    {
        //
    }
    
    /**
     * Handle the order item "restored" event.
     *
     * @param  OrderItem  $orderItem
     *
     * @return void
     */
    public function restored(OrderItem $orderItem): void
    {
        //
    }
    
    /**
     * Handle the order item "force deleted" event.
     *
     * @param  OrderItem  $orderItem
     *
     * @return void
     */
    public function forceDeleted(OrderItem $orderItem): void
    {
        //
    }
}
