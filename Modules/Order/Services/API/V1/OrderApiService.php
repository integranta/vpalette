<?php


namespace Modules\Order\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Order\Contracts\OrderContract;
use Modules\Order\Http\Requests\API\V1\CreateOrderAPIRequest;
use Modules\Order\Http\Requests\API\V1\UpdateOrderAPIRequest;
use Modules\Order\Models\Order;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\User\Repositories\UserRepository;

/**
 * Class OrderApiService
 *
 * @package Modules\Order\Services\API\V1
 */
class OrderApiService extends BaseService
{
	/** @var OrderContract */
	private $orderRepository;
	
	/** @var UserRepository */
	private $userRepository;
	
	/** @var PartnerOfferContract */
	private $offersRepository;
	
	/**
	 * OrderAPIController constructor.
	 *
	 * @param  OrderContract         $orderRepository
	 * @param  UserRepository        $userRepo
	 * @param  PartnerOfferContract  $offerRepo
	 */
	public function __construct(
		OrderContract $orderRepository,
		UserRepository $userRepo,
		PartnerOfferContract $offerRepo
	) {
		$this->orderRepository = $orderRepository;
		$this->userRepository = $userRepo;
		$this->offersRepository = $offerRepo;
	}
	
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
	public function index(): JsonResponse
	{
		$orders = $this->orderRepository->listOrders();
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('order::messages.success_retrieve'),
			$orders
		);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  CreateOrderAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function store(CreateOrderAPIRequest $request): JsonResponse
	{
		$order = $this->orderRepository->storeOrderDetails($request->all());
		
		if (!$order) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('order::messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('order::messages.success_create'),
			$order
		);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  string  $orderNumber
	 *
	 * @return JsonResponse
	 */
	public function show(string $orderNumber): JsonResponse
	{
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		$users = $this->userRepository->listUsers();
		
		
		if (!$order) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('order::messages.not_found', ['number' => $orderNumber])
			);
		}
		
		$targetUser = $order->user;
		$offers = $this->offersRepository->listOffers();
		$orderItems = $order->loadMissing(['items.offer.product.images'])->items;
		$orderStatuses = Order::getOrderStatus();
		$paymentStatuses = Order::getPaymentStatus();
		
		return $this->responseJson(
			false,
			Response::HTTP_FOUND,
			__('order::messages.success_found', ['number' => $orderNumber]),
			compact('order',
				'orderItems',
				'users',
				'targetUser',
				'orderStatuses',
				'paymentStatuses',
				'offers')
		);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  UpdateOrderAPIRequest  $request
	 * @param  string                 $orderNumber
	 *
	 * @return JsonResponse
	 */
	public function update(UpdateOrderAPIRequest $request, string $orderNumber): JsonResponse
	{
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		
		if (!$order) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('order::messages.not_found', ['number' => $orderNumber])
			);
		}
		
		$order = $this->orderRepository->updateOrder($request->all(), $orderNumber);
		
		if (!$order) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('order::messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('order::messages.success_update'),
			$order
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  string  $orderNumber
	 *
	 * @return JsonResponse
	 */
	public function destroy(string $orderNumber): JsonResponse
	{
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		
		if (!$order) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('order::messages.not_found', ['number' => $orderNumber])
			);
		}
		
		$order = $this->orderRepository->deleteOrder($orderNumber);
		
		if (!$order) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('order::messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('order::messages.success_delete'),
			$order
		);
	}
}
