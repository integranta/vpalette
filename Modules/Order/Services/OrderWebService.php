<?php


namespace Modules\Order\Services;


use App\DataTables\OrdersDataTable;
use App\Services\BaseService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Order\Contracts\OrderContract;
use Modules\Order\Http\Requests\CreateOrderRequest;
use Modules\Order\Http\Requests\UpdateOrderRequest;
use Modules\Order\Models\Order;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\User\Repositories\UserRepository;

/**
 * Class OrderWebService
 *
 * @package Modules\Order\Services
 */
class OrderWebService extends BaseService
{
	/** @var OrderContract */
	private $orderRepository;
	
	/** @var UserRepository */
	private $userRepository;
	
	/** @var PartnerOfferContract */
	private $offersReporitory;
	
	/**
	 * OrderController constructor.
	 *
	 * @param  OrderContract         $orderRepo
	 * @param  UserRepository        $userRepo
	 * @param  PartnerOfferContract  $offerRepo
	 */
	public function __construct(OrderContract $orderRepo, UserRepository $userRepo, PartnerOfferContract $offerRepo)
	{
		$this->orderRepository = $orderRepo;
		$this->userRepository = $userRepo;
		$this->offersReporitory = $offerRepo;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\OrdersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(OrdersDataTable $dataTable)
	{
		$this->setPageTitle(
			__('order::messages.title'),
			__('order::messages.index_subtitle'),
			__('order::messages.index_leadtext')
		);
		
		return $dataTable->render('order::orders.index');
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		
		$users = $this->userRepository->listUsers();
		$offers = $this->offersReporitory->listOffers();
		
		$this->setPageTitle(
			__('order::messages.title'),
			__('order::messages.create_subtitle'),
			__('order::messages.create_leadtext')
		);
		
		return view('order::orders.create', compact('users', 'offers'));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  CreateOrderRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateOrderRequest $request): RedirectResponse
	{
		$order = $this->orderRepository->storeOrderDetails($request->all());
		
		if (!$order) {
			return $this->responseRedirectBack(
				__('order::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.orders.index',
			__('order::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  string  $orderNumber
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(string $orderNumber): View
	{
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		$users = $this->userRepository->listUsers();
		
		
		if (!$order) {
			return $this->responseRedirect(
				'admin.orders.index',
				__('order::messages.not_found', ['number' => $orderNumber]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$targetUser = $order->load('user');
		$orderStatuses = Order::getOrderStatus();
		$paymentStatuses = Order::getPaymentStatus();
		
		$this->setPageTitle(
			__('order::messages.title'),
			__('order::messages.show_subtitle', ['number' => $orderNumber]),
			__('order::messages.show_leadtext', ['number' => $orderNumber])
		);
		
		return view('order::orders.show',
			compact('order', 'users', 'targetUser', 'orderStatuses', 'paymentStatuses'));
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string  $orderNumber
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(string $orderNumber): View
	{
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		$users = $this->userRepository->listUsers();
		
		if (!$order) {
			return $this->responseRedirect(
				'admin.orders.index',
				__('order::messages.not_found', ['number' => $orderNumber]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$targetUser = $order->load('user');
		$orderStatuses = Order::getOrderStatus();
		$paymentStatuses = Order::getPaymentStatus();
		
		$this->setPageTitle(
			__('order::messages.title'),
			__('order::messages.edit_subtitle', ['number' => $orderNumber]),
			__('order::messages.edit_leadtext', ['number' => $orderNumber])
		);
		
		return view('order::orders.edit',
			compact('order', 'users', 'targetUser', 'orderStatuses', 'paymentStatuses'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  UpdateOrderRequest  $request
	 * @param  string              $orderNumber
	 *
	 * @return RedirectResponse
	 */
	public function update(UpdateOrderRequest $request, string $orderNumber): RedirectResponse
	{
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		
		if (!$order) {
			return $this->responseRedirect(
				'admin.orders.index',
				__('order::messages.not_found', ['number' => $orderNumber]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$order = $this->orderRepository->updateOrder($request->all(),
			$orderNumber);
		
		if (!$order) {
			return $this->responseRedirectBack(
				__('order::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.orders.index',
			__('order::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  string  $orderNumber
	 *
	 * @return RedirectResponse
	 */
	public function destroy(string $orderNumber): RedirectResponse
	{
		$order = $this->orderRepository->findOrderByNumber($orderNumber);
		
		if (!$order) {
			return $this->responseRedirect(
				'admin.orders.index',
				__('order::messages.not_found', ['number' => $orderNumber]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$order = $this->orderRepository->deleteOrder($orderNumber);
		
		if (!$order) {
			return $this->responseRedirectBack(
				__('order::messages.error_delete'),
				self::FAIL_RESPONSE,
				false,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.orders.index',
			__('order::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
