<?php


namespace Modules\Order\Services\GraphQL;

use App\Enums\DeliveryServices;
use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use App\User;
use CdekSDK\CdekClient;
use GuzzleHttp\Client;
use Modules\Delivery\Services\BoxberryService;
use Modules\Delivery\Services\CdekService;
use Modules\Order\Contracts\OrderContract;
use Modules\Order\Models\Order;
use Modules\Order\Models\OrderItem;
use Modules\Order\Traits\GetInfoAboutCityFromCdek;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerShop\Models\PartnerShop;
use Modules\User\Repositories\UserRepository;
use Str;

/**
 * Class OrderGraphQLService
 *
 * @package Modules\Order\Services\GraphQL
 */
class OrderGraphQLService
{
	use GetInfoAboutCityFromCdek;
	
	/** @var OrderContract */
	private $orderRepository;
	
	/** @var UserRepository */
	private $userRepository;
	
	/** @var PartnerOfferContract */
	private $offersReporitory;
	
	/** @var \CdekSDK\CdekClient */
	private $cdekClient;
	
	
	/**
	 * OrderController constructor.
	 *
	 * @param  OrderContract         $orderRepo
	 * @param  UserRepository        $userRepo
	 * @param  PartnerOfferContract  $offerRepo
	 */
	public function __construct(
		OrderContract $orderRepo,
		UserRepository $userRepo,
		PartnerOfferContract $offerRepo
	) {
		$this->orderRepository = $orderRepo;
		$this->userRepository = $userRepo;
		$this->offersReporitory = $offerRepo;
		
		if (config('cdek.test_mode_enable')) {
			$this->cdekClient = new CdekClient(
				config('cdek.test_mode.account'),
				config('cdek.test_mode.password'),
				new Client([
					'base_uri' => config('cdek.test_mode.base_uri'),
					'timeout'  => config('cdek.test_mode.timeout')
				])
			);
		}
		
		$this->cdekClient = new CdekClient(
			config('cdek.prod_mode.account'),
			config('cdek.prod_mode.password'),
			new Client([
				'base_uri' => config('cdek.prod_mode.base_uri'),
				'timeout'  => config('cdek.prod_mode.timeout')
			])
		);
	}
	
	/***
	 * @param  array  $params
	 *
	 * @return Order|null
	 */
	public function createOrder(array $params): ?Order
	{
		$cart = collect($params['basket']['items']);
		$cartProducts = collect($params['basket']['items']['products']);
		$grandTotal = $cartProducts->sum('price');
		$itemCount = $cartProducts->count();
		
		$paymentMethod = $params['paymentMethod'];
		$typeDelivery = $params['typeDelivery'];
		
		// Адрес получателя
		$addressReciever = $this->getFullAddress(
			$params['country'],
			$params['city'],
			$params['street'],
			$params['house'],
			$params['flat'] ?? null
		);
		
		// Покупатель
		$buyer = $this->getOrCreateAccountBuyer($params);
		
		$senderCityInfo = $this->getInfoAboutCityFromCdekAutoComplete($params['city'])['geonames'][0];
		$addressSender = $this->getSenderAddressFromCdek($senderCityInfo['id']);
		
		$orderData = [
			'order_number'   => strtoupper(uniqid('ORD-', false)),
			'user_id'        => $buyer->id,
			'status'         => OrderStatus::PENDING,
			'grand_total'    => $grandTotal,
			'item_count'     => $itemCount,
			'payment_status' => PaymentStatus::NOT_PAID,
			'payment_method' => $paymentMethod,
			'first_name'     => $params['firstName'],
			'last_name'      => $params['lastName'],
			'address'        => $addressReciever,
			'city'           => $params['city'],
			'country'        => $params['country'],
			'post_code'      => $params['postCode'],
			'phone_number'   => $params['phone'],
			'comments'       => $params['comments']
		];
		
		$partnerShop = PartnerShop::whereId($cart->get('shopId'))->first();
		
		if (DeliveryServices::CDEK === $typeDelivery) {
			$orderCDEK = new CdekService(
				$this->cdekClient,
				$orderData,
				$addressSender,
				$partnerShop,
				$buyer,
				$cartProducts->toArray()
			);
			// Номер заказа СДЭК
			$orderCdek = $orderCDEK->getOrder();
		}
		
		if (DeliveryServices::BOXBERRY === $typeDelivery) {
			$boxberry = new BoxberryService(
				$orderData,
				$partnerShop,
				$addressSender,
				$buyer,
				$cartProducts->toArray()
			);
			$orderBoxberry = $boxberry->getOrder();
			dd(__METHOD__, $orderBoxberry);
		}
		
		$order = Order::create($orderData);
		
		if ($order) {
			foreach ($cartProducts as $item) {
				$offer = PartnerOffer::whereId($item['offerId'])->first();
				
				if ($offer) {
					$orderItem = new OrderItem([
						'offer_id' => $item['offerId'],
						'quantity' => $item['countBuy'],
						'price'    => $item['price']
					]);
					
					$order->items()->save($orderItem);
				}
			}
		}
		
		return $order;
	}
	
	/**
	 * Возващает полный адрес
	 *
	 * @param  string       $country
	 * @param  string       $city
	 * @param  string       $street
	 * @param  string       $house
	 * @param  string|null  $flat
	 *
	 * @return string
	 */
	private function getFullAddress(string $country, string $city, string $street, string $house, ?string $flat): string
	{
		return "{$country}, {$city}, {$street}, {$house}, {$flat}";
	}
	
	
	/**
	 * @param  array  $params
	 *
	 * @return \App\User|\Illuminate\Database\Eloquent\Model
	 */
	private function getOrCreateAccountBuyer(array $params): User
	{
		// TODO: Создать событие для отправки сгенерированного пароля, покупателю после оформления заказа.
		$password = Str::random();
		
		return User::firstOrCreate([
			'phone' => $params['phone'],
			'email' => $params['email']
		], [
			'active'     => true,
			'sort'       => 500,
			'first_name' => $params['firstName'],
			'last_name'  => $params['lastName'],
			'email'      => $params['email'],
			'phone'      => $params['phone'],
			'password'   => $password
		]);
	}
}