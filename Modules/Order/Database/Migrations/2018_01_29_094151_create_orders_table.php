<?php

use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateOrdersTable
 */
class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->boolean('active')->default(true);
                $table->unsignedInteger('sort', false)->default(500);
                $table->string('order_number')->unique()->index();
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

                $table->string('status')->default(OrderStatus::PENDING);
                $table->decimal('grand_total', 20, 6);
                $table->unsignedInteger('item_count');

                $table->string('payment_status')->default(PaymentStatus::NOT_PAID);
                $table->string('payment_method')->nullable();

                $table->string('first_name');
                $table->string('last_name');
                $table->text('address');
                $table->string('city');
                $table->string('country');
                $table->string('post_code');
                $table->string('phone_number');
                $table->text('comments')->nullable();

                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
