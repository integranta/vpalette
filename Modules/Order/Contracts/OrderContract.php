<?php


namespace Modules\Order\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Order\Models\Order;

/**
 * Interface OrderContract
 *
 * @package Modules\Order\Contracts
 */
interface OrderContract
{
    /**
     * @param  array  $params
     *
     * @return Order
     */
    public function storeOrderDetails(array $params): Order;
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listOrders(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  string  $orderNumber
     *
     * @return Order|null
     */
    public function findOrderByNumber(string $orderNumber): ?Order;
    
    /**
     * @param  string  $orderNumber
     *
     * @return bool
     */
    public function deleteOrder(string $orderNumber): bool;
    
    /**
     * @param  array   $all
     * @param  string  $orderNumber
     *
     * @return bool
     */
    public function updateOrder(array $all, string $orderNumber): bool;
}
