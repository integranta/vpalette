<?php


namespace Modules\Newsletters\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Newsletters\Contracts\NewslettersContract;
use Modules\Newsletters\Http\Requests\API\V1\CreateNewslettersAPIRequest;
use Modules\Newsletters\Http\Requests\API\V1\UpdateNewslettersAPIRequest;
use Modules\Newsletters\Transformers\NewslettersResource;

/**
 * Class NewslettersApiService
 *
 * @package Modules\Newsletters\Services\API\V1
 */
class NewslettersApiService extends BaseService
{
    /** @var NewslettersContract */
    private $newslettersRepository;
    
    /**
     * NewslettersAPIController constructor.
     *
     * @param  NewslettersContract  $newslettersRepo
     */
    public function __construct(NewslettersContract $newslettersRepo)
    {
        $this->newslettersRepository = $newslettersRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $listNewsletters = NewslettersResource::collection($this->newslettersRepository->listNewsletters());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('newsletters::messages.success_retrieve'),
            $listNewsletters
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateNewslettersAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateNewslettersAPIRequest $request): JsonResponse
    {
        $newsletter = $this->newslettersRepository->createNewsletter($request->all());
        
        if (!$newsletter) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('newsletters::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('newsletters::messages.success_create'),
            $newsletter
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $newsletter = NewslettersResource::make($this->newslettersRepository->findNewsletterById($id));
        
        if (!$newsletter) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('newsletters::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('newsletters::messages.success_found', ['id' => $id]),
            $newsletter
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNewslettersAPIRequest  $request
     * @param  int                          $id
     *
     * @return JsonResponse
     */
    public function update(UpdateNewslettersAPIRequest $request, int $id): JsonResponse
    {
        $newsletter = $this->newslettersRepository->updateNewsletter($request->all(), $id);
        
        if (!$newsletter) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('newsletters::messages.error_edit')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('newsletters::messages.success_edit'),
            $newsletter
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $newsletter = $this->newslettersRepository->findNewsletterById($id);
        
        if (!$newsletter) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('newsletters::messages.not_found', ['id' => $id]),
                null
            );
        }
        
        $newsletter = $this->newslettersRepository->deleteNewsletter($id);
        
        if (!$newsletter) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('newsletters::messages.error_delete'),
                null
            );
        }
        
        return $this->responseJson(
            true,
            Response::HTTP_OK,
            __('newsletters::messages.success_delete'),
            $newsletter
        );
    }
}
