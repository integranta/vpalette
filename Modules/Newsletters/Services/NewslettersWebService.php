<?php


namespace Modules\Newsletters\Services;


use App\Enums\NewsletterStatus;
use App\Services\BaseService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Newsletters\Contracts\NewslettersContract;
use Modules\Newsletters\Http\Requests\CreateNewslettersRequest;
use Modules\Newsletters\Http\Requests\UpdateNewslettersRequest;

/**
 * Class NewslettersWebService
 *
 * @package Modules\Newsletters\Services
 */
class NewslettersWebService extends BaseService
{
	/**
	 * @var NewslettersContract
	 */
	private $newslettersRepository;
	
	
	/**
	 * NewslettersController constructor.
	 *
	 * @param  NewslettersContract  $newsRepo
	 */
	public function __construct(NewslettersContract $newsRepo)
	{
		$this->newslettersRepository = $newsRepo;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Factory|View
	 */
	public function index(): View
	{
		$newsletters = $this->newslettersRepository->listNewsletters();
		
		$this->setPageTitle(
			__('newsletters::messages.title'),
			__('newsletters::messages.index_subtitle'),
			__('newsletters::messages.index_leadtext')
		);
		
		return view('newsletters::newsletters.index', compact('newsletters'));
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$newsletterStatus = $this->getStatus();
		
		$this->setPageTitle(
			__('newsletters::messages.title'),
			__('newsletters::messages.create_subtitle'),
			__('newsletters::messages.create_leadtext')
		);
		
		return view('newsletters::newsletters.create', compact('newsletterStatus'));
	}
	
	/**
	 * Return assoc array enum values
	 *
	 * @return array
	 */
	public function getStatus(): array
	{
		return NewsletterStatus::toSelectArray();
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  CreateNewslettersRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateNewslettersRequest $request): RedirectResponse
	{
		$newsletter = $this->newslettersRepository->createNewsletter($request->all());
		
		if (!$newsletter) {
			return $this->responseRedirectBack(
				__('newsletters::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.newsletters.index',
			__('newsletters::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(int $id): View
	{
		$newsletter = $this->newslettersRepository->findNewsletterById($id);
		
		if ($newsletter === null) {
			return $this->responseRedirect(
				'admin.newsletters.index',
				__('newsletters::message.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('newsletters::messages.title'),
			__('newsletters::messages.show_subtitle', ['name' => $newsletter->name]),
			__('newsletters::messages.show_leadtext', ['name' => $newsletter->name])
		);
		
		return view('newsletters::newsletters.show', compact('newsletter'));
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$newsletter = $this->newslettersRepository->findNewsletterById($id);
		
		if ($newsletter === null) {
			return $this->responseRedirect(
				'admin.newsletters.index',
				__('newsletters::message.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('newsletters::messages.title'),
			__('newsletters::messages.edit_subtitle', ['name' => $newsletter->name]),
			__('newsletters::messages.edit_leadtext', ['name' => $newsletter->name])
		);
		
		return view('newsletters::newsletters.edit', compact('newsletter'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  UpdateNewslettersRequest  $request
	 * @param  int                       $id
	 *
	 * @return RedirectResponse
	 */
	public function update(UpdateNewslettersRequest $request, int $id): RedirectResponse
	{
		$newsletter = $this->newslettersRepository->updateNewsletter($request->all(), $id);
		
		if (!$newsletter) {
			return $this->responseRedirectBack(
				__('newsletters::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.newsletters.index',
			__('newsletters::message.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 */
	public function destroy(int $id): RedirectResponse
	{
		$newsletter = $this->newslettersRepository->findNewsletterById($id);
		
		if ($newsletter === null) {
			return $this->responseRedirect(
				'admin.newsletters.index',
				__('newsletters::message.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$newsletter = $this->newslettersRepository->deleteNewsletter($id);
		
		if (!$newsletter) {
			return $this->responseRedirectBack(
				__('newsletters::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.newsletters.index',
			__('newsletters::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}