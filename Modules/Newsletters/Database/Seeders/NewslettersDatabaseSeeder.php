<?php

namespace Modules\Newsletters\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Newsletters\Models\Newsletter;

/**
 * Class NewslettersDatabaseSeeder
 *
 * @package Modules\Newsletters\Database\Seeders
 */
class NewslettersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Newsletter::class, 1)->create();
    }
}
