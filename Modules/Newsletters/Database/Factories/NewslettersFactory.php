<?php

use App\Enums\NewsletterStatus;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Newsletters\Models\Newsletter;

/** @var Factory $factory */
$factory->define(Newsletter::class,
    function (Faker $faker) {
        
        $status = $faker->randomElement(NewsletterStatus::getValues());
        
        return [
            'active' => true,
            'sort' => $faker->numberBetween(1, 500),
            'name' => $faker->sentence(2),
            'content' => $faker->realText(400),
            'email_recipients' => $faker->safeEmail,
            'title_mail' => $faker->realText(50),
            'mail_from' => $faker->safeEmail,
            'mail_cc' => $faker->safeEmail,
            'mail_bcc' => $faker->safeEmail,
            'status' => $status,
            'send_at' => Carbon::now('Europe/Samara'),
            'sended_at' => Carbon::now('Europe/Samara')->addMinutes(15),
            'send_now' => $faker->numberBetween(0, 1),
            'deleted_at' => $status === NewsletterStatus::DELETED ? Carbon::now('Europe/Samara') : null
        ];
    });
