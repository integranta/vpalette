<?php

use App\Enums\NewsletterStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateNewslettersTable
 */
class CreateNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->string('name');
            $table->longText('content')->nullable();
            $table->longText('email_recipients')->nullable();
            $table->string('title_mail');
            $table->string('mail_from');
            $table->string('mail_cc')->nullable();
            $table->string('mail_bcc')->nullable();
            $table->string('status')
                ->default(NewsletterStatus::DRAFT);
            $table->dateTime('send_at')->nullable();
            $table->dateTime('sended_at')->nullable();
            $table->boolean('send_now')->default(false);
            $table->timestamps();
            $table->softDeletes();
            $table->index(['active', 'sort']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
    }
}
