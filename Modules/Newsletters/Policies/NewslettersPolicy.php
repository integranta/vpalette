<?php

namespace Modules\Newsletters\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Newsletters\Models\Newsletter;

/**
 * Class NewslettersPolicy
 *
 * @package Modules\Newsletters\Policies
 */
class NewslettersPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the newsletter.
     *
     * @param  User|null   $user
     * @param  Newsletter  $newsletter
     *
     * @return mixed
     */
    public function view(?User $user, Newsletter $newsletter)
    {
        if ($newsletter->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished newsletters')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create newsletters.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create newsletters')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the newsletter.
     *
     * @param  User        $user
     * @param  Newsletter  $newsletter
     *
     * @return mixed
     */
    public function update(User $user, Newsletter $newsletter)
    {
        if ($user->can('edit all newsletters')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the newsletter.
     *
     * @param  User        $user
     * @param  Newsletter  $newsletter
     *
     * @return mixed
     */
    public function delete(User $user, Newsletter $newsletter)
    {
        if ($user->can('delete any newsletter')) {
            return true;
        }
        
        return false;
    }
}
