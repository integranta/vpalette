<?php

return [
    'name'      => 'Newsletters',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all newsletters as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('NEWSLETTERS_CACHE_KEY', 'index_newsletters'),
    'cache_ttl' => env('NEWSLETTERS_CACHE_TTL', 360000),
];
