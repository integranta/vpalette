<?php

namespace Modules\Newsletters\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class NewslettersResource
 *
 * @package Modules\Newsletters\Transformers
 */
class NewslettersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'mailContent' => $this->content,
            'mailRecipients' => $this->email_recipients,
            'mailTitle' => $this->title_mail,
            'mailFrom' => $this->mail_from,
            'mailCC' => $this->mail_cc,
            'mailBCC' => $this->mail_bcc,
            'mailStatus' => $this->status,
            'mailSendAt' => $this->send_at,
            'mailSendedAt' => $this->sended_at,
            'mailSendNow' => (bool) $this->send_now,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
