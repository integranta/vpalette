<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Newsletters\Http\Controllers\NewslettersController;

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		Route::resource('newsletters', NewslettersController::class);
	});

Route::name('admin.')
	->prefix('admin')
	->group(function () {
		Route::get('newsletters/get_templates', [NewslettersController::class, 'getTemplates']);
		Route::get('newsletters/get_data', [NewslettersController::class, 'getVueData']);
	});
