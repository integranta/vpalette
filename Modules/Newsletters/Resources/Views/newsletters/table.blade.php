<div class="table-responsive">
    <table class="table" id="newsletters-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th style="width:100px; min-width:100px;" class="text-center">Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($newsletters as $newsletter)
            <tr>
                <td>{{ $newsletter->id }}</td>
                <td>{{ $newsletter->name }}</td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="CRUD операции">
                        {!! Form::open(['route' => ['admin.newsletters.destroy', $newsletter->id], 'method' => 'delete']) !!}
                        <a
                            href="{{ route('admin.newsletters.show', $newsletter->id) }}"
                            class="btn btn-sm btn-success">
                            <i class="fa fa-eye"></i>
                        </a>
                        @can('edit all newsletters')
                            <a
                                href="{{ route('admin.newsletters.edit', $newsletter->id) }}"
                                class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endcan
                        @can('delete all newsletters')
                            {!! Form::button(
                                '<i class="fa fa-trash"></i>',
                                [
                                    'type' => 'submit',
                                    'class' => 'btn btn-sm btn-danger',
                                    'title' => 'Удалить',
                                    'onclick' => "return confirm('Вы уверены?')"
                                ])
                            !!}
                        @endcan
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
