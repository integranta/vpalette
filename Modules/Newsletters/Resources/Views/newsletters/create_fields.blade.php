<div class="form-group row col-md-12">
    {!!
        Form::label(
            'name',
            'Название E-mail рассылки:',
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'name',
                null,
                ['class' => 'form-control','id'=>'name', 'required']
            )
        !!}
    </div>
</div>
<!-- Active field -->
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Отправить сразу:</span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        {!! Form::hidden('send_now', 0) !!}
        {!!
            Form::checkbox(
                'send_now',
                true,
                null,
                ['class' => 'custom-control-input', 'id' => 'send_now']
            )
        !!}
        <label class="custom-control-label ml-lg-3" for="send_now"></label>
    </div>
</div>

<!-- Activity End at Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'sended_at',
            'Отправить в:',
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!!
            Form::text(
                'sended_at',
                null,
                ['class' => 'form-control datetimepicker','id'=>'sended_at']
            )
        !!}
    </div>
</div>
<!-- Name Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
            'title_mail',
            'Тема письма:',
            ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
        )
    !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('title_name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Sort Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
        'from',
        'От:',
        ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3'])
    !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('from', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- CC Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
        'cc',
        'Копия:',
        ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3'])
    !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('cc', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- BCC Field -->
<div class="form-group row col-md-12">
    {!!
        Form::label(
        'bcc',
        'Скрытая копия:',
        ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3'])
    !!}
    <div class="col-sm-12 col-md-7">
        {!! Form::text('bcc', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group row mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="status">Статус <span class="m-l-5 text-danger"> *</span>
    </label>
    <div class="col-sm-12 col-md-7">

        <select
            id="status"
            class="form-control custom-select mt-15 @error('status') is-invalid @enderror"
            name="status"
        >
            <option value="0">Установите статус</option>
            @foreach($newsletterStatus as $key => $nameStatus)
                <option value="{{ $key }}"> {{ $nameStatus }} </option>
            @endforeach
        </select>
        @error('status') {{ $message }} @enderror
    </div>
</div>
