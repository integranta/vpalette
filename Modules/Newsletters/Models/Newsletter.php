<?php

namespace Modules\Newsletters\Models;

use App\Enums\NewsletterStatus;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Newsletter
 *
 * @package Modules\Newsletters\Models
 * @property int $id
 * @property int $active
 * @property int $sort
 * @property string $name
 * @property string|null $content
 * @property string|null $emailRecipients
 * @property string $titleMail
 * @property string $mailFrom
 * @property string|null $mailCc
 * @property string|null $mailBcc
 * @property string $status
 * @property string|null $sendAt
 * @property string|null $sendedAt
 * @property int $sendNow
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Newsletters\Models\Newsletter onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereEmailRecipients($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereMailBcc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereMailCc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereMailFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereSendAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereSendNow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereSendedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereTitleMail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Newsletters\Models\Newsletter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Newsletters\Models\Newsletter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Newsletters\Models\Newsletter withoutTrashed()
 * @mixin \Eloquent
 */
class Newsletter extends Model
{
    use SoftDeletes;
    use CastsEnums;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'newsletters';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'email_recipients',
        'name',
        'content',
        'title_mail',
        'mail_from',
        'mail_cc',
        'mail_bcc',
        'status',
        'send_at',
        'sended_at',
        'send_now'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'email_recipients' => 'string',
        'name' => 'string',
        'content' => 'string',
        'status' => 'string'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'status' => NewsletterStatus::class
    ];
}
