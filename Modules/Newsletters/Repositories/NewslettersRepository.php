<?php


namespace Modules\Newsletters\Repositories;


use App\Repositories\BaseRepository;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Newsletters\Contracts\NewslettersContract;
use Modules\Newsletters\Models\Newsletter;
use Str;

/**
 * Class NewslettersRepository
 *
 * @package Modules\Newsletters\Repositories
 */
class NewslettersRepository extends BaseRepository implements NewslettersContract
{
    /**
     * NewslettersRepository constructor.
     *
     * @param  Newsletter  $model
     */
    public function __construct(Newsletter $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listNewsletters(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('newsletters.cache_key').$postfix,
            config('newsletters.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return Newsletter
     */
    public function findNewsletterById(int $id): ?Newsletter
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return Newsletter
     */
    public function createNewsletter(array $params): Newsletter
    {
        return $this->create($params);
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Newsletter
     */
    public function updateNewsletter(array $params, int $id): Newsletter
    {
        return $this->update($params, $id);
    }
    
    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteNewsletter(int $id): bool
    {
        return $this->delete($id);
    }
}
