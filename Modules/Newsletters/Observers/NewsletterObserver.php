<?php

namespace Modules\Newsletters\Observers;

use App\Traits\Cacheable;
use Modules\Newsletters\Models\Newsletter;

/**
 * Class NewsletterObserver
 *
 * @package Modules\Newsletters\Observers
 */
class NewsletterObserver
{
    use Cacheable;
    
    /**
     * Handle the newsletter "created" event.
     *
     * @param  Newsletter  $newsletter
     *
     * @return void
     */
    public function created(Newsletter $newsletter): void
    {
        $this->incrementCountItemsInCache($newsletter, config('newsletters.cache_key'));
    }
    
    /**
     * Handle the newsletter "updated" event.
     *
     * @param  Newsletter  $newsletter
     *
     * @return void
     */
    public function updated(Newsletter $newsletter): void
    {
        //
    }
    
    /**
     * Handle the newsletter "deleted" event.
     *
     * @param  Newsletter  $newsletter
     *
     * @return void
     */
    public function deleted(Newsletter $newsletter): void
    {
        $this->incrementCountItemsInCache($newsletter, config('newsletters.cache_key'));
    }
    
    /**
     * Handle the newsletter "restored" event.
     *
     * @param  Newsletter  $newsletter
     *
     * @return void
     */
    public function restored(Newsletter $newsletter): void
    {
        $this->incrementCountItemsInCache($newsletter, config('newsletters.cache_key'));
    }
    
    /**
     * Handle the newsletter "force deleted" event.
     *
     * @param  Newsletter  $newsletter
     *
     * @return void
     */
    public function forceDeleted(Newsletter $newsletter): void
    {
        $this->incrementCountItemsInCache($newsletter, config('newsletters.cache_key'));
    }
}
