<?php


namespace Modules\Newsletters\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Newsletters\Models\Newsletter;

/**
 * Interface NewslettersContract
 *
 * @package Modules\Newsletters\Contracts
 */
interface NewslettersContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listNewsletters(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Newsletter
     */
    public function findNewsletterById(int $id): ?Newsletter;
    
    /**
     * @param  array  $params
     *
     * @return Newsletter
     */
    public function createNewsletter(array $params): Newsletter;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Newsletter
     */
    public function updateNewsletter(array $params, int $id): Newsletter;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteNewsletter(int $id): bool;
}
