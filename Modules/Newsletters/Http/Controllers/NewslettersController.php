<?php

namespace Modules\Newsletters\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Newsletters\Http\Requests\CreateNewslettersRequest;
use Modules\Newsletters\Http\Requests\UpdateNewslettersRequest;
use Modules\Newsletters\Services\NewslettersWebService;

/**
 * Class NewslettersController
 *
 * @package Modules\Newsletters\Http\Controllers
 */
class NewslettersController extends Controller
{
    /** @var NewslettersWebService */
    private $newslettersWebService;


    /**
     * NewslettersController constructor.
     *
     * @param  NewslettersWebService  $service
     */
    public function __construct(NewslettersWebService $service)
    {
        $this->newslettersWebService = $service;

        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        return $this->newslettersWebService->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->newslettersWebService->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateNewslettersRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateNewslettersRequest $request): RedirectResponse
    {
        return $this->newslettersWebService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $id): View
    {
        return $this->newslettersWebService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->newslettersWebService->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNewslettersRequest  $request
     * @param  int                       $id
     *
     * @return RedirectResponse
     */
    public function update(UpdateNewslettersRequest $request, int $id): RedirectResponse
    {
        return $this->newslettersWebService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->newslettersWebService->destroy($id);
    }

    /**
     * Return assoc array enum values
     *
     * @return array
     */
    public function getStatus(): array
    {
        return $this->newslettersWebService->getStatus();
    }
}
