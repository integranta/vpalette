<?php

namespace Modules\Newsletters\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Newsletters\Http\Requests\API\V1\CreateNewslettersAPIRequest;
use Modules\Newsletters\Http\Requests\API\V1\UpdateNewslettersAPIRequest;
use Modules\Newsletters\Services\API\V1\NewslettersApiService;

/**
 * Class NewslettersAPIController
 *
 * @package Modules\Newsletters\Http\Controllers\API\V1
 */
class NewslettersAPIController extends Controller
{
    /** @var NewslettersApiService */
    private $newslettersApiService;

    /**
     * NewslettersAPIController constructor.
     *
     * @param  NewslettersApiService  $service
     */
    public function __construct(NewslettersApiService $service)
    {
        $this->newslettersApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->newslettersApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateNewslettersAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateNewslettersAPIRequest $request): JsonResponse
    {
        return $this->newslettersApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->newslettersApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNewslettersAPIRequest  $request
     * @param  int                          $id
     *
     * @return JsonResponse
     */
    public function update(UpdateNewslettersAPIRequest $request, int $id): JsonResponse
    {
        return $this->newslettersApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->newslettersApiService->destroy($id);
    }
}
