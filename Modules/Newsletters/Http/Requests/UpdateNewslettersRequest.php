<?php

namespace Modules\Newsletters\Http\Requests;

use App\Enums\NewsletterStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateNewslettersRequest
 *
 * @package Modules\Newsletters\Http\Requests
 */
class UpdateNewslettersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'content' => 'required|string',
            'email_recipients' => 'required|email',
            'title_mail' => 'required|string|max:100',
            'mail_from' => 'required|email',
            'mail_cc' => 'nullable|email',
            'mail_bcc' => 'nullable|email',
            'status' => [
                'required',
                Rule::in(NewsletterStatus::getValues())
            ],
            'send_at' => 'nullable|date',
            'send_now' => 'required|boolean'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
