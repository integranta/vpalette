<?php

namespace Modules\Wishlist\Providers;

use Config;
use Illuminate\Auth\AuthManager;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Wishlist\Contracts\WishlistContract;
use Modules\Wishlist\Http\Controllers\API\V1\WishlistAPIController;
use Modules\Wishlist\Services\API\V1\AuthWishlistService;
use Modules\Wishlist\Services\API\V1\CookieWishlistService;

/**
 * Class WishlistServiceProvider
 *
 * @package Modules\Wishlist\Providers
 */
class WishlistServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('Wishlist', 'Database/Migrations'));
    }
    
    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/wishlist');
        
        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'wishlist');
        } else {
            $this->loadTranslationsFrom(module_path('Wishlist', 'Resources/Lang'), 'wishlist');
        }
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Wishlist', 'Config/config.php') => config_path('wishlist.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Wishlist', 'Config/config.php'), 'wishlist'
        );
    }
    
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/wishlist');
        
        $sourcePath = module_path('Wishlist', 'Resources/Views');
        
        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');
        
        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path.'/modules/wishlist';
        }, Config::get('view.paths')), [$sourcePath]), 'wishlist');
    }
    
    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('Wishlist', 'Database/Factories'));
        }
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->when(WishlistAPIController::class)
            ->needs(WishlistContract::class)
            ->give(static function (AuthManager $authManager) {
                if ($authManager->guard()->user()) {
                    return new AuthWishlistService;
                }
                
                return new CookieWishlistService;
            });
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
