<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Wishlist\Http\Controllers\API\V1\WishlistAPIController;

Route::prefix('v1')->group(function () {
    Route::get('wishlist', [WishlistAPIController::class, 'index']);
    Route::post('wishlist', [WishlistAPIController::class, 'store']);
    Route::delete('wishlist/{product_id}', [WishlistAPIController::class, 'destroy']);
});
