<?php

namespace Modules\Wishlist\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Wishlist\Http\Requests\API\V1\CreateWishlistAPIRequest;
use Modules\Wishlist\Services\API\V1\AuthWishlistService;
use Modules\Wishlist\Services\API\V1\CookieWishlistService;
use Illuminate\Http\Response;

/**
 * Class WishlistAPIController
 *
 * @package Modules\Wishlist\Http\Controllers\API\V1
 */
class WishlistAPIController extends Controller
{
    /** @var CookieWishlistService */
    private $cookieWishlistService;

    /** @var AuthWishlistService */
    private $authWishlistService;


    /**
     * WishlistAPIController constructor.
     *
     * @param  CookieWishlistService  $cookieWishlistService
     * @param  AuthWishlistService    $authWishlistService
     */
    public function __construct(CookieWishlistService $cookieWishlistService, AuthWishlistService $authWishlistService)
    {
        $this->cookieWishlistService = $cookieWishlistService;
        $this->authWishlistService = $authWishlistService;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index']);

        if (!Auth::guard('api')->user()) {
            $this->middleware('auth:api')->except(['index', 'store', 'destroy']);
        } else {
            $this->middleware('auth:api');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if (Auth::guard('api')->user()) {
            return $this->authWishlistService->index($request);
        }

        return $this->cookieWishlistService->index($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateWishlistAPIRequest  $request
     *
     * @return JsonResponse|Response
     */
    public function store(CreateWishlistAPIRequest $request): Response
    {
        if (Auth::guard('api')->user()) {
            return $this->authWishlistService->store($request);
        }

        return $this->cookieWishlistService->store($request);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $productId
     *
     * @return JsonResponse
     */
    public function destroy(int $productId): JsonResponse
    {
        if (Auth::guard('api')->user()) {
            return $this->authWishlistService->destroy($productId);
        }

        return $this->cookieWishlistService->destroy($productId);
    }
}
