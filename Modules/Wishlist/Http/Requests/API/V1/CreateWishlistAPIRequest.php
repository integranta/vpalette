<?php

namespace Modules\Wishlist\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateWishlistAPIRequest
 *
 * @package Modules\Wishlist\Http\Requests\API\V1
 */
class CreateWishlistAPIRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'product_id' => 'required|integer'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
