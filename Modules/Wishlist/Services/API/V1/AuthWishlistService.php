<?php


namespace Modules\Wishlist\Services\API\V1;

use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Wishlist\Contracts\WishlistContract;
use Modules\Wishlist\Http\Requests\API\V1\CreateWishlistAPIRequest;

/**
 * Class AuthWishlistService
 *
 * @package Modules\Wishlist\Services\API\V1
 */
class AuthWishlistService
{
    /** @var WishlistContract */
    private $wishlistRepository;
    
    /**
     * AuthWishlistService constructor.
     *
     * @param  WishlistContract  $wishlistRepo
     *
     * @retun void
     */
    public function __construct(WishlistContract $wishlistRepo)
    {
        $this->wishlistRepository = $wishlistRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $wishlists = $this->wishlistRepository->listWishlists(['user_id' => Auth::id()], $request->perCount);
        
        return response()->json([
                'error' => false,
                'code' => Response::HTTP_OK,
                'message' => __('wishlist::messages.success_retrieve'),
                'data' => $wishlists
            ]
        );
    }
    
    /**
     * @param  CreateWishlistAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateWishlistAPIRequest $request): JsonResponse
    {
        $wishlist = $this->wishlistRepository->createWishlist((int) $request->product_id, Auth::id());
        
        if ($wishlist === null) {
            
            return response()->json([
                    'error' => true,
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => __('wishlist::messages.error_create')
                ]
            );
        }
        
        return response()->json([
                'error' => false,
                'code' => Response::HTTP_CREATED,
                'message' => __('wishlist::messages.success_create'),
                'data' => $wishlist
            ]
        );
    }
    
    /**
     * @param  int  $productId
     *
     * @return JsonResponse
     */
    public function destroy(int $productId): JsonResponse
    {
        $wishlist = $this->wishlistRepository->deleteWishlist($productId, Auth::id());
        
        if (!$wishlist) {
            return response()->json([
                    'error' => true,
                    'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'message' => __('wishlist::messages.error_delete')
                ]
            );
        }
        
        return response()->json([
                'error' => false,
                'code' => Response::HTTP_OK,
                'message' => __('wishlist::messages.success_delete')
            ]
        );
    }
}
