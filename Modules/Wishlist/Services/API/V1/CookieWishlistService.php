<?php


namespace Modules\Wishlist\Services\API\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Wishlist\Contracts\WishlistContract;

/**
 * Class CookieWishlistService
 *
 * @package Modules\Wishlist\Services\API\V1
 */
class CookieWishlistService
{
    /** @var WishlistContract */
    private $wishlistRepository;
    
    /**
     * CookieWishlistService constructor.
     *
     * @param  WishlistContract  $wishlistRepo
     *
     * @return void
     */
    public function __construct(WishlistContract $wishlistRepo)
    {
        $this->wishlistRepository = $wishlistRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $wishlists = $this->wishlistRepository->listWishlistsCookie(
            $request->cookie('favorite'),
            $request->perCount
        );
        
        return response()->json([
                'error' => false,
                'code' => Response::HTTP_OK,
                'message' => __('wishlist::messages.success_retrieve-cookie'),
                'data' => $wishlists
            ]
        );
    }
    
    /**
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request): Response
    {
        return $this->wishlistRepository->createWishlistCookie($request->all());
    }
    
    /**
     * @param  int  $productId
     *
     * @return JsonResponse
     */
    public function destroy(int $productId): JsonResponse
    {
        if (request()->hasCookie('favorite')) {
            
            return $this->wishlistRepository->deleteWishlistCookie(
                request()->cookie('favorite'),
                $productId
            );
        }
        
        return response()->json([
                'error' => true,
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => __('wishlist::messages.error_delete_cookie'),
                'data' => null
            ]
        );
    }
}
