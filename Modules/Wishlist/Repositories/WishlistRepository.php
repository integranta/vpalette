<?php


namespace Modules\Wishlist\Repositories;

use App\Repositories\BaseRepository;
use Cookie;
use Crypt;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Wishlist\Contracts\WishlistContract;
use Modules\Wishlist\Models\Wishlist;

/**
 * Class WishlistRepository
 *
 * @package Modules\Wishlist\Repositories
 */
class WishlistRepository extends BaseRepository implements WishlistContract
{
    /**
     * WishlistRepository constructor.
     *
     * @param  Wishlist  $model
     */
    public function __construct(Wishlist $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listWishlists(array $data, int $perPage): LengthAwarePaginator
    {
        return $this->paginate($data, $perPage);
    }
    
    /**
     * @param  string|array|null  $cookie
     * @param  int                $perPage
     *
     * @return LengthAwarePaginator
     */
    public function listWishlistsCookie(?string $cookie, int $perPage): LengthAwarePaginator
    {
        $values = Crypt::decrypt($cookie, true);
        return collect($values)->paginate($perPage);
    }
    
    /**
     * @inheritDoc
     */
    public function findWishlistById(int $id): ?Wishlist
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function findWishlistBy(array $data): ?Wishlist
    {
        return $this->findBy($data);
    }
    
    /**
     * @inheritDoc
     */
    public function createWishlist(int $productId, int $userId): ?array
    {
        $wishlist = $this->findOneBy(['user_id' => $userId, 'product_id' => $productId]);
        
        if (!isset($wishlist->user_id, $productId)) {
            if (request()->hasCookie('favorite')) {
                $values = collect(Crypt::decrypt(request()->cookie('favorite'), true));
                
                $wishlist = $this->findOneBy(['user_id' => $userId, 'product_id' => $values['product_id']]);
                
                if (!isset($wishlist->user_id, $values['product_id'])) {
                    $values->push(['product_id' => $productId]);
                    
                    $values = $values->map(static function ($item) use ($userId) {
                        $item['user_id'] = $userId;
                        return $item;
                    })->uniqueStrict('product_id');
                    
                    Cookie::forget('favorite');
                    
                    return $this->createMany($values->toArray());
                }
            }
            
            return $this->create([
                'product_id' => $productId,
                'user_id' => $userId
            ]);
        }
        
        return null;
    }
    
    /**
     * @inheritDoc
     */
    public function createWishlistCookie(array $params): Response
    {
        $response = new Response([
            'error' => false,
            'status' => Response::HTTP_CREATED,
            'message' => __('wishlist::messages.success_create-cookie'),
        ]);
        
        if (request()->hasCookie('favorite')) {
            $value[] = Crypt::decrypt(request()->cookie('favorite'), true);
            $value[]['product_id'] = $params['product_id'];
            
            $value = json_encode(Crypt::encrypt($value, true), JSON_THROW_ON_ERROR, 512);
            
            return $response->withCookie(
                Cookie::forever(
                    'favorite',
                    $value,
                    null,
                    null,
                    false,
                    true,
                    false,
                    null
                )
            );
        }
        
        $value = json_encode(Crypt::encrypt($params, true), JSON_THROW_ON_ERROR, 512);
        
        return $response->withCookie(Cookie::forever(
            'favorite',
            $value,
            null,
            null,
            false,
            true,
            false,
            null
        ));
    }
    
    /**
     * @inheritDoc
     */
    public function deleteWishlist(int $productId, int $userId): bool
    {
        $wishlist = $this->findOneBy(['product_id' => $productId, 'user_id' => $userId]);
        
        return $this->delete($wishlist);
    }
    
    /**
     * @param  string  $cookie
     * @param  int     $productId
     *
     * @return JsonResponse
     */
    public function deleteWishlistCookie(string $cookie, int $productId): JsonResponse
    {
        $response = response()->json([
            'error' => false,
            'status' => Response::HTTP_OK,
            'message' => __('wishlist::messages.success_delete-cookie'),
        ]);
        
        $values = collect(Crypt::decrypt($cookie, true))->only($productId);
        
        $value = json_encode(Crypt::encrypt($values, true), JSON_THROW_ON_ERROR, 512);
        
        return $response->withCookie(
            Cookie::forever(
                'favorite',
                $value,
                null,
                null,
                false,
                true,
                false,
                null
            ));
    }
}
