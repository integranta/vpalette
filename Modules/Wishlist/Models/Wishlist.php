<?php

namespace Modules\Wishlist\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Product\Models\Product;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Wishlist
 *
 * @package Modules\Wishlist\Models
 * @property int $id
 * @property int $userId
 * @property int $productId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \App\User $owner
 * @property-read \Modules\Product\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Wishlist\Models\Wishlist onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Wishlist\Models\Wishlist whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Wishlist\Models\Wishlist withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Wishlist\Models\Wishlist withoutTrashed()
 * @mixin \Eloquent
 */
class Wishlist extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required|integer|exists:users,id',
        'product_id' => 'required|integer|exists:products,id'
    ];

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'wishlists';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'product_id'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'product_id' => 'integer'
    ];

    /**
     * Получить владельца этого списка желания.
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получить товары из этого списка желания.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
