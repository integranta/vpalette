<?php


namespace Modules\Wishlist\Contracts;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Wishlist\Models\Wishlist;

/**
 * Interface WishlistContract
 *
 * @package Modules\Wishlist\Contracts
 */
interface WishlistContract
{
    /**
     * @param  array  $data
     * @param  int    $perPage
     *
     * @return LengthAwarePaginator
     */
    public function listWishlists(array $data, int $perPage): LengthAwarePaginator;
    
    /**
     * @param  string|array  $cookie
     * @param  int           $perPage
     *
     * @return LengthAwarePaginator
     */
    public function listWishlistsCookie(?string $cookie, int $perPage): LengthAwarePaginator;
    
    /**
     * @param  int  $id
     *
     * @return Wishlist
     */
    public function findWishlistById(int $id): ?Wishlist;
    
    /**
     * @param  array  $data
     *
     * @return Wishlist
     */
    public function findWishlistBy(array $data): ?Wishlist;
    
    /**
     * @param  int  $productId
     * @param  int  $userId
     *
     * @return array|Wishlist
     */
    public function createWishlist(int $productId, int $userId): ?array;
    
    /**
     * @param  array  $params
     *
     * @return Response
     */
    public function createWishlistCookie(array $params): Response;
    
    
    /**
     * @param  int  $productId
     * @param  int  $userId
     *
     * @return bool
     */
    public function deleteWishlist(int $productId, int $userId): bool;
    
    /**
     * @param  string  $params
     * @param  int     $productId
     *
     * @return JsonResponse
     */
    public function deleteWishlistCookie(string $params, int $productId): JsonResponse;
}
