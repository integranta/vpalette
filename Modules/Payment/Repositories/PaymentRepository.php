<?php


namespace Modules\Payment\Repositories;


use App\Repositories\BaseRepository;
use Modules\Payment\Contracts\PaymentContract;

/**
 * Class PaymentRepository
 *
 * @package Modules\Payment\Repositories
 */
class PaymentRepository extends BaseRepository implements PaymentContract
{

}
