<?php


namespace Modules\Auth\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Auth\Http\Requests\API\V1\RegisterAPIRequest;
use Modules\User\Contracts\UserContract;

/**
 * Class RegisterApiService
 *
 * @package Modules\Auth\Services\API\V1
 */
class RegisterApiService extends BaseService
{
    /** @var UserContract */
    private $userRepository;
    
    /**
     * RegisterAPIController constructor.
     *
     * @param  UserContract  $userRepo
     */
    public function __construct(UserContract $userRepo)
    {
        $this->userRepository = $userRepo;
    }
    
    /**
     * @param  RegisterAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function register(RegisterAPIRequest $request): JsonResponse
    {
        $user = $this->userRepository->createUser($request->all());
        
        if (!$user) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('auth::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('auth::messages.success_create'),
            $user
        );
    }
}
