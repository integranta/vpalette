<?php


namespace Modules\Auth\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class LogoutApiService
 *
 * @package Modules\Auth\Services\API\V1
 */
class LogoutApiService extends BaseService
{
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()->revoke();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('auth::messages.success_logout')
        );
    }
}
