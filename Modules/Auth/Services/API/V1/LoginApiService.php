<?php


namespace Modules\Auth\Services\API\V1;


use App\Services\BaseService;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Auth\Http\Requests\API\V1\LoginAPIRequest;

/**
 * Class LoginApiService
 *
 * @package Modules\Auth\Services\API\V1
 */
class LoginApiService extends BaseService
{
    /**
     * @param  LoginAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function login(LoginAPIRequest $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');
        
        if (!Auth::attempt($credentials)) {
            return $this->responseJson(
                true,
                Response::HTTP_UNAUTHORIZED,
                __('auth::messages.error_auth')
            );
        }
        
        $token = Auth::user()->createToken(config('app.name'));
        $token->token->expires_at = $request->remember_me
            ? Carbon::now()->addMonth()
            : Carbon::now()->addDay();
        
        $token->token->save();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('auth::messages.success_auth'),
            [
                'token_type' => 'Bearer',
                'token' => $token->accessToken,
                'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
            ]
        );
    }
}
