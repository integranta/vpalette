<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Auth\Http\Requests\API\V1\RegisterAPIRequest;
use Modules\Auth\Services\API\V1\RegisterApiService;

/**
 * Class RegisterAPIController
 *
 * @package Modules\Auth\Http\Controllers\API\V1
 */
class RegisterAPIController extends Controller
{
    /** @var RegisterApiService */
    private $registerApiService;
    
    /**
     * RegisterAPIController constructor.
     *
     * @param  RegisterApiService  $service
     */
    public function __construct(RegisterApiService $service)
    {
        $this->registerApiService = $service;
    }
    
    /**
     * @param  RegisterAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function __invoke(RegisterAPIRequest $request): JsonResponse
    {
        return $this->registerApiService->register($request);
    }
}
