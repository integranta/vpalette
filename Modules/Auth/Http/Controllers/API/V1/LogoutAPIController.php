<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Auth\Services\API\V1\LogoutApiService;

/**
 * Class LogoutAPIController
 *
 * @package Modules\Auth\Http\Controllers\API\V1
 */
class LogoutAPIController extends Controller
{
    /** @var LogoutApiService */
    private $logoutApiService;
    
    /**
     * LogoutAPIController constructor.
     *
     * @param  LogoutApiService  $service
     */
    public function __construct(LogoutApiService $service)
    {
        $this->logoutApiService = $service;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        return $this->logoutApiService->logout($request);
    }
}
