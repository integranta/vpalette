<?php

namespace Modules\Auth\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Auth\Http\Requests\API\V1\LoginAPIRequest;
use Modules\Auth\Services\API\V1\LoginApiService;

/**
 * Class LoginAPIController
 *
 * @package Modules\Auth\Http\Controllers\API\V1
 */
class LoginAPIController extends Controller
{
    /** @var LoginApiService */
    private $loginApiService;
    
    /**
     * LoginAPIController constructor.
     *
     * @param  LoginApiService  $service
     */
    public function __construct(LoginApiService $service)
    {
        $this->loginApiService = $service;
    }
    
    /**
     * @param  LoginAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function __invoke(LoginAPIRequest $request): JsonResponse
    {
        return $this->loginApiService->login($request);
    }
}
