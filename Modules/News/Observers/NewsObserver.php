<?php

namespace Modules\News\Observers;

use App\Traits\Cacheable;
use Carbon\Carbon;
use Modules\Dashboard\Models\Dashboard;
use Modules\News\Models\News;
use Storage;

/**
 * Class NewsObserver
 *
 * @package Modules\News\Observers
 */
class NewsObserver
{
    use Cacheable;
    
    /**
     * Handle the news "created" event.
     *
     * @param  News  $news
     *
     * @return void
     */
    public function created(News $news): void
    {
        if ($news->activity_end_at === Carbon::now()) {
            $news->update([
                'active' => 0
            ]);
        }
        
        Dashboard::increment('count_news', 1);
        
        $this->incrementCountItemsInCache($news, config('news.cache_key'));
    }
    
    /**
     * Handle the news "updated" event.
     *
     * @param  News  $news
     *
     * @return void
     */
    public function updated(News $news): void
    {
        //
    }
    
    /**
     * Handle the news "deleting" event.
     *
     * @param  News  $news
     *
     * @return void
     */
    public function deleting(News $news): void
    {
        if (Storage::disk('uploads')->exists("news/{$news->name}")) {
            Storage::disk('uploads')->deleteDirectory("news/{$news->name}");
        }
        
        $this->incrementCountItemsInCache($news, config('news.cache_key'));
    }
    
    /**
     * Handle the news "deleted" event.
     *
     * @param  News  $news
     *
     * @return void
     */
    public function deleted(News $news): void
    {
        Dashboard::decrement('count_news', 1);
        
        $this->decrementCountItemsInCache($news, config('news.cache_key'));
    }
    
    /**
     * Handle the news "restored" event.
     *
     * @param  News  $news
     *
     * @return void
     */
    public function restored(News $news): void
    {
        Dashboard::increment('count_news', 1);
        
        $this->incrementCountItemsInCache($news, config('news.cache_key'));
    }
    
    /**
     * Handle the news "force deleted" event.
     *
     * @param  News  $news
     *
     * @return void
     */
    public function forceDeleted(News $news): void
    {
        Dashboard::decrement('count_news', 1);
        
        $this->decrementCountItemsInCache($news, config('news.cache_key'));
    }
}
