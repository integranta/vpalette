<?php

namespace Modules\News\Events;

use Illuminate\Queue\SerializesModels;
use Modules\News\Models\News;

/**
 * Class NewsPostWasVisited
 *
 * @package Modules\News\Events
 */
class NewsPostWasVisited
{
    use SerializesModels;
    
    /**
     * @var News
     */
    public $post;
    
    /**
     * Create a new event instance.
     *
     * @param  News  $post
     */
    public function __construct(News $post)
    {
        $this->post = $post;
    }
    
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn(): array
    {
        return [];
    }
}
