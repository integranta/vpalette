<?php


namespace Modules\News\Services;


use App\DataTables\PostsDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\News\Contracts\NewsContract;
use Modules\News\Http\Requests\CreateNewsRequest;
use Modules\News\Http\Requests\UpdateNewsRequest;
use Modules\News\Models\News;
use Modules\User\Contracts\UserContract;

/**
 * Class NewsWebService
 *
 * @package Modules\News\Services
 */
class NewsWebService extends BaseService
{
	/*** @var NewsContract */
	private $newsRepository;
	
	
	/** @var UserContract */
	private $userRepository;
	
	/**
	 * NewsController constructor.
	 *
	 * @param  NewsContract  $newsRepo
	 * @param  UserContract  $userRepo
	 */
	public function __construct(
		NewsContract $newsRepo,
		UserContract $userRepo
	) {
		$this->newsRepository = $newsRepo;
		$this->userRepository = $userRepo;
	}
	
	/**
	 * Display a listing of the News.
	 *
	 * @param  \App\DataTables\PostsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(PostsDataTable $dataTable)
	{
		$this->setPageTitle(
			__('news::messages.title'),
			__('news::messages.index_subtitle'),
			__('news::messages.index_leadtext')
		);
		
		return $dataTable->render('news::news.index');
	}
	
	/**
	 * Show the form for creating a new News.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$model = new News;
		$seoInfo = $model->load('seo');
		
		$users = $this->userRepository->listUsers();
		
		$this->setPageTitle(
			__('news::messages.title'),
			__('news::messages.create_subtitle'),
			__('news::messages.create_leadtext')
		);
		
		return view('news::news.create', compact('model', 'seoInfo', 'users'));
	}
	
	/**
	 * Store a newly created News in storage.
	 *
	 * @param  CreateNewsRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateNewsRequest $request): RedirectResponse
	{
		$news = $this->newsRepository->createNews($request->all());
		
		if (!$news) {
			return $this->responseRedirectBack(
				__('news::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.posts.index',
			__('news::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified News.
	 *
	 * @param  int  $id
	 *
	 * @return View|RedirectResponse|Factory
	 */
	public function show(int $id): View
	{
		$model = $this->newsRepository->findNewsById($id);
		
		if ($model === null) {
			return $this->responseRedirect(
				'admin.posts.index',
				__('news::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$users = $this->userRepository->listUsers();
		
		$this->setPageTitle(
			__('news::messages.title'),
			__('news::messages.show_subtitle', ['name' => $model->name]),
			__('news::messages.show_leadtext', ['name' => $model->name])
		);
		
		return view('news::news.show', compact('model', 'users'));
	}
	
	/**
	 * Show the form for editing the specified News.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$model = $this->newsRepository->findNewsById($id);
		
		if ($model === null) {
			return $this->responseRedirect(
				'admin.posts.index',
				__('news::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$users = $this->userRepository->listUsers();
		
		$this->setPageTitle(
			__('news::messages.title'),
			__('news::messages.edit_subtitle', ['name' => $model->name]),
			__('news::messages.edit_leadtext', ['name' => $model->name])
		);
		
		return view('news::news.edit', compact('model', 'users'));
	}
	
	/**
	 * Update the specified News in storage.
	 *
	 * @param  int                $id
	 * @param  UpdateNewsRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function update(int $id, UpdateNewsRequest $request): RedirectResponse
	{
		$post = $this->newsRepository->findNewsById($id);
		
		if ($post === null) {
			return $this->responseRedirect(
				'admin.posts.index',
				__('news::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$post = $this->newsRepository->updateNews($request->all(), $id);
		
		if (!$post) {
			return $this->responseRedirectBack(
				__('news::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirectBack(
			__('news::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified News from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 *
	 */
	public function destroy(int $id): RedirectResponse
	{
		$post = $this->newsRepository->findNewsById($id);
		
		if ($post === null) {
			return $this->responseRedirect(
				'admin.posts.index',
				__('news::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$news = $this->newsRepository->deleteNews($id);
		
		if (!$news) {
			return $this->responseRedirectBack(
				__('news::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.posts.index',
			__('news::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
