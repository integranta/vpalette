<?php


namespace Modules\News\Services\API\V1;


use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\News\Contracts\NewsContract;
use Modules\News\Http\Requests\API\V1\CreateNewsAPIRequest;
use Modules\News\Http\Requests\API\V1\UpdateNewsAPIRequest;
use Modules\News\Transformers\NewsResource;

/**
 * Class NewsApiService
 *
 * @package Modules\News\Services\API\V1
 */
class NewsApiService extends BaseService
{
    /** @var  NewsContract */
    private $newsRepository;
    
    
    /**
     * NewsAPIController constructor.
     *
     * @param  NewsContract  $postRepo
     */
    public function __construct(NewsContract $postRepo)
    {
        $this->newsRepository = $postRepo;
    }
    
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $posts = NewsResource::collection($this->newsRepository->listNews());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('news::messages.success_retrieve'),
            $posts
        );
    }
    
    /**
     * @param  CreateNewsAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateNewsAPIRequest $request): JsonResponse
    {
        $post = $this->newsRepository->createNews($request->all());
        
        if (!$post) {
            $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('news::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('news::messages.success_create'),
            $post
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $post = NewsResource::make($this->newsRepository->findNewsById($id));
        
        
        if ($post === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('news::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('news::messages.success_found', ['id' => $id]),
            $post
        );
    }
    
    /**
     * @param  int                   $id
     * @param  UpdateNewsAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateNewsAPIRequest $request): JsonResponse
    {
        $post = $this->newsRepository->findNewsById($id);
        
        if ($post === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('news::messages.not_found', ['id' => $id])
            );
        }
        
        $post = $this->newsRepository->updateNews($request->all(), $id);
        
        if (!$post) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('news::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('news::messages.success_update'),
            $post
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return mixed
     * @throws Exception
     *
     */
    public function destroy(int $id): JsonResponse
    {
        $post = $this->newsRepository->findNewsById($id);
        
        if ($post === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('news::messages.not_found', ['id' => $id])
            );
        }
        
        $post = $this->newsRepository->deleteNews($id);
        
        if ($post) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('news::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('news::messages.success_delete'),
            $post
        );
    }
}
