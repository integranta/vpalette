<?php


namespace Modules\News\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\News\Models\News;

/**
 * Interface NewsContract
 *
 * @package Modules\News\Contracts
 */
interface NewsContract
{
    /**
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listNews(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection;

    /**
     * @param  int  $id
     *
     * @return News
     */
    public function findNewsById(int $id): ?News;

    /**
     * @param  array  $params
     *
     * @return News
     */
    public function createNews(array $params): News;

    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return News
     */
    public function updateNews(array $params, int $id): News;

    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteNews(int $id): bool;
}
