<?php

namespace Modules\News\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Analytics\Transformers\ViewResource;
use Modules\Category\Transformers\CategoryResource;
use Modules\Seo\Transformers\SeoResource;

/**
 * Class NewsResource
 *
 * @package Modules\News\Transformers
 */
class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'sort' => $this->sort,
            'activityEndAt' => $this->activity_end_at,
            'previewText' => $this->preview_text,
            'previewImage' => $this->preview_image,
            'detailText' => $this->detail_text,
            'detailImage' => $this->detail_image,
            'userId' => $this->user_id,
            'isActive' => $this->active,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at,
            'seoFields' => SeoResource::make($this->whenLoaded('seo')),
            'views' => ViewResource::collection($this->whenLoaded('views'))
        ];
    }
}
