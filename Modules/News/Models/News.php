<?php

namespace Modules\News\Models;

use App\Models\BaseModel;
use App\User;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Analytics\Traits\Viewable;
use Modules\Seo\Models\Seo;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Storage;


/**
 * Class News
 *
 * @package Modules\News\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string|null $name
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $activityEndAt
 * @property int $countView
 * @property string|null $previewText
 * @property string|null $previewImage
 * @property string|null $detailText
 * @property string|null $detailImage
 * @property int|null $userId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \App\User $author
 * @property-read \Builder|\Illuminate\Database\Eloquent\Model|object $next
 * @property-read \Builder|\Illuminate\Database\Eloquent\Model|object $prev
 * @property-read \Modules\Seo\Models\Seo|null $seo
 * @property-write mixed $isActive
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Analytics\Models\ViewCount[] $views
 * @property-read int|null $viewsCount
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\News\Models\News onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereActivityEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereCountView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereDetailImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereDetailText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News wherePreviewImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News wherePreviewText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\News\Models\News whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\News\Models\News withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\News\Models\News withoutTrashed()
 * @mixin \Eloquent
 */
class News extends BaseModel
{
    use SoftDeletes;
    use SoftCascadeTrait;
    use Viewable;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'news';

    /**
     * Записи из указанных отношении должны быть каскадно "мягко" удалиться.
     * Удаляються с помощью стороннего пакета Askedio\SoftCascade.
     *
     * @link https://github.com/Askedio/laravel-soft-cascade#usage
     *
     * @return array
     */
    protected $softCascade = ['seo'];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'activity_end_at'
    ];

    /**
     * Формат хранения отметок времени модели.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Отношение, которые всегда должны быть загружены.
     *
     * @var array
     */
    protected $with = ['seo', 'author', 'views'];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'sort',
        'activity_end_at',
        'preview_text',
        'preview_image',
        'detail_text',
        'detail_image',
        'user_id',
        'active'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'preview_text' => 'string',
        'preview_image' => 'string',
        'detail_image' => 'string',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'active' => 'required',
        'name' => 'required|string',
        'sort' => 'required|integer',
        'activity_end_at' => 'nullable',
        'preview_text' => 'nullable|string',
        'preview_image' => 'nullable',
        'detail_text' => 'nullable|string',
        'detail_image' => 'nullable',
        'user_id' => 'integer|exists:users,id'
    ];



    /**
     * Установить флаг активности.
     *
     * @param  bool  $value
     *
     * @return string
     */
    public function setIsActiveAttribute(bool $value): string
    {
        return $this->attributes['active'] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Получить дату в человеко понятном формате.
     *
     * @return string
     */
    public function getHumanDate(): string
    {
        $startDate = Carbon::parse($this->attributes['created_at'])->locale('ru');
        $endDate = $this->attributes['activity_end_at'] !== null
            ? Carbon::parse($this->attributes['activity_end_at'])->locale('ru')
            : null;

        if ($endDate) {
            return 'С '.$startDate->isoFormat('DD').' по '.$endDate->isoFormat('DD MMMM YYYY');
        }

        return $startDate->isoFormat('DD MMMM YYYY');
    }

    /**
     * Получить следующую новость.
     *
     * @return Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getNextAttribute(): object
    {
        $result = self::query()
            ->where('id', '>', $this->id)
            ->orderBy('id', 'asc')
            ->first();

        if ($result) {
            $this->attributes['current'] = 'nullable';
            return $result;
        }

        return self::query()->orderBy('id', 'desc')->first();
    }

    /**
     * Получить предыдущую новость.
     *
     * @return Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getPrevAttribute(): object
    {
        $result = self::query()
            ->where('id', '<', $this->id)
            ->orderBy('id', 'desc')
            ->first();
        if ($result) {
            $this->attributes['current'] = 'null';
            return $result;
        }

        return self::first();
    }

    /**
     * Получить ссылку на картинку для анонса.
     *
     * @param  string|null  $value
     *
     * @return string|null
     */
    public function getPreviewImageAttribute(?string $value): ?string
    {
        return $value !== null
            ? Storage::disk('uploads')->url('uploads/'.$value)
            : null;
    }

    /**
     * Получить ссылку на детальную картинку.
     *
     * @param  string|null  $value
     *
     * @return string|null
     */
    public function getDetailImageAttribute(?string $value): ?string
    {
        return $value !== null ? Storage::disk('uploads')->url('uploads/'.$value) : null;
    }

    public function getCountViewAttribute($value)
    {
        return $this->views()->count('views');
    }

    /**
     * Карта событий для модели.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        //'retrieved' => NewsPostWasVisited::class,
    ];

    /**
     * Получить связанные SEO параметры для новости.
     *
     * @return MorphOne
     */
    public function seo(): MorphOne
    {
        return $this->morphOne(Seo::class, 'seoble');
    }

    /**
     * Получить автора новости.
     *
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
