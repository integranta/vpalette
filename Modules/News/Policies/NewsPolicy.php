<?php

namespace Modules\News\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\News\Models\News;

/**
 * Class NewsPolicy
 *
 * @package Modules\News\Policies
 */
class NewsPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the news.
     *
     * @param  User|null  $user
     * @param  News       $news
     *
     * @return mixed
     */
    public function view(?User $user, News $news)
    {
        if ($news->active) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished news')) {
            return true;
        }
        
        // authors can view their own unpublished news
        return $user->id == $news->user_id;
    }
    
    /**
     * Determine whether the user can create news.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create news')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the news.
     *
     * @param  User  $user
     * @param  News  $news
     *
     * @return mixed
     */
    public function update(User $user, News $news)
    {
        if ($user->can('edit own news')) {
            return $user->id == $news->user_id;
        }
        
        if ($user->can('edit all news')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the news.
     *
     * @param  User  $user
     * @param  News  $news
     *
     * @return mixed
     */
    public function delete(User $user, News $news)
    {
        if ($user->can('delete own news')) {
            return $user->id == $news->user_id;
        }
        
        if ($user->can('delete any news')) {
            return true;
        }
        
        return false;
    }
}
