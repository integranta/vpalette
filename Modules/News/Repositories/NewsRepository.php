<?php

namespace Modules\News\Repositories;

use App\Repositories\BaseRepository;
use App\Traits\UploadAble;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Image;
use Modules\News\Contracts\NewsContract;
use Modules\News\Models\News;
use Storage;
use Str;

/**
 * Class NewsRepository
 *
 * @package Modules\News\Repositories
 * @version August 9, 2019, 9:54 am MSK
 */
class NewsRepository extends BaseRepository implements NewsContract
{
    use UploadAble;

    /**
     * BaseRepository constructor.
     *
     * @param  News  $model
     */
    public function __construct(News $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function listNews(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('news.cache_key').$postfix,
            config('news.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }

    /**
     * @inheritDoc
     */
    public function findNewsById(int $id): ?News
    {
        return $this->find($id);
    }

    /**
     * @inheritDoc
     */
    public function createNews(array $params): News
    {
        $newsData = $this->getNewsData($params);

        $post = $this->create($newsData);
        $post->views()->create([
            'viewable_type' => get_class($post),
            'viewable_id' => $post->id
        ]);
        $post->seo()->create([
            'title' => $params['title'] ?? null,
            'description' => $params['description'] ?? null,
            'keywords' => $params['keywords'] ?? null,
            'canonical_url' => $params['canonical_url'] ?? null,
            'ya_direct' => $params['ya_direct'] ?? null,
            'google_tag_manager' => $params['google_tag_manager'] ?? null,
            'google_analytics' => $params['google_analytics'] ?? null,
            'ya_metrica' => $params['ya_metrica'] ?? null
        ]);

        $post->save();
        return $post;
    }

    /**
     * @inheritDoc
     */
    public function updateNews(array $params, int $id): News
    {
        $newsData = $this->getNewsData($params);

        $this->update($newsData, $id);
        $post = $this->find($id);
        $post->seo()->update([
            'title' => $params['title'] ?? null,
            'description' => $params['description'] ?? null,
            'keywords' => $params['keywords'] ?? null,
            'canonical_url' => $params['canonical_url'] ?? null,
            'ya_direct' => $params['ya_direct'] ?? null,
            'google_tag_manager' => $params['google_tag_manager'] ?? null,
            'google_analytics' => $params['google_analytics'] ?? null,
            'ya_metrica' => $params['ya_metrica'] ?? null
        ]);
        return $post;
    }

    /**
     * @inheritDoc
     */
    public function deleteNews(int $id): bool
    {
        return $this->delete($id);
    }

    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getNewsData(array $params): array
    {
        $newsData = [
            'name' => $params['name'],
            'sort' => $params['sort'],
            'activity_end_at' => $params['activity_end_at'] ?? null,
            'preview_text' => $params['preview_text'] ?? null,
            'detail_text' => $params['detail_text'] ?? null,
            'user_id' => $params['user_id'],
            'active' => $params['active']
        ];

        $pathToUpload = 'news/'.Str::of($params['name'])->lower()->trim()->slug('_', 'ru');

        if (isset($params['preview_image'])) {
            $path = $this->makeCachedImage($params['preview_image'], $pathToUpload . '/preview');

            $newsData['preview_image'] = $path;
        }

        if (isset($params['detail_image'])) {

            $path = $this->makeCachedImage($params['detail_image'], $pathToUpload . '/detail');

            $newsData['detail_image'] = $path;
        }
        return $newsData;
    }
}
