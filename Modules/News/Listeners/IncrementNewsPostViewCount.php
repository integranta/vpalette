<?php

namespace Modules\News\Listeners;

use Carbon\Carbon;
use Modules\News\Events\NewsPostWasVisited;
use Modules\News\Models\News;
use Request;


/**
 * Class IncrementNewsPostViewCount
 *
 * @package Modules\News\Listeners
 */
class IncrementNewsPostViewCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewsPostWasVisited  $event
     *
     * @return bool
     */
    public function handle(NewsPostWasVisited $event): bool
    {
        $post = $event->post;

        $views = $post->views;


        // Если нет ещё просмотров.
        if (count($views) === 0) {
            $this->setNewView($post);
        }

        foreach ($views as $view) {
            if ((int) $view->vieable_id === (int) $post->id && (string) $view->ip === Request::ip()) {
                return false;
            }
        }

        $this->setNewView($post);

        return true;
    }

    /**
     * Create new view on post.
     *
     * @param  News  $post
     */
    private function setNewView(News $post): void
    {
        $post->views()->create([
            'user_id' => null,
            'ip' => Request::ip(),
            'viewable_type' => get_class($post),
            'viewable_id' => $post->id,
            'viewed_at' => Carbon::now(env('APP_TIMEZONE')),
            'views' => 1
        ]);
    }
}
