<?php namespace Modules\News\Tests\Traits;

use App;
use Faker\Factory as Faker;
use Modules\News\Models\News;
use Modules\News\Repositories\NewsRepository;

/**
 * Trait MakeNewsTrait
 *
 * @package Modules\News\Tests\Traits
 */
trait MakeNewsTrait
{
    /**
     * Create fake instance of News and save it in database
     *
     * @param  array  $newsFields
     *
     * @return News
     */
    public function makeNews($newsFields = [])
    {
        /** @var NewsRepository $newsRepo */
        $newsRepo = App::make(NewsRepository::class);
        $theme = $this->fakeNewsData($newsFields);
        return $newsRepo->create($theme);
    }
    
    /**
     * Get fake instance of News
     *
     * @param  array  $newsFields
     *
     * @return News
     */
    public function fakeNews($newsFields = [])
    {
        return new News($this->fakeNewsData($newsFields));
    }
    
    /**
     * Get fake data of News
     *
     * @param  array  $newsFields
     *
     * @return array
     */
    public function fakeNewsData($newsFields = [])
    {
        $fake = Faker::create();
        
        return array_merge([
            'name' => $fake->word,
            'slug' => $fake->word,
            'sort' => $fake->word,
            'activity_end_at' => $fake->word,
            'count_view' => $fake->word,
            'preview_text' => $fake->text,
            'preview_image' => $fake->word,
            'detail_text' => $fake->word,
            'detail_image' => $fake->word,
            'user_id' => $fake->word,
            'active' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $newsFields);
    }
}
