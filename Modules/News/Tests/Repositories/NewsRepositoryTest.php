<?php namespace Modules\News\Tests\Repositories;

use App;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\News\Models\News;
use Modules\News\Repositories\NewsRepository;
use Modules\News\Tests\ApiTestTrait;
use Modules\News\Tests\TestCase;
use Modules\News\Tests\Traits\MakeNewsTrait;

/**
 * Class NewsRepositoryTest
 *
 * @package Modules\News\Tests\Repositories
 */
class NewsRepositoryTest extends TestCase
{
    use MakeNewsTrait, ApiTestTrait, DatabaseTransactions;
    
    /**
     * @var NewsRepository
     */
    protected $newsRepo;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->newsRepo = App::make(NewsRepository::class);
    }
    
    /**
     * @test create
     */
    public function test_create_news()
    {
        $news = $this->fakeNewsData();
        $createdNews = $this->newsRepo->create($news);
        $createdNews = $createdNews->toArray();
        $this->assertArrayHasKey('id', $createdNews);
        $this->assertNotNull($createdNews['id'], 'Created News must have id specified');
        $this->assertNotNull(News::find($createdNews['id']), 'News with given id must be in DB');
        $this->assertModelData($news, $createdNews);
    }
    
    /**
     * @test read
     */
    public function test_read_news()
    {
        $news = $this->makeNews();
        $dbNews = $this->newsRepo->find($news->id);
        $dbNews = $dbNews->toArray();
        $this->assertModelData($news->toArray(), $dbNews);
    }
    
    /**
     * @test update
     */
    public function test_update_news()
    {
        $news = $this->makeNews();
        $fakeNews = $this->fakeNewsData();
        $updatedNews = $this->newsRepo->update($fakeNews, $news->id);
        $this->assertModelData($fakeNews, $updatedNews->toArray());
        $dbNews = $this->newsRepo->find($news->id);
        $this->assertModelData($fakeNews, $dbNews->toArray());
    }
    
    /**
     * @test delete
     */
    public function test_delete_news()
    {
        $news = $this->makeNews();
        $resp = $this->newsRepo->delete($news->id);
        $this->assertTrue($resp);
        $this->assertNull(News::find($news->id), 'News should not exist in DB');
    }
}
