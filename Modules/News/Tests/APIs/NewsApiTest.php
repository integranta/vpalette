<?php namespace Modules\News\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\News\Tests\ApiTestTrait;
use Modules\News\Tests\TestCase;
use Modules\News\Tests\Traits\MakeNewsTrait;

/**
 * Class NewsApiTest
 *
 * @package Modules\News\Tests\APIs
 */
class NewsApiTest extends TestCase
{
    use MakeNewsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_news()
    {
        $news = $this->fakeNewsData();
        $this->response = $this->json('POST', '/api/news', $news);
        
        $this->assertApiResponse($news);
    }
    
    /**
     * @test
     */
    public function test_read_news()
    {
        $news = $this->makeNews();
        $this->response = $this->json('GET', '/api/news/'.$news->id);
        
        $this->assertApiResponse($news->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_news()
    {
        $news = $this->makeNews();
        $editedNews = $this->fakeNewsData();
        
        $this->response = $this->json('PUT', '/api/news/'.$news->id, $editedNews);
        
        $this->assertApiResponse($editedNews);
    }
    
    /**
     * @test
     */
    public function test_delete_news()
    {
        $news = $this->makeNews();
        $this->response = $this->json('DELETE', '/api/news/'.$news->id);
        
        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/news/'.$news->id);
        
        $this->response->assertStatus(404);
    }
}
