<?php

namespace Modules\News\Http\Controllers;

use App\DataTables\PostsDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\News\Http\Requests\CreateNewsRequest;
use Modules\News\Http\Requests\UpdateNewsRequest;
use Modules\News\Services\NewsWebService;


/**
 * Class NewsController
 *
 * @package Modules\News\Http\Controllers
 */
class NewsController extends Controller
{
    /*** @var NewsWebService */
    private $newsWebService;

    /**
     * NewsController constructor.
     *
     * @param  NewsWebService  $service
     *
     */
    public function __construct(NewsWebService $service)
    {
        $this->newsWebService = $service;

        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the News.
	 *
	 * @param  \App\DataTables\PostsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(PostsDataTable $dataTable)
    {
        return $this->newsWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new News.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->newsWebService->create();
    }

    /**
     * Store a newly created News in storage.
     *
     * @param  CreateNewsRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateNewsRequest $request): RedirectResponse
    {
        return $this->newsWebService->store($request);
    }

    /**
     * Display the specified News.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->newsWebService->show($id);
    }

    /**
     * Show the form for editing the specified News.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->newsWebService->edit($id);
    }

    /**
     * Update the specified News in storage.
     *
     * @param  int                $id
     * @param  UpdateNewsRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdateNewsRequest $request): RedirectResponse
    {
        return $this->newsWebService->update($id, $request);
    }

    /**
     * Remove the specified News from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->newsWebService->destroy($id);
    }
}
