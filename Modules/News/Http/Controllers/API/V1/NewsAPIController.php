<?php

namespace Modules\News\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Modules\News\Http\Requests\API\V1\CreateNewsAPIRequest;
use Modules\News\Http\Requests\API\V1\UpdateNewsAPIRequest;
use Modules\News\Services\API\V1\NewsApiService;

/**
 * Class NewsAPIController
 *
 * @package Modules\News\Http\Controllers\API\V1
 */
class NewsAPIController extends Controller
{
    /** @var NewsApiService */
    private $newsApiService;

    /**
     * NewsAPIController constructor.
     *
     * @param  NewsApiService  $service
     */
    public function __construct(NewsApiService $service)
    {
        $this->newsApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->newsApiService->index();
    }

    /**
     * @param  CreateNewsAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateNewsAPIRequest $request): JsonResponse
    {
        return $this->newsApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->newsApiService->show($id);
    }

    /**
     * @param  int                   $id
     * @param  UpdateNewsAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateNewsAPIRequest $request): JsonResponse
    {
        return $this->newsApiService->update($id, $request);
    }

    /**
     * @param  int  $id
     *
     * @return mixed
     * @throws Exception
     *
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->newsApiService->destroy($id);
    }
}
