<?php

namespace Modules\News\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\News\Models\News;

/**
 * Class UpdateNewsRequest
 *
 * @package Modules\News\Http\Requests
 */
class UpdateNewsRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return News::$rules;
    }
}
