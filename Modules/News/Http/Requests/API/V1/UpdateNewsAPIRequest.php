<?php

namespace Modules\News\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\News\Models\News;

/**
 * Class UpdateNewsAPIRequest
 *
 * @package Modules\News\Http\Requests\API\V1
 */
class UpdateNewsAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return News::$rules;
    }
}
