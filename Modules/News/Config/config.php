<?php

return [
    'name' => 'News',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all news as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('NEWS_CACHE_KEY','index_news'),
    'cache_ttl' => env('NEWS_CACHE_TTL',360000),
];
