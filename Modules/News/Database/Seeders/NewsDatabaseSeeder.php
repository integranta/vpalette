<?php

namespace Modules\News\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Analytics\Models\ViewCount;
use Modules\Category\Models\Category;
use Modules\News\Models\News;

/**
 * Class NewsDatabaseSeeder
 *
 * @package Modules\News\Database\Seeders
 */
class NewsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(News::class, 20)->create();
    }
}
