<?php

/** @var Factory $factory */

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\News\Models\News;
use Illuminate\Http\File;

$factory->define(News::class,
    function (Faker $faker) {
        
        $name = $faker->sentence(2, false);
        $path = 'news/'.Str::of($name)->lower()->trim()->slug('_', 'ru');
        
        $imageFile = 'public/storage/uploads/faker/news/news-img.jpg';
        
        return [
            'active' => true,
            'name' => $name,
            'sort' => $faker->numberBetween(1, 500),
            'activity_end_at' => $faker->numberBetween(0, 1) === 1 ? Carbon::now('Europe/Samara')->addMonth() : null,
            'preview_text' => $faker->realText(150),
            'preview_image' => Storage::disk('uploads')->putFile($path, $imageFile),
            'detail_image' => Storage::disk('uploads')->putFile($path, $imageFile),
            'detail_text' => $faker->realText(1500),
            'user_id' => $faker->numberBetween(1, 3)
        ];
    });
