<?php

return [
	'tabs'                   => [
		'element' => 'Элемент',
		'preview' => 'Описание',
		'detail'  => 'Детальная',
		'seo'     => 'SEO'
	],
	'fields'                 => [
		'author_label'                => 'Автор:',
		'is_active_label'             => 'Активность:',
		'activity_end_at_label'       => 'Окончание активности:',
		'activity_end_at_placeholder' => 'Укажите окончание активности:',
		'name_label'                  => 'Название:',
		'sort_label'                  => 'Сортировка:',
		'categories_label'            => 'Категория:',
		'preview_image_label'         => 'Картинка для анонса:',
		'preview_text_label'          => 'Текст для анонса:',
		'detail_image_label'          => 'Детальная картинка:',
		'detail_text_label'           => 'Детальный текст:',
	],
	'create_button'          => 'Создать',
	'update_button'          => 'Обновить',
	'cancel_button'          => 'Отменить',
	'select_parent_user'     => 'Выберите автора',
	'select_parent_category' => 'Выберите родительскую категорию',
	'table'                  => [
		'id'              => 'ID',
		'name'            => 'Название',
		'slug'            => 'Символ.код',
		'sort'            => 'Сортировка',
		'activity_end_at' => 'Дата окончания',
		'preview_text'    => 'Текст анонса',
		'preview_image'   => 'Картинка анонса',
		'detail_text'     => 'Детальный текст',
		'detail_image'    => 'Детальная картинка',
		'user_id'         => 'Автор',
		'active'          => 'Активность',
		'action'          => 'Действия'
	]
];
