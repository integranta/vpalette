<div class="tab-content tab-bordered" id="myTab3Content">
    @csrf
    @include('news::includes.tabs.element')
    @include('news::includes.tabs.preview')
    @include('news::includes.tabs.detail')
    @include('news::includes.tabs.seo')
</div>
