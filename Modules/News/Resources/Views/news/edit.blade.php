@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/news.css') }}">
@endsection

@section('content')
    <section class="section">
        @include('news::includes.header')
        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')
                    {!! Form::model($model, ['route' => ['admin.posts.update', $model->id], 'method' => 'patch']) !!}
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            @include('news::includes.tabs')
                            @include('news::news.edit_fields')
                        </div>
                        @include('news::includes.footer')
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ asset('js/news.js') }}"></script>
@endpush
