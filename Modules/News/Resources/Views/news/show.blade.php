@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{--  CSS Libraies  --}}
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/css/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">

    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/news.css') }}">

@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.posts.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')

                    {!! Form::model($model,
                        [
                            'route' => ['admin.posts.show', $model->id],
                            'method' => 'get',
                            'files' => 'true'
                        ])
                    !!}
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            @include('news::includes.tabs')
                            @include('news::news.show_fields')
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    {{-- JS Libraies --}}
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('libs/js/tinymce/langs/ru.js') }}"></script>
    <script src="{{ asset('libs/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('libs/js/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>

    {{--  Module JS File  --}}
    <script>
        $(document).ready(function () {
            tinymce.init({
                readonly: 1,
                selector: ".tinyMCE"
            });
        });

    </script>
@endpush
