<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">

        <div class="form-group row col-md-12">
            <label for="user_id" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                {{ __('news::templates.fields.author_label') }}
            </label>

            <div class="col-sm-12 col-md-7">
                <select
                    class="form-control custom-select mt-15 @error('user_id') is-invalid @enderror"
                    name="user_id"
                    id="user_id"
                >
                    <option disabled>{{ __('news::templates.select_parent_user') }}</option>
                    @foreach($users as $user)
                        @if ($user->id === $model->user_id)
                            <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                        @else
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <!-- Active field -->
        <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                {{ __('news::templates.fields.is_active_label') }}
            </span>
            <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
                {!! Form::hidden('active', 0) !!}
                {!!
                    Form::checkbox(
                        'active',
                        true,
                        $model->active === true,
                        ['class' => 'custom-control-input', 'id' => 'active']
                    )
                !!}
                <label class="custom-control-label ml-lg-3" for="active"></label>
            </div>
        </div>

        <!-- Activity End at Field -->
        <div class="form-group row col-md-12">
            <label for="activity_end_at" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                {{ __('news::templates.fields.activity_end_at_label') }}
            </label>
            <div class="col-sm-12 col-md-7">
                <input
                    type="text"
                    name="activity_end_at"
                    id="activity_end_at"
                    class="form-control"
                    value="{{ $model->activity_end_at }}"
                    placeholder="{{ __('news::templates.fields.activity_end_at_placeholder') }}"
                >
            </div>
        </div>
        <!-- Name Field -->
        <div class="form-group row col-md-12">
            <label for="name" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                {{ __('news::templates.fields.name_label') }}
            </label>
            <div class="col-sm-12 col-md-7">
                {!! Form::text('name', $model->name, ['class' => 'form-control']) !!}
            </div>
        </div>

        <!-- Sort Field -->
        <div class="form-group row col-md-12">
            <label for="sort" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                {{ __('news::templates.fields.sort_label') }}
            </label>
            <div class="col-sm-12 col-md-7">
                {!! Form::text('sort', $model->sort, ['class' => 'form-control']) !!}
            </div>
        </div>

    </div>

    <div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <!-- Preview Image Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'preview_image',
                    'Картинка для анонса:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                @if(!empty($model->preview_image))
                    <img src="{{ $model->preview_image }}" alt="{{ $model->name }}" class="img-fluid">
                @endif
                {!! Form::file('preview_image', ['class' => 'form-control-file']) !!}
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Preview Text Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'preview_text',
                    'Текст для анонса:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea(
                        'preview_text',
                        $model->preview_text,
                        ['class' => 'form-control tinyMCE', 'id' => 'preview_text']
                    )
                !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
        <!-- Detail Image Field -->
        <div class="form-group row col-md-12">
            {!!
                Form::label(
                    'detail_image',
                    'Детальная картинка:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                @if(!empty($model->detail_image))
                    <img src="{{ $model->detail_image }}" alt="{{ $model->name }}" class="img-fluid">
                @endif
                {!! Form::file('detail_image') !!}
            </div>
        </div>
        <div class="clearfix"></div>

        <!-- Detail Text Field -->
        <div class="form-group row col-sm-12 col-lg-12">
            {!!
                Form::label(
                    'detail_text',
                    'Полное описание:',
                    ['class' => 'col-form-label text-md-right col-12 col-md-3 col-lg-3']
                )
            !!}
            <div class="col-sm-12 col-md-7">
                {!!
                    Form::textarea(
                        'detail_text',
                        $model->detail_text,
                        ['class' => 'form-control tinyMCE']
                    )
                !!}
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
        <!-- Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'title',
                    'Meta title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'title',
                        old('title', $model->seo->title ?? null),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>

        <!-- Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'description',
                    'Meta Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'description',
                        old('description', $model->seo->description ?? null),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- Keywords Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'keywords',
                    'Keywords:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'keywords',
                        old('keywords', $model->seo->keywords ?? null),
                        ['class' => 'form-control codeeditor', 'id' => 'keywords']
                    )
                !!}
            </div>
        </div>
        <!-- Canonical URL Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'canonical_url',
                    'Canonical URL:',
                    [
                        'class' => 'form-control-label col-sm-3 text-md-right',
                        'id' => 'canonical_url'
                    ]
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'canonical_url',
                        old('canonical_url', $model->seo->canonical_url ?? null),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>

        <!-- Og Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_title',
                    'Open Graph Title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_title',
                        old('og_title', $model->seo->og_title),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End title Field -->

        <!-- Og Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_description',
                    'Og Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'og_description',
                        old('og_description', $model->seo->og_description),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End description Field -->


        <!-- Og Image Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_image',
                    'Open Graph Image:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::file(
                        'og_image',
                        old('og_image', $model->seo->og_image),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End og image Field -->


        <!-- Og type Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_type',
                    'Open Graph type:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_type',
                        old('og_type', $model->seo->og_type),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End og type Field -->


        <!-- Og url Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_url',
                    'Open Graph url:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_url',
                        old('og_url', $model->seo->og_url),
                        ['class' => 'form-control']
                    )
                !!}
            </div>
        </div>
        <!-- End og url Field -->

        <div class="card-header">
            <h4>Реклама</h4>
        </div>

        <div class="form-group row">
            {!!
                Form::label(
                    'ya_direct',
                    'Яндекс.Директ:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_direct',
                        old('ya_direct', $model->seo->ya_direct ?? null),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_direct'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="card-header">
            <h4>Счётчики:</h4>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'google_tag_manager',
                    'Google Tag Manager:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_tag_manager',
                        old('google_tag_manager', $model->seo->google_tag_manager ?? null),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_tag_manager'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'google_analytics',
                    'Google Analytics:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_analytics',
                        old('google_anlytics', $model->seo->google_analytics ?? null),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_analytics'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'ya_metrica',
                    'Яндекс.Метрика:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_metrica',
                        old('ya_metrica', $model->seo->ya_metrica ?? null),
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_metrica'
                        ]
                    )
                !!}
            </div>
        </div>
    </div>
</div>
