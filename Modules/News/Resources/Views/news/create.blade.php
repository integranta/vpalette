@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/news.css') }}">
@endsection

@section('content')
    <section class="section">
        @include('news::includes.header')
        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')
                    <form
                        method="POST"
                        action="{{ route('admin.posts.store') }}"
                        enctype="multipart/form-data"
                        name="postForm"
                    >
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ $subTitle }}</h4>
                            </div>
                            <div class="card-body">
                                @include('news::includes.tabs')
                                @include('news::news.fields')
                            </div>
                            @include('news::includes.footer')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ asset('js/news.js') }}"></script>
@endpush
