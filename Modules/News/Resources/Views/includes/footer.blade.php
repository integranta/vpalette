<div class="card-footer">
    <!-- Submit Field -->
    <div class="form-group row col-mb-12">
        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
        <div class="col-sm-4 col-md-4">
            <button
                class="btn btn-primary"
                type="submit"
            >
                @if (route('admin.posts.create') === url()->current())
                    {{ __('news::template.create_button') }}
                @else
                    {{ __('news::template.update_button') }}
                @endif
            </button>
        </div>
    </div>
</div>
