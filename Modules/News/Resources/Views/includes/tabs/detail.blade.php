<div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
    <!-- Detail Image Field -->
    <div class="form-group row col-md-12">
        <label for="detail_image" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.detail_image_label') }}
        </label>
        <div class="col-sm-12 col-md-7">
            {!! Form::file('detail_image') !!}
        </div>
    </div>
    <div class="clearfix"></div>

    <!-- Detail Text Field -->
    <div class="form-group row col-sm-12 col-lg-12">
        <label for="detail_text" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.detail_text_label') }}
        </label>
        <div class="col-sm-12 col-md-7">
            {!!
                Form::textarea(
                    'detail_text',
                    $model->detail_text,
                    ['class' => 'form-control tinyMCE']
                )
            !!}
        </div>
    </div>
</div>
