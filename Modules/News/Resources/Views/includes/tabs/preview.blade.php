<div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
    <!-- Preview Image Field -->
    <div class="form-group row col-md-12">
        <label for="preview_image" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.preview_image_label') }}
        </label>
        <div class="col-sm-12 col-md-7">
            {!! Form::file('preview_image', ['class' => 'form-control-file']) !!}
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Preview Text Field -->
    <div class="form-group row col-sm-12 col-lg-12">
        <label for="preview_text" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.preview_text_label') }}
        </label>
        <div class="col-sm-12 col-md-7">
            {!!
                Form::textarea(
                    'preview_text',
                    $model->preview_text,
                    ['class' => 'form-control tinyMCE', 'id' => 'preview_text']
                )
            !!}
        </div>
    </div>
</div>
