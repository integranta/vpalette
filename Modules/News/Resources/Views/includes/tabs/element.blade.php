<div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">

    <div class="form-group row col-md-12">
        <label for="user_id" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.author_label') }}
        </label>

        <div class="col-sm-12 col-md-7">
            <select
                class="form-control custom-select mt-15 @error('user_id') is-invalid @enderror"
                name="user_id"
                id="user_id"
            >
                <option disabled>{{ __('news::templates.select_parent_user') }}</option>
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!-- Active field -->
    <div class="form-group row col-md-12">
            <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
                {{ __('news::templates.fields.is_active_label') }}
            </span>
        <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
            {!! Form::hidden('active', 0) !!}
            {!!
                Form::checkbox(
                    'active',
                    true,
                    $model->active === 1,
                    ['class' => 'custom-control-input', 'id' => 'active']
                )
            !!}
            <label class="custom-control-label ml-lg-3" for="active"></label>
        </div>
    </div>

    <!-- Activity End at Field -->
    <div class="form-group row col-md-12">
        <label for="activity_end_at" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.activity_end_at_label') }}
        </label>
        <div class="col-sm-12 col-md-7">
            <input
                type="text"
                name="activity_end_at"
                id="activity_end_at"
                class="form-control flatpickr flatpickr-input"
                placeholder="{{ __('news::templates.fields.activity_end_at_placeholder') }}"
                value="{{ $model->activity_end_at ?? null }}"
            >
        </div>
    </div>
    <!-- Name Field -->
    <div class="form-group row col-md-12">
        <label for="name" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.name_label') }}
        </label>
        <div class="col-sm-12 col-md-7">
            {!! Form::text('name', $model->name, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Sort Field -->
    <div class="form-group row col-md-12">
        <label for="sort" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
            {{ __('news::templates.fields.sort_label') }}
        </label>
        <div class="col-sm-12 col-md-7">
            {!! Form::text('sort', $model->sort, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
