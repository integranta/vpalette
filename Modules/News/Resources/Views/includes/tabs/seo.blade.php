<div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">

    <!-- Meta Title Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'title',
                'Meta title:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::text(
                    'title',
                    old('title', $seoInfo->title ?? ''),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>

    <!-- Description Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'description',
                'Meta Description:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::textarea(
                    'description',
                    old('description', $seoInfo->description ?? ''),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>
    <!-- Keywords Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'keywords',
                'Keywords:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::textarea(
                    'keywords',
                    old('keywords', $seoInfo->keywords ?? ''),
                    ['class' => 'form-control codeeditor', 'id' => 'keywords']
                )
            !!}
        </div>
    </div>
    <!-- Canonical URL Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'canonical_url',
                'Canonical URL:',
                [
                    'class' => 'form-control-label col-sm-3 text-md-right',
                    'id' => 'canonical_url'
                ]
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::text(
                    'canonical_url',
                    old('canonical_url', $seoInfo->canonical_url ?? ''),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>

    <!-- Og Title Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'og_title',
                'Open Graph Title:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::text(
                    'og_title',
                    old('og_title', $seoInfo->og_title ?? ''),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>
    <!-- End title Field -->

    <!-- Og Description Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'og_description',
                'Og Description:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::textarea(
                    'og_description',
                    old('og_description', $seoInfo->og_description ?? ''),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>
    <!-- End description Field -->


    <!-- Og Image Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'og_image',
                'Open Graph Image:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::file(
                    'og_image',
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>
    <!-- End og image Field -->


    <!-- Og type Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'og_type',
                'Open Graph type:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::text(
                    'og_type',
                    old('og_type', $seoInfo->og_type ?? ''),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>
    <!-- End og type Field -->


    <!-- Og url Field -->
    <div class="form-group row align-items-center">
        {!!
            Form::label(
                'og_url',
                'Open Graph url:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::text(
                    'og_url',
                    old('og_url', $seoInfo->og_url ?? ''),
                    ['class' => 'form-control']
                )
            !!}
        </div>
    </div>
    <!-- End og url Field -->

    <div class="card-header">
        <h4>Реклама</h4>
    </div>

    <div class="form-group row">
        {!!
            Form::label(
                'ya_direct',
                'Яндекс.Директ:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::textarea(
                    'ya_direct',
                    old('ya_direct', $seoInfo->ya_direct ?? ''),
                    [
                        'class' => 'form-control codeeditor',
                        'id' => 'ya_direct'
                    ]
                )
            !!}
        </div>
    </div>
    <div class="card-header">
        <h4>Счётчики:</h4>
    </div>
    <div class="form-group row">
        {!!
            Form::label(
                'google_tag_manager',
                'Google Tag Manager:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::textarea(
                    'google_tag_manager',
                    old('google_tag_manager', $seoInfo->google_tag_manager ?? ''),
                    [
                        'class' => 'form-control codeeditor',
                        'id' => 'google_tag_manager'
                    ]
                )
            !!}
        </div>
    </div>
    <div class="form-group row">
        {!!
            Form::label(
                'google_analytics',
                'Google Analytics:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::textarea(
                    'google_analytics',
                    old('google_anlytics', $seoInfo->google_analytics ?? ''),
                    [
                        'class' => 'form-control codeeditor',
                        'id' => 'google_analytics'
                    ]
                )
            !!}
        </div>
    </div>
    <div class="form-group row">
        {!!
            Form::label(
                'ya_metrica',
                'Яндекс.Метрика:',
                ['class' => 'form-control-label col-sm-3 text-md-right']
            )
        !!}
        <div class="col-sm-6 col-md-9">
            {!!
                Form::textarea(
                    'ya_metrica',
                    old('ya_metrica', $seoInfo->ya_metrica ?? ''),
                    [
                        'class' => 'form-control codeeditor',
                        'id' => 'ya_metrica'
                    ]
                )
            !!}
        </div>
    </div>
</div>
