<div class="section-header">
    <div class="section-header-back">
        <a href="{{ route('admin.posts.index') }}" class="btn btn-icon">
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>
    <h1>{{ $pageTitle }}</h1>
</div>
