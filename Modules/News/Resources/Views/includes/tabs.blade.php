<ul class="nav nav-tabs" id="myTab2" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="element-tab" data-toggle="tab" href="#element"
           role="tab"
           aria-controls="element" aria-selected="true">{{ __('news::templates.tabs.element') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="preview-tab" data-toggle="tab" href="#preview" role="tab"
           aria-controls="description" aria-selected="false">{{ __('news::templates.tabs.preview') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab"
           aria-controls="detail" aria-selected="false">{{ __('news::templates.tabs.detail') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab"
           aria-controls="seo" aria-selected="false">{{ __('news::templates.tabs.seo') }}</a>
    </li>
</ul>
