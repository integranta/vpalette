import flatpickr from 'flatpickr/dist/flatpickr.min';
import {Russian} from "flatpickr/dist/l10n/ru";
import confirmDatePlugin from "flatpickr/dist/plugins/confirmDate/confirmDate";

$(document).ready(function () {
    flatpickr("#activity_end_at", {
        wrap: false,
        altInput: false,
        enableTime: true,
        time_24hr: true,
        locale: Russian,
        theme: 'dark',
        plugins: [new confirmDatePlugin({
            confirmText: "OK",
            theme: 'dark',
            showAlways: true
        })]
    });
});
