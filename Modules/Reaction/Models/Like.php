<?php

namespace Modules\Reaction\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Like
 *
 * @package Modules\Reaction\Models
 * @property int $id
 * @property string $likeableType
 * @property int $likeableId
 * @property int $userId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $likeable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like whereLikeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like whereLikeableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Like whereUserId($value)
 * @mixin \Eloquent
 */
class Like extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'likeable_likes';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'likeable_id',
        'likeable_type',
        'user_id'
    ];

    /**
     * Получить все модели, владеющие likeable.
     *
     * @return MorphTo
     */
    public function likeable(): MorphTo
    {
        return $this->morphTo();
    }
}
