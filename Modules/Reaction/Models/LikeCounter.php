<?php

namespace Modules\Reaction\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Rennokki\QueryCache\Traits\QueryCacheable;
use RuntimeException;

/**
 * Class LikeCounter
 *
 * @package Modules\Reaction\Models
 * @property int $id
 * @property string $likeableType
 * @property int $likeableId
 * @property int $count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $likeable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\LikeCounter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\LikeCounter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\LikeCounter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\LikeCounter whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\LikeCounter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\LikeCounter whereLikeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\LikeCounter whereLikeableType($value)
 * @mixin \Eloquent
 */
class LikeCounter extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'likeable_like_counters';

    /**
     * Определяет необходимость отметок времени для модели.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'likeable_id',
        'likeable_type',
        'count'
    ];

    /**
     * Получить все модели, владеющие likeable.
     *
     * @return MorphTo
     */
    public function likeable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @param  Model  $modelClass
     */
    public static function rebuild(Model $modelClass): void
    {
        if ($modelClass === null) {
            throw new RuntimeException('$modelClass cannot be empty/null. Maybe set the $morphClass variable on your model.');
        }

        $builder = Like::query()
            ->select(DB::raw('count(*) as count, likeable_type, likeable_id'))
            ->where('likeable_type', $modelClass)
            ->groupBy('likeable_id');

        $results = $builder->get();

        $inserts = $results->toArray();

        DB::table((new static)->table)->insert($inserts);
    }
}
