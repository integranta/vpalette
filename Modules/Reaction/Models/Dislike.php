<?php

namespace Modules\Reaction\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Dislike
 *
 * @package Modules\Reaction\Models
 * @property int $id
 * @property string $dislikeableType
 * @property int $dislikeableId
 * @property int $userId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $dislikeable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike whereDislikeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike whereDislikeableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\Dislike whereUserId($value)
 * @mixin \Eloquent
 */
class Dislike extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'dislikeable_dislikes';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'dislikeable_id',
        'dislikeable_type',
        'user_id'
    ];

    /**
     * Получить все модели, владеющие dislikeable.
     *
     * @return MorphTo
     */
    public function dislikeable(): MorphTo
    {
        return $this->morphTo();
    }
}
