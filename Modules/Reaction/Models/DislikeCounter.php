<?php

namespace Modules\Reaction\Models;

use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Rennokki\QueryCache\Traits\QueryCacheable;
use RuntimeException;

/**
 * Class DislikeCounter
 *
 * @package Modules\Reaction\Models
 * @property int $id
 * @property string $dislikeableType
 * @property int $dislikeableId
 * @property int $count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $dislikeable
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\DislikeCounter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\DislikeCounter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\DislikeCounter query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\DislikeCounter whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\DislikeCounter whereDislikeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\DislikeCounter whereDislikeableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Reaction\Models\DislikeCounter whereId($value)
 * @mixin \Eloquent
 */
class DislikeCounter extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'dislikeable_dislike_counters';

    /**
     * Определяет необходимость отметок времени для модели.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'dislikeable_id',
        'dislikeable_type',
        'count'
    ];

    /**
     * Получить все модели, владеющие dislikeable.
     *
     * @return MorphTo
     */
    public function dislikeable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @param  Model  $modelClass
     *
     * @throws Exception
     */
    public static function rebuild(Model $modelClass): void
    {
        if ($modelClass === null) {
            throw new RuntimeException('$modelClass cannot be empty/null. Maybe set the $morphClass variable on your model.');
        }

        $builder = Dislike::query()
            ->select(DB::raw('count(*) as count, dislikeable_type, dislikeable_id'))
            ->where('dislikeable_type', $modelClass)
            ->groupBy('dislikeable_id');

        $results = $builder->get();

        $inserts = $results->toArray();

        DB::table((new static)->table)->insert($inserts);
    }
}
