<?php


namespace Modules\Reaction\Traits;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Modules\Reaction\Models\Like;
use Modules\Reaction\Models\LikeCounter;

/**
 * Trait Likeable
 *
 *
 * @package Modules\Reaction\Traits
 */
trait Likeable
{
    /** @var bool */
    public static $removeLikesOnDelete = false;
    
    /**
     * Handle the like "deleting" event
     *
     * @return void
     */
    public static function bootLikeable(): void
    {
        if (static::removeLikesOnDelete()) {
            static::deleting(static function ($model) {
                /** @var Likeable $model */
                $model->removeLikes();
            });
        }
    }
    
    /**
     * Populate the $model->likes attribute
     *
     * @return int
     */
    public function getLikeCountAttribute(): int
    {
        return $this->likeCounter() ? $this->likeCounter()->count() : 0;
    }
    
    /**
     * Add a like for this record by the given user.
     *
     * @param  int|null  $userId  - If null will use currently logged in user.
     *
     * @return void
     * @throws Exception
     *
     */
    public function like(int $userId = null): void
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        if ($userId) {
            if ($this->dislikes()->whereUserId($userId)->exists()) {
                $this->undislike($userId);
            } else {
                $like = $this->likes()
                    ->whereUserId($userId)
                    ->first();
                
                if ($like) {
                    return;
                }
            }
            
            $like = new Like();
            $like->user_id = $userId;
            $this->likes()->save($like);
        }
        $this->incrementLikeCount();
    }
    
    /**
     * Remove a like from this record for the given user.
     *
     * @param  int|null  $userId  - If null will use currently logged in user.
     *
     * @return void
     *
     * @throws Exception
     *
     */
    public function unlike(int $userId = null): void
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        if ($userId) {
            $like = $this->likes()
                ->whereUserId($userId)
                ->first();
            
            if (!$like) {
                return;
            }
            
            $like->delete();
        }
        
        $this->decrementLikeCount();
    }
    
    /**
     * Has the currently logged in user already "liked" the current object
     *
     * @param  int|null  $userId
     *
     * @return bool
     */
    public function liked(int $userId = null): bool
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        return (bool) $this->likes()
            ->whereUserId($userId)
            ->count();
    }
    
    /**
     * Should remove likes on model row delete (defaults to true)
     *
     * @return bool
     */
    public static function removeLikesOnDelete(): bool
    {
        return static::$removeLikesOnDelete ?? true;
    }
    
    /**
     * Delete likes related to the current record
     *
     * @return void
     */
    public function removeLikes(): void
    {
        $this->likes()->delete();
        $this->likeCounter()->delete();
    }
    
    /**
     * Collection of the likes on this record
     *
     * @return MorphMany
     */
    public function likes(): MorphMany
    {
        return $this->morphMany(Like::class, 'likeable');
    }
    
    /**
     * Did the currently logged in user like this model
     * Example : if($book->liked) { }
     *
     * @return bool
     */
    public function getLikedAttribute(): bool
    {
        return $this->liked();
    }
    
    /**
     * Counter is a record that stores the total likes for the
     * morphed record
     *
     * @return MorphOne
     */
    public function likeCounter(): MorphOne
    {
        return $this->morphOne(LikeCounter::class, 'likeable');
    }
    
    /**
     * Increment the total like count stored in the counter
     *
     * @return void
     */
    private function incrementLikeCount(): void
    {
        $counter = $this->likeCounter()->first();
        
        if ($counter) {
            $counter->increment('count');
            $counter->save();
        } else {
            $counter = new LikeCounter;
            $counter->count = 1;
            $this->likeCounter()->save($counter);
        }
    }
    
    /**
     * Decrement the total like count stored in the counter
     *
     * @return void
     * @throws Exception
     *
     */
    private function decrementLikeCount(): void
    {
        $counter = $this->likeCounter()->first();
        
        if ($counter) {
            $counter->decrement('count');
            if ($counter->count) {
                $counter->save();
            } else {
                $counter->delete();
            }
        }
    }
    
    
    /**
     * Fetch records that are liked by a given user.
     * Ex: Book::whereLikedBy(123)->get();
     *
     * @param  Builder   $query
     * @param  int|null  $userId
     *
     * @return Builder
     */
    public function scopeWhereLikedBy(Builder $query, int $userId = null)
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        return $query->whereHas('likes', static function ($q) use ($userId) {
            $q->where('user_id', '=', $userId);
        });
    }
}
