<?php


namespace Modules\Reaction\Traits;

use Exception;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Builder;
use Modules\Reaction\Models\Dislike;
use Modules\Reaction\Models\DislikeCounter;

/**
 * Trait Dislikeable
 *
 * @package Modules\Reaction\Traits
 */
trait Dislikeable
{
    /** @var bool */
    public static $removeDislikesOnDelete = false;
    
    /**
     * Handle the dislike "deleting" event
     *
     * @return void
     */
    public static function bootDislikeable(): void
    {
        if (static::removeDislikesOnDelete()) {
            static::deleting(static function ($model) {
                /** @var Dislikeable $model */
                $model->removeDislikes();
            });
        }
    }
    
    /**
     * Populate the $model->likes attribute
     *
     * @return int
     */
    public function getDislikeCountAttribute(): int
    {
        return $this->dislikeCounter() ? $this->dislikeCounter()->count() : 0;
    }
    
    /**
     * Add a dislike for this record by the given user.
     *
     * @param  int|null  $userId  - If null will use currently logged in user.
     *
     * @return void
     */
    public function dislike(int $userId = null): void
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        if ($userId) {
            if ($likes = $this->likes()->whereUserId($userId)->exists()) {
                $this->unlike($userId);
            } else {
                $dislike = $this->dislikes()
                    ->whereUserId($userId)
                    ->first();
                
                if ($dislike) {
                    return;
                }
            }
            
            $dislike = new Dislike();
            $dislike->user_id = $userId;
            $this->dislikes()->save($dislike);
        }
        $this->incrementDislikeCount();
    }
    
    /**
     * Remove a dislike from this record for the given user.
     *
     * @param  int|null  $userId
     *
     * @return void
     * @throws Exception
     *
     */
    public function undislike(int $userId = null): void
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        if ($userId) {
            $dislike = $this->dislikes()
                ->whereUserId($userId)
                ->first();
            
            if (!$dislike) {
                return;
            }
            
            $dislike->delete();
        }
        
        $this->decrementDislikeCount();
    }
    
    /**
     * Has the currently logged in user already "disliked" the current object
     *
     * @param  int|null  $userId
     *
     * @return bool
     */
    public function disliked(int $userId = null): bool
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        return (bool) $this->dislikes()
            ->whereUserId($userId)
            ->count();
    }
    
    /**
     * Should remove dislikes on model row delete (defaults to true)
     *
     * @return bool
     */
    public static function removeDislikesOnDelete(): bool
    {
        return static::$removeDislikesOnDelete ?? true;
    }
    
    /**
     * Delete likes related to the current record
     *
     * @return void
     */
    public function removeDislikes(): void
    {
        $this->dislikes()->delete();
        $this->dislikeCounter()->delete();
    }
    
    
    /**
     * Collection of the likes on this record
     *
     * @return MorphMany
     */
    public function dislikes(): MorphMany
    {
        return $this->morphMany(Dislike::class, 'dislikeable');
    }
    
    /**
     * Did the currently logged in user like this model
     * Example : if($book->liked) { }
     *
     * @return bool
     */
    public function getDislikedAttribute(): bool
    {
        return $this->liked();
    }
    
    /**
     * Counter is a record that stores the total likes for the morphed record
     *
     * @return MorphOne
     *
     */
    public function dislikeCounter(): MorphOne
    {
        return $this->morphOne(DislikeCounter::class, 'dislikeable');
    }
    
    /**
     * Private. Increment the total dislike count stored in the counter
     *
     * @return void
     */
    private function incrementDislikeCount(): void
    {
        $counter = $this->dislikeCounter()->first();
        
        if ($counter) {
            $counter->increment('count');
            $counter->save();
        } else {
            $counter = new DislikeCounter;
            $counter->count = 1;
            $this->dislikeCounter()->save($counter);
        }
    }
    
    /**
     * Fetch records that are disliked by a given user.
     * Ex: Book::whereDislikedBy(123)->get();
     *
     * @param  Builder   $query
     * @param  int|null  $userId
     *
     * @return Builder
     */
    public function scopeWhereDislikedBy(Builder $query, int $userId = null): Builder
    {
        if ($userId === null) {
            $userId = (int) auth()->id();
        }
        
        return $query->whereHas('dislikes', static function ($q) use ($userId) {
            $q->whereUserId($userId);
        });
    }
    
    
    /**
     * Decrement the total dislike count stored in the counter
     *
     * @return void
     * @throws Exception
     *
     */
    private function decrementDislikeCount(): void
    {
        $counter = $this->dislikeCounter()->first();
        
        if ($counter) {
            $counter->decrement('count');
            if ($counter->count) {
                $counter->save();
            } else {
                $counter->delete();
            }
        }
    }
}
