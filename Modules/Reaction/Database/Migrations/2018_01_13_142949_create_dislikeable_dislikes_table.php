<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDislikeableDislikesTable
 */
class CreateDislikeableDislikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dislikeable_dislikes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('dislikeable', 'dislikeable_index');
            $table->unsignedBigInteger('user_id')->index();
            $table->timestamps();
            $table->unique([
                'dislikeable_type',
                'dislikeable_id',
                'user_id'
            ], 'dislikeable_dislikes_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dislikeable_dislikes');
    }
}
