<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDislikeableDislikeCountersTable
 */
class CreateDislikeableDislikeCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dislikeable_dislike_counters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('dislikeable', 'dislikeable_id_index');
            $table->unsignedBigInteger('count')->default(0);
            $table->unique([
                'dislikeable_id',
                'dislikeable_type'
            ], 'dislikeable_counts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dislikeable_dislike_counters');
    }
}
