<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateLikeableLikeCountersTable
 */
class CreateLikeableLikeCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likeable_like_counters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('likeable', 'likeable_id_index');
            $table->unsignedBigInteger('count')->default(0);
            $table->unique([
                'likeable_id',
                'likeable_type'
            ], 'likeable_counts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likeable_like_counters');
    }
}
