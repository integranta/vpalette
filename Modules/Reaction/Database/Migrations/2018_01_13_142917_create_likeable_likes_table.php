<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateLikeableLikesTable
 */
class CreateLikeableLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likeable_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('likeable_type', 100);
            $table->unsignedBigInteger('likeable_id')->index('likeable_index');
            $table->unsignedBigInteger('user_id')->index();
            $table->timestamps();
            $table->unique([
                'likeable_id',
                'likeable_type',
                'user_id'
            ], 'likeable_likes_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likeable_likes');
    }
}
