<?php


namespace Modules\Finance\Repositories;


use App\Repositories\BaseRepository;
use Modules\Finance\Contracts\FinanceContract;

/**
 * Class FinanceRepository
 *
 * @package Modules\Finance\Repositories
 */
class FinanceRepository extends BaseRepository implements FinanceContract
{

}
