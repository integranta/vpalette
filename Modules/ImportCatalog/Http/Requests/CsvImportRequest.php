<?php

namespace Modules\ImportCatalog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CsvImportRequest
 *
 * @package Modules\ImportCatalog\Http\Requests
 */
class CsvImportRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv_file' => 'required|file|'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
