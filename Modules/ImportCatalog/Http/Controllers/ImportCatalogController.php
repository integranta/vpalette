<?php

namespace Modules\ImportCatalog\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ImportService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\{Request};
use Illuminate\View\View;

/**
 * Class ImportCatalogController
 *
 * @package Modules\ImportCatalog\Http\Controllers
 */
class ImportCatalogController extends Controller
{
    /** @var ImportService */
    private $importService;

    /**
     * ImportCatalogController constructor.
     *
     * @param  ImportService  $importService
     */
    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        $imports = $this->importService->index();

        $this->setPageTitle(
            __('importcatalog::messages.title'),
            __('importcatalog::messages.index_subtitle'),
            __('importcatalog::messages.index_leadtext')
        );

        return view('importcatalog::import_catalog.index', compact('imports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $owners = $this->importService->create();

        $this->setPageTitle(
            __('importcatalog::messages.title'),
            __('importcatalog::messages.create_subtitle'),
            __('importcatalog::messages.create_leadtext')
        );

        return view('importcatalog::import_catalog.create', compact('owners'));
    }

    /**
     * Parse uploaded file from first step.
     * Return in second step parsed uploaded file.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function parse(Request $request): View
    {
        $parseData = json_decode($this->importService->parse($request), true, 512, JSON_THROW_ON_ERROR);

        $data = $parseData['data'];
        $csv_data_file = $parseData['csv_data_file'];
        $csv_header_fields = $parseData['csv_header_fields'];


        $this->setPageTitle(
            __('importcatalog::messages.title'),
            __('importcatalog::messages.parse_subtitle'),
            __('importcatalog::messages.parse_leadtext')
        );

        return view('importcatalog::import_catalog.import_fields', compact(
                'data', 'csv_data_file', 'csv_header_fields')
        );
    }

    /**
     * Start importing rows from second step.
     * Update order columns from dropdown.
     * Update minimum price for products which bee matched.
     * Return array new created partner offers.
     *
     * @param  Request  $request
     *
     * @return Factory|View
     */
    public function process(Request $request): View
    {
        $processData = json_decode($this->importService->process($request), true, 512);

        $successImported = $processData['successImported'];
        $hasHeader = $processData['hasHeader'];

        $this->setPageTitle(
            __('importcatalog::messages.title'),
            __('importcatalog::messages.process_subtitle'),
            __('importcatalog::messages.process_leadtext')
        );

        return view('importcatalog::import_catalog.import_success', compact(
                'successImported',
                'hasHeader'
            )
        );
    }
}
