<?php


namespace Modules\ImportCatalog\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ImportCsvAdapter;
use App\Services\ImportExcelAdapter;
use App\Services\ParserService;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;

/**
 * Class ParseCatalogController
 *
 * @package Modules\ImportCatalog\Http\Controllers
 */
class ParseCatalogController extends Controller
{
    /**
     * @var ParserService
     */
    protected $service;
    
    /**
     * ParseCatalogController constructor.
     *
     * @param  ParserService  $service
     */
    public function __construct(ParserService $service)
    {
        $this->service = $service;
    }
    
    /**
     * @param  Request  $request
     *
     * @throws BindingResolutionException
     */
    public function parse(Request $request)
    {
        
        $request->has('header')
            ? new ImportExcelAdapter()
            : new ImportCsvAdapter();
        $this->service(ParserService::class)->store($request);
    }
    
    /**
     * @param  string  $service
     *
     * @return mixed
     * @throws BindingResolutionException
     */
    protected function service(string $service)
    {
        return Container::getInstance()->make($service);
    }
}
