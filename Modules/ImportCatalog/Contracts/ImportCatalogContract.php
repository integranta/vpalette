<?php

namespace Modules\ImportCatalog\Contracts;

/**
 * Interface ImportCatalogContract
 *
 * @package Modules\ImportCatalog\Contracts
 */
interface ImportCatalogContract
{
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return mixed
     */
    public function listImports(string $order = 'id', string $sort = 'desc', array $columns = ['*']);
    
    /**
     * @param  int  $id
     *
     * @return mixed
     */
    public function findImportById(int $id);
    
    /**
     * @param  array  $params
     *
     * @return mixed
     */
    public function createImport(array $params);
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return mixed
     */
    public function updateImport(array $params, int $id);
    
    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteImport($id);
}
