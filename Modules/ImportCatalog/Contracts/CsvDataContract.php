<?php


namespace Modules\ImportCatalog\Contracts;

/**
 * Interface CsvDataContract
 *
 * @package Modules\ImportCatalog\Contracts
 */
interface CsvDataContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return mixed
     */
    public function listCsvDatas(string $order = 'id', string $sort = 'desc', array $columns = ['*']);
    
    /**
     * @param  int  $id
     *
     * @return mixed
     */
    public function findCsvDataById(int $id);
    
    /**
     * @param  array  $params
     *
     * @return mixed
     */
    public function createCsvData(array $params);
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return mixed
     */
    public function updateCsvData(array $params, int $id);
    
    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteCsvData($id);
}
