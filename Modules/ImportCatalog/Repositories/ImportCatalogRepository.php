<?php


namespace Modules\ImportCatalog\Repositories;


use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\ImportCatalog\Contracts\ImportCatalogContract;
use Modules\ImportCatalog\Models\Import;

/**
 * Class ImportCatalogRepository
 *
 * @package Modules\ImportCatalog\Repositories
 */
class ImportCatalogRepository extends BaseRepository implements ImportCatalogContract
{
    /**
     * ImportCatalogRepository constructor.
     *
     * @param  Import  $model
     */
    public function __construct(Import $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return mixed
     */
    public function listImports(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    
    /**
     * @param  int  $id
     *
     * @return mixed
     */
    public function findImportById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $exception) {
            throw new ModelNotFoundException($exception);
        }
    }
    
    /**
     * @param  array  $params
     *
     * @return mixed
     */
    public function createImport(array $params)
    {
        // TODO: Implement createImport() method.
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return mixed
     */
    public function updateImport(array $params, int $id)
    {
        // TODO: Implement updateImport() method.
    }
    
    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteImport($id)
    {
        // TODO: Implement deleteImport() method.
    }
}
