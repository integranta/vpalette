<?php


namespace Modules\ImportCatalog\Repositories;


use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\ImportCatalog\Models\CsvData;
use Modules\ImportCatalog\Contracts\CsvDataContract;

/**
 * Class CsvDataRepository
 *
 * @package Modules\ImportCatalog\Repositories
 */
class CsvDataRepository extends BaseRepository implements CsvDataContract
{
    /**
     * CsvDataRepository constructor.
     *
     * @param  CsvData  $model
     */
    public function __construct(CsvData $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function createCsvData($params)
    {
        return $this->create($params);
    }
    
    /**
     * @inheritDoc
     */
    public function findCsvData($id)
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $exception) {
            throw new ModelNotFoundException($exception);
        }
    }
    
    /**
     * @inheritDoc
     */
    public function listCsvDatas(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    
    /**
     * @inheritDoc
     */
    public function findCsvDataById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $exception) {
            throw new ModelNotFoundException($exception);
        }
    }
    
    /**
     * @inheritDoc
     */
    public function updateCsvData(array $params, int $id)
    {
        return $this->update($params, $id);
    }
    
    /**
     * @inheritDoc
     */
    public function deleteCsvData($id)
    {
        return $this->delete($id);
    }
}
