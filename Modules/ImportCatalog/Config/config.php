<?php

return [
    'name' => 'ImportCatalog',
    'db_fields' => [
        'article',
        'barcode',
        'amount' ,
        'price'
    ]
];
