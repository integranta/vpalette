<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('admin.')
    ->prefix('admin')
    ->middleware('auth')
    ->group(function() {
        //Route::resource('import-catalog', 'ImportCatalogController');
        /*Route::get('import-catalog/', 'ImportCatalogController@index')
            ->name('import-catalog.index');
        Route::get('import-catalog/create', 'ImportCatalogController@create')
            ->name('import-catalog.create');
        Route::post('import-catalog/parse', 'ImportCatalogController@parse')
            ->name('import-catalog.import_parse');
        Route::post('import-catalog/process', 'ImportCatalogController@process')
            ->name('import-catalog.import_process');*/
    });
