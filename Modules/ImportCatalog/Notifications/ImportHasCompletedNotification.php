<?php

namespace Modules\ImportCatalog\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class ImportHasCompletedNotification
 *
 * @package Modules\ImportCatalog\Notifications
 */
class ImportHasCompletedNotification extends Notification
{
    use Queueable;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
            ->subject('Выгрузка остатков товаров прошла успешно')
            ->greeting('Привет !')
            ->line('Выгрузка остатков товаров при импорте прошла успешно.')
            ->action('Перейти к списку импортов', url(route('admin.import-catalog.index')))
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }
    
    /**
     * @param $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'typeNotify' => 'success_import',
            'message' => 'Выгрузка остатков товаров прошла успешно',
            'url' => route('admin.import-catalog.index')
        ];
    }
    
    /**
     * @param $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'typeNotify' => 'success_import',
            'message' => 'Выгрузка остатков товаров прошла успешно',
            'url' => route('admin.import-catalog.index')
        ]);
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'typeNotify' => 'success_import',
            'message' => 'Выгрузка остатков товаров прошла успешно',
            'url' => route('admin.import-catalog.index')
        ];
    }
}
