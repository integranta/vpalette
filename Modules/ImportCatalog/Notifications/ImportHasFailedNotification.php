<?php

namespace Modules\ImportCatalog\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class ImportHasFailedNotification
 *
 * @package Modules\ImportCatalog\Notifications
 */
class ImportHasFailedNotification extends Notification
{
    use Queueable;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->error()
            ->subject('Ошибка выгрузки остатков товаров')
            ->greeting('Привет !')
            ->line('Произошла ошибка при импорте, проверьте правильность данных и повторите попытку.')
            ->action('Перейти к списку импортов', url(route('admin.import-catalog.index')))
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }
    
    /**
     * @param $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'typeNotify' => 'fail_import',
            'message' => 'Выгрузка прошла не успешно, проверьте правильность данных',
            'url' => route('admin.import-catalog.index')
        ];
    }
    
    /**
     * @param $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'typeNotify' => 'fail_import',
            'message' => 'Выгрузка прошла не успешно, проверьте правильность данных',
            'url' => route('admin.import-catalog.index')
        ]);
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'typeNotify' => 'fail_import',
            'message' => 'Выгрузка прошла не успешно, проверьте правильность данных',
            'url' => route('admin.import-catalog.index')
        ];
    }
}
