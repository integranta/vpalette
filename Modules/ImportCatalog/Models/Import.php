<?php

namespace Modules\ImportCatalog\Models;

use App\Enums\ImportStatus;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Import
 *
 * @package Modules\ImportCatalog\Models
 * @property int $id
 * @property int $status
 * @property int $partnerId
 * @property string $importedAt
 * @property string|null $deletedAt
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereImportedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import wherePartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $userId
 * @property string|null $importStartAt
 * @property string|null $importFinishAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereImportFinishAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereImportStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\Import whereUserId($value)
 */
class Import extends Model
{
    use CastsEnums;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'imports';

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'status' => ImportStatus::class
    ];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'imported_at',
        'partner_id'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'int',
        'partner_id' => 'int'
    ];
}
