<?php

namespace Modules\ImportCatalog\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CsvData
 *
 * @package Modules\ImportCatalog\Models
 * @property int $id
 * @property string $csvFilename
 * @property int $csvHeader
 * @property string $csvData
 * @property string $delimiter
 * @property mixed $fields
 * @property int $importer
 * @property int $partnerShopId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereCsvData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereCsvFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereCsvHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereDelimiter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereFields($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereImporter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData wherePartnerShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\ImportCatalog\Models\CsvData whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CsvData extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'csv_data';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'csv_filename',
        'csv_header',
        'csv_data',
        'partner_shop_id',
        'delimiter',
        'importer',
        'fields'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'partner_shop_id' => 'int'
    ];
}
