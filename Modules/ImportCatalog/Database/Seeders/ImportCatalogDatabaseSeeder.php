<?php

namespace Modules\ImportCatalog\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

/**
 * Class ImportCatalogDatabaseSeeder
 *
 * @package Modules\ImportCatalog\Database\Seeders
 */
class ImportCatalogDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        // $this->call("OthersTableSeeder");
    }
}
