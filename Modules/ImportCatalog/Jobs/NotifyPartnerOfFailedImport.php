<?php

namespace Modules\ImportCatalog\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\ImportCatalog\Notifications\ImportHasFailedNotification;

/**
 * Class NotifyPartnerOfFailedImport
 *
 * @package Modules\ImportCatalog\Jobs
 */
class NotifyPartnerOfFailedImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * @var User
     */
    public $user;
    
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->notify(new ImportHasFailedNotification());
    }
}
