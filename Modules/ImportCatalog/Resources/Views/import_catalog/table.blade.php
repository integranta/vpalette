<div class="table-responsive">
    <table class="table" id="importcatalog-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Статус</th>
            <th>Импортирован</th>
            <th style="width:100px; min-width:100px;" class="text-center">Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($imports as $import)
            <tr>
                <td>{{ $import->id }}</td>
                <td>{{ $import->status }}</td>
                <td>{{ $import->imported_at }}</td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="CRUD операции">
                        {!! Form::open(['route' => ['admin.import.destroy', $import->id], 'method' => 'delete']) !!}
                        <a
                            href="{{ route('admin.import.edit', $import->id) }}"
                            class="btn btn-sm btn-primary">
                            <i class="fa fa-edit"></i>
                        </a>
                        {!! Form::button(
                            '<i class="fa fa-trash"></i>',
                            [
                                'type' => 'submit',
                                'class' => 'btn btn-sm btn-danger',
                                'title' => 'Удалить',
                                'onclick' => "return confirm('Вы уверены?')"
                            ])
                        !!}
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
