@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/importcatalog.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-sm-6 col-lg-12">
            @include('admin.partials.flash')
            <div class="card">
                <div class="card-header">
                    <h4>{{ $subTitle }}</h4>
                </div>
                <div class="card-body">
                    <!-- Step block -->
                    <div class="row mt-4">
                        <div class="col-12 col-lg-8 offset-lg-2">
                            <div class="wizard-steps">
                                <div
                                    class="wizard-step"
                                >
                                    <div class="wizard-step-icon">
                                        <i class="fas fa-file-upload"></i>
                                    </div>
                                    <div class="wizard-step-label">
                                        Загрузка файла
                                    </div>
                                </div>

                                <div
                                    class="wizard-step wizard-step-active"
                                >
                                    <div class="wizard-step-icon">
                                        <i class="fas fa-cog"></i>
                                    </div>
                                    <div class="wizard-step-label">
                                        Настройка выгрузки
                                    </div>
                                </div>

                                <div
                                    class="wizard-step"
                                >
                                    <div class="wizard-step-icon">
                                        <i class="fas fa-file-import"></i>
                                    </div>
                                    <div class="wizard-step-label">
                                        Импорт каталога
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Close Step block -->

                    <!-- Form -->
                    <form
                        method="POST"
                        action="{{ route('admin.import-catalog.import_process') }}"
                        enctype="multipart/form-data"
                        class="wizard-content mt-2"
                    >
                        @csrf
                        <input type="hidden" name="csv_data_file_id" value="{{ $csv_data_file['id'] }}">
                        <input type="hidden" name="partner_shop_id" value="{{ $csv_data_file['partner_shop_id'] }}">
                        @include('importcatalog::includes.parse')
                        <div class="card-footer">
                            <!-- Submit Field -->
                            <div class="form-group row col-mb-12">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-2"></label>
                                <div class="col-sm-4 col-md-4">
                                    <a
                                        href="{{ route('admin.import-catalog.index') }}"
                                        class="btn btn-danger">Отмена</a>
                                </div>
                                <div class="col-sm-4 col-md-4 text-right">

                                    <button
                                        type="submit"
                                        class="btn btn-icon icon-right btn-primary">
                                        Далее <i class="fas fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- Close Form -->
                </div>

            </div>
        </div>
    </div>
@endsection

@push('scripts')

    {{--  Module JS File  --}}
    <script src="{{ asset('js/importcatalog.js') }}"></script>
@endpush
