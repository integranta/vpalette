<table class="table" id="parse-table">
    <thead>
    <tr>
        @foreach (config('importcatalog.db_fields') as $db_field)
            <th>{{ $db_field }}</th>
        @endforeach
        <th>Товар</th>
    </tr>
    </thead>
    <tbody>

    @if (!$hasHeader)
        @foreach ($successImported as $offer)
            <tr>
                <td>{{ $offer->article }}</td>
                <td>{{ $offer->barcode }}</td>
                <td>{{ $offer->amount }}</td>
                <td>{{ $offer->price }}</td>
                <td>{{ $offer->product->name }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            @foreach($successImported as $item)
                <td>{{ $item }}</td>
            @endforeach
        </tr>
    @endif
    </tbody>
</table>
<div class="panel-body">
    Данные, которые были успешно импортированы.
</div>
