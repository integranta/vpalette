<div class="table-responsive">
    <table class="table" id="parse-table">
        <thead>
        @if ($csv_header_fields)
            <tr>
                @foreach ($csv_header_fields as $csv_header_field)
                    <th>{{ $csv_header_field }}</th>
                @endforeach
            </tr>
        @endif
        </thead>
        <tbody>
        @foreach ($data as $row)
            <tr>
                @foreach ($row as $key => $value)
                    <td>{{ $value }}</td>
                @endforeach
            </tr>
        @endforeach


        <tr>
            @foreach ($data[0] as $key => $value)
                <td>
                    <label>
                        <select name="fields[{{ $key }}]">
                            @foreach (config('importcatalog.db_fields') as $db_field)
                                <option value="{{ (\Request::has('header')) ? $db_field : $loop->index }}"
                                        @if ($key === $db_field) selected @endif
                                >
                                    {{ $db_field }}
                                </option>
                            @endforeach
                        </select>
                    </label>
                </td>
            @endforeach
        </tr>
        </tbody>
    </table>
</div>
