<?php

namespace Modules\PartnerOffer\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\PartnerShop\Transformers\PartnerShopResource;
use Modules\Product\Transformers\ProductResource;

/**
 * Class PartnerOfferResource
 *
 * @package Modules\PartnerOffer\Transformers
 */
class PartnerOfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'article' => $this->article,
            'barcode' => $this->barcode,
            'price' => $this->price,
            'productId' => $this->product_id,
            'partnerShopId' => $this->partner_shop_id,
            'product' => ProductResource::make($this->whenLoaded('product')),
            'partnerShop' => PartnerShopResource::make($this->whenLoaded('partnerShop')),
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
