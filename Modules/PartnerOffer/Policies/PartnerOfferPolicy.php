<?php

namespace Modules\PartnerOffer\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Class PartnerOfferPolicy
 *
 * @package Modules\PartnerOffer\Policies
 */
class PartnerOfferPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the partner offer.
     *
     * @param  User|null     $user
     * @param  PartnerOffer  $partnerOffer
     *
     * @return mixed
     */
    public function view(?User $user, PartnerOffer $partnerOffer)
    {
        if ($partnerOffer->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished partner offers')) {
            return true;
        }
        
        // authors can view their own unpublished partner offers
        return $user->id == $user->is($partnerOffer->partnerShop->parnter->user);
    }
    
    /**
     * Determine whether the user can create partner offers.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create partner offers')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the partner offer.
     *
     * @param  User          $user
     * @param  PartnerOffer  $partnerOffer
     *
     * @return mixed
     */
    public function update(User $user, PartnerOffer $partnerOffer)
    {
        if ($user->can('edit own partner offers')) {
            return $user->id == $user->is($partnerOffer->partnerShop->parnter->user);
        }
        
        if ($user->can('edit all partner offers')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the partner offer.
     *
     * @param  User          $user
     * @param  PartnerOffer  $partnerOffer
     *
     * @return mixed
     */
    public function delete(User $user, PartnerOffer $partnerOffer)
    {
        if ($user->can('delete own partner offers')) {
            return $user->id == $user->is($partnerOffer->partnerShop->parnter->user);
        }
        
        if ($user->can('delete any partner offer')) {
            return true;
        }
        
        return false;
    }
}
