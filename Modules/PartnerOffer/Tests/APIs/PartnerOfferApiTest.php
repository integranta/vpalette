<?php namespace Modules\PartnerOffer\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\PartnerOffer\Tests\ApiTestTrait;
use Modules\PartnerOffer\Tests\TestCase;
use Modules\PartnerOffer\Tests\Traits\MakePartnerOfferTrait;

/**
 * Class PartnerOfferApiTest
 *
 * @package Modules\PartnerOffer\Tests\APIs
 */
class PartnerOfferApiTest extends TestCase
{
    use MakePartnerOfferTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_partner_offer()
    {
        $partnerOffer = $this->fakePartnerOfferData();
        $this->response = $this->json('POST', '/api/partnerOffers', $partnerOffer);
        
        $this->assertApiResponse($partnerOffer);
    }
    
    /**
     * @test
     */
    public function test_read_partner_offer()
    {
        $partnerOffer = $this->makePartnerOffer();
        $this->response = $this->json('GET', '/api/partnerOffers/'.$partnerOffer->id);
        
        $this->assertApiResponse($partnerOffer->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_partner_offer()
    {
        $partnerOffer = $this->makePartnerOffer();
        $editedPartnerOffer = $this->fakePartnerOfferData();
        
        $this->response = $this->json('PUT', '/api/partnerOffers/'.$partnerOffer->id, $editedPartnerOffer);
        
        $this->assertApiResponse($editedPartnerOffer);
    }
    
    /**
     * @test
     */
    public function test_delete_partner_offer()
    {
        $partnerOffer = $this->makePartnerOffer();
        $this->response = $this->json('DELETE', '/api/partnerOffers/'.$partnerOffer->id);
        
        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/partnerOffers/'.$partnerOffer->id);
        
        $this->response->assertStatus(404);
    }
}
