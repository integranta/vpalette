<?php namespace Modules\PartnerOffer\Tests\Traits;

use App;
use Faker\Factory as Faker;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerOffer\Repositories\PartnerOfferRepository;

/**
 * Trait MakePartnerOfferTrait
 *
 * @package Modules\PartnerOffer\Tests\Traits
 */
trait MakePartnerOfferTrait
{
    /**
     * Create fake instance of PartnerOffer and save it in database
     *
     * @param  array  $partnerOfferFields
     *
     * @return PartnerOffer
     */
    public function makePartnerOffer($partnerOfferFields = [])
    {
        /** @var PartnerOfferRepository $partnerOfferRepo */
        $partnerOfferRepo = App::make(PartnerOfferRepository::class);
        $theme = $this->fakePartnerOfferData($partnerOfferFields);
        return $partnerOfferRepo->create($theme);
    }
    
    /**
     * Get fake instance of PartnerOffer
     *
     * @param  array  $partnerOfferFields
     *
     * @return PartnerOffer
     */
    public function fakePartnerOffer($partnerOfferFields = [])
    {
        return new PartnerOffer($this->fakePartnerOfferData($partnerOfferFields));
    }
    
    /**
     * Get fake data of PartnerOffer
     *
     * @param  array  $partnerOfferFields
     *
     * @return array
     */
    public function fakePartnerOfferData($partnerOfferFields = [])
    {
        $fake = Faker::create();
        
        return array_merge([
            'partner_id' => $fake->word,
            'article' => $fake->word,
            'barcode' => $fake->word,
            'name' => $fake->word,
            'description' => $fake->text,
            'slug' => $fake->word,
            'image_src' => $fake->word,
            'amount' => $fake->randomDigitNotNull,
            'product_id' => $fake->word,
            'price' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $partnerOfferFields);
    }
}
