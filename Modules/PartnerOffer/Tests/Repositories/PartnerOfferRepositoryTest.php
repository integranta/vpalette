<?php namespace Modules\PartnerOffer\Tests\Repositories;

use App;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerOffer\Repositories\PartnerOfferRepository;
use Modules\PartnerOffer\Tests\ApiTestTrait;
use Modules\PartnerOffer\Tests\TestCase;
use Modules\PartnerOffer\Tests\Traits\MakePartnerOfferTrait;

/**
 * Class PartnerOfferRepositoryTest
 *
 * @package Modules\PartnerOffer\Tests\Repositories
 */
class PartnerOfferRepositoryTest extends TestCase
{
    use MakePartnerOfferTrait, ApiTestTrait, DatabaseTransactions;
    
    /**
     * @var PartnerOfferRepository
     */
    protected $partnerOfferRepo;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->partnerOfferRepo = App::make(PartnerOfferRepository::class);
    }
    
    /**
     * @test create
     */
    public function test_create_partner_offer()
    {
        $partnerOffer = $this->fakePartnerOfferData();
        $createdPartnerOffer = $this->partnerOfferRepo->create($partnerOffer);
        $createdPartnerOffer = $createdPartnerOffer->toArray();
        $this->assertArrayHasKey('id', $createdPartnerOffer);
        $this->assertNotNull($createdPartnerOffer['id'], 'Created PartnerOffer must have id specified');
        $this->assertNotNull(PartnerOffer::find($createdPartnerOffer['id']),
            'PartnerOffer with given id must be in DB');
        $this->assertModelData($partnerOffer, $createdPartnerOffer);
    }
    
    /**
     * @test read
     */
    public function test_read_partner_offer()
    {
        $partnerOffer = $this->makePartnerOffer();
        $dbPartnerOffer = $this->partnerOfferRepo->find($partnerOffer->id);
        $dbPartnerOffer = $dbPartnerOffer->toArray();
        $this->assertModelData($partnerOffer->toArray(), $dbPartnerOffer);
    }
    
    /**
     * @test update
     */
    public function test_update_partner_offer()
    {
        $partnerOffer = $this->makePartnerOffer();
        $fakePartnerOffer = $this->fakePartnerOfferData();
        $updatedPartnerOffer = $this->partnerOfferRepo->update($fakePartnerOffer, $partnerOffer->id);
        $this->assertModelData($fakePartnerOffer, $updatedPartnerOffer->toArray());
        $dbPartnerOffer = $this->partnerOfferRepo->find($partnerOffer->id);
        $this->assertModelData($fakePartnerOffer, $dbPartnerOffer->toArray());
    }
    
    /**
     * @test delete
     */
    public function test_delete_partner_offer()
    {
        $partnerOffer = $this->makePartnerOffer();
        $resp = $this->partnerOfferRepo->delete($partnerOffer->id);
        $this->assertTrue($resp);
        $this->assertNull(PartnerOffer::find($partnerOffer->id), 'PartnerOffer should not exist in DB');
    }
}
