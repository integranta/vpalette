@extends('layouts.admin-master')

@section('content')
    <section class="content-header">
        <h1>
            Partner Offer
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($partnerOffer, ['route' => ['admin.partner-offers.update', $partnerOffer->id], 'method' => 'patch']) !!}

                    @include('partneroffer::partner_offers.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
