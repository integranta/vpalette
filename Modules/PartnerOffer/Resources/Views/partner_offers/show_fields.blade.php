<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $partnerOffer->id !!}</p>
</div>

<!-- Partner Id Field -->
<div class="form-group">
    {!! Form::label('partner_id', 'Partner Id:') !!}
    <p>{!! $partnerOffer->partner_id !!}</p>
</div>

<!-- Article Field -->
<div class="form-group">
    {!! Form::label('article', 'Article:') !!}
    <p>{!! $partnerOffer->article !!}</p>
</div>

<!-- Barcode Field -->
<div class="form-group">
    {!! Form::label('barcode', 'Barcode:') !!}
    <p>{!! $partnerOffer->barcode !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $partnerOffer->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $partnerOffer->description !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $partnerOffer->slug !!}</p>
</div>

<!-- Image Src Field -->
<div class="form-group">
    {!! Form::label('image_src', 'Image Src:') !!}
    <p>{!! $partnerOffer->image_src !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $partnerOffer->amount !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $partnerOffer->product_id !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $partnerOffer->price !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $partnerOffer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $partnerOffer->updated_at !!}</p>
</div>

