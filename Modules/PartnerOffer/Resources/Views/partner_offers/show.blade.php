@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.partners.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                На этой странице вы можете просмотреть заполненную информацию об партнёре.
            </p>
            <div class="row" style="padding-left: 20px">
                @include('partneroffer::partner_offers.show_fields')
                <a href="{!! route('admin.partner-offers.index') !!}" class="btn btn-default">Back</a>
            </div>
        </div>
    </section>
@endsection
