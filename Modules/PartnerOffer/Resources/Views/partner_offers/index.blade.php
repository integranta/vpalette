@extends('layouts.admin-master')

@section('title')
	{{ $pageTitle }}
@endsection

@section('plugin_css')
	<link rel="stylesheet" href="{{ asset('css/partneroffer.css') }}">
@endsection
@section('content')
	<section class="section">
		<div class="section-header">
			<h1>{{ $pageTitle }}</h1>
			@can('create partner offers')
				<div class="section-header-button">
					<a href="{{ route('admin.partner-offers.create') }}" class="btn btn-primary">Добавить</a>
				</div>
			@endcan
		</div>
		<div class="section-body">
			<h2 class="section-title">{{ $subTitle }}</h2>
			<p class="section-lead">
				{{ $leadText }}
			</p>
			<div class="row">
				<div class="col-md-12">
					@include('admin.partials.flash')
					<div class="card">
						<div class="card-header">
							<h4>{{ $subTitle }}</h4>
						</div>
						<div class="card-body">
							@include('partneroffer::partner_offers.table')
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('scripts')
	{{ $dataTable->scripts() }}
@endpush
