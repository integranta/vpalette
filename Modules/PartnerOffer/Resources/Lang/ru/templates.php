<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Переводы для шаблона таблицы.
	|--------------------------------------------------------------------------
	*/
	'table' => [
		'id'      => 'ID',
		'article' => 'Артикул',
		'barcode' => 'Штрих-код',
		'amount'  => 'Кол-во',
		'price'   => 'Цена',
		'product' => 'Товар',
		'action'  => 'Действия'
	],
];
