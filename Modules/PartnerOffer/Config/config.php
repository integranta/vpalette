<?php

return [
    'name' => 'PartnerOffer',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all partner offers as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('PARTNER_OFFERS_CACHE_KEY','index_partner_offers'),
    'cache_ttl' => env('PARTNER_OFFERS_CACHE_TTL',360000),
];
