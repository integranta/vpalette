<?php


namespace Modules\PartnerOffer\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\PartnerOffer\Http\Requests\API\V1\CreatePartnerOfferAPIRequest;
use Modules\PartnerOffer\Http\Requests\API\V1\UpdatePartnerOfferAPIRequest;
use Modules\PartnerOffer\Transformers\PartnerOfferResource;

/**
 * Class PartnerOfferApiService
 *
 * @package Modules\PartnerOffer\Services\API\V1
 */
class PartnerOfferApiService extends BaseService
{
	/** @var  PartnerOfferContract */
	private $partnerOfferRepository;
	
	/**
	 * PartnerOfferAPIController constructor.
	 *
	 * @param  PartnerOfferContract  $partnerOfferRepo
	 */
	public function __construct(PartnerOfferContract $partnerOfferRepo)
	{
		$this->partnerOfferRepository = $partnerOfferRepo;
	}
	
	
	/**
	 * @param  Request  $request
	 *
	 * @return JsonResponse
	 */
	public function index(Request $request): JsonResponse
	{
		$partnerOffers = PartnerOfferResource::collection($this->partnerOfferRepository->listOffers());
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partneroffer::messages.success_retrieve'),
			$partnerOffers
		);
	}
	
	
	/**
	 * @param  CreatePartnerOfferAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function store(CreatePartnerOfferAPIRequest $request): JsonResponse
	{
		$partnerOffer = $this->partnerOfferRepository->createOffer($request->all());
		
		if (!$partnerOffer) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('partneroffer::messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('partneroffer::messages.success_create'),
			$partnerOffer
		);
	}
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function show(int $id): JsonResponse
	{
		$partnerOffer = PartnerOfferResource::make($this->partnerOfferRepository->findOfferById($id));
		
		if ($partnerOffer === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('partneroffer::messages.not_found', ['id' => $id])
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partneroffer::messages.success_found', ['id' => $id]),
			$partnerOffer
		);
	}
	
	/**
	 * @param  int                           $id
	 * @param  UpdatePartnerOfferAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function update(int $id, UpdatePartnerOfferAPIRequest $request): JsonResponse
	{
		$partnerOffer = $this->partnerOfferRepository->findOfferById($id);
		
		if ($partnerOffer === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('partneroffer::messages.not_found', ['id' => $id])
			);
		}
		
		$partnerOffer = $this->partnerOfferRepository->updateOffer($request->all(), $id);
		
		if (!$partnerOffer) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('partneroffer::messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partneroffer::messages.success_update'),
			$partnerOffer
		);
	}
	
	
	/**
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function destroy(int $id): JsonResponse
	{
		$partnerOffer = $this->partnerOfferRepository->findOfferById($id);
		
		if ($partnerOffer === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('partneroffer::messages.not_found', ['id' => $id])
			);
		}
		
		$partnerOffer = $this->partnerOfferRepository->deleteOffer($id);
		
		if (!$partnerOffer) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('partneroffer::messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('partneroffer::messages.success_delete'),
			$partnerOffer
		);
	}
}
