<?php


namespace Modules\PartnerOffer\Services;


use App\DataTables\PartnerOffersDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Partner\Contracts\PartnerContract;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\PartnerOffer\Http\Requests\CreatePartnerOfferRequest;
use Modules\PartnerOffer\Http\Requests\UpdatePartnerOfferRequest;

/**
 * Class PartnerOfferWebService
 *
 * @package Modules\PartnerOffer\Services
 */
class PartnerOfferWebService extends BaseService
{
	/** @var PartnerOfferContract */
	protected $partnerOfferRepository;
	
	/** @var PartnerContract */
	protected $partnerRepository;
	
	/**
	 * PartnerOfferController constructor.
	 *
	 * @param  PartnerOfferContract  $partnerOfferRepo
	 * @param  PartnerContract       $partnerRepo
	 */
	public function __construct(
		PartnerOfferContract $partnerOfferRepo,
		PartnerContract $partnerRepo
	) {
		$this->partnerOfferRepository = $partnerOfferRepo;
		$this->partnerRepository = $partnerRepo;
	}
	
	/**
	 * Display a listing of the PartnerOffer.
	 *
	 * @param  \App\DataTables\PartnerOffersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(PartnerOffersDataTable $dataTable)
	{
		$this->setPageTitle(
			__('partneroffer::messages.title'),
			__('partneroffer::messages.index_subtitle'),
			__('partneroffer::messages.index_leadtext')
		);
		return $dataTable->render('partneroffer::partner_offers.index');
	}
	
	/**
	 * Show the form for creating a new PartnerOffer.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$this->setPageTitle(
			__('partneroffer::messages.title'),
			__('partneroffer::messages.show_subtitle'),
			__('partneroffer::messages.show_leadtext')
		);
		return view('partneroffer::partner_offers.create');
	}
	
	/**
	 * Store a newly created PartnerOffer in storage.
	 *
	 * @param  CreatePartnerOfferRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreatePartnerOfferRequest $request): RedirectResponse
	{
		$partnerOffer = $this->partnerOfferRepository->createOffer($request->all());
		
		if (!$partnerOffer) {
			return $this->responseRedirectBack(
				__('partneroffer::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partnerOffers.index',
			__('partneroffer::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified PartnerOffer.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|View
	 */
	public function show(int $id): View
	{
		$partnerOffer = $this->partnerOfferRepository->findOfferById($id);
		
		if ($partnerOffer === null) {
			$this->responseRedirect(
				'admin.partnerOffers.index',
				__('partneroffer::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$this->setPageTitle(
			__('partneroffer::messages.title'),
			__('partneroffer::messages.show_subtitle', ['id' => $partnerOffer->id]),
			__('partneroffer::messages.show_leadtext', ['id' => $partnerOffer->id])
		);
		
		return view('partneroffer::partner_offers.show', compact('partnerOffer'));
	}
	
	/**
	 * Show the form for editing the specified PartnerOffer.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|View
	 */
	public function edit(int $id): View
	{
		$partnerOffer = $this->partnerOfferRepository->findOfferById($id);
		
		if ($partnerOffer === null) {
			$this->responseRedirect(
				'admin.partnerOffers.index',
				__('partneroffer::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$this->setPageTitle(
			__('partneroffer::messages.title'),
			__('partneroffer::messages.edit_subtitle', ['id' => $partnerOffer->id]),
			__('partneroffer::messages.edit_leadtext', ['id' => $partnerOffer->id])
		);
		
		return view('partneroffer::partner_offers.edit', compact('partnerOffer'));
	}
	
	/**
	 * Update the specified PartnerOffer in storage.
	 *
	 * @param  int                        $id
	 * @param  UpdatePartnerOfferRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function update(int $id, UpdatePartnerOfferRequest $request): RedirectResponse
	{
		$partnerOffer = $this->partnerOfferRepository->findOfferById($id);
		
		if ($partnerOffer === null) {
			$this->responseRedirect(
				'admin.partnerOffers.index',
				__('partneroffer::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$partnerOffer = $this->partnerOfferRepository->updateOffer($request->all(), $id);
		
		if (!$partnerOffer) {
			return $this->responseRedirectBack(
				__('partneroffer::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partnerOffers.index',
			__('partneroffer::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified PartnerOffer from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 *
	 */
	public function destroy(int $id): RedirectResponse
	{
		$partnerOffer = $this->partnerOfferRepository->findOfferById($id);
		
		if ($partnerOffer === null) {
			$this->responseRedirect(
				'admin.partnerOffers.index',
				__('partneroffer::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$partnerOffer = $this->partnerOfferRepository->deleteOffer($id);
		
		if (!$partnerOffer) {
			return $this->responseRedirectBack(
				__('partneroffer::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partnerOffers.index',
			__('partneroffer::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}