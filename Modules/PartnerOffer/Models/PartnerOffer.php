<?php

namespace Modules\PartnerOffer\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Discount\Models\Discount;
use Modules\PartnerShop\Models\PartnerShop;
use Modules\Product\Models\Product;
use Rennokki\QueryCache\Traits\QueryCacheable;


/**
 * Class PartnerOffer
 *
 * @package Modules\PartnerOffer\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $article
 * @property string $barcode
 * @property int $amount
 * @property int $price
 * @property int $productId
 * @property int $partnerShopId
 * @property bool $allowDiscountFromPortal
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerOffer\Models\PartnerOffer[] $allowDiscountFromPortal
 * @property-read int|null $allowDiscountFromPortalCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerOffer\Models\PartnerOffer[] $disallowDiscountFromPortal
 * @property-read int|null $disallowDiscountFromPortalCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Discount\Models\Discount[] $discounts
 * @property-read int|null $discountsCount
 * @property-read \Modules\PartnerShop\Models\PartnerShop $partnerShop
 * @property-read \Modules\Product\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerOffer\Models\PartnerOffer onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereAllowDiscountFromPortal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereArticle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer wherePartnerShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerOffer\Models\PartnerOffer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerOffer\Models\PartnerOffer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerOffer\Models\PartnerOffer withoutTrashed()
 * @mixin \Eloquent
 */
class PartnerOffer extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'partner_offers';


    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'article',
        'barcode',
        'amount',
        'price',
        'product_id',
        'partner_shop_id',
        'allow_discount_from_portal'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'article' => 'string',
        'barcode' => 'string',
        'amount' => 'integer',
        'price' => 'integer',
        'product_id' => 'integer',
        'partner_shop_id' => 'integer',
        'allow_discount_from_portal' => 'boolean',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'article' => 'required|string|exists:products,article',
        'barcode' => 'required|string|exists:products,barcode',
        'amount' => 'required|integer|',
        'price' => 'required',
        'product_id' => 'required|integer|exists:products,id',
        'partner_shop_id' => 'required|integer|exists:partner_shop,id',
        'active' => 'required|boolean',
        'sort' => 'required|integer'
    ];

    /**
     * Получить партнёрский магазин, который принадлежит это предложение.
     *
     * @return BelongsTo
     */
    public function partnerShop(): BelongsTo
    {
        return $this->belongsTo(PartnerShop::class);
    }

    /**
     * Получить товар портала, для которого делается это предложение партнёра.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Скидки, который принадлежат этому предложению.
     *
     * @return BelongsToMany
     */
    public function discounts(): BelongsToMany
    {
        return $this->belongsToMany(Discount::class, 'partner_offer_discount')
            ->withPivot('partner_offer_id', 'discount_id')
            ->withTimestamps();
    }

    /**
     * Получить партнёрские предложения на которых разрешено применение скидок от портала.
     *
     * @return HasMany
     */
    public function allowDiscountFromPortal(): HasMany
    {
        return $this->hasMany(self::class)
            ->where('allow_discount_from_portal', '=', 1)
            ->orderByDesc('id');
    }

    /**
     * Получить партнёрские предложение на которых не разрешено применение скидок от портала.
     *
     * @return HasMany
     */
    public function disallowDiscountFromPortal(): HasMany
    {
        return $this->hasMany(self::class)
            ->where('allow_discount_from_portal', '=', 0)
            ->orderByDesc('id');
    }
}
