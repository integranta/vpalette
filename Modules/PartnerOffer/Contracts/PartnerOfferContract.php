<?php


namespace Modules\PartnerOffer\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Interface PartnerOfferContract
 *
 * @package Modules\PartnerOffer\Contracts
 */
interface PartnerOfferContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listOffers(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return PartnerOffer
     */
    public function findOfferById(int $id): ?PartnerOffer;
    
    /**
     * @param  array  $params
     *
     * @return PartnerOffer
     */
    public function createOffer(array $params): PartnerOffer;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return PartnerOffer
     */
    public function updateOffer(array $params, int $id): PartnerOffer;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteOffer(int $id): bool;
}
