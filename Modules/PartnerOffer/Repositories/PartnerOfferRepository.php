<?php

namespace Modules\PartnerOffer\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\PartnerOffer\Models\PartnerOffer;
use Str;

/**
 * Class PartnerOfferRepository
 *
 * @package Modules\PartnerOffer\Repositories
 * @version June 25, 2019, 7:27 am UTC
 */
class PartnerOfferRepository extends BaseRepository implements PartnerOfferContract
{
    /**
     * PartnerOfferRepository constructor.
     *
     * @param  PartnerOffer  $model
     */
    public function __construct(PartnerOffer $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listOffers(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('partneroffer.cache_key').$postfix,
            config('partneroffer.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return PartnerOffer
     */
    public function findOfferById(int $id): ?PartnerOffer
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return PartnerOffer
     */
    public function createOffer(array $params): PartnerOffer
    {
        return $this->create($params);
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return PartnerOffer
     */
    public function updateOffer(array $params, int $id): PartnerOffer
    {
        return $this->update($params, $id);
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteOffer(int $id): bool
    {
        return $this->delete($id);
    }
}
