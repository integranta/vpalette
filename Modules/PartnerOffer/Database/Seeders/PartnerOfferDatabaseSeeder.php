<?php

namespace Modules\PartnerOffer\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Class PartnerOfferDatabaseSeeder
 *
 * @package Modules\PartnerOffer\Database\Seeders
 */
class PartnerOfferDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(PartnerOffer::class, 20)->create();
    }
}
