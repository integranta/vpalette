<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerShop\Models\PartnerShop;
use Modules\Product\Models\Product;

$factory->define(PartnerOffer::class,
    function (Faker $faker) {
        return [
            'active' => true,
            'sort' => $faker->numberBetween(1, 500),
            'article' => $faker->unique()->ean8,
            'barcode' => $faker->unique()->ean13,
            'amount' => $faker->numberBetween(100, 50000),
            'price' => $faker->numberBetween(100, 50000),
            'allow_discount_from_portal' => $faker->numberBetween(0, 1),
            'product_id' => $faker->numberBetween(1, 20),
            'partner_shop_id' => factory(PartnerShop::class)
        ];
    });
