<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreatePartnerOffersTable
 */
class CreatePartnerOffersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->string('article');
            $table->string('barcode');
            $table->integer('amount');
            $table->unsignedInteger('price');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('partner_shop_id');
            $table->boolean('allow_discount_from_portal')->default(false);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('partner_shop_id')->references('id')->on('partner_shops')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->index(['active', 'sort', 'product_id', 'partner_shop_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partner_offers');
    }
}
