<?php

namespace Modules\PartnerOffer\Observers;

use App\Traits\Cacheable;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Class PartnerOfferObserver
 *
 * @package Modules\PartnerOffer\Observers
 */
class PartnerOfferObserver
{
    use Cacheable;
    
    /**
     * Handle the partner offer "created" event.
     *
     * @param  PartnerOffer  $partnerOffer
     *
     * @return void
     */
    public function created(PartnerOffer $partnerOffer): void
    {
        $this->updateMinPriceProduct($partnerOffer);
        $this->incrementCountItemsInCache($partnerOffer, config('partneroffer.cache_key'));
    }
    
    /**
     * Handle the partner offer "updated" event.
     *
     * @param  PartnerOffer  $partnerOffer
     *
     * @return void
     */
    public function updated(PartnerOffer $partnerOffer): void
    {
        $this->updateMinPriceProduct($partnerOffer);
    }
    
    /**
     * Handle the partner offer "deleted" event.
     *
     * @param  PartnerOffer  $partnerOffer
     *
     * @return void
     */
    public function deleted(PartnerOffer $partnerOffer): void
    {
        $this->updateMinPriceProduct($partnerOffer);
        $this->decrementCountItemsInCache($partnerOffer, config('partneroffer.cache_key'));
    }
    
    /**
     * Handle the partner offer "restored" event.
     *
     * @param  PartnerOffer  $partnerOffer
     *
     * @return void
     */
    public function restored(PartnerOffer $partnerOffer): void
    {
        $this->updateMinPriceProduct($partnerOffer);
        $this->incrementCountItemsInCache($partnerOffer, config('partneroffer.cache_key'));
    }
    
    /**
     * Handle the partner offer "force deleted" event.
     *
     * @param  PartnerOffer  $partnerOffer
     *
     * @return void
     */
    public function forceDeleted(PartnerOffer $partnerOffer): void
    {
        $this->updateMinPriceProduct($partnerOffer);
        $this->decrementCountItemsInCache($partnerOffer, config('partneroffer.cache_key'));
    }
    
    /**
     * @param  PartnerOffer  $partnerOffer
     *
     * @return void
     */
    private function updateMinPriceProduct(PartnerOffer $partnerOffer): void
    {
        $product = $partnerOffer->product;
        $product->update([
            'min_price' => number_format($product->partnerOffers->min('price'), 2, '.', ''),
        ]);
    }
}
