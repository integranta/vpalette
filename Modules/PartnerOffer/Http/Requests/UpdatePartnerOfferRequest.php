<?php

namespace Modules\PartnerOffer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Class UpdatePartnerOfferRequest
 *
 * @package Modules\PartnerOffer\Http\Requests
 */
class UpdatePartnerOfferRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return PartnerOffer::$rules;
    }
}
