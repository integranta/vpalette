<?php

namespace Modules\PartnerOffer\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\PartnerOffer\Models\PartnerOffer;

/**
 * Class UpdatePartnerOfferAPIRequest
 *
 * @package Modules\PartnerOffer\Http\Requests\API\V1
 */
class UpdatePartnerOfferAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return PartnerOffer::$rules;
    }
}
