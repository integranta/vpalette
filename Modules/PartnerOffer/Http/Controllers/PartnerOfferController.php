<?php

namespace Modules\PartnerOffer\Http\Controllers;

use App\DataTables\PartnerOffersDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\PartnerOffer\Http\Requests\CreatePartnerOfferRequest;
use Modules\PartnerOffer\Http\Requests\UpdatePartnerOfferRequest;
use Modules\PartnerOffer\Services\PartnerOfferWebService;

/**
 * Class PartnerOfferController
 *
 * @package Modules\PartnerOffer\Http\Controllers
 */
class PartnerOfferController extends Controller
{
    /** @var PartnerOfferWebService */
    private $partnerOfferWebService;

    /**
     * PartnerOfferController constructor.
     *
     * @param  PartnerOfferWebService  $service
     */
    public function __construct(PartnerOfferWebService $service)
    {
        $this->partnerOfferWebService = $service;
        $this->middleware('role:Owner|Admin|Partner');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the PartnerOffer.
	 *
	 * @param  \App\DataTables\PartnerOffersDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(PartnerOffersDataTable $dataTable)
    {
        return $this->partnerOfferWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new PartnerOffer.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->partnerOfferWebService->create();
    }

    /**
     * Store a newly created PartnerOffer in storage.
     *
     * @param  CreatePartnerOfferRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreatePartnerOfferRequest $request): RedirectResponse
    {
        return $this->partnerOfferWebService->store($request);
    }

    /**
     * Display the specified PartnerOffer.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->partnerOfferWebService->show($id);
    }

    /**
     * Show the form for editing the specified PartnerOffer.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->partnerOfferWebService->edit($id);
    }

    /**
     * Update the specified PartnerOffer in storage.
     *
     * @param  int                        $id
     * @param  UpdatePartnerOfferRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdatePartnerOfferRequest $request): RedirectResponse
    {
        return $this->partnerOfferWebService->update($id, $request);
    }

    /**
     * Remove the specified PartnerOffer from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->partnerOfferWebService->destroy($id);
    }
}
