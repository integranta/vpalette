<?php

namespace Modules\PartnerOffer\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\PartnerOffer\Http\Requests\API\V1\CreatePartnerOfferAPIRequest;
use Modules\PartnerOffer\Http\Requests\API\V1\UpdatePartnerOfferAPIRequest;
use Modules\PartnerOffer\Services\API\V1\PartnerOfferApiService;

/**
 * Class PartnerOfferController
 *
 * @package Modules\PartnerOffer\Http\Controllers\API\V1
 */
class PartnerOfferAPIController extends Controller
{
    /** @var  PartnerOfferApiService */
    private $partnerOfferApiService;

    /**
     * PartnerOfferAPIController constructor.
     *
     * @param  PartnerOfferApiService  $service
     */
    public function __construct(PartnerOfferApiService $service)
    {
        $this->partnerOfferApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }


    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->partnerOfferApiService->index($request);
    }


    /**
     * @param  CreatePartnerOfferAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreatePartnerOfferAPIRequest $request): JsonResponse
    {
        return $this->partnerOfferApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->partnerOfferApiService->show($id);
    }

    /**
     * @param  int                           $id
     * @param  UpdatePartnerOfferAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdatePartnerOfferAPIRequest $request): JsonResponse
    {
        return $this->partnerOfferApiService->update($id, $request);
    }


    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->partnerOfferApiService->destroy($id);
    }
}
