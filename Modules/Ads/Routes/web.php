<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Ads\Http\Controllers\AdsController;

Route::name('admin.')
    ->prefix('admin')
    ->middleware('auth')
    ->group(function() {
        Route::resource('ads', AdsController::class);
    });
