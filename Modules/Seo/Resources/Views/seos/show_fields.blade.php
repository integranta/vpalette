<div class="tab-content tab-bordered" id="myTab3Content">
    <div class="tab-pane fade show active" id="element" role="tabpanel" aria-labelledby="element-tab">
        <!-- Meta Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'title',
                    'Meta title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'title',
                        $seo->title,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End title Field -->

        <!-- Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'description',
                    'Meta Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'description',
                        $seo->description,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End description Field -->

        <!-- Keywords Field -->
        <div class="form-group row ">
            {!!
                Form::label(
                    'keywords',
                    'Keywords:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}

            <div class="col-sm-6 col-md-9">
                <input type="text"
                       id="keywords"
                       name="keywords"
                       value="{{ $seo->keywords }}"
                       class="form-control w-100"
                       disabled="disabled"
                >
            </div>
        </div>
        <!-- End keywords Field -->

        <!-- Canonical URL Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'canonical_url',
                    'Canonical URL:',
                    [
                        'class' => 'form-control-label col-sm-3 text-md-right',
                        'id' => 'canonical_url'
                    ]
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'canonical_url',
                        $seo->canonical_url,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>


        <!-- Og Title Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_title',
                    'Open Graph Title:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_title',
                        $seo->og_title,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End title Field -->

        <!-- Og Description Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_description',
                    'Og Description:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'og_description',
                        $seo->og_description,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End description Field -->


        <!-- Og Image Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_image',
                    'Open Graph Image:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_image',
                        $seo->og_image,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End og image Field -->


        <!-- Og type Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_type',
                    'Open Graph type:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_type',
                        $seo->og_type,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End og type Field -->


        <!-- Og url Field -->
        <div class="form-group row align-items-center">
            {!!
                Form::label(
                    'og_url',
                    'Open Graph url:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::text(
                        'og_url',
                        $seo->og_url,
                        ['class' => 'form-control', 'disabled']
                    )
                !!}
            </div>
        </div>
        <!-- End og url Field -->

    </div>
    <div class="tab-pane fade show" id="preview" role="tabpanel" aria-labelledby="preview-tab">
        <div class="form-group row">
            {!!
                Form::label(
                    'ya_direct',
                    'Яндекс.Директ:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_direct',
                        $seo->ya_direct,
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_direct',
                            'disabled'
                        ]
                    )
                !!}
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
        <div class="form-group row">
            {!!
                Form::label(
                    'google_tag_manager',
                    'Google Tag Manager:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_tag_manager',
                       $seo->google_tag_manager,
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_tag_manager',
                            'disabled'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'google_analytics',
                    'Google Analytics:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'google_analytics',
                        $seo->google_analytics,
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'google_analytics',
                            'disabled'
                        ]
                    )
                !!}
            </div>
        </div>
        <div class="form-group row">
            {!!
                Form::label(
                    'ya_metrica',
                    'Яндекс.Метрика:',
                    ['class' => 'form-control-label col-sm-3 text-md-right']
                )
            !!}
            <div class="col-sm-6 col-md-9">
                {!!
                    Form::textarea(
                        'ya_metrica',
                        $seo->ya_metrica,
                        [
                            'class' => 'form-control codeeditor',
                            'id' => 'ya_metrica',
                            'disabled'
                        ]
                    )
                !!}
            </div>
        </div>
    </div>
</div>
