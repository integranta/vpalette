import 'jquery';
import CodeMirror from 'codemirror/lib/codemirror';
import 'codemirror/mode/textile/textile';
import 'codemirror/mode/xml/xml';
import 'codemirror/mode/javascript/javascript';
import 'selectric/public/jquery.selectric.min.js';


$(document).ready(function () {

    let yaDirectEditor = CodeMirror.fromTextArea(document.getElementById("ya_direct"), {
        mode: "text",
        lineNumbers: true,
        readOnly: true
    });

    let googleTagManagerEditor = CodeMirror.fromTextArea(document.getElementById('google_tag_manager'), {
        mode: "text/html",
        lineNumbers: true,
        readOnly: true
    });

    let googleAnalyticsEditor = CodeMirror.fromTextArea(document.getElementById('google_analytics'), {
        mode: "text/html",
        lineNumbers: true,
        readOnly: true
    });

    let yaMetricaEditor = CodeMirror.fromTextArea(document.getElementById('ya_metrica'), {
        mode: "text/html",
        lineNumbers: true,
        readOnly: true
    });
});

