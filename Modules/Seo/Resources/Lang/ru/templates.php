<?php

return [
	'buttons' => [
		'add' => 'Добавить'
	],
	'table'   => [
		'id'                 => 'ID',
		'title'              => 'Meta заголовок',
		'description'        => 'Meta описание',
		'keywords'           => 'Ключевый слова',
		'seoble_type'        => 'Тип записи',
		'seoble_id'          => 'ID записи',
		'canonical_url'      => 'Каноническая ссылка',
		'ya_direct'          => 'Яндекс.Директ',
		'google_tag_manager' => 'Google Tag Manager',
		'google_analytics'   => 'Google Analytics',
		'ya_metrica'         => 'Яндекс.Метрика',
		'og_title'           => 'Open Graph заголовок',
		'og_description'     => 'Open Graph описание',
		'og_image'           => 'Open Graph картинка',
		'og_type'            => 'Open Graph тип',
		'og_url'             => 'Open Graph ссылка',
		'action'             => 'Действия'
	]
];
