<?php

namespace Modules\Seo\Http\Controllers;

use App\DataTables\SeoDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Seo\Http\Requests\CreateSeoRequest;
use Modules\Seo\Http\Requests\UpdateSeoRequest;
use Modules\Seo\Services\SeoWebService;
use Response;

/**
 * Class SeoController
 *
 * @package Modules\Seo\Http\Controllers
 */
class SeoController extends Controller
{
    /** @var SeoWebService */
    private $seoWebService;

    /**
     * SeoController constructor.
     *
     * @param  SeoWebService  $service
     *
     * @return void
     */
    public function __construct(SeoWebService $service)
    {
        $this->seoWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Seo.
	 *
	 * @param  \App\DataTables\SeoDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(SeoDataTable $dataTable)
    {
        return $this->seoWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Seo.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->seoWebService->create();
    }

    /**
     * Store a newly created Seo in storage.
     *
     * @param  CreateSeoRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateSeoRequest $request): RedirectResponse
    {
        return $this->seoWebService->store($request);
    }

    /**
     * Display the specified Seo.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $id): View
    {
        return $this->seoWebService->show($id);
    }

    /**
     * Show the form for editing the specified Seo.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->seoWebService->edit($id);
    }

    /**
     * Update the specified Seo in storage.
     *
     * @param  int               $id
     * @param  UpdateSeoRequest  $request
     *
     * @return RedirectResponse|Response
     */
    public function update(int $id, UpdateSeoRequest $request): RedirectResponse
    {
        return $this->seoWebService->update($id, $request);
    }

    /**
     * Remove the specified Seo from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->seoWebService->destroy($id);
    }
}
