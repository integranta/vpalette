<?php

namespace Modules\Seo\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Seo\Http\Requests\API\V1\CreateSeoAPIRequest;
use Modules\Seo\Http\Requests\API\V1\UpdateSeoAPIRequest;
use Modules\Seo\Services\API\V1\SeoApiService;

/**
 * Class SeoController
 *
 * @package Modules\Seo\Http\Controllers\API\V1
 */
class SeoAPIController extends Controller
{
    /** @var SeoApiService */
    private $seoApiService;

    /**
     * SeoAPIController constructor.
     *
     * @param  SeoApiService  $service
     */
    public function __construct(SeoApiService $service)
    {
        $this->seoApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }


    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->seoApiService->index($request);
    }

    /**
     * @param  CreateSeoAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateSeoAPIRequest $request): JsonResponse
    {
        return $this->seoApiService->store($request);
    }


    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->seoApiService->show($id);
    }

    /**
     * @param  int                  $id
     * @param  UpdateSeoAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateSeoAPIRequest $request): JsonResponse
    {
        return $this->seoApiService->update($id, $request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->seoApiService->destroy($id);
    }
}
