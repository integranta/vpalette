<?php

namespace Modules\Seo\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Seo\Models\Seo;

/**
 * Class UpdateSeoAPIRequest
 *
 * @package Modules\Seo\Http\Requests\API\V1
 */
class UpdateSeoAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Seo::$rules;
    }
}
