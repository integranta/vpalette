<?php

namespace Modules\Seo\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SeoResource
 *
 * @package Modules\Seo\Transformers
 */
class SeoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'seoId' => $this->id,
            'metaTitle' => $this->title,
            'metaDescription' => $this->description,
            'metaKeywords' => $this->keywords,
            'canonicalUri' => $this->canonical_url,
            'googleTagManagerCode' => $this->google_tag_manager,
            'googleAnalyticsCode' => $this->google_analytics,
            'yandexDirectCode' => $this->ya_direct,
            'yandexMetrikaCode' => $this->ya_metrica,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
