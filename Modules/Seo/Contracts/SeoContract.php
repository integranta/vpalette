<?php


namespace Modules\Seo\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Seo\Models\Seo;

/**
 * Interface SeoContract
 *
 * @package Modules\Seo\Contracts
 */
interface SeoContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listSeo(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Seo
     */
    public function findSeoById(int $id): ?Seo;
    
    /**
     * @param  array  $data
     *
     * @return Seo
     */
    public function findSeoBy(array $data): ?Seo;
    
    /**
     * @param  array  $params
     *
     * @return Seo
     */
    public function createSeo(array $params): Seo;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateSeo(array $params, int $id): bool;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteSeo(int $id): bool;
}
