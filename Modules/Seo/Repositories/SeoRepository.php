<?php

namespace Modules\Seo\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Modules\Seo\Contracts\SeoContract;
use Modules\Seo\Models\Seo;
use Str;

/**
 * Class SeoRepository
 *
 * @package Modules\Seo\Repositories
 * @version August 2, 2019, 8:34 am UTC
 */
class SeoRepository extends BaseRepository implements SeoContract
{
    /**
     * SeoRepository constructor.
     *
     * @param  Seo  $model
     */
    public function __construct(Seo $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listSeo(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('seo.cache_key').$postfix,
            config('seo.cache_ttl'),
            function() use ($columns, $order, $sort) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return Seo
     */
    public function findSeoById(int $id): ?Seo
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $data
     *
     * @return Seo
     */
    public function findSeoBy(array $data): ?Seo
    {
        return $this->findBy($data);
    }
    
    /**
     * @param  array  $params
     *
     * @return Seo
     */
    public function createSeo(array $params): Seo
    {
        return $this->create($params);
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateSeo(array $params, int $id): bool
    {
        return $this->update($params, $id);
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteSeo(int $id): bool
    {
        return $this->delete($id);
    }
}
