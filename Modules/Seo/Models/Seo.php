<?php

namespace Modules\Seo\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Seo
 *
 * @package Modules\Seo\Models
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property string|null $seobleType
 * @property int|null $seobleId
 * @property string|null $canonicalUrl
 * @property string|null $googleTagManager
 * @property string|null $yaDirect
 * @property string|null $googleAnalytics
 * @property string|null $yaMetrica
 * @property string|null $ogTitle
 * @property string|null $ogDescription
 * @property string|null $ogImage
 * @property string|null $ogType
 * @property string|null $ogUrl
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $seoble
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Seo\Models\Seo onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereCanonicalUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereGoogleAnalytics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereGoogleTagManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereOgDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereOgImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereOgTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereOgType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereOgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereSeobleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereSeobleType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereYaDirect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Seo\Models\Seo whereYaMetrica($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Seo\Models\Seo withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Seo\Models\Seo withoutTrashed()
 * @mixin \Eloquent
 */
class Seo extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'seos';


    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'keywords',
        'seoble_type',
        'seoble_id',
        'canonical_url',
        'ya_direct',
        'google_tag_manager',
        'google_analytics',
        'ya_metrica',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
        'og_url'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'keywords' => 'string',
        'seoble_type' => 'string',
        'canonical_url' => 'string',
        'ya_direct' => 'string',
        'google_tag_manager' => 'string',
        'google_analytics' => 'string',
        'ya_metrica' => 'string',
        'og_title' => 'string',
        'og_description' => 'string',
        'og_image' => 'string',
        'og_type' => 'string',
        'og_url' => 'string'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'title' => 'nullable|string',
        'description' => 'nullable|string',
        'keywords' => 'nullable|string',
        'canonical_url' => 'nullable|string',
        'ya_direct' => 'nullable|string',
        'google_tag_manager' => 'nullable|string',
        'google_analytics' => 'nullable|string',
        'ya_metrica' => 'nullable|string',
        'og_title' => 'nullable|string',
        'og_description' => 'nullable|string',
        'og_image' => 'nullable|string',
        'og_type' => 'nullable|string',
        'og_url' => 'nullable|string'
    ];

    /**
     * Получить все модели, владеющие seoble.
     *
     * @return MorphTo
     */
    public function seoble(): MorphTo
    {
        return $this->morphTo();
    }
}
