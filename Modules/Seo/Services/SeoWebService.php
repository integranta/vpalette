<?php


namespace Modules\Seo\Services;


use App\DataTables\SeoDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Modules\Category\Models\Category;
use Modules\News\Models\News;
use Modules\Page\Models\Page;
use Modules\Product\Models\Product;
use Modules\Seo\Contracts\SeoContract;
use Modules\Seo\Http\Requests\CreateSeoRequest;
use Modules\Seo\Http\Requests\UpdateSeoRequest;
use Modules\Seo\Models\Seo;
use Modules\Stock\Models\Stock;

/**
 * Class SeoWebService
 *
 * @package Modules\Seo\Services
 */
class SeoWebService extends BaseService
{
	/** @var  SeoContract */
	private $seoRepository;
	
	/**
	 * SeoController constructor.
	 *
	 * @param  SeoContract  $seoRepo
	 */
	public function __construct(SeoContract $seoRepo)
	{
		$this->seoRepository = $seoRepo;
	}
	
	/**
	 * Display a listing of the Seo.
	 *
	 * @param  \App\DataTables\SeoDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(SeoDataTable $dataTable)
	{
		$this->setPageTitle(
			__('seo::messages.title'),
			__('seo::messages.index_subtitle'),
			__('seo::messages.index_leadtext')
		);
		
		return $dataTable->render('seo::seos.index');
	}
	
	/**
	 * Show the form for creating a new Seo.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$this->setPageTitle(
			__('seo::messages.title'),
			__('seo::messages.create_subtitle'),
			__('seo::messages.create_leadtext')
		);
		
		return view('seo::seos.create');
	}
	
	/**
	 * Store a newly created Seo in storage.
	 *
	 * @param  CreateSeoRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateSeoRequest $request): RedirectResponse
	{
		$seo = $this->seoRepository->createSeo($request->all());
		
		if (!$seo) {
			return $this->responseRedirectBack(
				__('seo::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.seo.index',
			__('seo::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified Seo.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(int $id): View
	{
		$seo = $this->seoRepository->findSeoById($id);
		
		if ($seo === null) {
			return $this->responseRedirect(
				'admin.seo.index',
				__('seo::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$name = $this->getName($seo);
		
		
		$this->setPageTitle(
			__('seo::messages.title'),
			__('seo::messages.show_subtitle', ['name' => $name]),
			__('seo::messages.show_leadtext', ['name' => $name])
		);
		
		return view('seo::seos.show', compact('seo'));
	}
	
	/**
	 * @param  \Modules\Seo\Models\Seo  $seo
	 *
	 * @return string
	 */
	private function getName(Seo $seo): string
	{
		return [
			Product::class  => Product::firstWhere('id', '=', $seo->seobleId)->name,
			Category::class => Category::firstWhere('id', '=', $seo->seobleId)->name,
			News::class     => News::firstWhere('id', '=', $seo->seobleId)->name,
			Stock::class    => Stock::firstWhere('id', '=', $seo->seobleType)->name,
			Page::class     => Page::firstWhere('id', '=', $seo->seobleType)->name
		][$seo->seobleType];
	}
	
	/**
	 * Show the form for editing the specified Seo.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$seo = $this->seoRepository->findSeoById($id);
		
		if ($seo === null) {
			return $this->responseRedirect(
				'admin.seo.index',
				__('seo::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$name = $this->getName($seo);
		
		$this->setPageTitle(
			__('seo::messages.title'),
			__('seo::messages.edit_subtitle', ['name' => $name]),
			__('seo::messages.edit_leadtext', ['name' => $name])
		);
		
		return view('seo::seos.edit', compact('seo'));
	}
	
	/**
	 * Update the specified Seo in storage.
	 *
	 * @param  int               $id
	 * @param  UpdateSeoRequest  $request
	 *
	 * @return RedirectResponse|Response
	 */
	public function update(int $id, UpdateSeoRequest $request): RedirectResponse
	{
		$seo = $this->seoRepository->findSeoById($id);
		
		if ($seo === null) {
			return $this->responseRedirect(
				'admin.seo.index',
				__('seo::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$seo = $this->seoRepository->updateSeo($request->all(), $id);
		
		if (!$seo) {
			return $this->responseRedirectBack(
				__('seo::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.seo.index',
			__('seo::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified Seo from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 *
	 */
	public function destroy(int $id): RedirectResponse
	{
		$seo = $this->seoRepository->findSeoById($id);
		
		if ($seo === null) {
			return $this->responseRedirect(
				'admin.seo.index',
				__('seo::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$seo = $this->seoRepository->deleteSeo($id);
		
		if (!$seo) {
			return $this->responseRedirectBack(
				__('seo::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.seo.index',
			__('seo::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}