<?php


namespace Modules\Seo\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Seo\Contracts\SeoContract;
use Modules\Seo\Http\Requests\API\V1\CreateSeoAPIRequest;
use Modules\Seo\Http\Requests\API\V1\UpdateSeoAPIRequest;
use Modules\Seo\Transformers\SeoResource;

/**
 * Class SeoApiService
 *
 * @package Modules\Seo\Services\API\V1
 */
class SeoApiService extends BaseService
{
    /** @var  SeoContract */
    private $seoRepository;
    
    /**
     * SeoAPIController constructor.
     *
     * @param  SeoContract  $seoRepo
     */
    public function __construct(SeoContract $seoRepo)
    {
        $this->seoRepository = $seoRepo;
    }
    
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $seos = SeoResource::collection($this->seoRepository->listSeo());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('seo::messages.success_retrieve'),
            $seos
        );
    }
    
    /**
     * @param  CreateSeoAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateSeoAPIRequest $request): JsonResponse
    {
        $seo = $this->seoRepository->createSeo($request->all());
        
        if (!$seo) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('seo::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('seo::messages.success_create'),
            $seo
        );
    }
    
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $seo = SeoResource::make($this->seoRepository->findSeoById($id));
        
        if ($seo === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('seo::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('seo::messages.success_found', ['id' => $id]),
            $seo
        );
    }
    
    /**
     * @param  int                  $id
     * @param  UpdateSeoAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateSeoAPIRequest $request): JsonResponse
    {
        $seo = $this->seoRepository->findSeoById($id);
        
        if ($seo === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('seo::messages.not_found', ['id' => $id])
            );
        }
        
        $seo = $this->seoRepository->updateSeo($request->all(), $id);
        
        if (!$seo) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('seo::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('seo::messages.success_update'),
            $seo
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $seo = $this->seoRepository->findSeoById($id);
        
        if ($seo === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('seo::messages.not_found', ['id' => $id])
            );
        }
        
        $seo = $this->seoRepository->deleteSeo($id);
        
        if (!$seo) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('seo::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('seo::messages.success_delete'),
            $seo
        );
    }
}
