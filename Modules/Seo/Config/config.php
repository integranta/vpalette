<?php

return [
    'name' => 'Seo',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all products as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => config('SEO_CACHE_KEY', 'index_seo'),
    'cache_ttl' => config('SEO_CACHE_TTL', 36000)
];
