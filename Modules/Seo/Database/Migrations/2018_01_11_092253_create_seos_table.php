<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateSeosTable
 */
class CreateSeosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('keywords')->nullable();
            $table->string('seoble_type')->nullable();
            $table->unsignedBigInteger('seoble_id')->nullable()->index();
            $table->longText('canonical_url')->nullable();
            $table->longText('google_tag_manager')->nullable();
            $table->longText('ya_direct')->nullable();
            $table->longText('google_analytics')->nullable();
            $table->longText('ya_metrica')->nullable();

            // Open Graph
            $table->longText('og_title')->nullable();
            $table->longText('og_description')->nullable();
            $table->longText('og_image')->nullable();
            $table->longText('og_type')->nullable();
            $table->longText('og_url')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seos');
    }
}
