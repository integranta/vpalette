<?php

namespace Modules\Seo\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

/**
 * Class SeoDatabaseSeeder
 *
 * @package Modules\Seo\Database\Seeders
 */
class SeoDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        $this->call("SeoTableSeeder");
    }
}
