<?php

namespace Modules\Seo\Database\Seeders;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Seo\Models\Seo;

/**
 * Class SeoTableSeeder
 *
 * @package Modules\Seo\Database\Seeders
 */
class SeoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        DB::table('seos')->delete();
        
        Seo::insert([
            'active' => true,
            'sort' => 100,
            'title' => 'ВПалитре - Интернет-портал',
            'description' => 'ВПалитре - Интернет-портал',
            'keywords' => 'краски, интернет-магазин, мольберты, кисти, краски',
            'canonical_url' => '',
            'ya_direct' => '',
            'google_tag_manager' => '',
            'google_analytics' => '',
            'ya_metrica' => ''
        ]);
    }
}
