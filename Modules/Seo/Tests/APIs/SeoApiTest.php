<?php namespace Modules\Seo\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Seo\Tests\ApiTestTrait;
use Modules\Seo\Tests\TestCase;
use Modules\Seo\Tests\Traits\MakeSeoTrait;

/**
 * Class SeoApiTest
 *
 * @package Modules\Seo\Tests\APIs
 */
class SeoApiTest extends TestCase
{
    use MakeSeoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_seo()
    {
        $seo = $this->fakeSeoData();
        $this->response = $this->json('POST', '/api/seos', $seo);
        
        $this->assertApiResponse($seo);
    }
    
    /**
     * @test
     */
    public function test_read_seo()
    {
        $seo = $this->makeSeo();
        $this->response = $this->json('GET', '/api/seos/'.$seo->id);
        
        $this->assertApiResponse($seo->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_seo()
    {
        $seo = $this->makeSeo();
        $editedSeo = $this->fakeSeoData();
        
        $this->response = $this->json('PUT', '/api/seos/'.$seo->id, $editedSeo);
        
        $this->assertApiResponse($editedSeo);
    }
    
    /**
     * @test
     */
    public function test_delete_seo()
    {
        $seo = $this->makeSeo();
        $this->response = $this->json('DELETE', '/api/seos/'.$seo->id);
        
        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/seos/'.$seo->id);
        
        $this->response->assertStatus(404);
    }
}
