<?php namespace Modules\Seo\Tests\Repositories;

use App;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\Seo\Models\Seo;
use Modules\Seo\Repositories\SeoRepository;
use Modules\Seo\Tests\ApiTestTrait;
use Modules\Seo\Tests\TestCase;
use Modules\Seo\Tests\Traits\MakeSeoTrait;

/**
 * Class SeoRepositoryTest
 *
 * @package Modules\Seo\Tests\Repositories
 */
class SeoRepositoryTest extends TestCase
{
    use MakeSeoTrait, ApiTestTrait, DatabaseTransactions;
    
    /**
     * @var SeoRepository
     */
    protected $seoRepo;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->seoRepo = App::make(SeoRepository::class);
    }
    
    /**
     * @test create
     */
    public function test_create_seo()
    {
        $seo = $this->fakeSeoData();
        $createdSeo = $this->seoRepo->create($seo);
        $createdSeo = $createdSeo->toArray();
        $this->assertArrayHasKey('id', $createdSeo);
        $this->assertNotNull($createdSeo['id'], 'Created Seo must have id specified');
        $this->assertNotNull(Seo::find($createdSeo['id']), 'Seo with given id must be in DB');
        $this->assertModelData($seo, $createdSeo);
    }
    
    /**
     * @test read
     */
    public function test_read_seo()
    {
        $seo = $this->makeSeo();
        $dbSeo = $this->seoRepo->find($seo->id);
        $dbSeo = $dbSeo->toArray();
        $this->assertModelData($seo->toArray(), $dbSeo);
    }
    
    /**
     * @test update
     */
    public function test_update_seo()
    {
        $seo = $this->makeSeo();
        $fakeSeo = $this->fakeSeoData();
        $updatedSeo = $this->seoRepo->update($fakeSeo, $seo->id);
        $this->assertModelData($fakeSeo, $updatedSeo->toArray());
        $dbSeo = $this->seoRepo->find($seo->id);
        $this->assertModelData($fakeSeo, $dbSeo->toArray());
    }
    
    /**
     * @test delete
     */
    public function test_delete_seo()
    {
        $seo = $this->makeSeo();
        $resp = $this->seoRepo->delete($seo->id);
        $this->assertTrue($resp);
        $this->assertNull(Seo::find($seo->id), 'Seo should not exist in DB');
    }
}
