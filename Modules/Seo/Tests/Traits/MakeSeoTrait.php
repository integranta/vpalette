<?php namespace Modules\Seo\Tests\Traits;

use App;
use Faker\Factory as Faker;
use Modules\Seo\Models\Seo;
use Modules\Seo\Repositories\SeoRepository;

/**
 * Trait MakeSeoTrait
 *
 * @package Modules\Seo\Tests\Traits
 */
trait MakeSeoTrait
{
    /**
     * Create fake instance of Seo and save it in database
     *
     * @param  array  $seoFields
     *
     * @return Seo
     */
    public function makeSeo($seoFields = [])
    {
        /** @var SeoRepository $seoRepo */
        $seoRepo = App::make(SeoRepository::class);
        $theme = $this->fakeSeoData($seoFields);
        return $seoRepo->create($theme);
    }
    
    /**
     * Get fake instance of Seo
     *
     * @param  array  $seoFields
     *
     * @return Seo
     */
    public function fakeSeo($seoFields = [])
    {
        return new Seo($this->fakeSeoData($seoFields));
    }
    
    /**
     * Get fake data of Seo
     *
     * @param  array  $seoFields
     *
     * @return array
     */
    public function fakeSeoData($seoFields = [])
    {
        $fake = Faker::create();
        
        return array_merge([
            'title' => $fake->word,
            'description' => $fake->word,
            'keywords' => $fake->word,
            'seoble_type' => $fake->word,
            'seoble_id' => $fake->word,
            'canonical_url' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $seoFields);
    }
}
