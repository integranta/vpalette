<?php

namespace Modules\Seo\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Seo\Models\Seo;

/**
 * Class SeoPolicy
 *
 * @package Modules\Seo\Policies
 */
class SeoPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the seo.
     *
     * @param  User|null  $user
     * @param  Seo        $seo
     *
     * @return mixed
     */
    public function view(?User $user, Seo $seo)
    {
        if ($seo->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished seo')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create seo.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create seo')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the seo.
     *
     * @param  User  $user
     * @param  Seo   $seo
     *
     * @return mixed
     */
    public function update(User $user, Seo $seo)
    {
        if ($user->can('edit all seo')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the seo.
     *
     * @param  User  $user
     * @param  Seo   $seo
     *
     * @return mixed
     */
    public function delete(User $user, Seo $seo)
    {
        if ($user->can('delete any seo')) {
            return true;
        }
        
        return false;
    }
}
