<?php

namespace Modules\Seo\Observers;

use App\Traits\Cacheable;
use Modules\Seo\Models\Seo;

/**
 * Class SeoObserver
 *
 * @package Modules\Seo\Observers
 */
class SeoObserver
{
    use Cacheable;
    
    /**
     * Handle the seo "created" event.
     *
     * @param  Seo  $seo
     *
     * @return void
     */
    public function created(Seo $seo): void
    {
        $this->incrementCountItemsInCache($seo, config('seo.cache_key'));
    }
    
    /**
     * Handle the seo "updated" event.
     *
     * @param  Seo  $seo
     *
     * @return void
     */
    public function updated(Seo $seo): void
    {
        //
    }
    
    /**
     * Handle the seo "deleted" event.
     *
     * @param  Seo  $seo
     *
     * @return void
     */
    public function deleted(Seo $seo): void
    {
        $this->decrementCountItemsInCache($seo, config('seo.cache_key'));
    }
    
    /**
     * Handle the seo "restored" event.
     *
     * @param  Seo  $seo
     *
     * @return void
     */
    public function restored(Seo $seo): void
    {
        $this->incrementCountItemsInCache($seo, config('seo.cache_key'));
    }
    
    /**
     * Handle the seo "force deleted" event.
     *
     * @param  Seo  $seo
     *
     * @return void
     */
    public function forceDeleted(Seo $seo): void
    {
        $this->decrementCountItemsInCache($seo, config('seo.cache_key'));
    }
}
