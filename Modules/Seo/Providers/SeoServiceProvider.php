<?php

namespace Modules\Seo\Providers;

use Config;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;

/**
 * Class SeoServiceProvider
 *
 * @package Modules\Seo\Providers
 */
class SeoServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('seo.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'seo'
        );
    }
    
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/seo');
        
        $sourcePath = __DIR__.'/../Resources/Views';
        
        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');
        
        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path.'/modules/seo';
        }, Config::get('view.paths')), [$sourcePath]), 'seo');
    }
    
    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/seo');
        
        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'seo');
        } else {
            $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'seo');
        }
    }
    
    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__.'/../Database/Factories');
        }
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
