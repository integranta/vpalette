<?php

namespace Modules\Backup\Http\Controllers;

use App\Http\Controllers\Controller;
use Artisan;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class BackupController
 *
 * @package Modules\Backup\Http\Controllers
 */
class BackupController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index(): View
    {
        $disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        $files = $disk->files(config('backup.backup.name'));
        $backups = [];
        // make an array of backup files, with their filesize and creation date
        foreach ($files as $k => $f) {
            // only take the zip files into account
            if (substr($f, -4) === '.zip' && $disk->exists($f)) {
                $backups[] = [
                    'file_path' => $f,
                    'file_name' => str_replace(config('backup.backup.name').'/', '', $f),
                    'file_size' => $disk->size($f),
                    'last_modified' => $disk->lastModified($f),
                ];
            }
        }
        // reverse the backups, so the newest one would be on top
        $backups = array_reverse($backups);
        return view('backup::index', compact('backups'));
    }
    
    /**
     * @return RedirectResponse
     */
    public function create(): RedirectResponse
    {
        try {
            // start the backup process
            Artisan::call('backup:run --disable-notifications');
            $output = Artisan::output();
            // log the results
            Log::info("[Backup MODULE] -- new backup started from admin interface \r\n".$output);
            // return the results as a response to the ajax call
            return $this->responseRedirectBack(
                __('backup::messages.success_create'),
                self::SUCCESS_RESPONSE,
                false,
                false
            );
        } catch (Exception $e) {
            return $this->responseRedirectBack(
                __('backup::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
    }
    
    /**
     * Downloads a backup zip file.
     *
     * @param  string  $file_name
     *
     * @return StreamedResponse|void
     */
    public function download(string $file_name): StreamedResponse
    {
        $file = config('backup.backup.name').'/'.$file_name;
        $disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        if ($disk->exists($file)) {
            return Storage::download($file);
        }
        
        return abort(Response::HTTP_NOT_FOUND, "The backup file doesn't exist.");
    }
    
    /**
     * Deletes a backup file.
     *
     * @param  string  $file_name
     *
     * @return RedirectResponse|void
     */
    public function delete(string $file_name): RedirectResponse
    {
        $disk = Storage::disk(config('backup.backup.destination.disks')[0]);
        if ($disk->exists(config('backup.backup.name').'/'.$file_name)) {
            $disk->delete(config('backup.backup.name').'/'.$file_name);
            return redirect()->back();
        }
        
        return abort(Response::HTTP_NOT_FOUND, "The backup file doesn't exist.");
    }
}
