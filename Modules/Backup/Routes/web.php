<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Backup\Http\Controllers\BackupController;

Route::prefix('backup')->group(function () {
	// Backup routes
	Route::get('/', [BackupController::class, 'index']);
	Route::get('create', [BackupController::class, 'create']);
	Route::get('download/{file_name}', [BackupController::class, 'download']);
	Route::get('delete/{file_name}', [BackupController::class, 'delete']);
});
