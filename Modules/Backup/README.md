Модуль Backup
============

Этот модуль для управления резервными копиями сайта.


#### Начало работы

```bash
php artisan backup:run
```

#### Доступные ключи
````
--filename[=FILENAME]
--only-db
--db-name[=DB-NAME] (multiple values allowed)
--only-files
--only-to-disk[=ONLY-TO-DISK]
--disable-notifications
````
