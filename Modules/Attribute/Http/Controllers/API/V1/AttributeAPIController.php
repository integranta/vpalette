<?php

namespace Modules\Attribute\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Attribute\Http\Requests\API\V1\CreateAttributeAPIRequest;
use Modules\Attribute\Http\Requests\API\V1\UpdateAttributeAPIRequest;
use Modules\Attribute\Services\API\V1\AttributeApiService;

/**
 * Class AttributeController
 *
 * @package Modules\Attribute\Http\Controllers\API\V1
 */
class AttributeAPIController extends Controller
{
    /** @var  AttributeApiService */
    private $attributeApiService;

    /**
     * AttributeAPIController constructor.
     *
     * @param  AttributeApiService  $service
     */
    public function __construct(AttributeApiService $service)
    {
        $this->attributeApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->attributeApiService->index($request);
    }

    /**
     * @param  CreateAttributeAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateAttributeAPIRequest $request): JsonResponse
    {
        return $this->attributeApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->attributeApiService->show($id);
    }


    /**
     * @param  int                        $id
     * @param  UpdateAttributeAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateAttributeAPIRequest $request): JsonResponse
    {
        return $this->attributeApiService->update($id, $request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->attributeApiService->destroy($id);
    }
}
