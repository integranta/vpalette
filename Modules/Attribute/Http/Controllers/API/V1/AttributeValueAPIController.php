<?php

namespace Modules\Attribute\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Attribute\Http\Requests\API\V1\CreateAttributeValueAPIRequest;
use Modules\Attribute\Http\Requests\API\V1\UpdateAttributeValueAPIRequest;
use Modules\Attribute\Services\API\V1\AttributeValueApiService;

/**
 * Class AttributeValueController
 *
 * @package Modules\Attribute\Http\Controllers\API\V1
 */
class AttributeValueAPIController extends Controller
{
    /** @var  AttributeValueApiService */
    private $attributeValueApiService;


    /**
     * AttributeValueAPIController constructor.
     *
     * @param  AttributeValueApiService  $attributeValueApiService
     *
     */
    public function __construct(AttributeValueApiService $attributeValueApiService)
    {
        $this->attributeValueApiService = $attributeValueApiService;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->attributeValueApiService->index($request);
    }

    /**
     * @param  CreateAttributeValueAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateAttributeValueAPIRequest $request): JsonResponse
    {
        return $this->attributeValueApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->attributeValueApiService->show($id);
    }

    /**
     * @param  int                             $id
     * @param  UpdateAttributeValueAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateAttributeValueAPIRequest $request): JsonResponse
    {
        return $this->attributeValueApiService->update($id, $request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->attributeValueApiService->destroy($id);
    }
}
