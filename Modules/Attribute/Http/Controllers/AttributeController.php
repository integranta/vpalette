<?php

namespace Modules\Attribute\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Attribute\Http\Requests\CreateAttributeRequest;
use Modules\Attribute\Http\Requests\UpdateAttributeRequest;
use Modules\Attribute\Services\AttributeWebService;

/**
 * Class AttributeController
 *
 * @package Modules\Attribute\Http\Controllers
 */
class AttributeController extends Controller
{
    /** @var AttributeWebService */
    private $attributeWebService;

    /**
     * AttributeController constructor.
     *
     * @param  AttributeWebService  $service
     */
    public function __construct(AttributeWebService $service)
    {
        $this->attributeWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the Attribute.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        return $this->attributeWebService->index();
    }

    /**
     * Show the form for creating a new Attribute.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->attributeWebService->create();
    }

    /**
     * Store a newly created Attribute in storage.
     *
     * @param  CreateAttributeRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateAttributeRequest $request): RedirectResponse
    {
        return $this->attributeWebService->store($request);
    }

    /**
     * Show the form for editing the specified Attribute.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->attributeWebService->edit($id);
    }

    /**
     * Update the specified Attribute in storage.
     *
     * @param  UpdateAttributeRequest  $request
     * @param  int                     $id
     *
     * @return RedirectResponse
     */
    public function update(UpdateAttributeRequest $request, int $id): RedirectResponse
    {
        return $this->attributeWebService->update($request, $id);
    }

    /**
     * Remove the specified Attribute from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->attributeWebService->destroy($id);
    }
}
