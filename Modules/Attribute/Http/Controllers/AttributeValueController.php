<?php

namespace Modules\Attribute\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Attribute\Services\AttributeValueWebService;

/**
 * Class AttributeValueController
 *
 * @package Modules\Attribute\Http\Controllers
 */
class AttributeValueController extends Controller
{
    /** @var AttributeValueWebService */
    private $attributeValueWebService;

    /**
     * AttributeValueController constructor.
     *
     * @param  AttributeValueWebService  $service
     */
    public function __construct(AttributeValueWebService $service)
    {
        $this->attributeValueWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Return all values for editable attribute like JSON.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function getValues(Request $request): JsonResponse
    {
        return $this->attributeValueWebService->getValues($request);
    }

    /**
     * Add new value for editable attribute.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function addValues(Request $request): JsonResponse
    {
        return $this->attributeValueWebService->addValues($request);
    }

    /**
     * Update value option editable attribute.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function updateValues(Request $request): JsonResponse
    {
        return $this->attributeValueWebService->updateValues($request);
    }

    /**
     * Delete value option editable attribute.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function deleteValues(Request $request): JsonResponse
    {
        return $this->attributeValueWebService->deleteValues($request);
    }
}
