<?php

namespace Modules\Attribute\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Attribute\Models\Attribute;

/**
 * Class CreateAttributeAPIRequest
 *
 * @package Modules\Attribute\Http\Requests\API\V1
 */
class CreateAttributeAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Attribute::$rules;
    }
}
