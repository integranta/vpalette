<?php

namespace Modules\Attribute\Observers;

use App\Traits\Cacheable;
use Modules\Attribute\Models\Attribute;

/**
 * Class AttributeObserver
 *
 * @package Modules\Attribute\Observers
 */
class AttributeObserver
{
    use Cacheable;
    
    /**
     * Handle the attribute "created" event.
     *
     * @param  Attribute  $attribute
     *
     * @return void
     */
    public function created(Attribute $attribute): void
    {
        $this->incrementCountItemsInCache($attribute, config('attribute.attribute.cache_key'));
    }
    
    /**
     * Handle the attribute "updated" event.
     *
     * @param  Attribute  $attribute
     *
     * @return void
     */
    public function updated(Attribute $attribute): void
    {
        //
    }
    
    /**
     * Handle the attribute "deleted" event.
     *
     * @param  Attribute  $attribute
     *
     * @return void
     */
    public function deleted(Attribute $attribute): void
    {
        $this->decrementCountItemsInCache($attribute, config('attribute.attribute.cache_key'));
    }
    
    /**
     * Handle the attribute "restored" event.
     *
     * @param  Attribute  $attribute
     *
     * @return void
     */
    public function restored(Attribute $attribute): void
    {
        $this->incrementCountItemsInCache($attribute, config('attribute.attribute.cache_key'));
    }
    
    /**
     * Handle the attribute "force deleted" event.
     *
     * @param  Attribute  $attribute
     *
     * @return void
     */
    public function forceDeleted(Attribute $attribute): void
    {
        $this->decrementCountItemsInCache($attribute, config('attribute.attribute.cache_key'));
    }
}
