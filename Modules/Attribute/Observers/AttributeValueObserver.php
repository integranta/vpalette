<?php

namespace Modules\Attribute\Observers;

use App\Traits\Cacheable;
use Modules\Attribute\Models\AttributeValue;

/**
 * Class AttributeValueObserver
 *
 * @package Modules\Attribute\Observers
 */
class AttributeValueObserver
{
    use Cacheable;
    
    /**
     * Handle the attribute value "created" event.
     *
     * @param  AttributeValue  $attributeValue
     *
     * @return void
     */
    public function created(AttributeValue $attributeValue): void
    {
        $this->incrementCountItemsInCache($attributeValue, config('attribute.attribute_value.cache_key'));
    }
    
    /**
     * Handle the attribute value "updated" event.
     *
     * @param  AttributeValue  $attributeValue
     *
     * @return void
     */
    public function updated(AttributeValue $attributeValue): void
    {
        //
    }
    
    /**
     * Handle the attribute value "deleted" event.
     *
     * @param  AttributeValue  $attributeValue
     *
     * @return void
     */
    public function deleted(AttributeValue $attributeValue): void
    {
        $this->decrementCountItemsInCache($attributeValue, config('attribute.attribute_value.cache_key'));
    }
    
    /**
     * Handle the attribute value "restored" event.
     *
     * @param  AttributeValue  $attributeValue
     *
     * @return void
     */
    public function restored(AttributeValue $attributeValue): void
    {
        $this->incrementCountItemsInCache($attributeValue, config('attribute.attribute_value.cache_key'));
    }
    
    /**
     * Handle the attribute value "force deleted" event.
     *
     * @param  AttributeValue  $attributeValue
     *
     * @return void
     */
    public function forceDeleted(AttributeValue $attributeValue): void
    {
        $this->decrementCountItemsInCache($attributeValue, config('attribute.attribute_value.cache_key'));
    }
}
