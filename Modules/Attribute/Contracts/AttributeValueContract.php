<?php


namespace Modules\Attribute\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Attribute\Models\AttributeValue;

/**
 * Interface AttributeValueContract
 *
 * @package Modules\Attribute\Contracts
 */
interface AttributeValueContract
{
    /**
     * Получить список значении для аттрибутов.
     *
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listAttributeValues(
        array $columns = ['*'],
        string $order = 'id',
        string $sort = 'desc'
    ): Collection;
    
    /**
     * Получить значение аттрибута по указанному ID.
     *
     * @param  int  $id
     *
     * @return AttributeValue
     */
    public function findAttributeValueById(int $id): ?AttributeValue;
    
    /**
     * Создать новое значение для аттрибутов.
     *
     * @param  array  $params
     *
     * @return AttributeValue
     */
    public function createAttributeValue(array $params): AttributeValue;
    
    /**
     * Обновить значения аттрибута по указанному ID.
     *
     * @param  array  $params
     * @param  int    $id
     *
     * @return AttributeValue
     */
    public function updateAttributeValue(array $params, int $id): AttributeValue;
    
    /**
     * Удалить значение аттрибута по указанному ID.
     *
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteAttributeValue(int $id): bool;
}