<?php


namespace Modules\Attribute\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Attribute\Models\Attribute;

/**
 * Interface AttributeContract
 *
 * @package Modules\Attribute\Contracts
 */
interface AttributeContract
{
    /**
     * Получить список всех атрибутов товаров.
     *
     * @param  array   $columns
     * @param  string  $order
     * @param  string  $sort
     *
     * @return Collection
     */
    public function listAttributes(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection;

    /**
     * Найти атрибут по указанному ID.
     *
     * @param  int  $id
     *
     * @return Attribute
     */
    public function findAttributeById(int $id): ?Attribute;

    /**
     * Создать новый аттрибут для товаров.
     *
     * @param  array  $params
     *
     * @return Attribute
     */
    public function createAttribute(array $params): Attribute;

    /**
     * Обновить аттрибут по указанному ID.
     *
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateAttribute(array $params, int $id): bool;

    /**
     * Удалить аттрибут по указанному ID.
     *
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteAttribute(int $id): bool;
}
