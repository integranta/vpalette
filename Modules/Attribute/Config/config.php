<?php

return [
    'name'            => 'Attribute',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all attributes as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'attribute'       => [
        'cache_key' => env('ATTRIBUTE_CACHE_ATTRIBUTE_KEY', 'index_attributes'),
        'cache_ttl' => env('ATTRIBUTE_CACHE_ATTRIBUTE_TTL', 360000),
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all values for attributes as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'attribute_value' => [
        'cache_key' => env('ATTRIBUTE_CACHE_ATTRIBUTE_VALUE_KEY', 'index_attribute_values'),
        'cache_ttl' => env('ATTRIBUTE_CACHE_ATTRIBUTE_VALUE_TTL', 360000)
    ]
];
