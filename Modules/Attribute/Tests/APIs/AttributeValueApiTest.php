<?php namespace Modules\Attribute\Tests\APIs;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Modules\Attribute\Models\AttributeValue;
use Modules\Attribute\Tests\ApiTestTrait;
use Modules\Attribute\Tests\TestCase;

/**
 * Class AttributeValueApiTest
 *
 * @package Modules\Attribute\Tests\APIs
 */
class AttributeValueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;
    
    /**
     * @test
     */
    public function test_create_attribute_value()
    {
        $attributeValue = factory(AttributeValue::class)->make()->toArray();
        
        $this->response = $this->json(
            'POST',
            '/api/attribute_values', $attributeValue
        );
        
        $this->assertApiResponse($attributeValue);
    }
    
    /**
     * @test
     */
    public function test_read_attribute_value()
    {
        $attributeValue = factory(AttributeValue::class)->create();
        
        $this->response = $this->json(
            'GET',
            '/api/attribute_values/'.$attributeValue->id
        );
        
        $this->assertApiResponse($attributeValue->toArray());
    }
    
    /**
     * @test
     */
    public function test_update_attribute_value()
    {
        $attributeValue = factory(AttributeValue::class)->create();
        $editedAttributeValue = factory(AttributeValue::class)->make()->toArray();
        
        $this->response = $this->json(
            'PUT',
            '/api/attribute_values/'.$attributeValue->id,
            $editedAttributeValue
        );
        
        $this->assertApiResponse($editedAttributeValue);
    }
    
    /**
     * @test
     */
    public function test_delete_attribute_value()
    {
        $attributeValue = factory(AttributeValue::class)->create();
        
        $this->response = $this->json(
            'DELETE',
            '/api/attribute_values/'.$attributeValue->id
        );
        
        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/attribute_values/'.$attributeValue->id
        );
        
        $this->response->assertStatus(404);
    }
}
