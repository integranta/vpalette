<div class="table-responsive">
    <table class="table" id="attributes-table">
        <thead>
        <tr>
            <th>Код</th>
            <th>Название</th>
            <th>Тип</th>
            <th class="text-center">Фильтруемое</th>
            <th class="text-center">Обязательное</th>
            <th style="width:100px; min-width:100px;" class="text-center">Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($attributes as $attribute)
            <tr>
                <td>{!! $attribute->code !!}</td>
                <td>{!! $attribute->name !!}</td>
                <td>{!! $attribute->frontend_type !!}</td>
                <td class="text-center">
                    @if ($attribute->is_filterable == 1)
                        <span class="badge badge-success">Да</span>
                    @else
                        <span class="badge badge-danger">Нет</span>
                    @endif
                </td>
                <td class="text-center">
                    @if ($attribute->is_required == 1)
                        <span class="badge badge-success">Да</span>
                    @else
                        <span class="badge badge-danger">Нет</span>
                    @endif
                </td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="CRUD операции">
                        {!! Form::open([
								'route' => ['admin.attributes.destroy', $attribute->id],
								'method' => 'delete'
							])
						!!}
                        {{--<a
                            href="{{ route('admin.attributes.show', $attribute->id) }}"
                            class="btn btn-sm btn-success">
                            <i class="fa fa-eye"></i>
                        </a>--}}
                        @can('edit all attributes')
                            <a
                                href="{{ route('admin.attributes.edit', $attribute->id) }}"
                                class="btn btn-sm btn-primary">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endcan
                        @can('delete all attributes')
                            {!! Form::button(
                                '<i class="fa fa-trash"></i>',
                                [
                                    'type' => 'submit',
                                    'class' => 'btn btn-sm btn-danger',
                                    'title' => 'Удалить',
                                    'onclick' => "return confirm('Вы уверены?')"
                                ])
                            !!}
                        @endcan
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
