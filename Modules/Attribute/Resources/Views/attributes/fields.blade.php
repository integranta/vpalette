<div class="form-group row mb-4">
    <label
        for="code"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
    >
        Код
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            class="form-control"
            type="text"
            placeholder="Введите код атрибута"
            id="code"
            name="code"
            value="{{ old('code') }}"
        />
    </div>
</div>


<div class="form-group row mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="name"
    >
        Название
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            class="form-control"
            type="text"
            placeholder="Введите название атрибута"
            id="name"
            name="name"
            value="{{ old('name') }}"
        />
    </div>
</div>
<div class="form-group row mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="frontend_type"
    >
        Тип
    </label>
    <div class="col-sm-12 col-md-7">
        <select name="frontend_type" id="frontend_type" class="form-control">
            @foreach($types as $key => $label)
                <option value="{{ $key }}">{{ $label }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Фильтруемое</span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        <input class="custom-control-input" type="checkbox" id="is_filterable" name="is_filterable"/>
        <label class="custom-control-label ml-lg-3" for="is_filterable"></label>
    </div>
</div>
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Обязательное</span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        <input class="custom-control-input" type="checkbox" id="is_required" name="is_required"/>
        <label class="custom-control-label ml-lg-3" for="is_required"></label>
    </div>
</div>
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Показывать раскрытым</span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        <input class="custom-control-input" type="checkbox" id="show_expanded" name="show_expanded"/>
        <label class="custom-control-label ml-lg-3" for="show_expanded"></label>
    </div>
</div>

<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="categories">Категория
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="categories"
            class="form-control"
            name="categories[]"
            multiple
        >
            <option value="0">Выберите родительскую категорию</option>
            @foreach($categories as $id => $category)
                <option value="{{ $id }}"> {{ $category }} </option>
            @endforeach
        </select>
    </div>
</div>
