<div class="form-group row mb-4">
    <label
        for="code"
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
    >
        Код
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            class="form-control"
            type="text"
            placeholder="Введите код атрибута"
            id="code"
            name="code"
            value="{{ old('code', $attribute->code) }}"
        />
    </div>
</div>


<div class="form-group row mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="name"
    >
        Название
    </label>
    <div class="col-sm-12 col-md-7">
        <input
            class="form-control"
            type="text"
            placeholder="Введите название атрибута"
            id="name"
            name="name"
            value="{{ old('name', $attribute->name) }}"
        />
    </div>
</div>
<div class="form-group row mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="frontend_type"
    >
        Тип
    </label>
    @php

    @endphp
    <div class="col-sm-12 col-md-7">
        <select name="frontend_type" id="frontend_type" class="form-control">
            @foreach($types as $key => $label)
                @if(\App\Enums\FrontendTypeAttributes::fromValue($attribute->frontend_type)->value === $key)
                    <option value="{{ $key }}" selected>{{ $label }}</option>
                @else
                    <option value="{{ $key }}">{{ $label }}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Фильтруемое</span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        <input
            class="custom-control-input"
            type="checkbox"
            id="is_filterable"
            name="is_filterable"
            {{ $attribute->is_filterable === true ? 'checked' : '' }}
        />
        <label class="custom-control-label ml-lg-3" for="is_filterable"></label>
    </div>
</div>
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Обязательное</span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        <input
            class="custom-control-input"
            type="checkbox"
            id="is_required"
            name="is_required"
            {{ $attribute->is_required === true ? 'checked' : '' }}
        />
        <label class="custom-control-label ml-lg-3" for="is_required"></label>
    </div>
</div>
<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Показать раскрытым в фильтре</span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        <input
            class="custom-control-input"
            type="checkbox"
            id="show_expanded"
            name="show_expanded"
            {{ $attribute->show_expanded === true ? 'checked' : '' }}
        />
        <label class="custom-control-label ml-lg-3" for="show_expanded"></label>
    </div>
</div>
<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="categories">Категория
    </label>
    <div class="col-sm-12 col-md-7">
        <select name="categories[]" id="categories" class="form-control" multiple>
            @foreach($categories as $id => $category)
                @php $check = in_array($id, $attribute->categories->pluck('id')->toArray()) ? 'selected' : ''@endphp
                <option value="{{ $id }}" {{ $check }}>
                    {{ $category }}
                </option>
            @endforeach
        </select>
    </div>
</div>


<input type="hidden" name="id" value="{{ $attribute->id }}">
