@extends('layouts.admin-master')

@section ('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('libs/css/select2/dist/css/select2.min.css') }}" xmlns="">
    <link rel="stylesheet" href="{{ asset('css/attribute.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.attributes.index') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
            @include('admin.partials.flash')
            <!-- Sidebar -->
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-pills flex-column">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#general" data-toggle="tab">Основные</a>
                                </li>
								@if(\App\Enums\FrontendTypeAttributes::fromValue($attribute->frontend_type)->value !== \App\Enums\FrontendTypeAttributes::TEXT)
									<li class="nav-item">
										<a class="nav-link" href="#values" data-toggle="tab">Значения атрибута</a>
									</li>
								@endif
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Content -->
                <div class="col-md-8">
                    <div class="tab-content">
                        <div class="tab-pane active pt-0" id="general">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">{{ $subTitle }}</h3>
                                        </div>
                                        <div class="card-body">
											{!! Form::model($attribute,
												[
													'route' => ['admin.attributes.update', $attribute->id],
													'method' => 'patch',
													'files' => 'true'
												])
											!!}
                                                @csrf

                                                @include('attribute::attributes.edit_fields')
                                                <div class="form-group row mb-4">
                                                    <label
                                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
													>
													</label>
                                                    <div class="col-sm-12 col-md-7">
                                                        <button
                                                            class="btn btn-primary"
                                                            type="submit"
                                                        >
                                                            Обновить атрибут
                                                        </button>
                                                    </div>
                                                </div>
											{!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="values">
                            <attribute-values
                                :attributeid="{{ $attribute->id }}"
                                attributetype="{{ $attribute->frontend_type }}"
                            ></attribute-values>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@push('scripts')
    <script src="{{ asset('libs/js/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/attribute.js') }}"></script>
@endpush
