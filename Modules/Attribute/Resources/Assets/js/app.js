import 'datatables';
import 'datatables.net-responsive-bs4';
import 'datatables.net-select-bs4';

$(function () {
    $('#categories').select2();
    $('#attributes-table').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        }
    });
});
