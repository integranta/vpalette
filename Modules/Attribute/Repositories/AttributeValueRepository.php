<?php

namespace Modules\Attribute\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Modules\Attribute\Contracts\AttributeValueContract;
use Modules\Attribute\Models\AttributeValue;
use Str;

/**
 * Class AttributeValueRepository
 *
 * @package Modules\Attribute\Repositories
 * @version September 23, 2019, 10:28 pm MSK
 */
class AttributeValueRepository extends BaseRepository implements AttributeValueContract
{
    /**
     * BaseRepository constructor.
     *
     * @param  AttributeValue  $model
     */
    public function __construct(AttributeValue $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listAttributeValues(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('attribute.attribute_value.cache_key').$postfix,
            config('attribute.attribute_value.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @inheritDoc
     */
    public function findAttributeValueById(int $id): ?AttributeValue
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function createAttributeValue(array $params): AttributeValue
    {
        return $this->create($params);
    }
    
    /**
     * @inheritDoc
     */
    public function updateAttributeValue(array $params, int $id): AttributeValue
    {
        return $this->update($params, $id);
    }
    
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function deleteAttributeValue(int $id): bool
    {
        return $this->delete($id);
    }
}
