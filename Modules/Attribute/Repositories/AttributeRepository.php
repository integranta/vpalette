<?php

namespace Modules\Attribute\Repositories;

use App\Repositories\BaseRepository;
use Cache;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use InvalidArgumentException;
use Modules\Attribute\Contracts\AttributeContract;
use Modules\Attribute\Models\Attribute;
use Str;

/**
 * Class AttributeRepository
 *
 * @package Modules\Attribute\Repositories
 * @version September 23, 2019, 5:15 pm MSK
 */
class AttributeRepository extends BaseRepository implements AttributeContract
{
    /**
     * AttributeRepository constructor.
     *
     * @param  Attribute  $model
     */
    public function __construct(Attribute $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }


    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listAttributes(array $columns = ['*'], string $order = 'id', string $sort = 'desc'): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('attribute.attribute.cache_key_attribute').$postfix,
            config('attribute.attribute.cache_ttl_attribute'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }

    /**
     * @param  int  $id
     *
     * @return Attribute
     */
    public function findAttributeById(int $id): ?Attribute
    {
        return $this->find($id);
    }

    /**
     * @param  array  $params
     *
     * @return Attribute
     */
    public function createAttribute(array $params): Attribute
    {
        try {
            $collection = collect($params);

            $is_filterable = $collection->has('is_filterable') ? 1 : 0;
            $is_required = $collection->has('is_required') ? 1 : 0;
            $show_expanded = $collection->has('show_expanded') ? 1 : 0;

            $merge = $collection->merge(compact('is_filterable', 'is_required', 'show_expanded'));

            $attribute = new Attribute($merge->all());

            $attribute->save();

            $attribute->categories()->sync($collection->get('categories'));

            return $attribute;

        } catch (QueryException $exception) {
            throw new InvalidArgumentException($exception->getMessage());
        }
    }

    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updateAttribute(array $params, int $id): bool
    {
        $attribute = $this->findAttributeById($id);

        if (!$attribute)
        {
            return false;
        }

        $collection = collect($params)->except('_token');

        $is_filterable = $collection->has('is_filterable') ? 1 : 0;
        $is_required = $collection->has('is_required') ? 1 : 0;
        $show_expanded = $collection->has('show_expanded') ? 1 : 0;

        $merge = $collection->merge(compact('is_filterable', 'is_required', 'show_expanded'));
        
        if ($attribute->update($merge->all())) {
            $attribute->categories()->sync($collection->get('categories'));
    
            return true;
        }
        
        return false;
    }

    /**
     * @param  int  $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteAttribute(int $id): bool
    {
        return $this->delete($id);
    }
}
