<?php

namespace Modules\Attribute\Models;

use App\Enums\FrontendTypeAttributes;
use BenSampo\Enum\Traits\CastsEnums;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Category\Models\Category;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Attribute
 *
 * @package Modules\Attribute\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $code
 * @property string $name
 * @property string $frontendType
 * @property bool $isFilterable
 * @property bool $isRequired
 * @property bool $showExpanded
 * @property int|null $min
 * @property int|null $max
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Category\Models\Category[] $categories
 * @property-read int|null $categoriesCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Attribute\Models\AttributeValue[] $values
 * @property-read int|null $valuesCount
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Attribute\Models\Attribute onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereFrontendType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereIsFilterable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereIsRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereShowExpanded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\Attribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Attribute\Models\Attribute withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Attribute\Models\Attribute withoutTrashed()
 * @mixin \Eloquent
 */
class Attribute extends Model
{
    use SoftDeletes;
    use CastsEnums;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'attributes';


    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'code',
        'name',
        'frontend_type',
        'is_filterable',
        'is_required',
        'show_expanded',
        'min',
        'max'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'boolean',
        'sort' => 'integer',
        'code' => 'string',
        'name' => 'string',
        'frontend_type' => 'string',
        'is_filterable' => 'boolean',
        'is_required' => 'boolean',
        'show_expanded' => 'boolean',
        'min' => 'integer',
        'max' => 'integer'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'frontend_type' => FrontendTypeAttributes::class
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required',
        'name' => 'required',
        'frontend_type' => 'required',
        'show_expanded' => 'nullable',
        'is_filterable' => 'nullable',
        'is_required' => 'nullable',
        'min' => 'nullable|integer',
        'max' => 'nullable|integer'
    ];

    /**
     * Получить все значения для атрибута.
     *
     * @return HasMany
     */
    public function values(): HasMany
    {
        return $this->hasMany(AttributeValue::class);
    }

    /**
     * Категории, принадлежащие этому атрибуту.
     *
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
