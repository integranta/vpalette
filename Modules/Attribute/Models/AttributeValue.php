<?php

namespace Modules\Attribute\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Product\Models\ProductAttribute;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class AttributeValue
 *
 * @package Modules\Attribute\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property int $attributeId
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Modules\Attribute\Models\Attribute $attribute
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Models\ProductAttribute[] $productAttributes
 * @property-read int|null $productAttributesCount
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Attribute\Models\AttributeValue onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Attribute\Models\AttributeValue whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Attribute\Models\AttributeValue withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Attribute\Models\AttributeValue withoutTrashed()
 * @mixin \Eloquent
 */
class AttributeValue extends Model
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью табилица.
     *
     * @var string
     */
    public $table = 'attribute_values';


    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'attribute_id',
        'value'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'value' => 'string',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'attribute_id' => 'required|integer|exists:attributes,id',
        'value' => 'required|string'
    ];


    /**
     * Получить атрибут, который принадлежит это значение.
     *
     * @return BelongsTo
     */
    public function attribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class);
    }

    /**
     * Атрибуты товаров, принадлежащие этому значению атрибута.
     *
     * @return BelongsToMany
     */
    public function productAttributes(): BelongsToMany
    {
        return $this->belongsToMany(
        	ProductAttribute::class,
			'product_attributes',
			'attribute_id',
			'product_id');
    }
}
