<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Attribute\Http\Controllers\API\V1\AttributeAPIController;
use Modules\Attribute\Http\Controllers\API\V1\AttributeValueAPIController;

Route::prefix('v1')->group(function () {
    Route::apiResource('attributes', AttributeAPIController::class);
    Route::apiResource('attribute_values', AttributeValueAPIController::class);
});
