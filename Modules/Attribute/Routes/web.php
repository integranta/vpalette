<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Attribute\Http\Controllers\AttributeController;
use Modules\Attribute\Http\Controllers\AttributeValueController;

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		Route::resource('attributes', AttributeController::class);
		
		Route::post('attributes/get-values', [AttributeValueController::class, 'getValues']);
		Route::post('attributes/add-values', [AttributeValueController::class, 'addValues']);
		Route::post('attributes/update-values', [AttributeValueController::class, 'updateValues']);
		Route::post('attributes/delete-values', [AttributeValueController::class, 'deleteValues']);
		
	});


