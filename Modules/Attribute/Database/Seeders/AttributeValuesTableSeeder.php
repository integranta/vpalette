<?php

namespace Modules\Attribute\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Attribute\Models\AttributeValue;

/**
 * Class AttributeValuesTableSeeder
 *
 * @package Modules\Attribute\Database\Seeders
 */
class AttributeValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = ['small', 'medium', 'large'];
        $colors = ['black', 'blue', 'red', 'orange'];
        
        foreach ($sizes as $size) {
            AttributeValue::create([
                'active' => true,
                'attribute_id' => 1,
                'value' => $size
            ]);
        }
        
        foreach ($colors as $color) {
            AttributeValue::create([
                'active' => true,
                'attribute_id' => 2,
                'value' => $color
            ]);
        }
    }
}
