<?php

namespace Modules\Attribute\Database\Seeders;

use App\Enums\FrontendTypeAttributes;
use Illuminate\Database\Seeder;
use Modules\Attribute\Models\Attribute;

/**
 * Class AttributeDatabaseSeeder
 *
 * @package Modules\Attribute\Database\Seeders
 */
class AttributeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a size attribute
        Attribute::create([
            'active' => true,
            'code' => 'size',
            'name' => 'Size',
            'frontend_type' => FrontendTypeAttributes::SELECT,
            'is_filterable' => 1,
            'is_required' => 1,
        ]);
        
        // Create a color attribute
        Attribute::create([
            'active' => true,
            'code' => 'color',
            'name' => 'Color',
            'frontend_type' => FrontendTypeAttributes::SELECT,
            'is_filterable' => 1,
            'is_required' => 1,
        ]);
    }
}
