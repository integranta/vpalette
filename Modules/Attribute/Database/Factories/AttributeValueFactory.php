<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\Attribute\Models\AttributeValue;
use Faker\Generator as Faker;

$factory->define(AttributeValue::class, function (Faker $faker) {
    
    return [
        'attribute_id' => $faker->word,
        'value' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
