<?php

use App\Enums\FrontendTypeAttributes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateAttributesTable
 */
class CreateAttributesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->string('code')->unique()->index();
            $table->string('name');
            $table->enum('frontend_type', FrontendTypeAttributes::getValues())->index();
            $table->boolean('is_filterable')->default(0);
            $table->boolean('is_required')->default(0);
            $table->boolean('show_expanded')->default(0);
            $table->unsignedInteger('min')->nullable();
            $table->unsignedInteger('max')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attributes');
    }
}
