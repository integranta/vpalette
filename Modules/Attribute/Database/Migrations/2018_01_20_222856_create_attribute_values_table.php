<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateAttributeValuesTable
 */
class CreateAttributeValuesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_values', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->unsignedBigInteger('attribute_id')->index();
            $table->foreign('attribute_id')
                ->references('id')
                ->on('attributes');
            $table->text('value');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attribute_values');
    }
}
