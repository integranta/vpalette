<?php


namespace Modules\Attribute\Services;


use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Attribute\Contracts\AttributeContract;
use Modules\Attribute\Http\Requests\CreateAttributeRequest;
use Modules\Attribute\Http\Requests\UpdateAttributeRequest;
use Modules\Attribute\Repositories\AttributeRepository;
use Modules\Category\Contracts\CategoryContract;

/**
 * Class AttributeWebService
 *
 * @package Modules\Attribute\Services
 */
class AttributeWebService extends BaseService
{
    
    /** @var  AttributeRepository */
    private $attributeRepository;
    
    /** @var CategoryContract */
    private $categoryRepository;
    
    /**
     * AttributeController constructor.
     *
     * @param  AttributeContract  $attributeRepo
     * @param  CategoryContract   $categoryRepo
     */
    public function __construct(
        AttributeContract $attributeRepo,
        CategoryContract $categoryRepo
    ) {
        $this->attributeRepository = $attributeRepo;
        $this->categoryRepository = $categoryRepo;
    }
    
    /**
     * Display a listing of the Attribute.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        $attributes = $this->attributeRepository->listAttributes();
        
        $this->setPageTitle(
            __('attribute::messages.title'),
            __('attribute::messages.index_subtitle'),
            __('attribute::messages.index_leadtext')
        );
        
        return view('attribute::attributes.index', compact('attributes'));
    }
    
    /**
     * Show the form for creating a new Attribute.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $categories = $this->categoryRepository->listCategories();
        $types = $this->getTypes();
        
        $this->setPageTitle(
            __('attribute::messages.title'),
            __('attribute::messages.create_subtitle'),
            __('attribute::messages.create_leadtext')
        );
        return view('attribute::attributes.create', compact('categories', 'types'));
    }
    
    /**
     * Store a newly created Attribute in storage.
     *
     * @param  CreateAttributeRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateAttributeRequest $request): RedirectResponse
    {
        $attribute = $this->attributeRepository->createAttribute($request->all());
        
        if (!$attribute) {
            return $this->responseRedirectBack(
                __('attribute::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.attributes.index',
            __('attribute::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Show the form for editing the specified Attribute.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        $attribute = $this->attributeRepository->findAttributeById($id);
        $categories = $this->categoryRepository->listCategories();
        $types = $this->getTypes();
        
        $this->setPageTitle(
            __('attribute::messages.title'),
            __('attribute::messages.edit_subtitle', ['name' => $attribute->name]),
            __('attribute::messages.edit_leadtext', ['name' => $attribute->name])
        );
        return view('attribute::attributes.edit', compact('attribute', 'categories', 'types'));
    }
    
    /**
     * Update the specified Attribute in storage.
     *
     * @param  UpdateAttributeRequest  $request
     * @param  int                     $id
     *
     * @return RedirectResponse
     */
    public function update(UpdateAttributeRequest $request, int $id): RedirectResponse
    {
        $attribute = $this->attributeRepository->updateAttribute($request->all(), $id);
        
        if (!$attribute) {
            return $this->responseRedirectBack(
                __('attribute::messages.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        return $this->responseRedirectBack(
            __('attribute::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified Attribute from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        $attribute = $this->attributeRepository->deleteAttribute($id);
        
        if (!$attribute) {
            return $this->responseRedirectBack(
                __('attribute::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        return $this->responseRedirect(
            'admin.attributes.index',
            __('attribute::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    private function getTypes(): array
	{
		return [
			'select'    =>  'Список',
			'radio'     =>  'Переключатель',
			'checkbox'  =>  'Чекбокс',
			'text'      =>  'Строка',
			'textarea'  =>  'Текст',
			'range'     =>  'Ползунок',
			'color'     =>  'Цвет',
			'file'      =>  'Файл',
			'number'    =>  'Число',
			'email'     =>  'E-mail',
			'date'      =>  'Дата'
		];
	}
}
