<?php


namespace Modules\Attribute\Services;


use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attribute\Contracts\AttributeContract;
use Modules\Attribute\Models\AttributeValue;

/**
 * Class AttributeValueWebService
 *
 * @package Modules\Attribute\Services
 */
class AttributeValueWebService extends BaseService
{
    /** @var AttributeContract  */
    protected $attributeRepository;

    /**
     * AttributeValueController constructor.
     *
     * @param  AttributeContract  $attributeRepository
     */
    public function __construct(AttributeContract $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Return all values for editable attribute like JSON.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function getValues(Request $request): JsonResponse
    {
        $attributeId = $request->input('id');
        $attribute = $this->attributeRepository->findAttributeById($attributeId);
        if ($attribute) {
            $values = $attribute->values;
            return $this->responseJson(
                false,
                Response::HTTP_FOUND,
                __('attribute::messages.values.success_retrieve'),
                $values
            );
        }

        return $this->responseJson(
            true,
            Response::HTTP_NOT_FOUND,
            __('attribute::messages.not_found', ['id' => $attributeId])
        );
    }

    /**
     * Add new value for editable attribute.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function addValues(Request $request): JsonResponse
    {
        $value = new AttributeValue();
        $value->attribute_id = $request->input('id');
        $value->value = $request->input('value');
        $value->save();
        return $this->responseJson(
            false,
            Response::HTTP_FOUND,
            __('attribute::messages.values.success_create'),
            $value
        );
    }

    /**
     * Update value option editable attribute.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function updateValues(Request $request): JsonResponse
    {
        $attributeValue = AttributeValue::findOrFail($request->input('valueId'));
        $attributeValue->attribute_id = $request->input('id');
        $attributeValue->value = $request->input('value');
        $attributeValue->save();
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.values.success_update'),
            $attributeValue
        );
    }

    /**
     * Delete value option editable attribute.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function deleteValues(Request $request): JsonResponse
    {
        $attributeValue = AttributeValue::findOrFail($request->input('id'));
        $attributeValue->delete();
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.values.success_delete'),
            $attributeValue
        );
    }
}
