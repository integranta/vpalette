<?php


namespace Modules\Attribute\Services\API\V1;


use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attribute\Contracts\AttributeContract;
use Modules\Attribute\Contracts\AttributeValueContract;
use Modules\Attribute\Http\Requests\API\V1\CreateAttributeValueAPIRequest;
use Modules\Attribute\Http\Requests\API\V1\UpdateAttributeValueAPIRequest;
use Modules\Attribute\Transformers\AttributeResource;
use Modules\Attribute\Transformers\AttributeValueResource;

/**
 * Class AttributeValueApiService
 *
 * @package Modules\Attribute\Services\API\V1
 */
class AttributeValueApiService extends BaseService
{
    /** @var  AttributeValueContract */
    private $attributeValueRepository;
    
    /** @var AttributeContract */
    private $attributeRepository;
    
    /**
     * AttributeValueAPIController constructor.
     *
     * @param  AttributeValueContract  $attributeValueRepo
     * @param  AttributeContract       $attributeRepo
     */
    public function __construct(AttributeValueContract $attributeValueRepo, AttributeContract $attributeRepo)
    {
        $this->attributeValueRepository = $attributeValueRepo;
        $this->attributeRepository = $attributeRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $attributeValues = AttributeValueResource::collection($this->attributeValueRepository->listAttributeValues());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.values.success_retrieve'),
            $attributeValues
        );
    }
    
    /**
     * @param  CreateAttributeValueAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateAttributeValueAPIRequest $request): JsonResponse
    {
        $attributeValue = $this->attributeValueRepository->createAttributeValue($request->all());
        
        if ($attributeValue) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('attribute::messages.values.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('attribute::messages.values.success_create'),
            $attributeValue
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $attributeValue = AttributeValueResource::make($this->attributeValueRepository->findAttributeValueById($id));
        
        if ($attributeValue === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('attribute::messages.values.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.values.success_found'),
            $attributeValue
        );
    }
    
    /**
     * @param  int                             $id
     * @param  UpdateAttributeValueAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateAttributeValueAPIRequest $request): JsonResponse
    {
        $attributeValue = $this->attributeValueRepository->findAttributeValueById($id);
        
        if ($attributeValue === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('attribute::messages.values.not_found')
            );
        }
        
        $attributeValue = $this->attributeValueRepository->updateAttributeValue($request->all(), $id);
        
        if (!$attributeValue) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('attribute::messages.values.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.values.success_update'),
            $attributeValue
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $attributeValue = $this->attributeValueRepository->findAttributeValueById($id);
        
        if ($attributeValue === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('attribute::messages.values.not_found')
            );
        }
        
        $attributeValue = $this->attributeValueRepository->deleteAttributeValue($id);
        
        if (!$attributeValue) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('attribute::messages.values.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.values.success_delete'),
            $attributeValue
        );
    }
    
    /**
     * Return all values for editable attribute like JSON.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function getValues(int $id): JsonResponse
    {
        $attribute = AttributeResource::make($this->attributeRepository->findAttributeById($id));
        $values = $attribute->values;
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.values.get_values'),
            $values
        );
    }
}
