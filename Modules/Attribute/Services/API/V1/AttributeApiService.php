<?php


namespace Modules\Attribute\Services\API\V1;


use App\Services\BaseService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attribute\Contracts\AttributeContract;
use Modules\Attribute\Http\Requests\API\V1\CreateAttributeAPIRequest;
use Modules\Attribute\Http\Requests\API\V1\UpdateAttributeAPIRequest;
use Modules\Attribute\Transformers\AttributeResource;

/**
 * Class AttributeApiService
 *
 * @package Modules\Attribute\Services\API\V1
 */
class AttributeApiService extends BaseService
{
    /** @var  AttributeContract */
    private $attributeRepository;
    
    /**
     * AttributeAPIController constructor.
     *
     * @param  AttributeContract  $attributeRepo
     */
    public function __construct(AttributeContract $attributeRepo)
    {
        $this->attributeRepository = $attributeRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $attributes = AttributeResource::collection($this->attributeRepository->listAttributes());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.success_retrieve'),
            $attributes
        );
    }
    
    /**
     * @param  CreateAttributeAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateAttributeAPIRequest $request): JsonResponse
    {
        $attribute = $this->attributeRepository->createAttribute($request->all());
        
        if (!$attribute) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('attribute::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('attribute::messages.success_create'),
            $attribute
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $attribute = AttributeResource::make($this->attributeRepository->findAttributeById($id));
        
        if ($attribute === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('attribute::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.success_found', ['id' => $id]),
            $attribute
        );
    }
    
    
    /**
     * @param  int                        $id
     * @param  UpdateAttributeAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdateAttributeAPIRequest $request): JsonResponse
    {
        $attribute = $this->attributeRepository->findAttributeById($id);
        
        if ($attribute === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('attribute::messages.not_found', ['id' => $id])
            );
        }
        
        $attribute = $this->attributeRepository->updateAttribute($request->all(), $id);
        
        if (!$attribute) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('attribute::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.success_update'),
            $attribute
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(int $id): JsonResponse
    {
        $attribute = $this->attributeRepository->findAttributeById($id);
        
        if ($attribute === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('attribute::messages.not_found', ['id' => $id])
            );
        }
        
        $attribute = $this->attributeRepository->deleteAttribute($id);
        
        if (!$attribute) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('attribute::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('attribute::messages.success_delete'),
            $attribute
        );
    }
    
}
