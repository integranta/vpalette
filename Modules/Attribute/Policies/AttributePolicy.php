<?php

namespace Modules\Attribute\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Attribute\Models\Attribute;

/**
 * Class AttributePolicy
 *
 * @package Modules\Attribute\Policies
 */
class AttributePolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the attribute.
     *
     * @param  User|null  $user
     * @param  Attribute  $attribute
     *
     * @return mixed
     */
    public function view(?User $user, Attribute $attribute)
    {
        if ($attribute->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished attributes')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create attributes.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create attributes')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the attribute.
     *
     * @param  User       $user
     * @param  Attribute  $attribute
     *
     * @return mixed
     */
    public function update(User $user, Attribute $attribute)
    {
        if ($user->can('edit all attributes')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the attribute.
     *
     * @param  User       $user
     * @param  Attribute  $attribute
     *
     * @return mixed
     */
    public function delete(User $user, Attribute $attribute)
    {
        if ($user->can('delete any attribute')) {
            return true;
        }
        
        return false;
    }
}
