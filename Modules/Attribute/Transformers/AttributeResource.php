<?php

namespace Modules\Attribute\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AttributeResource
 *
 * @package Modules\Attribute\Transformers
 */
class AttributeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'frontendType' => $this->frontend_type,
            'isFilterable' => $this->is_filterable,
            'isRequired' => $this->is_required,
            'showExpanded' => $this->show_expanded,
            'minValue' => $this->min,
            'maxValue' => $this->max,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
