<?php

namespace Modules\Page\Repositories;

use App\Repositories\BaseRepository;
use Arr;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Page\Contracts\PageContract;
use Modules\Page\Models\Page;
use Str;

/**
 * Class PageRepository
 *
 * @package Modules\Page\Repositories
 * @version October 16, 2019, 12:14 am MSK
 */
class PageRepository extends BaseRepository implements PageContract
{
    /**
     * @var array
     */
    private $pageData = [
        'title',
        'description',
        'keywords',
        'canonical_url',
        'google_tag_manager',
        'google_analytics',
        'ya_direct',
        'ya_metrica',
        '_method',
        '_token',
    ];
    
    /**
     * @var array
     */
    private $seoData = [
        'name',
        'content',
        'slug',
        'sort',
        'active',
        '_method',
        '_token',
    ];
    
    /**
     * PageRepository constructor.
     *
     * @param  Page  $model
     */
    public function __construct(Page $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listPages(string $order = 'sort', string $sort = 'asc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('page.cache_key').$postfix,
            config('page.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return Page
     */
    public function findPageById(int $id): ?Page
    {
        return $this->find($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return Page
     */
    public function createPage(array $params): Page
    {
        $page = $this->create($this->getPageFields($params));
        $page->seo()->create($this->getSeoFields($params));
        $page->save();
        
        return $page;
    }
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Page
     */
    public function updatePage(array $params, int $id): Page
    {
        $this->update($this->getPageFields($params), $id);
        $page = $this->find($id);
        $page->seo()->update($this->getSeoFields($params));
        $page->save();
        
        return $page;
    }
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deletePage(int $id): bool
    {
        return $this->delete($id);
    }
    
    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getPageFields(array $params): array
    {
        return Arr::except($params, $this->pageData);
    }
    
    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getSeoFields(array $params): array
    {
        return Arr::except($params, $this->seoData);
    }
}
