<?php


namespace Modules\Page\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Page\Contracts\PageContract;
use Modules\Page\Http\Requests\API\V1\CreatePageAPIRequest;
use Modules\Page\Http\Requests\API\V1\UpdatePageAPIRequest;
use Modules\Page\Transformers\PageResource;

/**
 * Class PageApiService
 *
 * @package Modules\Page\Services\API\V1
 */
class PageApiService extends BaseService
{
    /** @var  PageContract */
    private $pageRepository;
    
    /**
     * PageAPIController constructor.
     *
     * @param  PageContract  $pageRepo
     */
    public function __construct(PageContract $pageRepo)
    {
        $this->pageRepository = $pageRepo;
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $pages = PageResource::collection($this->pageRepository->listPages());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('page::messages.success_retrieve'),
            $pages
        );
    }
    
    /**
     * @param  CreatePageAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreatePageAPIRequest $request): JsonResponse
    {
        $page = $this->pageRepository->createPage($request->all());
        
        if (!$page) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('page::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('page::messages.success_create'),
            $page
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $page = PageResource::make($this->pageRepository->findPageById($id));
        
        if ($page === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('page::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('page::messages.success_found', ['id' => $id]),
            $page
        );
    }
    
    /**
     * @param  int                   $id
     * @param  UpdatePageAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdatePageAPIRequest $request): JsonResponse
    {
        $page = $this->pageRepository->findPageById($id);
        
        if ($page === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('page::messages.not_found', ['id' => $id])
            );
        }
        
        $page = $this->pageRepository->updatePage($request->all(), $id);
        
        if (!$page) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('page::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('page::messages.success_update'),
            $page
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $page = $this->pageRepository->findPageById($id);
        
        if ($page === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('page::messages.not_found', ['id' => $id])
            );
        }
        
        $page = $this->pageRepository->deletePage($id);
        
        if (!$page) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('page::messages.error_delete')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('page::messages.success_delete'),
            $page
        );
    }
}
