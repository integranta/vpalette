<?php


namespace Modules\Page\Services;


use App\DataTables\PagesDataTable;
use App\Services\BaseService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Page\Contracts\PageContract;
use Modules\Page\Http\Requests\CreatePageRequest;
use Modules\Page\Http\Requests\UpdatePageRequest;
use Modules\Page\Models\Page;

/**
 * Class PageWebService
 *
 * @package Modules\Page\Services
 */
class PageWebService extends BaseService
{
	/** @var  PageContract */
	private $pageRepository;
	
	/**
	 * PageController constructor.
	 *
	 * @param  PageContract  $pageRepo
	 */
	public function __construct(PageContract $pageRepo)
	{
		$this->pageRepository = $pageRepo;
	}
	
	/**
	 * Display a listing of the Page.
	 *
	 * @param  \App\DataTables\PagesDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(PagesDataTable $dataTable)
	{
		$this->setPageTitle(
			__('page::messages.title'),
			__('page::messages.index_subtitle'),
			__('page::messages.index_leadtext')
		);
		
		return $dataTable->render('page::pages.index');
	}
	
	/**
	 * Show the form for creating a new Page.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$model = new Page;
		$seoInfo = $model->load('seo');
		
		$this->setPageTitle(
			__('page::messages.title'),
			__('page::messages.create_subtitle'),
			__('page::messages.create_leadtext')
		);
		return view('page::pages.create', compact('model', 'seoInfo'));
	}
	
	/**
	 * Store a newly created Page in storage.
	 *
	 * @param  CreatePageRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreatePageRequest $request): RedirectResponse
	{
		$page = $this->pageRepository->createPage($request->all());
		
		if (!$page) {
			return $this->responseRedirectBack(
				__('page::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.pages.index',
			__('page::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Display the specified Page.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(int $id): View
	{
		$page = $this->pageRepository->findPageById($id);
		
		if ($page === null) {
			return $this->responseRedirect(
				'admin.pages.index',
				__('page::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('page::messages.title'),
			__('page::messages.show_subtitle', ['name' => $page->name]),
			__('page::messages.show_leadtext', ['name' => $page->name])
		);
		return view('page::pages.show', compact('page'));
	}
	
	/**
	 * Show the form for editing the specified Page.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$page = $this->pageRepository->findPageById($id);
		
		if ($page === null) {
			return $this->responseRedirect(
				'admin.pages.index',
				__('page::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('page::messages.title'),
			__('page::messages.edit_subtitle', ['name' => $page->name]),
			__('page::messages.edit_leadtext', ['name' => $page->name])
		);
		return view('page::pages.edit', compact('page'));
	}
	
	/**
	 * Update the specified Page in storage.
	 *
	 * @param  int                $id
	 * @param  UpdatePageRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function update(int $id, UpdatePageRequest $request): RedirectResponse
	{
		$page = $this->pageRepository->findPageById($id);
		
		if ($page === null) {
			return $this->responseRedirect(
				'admin.pages.index',
				__('page::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$page = $this->pageRepository->updatePage($request->all(), $id);
		
		if (!$page) {
			return $this->responseRedirectBack(
				__('page::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirectBack(
			__('page::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified Page from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 * @throws Exception
	 *
	 */
	public function destroy(int $id): RedirectResponse
	{
		$page = $this->pageRepository->findPageById($id);
		
		if ($page === null) {
			return $this->responseRedirect(
				'admin.pages.index',
				__('page::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$page = $this->pageRepository->deletePage($id);
		
		if (!$page) {
			return $this->responseRedirectBack(
				__('page::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		return $this->responseRedirect(
			'admin.pages.index',
			__('page::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}