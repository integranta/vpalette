<?php

return [
    'name' => 'Page',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all pages as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'cache_key' => env('PAGE_CACHE_KEY','index_pages'),
    'cache_ttl' => env('PAGE_CACHE_TTL',360000),
];
