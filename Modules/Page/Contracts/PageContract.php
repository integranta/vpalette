<?php


namespace Modules\Page\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Modules\Page\Models\Page;

/**
 * Interface PageContract
 *
 * @package Modules\Page\Contracts
 */
interface PageContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listPages(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Page
     */
    public function findPageById(int $id): ?Page;
    
    /**
     * @param  array  $params
     *
     * @return Page
     */
    public function createPage(array $params): Page;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Page
     */
    public function updatePage(array $params, int $id): Page;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deletePage(int $id): bool;
}
