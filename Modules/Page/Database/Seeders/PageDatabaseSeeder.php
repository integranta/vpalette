<?php

namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Page\Models\Page;

/**
 * Class PageDatabaseSeeder
 *
 * @package Modules\Page\Database\Seeders
 */
class PageDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Страницы блока "Покупателям".
        Page::create([
            'active' => true,
            'name' => 'Как оформить заказ',
            'sort' => 100,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Обратная связь',
            'sort' => 200,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Помощь по сервису',
            'sort' => 300,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Доставка и оплата',
            'sort' => 400,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Обмен и возврат товара',
            'sort' => 500,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Политика конфиденциальности',
            'sort' => 600,
            'content' => ''
        ]);
        
        // Страницы блока "Магазинам".
        Page::create([
            'active' => true,
            'name' => 'Как стать партнёром?',
            'sort' => 200,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Подключение магазина',
            'sort' => 300,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Помощь в подключении',
            'sort' => 400,
            'content' => ''
        ]);
        
        Page::create([
            'active' => true,
            'name' => 'Информация для юрилических лиц',
            'sort' => 600,
            'content' => ''
        ]);
    }
}
