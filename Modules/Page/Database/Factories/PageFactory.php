<?php

use Faker\Generator as Faker;
use Modules\Page\Models\Page;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'active' => true,
        'name' => $faker->text(50),
        'sort' => $faker->numberBetween(1, 500),
        'content' => $faker->realText(1000, 2)
    ];
});
