<?php

namespace Modules\Page\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Page\Models\Page;

/**
 * Class PagePolicy
 *
 * @package Modules\Page\Policies
 */
class PagePolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the page.
     *
     * @param  User|null  $user
     * @param  Page       $page
     *
     * @return mixed
     */
    public function view(?User $user, Page $page)
    {
        if ($page->active) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished pages')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create pages.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create pages')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the page.
     *
     * @param  User  $user
     * @param  Page  $page
     *
     * @return mixed
     */
    public function update(User $user, Page $page)
    {
        if ($user->can('edit all pages')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the page.
     *
     * @param  User  $user
     * @param  Page  $page
     *
     * @return mixed
     */
    public function delete(User $user, Page $page)
    {
        if ($user->can('delete any page')) {
            return true;
        }
        
        return false;
    }
}
