<?php

namespace Modules\Page\Observers;

use App\Traits\Cacheable;
use Modules\Page\Models\Page;

/**
 * Class PageObserver
 *
 * @package Modules\Page\Observers
 */
class PageObserver
{
    use Cacheable;
    
    /**
     * Handle the page "created" event.
     *
     * @param  Page  $page
     *
     * @return void
     */
    public function created(Page $page): void
    {
        $this->incrementCountItemsInCache($page, config('page.cache_key'));
    }
    
    /**
     * Handle the page "updated" event.
     *
     * @param  Page  $page
     *
     * @return void
     */
    public function updated(Page $page): void
    {
        //
    }
    
    /**
     * Handle the page "deleted" event.
     *
     * @param  Page  $page
     *
     * @return void
     */
    public function deleted(Page $page): void
    {
        $this->decrementCountItemsInCache($page, config('page.cache_key'));
    }
    
    /**
     * Handle the page "restored" event.
     *
     * @param  Page  $page
     *
     * @return void
     */
    public function restored(Page $page): void
    {
        $this->incrementCountItemsInCache($page, config('page.cache_key'));
    }
    
    /**
     * Handle the page "force deleted" event.
     *
     * @param  Page  $page
     *
     * @return void
     */
    public function forceDeleted(Page $page): void
    {
        $this->decrementCountItemsInCache($page, config('page.cache_key'));
    }
}
