<?php

namespace Modules\Page\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Seo\Transformers\SeoResource;

/**
 * Class PageResource
 *
 * @package Modules\Page\Transformers
 */
class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'content' => $this->content,
            'uri' => $this->slug,
            'active' => (bool) $this->active,
            'sort' => $this->sort,
            'seo' => SeoResource::make($this->whenLoaded('seo')),
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
