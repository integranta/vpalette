<?php

namespace Modules\Page\Http\Requests\API\V1;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Page\Models\Page;

/**
 * Class CreatePageAPIRequest
 *
 * @package Modules\Page\Http\Requests\API\V1
 */
class CreatePageAPIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Page::$rules;
    }
}
