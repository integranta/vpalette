<?php

namespace Modules\Page\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Page\Models\Page;

/**
 * Class UpdatePageRequest
 *
 * @package Modules\Page\Http\Requests
 */
class UpdatePageRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Page::$rules;
    }
}
