<?php

namespace Modules\Page\Http\Controllers;

use App\DataTables\PagesDataTable;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Page\Http\Requests\CreatePageRequest;
use Modules\Page\Http\Requests\UpdatePageRequest;
use Modules\Page\Services\PageWebService;

/**
 * Class PageController
 *
 * @package Modules\Page\Http\Controllers
 */
class PageController extends Controller
{
    /** @var PageWebService */
    private $pageWebService;

    /**
     * PageController constructor.
     *
     * @param  PageWebService  $service
     */
    public function __construct(PageWebService $service)
    {
        $this->pageWebService = $service;
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the Page.
	 *
	 * @param  \App\DataTables\PagesDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(PagesDataTable $dataTable)
    {
        return $this->pageWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new Page.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->pageWebService->create();
    }

    /**
     * Store a newly created Page in storage.
     *
     * @param  CreatePageRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreatePageRequest $request): RedirectResponse
    {
        return $this->pageWebService->store($request);
    }

    /**
     * Display the specified Page.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->pageWebService->show($id);
    }

    /**
     * Show the form for editing the specified Page.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->pageWebService->edit($id);
    }

    /**
     * Update the specified Page in storage.
     *
     * @param  int                $id
     * @param  UpdatePageRequest  $request
     *
     * @return RedirectResponse
     */
    public function update(int $id, UpdatePageRequest $request): RedirectResponse
    {
        return $this->pageWebService->update($id, $request);
    }

    /**
     * Remove the specified Page from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     * @throws Exception
     *
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->pageWebService->destroy($id);
    }
}
