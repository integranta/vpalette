<?php

namespace Modules\Page\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Page\Http\Requests\API\V1\CreatePageAPIRequest;
use Modules\Page\Http\Requests\API\V1\UpdatePageAPIRequest;
use Modules\Page\Services\API\V1\PageApiService;

/**
 * Class PageController
 *
 * @package Modules\Page\Http\Controllers\API\V1
 */
class PageAPIController extends Controller
{
    /** @var  PageApiService */
    private $pageApiService;

    /**
     * PageAPIController constructor.
     *
     * @param  PageApiService  $service
     */
    public function __construct(PageApiService $service)
    {
        $this->pageApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->pageApiService->index($request);
    }

    /**
     * @param  CreatePageAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreatePageAPIRequest $request): JsonResponse
    {
        return $this->pageApiService->store($request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->pageApiService->show($id);
    }

    /**
     * @param  int                   $id
     * @param  UpdatePageAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function update(int $id, UpdatePageAPIRequest $request): JsonResponse
    {
        return $this->pageApiService->update($id, $request);
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->pageApiService->destroy($id);
    }
}
