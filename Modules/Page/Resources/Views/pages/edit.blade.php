@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('css/page.css') }}">
@endsection

@section('content')
    <page-edit
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        :page-id={{ $page->id }}
    />
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ asset('js/page.js') }}"></script>
@endpush
