<?php

namespace Modules\Page\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Seo\Models\Seo;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Page
 *
 * @package Modules\Page\Models
 * @property int $id
 * @property bool $active
 * @property string $name
 * @property string $content
 * @property string $slug
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Modules\Seo\Models\Seo|null $seo
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Page\Models\Page onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Page\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Page\Models\Page withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Page\Models\Page withoutTrashed()
 * @mixin \Eloquent
 */
class Page extends BaseModel
{
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    public $table = 'pages';

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Отношение, которые всегда должны быть загружены.
     *
     * @var array
     */
    protected $with = ['seo'];

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'slug',
        'active',
        'sort'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Правила для валидации атрибутов.
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string',
        'content' => 'nullable|string',
        'slug' => 'string',
        'active' => 'required',
        'sort' => 'required|integer'
    ];


    /**
     * Установить флаг активности.
     *
     * @param  bool  $value
     *
     * @return string
     */
    public function setActiveAttribute(bool $value): string
    {
        return $this->attributes['active'] = filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Получить связанные SEO параметры для страницы.
     *
     * @return MorphOne
     */
    public function seo(): MorphOne
    {
        return $this->morphOne(Seo::class, 'seoble');
    }
}
