<?php


namespace Modules\Coupon\Contracts;

/**
 * Interface CouponContract
 *
 * @package Modules\Coupon\Contracts
 */
interface CouponContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return mixed
     */
    public function listCoupons(string $order = 'id', string $sort = 'desc', array $columns = ['*']);
    
    /**
     * @param  int  $id
     *
     * @return mixed
     */
    public function findCouponById(int $id);
    
    /**
     * @param  array  $params
     *
     * @return mixed
     */
    public function createCoupon(array $params);
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return mixed
     */
    public function updateCoupon(array $params, int $id);
    
    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteCoupon($id);
}
