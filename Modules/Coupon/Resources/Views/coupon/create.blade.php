@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/coupon.css') }}">
@endsection

@section('content')
    <coupon-create
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
    ></coupon-create>
@endsection
@push('scripts')
    {{--  Module JS File  --}}
@endpush
