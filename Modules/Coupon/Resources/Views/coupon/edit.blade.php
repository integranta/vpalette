@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ mix('css/coupon.css') }}">
@endsection

@section('content')
    <coupon-edit
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        coupon-id="{{ $coupon->id }}"
    ></coupon-edit>
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ mix('js/coupon.js') }}"></script>
@endpush
