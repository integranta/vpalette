@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/coupon.css') }}">
@endsection

@section('content')
    <coupon-show
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        coupon-id="{{ $coupon->id }}"
    ></coupon-show>
@endsection

@push('scripts')
    <script src="{{ asset('js/coupon.js') }}"></script>
@endpush
