
<!-- Username Field -->
<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="user_id"
    >
        Автор:
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="user_id"
            class="form-control select2"
            name="user_id[]"
            multiple
        >
            <option disabled>Выберите пользователей</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}"> {{ $user->name }} </option>
            @endforeach
        </select>
    </div>
</div>
<!-- End Username Field -->

<!-- Product_id Field -->

<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="product_id">Товар
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="product_id"
            class="form-control select2"
            name="product_id[]"
            multiple
        >
            <option disabled>Выберите товар(ы)</option>
            @foreach($products as $product)
                <option value="{{ $product->id }}"> {{ $product->name }} </option>
            @endforeach
        </select>
    </div>
</div>
<!-- End Product_Id Field -->

<!-- Question Field -->
<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="code">Код на скидку</label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="code"
            id="code"
            class="form-control"
            value="{{ old('code') }}"
        >
    </div>
</div>

<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="name">Название скидки</label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="name"
            id="name"
            class="form-control"
            value="{{ old('name') }}"
        >
    </div>
</div>

<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="uses_count">Количество использований в настоящее время</label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="uses_count"
            id="uses_count"
            class="form-control"
            value="{{ old('uses_count') }}"
        >
    </div>
</div>


<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="max_uses_count">Максимальное кол-во использований</label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="max_uses_count"
            id="max_uses_count"
            class="form-control"
            value="{{ old('max_uses_count') }}"
        >
    </div>
</div>


<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="max_users_count_user">Максимальное кол-во использований пользователем</label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="max_users_count_user"
            id="max_users_count_user"
            class="form-control"
            value="{{ old('max_users_count_user') }}"
        >
    </div>
</div>

<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="type">Тип
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="type"
            class="form-control"
            name="type"
        >
            <option disabled>Выберите тип</option>
            @foreach($discountTypes as $key => $discountType)
                <option value="{{ $key }}"> {{ $discountType }} </option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row col-md-12 mb-4">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 font-weight-bold"
        for="amount_type">Тип стоимости скидки
    </label>
    <div class="col-sm-12 col-md-7">
        <select
            id="amount_type"
            class="form-control select2"
            name="amount_type"
        >
            <option disabled>Выберите тип</option>
            @foreach($discountAmountTypes as $key => $discountAmountType)
                <option value="{{ $key }}"> {{ $discountAmountType }} </option>
            @endforeach
        </select>
    </div>
</div>


<div class="form-group row col-md-12">
    <span class="col-form-label text-md-right col-12 col-md-3 col-lg-3">
        Прекратить дальнейшее применение правил:
    </span>
    <div class="custom-control custom-checkbox col-sm-12 col-md-7 d-flex align-self-center">
        <input type="hidden" name="is_fixed" value="0">
        <input type="checkbox" name="is_fixed" id="is_fixed" value="1" class="custom-control-input">
        <label class="custom-control-label ml-lg-3" for="is_fixed"></label>
    </div>
</div>

<!-- Activity End at Field -->
<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="starts_at">Начинает действовать с:</label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="starts_at"
            id="starts_at"
            class="flatpickr flatpickr-input"
            placeholder="Введите дату начала активности"
            value="{{ old('starts_at') }}"
        >
    </div>
</div>

<div class="form-group row col-md-12">
    <label
        class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
        for="expires_at">Заканчивает действовать по:</label>
    <div class="col-sm-12 col-md-7">
        <input
            type="text"
            name="expires_at"
            id="expires_at"
            class="flatpickr flatpickr-input"
            placeholder="Введите дату окончания активности"
            value="{{ old('expires_at') }}"
        >
    </div>
</div>
