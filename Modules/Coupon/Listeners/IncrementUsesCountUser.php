<?php

namespace Modules\Coupon\Listeners;

use DB;
use Modules\Coupon\Events\CouponWasUsed;

/**
 * Class IncrementUsesCountUser
 *
 * @package Modules\Coupon\Listeners
 */
class IncrementUsesCountUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Handle the event.
     *
     * @param  CouponWasUsed  $event
     *
     * @return void
     */
    public function handle(CouponWasUsed $event)
    {
        if ($event->coupon->uses_user_count < $event->coupon->max_uses_count) {
            DB::table('coupons')->increment('uses_user_count', 1, ['id' => $event->coupon->id]);
        }
    }
}
