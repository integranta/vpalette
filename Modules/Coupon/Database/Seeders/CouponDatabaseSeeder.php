<?php

namespace Modules\Coupon\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Coupon\Models\Coupon;

/**
 * Class CouponDatabaseSeeder
 *
 * @package Modules\Coupon\Database\Seeders
 */
class CouponDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Coupon::class, 10)->create();
    }
}
