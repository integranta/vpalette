<?php

use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Coupon\Models\Coupon;
use Modules\Discount\Models\Discount;

/** @var Factory $factory */
$factory->define(Coupon::class, function (Faker $faker) {
    
    $periodActivity = $faker->randomElement(CouponPeriodActivity::getValues());
    
    $startsAt = Carbon::now('Europe/Samara');
    
    $expiresAt = Carbon::now('Europe/Samara')->addMonths(1);
    
    return [
        'active' => random_int(0, 1),
        'sort' => $faker->numberBetween(1, 500),
        'code' => $faker->sentence(1, false),
        'type' => $faker->randomElement(CouponTypes::getValues()),
        'period_activity' => $faker->randomElement(CouponPeriodActivity::getValues()),
        'starts_at' => $periodActivity === CouponPeriodActivity::INTERVAL ? $startsAt : null,
        'expires_at' => $periodActivity === CouponPeriodActivity::INTERVAL ? $expiresAt : null,
        'comments' => $faker->realText(150),
        'max_uses_count' => $faker->randomNumber(3),
        'uses_user_count' => $faker->randomNumber(2),
        'user_id' => factory(User::class),
        'discount_id' => factory(Discount::class)
    ];
});
