<?php

use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateCouponsTable
 */
class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->string('code', 30);
            $table->enum('type', CouponTypes::getValues())->default(CouponTypes::SINGLE_ORDER);
            $table->enum('period_activity', CouponPeriodActivity::getValues())
                ->default(CouponPeriodActivity::NO_LIMIT)
                ->nullable();
            $table->timestamp('starts_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->text('comments')->nullable();
            $table->unsignedInteger('max_uses_count')->default(0)->nullable();
            $table->unsignedInteger('uses_user_count')->default(0)->nullable();
            $table->unsignedBigInteger('discount_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->index(['discount_id', 'user_id', 'active', 'sort']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->dropForeign('coupons_discount_id_foreign');
            $table->dropForeign('coupons_user_id_foreign');
        });
        Schema::dropIfExists('coupons');
    }
}
