<?php

namespace Modules\Coupon\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CouponResource
 *
 * @package Modules\Coupon\Transformers
 */
class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'isActive' => $this->is_acitive,
            'code' => $this->code,
            'typeCoupon' => $this->type,
            'periodActivity' => $this->period_activity,
            'startsAt' => $this->starts_at,
            'expiresAt' => $this->expires_at,
            'comments' => $this->comments,
            'maxUsesCouponCount' => $this->max_uses_count,
            'usesUserCouponCount' => $this->uses_user_count,
            'discountId' => $this->discount_id,
            'userId' => $this->user_id,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at
        ];
    }
}
