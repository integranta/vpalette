<?php

namespace Modules\Coupon\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Coupon\Models\Coupon;

/**
 * Class CouponWasUsed
 *
 * @package Modules\Coupon\Events
 */
class CouponWasUsed
{
    use SerializesModels;
    
    /**
     * @var Coupon
     */
    public $coupon;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }
    
    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
