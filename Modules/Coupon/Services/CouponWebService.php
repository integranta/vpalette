<?php


namespace Modules\Coupon\Services;


use App\DataTables\CouponsDataTable;
use App\Services\BaseService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\Factory;
use Modules\Coupon\Contracts\CouponContract;
use Modules\Coupon\Http\Requests\CreateCouponRequest;
use Modules\Coupon\Http\Requests\UpdateCouponRequest;

/**
 * Class CouponWebService
 *
 * @package Modules\Coupon\Services
 */
class CouponWebService extends BaseService
{
	/** @var CouponContract */
	private $couponRepository;
	
	/**
	 * CouponController constructor.
	 *
	 * @param  CouponContract  $couponRepo
	 */
	public function __construct(CouponContract $couponRepo)
	{
		$this->couponRepository = $couponRepo;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\CouponsDataTable  $dataTable
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\JsonResponse
	 */
	public function index(CouponsDataTable $dataTable)
	{
		$this->setPageTitle(
			__('coupon::messages.title'),
			__('coupon::messages.index_subtitle'),
			__('coupon::messages.index_leadtext')
		);
		
		return $dataTable->render('coupon::coupon.index');
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Factory|View
	 */
	public function create(): View
	{
		$this->setPageTitle(
			__('coupon::messages.title'),
			__('coupon::messages.create_subtitle'),
			__('coupon::messages.create_leadtext')
		);
		return view('coupon::coupon.create');
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  CreateCouponRequest  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(CreateCouponRequest $request): RedirectResponse
	{
		$coupon = $this->couponRepository->createCoupon($request->all());
		
		if (!$coupon) {
			return $this->responseRedirectBack(
				__('coupon::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.coupon.index',
			__('coupon::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(int $id): View
	{
		$coupon = $this->couponRepository->findCouponById($id);
		
		if ($coupon === null) {
			return $this->responseRedirect(
				'admin.coupon.index',
				__('coupon.messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$this->setPageTitle(
			__('coupon::messages.title'),
			__('coupon::messages.show_subtitle', ['code' => $coupon->code]),
			__('coupon::messages.show_leadtext', ['code' => $coupon->code])
		);
		
		return view('coupon::coupon.show', compact('coupon'));
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function edit(int $id): View
	{
		$coupon = $this->couponRepository->findCouponById($id);
		
		if ($coupon === null) {
			return $this->responseRedirect(
				'admin.coupon.index',
				__('coupon.messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$this->setPageTitle(
			__('coupon::messages.title'),
			__('coupon::messages.edit_subtitle', ['code' => $coupon->code]),
			__('coupon::messages.edit_leadtext', ['code' => $coupon->code])
		);
		return view('coupon::coupon.edit', compact('coupon'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  UpdateCouponRequest  $request
	 * @param  int                  $id
	 *
	 * @return RedirectResponse
	 */
	public function update(UpdateCouponRequest $request, int $id): RedirectResponse
	{
		$coupon = $this->couponRepository->findCouponById($id);
		
		if ($coupon === null) {
			return $this->responseRedirect(
				'admin.coupon.index',
				__('coupon.messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$coupon = $this->couponRepository->updateCoupon($request->all(), $id);
		
		if (!$coupon) {
			return $this->responseRedirectBack(
				__('coupon::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.coupon.index',
			__('coupon::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return RedirectResponse
	 */
	public function destroy(int $id): RedirectResponse
	{
		$coupon = $this->couponRepository->findCouponById($id);
		
		if ($coupon === null) {
			return $this->responseRedirect(
				'admin.coupon.index',
				__('coupon.messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		$coupon = $this->couponRepository->deleteCoupon($id);
		
		if (!$coupon) {
			return $this->responseRedirectBack(
				__('coupon::messages.error_delete'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.coupon.index',
			__('coupon::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
