<?php


namespace Modules\Coupon\Services\API\V1;

use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Coupon\Contracts\CouponContract;
use Modules\Coupon\Http\Requests\API\V1\CreateCouponAPIRequest;
use Modules\Coupon\Http\Requests\API\V1\UpdateCouponAPIRequest;
use Modules\Coupon\Models\Coupon;
use Modules\Coupon\Transformers\CouponResource;

/**
 * Class CouponApiService
 *
 * @package Modules\Coupon\Services\API\V1
 */
class CouponApiService extends BaseService
{
	/** @var CouponContract */
	private $couponRepository;
	
	/**
	 * CouponAPIController constructor.
	 *
	 * @param  CouponContract  $couponRepo
	 */
	public function __construct(CouponContract $couponRepo)
	{
		$this->couponRepository = $couponRepo;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return JsonResponse
	 */
	public function index(): JsonResponse
	{
		$coupons = CouponResource::collection($this->couponRepository->listCoupons());
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('coupon::messages.success_retrieve'),
			$coupons
		);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  CreateCouponAPIRequest  $request
	 *
	 * @return JsonResponse
	 */
	public function store(CreateCouponAPIRequest $request): JsonResponse
	{
		$coupon = $this->couponRepository->createCoupon($request->get('couponFields'));
		
		if (!$coupon) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('coupon::messages.error_create')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_CREATED,
			__('coupon::messages.success_create'),
			$coupon
		);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function show(int $id): JsonResponse
	{
		$coupon = CouponResource::make($this->couponRepository->findCouponById($id));
		
		if ($coupon === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('coupon::messages.not_found', ['id' => $id])
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('coupon::messages.success_found', ['id' => $id]),
			$coupon
		);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  UpdateCouponAPIRequest  $request
	 * @param  int                     $id
	 *
	 * @return JsonResponse
	 */
	public function update(UpdateCouponAPIRequest $request, int $id): JsonResponse
	{
		$coupon = $this->couponRepository->findCouponById($id);
		
		if ($coupon === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('coupon::messages.not_found', ['id' => $id])
			);
		}
		
		$coupon = $this->couponRepository->updateCoupon($request->get('couponFields'), $id);
		
		if (!$coupon) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('coupon::messages.error_update')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('coupon::messages.success_update'),
			$coupon
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return JsonResponse
	 */
	public function destroy(int $id): JsonResponse
	{
		$coupon = $this->couponRepository->findCouponById($id);
		
		if ($coupon === null) {
			return $this->responseJson(
				true,
				Response::HTTP_NOT_FOUND,
				__('coupon::messages.not_found', ['id' => $id])
			);
		}
		
		$coupon = $this->couponRepository->deleteCoupon($id);
		
		if (!$coupon) {
			return $this->responseJson(
				true,
				Response::HTTP_UNPROCESSABLE_ENTITY,
				__('coupon::messages.error_delete')
			);
		}
		
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('coupon::messages.success_delete'),
			$coupon
		);
	}
	
	/**
	 * @return JsonResponse
	 */
	public function getPeriodActivities(): JsonResponse
	{
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('coupon::messages.success_retrieve_activities-types'),
			Coupon::getPeriodActivity()
		);
	}
	
	/**
	 * @return JsonResponse
	 */
	public function getCouponTypes(): JsonResponse
	{
		return $this->responseJson(
			false,
			Response::HTTP_OK,
			__('coupon::messages.success_retrieve_types'),
			Coupon::getCouponTypes()
		);
	}
}
