<?php


namespace Modules\Coupon\Repositories;


use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Coupon\Contracts\CouponContract;
use Modules\Coupon\Models\Coupon;

/**
 * Class CouponRepository
 *
 * @package Modules\Coupon\Repositories
 */
class CouponRepository extends BaseRepository implements CouponContract
{
    /**
     * CouponRepository constructor.
     *
     * @param  Coupon  $model
     */
    public function __construct(Coupon $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listCoupons(string $order = 'id', string $sort = 'desc', array $columns = ['*'])
    {
        return $this->all($columns, $order, $sort);
    }
    
    /**
     * @inheritDoc
     */
    public function findCouponById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $exception) {
            throw new ModelNotFoundException($exception);
        }
    }
    
    /**
     * @inheritDoc
     */
    public function createCoupon(array $params)
    {
        return $this->create($params);
    }
    
    /**
     * @inheritDoc
     */
    public function updateCoupon(array $params, int $id)
    {
        return $this->update($params, $id);
    }
    
    /**
     * @inheritDoc
     */
    public function deleteCoupon($id)
    {
        return $this->delete($id);
    }
}
