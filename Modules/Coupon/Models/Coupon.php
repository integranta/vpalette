<?php

namespace Modules\Coupon\Models;

use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use App\User;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Discount\Models\Discount;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Coupon
 *
 * @package Modules\Coupon\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $code
 * @property string $type
 * @property string|null $periodActivity
 * @property string|null $startsAt
 * @property string|null $expiresAt
 * @property string|null $comments
 * @property int|null $maxUsesCount
 * @property int|null $usesUserCount
 * @property int $discountId
 * @property int|null $userId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Modules\Discount\Models\Discount $discount
 * @property-read \App\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Coupon\Models\Coupon onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereDiscountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereMaxUsesCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon wherePeriodActivity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereStartsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Coupon\Models\Coupon whereUsesUserCount($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Coupon\Models\Coupon withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Coupon\Models\Coupon withoutTrashed()
 * @mixin \Eloquent
 */
class Coupon extends Model
{
    use SoftDeletes;
    use CastsEnums;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'code',
        'type',
        'period_activity',
        'starts_at',
        'expires_at',
        'comments',
        'max_uses_count',
        'uses_user_count',
        'discount_id',
        'user_id'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'active' => 'boolean',
        'code' => 'string',
        'discount_id' => 'integer',
        'user_id' => 'integer',
        'comments' => 'string',
        'uses_user_count' => 'integer',
        'sort' => 'integer'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'type' => CouponTypes::class,
        'period_activity' => CouponPeriodActivity::class
    ];


    /**
     * Возвращает список значений периодов активности из enum класса для select тега.
     *
     * @return array
     */
    public static function getPeriodActivity(): array
    {
        return CouponPeriodActivity::toSelectArray();
    }

    /**
     * Возвращает список значений типов купонов из enum класса для select тега.
     *
     * @return array
     */
    public static function getCouponTypes(): array
    {
        return CouponTypes::toSelectArray();
    }

    /**
     * Получить владельца купона.
     *
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получить привязанную скидку.
     *
     * @return BelongsTo
     */
    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }
}
