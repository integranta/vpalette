<?php

namespace Modules\Coupon\Http\Controllers\API\V1;

use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Coupon\Http\Requests\API\V1\CreateCouponAPIRequest;
use Modules\Coupon\Http\Requests\API\V1\UpdateCouponAPIRequest;
use Modules\Coupon\Services\API\V1\CouponApiService;

/**
 * Class CouponAPIController
 *
 * @package Modules\Coupon\Http\Controllers\API\V1
 */
class CouponAPIController extends Controller
{
    /** @var CouponApiService */
    private $couponApiService;

    /**
     * CouponAPIController constructor.
     *
     * @param  CouponApiService  $service
     */
    public function __construct(CouponApiService $service)
    {
        $this->couponApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->couponApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCouponAPIRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CreateCouponAPIRequest $request): JsonResponse
    {
        return $this->couponApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->couponApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCouponAPIRequest  $request
     * @param  int                     $id
     *
     * @return JsonResponse
     */
    public function update(UpdateCouponAPIRequest $request, int $id): JsonResponse
    {
        return $this->couponApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->couponApiService->destroy($id);
    }

    /**
     * Return values from enum CouponPeriodActivities
     *
     * @return JsonResponse
     * @see CouponPeriodActivity
     *
     */
    public function getPeriodActivities(): JsonResponse
    {
        return $this->couponApiService->getPeriodActivities();
    }

    /**
     * Return values from enum CouponTypes
     *
     * @return JsonResponse
     * @see CouponTypes
     *
     */
    public function getCouponTypes(): JsonResponse
    {
        return $this->couponApiService->getCouponTypes();
    }
}
