<?php

namespace Modules\Coupon\Http\Controllers;

use App\DataTables\CouponsDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Modules\Coupon\Http\Requests\CreateCouponRequest;
use Modules\Coupon\Http\Requests\UpdateCouponRequest;
use Modules\Coupon\Services\CouponWebService;

/**
 * Class CouponController
 *
 * @package Modules\Coupon\Http\Controllers
 */
class CouponController extends Controller
{
    /** @var CouponWebService */
    private $couponWebService;

    /**
     * CouponController constructor.
     *
     * @param  CouponWebService  $service
     */
    public function __construct(CouponWebService $service)
    {
        $this->couponWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\CouponsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(CouponsDataTable $dataTable)
    {
        return $this->couponWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->couponWebService->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCouponRequest  $request
     *
     * @return RedirectResponse
     */
    public function store(CreateCouponRequest $request): RedirectResponse
    {
        return $this->couponWebService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $id): View
    {
        return $this->couponWebService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(int $id): View
    {
        return $this->couponWebService->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCouponRequest  $request
     * @param  int                  $id
     *
     * @return RedirectResponse
     */
    public function update(UpdateCouponRequest $request, int $id): RedirectResponse
    {
        return $this->couponWebService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->couponWebService->destroy($id);
    }
}
