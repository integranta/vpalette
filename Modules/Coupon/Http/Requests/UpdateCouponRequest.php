<?php

namespace Modules\Coupon\Http\Requests;

use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateCouponRequest
 *
 * @package Modules\Coupon\Http\Requests
 */
class UpdateCouponRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'active' => 'required|boolean',
            'code' => 'required|string',
            'type' => [
                'required',
                Rule::in(CouponTypes::getValues())
            ],
            'period_activity' => [
                'required',
                Rule::in(CouponPeriodActivity::getValues())
            ],
            'starts_at' => 'nullable|date',
            'expires_at' => 'nullable|date',
            'comments' => 'nullable|string|max:255',
            'max_uses_count' => 'nullable|integer|min:1',
            'uses_user_count' => 'nullable|integer|min:1',
            'discount_id' => 'required|integer|exists:discounts,id',
            'user_id' => 'required|integer|exists:users,id'
        ];
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
