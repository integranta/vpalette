<?php

namespace Modules\PartnerProduct\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\PartnerProduct\Models\PartnerProduct;
use Modules\PartnerProduct\Notifications\PartnerProductStatusAdded;

/**
 * Class NotifyPartnerProductStatusAdded
 *
 * @package Modules\PartnerProduct\Jobs
 */
class NotifyPartnerProductStatusAdded implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /** @var User */
    public $partner;
    
    /** @var PartnerProduct */
    public $partnerProduct;
    
    /**
     * Create a new job instance.
     *
     * @param  User            $partner
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function __construct(User $partner, PartnerProduct $partnerProduct)
    {
        $this->partner = $partner;
        $this->partnerProduct = $partnerProduct;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->partner->notify(new PartnerProductStatusAdded($this->partnerProduct));
    }
}
