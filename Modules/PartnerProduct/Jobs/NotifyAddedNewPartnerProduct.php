<?php

namespace Modules\PartnerProduct\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\PartnerProduct\Models\PartnerProduct;
use Modules\PartnerProduct\Notifications\AddedNewPartnerProduct;

/**
 * Class NotifyAddedNewPartnerProduct
 *
 * @package Modules\PartnerProduct\Jobs
 */
class NotifyAddedNewPartnerProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /** @var User */
    public $adminUser;
    
    /** @var PartnerProduct */
    public $partnerProduct;
    
    /**
     * Create a new job instance.
     *
     * @param  User            $adminUser
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function __construct(User $adminUser, PartnerProduct $partnerProduct)
    {
        $this->adminUser = $adminUser;
        $this->partnerProduct = $partnerProduct;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->adminUser->notify(new AddedNewPartnerProduct($this->partnerProduct));
    }
}
