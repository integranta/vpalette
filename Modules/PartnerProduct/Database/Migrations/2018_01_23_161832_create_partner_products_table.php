<?php

use App\Enums\PartnerProductStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePartnerProductsTable
 */
class CreatePartnerProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('partner_id');
            $table->string('status_offer')
                ->default(PartnerProductStatus::QUEUE);
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
            $table->foreign('partner_id')
                ->references('id')
                ->on('partners');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['active', 'sort', 'product_id', 'partner_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_products');
    }
}
