<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\PartnerProduct\Http\Controllers\AdminPartnerProductController;
use Modules\PartnerProduct\Http\Controllers\PartnerProductController;

Route::name('admin.')
    ->prefix('admin')
    ->middleware('auth')
    ->group(function () {
        Route::resource('partner.products', PartnerProductController::class);
        Route::resource('partner-products', AdminPartnerProductController::class)->except([
        		'store',
				'create'
			]
		);
    });
