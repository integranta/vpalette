<?php

namespace Modules\PartnerProduct\Http\Controllers;

use App\DataTables\PartnerProductsDataTable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Modules\PartnerProduct\Services\AdminPartnerProductWebService;

/**
 * Class AdminPartnerProductController
 *
 * @package Modules\PartnerProduct\Http\Controllers
 */
class AdminPartnerProductController extends Controller
{
    /**
     * @var AdminPartnerProductWebService
     */
    private $adminPartnerProductWebService;

    /**
     * AdminPartnerProductController constructor.
     *
     * @param  AdminPartnerProductWebService  $adminPartnerProductWebService
     */
    public function __construct(AdminPartnerProductWebService $adminPartnerProductWebService)
    {
        $this->adminPartnerProductWebService = $adminPartnerProductWebService;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\PartnerProductsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(PartnerProductsDataTable $dataTable)
    {
        return $this->adminPartnerProductWebService->index($dataTable);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->adminPartnerProductWebService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->adminPartnerProductWebService->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        return $this->adminPartnerProductWebService->update($request, $id);
    }
}
