<?php

namespace Modules\PartnerProduct\Http\Controllers;

use App\DataTables\PartnerProductsDataTable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Modules\PartnerProduct\Services\PartnerProductWebService;

/**
 * Class PartnerProductController
 *
 * @package Modules\PartnerProduct\Http\Controllers
 */
class PartnerProductController extends Controller
{
    /**
     * @var PartnerProductWebService
     */
    private $partnerProductWebService;

    /**
     * PartnerProductController constructor.
     *
     * @param  PartnerProductWebService  $partnerProductWebService
     */
    public function __construct(PartnerProductWebService $partnerProductWebService)
    {
        $this->partnerProductWebService = $partnerProductWebService;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  int                                       $partnerId
	 *
	 * @param  \App\DataTables\PartnerProductsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(int $partnerId, PartnerProductsDataTable $dataTable)
    {
        return $this->partnerProductWebService->index($partnerId, $dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $partnerId
     *
     * @return Factory|View
     */
    public function create(int $partnerId): View
    {
        return $this->partnerProductWebService->create($partnerId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int      $partnerId
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function store(int $partnerId, Request $request): RedirectResponse
    {
        return $this->partnerProductWebService->store($partnerId, $request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $partnerId
     * @param  int  $productId
     *
     * @return Factory|RedirectResponse|View
     */
    public function show(int $partnerId, int $productId): View
    {
        return $this->partnerProductWebService->show($partnerId, $productId);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $partnerId
     * @param  int  $productId
     *
     * @return Factory|RedirectResponse|Response|View
     */
    public function edit(int $partnerId, int $productId): View
    {
        return $this->partnerProductWebService->edit($partnerId, $productId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $partnerId
     * @param  int      $productId
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $partnerId, int $productId): RedirectResponse
    {
        return $this->partnerProductWebService->update($request, $partnerId, $productId);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $partnerId
     * @param  int  $productId
     *
     * @return RedirectResponse
     */
    public function destroy(int $partnerId, int $productId): RedirectResponse
    {
        return $this->partnerProductWebService->destroy($partnerId, $productId);
    }
}
