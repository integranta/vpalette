<?php

return [
    'name'    => 'PartnerProduct',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all partner products as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'portal'  => [
        'cache_key' => env('PARTNER_PRODUCTS_CACHE_KEY', 'index_portal_partner_products'),
        'cache_ttl' => env('PARTNER_PRODUCTS_CACHE_TTL', 360000),
    ],
    'partner' => [
        'cache_key' => env('PARTNER_PRODUCTS_CACHE_KEY', 'index_partner_partner_products'),
        'cache_ttl' => env('PARTNER_PRODUCTS_CACHE_TTL', 360000),
    ],

];
