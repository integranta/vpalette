@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('libs/css/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/product.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.partner.products.index', ['partner' => $partnerId]) }}"
                   class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')

                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                @include('product::includes.edit-tabs')
                            </ul>

                            {!!
                                Form::open([
                                    'route' => ['admin.partner.products.update', $partnerId, $productId],
                                    'method' => 'PATCH',
                                    'files' => true
                                ])
                            !!}
                            <div class="tab-content tab-bordered" id="myTab3Content">
                                <div
                                    class="tab-pane fade show active"
                                    id="element"
                                    role="tabpanel"
                                    aria-labelledby="element-tab"
                                >
                                    <h3 class="tile-title">Информация о товаре</h3>
                                    <hr>
                                    <div class="tile-body">
                                        <div class="form-group">
                                            <label class="control-label" for="name">Название</label>
                                            <input
                                                class="form-control @error('name') is-invalid @enderror"
                                                type="text"
                                                placeholder="Введите название товара"
                                                id="name"
                                                name="name"
                                                value="{{ old('name', $partnerProduct->product->name) }}"
                                            />
                                            <input type="hidden" name="product_id"
                                                   value="{{ $partnerProduct->product->id }}">
                                            <div class="invalid-feedback active">
                                                <i class="fa fa-exclamation-circle fa-fw"></i> @error('name')
                                                <span>{{ $message }}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="article">Артикул</label>
                                                    <input
                                                        class="form-control @error('article') is-invalid @enderror"
                                                        type="text"
                                                        placeholder="Введите артикул товара"
                                                        id="article"
                                                        name="article"
                                                        value="{{ old('article', $partnerProduct->product->article) }}"
                                                    />
                                                    <div class="invalid-feedback active">
                                                        <i class="fa fa-exclamation-circle fa-fw"></i> @error('article')
                                                        <span>{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="barcode">Штрих-код</label>
                                                    <input
                                                        class="form-control @error('barcode') is-invalid @enderror"
                                                        type="text"
                                                        placeholder="Введите штрих-код товара"
                                                        id="barcode"
                                                        name="barcode"
                                                        value="{{ old('barcode', $partnerProduct->product->barcode) }}"
                                                    />
                                                    <div class="invalid-feedback active">
                                                        <i class="fa fa-exclamation-circle fa-fw"></i> @error('barcode')
                                                        <span>{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="brand_id">Бренд</label>
                                                    <select name="brand_id" id="brand_id"
                                                            class="form-control @error('brand_id') is-invalid @enderror">
                                                        <option value="0">Выберите бренд</option>
                                                        @foreach($brands as $brand)
                                                            @if ($partnerProduct->product->brand_id === $brand->id)
                                                                <option value="{{ $brand->id }}"
                                                                        selected>{{ $brand->name }}</option>
                                                            @else
                                                                <option
                                                                    value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback active">
                                                        <i class="fa fa-exclamation-circle fa-fw"></i> @error('brand_id')
                                                        <span>{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="sort">Сортировка</label>
                                                    <input
                                                        class="form-control @error('sort') is-invalid @enderror"
                                                        type="text"
                                                        placeholder="Введите сортировку товара"
                                                        id="sort"
                                                        name="sort"
                                                        value="{{ old('sort', $partnerProduct->product->sort) }}"
                                                    />
                                                    <div class="invalid-feedback active">
                                                        <i class="fa fa-exclamation-circle fa-fw"></i> @error('sort')
                                                        <span>{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label" for="categories">Категория</label>
                                                    <select name="categories[]" id="categories" class="form-control"
                                                            multiple>
                                                        @foreach($categories as $category)
                                                            @php $check = in_array($category->id, $partnerProduct->product->categories->pluck('id')->toArray(), true) ? 'selected' : ''@endphp
                                                            <option value="{{ $category->id }}" {{ $check }}>
                                                                {{ $category->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="weight">Вес</label>
                                                    <input
                                                        class="form-control"
                                                        type="text"
                                                        placeholder="Введите вес товара"
                                                        id="weight"
                                                        name="weight"
                                                        value="{{ old('weight', $partnerProduct->product->weight) }}"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <!-- Active field -->
                                                <div class="form-group row col-md-12">
                                                    <span class="col-form-label text-md-right">Активность:</span>
                                                    <div
                                                        class="custom-control custom-checkbox d-flex align-self-center">
                                                        {!! Form::hidden('active', 0) !!}
                                                        {!!
                                                            Form::checkbox(
                                                                'active',
                                                                true,
                                                                $partnerProduct->product->active === true ? 1 : 0,
                                                                ['class' => 'custom-control-input', 'id' => 'active']
                                                            )
                                                        !!}
                                                        <label class="custom-control-label ml-lg-3"
                                                               for="active"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <!-- Popular field -->
                                                <div class="form-group row col-md-12">
                                                    <span class="col-form-label text-md-right">Популярное:</span>
                                                    <div
                                                        class="custom-control custom-checkbox d-flex align-self-center">
                                                        {!! Form::hidden('is_popular', 0) !!}
                                                        {!!
                                                            Form::checkbox(
                                                                'is_popular',
                                                                true,
                                                                $partnerProduct->product->is_popular === true ? 1 : 0,
                                                                ['class' => 'custom-control-input', 'id' => 'is_popular']
                                                            )
                                                        !!}
                                                        <label class="custom-control-label ml-lg-3"
                                                               for="is_popular"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <!-- Popular field -->
                                                <div class="form-group row col-md-12">
                                                    <span class="col-form-label text-md-right">Новинка:</span>
                                                    <div
                                                        class="custom-control custom-checkbox d-flex align-self-center">
                                                        {!! Form::hidden('is_new', 0) !!}
                                                        {!!
                                                            Form::checkbox(
                                                                'is_new',
                                                                true,
                                                                $partnerProduct->product->is_new === true ? 1 : 0,
                                                                ['class' => 'custom-control-input', 'id' => 'is_new']
                                                            )
                                                        !!}
                                                        <label class="custom-control-label ml-lg-3"
                                                               for="is_new"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="preview" role="tabpanel" aria-labelledby="preview-tab">
                                    <div class="row">
                                        <div class="col-sm-12 col-lg-12">
                                            <div class="form-group">
                                                <label class="control-label" for="about">Описание товара</label>
                                                <textarea
                                                    name="about"
                                                    id="about"
                                                    rows="8"
                                                    class="form-control tinyMCE"
                                                >
                                           {{ old('about', $partnerProduct->product->about) }}
                                       </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                                    <!-- Meta Title Field -->
                                    <div class="form-group row align-items-center">
                                        {!!
                                            Form::label(
                                                'title',
                                                'Meta title:',
                                                ['class' => 'form-control-label col-sm-3 text-md-right']
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::text(
                                                    'title',
                                                    old('title',
                                                    !empty($partnerProduct->product->seo->title)
                                                        ? $partnerProduct->product->seo->title
                                                        : ''
                                                     ),
                                                    ['class' => 'form-control']
                                                )
                                            !!}
                                        </div>
                                    </div>

                                    <!-- Description Field -->
                                    <div class="form-group row align-items-center">
                                        {!!
                                            Form::label(
                                                'description',
                                                'Meta Description:',
                                                ['class' => 'form-control-label col-sm-3 text-md-right']
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::textarea(
                                                    'description',
                                                    old('description',
                                                    !empty($partnerProduct->product->seo->description)
                                                        ? $partnerProduct->product->seo->description
                                                        : ''
                                                    ),
                                                    ['class' => 'form-control']
                                                )
                                            !!}
                                        </div>
                                    </div>
                                    <!-- Keywords Field -->
                                    <div class="form-group row align-items-center">
                                        {!!
                                            Form::label(
                                                'keywords',
                                                'Keywords:',
                                                ['class' => 'form-control-label col-sm-3 text-md-right']
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::textarea(
                                                    'keywords',
                                                    old('keywords',
                                                    !empty($partnerProduct->product->seo->keywords)
                                                        ? $partnerProduct->product->seo->keywords
                                                        : ''
                                                    ),
                                                    ['class' => 'form-control codeeditor', 'id' => 'keywords']
                                                )
                                            !!}
                                        </div>
                                    </div>
                                    <!-- Canonical URL Field -->
                                    <div class="form-group row align-items-center">
                                        {!!
                                            Form::label(
                                                'canonical_url',
                                                'Canonical URL:',
                                                [
                                                    'class' => 'form-control-label col-sm-3 text-md-right',
                                                    'id' => 'canonical_url'
                                                ]
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::text(
                                                    'canonical_url',
                                                    old('canonical_url',
                                                    !empty($partnerProduct->product->seo->canonical_url)
                                                        ? $partnerProduct->product->seo->canonical_url
                                                        : ''
                                                    ),
                                                    ['class' => 'form-control']
                                                )
                                            !!}
                                        </div>
                                    </div>

                                    <div class="card-header">
                                        <h4>Реклама</h4>
                                    </div>

                                    <div class="form-group row">
                                        {!!
                                            Form::label(
                                                'ya_direct',
                                                'Яндекс.Директ:',
                                                ['class' => 'form-control-label col-sm-3 text-md-right']
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::textarea(
                                                    'ya_direct',
                                                    old('ya_direct',
                                                    !empty($partnerProduct->product->seo->ya_direct)
                                                        ? $partnerProduct->product->seo->ya_direct
                                                        : ''
                                                    ),
                                                    [
                                                        'class' => 'form-control codeeditor',
                                                        'id' => 'ya_direct'
                                                    ]
                                                )
                                            !!}
                                        </div>
                                    </div>
                                    <div class="card-header">
                                        <h4>Счётчики:</h4>
                                    </div>
                                    <div class="form-group row">
                                        {!!
                                            Form::label(
                                                'google_tag_manager',
                                                'Google Tag Manager:',
                                                ['class' => 'form-control-label col-sm-3 text-md-right']
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::textarea(
                                                    'google_tag_manager',
                                                    old('google_tag_manager',
                                                    !empty($partnerProduct->product->seo->google_tag_manager)
                                                        ? $partnerProduct->product->seo->google_tag_manager
                                                        : ''
                                                    ),
                                                    [
                                                        'class' => 'form-control codeeditor',
                                                        'id' => 'google_tag_manager'
                                                    ]
                                                )
                                            !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {!!
                                            Form::label(
                                                'google_analytics',
                                                'Google Analytics:',
                                                ['class' => 'form-control-label col-sm-3 text-md-right']
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::textarea(
                                                    'google_analytics',
                                                    old('google_analytics', !empty($partnerProduct->product->seo->google_analytics)
                                                        ? $partnerProduct->product->seo->google_analytics
                                                        : ''
                                                    ),
                                                    [
                                                        'class' => 'form-control codeeditor',
                                                        'id' => 'google_analytics'
                                                    ]
                                                )
                                            !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {!!
                                            Form::label(
                                                'ya_metrica',
                                                'Яндекс.Метрика:',
                                                ['class' => 'form-control-label col-sm-3 text-md-right']
                                            )
                                        !!}
                                        <div class="col-sm-6 col-md-9">
                                            {!!
                                                Form::textarea(
                                                    'ya_metrica',
                                                    old('ya_metrica',
                                                    !empty($partnerProduct->product->seo->ya_metrica)
                                                        ? $partnerProduct->product->seo->ya_metrica
                                                        : ''
                                                    ),
                                                    [
                                                        'class' => 'form-control codeeditor',
                                                        'id' => 'ya_metrica'
                                                    ]
                                                )
                                            !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
                                    <div class="tab-pane" id="images">
                                        <div class="tile">
                                            <h3 class="tile-title">Загруженные изображения</h3>
                                            <hr>
                                            <div class="tile-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input
                                                            type="file"
                                                            name="pictures[]"
                                                            id="picture"
                                                            multiple
                                                            accept="image/*"
                                                        >
                                                    </div>
                                                </div>
                                                @if ($partnerProduct->product->images)
                                                    <hr>
                                                    <div class="row">
                                                        @foreach($partnerProduct->product->images as $image)
                                                            <div class="col-md-3">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <img src="{{ asset($image->full) }}"
                                                                             id="brandLogo"
                                                                             class="img-fluid" alt="img">
                                                                        <a class="card-link float-right text-danger"
                                                                           href="{{ route('admin.products.images.delete', $image->id) }}">
                                                                            <i class="fa fa-fw fa-lg fa-trash"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="attributes" role="tabpanel"
                                     aria-labelledby="attributes-tab">
                                    <product-attributes
                                        :productid="{{ $partnerProduct->product->id }}"></product-attributes>
                                </div>
                            </div>
                            <div class="card-footer">
                                <!-- Submit Field -->
                                <div class="form-group row col-mb-12">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-4 col-md-4">
                                        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary', 'id' => "updateProduct"]) !!}
                                        <a href="{!! route('admin.products.index') !!}"
                                           class="btn btn-danger">Отмена</a>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="product_id" name="product_id"
                                   value="{{ $partnerProduct->product->id }}">
                            {{ csrf_field() }}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    {{-- JS Libraies --}}
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('libs/js/tinymce/langs/ru.js') }}"></script>
    <script src="{{ asset('libs/js/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
    {{--  Module JS File  --}}
    <script src="{{ asset('js/edit-product.js') }}"></script>
@endpush
