@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('libs/css/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dropzone/basic.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dropzone/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/product.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.partner.products.index', ['partner' => $partnerId]) }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-12">
                    @include('admin.partials.flash')
                    {!! Form::open(['route' => [
                        'admin.partner.products.store',
                        $partnerId,
                        'files' => true,
                        'enctype'=>'multipart/form-data',
                        'method' => 'POST',
                        'id' => 'partner-product-form'
                        ]])
                    !!}
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ $subTitle }}</h4>
                        </div>
                        <div class="card-body">
                            @include('partnerproduct::includes.tabs')
                            @include('partnerproduct::partnerproduct.fields')
                        </div>
                        <div class="card-footer">
                            <!-- Submit Field -->
                            <div class="form-group row col-mb-12">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-4 col-md-4">
                                    {!! Form::submit('Предложить',
                                        ['class' => 'btn btn-primary', 'id' => 'submitForm'])
                                    !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    {{-- JS Libraies --}}
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('libs/js/tinymce/langs/ru.js') }}"></script>
    <script src="{{ asset('libs/js/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    {{--  Module JS File  --}}
    <script src="{{ asset('js/create-product.js') }}"></script>
@endpush
