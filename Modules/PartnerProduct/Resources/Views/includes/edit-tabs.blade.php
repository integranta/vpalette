<li class="nav-item">
    <a
        class="nav-link active"
        id="element-tab"
        data-toggle="tab"
        href="#element"
        role="tab"
        aria-controls="element"
        aria-selected="true"
    >
        Элемент
    </a>
</li>
<li class="nav-item">
    <a
        class="nav-link"
        id="preview-tab"
        data-toggle="tab"
        href="#preview"
        role="tab"
        aria-controls="preview"
        aria-selected="false">
        Описание
    </a>
</li>
<li class="nav-item">
    <a
        class="nav-link"
        id="images-tab"
        data-toggle="tab"
        href="#images"
        role="tab"
        aria-controls="image"
        aria-selected="false"
    >
        Изображения
    </a>
</li>
<li class="nav-item">
    <a
        class="nav-link"
        id="attributes-tab"
        data-toggle="tab"
        href="#attributes"
        role="tab"
        aria-controls="attributes"
        aria-selected="false"
    >
        Атрибуты
    </a>
</li>
<li class="nav-item">
    <a
        class="nav-link"
        id="seo-tab"
        data-toggle="tab"
        href="#seo"
        role="tab"
        aria-controls="seo"
        aria-selected="false"
    >
        SEO
    </a>
</li>
