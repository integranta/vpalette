<ul class="nav nav-tabs" id="myTab2" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="element-tab" data-toggle="tab" href="#element"
           role="tab"
           aria-controls="element" aria-selected="true">Элемент</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="preview-tab" data-toggle="tab" href="#preview" role="tab"
           aria-controls="description" aria-selected="false">Описание</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab"
           aria-controls="images" aria-selected="false">Картинки</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab"
           aria-controls="seo" aria-selected="false">SEO</a>
    </li>
</ul>
