import {tinyMCEConfiguration} from "../../../../../resources/js/shared/tinyMCEConfig";

$(document).ready(function () {
    tinymce.init({
        ...tinyMCEConfiguration,
        readonly: true,
        selector: ".tinyMCE"
    });
    $('#categories').select2();
});
