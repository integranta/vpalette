<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Заголовок (meta title) для всех страниц.
	|--------------------------------------------------------------------------
	*/
	'title'            => 'Товары от партнёра',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы списка всех товара от партнёра.
	|--------------------------------------------------------------------------
	*/
	'index_subtitle'   => 'Список всех предложенных товаров от партнёра.',
	'index_leadtext'   => 'Здесь вы можете увидеть список всех ваших предложенных товаров.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы создания товара от партнёра.
	|--------------------------------------------------------------------------
	*/
	'create_subtitle'  => 'Предложение добавить новый товар.',
	'create_leadtext'  => 'На этой странице вы можете предложить новый товар для портала и заполнить все поля.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об создания товара от партнёра.
	|--------------------------------------------------------------------------
	*/
	'error_create'     => 'Произошла ошибка при добавлении нового предложенного товара.',
	'success_create'   => 'Предложенный товар успешно был добавлен на рассмотрение.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы просмотра информации об товара от партнёра.
	|--------------------------------------------------------------------------
	*/
	'show_subtitle'    => 'Просмотр информации об предложенном товаре: :product.',
	'show_leadtext'    => 'Здесь вы можете просмотреть информацию об предложенном товаре: :product.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для страницы редактирования товара от партнёра.
	|--------------------------------------------------------------------------
	*/
	'edit_subtitle'    => 'Редактирование информации об предложенном товаре: :product.',
	'edit_leadtext'    => 'Здесь вы можете заполнить поля для редактирования предложенного товара: :product.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об обновление товара от партнёра.
	|--------------------------------------------------------------------------
	*/
	'error_update'     => 'Произошла ошибка при обновлении информации о предложенном товаре.',
	'success_update'   => 'Информация об предложенном товаре успешно обновлена.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об удаления товара от партнёра.
	|--------------------------------------------------------------------------
	*/
	'error_delete'     => 'Произошла ошибка при удалении предложенного товара.',
	'success_delete'   => 'Информация об предложенном товаре успешно удалена.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об извлечения товара от партнёра, через REST API.
	|--------------------------------------------------------------------------
	*/
	'not_found'        => 'Предложенный товар с ID :id не был найден в системе.',
	'success_found'    => 'Предложенный товар с ID :id был найден в системе.',
	
	/*
	|--------------------------------------------------------------------------
	| Переводы для сообщении статуса об извлечения товаров от партнёров, через REST API.
	|--------------------------------------------------------------------------
	*/
	'success_retrieve' => 'Список предложенных товаров успешно извлечён.',
];
