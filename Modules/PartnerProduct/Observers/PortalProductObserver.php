<?php

namespace Modules\PartnerProduct\Observers;

use App\Enums\PartnerProductStatus;
use App\Traits\Cacheable;
use App\User;
use Modules\PartnerProduct\Jobs\NotifyAddedNewPartnerProduct;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusAdded;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusDontMatchSubject;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusExist;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusFalseProduct;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusQueue;
use Modules\PartnerProduct\Models\PartnerProduct;

/**
 * Class PortalProductObserver
 *
 * @package Modules\PartnerProduct\Observers
 */
class PortalProductObserver
{
    use Cacheable;
    
    /**
     * Handle the partner product "created" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function created(PartnerProduct $partnerProduct): void
    {
        $this->incrementCountItemsInCache($partnerProduct, config('partnerproduct.portal.cache_key'));
    }
    
    /**
     * Handle the partner product "updated" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function updated(PartnerProduct $partnerProduct): void
    {
        //
    }
    
    /**
     * Handle the partner product "deleted" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function deleted(PartnerProduct $partnerProduct): void
    {
        $this->decrementCountItemsInCache($partnerProduct, config('partnerproduct.portal.cache_key'));
    }
    
    /**
     * Handle the partner product "restored" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function restored(PartnerProduct $partnerProduct): void
    {
        $this->incrementCountItemsInCache($partnerProduct, config('partnerproduct.portal.cache_key'));
    }
    
    /**
     * Handle the partner product "force deleted" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function forceDeleted(PartnerProduct $partnerProduct): void
    {
        $this->decrementCountItemsInCache($partnerProduct, config('partnerproduct.portal.cache_key'));
    }
}
