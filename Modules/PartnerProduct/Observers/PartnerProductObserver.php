<?php

namespace Modules\PartnerProduct\Observers;

use App\Enums\PartnerProductStatus;
use App\Traits\Cacheable;
use App\User;
use Modules\PartnerProduct\Jobs\NotifyAddedNewPartnerProduct;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusAdded;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusDontMatchSubject;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusExist;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusFalseProduct;
use Modules\PartnerProduct\Jobs\NotifyPartnerProductStatusQueue;
use Modules\PartnerProduct\Models\PartnerProduct;

/**
 * Class PartnerProductObserver
 *
 * @package Modules\PartnerProduct\Observers
 */
class PartnerProductObserver
{
    use Cacheable;
    
    /**
     * Handle the partner product "created" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function created(PartnerProduct $partnerProduct): void
    {
        $partner = $partnerProduct->partner->user;
        $adminUsers = User::role('Admin')->get();
        NotifyPartnerProductStatusQueue::dispatch($partner, $partnerProduct);
        
        foreach ($adminUsers as $key => $user) {
            NotifyAddedNewPartnerProduct::dispatch($user, $partnerProduct);
            $this->incrementCountItemsInCache($partnerProduct, config('partnerproduct.partner.cache_key'));
        }
    }
    
    /**
     * Handle the partner product "updated" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function updated(PartnerProduct $partnerProduct): void
    {
        /** @var string $statusOffer */
        $statusOffer = PartnerProductStatus::fromValue($partnerProduct->status_offer)->value;
        $partner = $partnerProduct->partner->user;
        
        if ($statusOffer === PartnerProductStatus::ADDED) {
            NotifyPartnerProductStatusAdded::dispatch($partner, $partnerProduct);
        }
        
        if ($statusOffer === PartnerProductStatus::DONT_MATCH_SUBJECT) {
            NotifyPartnerProductStatusDontMatchSubject::dispatch($partner, $partnerProduct);
        }
        
        if ($statusOffer === PartnerProductStatus::EXIST) {
            NotifyPartnerProductStatusExist::dispatch($partner, $partnerProduct);
        }
        
        if ($statusOffer === PartnerProductStatus::FALSE_PRODUCT) {
            NotifyPartnerProductStatusFalseProduct::dispatch($partner, $partnerProduct);
        }
    }
    
    /**
     * Handle the partner product "deleted" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function deleted(PartnerProduct $partnerProduct): void
    {
        $this->decrementCountItemsInCache($partnerProduct, config('partnerproduct.partner.cache_key'));
    }
    
    /**
     * Handle the partner product "restored" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function restored(PartnerProduct $partnerProduct): void
    {
        $this->incrementCountItemsInCache($partnerProduct, config('partnerproduct.partner.cache_key'));
    }
    
    /**
     * Handle the partner product "force deleted" event.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function forceDeleted(PartnerProduct $partnerProduct): void
    {
        $this->decrementCountItemsInCache($partnerProduct, config('partnerproduct.partner.cache_key'));
    }
}
