<?php


namespace Modules\PartnerProduct\Services;


use App\DataTables\PartnerProductsDataTable;
use App\Enums\PartnerProductStatus;
use App\Services\BaseService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Brand\Contracts\BrandContract;
use Modules\Category\Contracts\CategoryContract;
use Modules\PartnerProduct\Contracts\PortalPartnerProductContract;

/**
 * Class AdminPartnerProductWebService
 *
 * @package Modules\PartnerProduct\Services
 */
class AdminPartnerProductWebService extends BaseService
{
	/**
	 * @var PortalPartnerProductContract
	 */
	private $adminPartnerProductRepository;
	/**
	 * @var BrandContract
	 */
	private $brandRepository;
	/**
	 * @var CategoryContract
	 */
	private $categoryRepository;
	
	/**
	 * AdminPartnerProductWebService constructor.
	 *
	 * @param  PortalPartnerProductContract  $adminPartnerProductRepository
	 * @param  BrandContract                 $brandRepository
	 * @param  CategoryContract              $categoryRepository
	 */
	public function __construct(
		PortalPartnerProductContract $adminPartnerProductRepository,
		BrandContract $brandRepository,
		CategoryContract $categoryRepository
	) {
		$this->adminPartnerProductRepository = $adminPartnerProductRepository;
		$this->brandRepository = $brandRepository;
		$this->categoryRepository = $categoryRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\PartnerProductsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
	public function index(PartnerProductsDataTable $dataTable)
	{
		$this->setPageTitle(
			__('partnerproduct::messages.title'),
			__('partnerproduct::messages.admin.index_subtitle'),
			__('partnerproduct::messages.admin.index_leadtext')
		);
		
		return $dataTable->render('partnerproduct::admin.index');
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|View
	 */
	public function show(int $id): View
	{
		$partnerProduct = $this->adminPartnerProductRepository->findPartnerProductById($id);
		
		if (!$partnerProduct) {
			$this->responseRedirect(
				'admin.partner-products.index',
				__('partnerproduct::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$brands = $this->brandRepository->listBrands();
		$categories = $this->categoryRepository->listCategories();
		$partnerProductStatues = PartnerProductStatus::toSelectArray();
		
		$this->setPageTitle(
			__('partnerproduct::messages.title'),
			__('partnerproduct::messages.admin.show_subtitle', ['product' => $partnerProduct->product->name]),
			__('partnerproduct::messages.admin.show_leadtext', ['product' => $partnerProduct->product->name])
		);
		
		
		return view('partnerproduct::admin.show', compact(
			'partnerProduct',
			'partnerProductStatues',
			'brands',
			'categories'
		));
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return Factory|View
	 */
	public function edit(int $id): View
	{
		$partnerProduct = $this->adminPartnerProductRepository->findPartnerProductById($id);
		$brands = $this->brandRepository->listBrands();
		$categories = $this->categoryRepository->listCategories();
		$partnerProductStatuses = PartnerProductStatus::toSelectArray();
		
		if (!$partnerProduct) {
			$this->responseRedirect(
				'admin.partner-products.index',
				__('partnerproduct::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$this->setPageTitle(
			__('partnerproduct::messages.title'),
			__('partnerproduct::messages.admin.edit_subtitle', ['product' => $partnerProduct->product->name]),
			__('partnerproduct::messages.admin.edit_leadtext', ['product' => $partnerProduct->product->name])
		);
		
		
		return view('partnerproduct::admin.edit', compact(
			'partnerProduct',
			'partnerProductStatuses',
			'brands',
			'categories'
		));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request  $request
	 * @param  int      $id
	 *
	 * @return RedirectResponse
	 */
	public function update(Request $request, int $id): RedirectResponse
	{
		$partnerProduct = $this->adminPartnerProductRepository->findPartnerProductById($id);
		
		if (!$partnerProduct) {
			$this->responseRedirect(
				'admin.partner-products.index',
				__('partnerproduct::messages.not_found', ['id' => $id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$partnerProduct = $this->adminPartnerProductRepository->updatePartnerProduct($request->all(), $id);
		
		if (!$partnerProduct) {
			return $this->responseRedirectBack(
				__('partnerproduct::messages.admin.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partner-products.index',
			__('partnerproduct::messages.admin.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
