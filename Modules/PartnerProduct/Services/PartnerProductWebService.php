<?php


namespace Modules\PartnerProduct\Services;


use App\DataTables\PartnerProductsDataTable;
use App\Services\BaseService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Modules\Brand\Contracts\BrandContract;
use Modules\Category\Contracts\CategoryContract;
use Modules\PartnerProduct\Contracts\PartnerProductContract;

/**
 * Class PartnerProductWebService
 *
 * @package Modules\PartnerProduct\Services
 */
class PartnerProductWebService extends BaseService
{
	/**
	 * @var PartnerProductContract
	 */
	private $partnerProductRepository;
	/**
	 * @var CategoryContract
	 */
	private $categoryRepository;
	/**
	 * @var BrandContract
	 */
	private $brandRepository;
	
	/**
	 * PartnerProductController constructor.
	 *
	 * @param  PartnerProductContract  $partnerProductRepository
	 * @param  CategoryContract        $categoryRepository
	 * @param  BrandContract           $brandRepository
	 */
	public function __construct(
		PartnerProductContract $partnerProductRepository,
		CategoryContract $categoryRepository,
		BrandContract $brandRepository
	) {
		$this->partnerProductRepository = $partnerProductRepository;
		$this->categoryRepository = $categoryRepository;
		$this->brandRepository = $brandRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  int                                       $partnerId
	 *
	 * @param  \App\DataTables\PartnerProductsDataTable  $dataTable
	 *
	 * @return Factory|View
	 */
	public function index(int $partnerId, PartnerProductsDataTable $dataTable)
	{
		$this->setPageTitle(
			__('partnerproduct::messages.title'),
			__('partnerproduct::messages.index_subtitle'),
			__('partnerproduct::messages.index_leadtext')
		);
		
		return $dataTable->render('partnerproduct::partnerproduct.index', compact('partnerId'));
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @param  int  $partnerId
	 *
	 * @return Factory|View
	 */
	public function create(int $partnerId): View
	{
		$brands = $this->brandRepository->listBrands();
		$categories = $this->categoryRepository->listCategories();
		
		$this->setPageTitle(
			__('partnerproduct::messages.title'),
			__('partnerproduct::messages.create_subtitle'),
			__('partnerproduct::messages.create_leadtext')
		);
		
		return view('partnerproduct::partnerproduct.create', compact(
			'partnerId',
			'brands',
			'categories'
		));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  int      $partnerId
	 * @param  Request  $request
	 *
	 * @return RedirectResponse
	 */
	public function store(int $partnerId, Request $request): RedirectResponse
	{
		$partnerProduct = $this->partnerProductRepository->createPartnerProduct($partnerId, $request->all());
		
		if (!$partnerProduct) {
			return $this->responseRedirectBack(
				__('partnerproduct::messages.error_create'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partner.products.index',
			__('partnerproduct::messages.success_create'),
			self::SUCCESS_RESPONSE,
			false,
			false,
			['partner' => $partnerId]
		);
	}
	
	/**
	 * Show the specified resource.
	 *
	 * @param  int  $partnerId
	 * @param  int  $productId
	 *
	 * @return Factory|RedirectResponse|View
	 */
	public function show(int $partnerId, int $productId): View
	{
		$partnerProduct = $this->partnerProductRepository->findPartnerProductById($partnerId, $productId);
		$brands = $this->brandRepository->listBrands();
		$categories = $this->categoryRepository->listCategories();
		
		if (!$partnerProduct) {
			return $this->responseRedirect(
				'admin.partner.products.index',
				__('partnerproduct::messages.not_found', ['id' => $partnerProduct->id]),
				self::FAIL_RESPONSE,
				true,
				false,
				['partner' => $partnerId, 'product' => $productId]
			);
		}
		
		$this->setPageTitle(
			__('partnerproduct::messages.title'),
			__('partnerproduct::messages.show_subtitle', ['product' => $partnerProduct->product->name]),
			__('partnerproduct::messages.show_leadtext', ['product' => $partnerProduct->product->name])
		);
		
		return view('partnerproduct::partnerproduct.show', compact(
			'partnerId',
			'productId',
			'partnerProduct',
			'brands',
			'categories'
		));
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $partnerId
	 * @param  int  $productId
	 *
	 * @return Factory|RedirectResponse|Response|View
	 */
	public function edit(int $partnerId, int $productId): View
	{
		$partnerProduct = $this->partnerProductRepository->findPartnerProductById($partnerId, $productId);
		$brands = $this->brandRepository->listBrands();
		$categories = $this->categoryRepository->listCategories();
		
		if (!$partnerProduct) {
			return $this->responseRedirect(
				'admin.partner.products.index',
				__('partnerproduct::messages.not_found', ['id' => $partnerProduct->id]),
				self::FAIL_RESPONSE,
				true,
				false,
				['partner' => $partnerId, 'product' => $productId]
			);
		}
		
		$this->setPageTitle(
			__('partnerproduct::messages.title'),
			__('partnerproduct::messages.edit_subtitle', ['product' => $partnerProduct->product->name]),
			__('partnerproduct::messages.edit_leadtext', ['product' => $partnerProduct->product->name])
		);
		
		return view('partnerproduct::partnerproduct.edit', compact('partnerProduct',
			'partnerId',
			'productId',
			'brands',
			'categories'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request  $request
	 * @param  int      $partnerId
	 * @param  int      $productId
	 *
	 * @return RedirectResponse
	 */
	public function update(Request $request, int $partnerId, int $productId): RedirectResponse
	{
		$partnerProduct = $this->partnerProductRepository->findPartnerProductById($partnerId, $productId);
		
		if (!$partnerProduct) {
			return $this->responseRedirect(
				'admin.partner.products.index',
				__('partnerproduct::messages.not_found', ['id' => $partnerProduct->id]),
				self::FAIL_RESPONSE,
				true,
				false,
				['partner' => $partnerId, 'product' => $productId]
			);
		}
		
		$partnerProduct = $this->partnerProductRepository->updatePartnerProduct(
			$request->all(),
			$partnerId,
			$productId
		);
		
		if (!$partnerProduct) {
			return $this->responseRedirectBack(
				__('partnerproduct::messages.error_update'),
				self::FAIL_RESPONSE,
				true,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partner.products.index',
			__('partnerproduct::messages.success_update'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $partnerId
	 * @param  int  $productId
	 *
	 * @return RedirectResponse
	 */
	public function destroy(int $partnerId, int $productId): RedirectResponse
	{
		$partnerProduct = $this->partnerProductRepository->findPartnerProductById(
			$partnerId,
			$productId
		);
		
		if (!$partnerProduct) {
			return $this->responseRedirect(
				'admin.partner.products.index',
				__('partnerproduct::messages.not_found', ['id' => $partnerProduct->id]),
				self::FAIL_RESPONSE,
				true,
				false
			);
		}
		
		$partnerProduct = $this->partnerProductRepository->deletePartnerProduct($partnerId, $productId);
		
		if (!$partnerProduct) {
			return $this->responseRedirectBack(
				__('partnerproduct::messages.error_delete'),
				self::FAIL_RESPONSE,
				false,
				true
			);
		}
		
		return $this->responseRedirect(
			'admin.partner.products.index',
			__('partnerproduct::messages.success_delete'),
			self::SUCCESS_RESPONSE,
			false,
			false
		);
	}
}
