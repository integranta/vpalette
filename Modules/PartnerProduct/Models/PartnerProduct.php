<?php

namespace Modules\PartnerProduct\Models;

use App\Enums\PartnerProductStatus;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Partner\Models\Partner;
use Modules\Product\Models\Product;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class PartnerProduct
 *
 * @package Modules\PartnerProduct\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property int $productId
 * @property int $partnerId
 * @property string $statusOffer
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Modules\Partner\Models\Partner $partner
 * @property-read \Modules\Product\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerProduct\Models\PartnerProduct onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct wherePartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereStatusOffer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\PartnerProduct\Models\PartnerProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerProduct\Models\PartnerProduct withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\PartnerProduct\Models\PartnerProduct withoutTrashed()
 * @mixin \Eloquent
 */
class PartnerProduct extends Model
{
    use SoftDeletes;
    use CastsEnums;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'partner_products';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'partner_id',
        'status_offer',
        'active',
        'sort'
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'partner_id' => 'integer',
        'status_offer' => 'string',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'status_offer' => PartnerProductStatus::class
    ];

    /**
     * Возвращает список значений статусов предложения добавить товар партнёром из enum класса для select тега.
     *
     * @return array
     */
    public static function partnerProductStatuses(): array
    {
        return PartnerProductStatus::toSelectArray();
    }

    /**
     * Получить партнёра, который предложил добавить этот товар.
     *
     * @return BelongsTo
     */
    public function partner(): BelongsTo
    {
        return $this->belongsTo(Partner::class);
    }

    /**
     * Получить товар, который был добавлен.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
