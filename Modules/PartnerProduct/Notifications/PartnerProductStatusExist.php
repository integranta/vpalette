<?php

namespace Modules\PartnerProduct\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\PartnerProduct\Models\PartnerProduct;

/**
 * Class PartnerProductStatusExist
 *
 * @package Modules\PartnerProduct\Notifications
 */
class PartnerProductStatusExist extends Notification
{
    use Queueable;
    
    /** @var PartnerProduct */
    private $partnerProduct;
    
    /**
     * Create a new notification instance.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function __construct(PartnerProduct $partnerProduct)
    {
        $this->partnerProduct = $partnerProduct;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail', 'database', 'broadcast'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject("Предложение добавить {$this->partnerProduct->product->name} - отказан")
            ->greeting('Привет Партнёр!')
            ->line("Хотим сообщить Вам что ваше предложение добавить {$this->partnerProduct->product->name}")
            ->line('был отказн по причине, так как он уже существует на портале.')
            ->action('Перейти',
                route('admin.partner.products.show',
                    [
                        'partner' => $this->partnerProduct->partner_id,
                        'product' => $this->partnerProduct->product_id
                    ]))
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'message' => "Предложение {$this->partnerProduct->product->name} - отказан",
            'typeNotify' => 'partner_product_exist',
            'url' => route('admin.partner.products.show', [
                'partner' => $this->partnerProduct->partner_id,
                'product' => $this->partnerProduct->product_id
            ])
        ];
    }
    
    /**
     * @param $notifiable
     *
     * @return array
     */
    public function toBroadcast($notifiable): array
    {
        return [
            'message' => "Предложение {$this->partnerProduct->product->name} - отказан",
            'typeNotify' => 'partner_product_exist',
            'url' => route('admin.partner.products.show', [
                'partner' => $this->partnerProduct->partner_id,
                'product' => $this->partnerProduct->product_id
            ])
        ];
    }
}
