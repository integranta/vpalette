<?php

namespace Modules\PartnerProduct\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\PartnerProduct\Models\PartnerProduct;

/**
 * Class AddedNewPartnerProduct
 *
 * @package Modules\PartnerProduct\Notifications
 */
class AddedNewPartnerProduct extends Notification
{
    use Queueable;
    
    /** @var PartnerProduct */
    private $partnerProduct;
    
    /**
     * Create a new notification instance.
     *
     * @param  PartnerProduct  $partnerProduct
     *
     * @return void
     */
    public function __construct(PartnerProduct $partnerProduct)
    {
        $this->partnerProduct = $partnerProduct;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail', 'database', 'broadcast'];
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject("Поступило предложение добавить {$this->partnerProduct->product->name}")
            ->greeting("Партнёр {$this->partnerProduct->partner->user->name}")
            ->line("Хочет предложить добавить свой товар {$this->partnerProduct->product->name}")
            ->action('Перейти к предложению',
                route('admin.partner-products.show', [
                    'partner_product' => $this->partnerProduct->id
                ])
            )
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'message' => "Партнёр {$this->partnerProduct->partner->user->name} предложил добавить {$this->partnerProduct->product->name}",
            'typeNotify' => 'added_partner_product',
            'url' => route('admin.partner-products.show', [
                'partner_product' => $this->partnerProduct->id,
            ])
        ];
    }
    
    /**
     * @param $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'message' => "Партнёр {$this->partnerProduct->partner->user->name} предложил добавить {$this->partnerProduct->product->name}",
            'typeNotify' => 'added_partner_product',
            'url' => route('admin.partner-products.show', [
                'partner_product' => $this->partnerProduct->id,
            ])
        ]);
    }
}
