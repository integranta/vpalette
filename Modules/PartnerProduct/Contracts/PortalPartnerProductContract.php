<?php


namespace Modules\PartnerProduct\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\PartnerProduct\Models\PartnerProduct;

/**
 * Interface PortalPartnerProductContract
 *
 * @package Modules\PartnerProduct\Contracts
 */
interface PortalPartnerProductContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return null|Collection
     */
    public function listPartnerProducts(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): ?Collection;
    
    /**
     * @param  int  $id
     *
     * @return null|PartnerProduct
     */
    public function findPartnerProductById(int $id): ?PartnerProduct;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return bool
     */
    public function updatePartnerProduct(array $params, int $id): bool;
    
    
    /**
     * @param  array  $data
     *
     * @return null|PartnerProduct
     */
    public function findPartnerProductBy(array $data): ?PartnerProduct;
}
