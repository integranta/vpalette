<?php


namespace Modules\PartnerProduct\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\PartnerProduct\Models\PartnerProduct;


/**
 * Interface PartnerProductContract
 *
 * @package Modules\PartnerProduct\Contracts
 */
interface PartnerProductContract
{
    /**
     * @param  int     $partnerId
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return null|Collection
     */
    public function listPartnerProducts(
        int $partnerId,
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): ?Collection;

    /**
     * @param  int  $partnerId
     * @param  int  $productId
     *
     * @return null|PartnerProduct
     */
    public function findPartnerProductById(int $partnerId, int $productId): ?PartnerProduct;

    /**
     * @param  int    $partnerId
     * @param  array  $params
     *
     * @return PartnerProduct
     */
    public function createPartnerProduct(int $partnerId, array $params): PartnerProduct;

    /**
     * @param  array  $params
     * @param  int    $partnerId
     * @param  int    $productId
     *
     * @return bool
     */
    public function updatePartnerProduct(array $params, int $partnerId, int $productId): bool;

    /**
     * @param  int  $partnerId
     * @param  int  $productId
     *
     * @return bool
     */
    public function deletePartnerProduct(int $partnerId, int $productId): bool;

    /**
     * @param  array  $data
     *
     * @return null|PartnerProduct
     */
    public function findPartnerProductBy(array $data): ?PartnerProduct;
}
