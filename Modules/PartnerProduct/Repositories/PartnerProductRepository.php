<?php


namespace Modules\PartnerProduct\Repositories;


use App\Enums\PartnerProductStatus;
use App\Enums\ProductType;
use App\Repositories\BaseRepository;
use App\Traits\Seoable;
use App\Traits\UploadAble;
use Arr;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Category\Models\Category;
use Modules\PartnerProduct\Contracts\PartnerProductContract;
use Modules\PartnerProduct\Models\PartnerProduct;
use Modules\Product\Models\Product;
use Storage;
use Str;

/**
 * Class PartnerProductRepository
 *
 * @package Modules\PartnerProduct\Repositories
 */
class PartnerProductRepository extends BaseRepository implements PartnerProductContract
{
    use UploadAble;
    use Seoable;

    /**
     * PartnerProductRepository constructor.
     *
     * @param  PartnerProduct  $model
     */
    public function __construct(PartnerProduct $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function listPartnerProducts(
        int $partnerId,
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): ?Collection {
        $partner = $this->find($partnerId);
        $postfix = Str::of($this->getLastId())->start('_');

        if ($partner) {
            return Cache::remember(
                config('partnerproduct.partner.cache_key').$postfix,
                config('partnerproduct.partner.cache_ttl'),
                function () use ($order, $sort, $columns) {
                    return $this->all($columns, $order, $sort);
                }
            );
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function findPartnerProductById(int $partnerId, int $productId): ?PartnerProduct
    {
        return $this->findOneBy(['partner_id' => $partnerId, 'product_id' => $productId]);
    }

    /**
     * @inheritDoc
     */
    public function createPartnerProduct(int $partnerId, array $params): PartnerProduct
    {
        $fullImagePaths = array();

        if ($params['pictures'] !== null && isset($params['pictures'])) {
            foreach ($params['pictures'] as $key => $picture) {
                $fullImagePaths[$key]['full'] = $this->makeCachedImage(
                    $picture,
                    "partner_products/{$params['name']}"
                );
            }
        }

        $product = Product::create([
            'article' => $params['article'],
            'barcode' => $params['barcode'],
            'name' => $params['name'],
            'about' => $params['about'],
            'weight' => $params['weight'],
            'sort' => $params['sort'],
            'active' => $params['active'],
            'is_popular' => $params['is_popular'],
            'is_new' => $params['is_new'],
            'brand_id' => $params['brand_id'],
            'type_product' => ProductType::PARTNER_PRODUCT
        ]);

        if (isset($params['og_image']))
        {
            $path = 'partner_products/'.Str::of($params['name'])
                    ->trim()
                    ->lower()
                    ->slug('_', 'ru');
            $params['og_image'] = $this->makeCachedImage($params['og_image'], $path);
        }

        $this->setSeoFields($params);
        $product->seo()->create($this->getSeoFields());
        $product->images()->createMany($fullImagePaths);
        $product->save();

        $product->load('categories', 'images');

        $category = Category::find($this->getCategoriesIds($params));
        $catPath = $category->first()->full_path;

        $catPaths = [];

        foreach ($category as $cat) {
            $full = $cat->full_path.'/'.$product->slug;
            $catPaths[] = $full;
        }


        $product->categories()->attach($category);
        $product->save();

        $product->update([
            'full_path' => $catPath.'/'.$product->slug,
            'full_paths' => $catPaths
        ]);

        return $this->create([
            'partner_id' => $partnerId,
            'product_id' => $product->id,
            'status_offer' => PartnerProductStatus::QUEUE
        ]);
    }

    /**
     * @inheritDoc
     */
    public function updatePartnerProduct(array $params, int $partnerId, int $productId): bool
    {
        $product = Product::find($productId);

        if (!$product) {
            $product = $product->fill([
                'article' => $params['article'],
                'barcode' => $params['barcode'],
                'name' => $params['name'],
                'about' => $params['about'],
                'weight' => $params['weight'],
                'sort' => $params['sort'],
                'active' => $params['active'],
                'is_popular' => $params['is_popular'],
                'is_new' => $params['is_new'],
                'brand_id' => $params['brand_id'],
                'type_product' => ProductType::PORTAL_PRODUCT
            ]);
            $this->setSeoFields($params);
            $product->load('seo');
            $product->seo()->update($this->getSeoFields());

            $product->load('categories');
            $category = Category::find($this->getCategoriesIds($params));
            $catPath = $category->first()->full_path;

            $catPaths = [];


            foreach ($category as $cat) {
                $full = $cat->full_path.'/'.$product->slug;
                $catPaths[] = $full;
            }

            $catPaths = array_unique($catPaths);


            $product->categories()->sync($this->getCategoriesIds($params));

            $product->update([
                'full_path' => $catPath.'/'.$product->slug,
                'full_paths' => json_encode($catPaths,  JSON_FORCE_OBJECT, 512)
            ]);
            $product->save();
        }


        return false;
    }

    /**
     * @inheritDoc
     */
    public function deletePartnerProduct(int $partnerId, int $productId): bool
    {
        $partner = $this->find($partnerId);

        if ($partner) {
            return $partner->product->delete($productId);
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function findPartnerProductBy(array $data): ?PartnerProduct
    {
        return $this->findBy($data);
    }

    /**
     * @param  array  $params
     *
     * @return array
     */
    private function getCategoriesIds(array $params): array
    {
        return Arr::get($params, 'categories');
    }
}
