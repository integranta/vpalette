<?php


namespace Modules\PartnerProduct\Repositories;


use App\Repositories\BaseRepository;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\PartnerProduct\Contracts\PortalPartnerProductContract;
use Modules\PartnerProduct\Models\PartnerProduct;
use Str;

/**
 * Class PortalPartnerProductRepository
 *
 * @package Modules\PartnerProduct\Repositories
 */
class PortalPartnerProductRepository extends BaseRepository implements PortalPartnerProductContract
{
    /**
     * PortalPartnerProductRepository constructor.
     *
     * @param  PartnerProduct  $model
     */
    public function __construct(PartnerProduct $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }
    
    /**
     * @inheritDoc
     */
    public function listPartnerProducts(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): ?Collection {
    
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('partnerproduct.portal.cache_key').$postfix,
            config('partnerproduct.portal.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @inheritDoc
     */
    public function findPartnerProductById(int $id): ?PartnerProduct
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function updatePartnerProduct(array $params, int $id): bool
    {
        return $this->update($params, $id);
    }
    
    /**
     * @inheritDoc
     */
    public function findPartnerProductBy(array $data): ?PartnerProduct
    {
        return $this->findBy($data);
    }
}
