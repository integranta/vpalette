<?php

namespace Modules\Discount\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class DiscountPolicy
 *
 * @package Modules\Discount\Policies
 */
class DiscountPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
