<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Modules\Discount\Http\Controllers\API\V1\DiscountAPIController;
use Modules\Discount\Http\Controllers\API\V1\DiscountPartnerAPIController;

Route::prefix('v1')->group(function () {
	Route::apiResource('discount', DiscountAPIController::class);
	Route::apiResource('discount-partner', DiscountPartnerAPIController::class);
	Route::get('discount/get/types', [DiscountAPIController::class, 'getDiscountTypes']);
	Route::get('discount/get/discount-amount-types', [DiscountAPIController::class, 'getDiscountAmountTypes']);
	Route::get('discount/get/discount-period-activities', [DiscountAPIController::class, 'getDiscountPeriodActivity']);
});
