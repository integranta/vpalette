<?php


namespace Modules\Discount\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Discount\Models\Discount;

/**
 * Interface DiscountPartnerContract
 *
 * @package Modules\Discount\Contracts
 */
interface DiscountPartnerContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listPartnerDiscounts(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Discount
     */
    public function findPartnerDiscountById(int $id): ?Discount;
    
    /**
     * @param  array  $params
     *
     * @return Discount
     */
    public function createPartnerDiscount(array $params): Discount;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Discount
     */
    public function updatePartnerDiscount(array $params, int $id): Discount;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deletePartnerDiscount(int $id): bool;
}
