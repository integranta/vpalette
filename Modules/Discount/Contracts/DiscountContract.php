<?php


namespace Modules\Discount\Contracts;


use Illuminate\Database\Eloquent\Collection;
use Modules\Discount\Models\Discount;

/**
 * Interface DiscountContract
 *
 * @package Modules\Discount\Contracts
 */
interface DiscountContract
{
    /**
     * @param  string  $order
     * @param  string  $sort
     * @param  array   $columns
     *
     * @return Collection
     */
    public function listDiscounts(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection;
    
    /**
     * @param  int  $id
     *
     * @return Discount
     */
    public function findDiscountById(int $id): ?Discount;
    
    /**
     * @param  array  $params
     *
     * @return Discount
     */
    public function createDiscount(array $params): Discount;
    
    /**
     * @param  array  $params
     * @param  int    $id
     *
     * @return Discount
     */
    public function updateDiscount(array $params, int $id): Discount;
    
    /**
     * @param  int  $id
     *
     * @return bool
     */
    public function deleteDiscount(int $id): bool;
}
