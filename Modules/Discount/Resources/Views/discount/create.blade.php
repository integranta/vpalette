@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ asset('css/discount.css') }}">
@endsection

@section('content')
    {{-- Если скидку создаёт владелец / администратор портала  --}}
    @hasrole('Admin')
    <discount-portal-create
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}">
    </discount-portal-create>
    @endhasrole

    {{-- Если скидку создаёт партнёр --}}
    @hasrole('Partner')
    <discount-partner-create
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}">
    </discount-partner-create>
    @endrole
@endsection
@push('scripts')
    {{--  Module JS File  --}}
@endpush
