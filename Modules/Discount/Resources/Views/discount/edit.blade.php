@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ mix('css/discount.css') }}">
@endsection

@section('content')
    {{-- Если скидку создаёт владелец / администратор портала  --}}
    @hasrole('Admin')
    <discount-portal-edit
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        discount-id="{{ $discount->id }}">
    </discount-portal-edit>
    @endhasrole

    {{-- Если скидку создаёт партнёр --}}
    @hasrole('Partner')
    <discount-partner-edit
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        discount-id="{{ $discount->id }}">
    </discount-partner-edit>
    @endrole
@endsection

@push('scripts')
    {{--  Module JS File  --}}
    <script src="{{ mix('js/discount.js') }}"></script>
@endpush
