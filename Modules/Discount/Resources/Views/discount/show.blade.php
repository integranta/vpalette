@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection

@section('plugin_css')
    {{-- CSS Module --}}
    <link rel="stylesheet" href="{{ mix('css/discount.css') }}">
@endsection

@section('content')
    {{-- Если скидку создаёт владелец / администратор портала  --}}
    @hasrole('Admin')
    <discount-portal-show
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        discount-id="{{ $discount->id }}">
    </discount-portal-show>
    @endhasrole

    {{-- Если скидку создаёт партнёр --}}
    @hasrole('Partner')
    <discount-partner-show
        page-title="{{ $pageTitle }}"
        sub-title="{{ $subTitle }}"
        lead-text="{{ $leadText }}"
        discount-id="{{ $discount->id }}">
    </discount-partner-show>
    @endrole
@endsection

@push('scripts')
    <script src="{{ mix('js/discount.js') }}"></script>
@endpush
