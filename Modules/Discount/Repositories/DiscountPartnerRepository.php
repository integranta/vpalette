<?php


namespace Modules\Discount\Repositories;


use App\Repositories\BaseRepository;
use App\User;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Discount\Contracts\DiscountPartnerContract;
use Modules\Discount\Models\Discount;
use Str;

/**
 * Class DiscountPartnerRepository
 *
 * @package Modules\Discount\Repositories
 */
class DiscountPartnerRepository extends BaseRepository implements DiscountPartnerContract
{
    
    /**
     * DiscountPartnerRepository constructor.
     *
     * @param  Discount  $model
     */
    public function __construct(Discount $model)
    {
        parent::__construct($model);
        $this->model = $model;
        $this->model->load(['users', 'partnerOffers', 'coupons']);
    }
    
    /**
     * @inheritDoc
     */
    public function listPartnerDiscounts(
        string $order = 'id',
        string $sort = 'desc',
        array $columns = ['*']
    ): Collection {
    
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('discount.partner.cache_key').$postfix,
            config('discount.partner.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @inheritDoc
     */
    public function findPartnerDiscountById(int $id): ?Discount
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function createPartnerDiscount(array $params): Discount
    {
        // Поля для создания новой скидки.
        $discountFields = $this->getArrDiscountFields($params['discountFields']);
        
        // Поля для создания купона
        $this->getArrCouponFields($params['couponFields']);
        
        // Создаём новую скидку.
        $discount = $this->create($discountFields);
        
        $discountId = $discount->id;
        
        // Связываем скидку и предложение.
        $this->model->offers()->sync($this->getArrOffers($params['discountFields'], $discountId));
        
        $arCoupons = [];
        $arUsers = [];
        
        // TODO: Утром это до делать !!!
        
        if (($params['discountFields']['selected_roles']) !== null) {
            
            foreach ($params['discountFields']['selected_roles'] as $key => $role) {
                foreach (User::role($role['name'])->get() as $k => $value) {
                    $arUsers[$key]['user_id'] = $value->id;
                    $arUsers[$key]['discount_id'] = $discountId;
                    $arCoupons[$key]['user_id'] = $value->id;
                    $arCoupons[$key]['discount_id'] = $discountId;
                }
            }
            
            $this->model->users()->sync($arUsers);
        }
        
        if (!empty($params['couponFields']) && $params['discountFields']['save_coupon']) {
            foreach ($arCoupons as $idx => $coupon) {
                $arCoupons[$idx]['active'] = $params['couponFields']['active'];
                $arCoupons[$idx]['code'] = $params['couponFields']['code'];
                $arCoupons[$idx]['type'] = $params['couponFields']['selected_couponType'];
                $arCoupons[$idx]['period_activity'] = $params['couponFields']['selected_couponPeriodActivityType'];
                $arCoupons[$idx]['starts_at'] = $params['couponFields']['starts_at'];
                $arCoupons[$idx]['expires_at'] = $params['couponFields']['expires_at'];
                $arCoupons[$idx]['comments'] = $params['couponFields']['comments'];
                $arCoupons[$idx]['max_uses_count'] = $params['couponFields']['max_uses_count'];
            }
            $discount->coupons()->createMany($arCoupons);
        }
        
        return $discount;
        
    }
    
    /**
     * @inheritDoc
     */
    public function updatePartnerDiscount(array $params, int $id): Discount
    {
        // TODO: Implement updatePartnerDiscount() method.
    }
    
    /**
     * @inheritDoc
     */
    public function deletePartnerDiscount(int $id): bool
    {
        return $this->delete($id);
    }
    
    /**
     * Получить подготовленный массив для работы со скидками.
     *
     * @param  array  $discountFields
     *
     * @return array
     */
    private function getArrDiscountFields(array $discountFields): array
    {
        return [
            'name' => $discountFields['name'],
            'description' => $discountFields['description'],
            'type' => $discountFields['selected_discountType'],
            'amount' => $discountFields['amount'],
            'amount_type' => $discountFields['selected_discountAmountType'],
            'period_activity' => $discountFields['selected_discountPeriodActivityType'],
            'save_coupon' => $discountFields['save_coupon'],
            'starts_at' => $discountFields['starts_at'],
            'expires_at' => $discountFields['expires_at']
        ];
    }
    
    /**
     * Получить подготовленный массив для работы с купонами.
     *
     * @param  array  $couponFields
     *
     * @return array
     */
    private function getArrCouponFields(array $couponFields): array
    {
        return [
            'active' => $couponFields['active'],
            'code' => $couponFields['code'],
            'type' => $couponFields['selected_couponType'],
            'period_activity' => $couponFields['selected_couponPeriodActivityType'],
            'starts_at' => $couponFields['starts_at'],
            'expires_at' => $couponFields['expires_at'],
            'comments' => $couponFields['comments'],
            'max_uses_count' => $couponFields['max_uses_count']
        ];
    }
    
    /**
     * Получить поготовленный массив для синхронизации скидки и предложения.
     *
     * @param  array  $discountFields
     * @param  int    $discountId
     *
     * @return array
     */
    private function getArrOffers(array $discountFields, int $discountId): array
    {
        $arOffers = [];
        
        if ($discountFields['selected_offers'] !== null) {
            foreach ($discountFields['selected_offers'] as $key => $param) {
                $arOffers[$key]['offer_id'] = $param['id'];
                $arOffers[$key]['discount_id'] = $discountId;
            }
        }
        
        return $arOffers;
    }
}
