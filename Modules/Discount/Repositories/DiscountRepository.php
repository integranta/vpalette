<?php


namespace Modules\Discount\Repositories;


use App\Repositories\BaseRepository;
use App\User;
use Cache;
use Illuminate\Database\Eloquent\Collection;
use Modules\Discount\Contracts\DiscountContract;
use Modules\Discount\Models\Discount;
use Str;

/**
 * Class DiscountRepository
 *
 * @package Modules\Discount\Repositories
 */
class DiscountRepository extends BaseRepository implements DiscountContract
{
    
    /**
     * DiscountRepository constructor.
     *
     * @param  Discount  $model
     */
    public function __construct(Discount $model)
    {
        parent::__construct($model);
        $this->model = $model;
        $this->model->load(['users', 'products', 'coupons']);
    }
    
    /**
     * @inheritDoc
     */
    public function listDiscounts(string $order = 'id', string $sort = 'desc', array $columns = ['*']): Collection
    {
        $postfix = Str::of($this->getLastId())->start('_');
        
        return Cache::remember(
            config('discount.portal.cache_key').$postfix,
            config('discount.portal.cache_ttl'),
            function () use ($order, $sort, $columns) {
                return $this->all($columns, $order, $sort);
            }
        );
    }
    
    /**
     * @inheritDoc
     */
    public function findDiscountById(int $id): ?Discount
    {
        return $this->find($id);
    }
    
    /**
     * @inheritDoc
     */
    public function createDiscount(array $params): Discount
    {
        $discount = $this->create([
            'name' => $params['discountFields']['name'],
            'description' => $params['discountFields']['description'],
            'type' => $params['discountFields']['selected_discountType'],
            'amount' => $params['discountFields']['amount'],
            'amount_type' => $params['discountFields']['selected_discountAmountType'],
            'period_activity' => $params['discountFields']['selected_discountPeriodActivityType'],
            'starts_at' => $params['discountFields']['starts_at'],
            'expires_at' => $params['discountFields']['expires_at'],
            'save_coupon' => $params['discountFields']['save_coupon']
        ]);
        
        $discountId = $discount->id;
        
        
        if ($params['discountFields']['selected_products'] !== null) {
            $arProducts = [];
            foreach ($params['discountFields']['selected_products'] as $key => $param) {
                $arProducts[$key]['product_id'] = $param['id'];
                $arProducts[$key]['discount_id'] = $discountId;
            }
            $this->model->products()->sync($arProducts);
        }
        
        $arCoupons = [];
        if (($params['discountFields']['selected_roles']) !== null) {
            $arUsers = [];
            
            foreach ($params['discountFields']['selected_roles'] as $key => $role) {
                foreach (User::role($role['name'])->get() as $k => $value) {
                    $arUsers[$key]['user_id'] = $value->id;
                    $arUsers[$key]['discount_id'] = $discountId;
                    $arCoupons[$key]['user_id'] = $value->id;
                    $arCoupons[$key]['discount_id'] = $discountId;
                }
            }
            
            $this->model->users()->sync($arUsers);
        }
        
        if (!empty($params['couponFields']) && $params['discountFields']['save_coupon']) {
            foreach ($arCoupons as $idx => $coupon) {
                $arCoupons[$idx]['active'] = $params['couponFields']['active'];
                $arCoupons[$idx]['code'] = $params['couponFields']['code'];
                $arCoupons[$idx]['type'] = $params['couponFields']['selected_couponType'];
                $arCoupons[$idx]['period_activity'] = $params['couponFields']['selected_couponPeriodActivityType'];
                $arCoupons[$idx]['starts_at'] = $params['couponFields']['starts_at'];
                $arCoupons[$idx]['expires_at'] = $params['couponFields']['expires_at'];
                $arCoupons[$idx]['comments'] = $params['couponFields']['comments'];
                $arCoupons[$idx]['max_uses_count'] = $params['couponFields']['max_uses_count'];
            }
            $discount->coupons()->createMany($arCoupons);
        }
        
        return $discount;
    }
    
    /**
     * @inheritDoc
     */
    public function updateDiscount(array $params, int $id): Discount
    {
        $discount = $this->update([
            'name' => $params['discountFields']['name'],
            'description' => $params['discountFields']['description'],
            'type' => $params['discountFields']['selected_discountType'],
            'amount' => $params['discountFields']['amount'],
            'amount_type' => $params['discountFields']['selected_discountAmountType'],
            'period_activity' => $params['discountFields']['selected_discountPeriodActivityType'],
            'starts_at' => $params['discountFields']['starts_at'],
            'expires_at' => $params['discountFields']['expires_at'],
            'save_coupon' => $params['discountFields']['save_coupon']
        ], $id);
        
        $discountId = $id;
        
        
        if ($params['discountFields']['selected_products'] !== null) {
            $arProducts = [];
            foreach ($params['discountFields']['selected_products'] as $key => $param) {
                $arProducts[$key]['product_id'] = $param['id'];
                $arProducts[$key]['discount_id'] = $discountId;
            }
            $this->model->products()->sync($arProducts);
        }
        
        $arCoupons = [];
        if ($params['discountFields']['selected_roles'] !== null) {
            $arUsers = [];
            
            foreach ($params['discountFields']['selected_roles'] as $key => $role) {
                foreach (User::role($role['name'])->get() as $k => $value) {
                    $arUsers[$key]['user_id'] = $value->id;
                    $arUsers[$key]['discount_id'] = $discountId;
                    $arCoupons[$key]['user_id'] = $value->id;
                    $arCoupons[$key]['discount_id'] = $discountId;
                }
            }
            $this->model->users()->sync($arUsers);
        }
        
        if (!empty($params['couponFields']) && $params['discountFields']['save_coupon']) {
            foreach ($arCoupons as $idx => $coupon) {
                $arCoupons[$idx]['active'] = $params['couponFields']['active'];
                $arCoupons[$idx]['code'] = $params['couponFields']['code'];
                $arCoupons[$idx]['type'] = $params['couponFields']['selected_couponType'];
                $arCoupons[$idx]['period_activity'] = $params['couponFields']['selected_couponPeriodActivityType'];
                $arCoupons[$idx]['starts_at'] = $params['couponFields']['starts_at'];
                $arCoupons[$idx]['expires_at'] = $params['couponFields']['expires_at'];
                $arCoupons[$idx]['comments'] = $params['couponFields']['comments'];
                $arCoupons[$idx]['max_uses_count'] = $params['couponFields']['max_uses_count'];
            }
            $this->model->coupons()->create($arCoupons);
        }
        
        return $discount;
    }
    
    /**
     * @inheritDoc
     */
    public function deleteDiscount(int $id): bool
    {
        return $this->delete($id);
    }
}
