<?php

namespace Modules\Discount\Models;

use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use App\User;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Coupon\Models\Coupon;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\Product\Models\Product;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * Class Discount
 *
 * @package Modules\Discount\Models
 * @property int $id
 * @property bool $active
 * @property int $sort
 * @property string $name
 * @property string|null $description
 * @property string $type
 * @property int $amount
 * @property string $amountType
 * @property string $periodActivity
 * @property bool $saveCoupon
 * @property string|null $startsAt
 * @property string|null $expiresAt
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property \Illuminate\Support\Carbon|null $deletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Coupon\Models\Coupon[] $coupons
 * @property-read int|null $couponsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\PartnerOffer\Models\PartnerOffer[] $partnerOffers
 * @property-read int|null $partnerOffersCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Product\Models\Product[] $products
 * @property-read int|null $productsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $usersCount
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Discount\Models\Discount onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereAmountType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount wherePeriodActivity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereSaveCoupon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereStartsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Discount\Models\Discount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Discount\Models\Discount withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Discount\Models\Discount withoutTrashed()
 * @mixin \Eloquent
 */
class Discount extends Model
{
    use CastsEnums;
    use SoftDeletes;
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'discounts';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'active',
        'sort',
        'description',
        'type',
        'amount',
        'amount_type',
        'save_coupon',
        'starts_at',
        'expires_at',
        'period_activity'
    ];

    /**
     * Атрибуты, которые нужно связать с enum классом.
     *
     * @var array
     */
    protected $enumCasts = [
        'type' => DiscountTypes::class,
        'amount_type' => DiscountAmountTypes::class,
        'period_activity' => DiscountPeriodActivity::class
    ];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'string',
        'amount_type' => 'string',
        'period_activity' => 'string',
        'save_coupon' => 'boolean',
        'active' => 'boolean',
        'sort' => 'integer'
    ];

    /**
     * Возвращает список значений типов скидок из enum класса для select тега.
     *
     * @return array
     */
    public static function getDiscountTypes(): array
    {
        return DiscountTypes::toSelectArray();
    }

    /**
     * Возвращает список значений типов стоимости скидки из enum класса для select тега.
     *
     * @return array
     */
    public static function getDiscountAmountTypes(): array
    {
        return DiscountAmountTypes::toSelectArray();
    }

    /**
     * Возвращает список значений периодов активности из enum класса для select тега.
     *
     * @return array
     */
    public static function getDiscountPeriodActivity(): array
    {
        return DiscountPeriodActivity::toSelectArray();
    }

    /**
     * Возвращает подготовленную строку для вычитания процента скидки.
     * Пример возвращаемой строки "-5%".
     *
     * @return string
     */
    public function getFormatedDiscountInPercentValue(): string
    {
        return "-{$this->amount}%";
    }

    /**
     * Возвращает подготовленную строку для вычитания стоимости скидки.
     * Пример возвращаемой строки "-500".
     *
     * @return string
     */
    public function getFormatedDiscountInMoneyValue(): string
    {
        return "-{$this->amount}";
    }

    /**
     * Партнёрские предложение, на которые распространяется эта скидка.
     *
     * @return BelongsToMany
     */
    public function partnerOffers(): BelongsToMany
    {
        return $this->belongsToMany(PartnerOffer::class, 'partner_offer_discount')
            ->withPivot('partner_offer_id', 'discount_id')
            ->withTimestamps();
    }

    /**
     * Товары портала, на которые распространяется эта скидка.
     *
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_discount')
            ->withPivot('product_id', 'discount_id')
            ->withTimestamps();
    }

    /**
     * Пользователи, принадлежащие эта скидка.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_discount')
            ->withPivot('user_id', 'discount_id')
            ->withTimestamps();
    }

    /**
     * Получить купоны для этой скидки.
     *
     * @return HasMany
     */
    public function coupons(): HasMany
    {
        return $this->hasMany(Coupon::class);
    }
}
