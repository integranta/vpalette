<?php

namespace Modules\Discount\Observers;

use App\Traits\Cacheable;
use Modules\Discount\Models\Discount;

/**
 * Class DiscountPartnerObserver
 *
 * @package Modules\Discount\Observers
 */
class DiscountPartnerObserver
{
    use Cacheable;
    
    /**
     * Handle the discount "created" event.
     *
     * @param  Discount  $discount
     *
     * @return void
     */
    public function created(Discount $discount): void
    {
        $this->incrementCountItemsInCache($discount, config('discount.partner.cache_key'));
    }
    
    /**
     * Handle the discount "updated" event.
     *
     * @param  Discount  $discount
     *
     * @return void
     */
    public function updated(Discount $discount): void
    {
        //
    }
    
    /**
     * Handle the discount "deleted" event.
     *
     * @param  Discount  $discount
     *
     * @return void
     */
    public function deleted(Discount $discount): void
    {
        $this->decrementCountItemsInCache($discount, config('discount.partner.cache_key'));
    }
    
    /**
     * Handle the discount "restored" event.
     *
     * @param  Discount  $discount
     *
     * @return void
     */
    public function restored(Discount $discount): void
    {
        $this->incrementCountItemsInCache($discount, config('discount.partner.cache_key'));
    }
    
    /**
     * Handle the discount "deleting" event.
     *
     * @param  Discount  $discount
     */
    public function deleting(Discount $discount): void
    {
        $discount->loadMissing(['coupons']);
        
        if ($discount->coupons()->exists()) {
            $discount->coupons()->each(static function ($coupon) {
                $coupon->delete();
            });
        }
    }
    
    /**
     * Handle the discount "force deleted" event.
     *
     * @param  Discount  $discount
     *
     * @return void
     */
    public function forceDeleted(Discount $discount): void
    {
        $this->decrementCountItemsInCache($discount, config('discount.partner.cache_key'));
    }
}
