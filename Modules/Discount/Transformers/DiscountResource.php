<?php

namespace Modules\Discount\Transformers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Coupon\Transformers\CouponResource;
use Modules\Product\Transformers\ProductResource;

/**
 * Class DiscountResource
 *
 * @package Modules\Discount\Transformers
 */
class DiscountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'discountType' => $this->type,
            'periodActivity' => $this->period_activity,
            'isFixed' => $this->is_fixed,
            'saveCoupon' => $this->save_coupon,
            'startsAt' => $this->starts_at,
            'expiresAt' => $this->expires_at,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'deletedAt' => $this->deleted_at,
            'productId' => $this->whenPivotLoaded('product_discount', function () {
                return $this->pivot->product_id;
            }),
            'userId' => $this->whenPivotLoaded('user_discount', function () {
                return $this->pivot->user_id;
            }),
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'users' => UserResource::collection($this->whenLoaded('users')),
            'coupons' => CouponResource::collection($this->whenLoaded('coupons'))
        ];
    }
}
