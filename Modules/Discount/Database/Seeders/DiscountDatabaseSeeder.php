<?php

namespace Modules\Discount\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Coupon\Models\Coupon;
use Modules\Discount\Models\Discount;

/**
 * Class DiscountDatabaseSeeder
 *
 * @package Modules\Discount\Database\Seeders
 */
class DiscountDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        
        factory(Discount::class, 1)
            ->create()
            ->each(function ($discount) {
                if ($discount->save_coupon === filter_var($discount->save_coupon, FILTER_VALIDATE_BOOLEAN)) {
                    $discount->coupons()->save(factory(Coupon::class)->make());
                }
            });
    }
}
