<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePartnerOfferDiscountTable
 */
class CreatePartnerOfferDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_offer_discount', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('partner_offer_id');
            $table->unsignedBigInteger('discount_id');

            $table->foreign('partner_offer_id')
                ->references('id')
                ->on('partner_offers')
                ->onDelete('cascade');

            $table->foreign('discount_id')
                ->references('id')
                ->on('discounts')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['partner_offer_id', 'discount_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_offer_discount');
    }
}
