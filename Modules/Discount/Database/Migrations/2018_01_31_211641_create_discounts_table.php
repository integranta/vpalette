<?php

use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateDiscountsTable
 */
class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('active')->default(true);
            $table->unsignedInteger('sort', false)->default(500);
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('type')->default(DiscountTypes::DISCOUNT);
            $table->unsignedInteger('amount');
            $table->string('amount_type')->default(DiscountAmountTypes::PERCENT);
            $table->string('period_activity')->default(DiscountPeriodActivity::NO_LIMIT);
            $table->boolean('save_coupon')->default(false);
            $table->timestamp('starts_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(['active', 'sort', 'type', 'amount_type', 'period_activity']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
