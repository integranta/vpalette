<?php

use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Modules\Discount\Models\Discount;

/** @var Factory $factory */
$factory->define(Discount::class, function (Faker $faker) {
    $periodActivity = $faker->randomElement(DiscountPeriodActivity::getValues());
    
    $startsAt = Carbon::now('Europe/Samara');
    
    $expiresAt = Carbon::now('Europe/Samara')->addMonths(1);
    
    return [
        'active' => true,
        'sort' => $faker->numberBetween(1, 500),
        'name' => $faker->sentence,
        'description' => $faker->realText(150),
        'type' => $faker->randomElement(DiscountTypes::getValues()),
        'amount' => $faker->randomDigitNotNull,
        'amount_type' => $faker->randomElement(DiscountAmountTypes::getValues()),
        'save_coupon' => random_int(0, 1),
        'period_activity' => $periodActivity,
        'starts_at' => $periodActivity === DiscountPeriodActivity::INTERVAL ? $startsAt : null,
        'expires_at' => $periodActivity === DiscountPeriodActivity::INTERVAL ? $expiresAt : null,
    ];
});
