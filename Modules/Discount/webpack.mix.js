const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/Assets/js/app.js', 'js/discount.js')
    .sass( __dirname + '/Resources/Assets/sass/app.scss', 'css/discount.css', {
        implementation: require('node-sass')
    });

if (mix.inProduction()) {
    mix.version();
}
