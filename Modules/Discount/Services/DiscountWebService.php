<?php


namespace Modules\Discount\Services;


use App\DataTables\DiscountsDataTable;
use App\Services\BaseService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\Factory;
use Modules\Discount\Contracts\DiscountContract;
use Modules\Discount\Models\Discount;
use Modules\Product\Contracts\ProductContract;
use Modules\User\Contracts\UserContract;

/**
 * Class DiscountWebService
 *
 * @package Modules\Discount\Services
 */
class DiscountWebService extends BaseService
{
    /** @var DiscountContract */
    private $discountRepository;
    
    /** @var UserContract */
    private $userRepository;
    
    /** @var ProductContract */
    private $productRepository;
    
    /**
     * DiscountController constructor.
     *
     * @param  DiscountContract  $discountRepo
     * @param  UserContract      $userRepo
     * @param  ProductContract   $productRepo
     *
     */
    public function __construct(
        DiscountContract $discountRepo,
        UserContract $userRepo,
        ProductContract $productRepo
    ) {
        $this->discountRepository = $discountRepo;
        $this->userRepository = $userRepo;
        $this->productRepository = $productRepo;
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\DiscountsDataTable  $dataTable
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function index(DiscountsDataTable $dataTable)
    {
        $this->setPageTitle(
            __('discount::messages.title'),
            __('discount::messages.index_subtitle'),
            __('discount::messages.index_leadtext')
        );
        
        return $dataTable->render('discount::discount.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        $users = $this->userRepository->listUsers();
        $products = $this->productRepository->listProducts();
        $discountTypes = Discount::getDiscountTypes();
        $discountAmountTypes = Discount::getDiscountAmountTypes();
        
        $this->setPageTitle(
            __('discount::messages.title'),
            __('discount::messages.create_subtitle'),
            __('discount::messages.create_leadtext')
        );
        return view('discount::discount.create', compact(
                'users', 'products', 'discountAmountTypes', 'discountTypes')
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $discount = $this->discountRepository->createDiscount($request->all());
        
        if (!$discount) {
            return $this->responseRedirectBack(
                __('discount::messages.error_create'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.discount.index',
            __('discount::messages.success_create'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        $discount = $this->discountRepository->findDiscountById($id);
        
        if ($discount === null) {
            $this->responseRedirect(
                'admin.discount.index',
                __('discount::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        $this->setPageTitle(
            __('discount::messages.title'),
            __('discount::messages.show_subtitle', ['name' => $discount->name]),
            __('discount::messages.show_leadtext', ['name' => $discount->name])
        );
        return view('discount::discount.show', compact('discount'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        $discount = $this->discountRepository->findDiscountById($id);
        
        if ($discount === null) {
            $this->responseRedirect(
                'admin.discount.index',
                __('discount::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        $this->setPageTitle(
            __('discount::messages.title'),
            __('discount::messages.edit_subtitle', ['name' => $discount->name]),
            __('discount::messages.edit_leadtext', ['name' => $discount->name])
        );
        return view('discount::discount.edit', compact('discount'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $discount = $this->discountRepository->findDiscountById($id);
        
        if ($discount === null) {
            $this->responseRedirect(
                'admin.discount.index',
                __('discount::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        $discount = $this->discountRepository->updateDiscount($request->all(), $id);
        
        if (!$discount) {
            return $this->responseRedirectBack(
                __('discount::messages.error_update'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.discount.index',
            __('discount::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $discount = $this->discountRepository->findDiscountById($id);
        
        if ($discount === null) {
            $this->responseRedirect(
                'admin.discount.index',
                __('discount::messages.not_found', ['id' => $id]),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        $discount = $this->discountRepository->deleteDiscount($id);
        
        if (!$discount) {
            return $this->responseRedirectBack(
                __('discount::messages.error_delete'),
                self::FAIL_RESPONSE,
                true,
                true
            );
        }
        
        return $this->responseRedirect(
            'admin.discount.index',
            __('discount::messages.success_delete'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
}
