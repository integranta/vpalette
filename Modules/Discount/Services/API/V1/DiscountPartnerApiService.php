<?php


namespace Modules\Discount\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Discount\Contracts\DiscountPartnerContract;

/**
 * Class DiscountPartnerApiService
 *
 * @package Modules\Discount\Services\API\V1
 */
class DiscountPartnerApiService extends BaseService
{
    /** @var DiscountPartnerContract */
    private $discountPartnerRepository;
    
    /**
     * DiscountPartnerApiService constructor.
     *
     * @param  DiscountPartnerContract  $discountPartnerRepository
     */
    public function __construct(DiscountPartnerContract $discountPartnerRepository)
    {
        $this->discountPartnerRepository = $discountPartnerRepository;
    }
    
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $discounts = $this->discountPartnerRepository->listPartnerDiscounts();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_retrieve'),
            $discounts
        );
    }
    
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $discount = $this->discountPartnerRepository->createPartnerDiscount($request->all());
        
        if (!$discount) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('discount::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('discount::messages.success_create'),
            $discount
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $discount = $this->discountPartnerRepository->findPartnerDiscountById($id);
        
        if ($discount === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('discount::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_found', ['id' => $id]),
            $discount
        );
    }
    
    /**
     * @param  Request  $request
     * @param  int      $id
     *
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $discount = $this->discountPartnerRepository->findPartnerDiscountById($id);
        
        if ($discount === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('discount::messages.not_found', ['id' => $id])
            );
        }
        
        $discount = $this->discountPartnerRepository->updatePartnerDiscount($request->all(), $id);
        
        if (!$discount) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('discount::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_update'),
            $discount
        );
    }
    
    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $discount = $this->discountPartnerRepository->findPartnerDiscountById($id);
        
        if ($discount === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('discount::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_delete'),
            $discount
        );
    }
}
