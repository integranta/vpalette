<?php


namespace Modules\Discount\Services\API\V1;

use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Discount\Contracts\DiscountContract;
use Modules\Discount\Models\Discount;
use Modules\Discount\Transformers\DiscountResource;

/**
 * Class DiscountApiService
 *
 * @package Modules\Discount\Services\API\V1
 */
class DiscountApiService extends BaseService
{
    /** @var DiscountContract */
    private $discountRepository;
    
    /**
     * DiscountController constructor.
     *
     * @param  DiscountContract  $discountRepo
     *
     * @return void
     */
    public function __construct(DiscountContract $discountRepo)
    {
        $this->discountRepository = $discountRepo;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $discounts = DiscountResource::collection($this->discountRepository->listDiscounts());
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_retrieve'),
            $discounts
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $discount = $this->discountRepository->createDiscount($request->all());
        
        if (!$discount) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('discount::messages.error_create')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('discount::messages.success_create'),
            $discount
        );
    }
    
    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $discount = DiscountResource::make($this->discountRepository->findDiscountById($id));
        
        if ($discount === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('discount::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_found', ['id' => $id]),
            $discount
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $discount = $this->discountRepository->findDiscountById($id);
        
        if ($discount === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('discount::messages.not_found', ['id' => $id])
            );
        }
        
        $discount = $this->discountRepository->updateDiscount($request->all(), $id);
        
        if (!$discount) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('discount::messages.error_update')
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_update'),
            $discount
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $discount = $this->discountRepository->findDiscountById($id);
        
        if ($discount === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('discount::messages.not_found', ['id' => $id])
            );
        }
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_delete'),
            $discount
        );
    }
    
    /**
     * Retrieve values from enum DiscountTypes
     *
     * @return JsonResponse
     * @see DiscountTypes
     *
     */
    public function getDiscountTypes(): JsonResponse
    {
        $discountTypes = Discount::getDiscountTypes();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_retrieve_types'),
            $discountTypes
        );
    }
    
    /**
     * Retrieve values from enum DiscountAmountTypes
     *
     * @return JsonResponse
     * @see DiscountAmountTypes
     *
     */
    public function getDiscountAmountTypes(): JsonResponse
    {
        $discountAmountTypes = Discount::getDiscountAmountTypes();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_retrieve_amount-types'),
            $discountAmountTypes
        );
    }
    
    /**
     * Retrieve values from enum DiscountPeriodActivity
     *
     * @return JsonResponse
     * @see DiscountPeriodActivity
     *
     */
    public function getDiscountPeriodActivity(): JsonResponse
    {
        $discountPeriodActivity = Discount::getDiscountPeriodActivity();
        
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('discount::messages.success_retrieve_period_activity'),
            $discountPeriodActivity
        );
    }
}
