<?php

return [
    'name' => 'Discount',
    /*
    |--------------------------------------------------------------------------
    | Cache settings
    |--------------------------------------------------------------------------
    |
    | These parameters are responsible for managing the cache parameters used when
    | getting a list of all discounts as a simple list.
    | The time specified in the cache_ttl parameter is expressed in seconds.
    |
    */
    'partner' => [
        'cache_key' => env('DISCOUNT_PARTNER_CACHE_KEY', 'index_partner_discounts'),
        'cache_ttl' => env('DISCOUNT_PARTNER_CACHE_TTL', 360000),
    ],
    'portal' => [
        'cache_key' => env('DISCOUNT_CACHE_KEY', 'index_portal_discounts'),
        'cache_ttl' => env('DISCOUNT_CACHE_TTL', 360000),
    ]
    
];
