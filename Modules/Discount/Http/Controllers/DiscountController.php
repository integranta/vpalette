<?php

namespace Modules\Discount\Http\Controllers;

use App\DataTables\DiscountsDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Discount\Services\DiscountWebService;


/**
 * Class DiscountController
 *
 * @package Modules\Discount\Http\Controllers
 */
class DiscountController extends Controller
{
    /** @var DiscountWebService */
    private $discountWebService;

    /**
     * DiscountController constructor.
     *
     * @param  DiscountWebService  $service
     */
    public function __construct(DiscountWebService $service)
    {
        $this->discountWebService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \App\DataTables\DiscountsDataTable  $dataTable
	 *
	 * @return Factory|View|\Illuminate\Http\JsonResponse
	 */
    public function index(DiscountsDataTable $dataTable)
    {
        return $this->discountWebService->index($dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create(): View
    {
        return $this->discountWebService->create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        return $this->discountWebService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function show(int $id): View
    {
        return $this->discountWebService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Factory|View
     */
    public function edit(int $id): View
    {
        return $this->discountWebService->edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        return $this->discountWebService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        return $this->discountWebService->destroy($id);
    }
}
