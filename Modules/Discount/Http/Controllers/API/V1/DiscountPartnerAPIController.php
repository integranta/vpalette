<?php

namespace Modules\Discount\Http\Controllers\API\V1;

use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Discount\Services\API\V1\DiscountPartnerApiService;

/**
 * Class DiscountPartnerAPIController
 *
 * @package Modules\Discount\Http\Controllers\API\V1
 */
class DiscountPartnerAPIController extends Controller
{
    /** @var DiscountPartnerApiService */
    private $discountPartnerApiService;

    /**
     * DiscountController constructor.
     *
     * @param  DiscountPartnerApiService  $service
     *
     * @return void
     */
    public function __construct(DiscountPartnerApiService $service)
    {
        $this->discountPartnerApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->discountPartnerApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        return $this->discountPartnerApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->discountPartnerApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        return $this->discountPartnerApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->discountPartnerApiService->destroy($id);
    }
}
