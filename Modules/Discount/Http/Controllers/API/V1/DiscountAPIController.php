<?php

namespace Modules\Discount\Http\Controllers\API\V1;

use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Modules\Discount\Services\API\V1\DiscountApiService;

/**
 * Class DiscountAPIController
 *
 * @package Modules\Discount\Http\Controllers\API\V1
 */
class DiscountAPIController extends Controller
{
    /** @var DiscountApiService */
    private $DiscountApiService;

    /**
     * DiscountController constructor.
     *
     * @param  DiscountApiService  $service
     *
     * @return void
     */
    public function __construct(DiscountApiService $service)
    {
        $this->DiscountApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->DiscountApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        return $this->DiscountApiService->store($request);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->DiscountApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     *
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        return $this->DiscountApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->DiscountApiService->destroy($id);
    }

    /**
     * Retrieve values from enum DiscountTypes
     *
     * @return JsonResponse
     * @see DiscountTypes
     *
     */
    public function getDiscountTypes(): JsonResponse
    {
        return $this->DiscountApiService->getDiscountTypes();
    }

    /**
     * Retrieve values from enum DiscountAmountTypes
     *
     * @return JsonResponse
     * @see DiscountAmountTypes
     *
     */
    public function getDiscountAmountTypes(): JsonResponse
    {
        return $this->DiscountApiService->getDiscountAmountTypes();
    }

    /**
     * Retrieve values from enum DiscountPeriodActivity
     *
     * @return JsonResponse
     * @see DiscountPeriodActivity
     *
     */
    public function getDiscountPeriodActivity(): JsonResponse
    {
        return $this->DiscountApiService->getDiscountPeriodActivity();
    }
}
