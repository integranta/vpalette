<?php

namespace Modules\Core\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Core\Models\Setting;

/**
 * Class CorePolicy
 *
 * @package Modules\Core\Policies
 */
class CorePolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine whether the user can view the setting.
     *
     * @param  User|null  $user
     * @param  Setting    $setting
     *
     * @return mixed
     */
    public function view(?User $user, Setting $setting)
    {
        if ($setting->created_at) {
            return true;
        }
        
        // visitors cannot view unpublished items
        if ($user === null) {
            return false;
        }
        
        // admin overrides published status
        if ($user->can('view unpublished settings')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can create settings.
     *
     * @param  User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can('create settings')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can update the setting.
     *
     * @param  User     $user
     * @param  Setting  $setting
     *
     * @return mixed
     */
    public function update(User $user, Setting $setting)
    {
        if ($user->can('edit all settings')) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Determine whether the user can delete the setting.
     *
     * @param  User     $user
     * @param  Setting  $setting
     *
     * @return mixed
     */
    public function delete(User $user, Setting $setting)
    {
        if ($user->can('delete any setting')) {
            return true;
        }
        
        return false;
    }
}
