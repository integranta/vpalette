<?php

namespace Modules\Core\Models;

use Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Throwable;

/**
 * Class Setting
 *
 * @package Modules\Core\Models
 * @property int $id
 * @property string $key
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Setting whereValue($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{
	use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds
	
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = ['key', 'value'];

    /**
     * Получить значение параметра по ключу.
     *
     * @param  array|string|string[]  $key
     *
     * @return Collection|mixed
     */
    public function get($key): Collection
    {
        $setting = new self();
        $entry = $setting->where('key', $key)->first();

        return $entry->value;
    }

    /**
     * Установить значение параметра по ключу.
     *
     * @param               $key
     * @param  null|string  $value
     *
     * @return bool
     * @throws Throwable
     */
    public static function set(string $key, string $value = null): bool
    {
        $setting = new self();
        $entry = $setting->where('key', $key)->firstOrFail();
        $entry->value = $value;
        $entry->saveOrFail();
        Config::set('key', $value);
        if (Config::get($key) == $value) {
            return true;
        }
        return false;
    }
}
