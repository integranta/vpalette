<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            <h4>Перейти в</h4>
        </div>
        <div class="card-body">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link active" href="#general" data-toggle="tab">Основные</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#site-logo" data-toggle="tab">Логотип сайта</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#footer-seo" data-toggle="tab">Подвал и SEO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#social-links" data-toggle="tab">Ссылки на соц.сети</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#analytics" data-toggle="tab">Аналитика</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#payments" data-toggle="tab">Платежи</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#deliveries" data-toggle="tab">Доставка</a>
                </li>
            </ul>
        </div>
    </div>
</div>
