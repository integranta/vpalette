@extends('layouts.admin-master')

@section('title')
    {{ $pageTitle }}
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a
                    href="{{ route('admin.settings.index') }}"
                    class="btn btn-icon"
                >
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>{{ $pageTitle }}</h1>
        </div>

        <div class="section-body">
            <h2 class="section-title">{{ $subTitle }}</h2>
            <p class="section-lead">
                {{ $leadText }}
            </p>

            @include('admin.partials.flash')
            <div class="row">
                @include('core::shared.sidebar')
                <div class="col-md-8">
                    <div class="tab-content">
                        <div class="tab-pane active" id="general">
                            @include('core::includes.general')
                        </div>
                        <div class="tab-pane fade" id="site-logo">
                            @include('core::includes.logo')
                        </div>
                        <div class="tab-pane fade" id="footer-seo">
                            @include('core::includes.footer_seo')
                        </div>
                        <div class="tab-pane fade" id="social-links">
                            @include('core::includes.social_links')
                        </div>
                        <div class="tab-pane fade" id="analytics">
                            @include('core::includes.analytics')
                        </div>
                        <div class="tab-pane fade" id="payments">
                            @include('core::includes.payments')
                        </div>
                        <div class="tab-pane fade" id="deliveries">
                            @include('core::includes.deliveries')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

