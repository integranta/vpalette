@extends('layouts.admin-master')

@section('title')
    Seo настройки
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('css/core.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.settings.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>SEO настройки</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ route('admin.settings.index') }}">Настройки</a></div>
                <div class="breadcrumb-item">SEO</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Все о SEO настройках</h2>
            <p class="section-lead">
                Вы можете настроить все SEO настройки здесь
            </p>

            <div id="output-status"></div>
            <div class="row">
                @include('core::shared.sidebar')
                <div class="col-md-8">
                    {!! Form::model($seoSettings->first(), ['route' => ['admin.seo.update', $seoSettings->first()->id], 'method' => 'put']) !!}
                    @include('core::seo_settings.fields', ['params' => $seoSettings])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@section ('scripts')
    {{--    <script src="{{ asset('assets/js/page/features-setting-detail.js') }}"></script>--}}
    <script src="{{ asset('js/core.js') }}"></script>
@endsection
