<div class="card" id="settings-card">
    <div class="card-header">
        <h4>SEO настройки</h4>
    </div>
    <div class="card-body">
        <p class="text-muted">Настройки поисковой оптимизации, такие как метатеги и социальные сети.</p>
        <div class="card-header">
            <h4>Основные</h4>
        </div>

    @foreach($params as $param)
        <!-- Title Field -->
            <div class="form-group row align-items-center">
                {!! Form::label('title', ' Meta title:', ['class' => 'form-control-label col-sm-3 text-md-right']) !!}
                <div class="col-sm-6 col-md-9">
                    {!! Form::text('title', $param->title , ['class' => 'form-control']) !!}
                </div>
            </div>

            <!-- Description Field -->
            <div class="form-group row align-items-center">
                {!! Form::label('description', 'Meta Description:', ['class' => 'form-control-label col-sm-3 text-md-right']) !!}
                <div class="col-sm-6 col-md-9">
                    {!! Form::textarea('description', $param->description, ['class' => 'form-control']) !!}
                </div>
            </div>

            <!-- Keywords Field -->
            <div class="form-group row align-items-center">
                {!! Form::label('keywords', 'Keywords:', ['class' => 'form-control-label col-sm-3 text-md-right']) !!}
                <div class="col-sm-6 col-md-9">
                    {!! Form::text('keywords', $param->keywords, ['class' => 'form-control']) !!}
                </div>
            </div>
        @endforeach


        <div class="form-group row align-items-center">
            <label
                for="meta_canonical"
                class="form-control-label col-sm-3 text-md-right"
            >
                Canonical URL:
            </label>
            <div class="col-sm-6 col-md-9">
                <input
                    type="text"
                    name="meta_canonical"
                    class="form-control"
                    id="meta_canonical"
                >
            </div>
        </div>
        <div class="card-header">
            <h4>OpenGraph</h4>
        </div>
        <div class="form-group row align-items-center">
            <label
                for="opengraph_title"
                class="form-control-label col-sm-3 text-md-right"
            >
                OpenGraph Title:
            </label>
            <div class="col-sm-6 col-md-9">
                <input
                    type="text"
                    name="opengraph_title"
                    class="form-control"
                    id="opengraph_title"
                >
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label
                for="opengraph_description"
                class="form-control-label col-sm-3 text-md-right"
            >
                OpenGraph Description:
            </label>
            <div class="col-sm-6 col-md-9">
                                        <textarea
                                            class="form-control"
                                            name="opengraph_description"
                                            id="opengraph_description">
                                        </textarea>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label
                for="opengraph_url"
                class="form-control-label col-sm-3 text-md-right"
            >
                OpenGraph URL:
            </label>
            <div class="col-sm-6 col-md-9">
                <input
                    type="text"
                    name="opengraph_url"
                    class="form-control"
                    id="opengraph_url"
                >
            </div>
        </div>
        <div class="card-header">
            <h4>Реклама</h4>
        </div>

        <div class="form-group row">
            <label
                class="form-control-label col-sm-3 mt-3 text-md-right"
                id="ya_direct_code"
            >
                Яндекс.Директ
            </label>
            <div class="col-sm-6 col-md-9">
                                                <textarea
                                                    class="form-control codeeditor"
                                                    name="ya_direct_code"
                                                    id="ya_direct_code"
                                                ></textarea>
            </div>
        </div>
        <div class="card-header">
            <h4>Счётчики:</h4>
        </div>
        <div class="form-group row">
            <label
                class="form-control-label col-sm-3 mt-3 text-md-right"
                for="google_analytics_code"
            >
                Google Analytics
            </label>
            <div class="col-sm-6 col-md-9">
                                        <textarea
                                            class="form-control codeeditor"
                                            name="google_analytics_code"
                                            id="google_analytics_code"
                                        >
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144934640-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
      dataLayer.push(arguments);
  }

  gtag('js', new Date());

  gtag('config', 'UA-144934640-1');
</script>
                                        </textarea>
            </div>
        </div>
        <div class="form-group row">
            <label
                class="form-control-label col-sm-3 mt-3 text-md-right"
                for="ya_metric_code"
            >
                Яндекс.Метрика
            </label>
            <div class="col-sm-6 col-md-9">
                                        <textarea
                                            class="form-control codeeditor"
                                            name="ya_metrica_code"
                                            id="ya_metrica_code"
                                        ><!-- Yandex.Metrika counter -->
<script type="text/javascript">
   (function (m, e, t, r, i, k, a) {
       m[i] = m[i] || function () {
           (m[i].a = m[i].a || []).push(arguments)
       };
       m[i].l = 1 * new Date();
       k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
   })
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(54672388, "init", {
       clickmap: true,
       trackLinks: true,
       accurateTrackBounce: true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54672388" style="position:absolute; left:-9999px;"
                    alt=""/></div></noscript>
                                            <!-- /Yandex.Metrika counter --></textarea>
            </div>
        </div>
    </div>

    <!-- Submit Field -->
    <div class="card-footer bg-whitesmoke text-md-right">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary', 'id' => 'save-btn']) !!}
        <a href="{!! route('admin.seo.index') !!}" class="btn btn-default">Сбросить</a>
    </div>
</div>
