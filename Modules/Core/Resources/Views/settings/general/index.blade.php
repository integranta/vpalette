@extends('layouts.admin-master')

@section('title')
    Основные настройки
@endsection

@section('plugin_css')
    <link rel="stylesheet" href="{{ asset('css/core.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a
                    href="{{ route('admin.settings.index') }}"
                    class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Основные настройки</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active">
                    <a href="{{ route('admin.settings.index') }}">
                        Настройки
                    </a>
                </div>
                <div class="breadcrumb-item">
                    Основные
                </div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Все о общих настройках</h2>
            <p class="section-lead">
                Вы можете настроить все общие настройки здесь
            </p>

            <div id="output-status"></div>
            <div class="row">
                @include('core::shared.sidebar')
                <div class="col-md-8">
                    <form id="setting-form">
                        <div class="card" id="settings-card">
                            <div class="card-header">
                                <h4>Основные найстройки</h4>
                            </div>
                            <div class="card-body">
                                <p class="text-muted">
                                    Общие настройки, такие как название сайта, описание сайта, адрес и т.д.
                                </p>

                                <div class="form-group row align-items-center">
                                    <label
                                        class="form-control-label col-sm-3 text-md-right">
                                        Логотип сайта
                                    </label>
                                    <div class="col-sm-6 col-md-9">
                                        <div class="custom-file">
                                            <input
                                                type="file"
                                                name="site_logo"
                                                class="custom-file-input"
                                                id="site-logo"
                                            >
                                            <label class="custom-file-label">
                                                Выберите файл
                                            </label>
                                        </div>
                                        <div
                                            class="form-text text-muted">
                                            Изображение должно иметь максимальный размер 1 МБ
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row align-items-center">
                                    <label
                                        class="form-control-label col-sm-3 text-md-right">
                                        Favicon
                                    </label>
                                    <div class="col-sm-6 col-md-9">
                                        <div class="custom-file">
                                            <input
                                                type="file"
                                                name="site_favicon"
                                                class="custom-file-input"
                                                id="site-favicon"
                                            >
                                            <label class="custom-file-label">Выбрать файл</label>
                                        </div>
                                        <div class="form-text text-muted">
                                            Изображение должно иметь максимальный размер 1 МБ
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-whitesmoke text-md-right">
                                <button
                                    class="btn btn-primary"
                                    id="save-btn"
                                >
                                    Сохранить
                                </button>
                                <button
                                    class="btn btn-danger"
                                    type="button"
                                >
                                    Сбросить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@push ('scripts')

    <script src="{{ asset('assets/js/page/features-setting-detail.js') }}"></script>
    <script src="{{ asset('js/core.js') }}"></script>
@endsection
