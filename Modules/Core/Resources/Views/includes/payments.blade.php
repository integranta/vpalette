<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Настройки платежей</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group pt-2">
                <label
                    class="control-label"
                    for="yandex_kassa_payment_method"
                >
                    Яндекс.Касса
                </label>
                <select
                    name="yandex_kassa_payment_method"
                    id="yandex_kassa_payment_method"
                    class="form-control"
                >
                    <option value="1" {{ (config('settings.yandex_kassa_payment_method')) == 1 ? 'selected' : '' }}>
                        Включена
                    </option>
                    <option value="0" {{ (config('settings.yandex_kassa_payment_method')) == 0 ? 'selected' : '' }}>
                        Выключена
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label
                    class="control-label"
                    for="yandex_kassa_account_id"
                >
                    Идентификатор магазина
                </label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите Идентификатор магазина от Яндекс.Касса"
                    id="yandex_kassa_account_id"
                    name="yandex_kassa_account_id"
                    value="{{ config('settings.yandex_kassa_account_id') }}"
                />
            </div>
            <div class="form-group">
                <label
                    class="control-label"
                    for="yandex_kassa_secret_id"
                >
                    Секретный ключ
                </label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите Секретный ключ от Яндекс.Касса"
                    id="yandex_kassa_secret_id"
                    name="yandex_kassa_secret_id"
                    value="{{ config('settings.yandex_kassa_secret_id') }}"
                />
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i>
                        Обновить настройки
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
