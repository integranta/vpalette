<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Настройки сервисов доставок</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group pt-2">
                <label
                    class="control-label"
                    for="post_russia_delivery_method"
                >
                    Почта России
                </label>
                <select
                    name="post_russia_delivery_method"
                    id="post_russia_delivery_method"
                    class="form-control"
                >
                    <option value="1" {{ (config('settings.post_russia_delivery_method')) == 1 ? 'selected' : '' }}>
                        Включена
                    </option>
                    <option value="0" {{ (config('settings.post_russia_delivery_method')) == 0 ? 'selected' : '' }}>
                        Выключена
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label
                    class="control-label"
                    for="post_russia_delivery_account_id"
                >
                    Токен авторизации
                </label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите токен авторизации от Почта России"
                    id="post_russian_account_id"
                    name="post_russian_account_id"
                    value="{{ config('settings.post_russian_account_id') }}"
                />
            </div>
            <div class="form-group">
                <label
                    class="control-label"
                    for="post_russian_secret_id"
                >
                    Секретный ключ
                </label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите Секретный ключ от Почта России"
                    id="post_russian_secret_id"
                    name="post_russian_secret_id"
                    value="{{ config('settings.post_russian_secret_id') }}"
                />
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i>
                        Обновить настройки
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
