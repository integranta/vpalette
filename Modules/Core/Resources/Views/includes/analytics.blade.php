<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Аналитика и реклама</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="google_analytics">Google Analytics</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Введите код от Google Analytics"
                    id="google_analytics"
                    name="google_analytics"
                >{!! Config::get('settings.google_analytics') !!}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="facebook_pixels">Facebook Pixel</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Введите код от Facebook Pixel"
                    id="facebook_pixels"
                    name="facebook_pixels"
                >{{ Config::get('settings.facebook_pixels') }}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="yandex_metrica">Яндекс.Метрика</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Введите код от Яндекс.Метрика"
                    id="yandex_metrica"
                    name="yandex_metrica"
                >{{ Config::get('settings.yandex_metrica') }}</textarea>
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i>
                        Обновить настройки
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
