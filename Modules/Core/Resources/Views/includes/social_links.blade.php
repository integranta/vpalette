<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Ссылки на соц.сети</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="social_facebook">Facebook</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите ссылку на facebook аккаунт"
                    id="social_facebook"
                    name="social_facebook"
                    value="{{ config('settings.social_facebook') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="social_twitter">Twitter</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите ссылку на twitter аккаунт"
                    id="social_twitter"
                    name="social_twitter"
                    value="{{ config('settings.social_twitter') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="social_instagram">Instagram</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите ссылку на instagram аккаунт"
                    id="social_instagram"
                    name="social_instagram"
                    value="{{ config('settings.social_instagram') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="social_vkontakte">ВКонтакте</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите ссылку на ВКонтакте аккаунт"
                    id="social_vkontakte"
                    name="social_vkontakte"
                    value="{{ config('settings.social_vkontakte') }}"
                />
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i>
                        Обновить настройки
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
