<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Основные настройки</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="site_name">Название сайта</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите название сайта"
                    id="site_name"
                    name="site_name"
                    value="{{ config('settings.site_name') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="site_title">Заголовок сайта</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите заголовок сайта"
                    id="site_title"
                    name="site_title"
                    value="{{ config('settings.site_title') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="site_phone">Номер телефона</label>
                <input
                    class="form-control"
                    type="tel"
                    placeholder="Введите номер телефона"
                    id="site_phone"
                    name="site_phone"
                    value="{{ config('settings.site_phone') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="site_address">Фактический адрес</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите заголовок сайта"
                    id="site_address"
                    name="site_address"
                    value="{{ config('settings.site_address') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="site_work_time">Режим работы</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите режим работы сайта"
                    id="site_work_time"
                    name="site_work_time"
                    value="{{ config('settings.site_work_time') }}"
                />
            </div>
            <h4 class="tile-title">Реквизиты</h4>
            <div class="form-group">
                <label class="control-label" for="site_requisites">Название организации</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите название организации сайта"
                    id="site_requisites"
                    name="site_requisites"
                    value="{{ config('settings.site_requisites') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="site_requisites_ogrnip">ОГРНИП</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите название ОГРНИП сайта"
                    id="site_requisites_ogrnip"
                    name="site_requisites_ogrnip"
                    value="{{ config('settings.site_requisites_ogrnip') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="site_requisites_inn">ИНН</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите название ИНН сайта"
                    id="site_requisites_inn"
                    name="site_requisites_inn"
                    value="{{ config('settings.site_requisites_inn') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="default_email_address">Email магазина по умолчанию</label>
                <input
                    class="form-control"
                    type="email"
                    placeholder="Введите адрес электронной почты магазина по умолчанию"
                    id="default_email_address"
                    name="default_email_address"
                    value="{{ config('settings.default_email_address') }}"
                />
            </div>
            <h4 class="tile-title">Магазин</h4>
            <div class="form-group">
                <label class="control-label" for="currency_code">Код валюты</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите код валюты магазина"
                    id="currency_code"
                    name="currency_code"
                    value="{{ config('settings.currency_code') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="currency_symbol">Символ валюты</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Введите символ валюты магазина"
                    id="currency_symbol"
                    name="currency_symbol"
                    value="{{ config('settings.currency_symbol') }}"
                />
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-fw fa-lg fa-check-circle"></i>Обновить настройки
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
