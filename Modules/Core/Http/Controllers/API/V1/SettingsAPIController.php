<?php

namespace Modules\Core\Http\Controllers\API\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Core\Services\API\V1\SettingsApiService;

/**
 * Class SettingsAPIController
 *
 * @package Modules\Core\Http\Controllers\API\V1
 */
class SettingsAPIController extends Controller
{
    private $settingApiService;

    /**
     * SettingsAPIController constructor.
     *
     * @param  SettingsApiService  $service
     */
    public function __construct(SettingsApiService $service)
    {
        $this->settingApiService = $service;
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->settingApiService->index();
    }
}
