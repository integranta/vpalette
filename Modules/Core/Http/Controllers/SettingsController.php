<?php

namespace Modules\Core\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Traits\UploadAble;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\View\View;
use Lang;
use Modules\Core\Services\SettingsWebService;
use Setting;

/**
 * Class SettingsController
 *
 * @package Modules\Core\Http\Controllers
 */
class SettingsController extends Controller
{

    /**
     * @var SettingsWebService
     */
    private $service;

    /**
     * SettingsController constructor.
     *
     * @param  SettingsWebService  $service
     */
    public function __construct(SettingsWebService $service)
    {
        $this->middleware('role:Owner|Admin');
        $this->middleware(['cache.headers:public;max_age=2628000;etag'])->only(['index', 'show']);
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        return $this->service->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        return $this->service->update($request);
    }
}
