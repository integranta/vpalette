<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Setting;

/**
 * Class SettingsTableSeeder
 *
 * @package Modules\Core\Database\Seeders
 */
class SettingsTableSeeder extends Seeder
{
    /**
     * @var array
     */
    protected $settings = [
        [
            'key' => 'default_email_address',
            'value' => 'vpalitre@gmail.com',
        ],
        [
            'key' => 'currency_code',
            'value' => 'RUB',
        ],
        [
            'key' => 'currency_symbol',
            'value' => '₽',
        ],
        [
            'key' => 'site_name',
            'value' => 'ВПалитре',
        ],
        [
            'key' => 'site_title',
            'value' => 'ВПалитре - Интернет-портал',
        ],
        [
            'key' => 'site_logo',
            'value' => '',
        ],
        [
            'key' => 'site_favicon',
            'value' => '',
        ],
        [
            'key' => 'site_phone',
            'value' => '8 800 900-00-00'
        ],
        [
            'key' => 'site_address',
            'value' => '115172, город Астрахань, улица Свердлова, дом 4',
        ],
        [
            'key' => 'site_work_time',
            'value' => 'пн-вс: с 10:00 до 24:00'
        ],
        [
            'key' => 'site_requisites',
            'value' => 'Индивидуальный предприниматель Иванов Иван Иванович'
        ],
        [
            'key' => 'site_requisites_ogrnip',
            'value' => '317774600300812'
        ],
        [
            'key' => 'site_requisites_inn',
            'value' => '772706085275'
        ],
        [
            'key' => 'footer_copyright_text',
            'value' => '',
        ],
        [
            'key' => 'social_facebook',
            'value' => '',
        ],
        [
            'key' => 'social_twitter',
            'value' => '',
        ],
        [
            'key' => 'social_instagram',
            'value' => '',
        ],
        [
            'key' => 'social_vkontakte',
            'value' => '',
        ],
        [
            'key' => 'google_analytics',
            'value' => '',
        ],
        [
            'key' => 'yandex_metrica',
            'value' => '',
        ],
        [
            'key' => 'facebook_pixels',
            'value' => '',
        ]
    ];
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->settings as $index => $setting) {
            $result = Setting::create($setting);
            if (!$result) {
                $this->command->info("Insert failed at record $index.");
                return;
            }
        }
        $this->command->info('Inserted '.count($this->settings).' records');
    }
}
