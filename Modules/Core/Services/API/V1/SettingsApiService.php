<?php


namespace Modules\Core\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Modules\Core\Models\Setting;

/**
 * Class SettingsApiService
 *
 * @package Modules\Core\Services\API\V1
 */
class SettingsApiService extends BaseService
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('core::messages.success_retrieve'),
            Setting::all()
        );
    }
}
