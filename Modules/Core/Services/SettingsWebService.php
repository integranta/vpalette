<?php


namespace Modules\Core\Services;


use App\Services\BaseService;
use App\Traits\UploadAble;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\View\View;
use Setting;

class SettingsWebService extends BaseService
{
    use UploadAble;

    /**
     * @return Application|Factory|View
     */
    public function index(): View
    {
        $this->setPageTitle(
            __('core::messages.title'),
            __('core::messages.index_subtitle'),
            __('core::messages.index_leadtext')
        );

        return view('core::settings.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        if ($request->has('site_logo') && $request->file('site_logo') instanceof UploadedFile) {

            if (config('settings.site_logo') !== null) {
                $this->deleteOne(config('settings.site_logo'));
            }
            $logo = $this->uploadOne($request->file('site_logo'), 'img');
            Setting::set('site_logo', $logo);

        } elseif ($request->has('site_favicon') && $request->file('site_favicon') instanceof UploadedFile) {

            if (config('settings.site_favicon') !== null) {
                $this->deleteOne(config('settings.site_favicon'));
            }
            $favicon = $this->uploadOne($request->file('site_favicon'), 'img');
            Setting::set('site_favicon', $favicon);

        } else {

            $keys = $request->except('_token');

            foreach ($keys as $key => $value) {
                Setting::set($key, $value);
            }
        }
        return $this->responseRedirectBack(
            __('core::messages.success_update'),
            self::SUCCESS_RESPONSE,
            false,
            false
        );
    }
}
