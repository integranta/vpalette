<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class PermissionSeeder
 */
class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        // Права "Владельца" портала.
        $ownerPermissions = [
            'view unpublished advantages',
            'create advantages',
            'edit all advantages',
            'delete any advantages',

            'view unpublished attributes',
            'create attributes',
            'edit all attributes',
            'delete any attribute',

            'view unpublished banners',
            'create banners',
            'edit own banners',
            'edit all banners',
            'delete any banner',

            'view unpublished brands',
            'create brands',
            'edit own brands',
            'edit all brands',
            'delete any brand',

            'view unpublished categories',
            'create categories',
            'edit own categories',
            'edit all categories',
            'delete any category',

            'view unpublished settings',
            'create settings',
            'edit own settings',
            'edit all settings',
            'delete any settings',

            'view unpublished faqs',
            'create faqs',
            'edit own faqs',
            'edit all faqs',
            'delete any faqs',

            'view unpublished news',
            'create news',
            'edit own news',
            'edit all news',
            'delete any news',

            'view unpublished newsletters',
            'create newsletters',
            'edit own newsletters',
            'edit all newsletters',
            'delete any newsletters',

            'view unpublished pages',
            'create pages',
            'edit own pages',
            'edit all pages',
            'delete any pages',

            'view unpublished partners',
            'create partners',
            'edit own partners',
            'edit all partners',
            'delete any partners',

            'view unpublished partner offers',
            'create partner offers',
            'edit own partner offers',
            'edit all partner offers',
            'delete any partner offers',

            'view unpublished partner shops',
            'create partner shops',
            'edit own partner shops',
            'edit all partner shops',
            'delete any partner shops',

            'view unpublished reviews',
            'create reviews',
            'edit own reviews',
            'edit all reviews',
            'delete any reviews',

            'view unpublished seo',
            'create seo',
            'edit own seo',
            'edit all seo',
            'delete any seo',

            'view unpublished stocks',
            'create stocks',
            'edit own stocks',
            'edit all stocks',
            'delete any stocks',

            'view unpublished subscribers',
            'create subscribers',
            'edit own subscribers',
            'edit all subscribers',
            'delete any subscribers'
        ];

        $ownerRole = Role::create(['name' => 'Owner']);

        foreach ($ownerPermissions as $op) {
            $permission = Permission::create(['name' => $op]);
            $ownerRole->givePermissionTo($permission);
        }

       /* // Права "Администратора" портала.
        $adminPermissions = [];

        // Права "Администратора магазина-партнёра".
        $partnerPermissions = [];

        // Обычный зарегистрированый пользователь
        $registerUserPermissions = [];*/
    }
}
