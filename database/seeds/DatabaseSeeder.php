<?php

use Illuminate\Database\Seeder;
use Modules\Advantages\Database\Seeders\AdvantagesDatabaseSeeder;
use Modules\Attribute\Database\Seeders\AttributeDatabaseSeeder;
use Modules\Attribute\Database\Seeders\AttributeValuesTableSeeder;
use Modules\Banner\Database\Seeders\BannerDatabaseSeeder;
use Modules\Brand\Database\Seeders\BrandDatabaseSeeder;
use Modules\Category\Database\Seeders\CategoryDatabaseSeeder;
use Modules\Core\Database\Seeders\SettingsTableSeeder;
use Modules\Discount\Database\Seeders\DiscountDatabaseSeeder;
use Modules\Faq\Database\Seeders\FaqDatabaseSeeder;
use Modules\News\Database\Seeders\NewsDatabaseSeeder;
use Modules\Newsletters\Database\Seeders\NewslettersDatabaseSeeder;
use Modules\Page\Database\Seeders\PageDatabaseSeeder;
use Modules\Partner\Database\Seeders\PartnerDatabaseSeeder;
use Modules\PartnerOffer\Database\Seeders\PartnerOfferDatabaseSeeder;
use Modules\Product\Database\Seeders\ProductDatabaseSeeder;
use Modules\Reviews\Database\Seeders\ReviewsDatabaseSeeder;
use Modules\Stock\Database\Seeders\StockDatabaseSeeder;
use Modules\Subscriber\Database\Seeders\SubscriberDatabaseSeeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(AdvantagesDatabaseSeeder::class);
        $this->call(BannerDatabaseSeeder::class);
        $this->call(BrandDatabaseSeeder::class);
        $this->call(SettingsTableSeeder::class);
        //$this->call(SubscriberDatabaseSeeder::class);
        $this->call(PageDatabaseSeeder::class);
        //$this->call(NewslettersDatabaseSeeder::class);
        $this->call(StockDatabaseSeeder::class);
        $this->call(CategoryDatabaseSeeder::class);
        $this->call(NewsDatabaseSeeder::class);
        $this->call(AttributeDatabaseSeeder::class);
        $this->call(AttributeValuesTableSeeder::class);
        //$this->call(ProductDatabaseSeeder::class);
        //$this->call(PartnerDatabaseSeeder::class);
        //$this->call(PartnerOfferDatabaseSeeder::class);
        //$this->call(FaqDatabaseSeeder::class);
        //$this->call(ReviewsDatabaseSeeder::class);
        //$this->call(DiscountDatabaseSeeder::class);
    }
}
