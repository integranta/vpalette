<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $adminRole = Role::create(['name' => 'Admin']);
        $adminPermissions = ['manage-users', 'view-users', 'create-users', 'edit-users', 'delete-users'];
        foreach ($adminPermissions as $ap) {
            $permission = Permission::create(['name' => $ap]);
            $adminRole->givePermissionTo($permission);
        }
        $adminUser = User::create([
            'first_name' => 'Pivovarov',
            'last_name' => 'Vasiliy',
            'phone' => '+79990001234',
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => \Hash::make('123456')
        ]);
        $adminUser->assignRole($adminRole);
        DB::table('dashboards')->insert(['count_admins' => 1]);

        $editorRole = Role::create(['name' => 'Editor']);
        $editorPermissions = ['manage-users', 'view-users'];
        foreach ($editorPermissions as $ep) {
            $permission = Permission::firstOrCreate(['name' => $ep]);
            $editorRole->givePermissionTo($permission);
        }
        $editorUser = User::create([
            'first_name' => 'Ivanov',
            'last_name' => 'Ivan',
            'phone' => '+79990001234',
            'name' => 'Editor',
            'email' => 'editor@example.com',
            'password' => Hash::make('123456')
        ]);
        $editorUser->assignRole($editorRole);

        $userRole = Role::create(['name' => 'User']);
        $generalUser = User::create([
            'first_name' => 'Smirnov',
            'last_name' => 'Andrey',
            'phone' => '+79990001234',
            'name' => 'User',
            'email' => 'user@example.com',
            'password' => Hash::make('123456')
        ]);
        $generalUser->assignRole($userRole);

        $partnerRole = Role::create(['name' => 'Partner']);
        $partnerPermissions = [
            'view own published partner offers',
            'create own partner offers',
            'edit own partner offers',
            'delete own partner offers',

            'view own published partner products',
            'create own partner products',
            'edit own partner products',
            'delete own partner products',

            'view own published orders',
            'create own orders',
            'edit own orders',
            'delete own orders',
        ];
    }
}
