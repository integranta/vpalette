export type Maybe<T> = T | null | undefined;
/** All built-in and custom scalars, mapped to their actual values */
export interface Scalars {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    /** Arbitrary data encoded in JavaScript Object Notation. See https://www.json.org/. */
    JSON: { "[object Object]": "any" };
    Date: any;
    /** A [RFC 5321](https://tools.ietf.org/html/rfc5321) compliant email. */
    Email: any;
    DateTime: Date;
    /** A [RFC 5321](https://tools.ietf.org/html/rfc5321) compliant email. */
    Mixed: any;
}

export interface IAuthPayload {
    readonly access_token: Scalars["String"];
    readonly refresh_token: Scalars["String"];
    readonly expires_in: Scalars["Int"];
    readonly token_type: Scalars["String"];
    readonly user: IUser;
}

export interface ICategory {
    readonly categoryName: Scalars["String"];
    readonly categoryRoute: Scalars["String"];
    readonly categoryDescription: Scalars["String"];
    readonly categoryImageSrc: Scalars["String"];
    readonly posts: ReadonlyArray<IPosts>;
}

export interface IFaq {
    readonly faqDate: Scalars["Date"];
    readonly faqCountLike?: Maybe<Scalars["Int"]>;
    readonly faqCountDislike?: Maybe<Scalars["Int"]>;
    readonly question: Scalars["String"];
    readonly answer: Scalars["String"];
    readonly answerDate: Scalars["Date"];
    readonly totalCountLikes: Scalars["Int"];
    readonly totalCountDislikes: Scalars["Int"];
}

export interface IForgotPasswordInput {
    readonly email: Scalars["String"];
}

export interface IForgotPasswordResponse {
    readonly status: Scalars["String"];
    readonly message?: Maybe<Scalars["String"]>;
}

export interface ILoginInput {
    readonly username: Scalars["String"];
    readonly password: Scalars["String"];
}

export interface ILogoutResponse {
    readonly status: Scalars["String"];
    readonly message?: Maybe<Scalars["String"]>;
}

export interface IMutation {
    readonly createUser: IUser;
    readonly login: IAuthPayload;
    readonly refreshToken: IRefreshTokenPayload;
    readonly logout: ILogoutResponse;
    readonly forgotPassword: IForgotPasswordResponse;
    readonly updateForgottenPassword: IForgotPasswordResponse;
}

export interface IMutationCreateUserArgs {
    name: Scalars["String"];
    email: Scalars["String"];
    password: Scalars["String"];
}

export interface IMutationLoginArgs {
    data?: Maybe<ILoginInput>;
}

export interface IMutationRefreshTokenArgs {
    data?: Maybe<IRefreshTokenInput>;
}

export interface IMutationForgotPasswordArgs {
    data: IForgotPasswordInput;
}

export interface IMutationUpdateForgottenPasswordArgs {
    data?: Maybe<INewPasswordWithCodeInput>;
}

export interface INewPasswordWithCodeInput {
    readonly email: Scalars["String"];
    readonly token: Scalars["String"];
    readonly password: Scalars["String"];
    readonly password_confirmation: Scalars["String"];
}

export interface IOrderByClause {
    readonly field: Scalars["String"];
    readonly order: ISortOrder;
}

export interface IPageInfo {
    /** When paginating forwards, are there more items? */
    readonly hasNextPage: Scalars["Boolean"];
    /** When paginating backwards, are there more items? */
    readonly hasPreviousPage: Scalars["Boolean"];
    /** When paginating backwards, the cursor to continue. */
    readonly startCursor?: Maybe<Scalars["String"]>;
    /** When paginating forwards, the cursor to continue. */
    readonly endCursor?: Maybe<Scalars["String"]>;
    /** Total number of node in connection. */
    readonly total?: Maybe<Scalars["Int"]>;
    /** Count of nodes in current request. */
    readonly count?: Maybe<Scalars["Int"]>;
    /** Current page of request. */
    readonly currentPage?: Maybe<Scalars["Int"]>;
    /** Last page in connection. */
    readonly lastPage?: Maybe<Scalars["Int"]>;
}

export interface IPaginatorInfo {
    /** Total count of available items in the page. */
    readonly count: Scalars["Int"];
    /** Current pagination page. */
    readonly currentPage: Scalars["Int"];
    /** Index of first item in the current page. */
    readonly firstItem?: Maybe<Scalars["Int"]>;
    /** If collection has more pages. */
    readonly hasMorePages: Scalars["Boolean"];
    /** Index of last item in the current page. */
    readonly lastItem?: Maybe<Scalars["Int"]>;
    /** Last page number of the collection. */
    readonly lastPage: Scalars["Int"];
    /** Number of items per page in the collection. */
    readonly perPage: Scalars["Int"];
    /** Total items available in the collection. */
    readonly total: Scalars["Int"];
}

export interface IPartner {
    readonly partnerName: Scalars["String"];
    readonly partnerAddress?: Maybe<Scalars["String"]>;
    readonly partnerCity?: Maybe<Scalars["String"]>;
    readonly partnerEmail?: Maybe<Scalars["Email"]>;
    readonly partnerPhone?: Maybe<Scalars["String"]>;
    readonly owner: ReadonlyArray<IUser>;
    readonly offers: ReadonlyArray<IPartnerOffers>;
}

export interface IPartnerOffers {
    readonly amount: Scalars["Int"];
    readonly price: Scalars["Int"];
    readonly partner: ReadonlyArray<IPartner>;
    readonly products: ReadonlyArray<IProduct>;
}

export interface IPosts {
    readonly id: Scalars["ID"];
    readonly sort?: Maybe<Scalars["Int"]>;
    readonly postRoute: Scalars["String"];
    readonly activityEndAt: Scalars["String"];
    readonly author?: Maybe<IUser>;
    readonly title: Scalars["String"];
    readonly previewText: Scalars["String"];
    readonly previewImageSrc: Scalars["String"];
    readonly detailText: Scalars["String"];
    readonly detailImageSrc: Scalars["String"];
    readonly countView?: Maybe<Scalars["Int"]>;
    readonly category: ReadonlyArray<ICategory>;
    readonly seoInfo: ReadonlyArray<ISeo>;
}

export interface IProduct {
    readonly sort: Scalars["Int"];
    readonly isPopular?: Maybe<Scalars["Boolean"]>;
    readonly isActive: Scalars["Boolean"];
    readonly productName: Scalars["String"];
    readonly productMinPrice: Scalars["Int"];
    readonly productRoute: Scalars["String"];
    readonly productDescription: Scalars["String"];
    readonly productImageSrc: Scalars["String"];
    readonly productArticle: Scalars["String"];
    readonly productBarcode: Scalars["String"];
    readonly galleryImages?: Maybe<Scalars["JSON"]>;
    readonly totalCountRating: Scalars["Int"];
    readonly avgRating?: Maybe<Scalars["Float"]>;
    readonly ratingPercent?: Maybe<Scalars["Float"]>;
    readonly questions: ReadonlyArray<IFaq>;
    readonly reviews: ReadonlyArray<IReview>;
    readonly seoInfo: ReadonlyArray<ISeo>;
    readonly category: ReadonlyArray<ICategory>;
    readonly partnerOffers: ReadonlyArray<IPartnerOffers>;
}

export interface IQuery {
    readonly users?: Maybe<IUserPaginator>;
    readonly user?: Maybe<IUser>;
    readonly me?: Maybe<IUser>;
    readonly slides: ReadonlyArray<ISlides>;
    readonly slide?: Maybe<ISlides>;
    readonly categories: ReadonlyArray<ICategory>;
    readonly category?: Maybe<ICategory>;
    readonly posts: ReadonlyArray<IPosts>;
    readonly post?: Maybe<IPosts>;
    readonly seo?: Maybe<ISeo>;
    readonly products: ReadonlyArray<IProduct>;
    readonly product?: Maybe<IProduct>;
    readonly partners: ReadonlyArray<IPartner>;
    readonly partner?: Maybe<IPartner>;
    readonly partnerOffers: ReadonlyArray<IPartnerOffers>;
    readonly partnerOffer?: Maybe<IPartnerOffers>;
    readonly faqs: ReadonlyArray<IFaq>;
    readonly faq?: Maybe<IFaq>;
    readonly reviews: ReadonlyArray<IReview>;
    readonly review?: Maybe<IReview>;
}

export interface IQueryUsersArgs {
    count: Scalars["Int"];
    page?: Maybe<Scalars["Int"]>;
}

export interface IQueryUserArgs {
    id?: Maybe<Scalars["ID"]>;
}

export interface IQuerySlideArgs {
    id: Scalars["Int"];
}

export interface IQueryCategoryArgs {
    id: Scalars["Int"];
}

export interface IQueryPostArgs {
    id: Scalars["Int"];
}

export interface IQuerySeoArgs {
    id: Scalars["Int"];
}

export interface IQueryProductArgs {
    id: Scalars["Int"];
}

export interface IQueryPartnerArgs {
    id: Scalars["Int"];
}

export interface IQueryPartnerOfferArgs {
    id: Scalars["Int"];
}

export interface IQueryFaqArgs {
    id: Scalars["Int"];
}

export interface IQueryReviewArgs {
    id: Scalars["Int"];
}

export interface IRefreshTokenInput {
    readonly refresh_token?: Maybe<Scalars["String"]>;
}

export interface IRefreshTokenPayload {
    readonly access_token: Scalars["String"];
    readonly refresh_token: Scalars["String"];
    readonly expires_in: Scalars["Int"];
    readonly token_type: Scalars["String"];
}

export interface IReview {
    readonly id: Scalars["ID"];
    readonly virtues: Scalars["String"];
    readonly limitations: Scalars["String"];
    readonly comments: Scalars["String"];
    readonly makeAnonymous?: Maybe<Scalars["Boolean"]>;
    readonly rating: Scalars["Int"];
    readonly author?: Maybe<IUser>;
    readonly product?: Maybe<ReadonlyArray<IProduct>>;
    readonly totalCountLikes: Scalars["Int"];
    readonly totalCountDislikes: Scalars["Int"];
    readonly totalCountRating: Scalars["Int"];
    readonly avgRating?: Maybe<Scalars["Float"]>;
    readonly ratingPercent?: Maybe<Scalars["Float"]>;
}

export interface ISeo {
    readonly id: Scalars["ID"];
    readonly metaTitle?: Maybe<Scalars["String"]>;
    readonly metaDescription?: Maybe<Scalars["String"]>;
    readonly metaKeywords?: Maybe<Scalars["String"]>;
    readonly metaCanonicalUrl?: Maybe<Scalars["String"]>;
    readonly metaGoogleTagManager?: Maybe<Scalars["String"]>;
    readonly metaGoogleAnalytics?: Maybe<Scalars["String"]>;
    readonly metaYandexMetrica?: Maybe<Scalars["String"]>;
}

export interface ISlides {
    readonly id: Scalars["ID"];
    readonly sort?: Maybe<Scalars["Int"]>;
    readonly title: Scalars["String"];
    readonly text: Scalars["String"];
    readonly buttonText: Scalars["String"];
    readonly buttonLink: Scalars["String"];
    readonly bannerSrc: Scalars["String"];
}

export const enum ISortOrder {
    Asc = "ASC",
    Desc = "DESC"
}

export interface IUser {
    readonly id: Scalars["ID"];
    readonly name: Scalars["String"];
    readonly email: Scalars["String"];
}

export interface IUserPaginator {
    readonly paginatorInfo: IPaginatorInfo;
    readonly data: ReadonlyArray<IUser>;
}
