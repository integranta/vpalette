<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Product\Models\Product;

/**
 * Class Article
 *
 * @package App\Rules
 */
class Article implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (Product::where('article', '=', $value)->first()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Product not found this article in database.';
    }
}
