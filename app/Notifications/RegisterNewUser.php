<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class RegisterNewUser
 *
 * @package App\Notifications
 */
class RegisterNewUser extends Notification
{
    use Queueable;

    /** @var User */
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @param  User  $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject("Зарегистрировался новый пользователь")
            ->greeting('Привет !')
            ->line("Зарегистрировался новый пользователь {$this->user->name}")
            ->action(
                'Посмотреть информацию об пользователь',
                url(route('admin.users.show', ['id' => $this->user->id]))
            )
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }

    /**
     * @param $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable): array
    {
        return [
            'message' => "Зарегистрировался новый пользователь {$this->user->name}",
            'typeNotify' => 'register_user',
            'url' => route('admin.users.show', ['id' => $this->user->id])
        ];
    }

    /**
     * Get the JSON representation of the notification for Pusher
     *
     * @param $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'message' => "Зарегистрировался новый пользователь {$this->user->name}",
            'typeNotify' => 'register_user',
            'url' => route('admin.users.show', ['id' => $this->user->id])
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'message' => "Зарегистрировался новый пользователь {$this->user->name}",
            'typeNotify' => 'register_user',
            'url' => route('admin.users.show', ['id' => $this->user->id])
        ];
    }
}
