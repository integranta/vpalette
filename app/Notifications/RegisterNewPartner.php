<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Modules\Partner\Models\Partner;

/**
 * Class RegisterNewPartner
 *
 * @package App\Notifications
 */
class RegisterNewPartner extends Notification
{
    use Queueable;

    /** @var Partner */
    protected $partner;

    /**
     * Create a new notification instance.
     *
     * @param  Partner  $partner
     *
     * @return void
     */
    public function __construct(Partner $partner)
    {
        $this->partner = $partner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Зарегистрировался новый партнёр')
            ->greeting('Привет !')
            ->line("Зарегистрировался новый партнёр {$this->partner->user->name}")
            ->action(
                'Посмотреть информацию об партнёре',
                url(route('admin.partners.show', ['id' => $this->partner->id]))
            )
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }

    /**
     * @param $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable): array
    {
        return [
            'message' => "Зарегистрировался новый партнёр {$this->partner->user->name}",
            'typeNotify' => 'new_partner',
            'url' => route('admin.partners.show', ['id' => $this->partner->id])
        ];
    }

    /**
     * @param $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'message' => "Зарегистрировался новый партнёр {$this->partner->user->name}",
            'typeNotify' => 'new_partner',
            'url' => route('admin.partners.show', ['id' => $this->partner->id])
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'message' => "Зарегистрировался новый партнёр {$this->partner->user->name}",
            'typeNotify' => 'new_partner',
            'url' => route('admin.partners.show', ['id' => $this->partner->id])
        ];
    }
}
