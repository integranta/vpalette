<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Class RegisterNewPartnerShop
 *
 * @package App\Notifications
 */
class RegisterNewPartnerShop extends Notification
{
    use Queueable;

    /** @var PartnerShop */
    protected $partnerShop;

    /**
     * Create a new notification instance.
     *
     * @param  PartnerShop  $partnerShop
     */
    public function __construct(PartnerShop $partnerShop)
    {
        $this->partnerShop = $partnerShop;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Добавлен новый партнёрский магазин')
            ->greeting('Привет !')
            ->line("Добавлен новый партнёрский магазин - {$this->partnerShop->shop_name}")
            ->action(
                'Посмотреть информацию об магазине',
                url(route('admin.partner-shop.show', ['id' => $this->partnerShop->id]))
            )
            ->line('Сообщение был отправлено системой уведомлений!')
            ->line('Отвечать на это письмо не нужно.');
    }

    /**
     * @param $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'message' => "Добавлен новый партнёрский магазин - {$this->partnerShop->shop_name}",
            'typeNotify' => 'register_partnerShop',
            'url' => route('admin.partner-shop.show', ['id' => $this->partnerShop->id])
        ]);
    }

    /**
     * @param $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable): array
    {
        return [
            'message' => "Добавлен новый партнёрский магазин - {$this->partnerShop->shop_name}",
            'typeNotify' => 'register_partnerShop',
            'url' => route('admin.partner-shop.show', ['id' => $this->partnerShop->id])
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [
            'message' => "Добавлен новый партнёрский магазин - {$this->partnerShop->shop_name}",
            'typeNotify' => 'register_partnerShop',
            'url' => route('admin.partner-shop.show', ['id' => $this->partnerShop->id])
        ];
    }
}
