<?php


namespace App\Traits;

/**
 * Trait FlashMessages
 *
 * @package App\Traits
 */
trait FlashMessages
{
    /**
     * @var array
     */
    protected $errorMessages = [];

    /**
     * @var array
     */
    protected $infoMessages = [];

    /**
     * @var array
     */
    protected $successMessages = [];

    /**
     * @var array
     */
    protected $warningMessages = [];

    /**
     * @param  array|string  $message
     * @param  string        $type
     */
    public function setFlashMessage($message, string $type): void
    {
        $model = 'infoMessages';

        switch ($type) {
            case 'info':
                {
                    $model = 'infoMessages';
                }
                break;
            case 'error':
                {
                    $model = 'errorMessages';
                }
                break;
            case 'success':
                {
                    $model = 'successMessages';
                }
                break;
            case 'warning':
                {
                    $model = 'warningMessages';
                }
                break;
        }

        if (is_array($message)) {
            foreach ($message as $key => $value) {
                $this->$model[] = $value;
            }
        } else {
            $this->$model[] = $message;
        }
    }

    /**
     * @return array
     */
    public function getFlashMessage(): array
    {
        return [
            'error' => $this->errorMessages,
            'info' => $this->infoMessages,
            'success' => $this->successMessages,
            'warning' => $this->warningMessages,
        ];
    }

    /**
     * Flushing flash messages to Laravel's session
     */
    public function showFlashMessages(): void
    {
        session()->flash('error', $this->errorMessages);
        session()->flash('info', $this->infoMessages);
        session()->flash('success', $this->successMessages);
        session()->flash('warning', $this->warningMessages);
    }
}
