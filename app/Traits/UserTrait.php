<?php

namespace App\Traits;

use Auth;
use Spatie\Permission\Models\Permission;
use Storage;

/**
 * Trait UserTrait
 *
 * @package App\Traits
 */
trait UserTrait
{
    /**
     * @return string
     */
    public function getProfilelinkAttribute(): string
    {
        return route('admin.users.edit', ['user' => $this->id]);
    }

    /**
     * @return string
     */
    public function getAvatarlinkAttribute(): string
    {
        if (Storage::disk('public')->exists($this->avatar)) {
            return Storage::disk('public')->url($this->avatar);
        }
        return asset('assets/img/avatar/avatar-1.png');
    }

    /**
     * @return bool
     */
    public function getIsmeAttribute(): bool
    {
        return Auth::check() && Auth::id() === $this->id;
    }

    /**
     * @return array
     */
    public function getAllPermissionsAttribute(): array
    {
        $permissions = [];
        foreach (Permission::all() as $permission) {
            if (Auth::user()->can($permission->name)) {
                $permissions[] = $permission->name;
            }
        }
        return $permissions;
    }
}
