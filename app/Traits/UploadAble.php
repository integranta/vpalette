<?php


namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Image;
use Storage;
use Str;

/***
 * Trait UploadAble
 *
 * @package App\Traits
 */
trait UploadAble
{
    /**
     * Helper function for upload multiple images.
     *
     * @param  array   $files
     *
     * @param  string  $path
     *
     * @return array $gallery
     */
    public function uploadFiles(array $files, string $path): array
    {
        $result = [];

        if (!empty($files)) {
            foreach ($files as $file) {
                if (env('FILESYSTEM_DRIVER') === 's3') {
                    Storage::disk('s3')->put($path.$file->getClientOriginalName(), file_get_contents($file));
                    $result[] = [
                        'name' => $file->getClientOriginalName(),
                        'path' => Storage::cloud()->url($path.$file->getClientOriginalName())
                    ];
                } else {
                    Storage::disk('public')->put($path.$file->getClientOriginalName(), file_get_contents($file));
                    $result[] = [
                        'name' => $file->getClientOriginalName(),
                        'path' => Storage::url($path.$file->getClientOriginalName())
                    ];
                }
            }
        }
        return $result;
    }

    /**
     * @param  UploadedFile  $file
     * @param  string|null   $folder
     * @param  string        $disk
     * @param  string|null   $filename
     *
     * @return false|string
     */
    public function uploadOne(
        UploadedFile $file,
        string $folder = null,
        string $disk = 'public',
        string $filename = null
    ): string {
        $name = $filename ?? Str::random(25);

        return $file->storeAs(
            $folder,
            $name.'.'.$file->getClientOriginalExtension(),
            $disk
        );
    }

    /**
     * @param  string  $path
     * @param  string  $disk
     *
     * @return void
     */
    public function deleteOne(string $path = null, string $disk = 'public'): void
    {
        Storage::disk($disk)->delete($path);
    }

    /**
     * Upload image with Intervention Image Cache
     *
     * @param  string|UploadedFile  $image
     * @param  string               $pathToUpload
     * @param  string               $disk
     *
     * @return string
     */
    public function makeCachedImage(string $image, string $pathToUpload, string $disk = 'uploads'): string
    {
        $_image = Image::make($image);

        $cachedImage = Image::cache(static function ($img) use ($_image) {
            return $img->make($_image)->resize(800, 600);
        });

        $cachedImg = Image::make($cachedImage)->encode('jpg');
        $hash = Str::random();
        $path = $pathToUpload.'/'.$hash.'.jpg';

        Storage::disk($disk)->put(
            $path, $cachedImg->__toString()
        );
        return $path;
    }
}
