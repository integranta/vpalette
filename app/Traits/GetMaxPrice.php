<?php


namespace App\Traits;


use Modules\Product\Models\Product;

trait GetMaxPrice
{
	/**
	 * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $products
	 *
	 * @return mixed
	 */
	public function getMaxPrice($products) {
		return $products->get()->map(function (Product $product) {
			return $product->partnerOffers()->min('price');
		})->max();
	}
}