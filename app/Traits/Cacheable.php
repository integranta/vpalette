<?php


namespace App\Traits;


use Cache;
use Illuminate\Database\Eloquent\Model;
use Str;

/**
 * Trait Cacheable
 *
 * @package App\Traits
 */
trait Cacheable
{
    /**
     * Увеличивает кол-во элементов в кэше, через Cache::increment с постфиксом, как первый последний созданный ID
     *
     * @param  Model   $model
     * @param  string  $cacheKey
     *
     * @return void
     */
    private function incrementCountItemsInCache(Model $model, string $cacheKey): void
    {
        $postfix = Str::of($model->orderByDesc('id')->first(['id'])->id ?? 1)->start('_');
        Cache::increment($cacheKey.$postfix);
    }

    /**
     * Уменьшает кол-во элементов в кэше, через Cache::decrement с постфиксом, как первый последний созданный ID
     *
     * @param  Model   $model
     * @param  string  $cacheKey
     *
     * @return void
     */
    private function decrementCountItemsInCache(Model $model, string $cacheKey): void
    {
        $postfix = Str::of($model->orderByDesc('id')->first(['id'])->id ?? 1)->start('_');
        Cache::decrement($cacheKey.$postfix);
    }
}
