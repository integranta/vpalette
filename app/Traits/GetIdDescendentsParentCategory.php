<?php


namespace App\Traits;

use Modules\Category\Models\Category;

/**
 * Возвращает ID всех категорий-потомков.
 *
 * Trait GetIdDescendentsParentCategory
 *
 * @package App\Traits
 */
trait GetIdDescendentsParentCategory
{
	/**
	 * Получить ID всех категорий-потомков
	 *
	 * @param  string  $column
	 * @param  string  $value
	 *
	 * @return array
	 */
	public function getIdDescendentsParentCategory(string $column, string $value): array
	{
		return Category::whereParentId($parentId = Category::where($column, '=', $value)
			->value('id'))
			->pluck('id')
			->push($parentId)
			->all();
	}
}