<?php


namespace App\Traits;


trait GetProductPaginateData
{
	/**
	 * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $products
	 *
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
	 */
	public function getProductPaginateData($products) {
		return $products->paginate(
			15,
			[
				'id',
				'sections',
				'article',
				'barcode',
				'name',
				'slug',
				'type_product',
				'is_new',
				'full_path',
				'full_paths',
				'about',
				'weight',
				'min_price',
				'sort',
				'active',
				'is_popular',
				'brand_id',
				'created_at',
				'updated_at',
				'deleted_at'
			],
			'page',
			1
		);
	}
}