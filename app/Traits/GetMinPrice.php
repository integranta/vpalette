<?php


namespace App\Traits;


use Modules\Product\Models\Product;

trait GetMinPrice
{
	/**
	 * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $products
	 *
	 * @return mixed
	 */
	public function getMinPrice($products) {
		return $products->get()->map(function (Product $product) {
			return $product->partnerOffers()->min('price');
		})->min();
	}
}