<?php


namespace App\Traits;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;

trait GetProductPaginateResponse
{
	public function getProductPaginateResponse(LengthAwarePaginator $paginateData, ?int $minPrice, ?int $maxPrice): array {
		return [
			'paginatorInfo' => [
				'currentPage'  => $paginateData->currentPage(),
				'firstItem'    => $paginateData->firstItem(),
				'hasMorePages' => $paginateData->hasMorePages(),
				'lastItem'     => $paginateData->lastItem(),
				'lastPage'     => $paginateData->lastPage(),
				'perPage'      => $paginateData->perPage(),
				'total'        => $paginateData->total()
			],
			'data'          => $paginateData->items(),
			'min_price'     => $minPrice,
			'max_price'     => $maxPrice
		];
	}
}