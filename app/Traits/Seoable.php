<?php


namespace App\Traits;

/***
 * Trait Seoable
 *
 * Заполняет подготовленные поля для связанной модели SEO.
 *
 * @package App\Traits
 */
trait Seoable
{
    private $seoFields = [];

    /**
     * @return array
     */
    public function getSeoFields(): array
    {
        return $this->seoFields;
    }

    /**
     * @param  array  $seoFields
     */
    public function setSeoFields(array $seoFields): void
    {
        $this->seoFields = [
            'title' => $seoFields['title'],
            'description' => $seoFields['description'],
            'keywords' => $seoFields['keywords'],
            'canonical_url' => $seoFields['canonical_url'],
            'google_tag_manager' => $seoFields['google_tag_manager'],
            'ya_direct' => $seoFields['ya_direct'],
            'google_analytics' => $seoFields['google_analytics'],
            'ya_metrica' => $seoFields['ya_metrica'],
            'og_title' => $seoFields['og_title'],
            'og_description' => $seoFields['og_description'],
            'og_image' => $seoFields['og_image'],
            'og_type' => $seoFields['og_type'],
            'og_url' => $seoFields['og_url']
        ];
    }
}
