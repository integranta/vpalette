<?php


namespace App\Traits;

use Illuminate\Database\Query\Builder;
use DB;
use Modules\Product\Models\Product;

/**
 * Возвращает все товары из указанных ID категории.
 *
 * Trait FetchAllProductsFromCategories
 *
 * @package App\Traits
 */
trait GetAllProductsFromCategories
{
	/**
	 * Получить все товары из указанных ID категорий.
	 *
	 * @param  array|string  $categoryIds
	 * @param  string        $sortBy
	 * @param  string        $orderBy
	 *
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function getAllProductsFromCategories($categoryIds, string $sortBy, string $orderBy)
	{
		$productIds = DB::table('category_product')
			->where('category_id', '=', $categoryIds)
			->get(['id', 'product_id'])
			->pluck('product_id')
			->toArray();
		
		return Product::whereIn('id', $productIds)->orderBy($sortBy, $orderBy);
	}
}