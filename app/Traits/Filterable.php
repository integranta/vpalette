<?php


namespace App\Traits;

use App;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Filterable
 *
 * @package App\Traits
 */
trait Filterable
{
    protected $builder;

    /**
     * Return new instance of self class
     *
     * @return self
     */
    public static function make(): Filterable
    {
        return new self;
    }

    /**
     * @param $with
     *
     * @return $this
     */
    public function getWith($with): Filterable
    {
        $this->getChain()->with($with);

        return $this;
    }

    /**
     * Return current Builder
     *
     * @return Model|Builder
     */
    public function getChain()
    {
        return $this->builder;
    }

    /**
     * Return ended by date news
     *
     * @return $this
     */
    public function getEnded(): Filterable
    {
        $this->getChain()->where('published', '=', true)
            ->where('published_end', '!=', null)
            ->where('published_end', '<=', Carbon::now()->toDateTimeString());
        return $this;
    }

    /**
     * Return published and available by this language items
     *
     * @return $this
     */
    public function getReady(): Filterable
    {
        $this->getPublished()->getByLang();

        return $this;
    }

    /**
     * Build chain with current language.
     *
     * @param  null  $lang
     *
     * @return $this
     */
    public function getByLang($lang = null): Filterable
    {
        $this->getChain()->where('language', '=', $lang ? : App::getLocale());
        return $this;
    }

    /**
     * Return published news
     *
     * @return $this
     */
    public function getPublished(): Filterable
    {
        $this->getChain()->where('published', '=', true);

        return $this;
    }

    /**
     * @return $this
     */
    public function getNotPublished(): Filterable
    {
        $this->getChain()->where('published', '=', false);
        return $this;
    }

    /**
     * @return $this
     */
    public function getPlanned(): Filterable
    {
        $this->getChain()->where('published', '=', true)
            ->where('published_at', '>=', Carbon::now()->toDateTimeString());
        return $this;
    }

    /**
     * @return $this
     */
    public function getDeleted(): Filterable
    {
        $this->getChain()->onlyTrashed();
        return $this;
    }

    /**
     * Return pages which can't be deleted at all.
     *
     * @return $this
     */
    public function getGuarded(): Filterable
    {
        $this->getChain()->where('is_guarded', '=', true);
        return $this;
    }

    /**
     * Return an array of items. Not throws exception
     *
     * @param  null  $ipp
     *
     * @return LengthAwarePaginator
     */
    public function getCollection($ipp = null): LengthAwarePaginator
    {
        if ($ipp) {
            return $this->getChain()->paginate($ipp);
        }
        return $this->getChain()->get();
    }

    /**
     * Return an object of item. Not throws exception
     *
     * @return Model
     */
    public function getSingle(): Model
    {
        return $this->getChain()->first();
    }

    /**
     * @param $field
     * @param $direction
     *
     * @return $this
     */
    public function setOrderBy($field, $direction): Filterable
    {
        $this->getChain()->orderBy($field, $direction);

        return $this;
    }

    /**
     * @param $field
     * @param $comp
     * @param $value
     *
     * @return $this
     */
    public function whereLike($field, $comp, $value): Filterable
    {
        $this->getChain()->where($field, $comp, $value);

        return $this;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function except($id): Filterable
    {
        $this->getChain()->where('id', '!=', $id);

        return $this;
    }
}
