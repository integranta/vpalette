<?php


namespace App\Traits;

use App;
use \League\OAuth2\Server\ResourceServer;
use \Laravel\Passport\TokenRepository;
use \Laravel\Passport\Guards\TokenGuard;
use \Laravel\Passport\ClientRepository;
use Auth;
use \Illuminate\Http\Request;

trait UserFromBearerToken
{
	function getUser($bearerToken) {
		$tokenguard = new TokenGuard(
			App::make(ResourceServer::class),
			Auth::createUserProvider('users'),
			App::make(TokenRepository::class),
			App::make(ClientRepository::class),
			App::make('encrypter')
		);
		$request = Request::create('/');
		$request->headers->set('Authorization', 'Bearer ' . $bearerToken);
		return $tokenguard->user($request);
	}
	
	function authorizeUser($bearerToken) {
		$request = request();
		$request->headers->set('Authorization', 'Bearer ' . $bearerToken);
		Auth::setRequest($request);
		
		return Auth::user();
	}
}