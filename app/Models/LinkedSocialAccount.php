<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class LinkedSocialAccount
 *
 * @package App\Models
 * @property int $id
 * @property string $providerId
 * @property string $providerName
 * @property int $userId
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount whereProviderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LinkedSocialAccount whereUserId($value)
 * @mixin \Eloquent
 */
class LinkedSocialAccount extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'provider_name',
        'provider_id'
    ];

    /**
     * Linked User with Social Network
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
