<?php


namespace App\Models;

use Eloquent as Model;
use Str;

/**
 * Class BaseModel
 *
 * @package App\Models
 * @property-write mixed $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel query()
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
    /**
     * Установить имя и slug (ЧПУ) для категории.
     *
     * @param  string  $value
     *
     * @return void
     */
    public function setNameAttribute(string $value): void
    {
        $slug = Str::of($value)->lower()->trim()->slug('_', 'ru');
        $next = 1;

        while (self::where('slug', '=', $slug)->first()) {
            $slug .= '-'.$next;
            $next++;
        }

        $this->attributes['name'] = $value;
        $this->attributes['slug'] = $slug;
    }
}
