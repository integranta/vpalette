<?php

namespace App\DataTables;

use Modules\Reviews\Models\Review;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ReviewsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.reviews.edit', $query->id);
				$deleteRoute = route('admin.reviews.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('id', function ($query) {
				$route = route('admin.reviews.show', $query->id);
				return "<span><a target='_blank' href='$route'>$query->id</a></span>";
			})
			->editColumn('make_anonymous', function ($query) {
				$isAnonymous = filter_var($query->make_anonymous, FILTER_VALIDATE_BOOLEAN);
				$anonymousStatus = $isAnonymous === true
					? __('reviews::templates.make_anonymous.is_anonymous')
					: __('reviews::templates.make_anonymous.not_anonymous');
				$className = $isAnonymous === true
					? 'badge badge-success'
					: 'badge badge-danger';
				
				return "<span class='$className'>$anonymousStatus</span>";
			})
			->rawColumns(['action', 'id', 'make_anonymous']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Review  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Review $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('reviews-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('reviews::templates.table.id')),
			Column::make('virtues')
				->title(__('reviews::templates.table.virtues')),
			Column::make('limitations')
				->title(__('reviews::templates.table.limitations')),
			Column::make('comments')
				->title(__('reviews::templates.table.comments')),
			Column::make('make_anonymous')
				->title(__('reviews::templates.table.make_anonymous')),
			Column::make('rating')
				->title(__('reviews::templates.table.rating')),
			Column::computed('action')
				->title(__('reviews::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Reviews_'.date('YmdHis');
	}
}
