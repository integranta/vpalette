<?php

namespace App\DataTables;

use Modules\News\Models\News;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PostsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.posts.edit', $query->id);
				$deleteRoute = route('admin.posts.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button class="btn btn-sm btn-danger" onclick="return confirm(\'Вы уверены?\')" title="Удалить" type="submit"><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->addColumn('name', function ($query) {
				$postRoute = route('admin.posts.show', $query->id);
				return "<span><a target='_blank' href='$postRoute'>$query->name</a></span>";
			})
			->addColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive ? 'Активен' : 'Не активен';
				$className = $isActive ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->rawColumns(['action', 'name', 'active']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  News  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(News $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('posts-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('news::templates.table.id')),
			Column::make('name')
				->title(__('news::templates.table.name')),
			Column::make('slug')
				->title(__('news::templates.table.slug')),
			Column::make('active')
				->title(__('news::templates.table.active')),
			Column::computed('action')
				->title(__('news::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Posts_'.date('YmdHis');
	}
}
