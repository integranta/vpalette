<?php

namespace App\DataTables;

use Modules\PartnerShop\Models\PartnerShop;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PartnerShopsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.partner-shop.edit', $query->id);
				$deleteRoute = route('admin.partner-shop.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->addColumn('owner', function($query) {
				$owner = $query->partner->user->name;
				
				$route = route('admin.partners.show', $query->partner->id);
				return "<span><a target='_blank' href='$route'>$owner</a></span>";
			})
			->editColumn('id', function ($query) {
				$route = route('admin.partner-shop.show', $query->id);
				return "<span><a target='_blank' href='$route'>$query->id</a></span>";
			})
			->rawColumns(['action', 'owner', 'id']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  PartnerShop  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(PartnerShop $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('partnershops-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('partnershop::templates.table.id')),
			Column::make('shop_name')
				->title(__('partnershop::templates.table.shop_name')),
			Column::make('city')
				->title(__('partnershop::templates.table.city')),
			Column::make('owner')
				->title(__('partnershop::templates.table.owner')),
			Column::computed('action')
				->title(__('partnershop::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'PartnerShops_'.date('YmdHis');
	}
}
