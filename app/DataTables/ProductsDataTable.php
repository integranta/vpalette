<?php

namespace App\DataTables;

use Modules\Product\Models\Product;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use function __;

class ProductsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 *
	 * @return \Yajra\DataTables\DataTableAbstract|\Yajra\DataTables\EloquentDataTable
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action',
				function ($query) {
					
					$editRoute = route('admin.products.edit', $query->id);
					$deleteRoute = route('admin.products.destroy', $query->id);
					$csrfToken = csrf_field();
					$deleteField = method_field('DELETE');
					
					$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
					$button .= $csrfToken;
					$button .= $deleteField;
					$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
					$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
								><i class="fa fa-trash"></i></button></form>';
					return $button;
				})
			->editColumn('name', function ($query) {
				$showRoute = route('admin.products.show', $query->id);
				return "<a target='_blank' href={$showRoute}>$query->name</a>";
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true
					? __('product::templates.active.is_active')
					: __('product::templates.active.not_active');
				$className = $isActive ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->editColumn('is_popular', function ($query) {
				$isPopular = filter_var($query->is_popular, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isPopular === true
					? __('product::templates.is_popular.is_popular')
					: __('product::templates.is_popular.not_popular');
				$className = $isPopular ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->editColumn('is_new', function ($query) {
				$isPopular = filter_var($query->is_new, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isPopular === true
					? __('product::templates.popular.is_new')
					: __('product::templates.popular.not_new');
				$className = $isPopular ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->rawColumns(['action', 'name', 'active', 'is_popular']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Product  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Product $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('products-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('product::templates.table.id')),
			Column::make('article')
				->title(__('product::templates.table.article')),
			Column::make('name')
				->title(__('product::templates.table.name')),
			Column::make('slug')
				->title(__('product::templates.table.slug')),
			Column::make('active')
				->title(__('product::templates.table.active')),
			Column::make('is_popular')
				->title(__('product::templates.table.is_popular')),
			Column::computed('action')
				->width(10)
				->addClass('text-center')
				->title(__('product::templates.table.action'))
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Products_'.date('YmdHis');
	}
}
