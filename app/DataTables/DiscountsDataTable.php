<?php

namespace App\DataTables;

use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use Modules\Discount\Models\Discount;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class DiscountsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.discount.edit', $query->id);
				$deleteRoute = route('admin.discount.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
								><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('type', function ($query) {
				return DiscountTypes::fromValue($query->type)->description;
			})
			->editColumn('amount_type', function ($query) {
				return DiscountAmountTypes::fromValue($query)->description;
			})
			->editColumn('period_activity', function ($query) {
				return DiscountPeriodActivity::fromValue($query->period_activity)->description;
			})
			->rawColumns(['action', 'type', 'amount_type', 'period_activity']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Discount  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Discount $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('discounts-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('discount::templates.table.id')),
			Column::make('name')
				->title(__('discount::templates.table.name')),
			Column::make('type')
				->title(__('discount::templates.table.type')),
			Column::make('amount')
				->title(__('discount::templates.table.amount')),
			Column::make('amount_type')
				->title(__('discount::templates.table.amount_type')),
			Column::make('period_activity')
				->title(__('discount::templates.table.period_activity')),
			Column::make('starts_at')
				->title(__('discount::templates.table.starts_at')),
			Column::make('expires_at')
				->title(__('discount::templates.table.expires_at')),
			Column::computed('action')
				->title(__('discount::templates.table.action'))
				->width(60)
				->addClass('text-center')
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Discounts_'.date('YmdHis');
	}
}
