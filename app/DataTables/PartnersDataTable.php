<?php

namespace App\DataTables;

use Modules\Partner\Models\Partner;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PartnersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
			->addColumn('action', function ($query) {
		
				$editRoute = route('admin.partners.edit', $query->id);
				$deleteRoute = route('admin.partners.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
		
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->addColumn('owner', function ($query) {
				$route = route('admin.users.show', $query->user->id);
				$name = $query->user->name;
				
				return "<span><a href='$route' target='_blank'>$name</a></span>";
			})
			->addColumn('companies', function ($query) {
				$partnerShops = $query->partnerShops;
				$companies = [];
				
				foreach ($partnerShops as $partnerShop) {
					$companies = "<span class='badge badge-light'>$partnerShop->shop_name</span>";
				}
				
				return $companies;
			})
			->editColumn('phone', function($query) {
				return "<span><a href='tel:$query->phone'>$query->phone</a></span>";
			})
			->editColumn('id', function ($query) {
				$route = route('admin.partners.show', $query->id);
				return "<span><a href='$route'>$query->id</a></span>";
			})
			->editColumn('email', function ($query) {
				return "<span><a href='mailto:$query->email'>$query->email</a></span>";
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
		
				$activeStatus = $isActive === true
					? __('partner::templates.active.is_active')
					: __('partner::templates.active.not_active');
		
				$className = $activeStatus ? 'badge badge-success' : 'badge badge-danger';
		
				return "<span class='$className'>$activeStatus</span>";
			})
			->rawColumns([
				'id',
				'email',
				'action',
				'owner',
				'companies',
				'phone',
				'active'
			]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Partner $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Partner $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('partners-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
					->addTableClass('table')
					->orderBy(0);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')
				->title(__('partner::templates.table.id')),
			Column::make('active')
				->title(__('partner::templates.table.active')),
            Column::make('email')
				->title(__('partner::templates.table.email')),
            Column::make('phone')
				->title(__('partner::templates.table.phone')),
            Column::make('owner')
				->title(__('partner::templates.table.owner')),
            Column::make('companies')
				->title(__('partner::templates.table.companies')),
			Column::computed('action')
				->title(__('partner::templates.table.action'))
				->width(60)
				->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Partners_' . date('YmdHis');
    }
}
