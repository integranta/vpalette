<?php

namespace App\DataTables;

use Modules\Seo\Models\Seo;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Modules\Product\Models\Product;
use Modules\Category\Models\Category;
use Modules\News\Models\News;
use Modules\Page\Models\Page;
use Modules\Stock\Models\Stock;

class SeoDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		$seobleType = [
			Product::class  => 'Товар',
			Category::class => 'Категория',
			News::class     => 'Новости',
			Stock::class    => 'Акция',
			Page::class     => 'Страница'
		];
		
		$seobleRoute = [
			Product::class  => 'admin.products.show',
			Category::class => 'admin.categories.show',
			News::class     => 'admin.posts.show',
			Stock::class    => 'admin.stocks.show',
			Page::class     => 'admin.pages.show'
		];
		
		$className = [
			Product::class  => 'badge badge-primary',
			Category::class => 'badge badge-secondary',
			News::class     => 'badge badge-info',
			Stock::class    => 'badge badge-warning',
			Page::class     => 'badge badge-light'
		];
		
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.seo.edit', $query->id);
				$deleteRoute = route('admin.seo.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('seoble_type', function ($query) use ($seobleType, $className) {
				return "<span class='{$className[$query->seoble_type]}'>{$seobleType[$query->seoble_type]}</span>";
			})
			->editColumn('seoble_id', function ($query) use ($seobleRoute) {
				$route = route($seobleRoute[$query->seoble_type], $query->seoble_id);
				return "<a target='_blank' href='$route'>$query->seoble_id</a>";
			})
			->editColumn('id', function ($query) {
				$route = route('admin.seo.show', $query->id);
				return "<span><a href='$route' target='_blank'>$query->id</a></span>";
			})
			->rawColumns(['action', 'seoble_type', 'seoble_id', 'id']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Seo  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Seo $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('seo-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('seo::templates.table.id')),
			Column::make('seoble_id')
				->title(__('seo::templates.table.seoble_id')),
			Column::make('seoble_type')
				->title(__('seo::templates.table.seoble_type')),
			Column::computed('action')
				->title(__('seo::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Seo_'.date('YmdHis');
	}
}
