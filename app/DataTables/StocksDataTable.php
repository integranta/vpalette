<?php

namespace App\DataTables;

use Carbon\Carbon;
use Modules\Stock\Models\Stock;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class StocksDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.stocks.edit', $query->id);
				$deleteRoute = route('admin.stocks.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true
					? __('stock::templates.active.is_active')
					: __('stock::templates.active.not_active');
				$className = $isActive === true
					? 'badge badge-success'
					: 'badge badge-danger';
				return "<span class='$className'>$activeStatus</span>";
			})
			->editColumn('updated_at', function ($query) {
				return Carbon::make($query->updated_at)->longRelativeDiffForHumans();
			})
			->editColumn('name', function ($query) {
				$route = route('admin.stocks.show', $query->id);
				return "<span><a href='$route' target='_blank'>$query->name</a></span>";
			})
			->editColumn('id', function ($query) {
				$route = route('admin.stocks.show', $query->id);
				return "<span><a href='$route' target='_blank'>$query->id</a></span>";
			})
			->rawColumns(['action', 'active', 'updated_at', 'id', 'name']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Stock  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Stock $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('stocks-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('stock::templates.table.id')),
			Column::make('name')
				->title(__('stock::templates.table.name')),
			Column::make('active')
				->title(__('stock::templates.table.active')),
			Column::make('sort')
				->title(__('stock::templates.table.sort')),
			Column::make('updated_at')
				->title(__('stock::templates.table.updated_at')),
			Column::computed('action')
				->title(__('stock::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Stocks_'.date('YmdHis');
	}
}
