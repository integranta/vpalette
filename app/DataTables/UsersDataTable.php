<?php

namespace App\DataTables;

use App\User;
use Carbon\Carbon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', static function ($query) {
				
				$editRoute = route('admin.users.edit', $query->id);
				$deleteRoute = route('admin.users.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('name', static function ($query) {
				$showRoute = route('admin.users.show', $query->id);
				return "<a href={$showRoute}>$query->name</a>";
			})
			->editColumn('id', static function ($query) {
				return $query->id;
			})
			->editColumn('active', static function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true ? 'Активен' : 'Не активен';
				$className = $activeStatus ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->editColumn('created_at', static function ($query) {
				return Carbon::createFromTimeString($query->created_at)->longRelativeToNowDiffForHumans();
			})
			->rawColumns(['action', 'name', 'active', 'id']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  \App\User  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(User $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('users-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('user::templates.id')),
			Column::make('active')
				->title(__('user::templates.active')),
			Column::make('name')
				->title(__('user::templates.name')),
			Column::make('email')
				->title(__('user::templates.email')),
			Column::make('created_at')
				->title(__('user::templates.created_at')),
			Column::computed('action')
				->title(__('user::templates.action'))
				->width(60)
				->addClass('text-center')
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Users_'.date('YmdHis');
	}
}
