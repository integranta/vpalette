<?php

namespace App\DataTables;

use App\Enums\OrderStatus;
use App\Enums\PaymentStatus;
use Modules\Order\Models\Order;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class OrdersDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.orders.edit', $query->id);
				$deleteRoute = route('admin.orders.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
								><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('status', function ($query) {
				$status = [
					'pending'    => OrderStatus::PENDING,
					'processing' => OrderStatus::PROCESSING,
					'delivered'  => OrderStatus::DELIVERED,
					'completed'  => OrderStatus::COMPLETED,
					'decline'    => OrderStatus::DECLINE
				][$query->status];
				
				$statusClassName = [
					'pending'    => 'badge badge-warning',
					'processing' => 'badge badge-info',
					'delivered'  => 'badge badge-dark',
					'completed'  => 'badge badge-success',
					'decline'    => 'badge badge-danger'
				][$status];
				
				return "<span class='$statusClassName'>.$status.</span>";
			})
			->editColumn('name', function ($query) {
				return $query->user->name;
			})
			->editColumn('payment_status', function ($query) {
				$currentStatus = [
					'not_paid' => PaymentStatus::NOT_PAID,
					'paid'     => PaymentStatus::PAID
				][$query->payment_status];
				
				$statusClassName = [
					'not_paid' => 'badge badge-danger',
					'paid'     => 'badge badge-success'
				][$query->payment_status];
				
				$descriptionStatus = PaymentStatus::fromValue($currentStatus)->description;
				
				return "<span class'${$statusClassName}'>$descriptionStatus</span>";
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true
					? __('order::templates.active.is_active')
					: __('order::templates.active.not_active');
				$className = $activeStatus ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->rawColumns([
				'action',
				'status',
				'name',
				'payment_status',
				'active'
			]);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Order  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Order $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('orders-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('order::templates.table.id')),
			Column::make('active')
				->title(__('order::templates.table.active')),
			Column::make('created_at')
				->title(__('order::templates.table.created_at')),
			Column::make('name')
				->title(__('order::templates.table.name')),
			Column::make('status')
				->title(__('order::templates.table.status')),
			Column::make('payment_status')
				->title(__('order::templates.table.payment_status')),
			Column::make('payment_method')
				->title(__('order::templates.table.payment_method')),
			Column::make('order_number')
				->title(__('order::templates.table.order_number')),
			Column::make('grand_total')
				->title(__('order::templates.table.grand_total')),
			Column::computed('action')
				->title(__('order::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Orders_'.date('YmdHis');
	}
}
