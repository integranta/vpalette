<?php

namespace App\DataTables;

use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\Product\Models\Product;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PartnerOffersDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.partner-offers.edit', $query->id);
				$deleteRoute = route('admin.partner-offers.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->addColumn('product', function ($query) {
				$product = Product::firstWhere('id', '=', $query->product_id);
				$route = route('admin.products.show', $product->id);
				
				return "<span><a href='$route' target='_blank'>$product->name</a></span>";
			})
			->rawColumns(['action', 'product']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  PartnerOffer  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(PartnerOffer $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('partneroffers-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('partneroffer::templates.table.id')),
			Column::make('article')
				->title(__('partneroffer::templates.table.article')),
			Column::make('barcode')
				->title(__('partneroffer::templates.table.barcode')),
			Column::make('amount')
				->title(__('partneroffer::templates.table.amount')),
			Column::make('price')
				->title(__('partneroffer::templates.table.price')),
			Column::make('product')
				->title(__('partneroffer::templates.table.product')),
			Column::computed('action')
				->title(__('partneroffer::templates.table.action'))
				->width(60)
				->addClass('text-center')
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'PartnerOffers_'.date('YmdHis');
	}
}
