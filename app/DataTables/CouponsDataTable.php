<?php

namespace App\DataTables;

use Modules\Coupon\Models\Coupon;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CouponsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.coupon.edit', $query->id);
				$deleteRoute = route('admin.coupon.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
								class="btn btn-sm btn-danger"
								onclick="return confirm(\'Вы уверены?\')"
								title="Удалить"
								type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('rule', function ($query) {
				return $query->discount->name;
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true
					? __('coupon::templates.active.is_active')
					: __('coupon::templates.active.not_active');
				$className = $activeStatus ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->rawColumns(['action']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Coupon  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Coupon $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('coupons-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('coupon::templates.table.id')),
			Column::make('rule')
				->title(__('coupon::templates.table.rule')),
			Column::make('code')
				->title(__('coupon::templates.table.code')),
			Column::make('active')
				->title(__('coupon::templates.table.active')),
			Column::make('starts_at')
				->title(__('coupon::templates.table.starts_at')),
			Column::make('expires_at')
				->title(__('coupon::templates.table.expires_at')),
			Column::computed('action')
				->title(__('coupon::templates.table.action'))
				->width(60)
				->addClass('text-center')
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Coupons_'.date('YmdHis');
	}
}
