<?php

namespace App\DataTables;

use Modules\Page\Models\Page;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PagesDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.pages.edit', $query->id);
				$deleteRoute = route('admin.pages.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href='{$editRoute}' class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
									><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true
					? __('page::templates.active.is_active')
					: __('page::templates.active.not_active');
				$className = $activeStatus ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->editColumn('name', function ($query) {
				$route = route('admin.pages.show', $query->id);
				
				return "<span><a target='_blank' href='$route'>$query->name</a></span>";
			})
			->rawColumns(['action', 'active', 'name']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Page  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Page $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('pages-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('page::templates.table.id')),
			Column::make('name')
				->title(__('page::templates.table.name')),
			Column::make('slug')
				->title(__('page::templates.table.slug')),
			Column::make('sort')
				->title(__('page::templates.table.sort')),
			Column::make('active')
				->title(__('page::templates.table.active')),
			Column::computed('action')
				->title(__('page::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Pages_'.date('YmdHis');
	}
}
