<?php

namespace App\DataTables;

use Modules\Category\Models\Category;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CategoriesDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.categories.edit', $query->id);
				$deleteRoute = route('admin.categories.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
								class="btn btn-sm btn-danger"
								onclick="return confirm(\'Вы уверены?\')"
								title="Удалить"
								type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->addColumn('parent', function ($query) {
				return $query->parent->name;
			})
			->editColumn('name', function ($query) {
				$postRoute = route('admin.categories.show', $query->id);
				return "<span><a target='_blank' href='$postRoute'>$query->name</a></span>";
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true
					? __('category::templates.active.is_active')
					: __('category::templates.active.not_active');
				$className = $activeStatus ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->rawColumns(['action', 'parent', 'active', 'name']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Category  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Category $model)
	{
		return $model->newQuery()->with([
			'parent' => function ($query) {
				return $query
					->cacheFor(60 * 60)
					->cacheTags(['category:parent']);
				},
			'children' => function($query) {
				return $query
					->cacheFor(60 * 60)
					->cacheTags(['category:children']);
			}
		]);
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('categories-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('category::templates.table.id')),
			Column::make('name')
				->title(__('category::templates.table.name')),
			Column::make('slug')
				->title(__('category::templates.table.slug')),
			Column::make('parent')
				->title(__('category::templates.table.parent_id')),
			Column::make('active')
				->title(__('category::templates.table.active')),
			Column::computed('action')
				->title(__('category::templates.table.action'))
				->width(60)
				->addClass('text-center')
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Categories_'.date('YmdHis');
	}
}
