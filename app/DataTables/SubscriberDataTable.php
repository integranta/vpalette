<?php

namespace App\DataTables;

use Modules\Subscriber\Models\Subscriber;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use function __;

class SubscriberDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action',
				function ($query) {
					
					$editRoute = route('admin.subscriber.edit', $query->id);
					$deleteRoute = route('admin.subscriber.destroy', $query->id);
					$csrfToken = csrf_field();
					$deleteField = method_field('DELETE');
					
					$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
					$button .= $csrfToken;
					$button .= $deleteField;
					$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
					$button .= '<button
										class="btn btn-sm btn-danger"
										onclick="return confirm(\'Вы уверены?\')"
										title="Удалить"
										type="submit"
								><i class="fa fa-trash"></i></button></form>';
					return $button;
				})
			->addColumn('email', function ($query) {
				$showRoute = route('admin.subscriber.show', $query->id);
				return "<a href={$showRoute}>$query->email</a>";
			})
			->rawColumns(['action', 'email']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  \Modules\Subscriber\Models\Subscriber  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Subscriber $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('subscribers-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(1);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('subscriber::templates.id')),
			Column::make('email')
				->title(__('subscriber::templates.email')),
			Column::make('active')
				->title(__('subscriber::templates.active')),
			Column::computed('action')
				->exportable(false)
				->printable(false)
				->width(60)
				->addClass('text-center')
				->title(__('subscriber::templates.action')),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Subscriber_'.date('YmdHis');
	}
}
