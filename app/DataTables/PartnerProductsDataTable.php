<?php

namespace App\DataTables;

use App\Enums\PartnerProductStatus;
use Modules\PartnerProduct\Models\PartnerProduct;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PartnerProductsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.partner.products.edit', $query->partner_id, $query->product_id);
				$deleteRoute = route('admin.partner.products.destroy', $query->partner_id, $query->product_id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->addColumn('product', function ($query) {
				$route = route('admin.products.show', $query->product_id);
				
				return "<span><a href='$route' target='_blank'>$query->product->name</a></span>";
			})
			->editColumn('status', function ($query) {
				$status = [
					'queue'              => PartnerProductStatus::QUEUE,
					'added'              => PartnerProductStatus::ADDED,
					'exist'              => PartnerProductStatus::EXIST,
					'dont_match_subject' => PartnerProductStatus::DONT_MATCH_SUBJECT,
					'false_product'      => PartnerProductStatus::FALSE_PRODUCT
				][$query->status_offer];
				
				$className = [
					'queue'              => 'badge badge-warning',
					'added'              => 'badge badge-success',
					'exist'              => 'badge badge-danger',
					'dont_match_subject' => 'badge badge-secondary',
					'false_product'      => 'badge badge-dark'
				][$status];
				
				$descriptionStatus = PartnerProductStatus::fromValue($status)->description;
				
				return "<span class='$className'>$descriptionStatus</span>";
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				
				$activeStatus = $isActive === true
					? __('partnerproduct::templates.active.is_active')
					: __('partnerproduct::templates.active.not_active');
				
				$className = $activeStatus ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->rawColumns(['action', 'product', 'status', 'active']);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  PartnerProduct  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(PartnerProduct $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('partnerproducts-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('partnerproduct::templates.table.id')),
			Column::make('active')
				->title(__('partnerproduct::templates.table.active')),
			Column::make('product')
				->title(__('partnerproduct::templates.table.product')),
			Column::make('status')
				->title(__('partnerproduct::templates.table.status')),
			Column::computed('action')
				->title(__('partnerproduct::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'PartnerProducts_'.date('YmdHis');
	}
}
