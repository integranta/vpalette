<?php

namespace App\DataTables;

use App\User;
use Modules\Faq\Models\Faq;
use Modules\Product\Models\Product;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class QuestionsDataTable extends DataTable
{
	/**
	 * Build DataTable class.
	 *
	 * @param  mixed  $query  Results from query() method.
	 *
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables()
			->eloquent($query)
			->addColumn('action', function ($query) {
				
				$editRoute = route('admin.faqs.edit', $query->id);
				$deleteRoute = route('admin.faqs.destroy', $query->id);
				$csrfToken = csrf_field();
				$deleteField = method_field('DELETE');
				
				$button = "<form method='POST' accept-charset='UTF-8' action=".$deleteRoute.">";
				$button .= $csrfToken;
				$button .= $deleteField;
				$button .= "<a href={$editRoute} class='btn btn-sm btn-primary'><i class='fa fa-edit'></i></a>";
				$button .= '<button
									class="btn btn-sm btn-danger"
									onclick="return confirm(\'Вы уверены?\')"
									title="Удалить"
									type="submit"
							><i class="fa fa-trash"></i></button></form>';
				return $button;
			})
			->editColumn('active', function ($query) {
				$isActive = filter_var($query->active, FILTER_VALIDATE_BOOLEAN);
				$activeStatus = $isActive === true
					? __('product::templates.active.is_active')
					: __('product::templates.active.not_active');
				$className = $isActive ? 'badge badge-success' : 'badge badge-danger';
				
				return "<span class='$className'>$activeStatus</span>";
			})
			->addColumn('product', function ($query) {
				if ($query->product_id === null) {
					return null;
				}
				
				$productRoute = route('admin.products.show', ['product' => $query->product_id]);
				$product = Product::firstWhere('id', '=', $query->product_id);
				
				return "<span><a target='_blank' href={$productRoute}>$product->name</a></span>";
			})
			->addColumn('user', function ($query) {
				if ($query->user_id === null)
				{
					return null;
				}
				$userRoute = route('admin.users.show', ['user' => $query->user_id]);
				$user = User::firstWhere('id', '=', $query->user_id);
				return "<span><a target='_blank' href={$userRoute}>$user->name</a></span>";
			})
			->editColumn('id', function ($query) {
				$route = route('admin.faqs.show', ['faq' => $query->id]);
				return "<span><a target='_blank' href='$route'>$query->id</a></span>";
			})
			->rawColumns([
				'id',
				'action',
				'active',
				'product',
				'user'
			]);
	}
	
	/**
	 * Get query source of dataTable.
	 *
	 * @param  Faq  $model
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Faq $model)
	{
		return $model->newQuery();
	}
	
	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->setTableId('questions-table')
			->columns($this->getColumns())
			->minifiedAjax()
			->dom('Bfrtip')
			->languageUrl('/assets/configs/DataTables/Lang/ru/Russian.json')
			->addTableClass('table')
			->orderBy(0);
	}
	
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return [
			Column::make('id')
				->title(__('faq::templates.table.id')),
			Column::make('active')
				->title(__('faq::templates.table.active')),
			Column::make('question')
				->title(__('faq::templates.table.question')),
			Column::make('product')
				->title(__('faq::templates.table.product')),
			Column::make('user')
				->title(__('faq::templates.table.user')),
			Column::computed('action')
				->title(__('faq::templates.table.action'))
				->width(60)
				->addClass('text-center'),
		];
	}
	
	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Questions_'.date('YmdHis');
	}
}
