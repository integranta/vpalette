<?php


namespace App\Services;


use App\Http\Controllers\Controller;
use App\Traits\FlashMessages;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

/**
 * Class BaseService
 *
 * @package App\Services
 */
class BaseService extends Controller
{
    use FlashMessages;

    /**
     *
     */
    public const SUCCESS_RESPONSE = 'success';
    /**
     *
     */
    public const FAIL_RESPONSE = 'error';

    /**
     * @var null
     */
    public $data = null;

    /**
     * @param  string  $title
     * @param  string  $subTitle
     * @param  string  $leadText
     *
     * @return void
     */
    public function setPageTitle(string $title, string $subTitle, string $leadText): void
    {
        view()->share(['pageTitle' => $title, 'subTitle' => $subTitle, 'leadText' => $leadText]);
    }

    /**
     * @param  int     $errorCode
     * @param  string  $message
     *
     * @return Response
     */
    public function showErrorPage(int $errorCode = 404, string $message = null): Response
    {
        $data['message'] = $message;
        return response()->view('errors.'.$errorCode, $data, $errorCode);
    }

    /**
     * @param  bool          $error
     * @param  int           $responseCode
     * @param  array|string  $message
     * @param  null          $data
     *
     * @return JsonResponse
     */
    public function responseJson(
        bool $error = true,
        int $responseCode = 200,
        $message = null,
        $data = null
    ): JsonResponse {
        return response()->json([
            'error' => $error,
            'response_code' => $responseCode,
            'message' => $message,
            'data' => $data
        ]);
    }

    /**
     * @param  string        $route
     * @param  array|string  $message
     * @param  string        $type
     * @param  bool          $error
     * @param  bool          $withOldInputWhenError
     * @param  array|null    $routeParams
     *
     * @return RedirectResponse
     */
    public function responseRedirect(
        string $route,
        string $message,
        string $type = 'info',
        bool $error = false,
        bool $withOldInputWhenError = false,
        array $routeParams = []
    ): RedirectResponse {
        $this->setFlashMessage($message, $type);
        $this->showFlashMessages();
        if ($error && $withOldInputWhenError) {
            return redirect()->back()->withInput();
        }
        return redirect()->route($route, $routeParams);
    }

    /**
     * @param  array|string  $message
     * @param  string        $type
     * @param  bool          $withOldInputWhenError
     * @param  bool          $error
     *
     * @return RedirectResponse
     */
    public function responseRedirectBack(
        string $message,
        string $type = 'info',
        bool $withOldInputWhenError = false,
        bool $error = false
    ): RedirectResponse {
        $this->setFlashMessage($message, $type);
        $this->showFlashMessages();

        if ($withOldInputWhenError && $error) {
            return redirect()->back()->withInput();
        }
        return redirect()->back();
    }
}
