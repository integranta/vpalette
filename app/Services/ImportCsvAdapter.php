<?php


namespace App\Services;

use App\Contracts\ImportableCsvContract;
use App\User;
use File;
use Illuminate\Http\UploadedFile;
use League\Csv\Exception;
use League\Csv\Reader;
use Modules\Product\Models\Product;

/**
 * Class ImportCsvAdapter
 *
 * @property string            $delimiter
 * @property array             $rows
 * @property File|UploadedFile $file
 * @property array             $successfulRows
 * @property int               $productId
 * @property int               $partnerShopId
 * @property User              $importerBy
 * @property array             $fields
 *
 * @package App\Services
 */
class ImportCsvAdapter implements ImportableCsvContract
{
    /** @var string|null */
    private $delimiter;

    /** @var array|null */
    private $rows;

    /** @var array|null */
    private $successImportedRows;

    /** @var File|UploadedFile|null */
    private $file;

    /** @var int|null */
    private $productId;

    /** @var int|null */
    private $partnerShopId;

    /** @var User|null */
    private $importerBy;

    /** @var array|null */
    private $fields;

    /**
     * Return successful imported rows.
     *
     * @return array
     */
    public function getSuccessfulImportRows(): array
    {
        return $this->successImportedRows;
    }

    /**
     * Set successful imported rows.
     *
     * @param  array  $rows
     *
     * @return void
     */
    public function setSuccessfulImportRows(array $rows): void
    {
        $this->successImportedRows = $rows;
    }

    /**
     * Return importer
     *
     * @return User
     */
    public function getImportedBy(): User
    {
        return $this->importerBy;
    }

    /**
     * Set importer which be use for notification to successful or failed import file
     *
     * @param  User  $importedBy
     *
     * @return void
     */
    public function setImporterBy(User $importedBy): void
    {
        $this->importerBy = $importedBy;
    }

    /**
     * Return Id find product to importing file
     *
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * Set Id product for which be set amount and price from importing file
     *
     * @param  int  $id
     *
     * @return void
     */
    public function setProductId(int $id): void
    {
        $this->productId = $id;
    }

    /**
     * Upload imported file to Storage
     *
     * @param  UploadedFile  $file
     *
     * @return void
     */
    public function uploadFile(UploadedFile $file): void
    {
        $fileName = date('j-Y-H-i').'.'.$file->getClientOriginalExtension();
        $this->setFile($file->storePubliclyAs('import', $fileName));
    }

    /**
     * @return array
     */
    public function process(): array
    {
        try {
            $this->parse();
        } catch (Exception $exception) {
            throw new $exception;
        }

        $rows = $this->getRows();
        $fields = $this->getFields();
        $sourceHeaders = config('importcatalog.db_fields');
        $headerRow = array_combine($sourceHeaders, $fields);

        $passedOffers = [];
        $skippedOffers = [];

        $arResult = [];

        $swapHeaderRow = array_flip($headerRow);

        foreach ($rows as $row) {
            $arResult[] = array_combine($swapHeaderRow, $row);
        }

        $arOffers = [];

        foreach ($arResult as $key => $arItem) {
            $product = (new Product)->findByArticle($arItem['article']);

            if (!empty($product)) {
                $passedOffers[] = $product->id;

                $arOffers[] = [
                    'partner_shop_id' => $this->getPartnerShopId(),
                    'product_id' => $product->id,
                    'article' => $arItem['article'],
                    'barcode' => $arItem['barcode'],
                    'amount' => $arItem['amount'],
                    'price' => $arItem['price']
                ];
            } else {
                $product = (new Product())->findByBarcode($arItem['barcode']);
                if (!empty($product)) {
                    $passedOffers[] = $product->id;
                    $arOffers[] = [
                        'partner_shop_id' => $this->getPartnerShopId(),
                        'product_id' => $product->id,
                        'article' => $arItem['article'],
                        'barcode' => $arItem['barcode'],
                        'amount' => $arItem['amount'],
                        'price' => $arItem['price']
                    ];
                } else {
                    $skippedOffers[] = $arItem;
                    continue;
                }
            }
        }

        return $arOffers;
    }

    /**
     * Handle method for parsing importing file.
     *
     * @return void
     * @throws Exception
     */
    public function parse(): void
    {
        $reader = Reader::createFromPath(public_path('storage/'.$this->getFile()), 'r');
        $reader->setDelimiter($this->getDelimiter());
        $rows = iterator_to_array($reader->getRecords(), false);
        $this->setRows($rows);
    }

    /**
     * Return path to imported file
     *
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * Set file which be use for import
     *
     * @param  string  $file
     *
     * @return void
     */
    public function setFile(string $file): void
    {
        $this->file = $file;
    }

    /**
     * Return delimiter which bee used in import file
     *
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * Set delimiter which be use in imported file
     *
     * @param  string  $delimiter
     *
     * @return void
     */
    public function setDelimiter(string $delimiter): void
    {
        if (strlen($delimiter) === 1) {
            $this->delimiter = $delimiter;
        } else {
            $this->delimiter = ';';
        }
    }

    /**
     * Get parsed rows from imported file
     *
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     * Set parsed rows from imported file
     *
     * @param  array  $rows
     */
    public function setRows(array $rows): void
    {
        $this->rows = $rows;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param  array  $fields
     */
    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }

    /**
     * Return Id partner shop which bee use to import
     *
     * @return int
     */
    public function getPartnerShopId(): int
    {
        return $this->partnerShopId;
    }

    /**
     *  Set Id partner shop which be process importing be file
     *
     * @param  int  $id
     *
     * @return void
     */
    public function setPartnerShopId(int $id): void
    {
        $this->partnerShopId = $id;
    }
}
