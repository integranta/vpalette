<?php


namespace App\Services;


use App\User;
use Illuminate\Http\JsonResponse;

/**
 * Class NotificationWebService
 *
 * @package App\Services
 */
class NotificationWebService extends BaseService
{
    /**
     * Make all unread notifications as read.
     *
     * @return JsonResponse
     */
    public function markAllAsRead(): JsonResponse
    {
        auth()->user()->unreadNotifications->markAsRead();
        return $this->responseJson(
            false,
            200,
            __('notifications.all_mark_as_readed')
        );
    }

    /**
     * Make notification by ID as read
     *
     * @param  string  $id
     *
     * @return JsonResponse
     */
    public function markAsReadById(string $id): JsonResponse
    {
        $unReadNotification = auth()->user()->unreadNotifications->where('id', $id)->first();
        if ($unReadNotification) {
            $unReadNotification->markAsRead();
        }
        return $this->responseJson(
            false,
            200,
            __('notifications.notification_mark_as_readed')
        );
    }

    /**
     * Retrieve list all unread notifications
     *
     * @return JsonResponse
     */
    public function listAllUnReadNotifications(): JsonResponse
    {
        return $this->responseJson(
            false,
            200,
            __('notifications.list_not_read_success_retrieved'),
            [
                'unreadNotifications' => auth()->user()->unreadNotifications()->toArray()
            ]
        );
    }

    /**
     * Retrieve list all read notifications
     *
     * @return JsonResponse
     */
    public function listAllReadNotifications(): JsonResponse
    {
        return $this->responseJson(
            false,
            200,
            __('notifications.list_readed_success_retrieved'),
            [
                'readNotifications' => auth()->user()->readNotifications()->toArray()
            ]
        );
    }

    /**
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function listAllNotifications(int $id): JsonResponse
    {
        return $this->responseJson(
            false,
            200,
            __('notifications.list_all_success_retrived'),
            ['notifications' => $listNotifications = (new User)->find($id)->notifications()->get()->toArray()]
        );
    }

    /**
     * Remove single notification by id notification.
     *
     * @param  string  $id
     *
     * @return JsonResponse
     */
    public function removeNotificationById(string $id): JsonResponse
    {
        auth()->user()->notifications()->where('id', $id)->delete();
        return $this->responseJson(
            false,
            200,
            __('notifications.notification_success_delete'),
            ['notifications' => auth()->user()->notifications()->get()->toArray()]
        );
    }
}
