<?php


namespace App\Services\API\V1;


use App\Services\BaseService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Contracts\UserContract;

/**
 * Class UserApiService
 * @package App\Services\API\V1
 */
class UserApiService extends BaseService
{
    /** @var UserContract */
    private $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserContract $userRepo
     *
     * @return void
     */
    public function __construct(UserContract $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $users = $this->userRepository->listUsers();

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('messages.success_retrieve'),
            $users
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $user = $this->userRepository->createUser($request->all());

        if (!$user) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('messages.error_create')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_CREATED,
            __('messages.success_create'),
            $user
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $user = $this->userRepository->findUserById($id);

        if ($user === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('messages.not_found')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('messages_success_found'),
            $user
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $user = $this->userRepository->findUserById($id);

        if ($user === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('message.not_found')
            );
        }

        $user = $this->userRepository->updateUser($request->all(), $id);

        if (!$user) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('messages.error_update')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('messages.success_update'),
            $user
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $user = $this->userRepository->findUserById($id);

        if ($user === null) {
            return $this->responseJson(
                true,
                Response::HTTP_NOT_FOUND,
                __('messages.not_found')
            );
        }

        $user = $this->userRepository->deleteUser($id);

        if (!$user) {
            return $this->responseJson(
                true,
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __('messages.error_delete')
            );
        }

        return $this->responseJson(
            false,
            Response::HTTP_OK,
            __('messages.success_delete'),
            $user
        );
    }
}
