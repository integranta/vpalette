<?php


namespace App\Services;


use App\Imports\PartnerOffersImport;
use Excel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use League\Csv\Exception;
use Modules\ImportCatalog\Contracts\CsvDataContract;
use Modules\ImportCatalog\Contracts\ImportCatalogContract;
use Modules\Partner\Contracts\PartnerContract;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\PartnerShop\Contracts\PartnerShopContract;
use Modules\Product\Contracts\ProductContract;
use Modules\User\Contracts\UserContract;

/**
 * Class ImportService
 *
 * @package App\Services
 */
class ImportService
{
    /** @var ImportCatalogContract */
    private $importRepository;

    /** @var CsvDataContract */
    private $csvDataRepository;

    /** @var PartnerOfferContract */
    private $partnerOfferRepository;

    /** @var PartnerContract */
    private $partnerRepository;

    /** @var PartnerShopContract */
    private $partnerShopRepository;

    /** @var UserContract */
    private $userRepository;
	
	/**
	 * ImportService constructor.
	 *
	 * @param  ImportCatalogContract  $importRepo
	 * @param  CsvDataContract        $csvDataRepo
	 * @param  PartnerOfferContract   $partnerOfferRepo
	 * @param  PartnerContract        $partnerRepo
	 * @param  PartnerShopContract    $partnerShopRepo
	 * @param  UserContract           $userRepo
	 */
    public function __construct(
        ImportCatalogContract $importRepo,
        CsvDataContract $csvDataRepo,
        PartnerOfferContract $partnerOfferRepo,
        PartnerContract $partnerRepo,
        PartnerShopContract $partnerShopRepo,
        UserContract $userRepo
    ) {
        $this->importRepository = $importRepo;
        $this->csvDataRepository = $csvDataRepo;
        $this->partnerOfferRepository = $partnerOfferRepo;
        $this->partnerRepository = $partnerRepo;
        $this->partnerShopRepository = $partnerShopRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->importRepository->listImports();
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return $this->partnerRepository->listPartners();
    }

    /**
     * @param  Request  $request
     *
     * @return false|RedirectResponse|string
     * @throws Exception *@throws \JsonException
     */
    public function parse(Request $request)
    {
        $file = $request->file('csv_file');
        $partnerShopId = $request->get('partner_shop_id');
        $delimiter = $request->get('csv_file_delimiter');
        $partnerId = $this->partnerShopRepository->findPartnerShopById($partnerShopId);
        $importerBy = $this->partnerRepository->findPartnerById($partnerId->partner_id)->user;

        $parser = $request->has('header')
            ? new ImportExcelAdapter(new PartnerOffersImport($delimiter, $partnerShopId))
            : new ImportCsvAdapter();

        $parser->setDelimiter($delimiter);
        $parser->uploadFile($file);
        $parser->setPartnerShopId($partnerShopId);
        $parser->setImporterBy($importerBy);
        $parser->parse();

        $data = $parser->getRows();
        $csv_header_fields = [];

        if (count($data) > 0) {
            if ($request->has('header')) {
                foreach ($data[0] as $key => $value) {
                    $csv_header_fields[] = $key;
                }
            }

            $csvData = [
                'csv_filename' => $parser->getFile(),
                'csv_header' => $request->has('header'),
                'csv_data' => json_encode($data),
                'partner_shop_id' => $partnerShopId,
                'delimiter' => $parser->getDelimiter(),
                'importer' => $parser->getImportedBy()->id,
                'fields' => json_encode([])
            ];

            // Save settings import
            $csv_data_file = $this->csvDataRepository->createCsvData($csvData);

            return json_encode([
                'data' => $data,
                'csv_data_file' => $csv_data_file,
                'csv_header_fields' => $csv_header_fields
            ], JSON_PRETTY_PRINT, 512);
        }

        return redirect()->back();
    }

    /**
     * @param  Request  $request
     *
     * @return false|string
     */
    public function process(Request $request)
    {
        // Сохраняем настройки выбранных поле для следующих импортов файлов.
        $this->csvDataRepository->updateCsvData(
            ['fields' => json_encode($request->get('fields'), JSON_PRETTY_PRINT, 512)],
            $request->get('csv_data_file_id')
        );

        $data = $this->csvDataRepository->findCsvData($request->get('csv_data_file_id'));

        $csv_data = json_decode($data->csv_data, true, 512, JSON_PRETTY_PRINT);
        $successImported = [];
        $hasHeader = null;

        foreach ($csv_data as $key => $row) {
            $hasHeader = $data->csv_header ? true : false;

            if ($hasHeader) {
                $import = new PartnerOffersImport(
                    $data->delimiter,
                    $data->partner_shop_id
                );
                Excel::import($import, public_path('storage/'.$data->csv_filename));

                $successImported = $import->getSuccessImportedRows();
            } else {
                $import = new ImportCsvAdapter();
                $import->setDelimiter($data->delimiter);
                $import->setPartnerShopId($data->partner_shop_id);
                $import->setImporterBy($this->userRepository->findUserById($data->importer));
                $import->setFile($data->csv_filename);
                $import->setFields(json_decode($data->fields, true, 512, JSON_PRETTY_PRINT));

                $offers = $import->process();

                foreach ($offers as $offer) {
                    $successImported[] = $this->partnerOfferRepository->createOffer($offer);
                }
            }
        }

        return json_encode([
            'successImported' => $successImported,
            'hasHeader' => $hasHeader
        ], JSON_PRETTY_PRINT, 512);
    }
}
