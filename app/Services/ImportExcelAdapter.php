<?php


namespace App\Services;

use App\Contracts\ImportableExcelContract;
use App\User;
use Excel;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use InvalidArgumentException;

/**
 * Class ImportExcelAdapter
 *
 * @property object            $importable
 * @property string            $delimiter
 * @property array             $rows
 * @property File|UploadedFile $file
 * @property array             $successfulRows
 * @property int               $productId
 * @property int               $partnerShopId
 * @property User              $importerBy
 *
 * @package App\Services
 */
class ImportExcelAdapter implements ImportableExcelContract
{
    /** @var object */
    private $importable;

    /** @var string */
    private $delimiter;

    /** @var array */
    private $rows;

    /** @var File|UploadedFile */
    private $file;

    /** @var array */
    private $successfulRows;

    /** @var int */
    private $productId;

    /** @var int */
    private $partnerShopId;

    /** @var User */
    private $importerBy;

    /**
     * ImportExcelAdapter constructor.
     *
     * @param  object  $importable
     *
     * @return void
     *
     */
    public function __construct(object $importable)
    {
        $this->setImportable($importable);
    }

    /**
     * Return successful imported rows.
     *
     * @return array
     */
    public function getSuccessfulImportRows(): array
    {
        return $this->successfulRows;
    }

    /**
     * Set successful imported rows.
     *
     * @param  array  $rows
     *
     * @return void
     */
    public function setSuccessfulImportRows(array $rows): void
    {
        $this->successfulRows = $rows;
    }

    /**
     * Handle method for parsing importing file.
     *
     * @return void
     */
    public function parse(): void
    {
        $this->importable->setDelimiter($this->getDelimiter() ?? ';');
        $this->importable->setPartnerShopId($this->getPartnerShopId());
        $this->importable->setImporterBy($this->getImportedBy());
        $this->setRows(Excel::toArray($this->importable, $this->getFile())[0]);
    }

    /**
     * Return delimiter which bee used in import file
     *
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * Set delimiter which be use in imported file
     *
     * @param  string  $delimiter
     *
     * @return void
     */
    public function setDelimiter(string $delimiter): void
    {
        if (strlen($delimiter) === 1) {
            $this->delimiter = $delimiter;
        } else {
            throw new InvalidArgumentException("Length delimiter can't be more that 1 symbol");
        }
    }

    /**
     * Return Id partner shop which bee use to import
     *
     * @return int
     */
    public function getPartnerShopId(): int
    {
        return $this->partnerShopId;
    }

    /**
     * Set Id partner shop which be process importing be file
     *
     * @param  int  $id
     *
     * @return void
     */
    public function setPartnerShopId(int $id): void
    {
        $this->partnerShopId = $id;
    }

    /**
     * Return importer
     *
     * @return User
     */
    public function getImportedBy(): User
    {
        return $this->importerBy;
    }

    /**
     * Return path to imported file
     *
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * Set file which be use for import
     *
     * @param  string  $file
     *
     * @return void
     */
    public function setFile(string $file): void
    {
        $this->file = $file;
    }

    /**
     * Handle method for process import & create or update products
     *
     * @return void
     */
    public function process(): void
    {
        $this->importable->setDelimiter($this->getDelimiter() ?? ';');
        $this->importable->setPartnerShopId($this->getPartnerShopId());
        $this->importable->setImporterBy($this->getImportedBy());
    }

    /**
     * Get parsed rows from imported file
     *
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     * Set parsed rows from imported file
     *
     * @param  array  $rows
     */
    public function setRows(array $rows): void
    {
        $this->rows = $rows;
    }

    /**
     * Return Id find product to importing file
     *
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * Set Id product for which be set amount and price from importing file
     *
     * @param  int  $id
     *
     * @return void
     */
    public function setProductId(int $id): void
    {
        $this->productId = $id;
    }

    /**
     * Set importer which be use for notification to successful or failed import file
     *
     * @param  User  $importedBy
     *
     * @return void
     */
    public function setImporterBy(User $importedBy): void
    {
        $this->importerBy = $importedBy;
    }

    /**
     * Return object class importable
     *
     * @return object
     */
    public function getImportable(): object
    {
        return new $this->importable;
    }

    /**
     * Set class generated early via package Laravel-Excel class import
     *
     * @param  object  $importable
     *
     * @return void
     */
    public function setImportable(object $importable): void
    {
        $this->importable = $importable;
    }

    /**
     * Upload imported file to Storage
     *
     * @param  UploadedFile  $file
     *
     * @return void
     */
    public function uploadFile(UploadedFile $file): void
    {
        $fileName = $file->getClientOriginalName().'-'.date('j-Y-H-i').'.'.$file->getClientOriginalExtension();
        $this->setFile($file->storePubliclyAs('import', $fileName));
    }

    /**
     * Set success imported rows
     *
     * @param  array  $successfulRows
     *
     * @return void
     */
    public function setSuccessfulRows(array $successfulRows): void
    {
        $this->successfulRows = $successfulRows;
    }
}
