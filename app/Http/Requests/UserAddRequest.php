<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UserAddRequest
 *
 * @package App\Http\Requests
 */
class UserAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        if (Auth::check() && $user = Auth::user())
        {
            return Auth::check() && $user->can('create-users');
        }
        
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'email' => 'required|email|unique:users',
            'role' => 'required|exists:roles,id',
            'password' => 'required|min:5|confirmed'
        ];
    }
}
