<?php

namespace App\Http\Requests;

use Auth;
use Hash;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UserUpdateRequest
 *
 * @package App\Http\Requests
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'first_name' => 'required|max:255|min:3',
            'last_name'  => 'required|max:255|min:3',
            'email'      => 'required|email|unique:users,email,'.$this->user->id,
            'password'   => 'nullable|min:5|confirmed'
        ];
        
        $user = Auth::user();
        
        if ($user) {
            if (!$user->can('edit-users')) {
                $rules['current_password'] = 'required';
            } else {
                $rules['role'] = 'nullable|integer|exists:roles,id';
            }
        }
        
        return $rules;
    }
    
    /**
     * @param $validator
     */
    public function withValidator($validator): void
    {
        $user = Auth::user();
        
        if ($user) {
            if (!$user->can('edit-users')) {
                $validator->after(function ($validator) {
                    if (!Hash::check($this->current_password, $this->user->password)) {
                        $validator->errors()->add('current_password',
                            'Неверный пароль учетной записи. Настройки не могут быть обновлены!');
                    }
                });
            }
        }
    }
}
