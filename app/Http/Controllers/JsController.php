<?php

namespace App\Http\Controllers;

use Response;
use View;

/**
 * Class JsController
 *
 * @package App\Http\Controllers
 */
class JsController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function dynamic(): \Illuminate\Http\Response
    {
        $contents = View::make('admin.js.dynamic');
        $response = Response::make($contents, 200);
        $response->header('Content-Type', 'application/javascript');
        return $response;
    }
}
