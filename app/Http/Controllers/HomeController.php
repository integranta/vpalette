<?php

namespace App\Http\Controllers;


use Illuminate\Http\RedirectResponse;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return RedirectResponse
     */
    public function __invoke()
    {
        return redirect()->route('admin.dashboard.index');
    }
}
