<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Modules\Category\Models\Category;

class CategoryTreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = Category::with(['products', 'children', 'parent'])
            ->where('slug', $request['slug'])
            ->first();
            
        if (!$data) {
            return response()->json([
                'data' => null
            ]);
        }
        
        /** @var Category|Model $childrenCategories */
        $childrenCategories = $data->children()->get();
        
        $arData = null;
    
        /** @var Category|Model $children */
        foreach ($childrenCategories as $children)
        {
            foreach ($children->products as $product)
            {
                $arData[] = $product;
            }
        }
            
        return response()->json([
            'id' => $data->id,
            'active' => $data->active,
            'sort' => $data->sort,
            'name' => $data->name,
            'slug' => $data->slug,
            'seo_text' => $data->seo_text,
            'dropdown_text' => $data->dropdown_text,
            'full_path' => $data->full_path,
            'dropdown_image_src' => $data->dropdown_image_src,
            'menu_icon_src' => $data->menu_icon_src,
            'section_image_src' => $data->section_image_src,
            'parent_id' => $data->parent_id,
            'arProducts' => $arData
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
