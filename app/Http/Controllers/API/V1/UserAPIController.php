<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Services\API\V1\UserApiService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


/**
 * Class UserAPIController
 * @package App\Http\Controllers\API\V1
 */
class UserAPIController extends Controller
{
    /** @var UserApiService */
    private $userApiService;

    /**
     * UserAPIController constructor.
     *
     * @param UserApiService $userApiService
     */
    public function __construct(UserApiService $userApiService)
    {
        $this->userApiService = $userApiService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->userApiService->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        return $this->userApiService->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->userApiService->show($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        return $this->userApiService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
       return $this->userApiService->destroy($id);
    }
}
