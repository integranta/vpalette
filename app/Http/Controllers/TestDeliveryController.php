<?php

namespace App\Http\Controllers;

use App\Drivers\Cdek\Requests\AuthorizationRequest;
use App\Drivers\Cdek\Requests\GetListCitiesRequest;
use App\Drivers\Cdek\Requests\GetListDeliveryPointsRequest;
use App\Drivers\Cdek\Requests\GetListRegionsRequest;
use App\Drivers\Cdek\Requests\RegistrationOrderRequest;
use App\Drivers\Cdek\Types\City;
use App\Drivers\Cdek\Types\DeliveryOrder;
use App\Drivers\Cdek\Types\Order;
use App\Drivers\Cdek\Types\Region;
use App\Drivers\Cdek\Types\Seller;
use App\Drivers\CdekDeliveryDriver;
use App\Enums\DeliveriesTariffsCdek;
use CdekSDK\CdekClient;
use GuzzleHttp\Client;

/**
 * Class TestDeliveryController
 *
 * @package App\Http\Controllers
 */
class TestDeliveryController extends Controller
{
	private $client;
	
	public function __construct()
	{
		if (config('cdek.test_mode_enable')) {
			$this->client = new CdekClient(
				config('cdek.test_mode.account'),
				config('cdek.test_mode.password'),
				new Client([
					'base_uri' => config('cdek.test_mode.base_uri')
				])
			);
		}
		
		$this->client = new CdekClient(
			config('cdek.prod_mode.account'),
			config('cdek.prod_mode.password'),
			new Client([
				'base_uri' => config('cdek.prod_mode.base_uri')
			])
		);
	}
}
