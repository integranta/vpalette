<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения типов в чём выражается стоимость скидки.
 *
 * @method static static PERCENT()
 * @method static static FIXED()
 * @method static static SET_PRICE()
 */
final class DiscountAmountTypes extends Enum implements LocalizedEnum
{
    /** @var string */
    public const PERCENT = 'percent';
    
    /** @var string */
    public const FIXED = 'fixed';
    
    /** @var string */
    public const SET_PRICE = 'set_price';
}
