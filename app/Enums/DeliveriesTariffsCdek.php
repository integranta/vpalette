<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class DeliveriesTariffsCdek
 *
 * Константы хранящие коды тарифов для ИМ (Интернет-магазинов) используемые СДЭК'ом.
 *
 * @package App\Enums
 */
final class DeliveriesTariffsCdek extends Enum
{
	/** @var int Международный экспресс документы дверь-дверь */
    public const INTER_EXPRESS_DOCS_DOOR_DOOR = 7;
    
    /** @var int Международный экспресс грузы дверь-дверь */
    public const INTER_EXPRESS_CARGO_DOOR_DOOR = 8;
    
    /** @var int Посылка склад-склад */
    public const PARCEL_WAREHOUSE_WAREHOUSE = 136;
    
    /** @var int Посылка склад-дверь */
    public const PARCEL_WAREHOUSE_DOOR = 137;
    
    /** @var int Посылка дверь-склад */
    public const PARCEL_DOOR_WAREHOUSE = 138;
    
    /** @var int Посылка дверь-дверь */
    public const DOOR_DOOR_PACKAGE = 139;
    
    /** @var int Международный экспресс грузы склад-склад */
    public const INTER_EXPRESS_CARGO_WAREHOUSE_WAREHOUSE = 178;
	
    /** @var int Международный экспресс грузы склад-дверь */
	public const INTER_EXPRESS_CARGO_WAREHOUSE_DOOR = 179;
	
	/** @var int Международный экспресс грузы дверь-склад */
	public const INTER_EXPRESS_CARGO_DOOR_WAREHOUSE = 180;
	
	/** @var int Международный экспресс документы склад-склад */
	public const INTER_EXPRESS_DOCS_WAREHOUSE_WAREHOUSE = 181;
	
	/** @var int Международный экспресс документы склад-дверь */
	public const INTER_EXPRESS_DOCS_WAREHOUSE_DOOR = 182;
	
	/** @var int Международный экспресс документы дверь-склад */
	public const INTER_EXPRESS_DOCS_DOOR_WAREHOUSE = 183;
	
	/** @var int Экономичная посылка дверь-дверь */
	public const ECONOMICAL_PACKAGE_DOOR_DOOR = 231;
	
	/** @var int Экономичная посылка дверь-склад */
	public const ECONOMICAL_PACKAGE_DOOR_WAREHOUSE = 232;
	
	/** @var int Экономичная посылка склад-дверь */
	public const ECONOMICAL_PACKAGE_WAREHOUSE_DOOR = 233;
	
	/** @var int Экономичная посылка склад-склад */
	public const ECONOMICAL_PACKAGE_WAREHOUSE_WAREHOUSE = 234;
	
	/** @var int CDEK Express склад-склад */
	public const CDEK_EXPRESS_WAREHOUSE_WAREHOUSE = 291;
	
	/** @var int CDEK Express дверь-дверь */
	public const CDEK_EXPRESS_DOOR_DOOR = 293;
	
	/** @var int CDEK Express склад-дверь */
	public const CDEK_EXPRESS_WAREHOUSE_DOOR = 294;
	
	/** @var int CDEK Express дверь-склад */
	public const CDEK_EXPRESS_DOOR_WAREHOUSE = 295;
	
	/** @var int Китайский экспресс (склад-склад) */
	public const CHINA_EXPRESS_WAREHOUSE_WAREHOUSE = 243;
	
	/** @var int Китайский экспресс (дверь-дверь) */
	public const CHINA_EXPRESS_DOOR_DOOR = 245;
	
	/** @var int Китайский экспресс (склад-дверь) */
	public const CHINA_EXPRESS_WAREHOUSE_DOOR = 246;
	
	/** @var int Китайский экспресс (дверь-склад) */
	public const CHINA_EXPRESS_DOOR_WAREHOUSE = 247;
	
	/** @var int Экспресс лайт дверь-дверь */
	public const EXPRESS_LITE_DOOR_DOOR = 1;
	
	/** @var int Супер-экспресс до 18 */
	public const SUPER_EXPRESS_UP_18 = 3;
	
	/** @var int Экономичный экспресс склад-склад */
	public const ECONOMIC_EXPRESS_WAREHOUSE_WAREHOUSE = 5;
	
	/** @var int Экспресс лайт склад-склад */
	public const EXPRESS_LITE_WAREHOUSE_WAREHOUSE = 10;
	
	/** @var int Экспресс лайт склад-дверь */
	public const EXPRESS_LITE_WAREHOUSE_DOOR = 11;
	
	/** @var int Экспресс лайт дверь-склад */
	public const EXPRESS_LITE_DOOR_WAREHOUSE = 12;
	
	/** @var int Экспресс тяжеловесы склад-склад */
	public const EXPRESS_HEAVYWEIHTS_WAREHOUSE_WAREHOUSE = 15;
	
	/** @var int Экспресс тяжеловесы склад-дверь */
	public const EXPRESS_HEAVYWEIHTS_WAREHOUSE_DOOR = 16;
	
	/** @var int Экспресс тяжеловесы дверь-склад */
	public const EXPRESS_HEAVYWEIHTS_DOOR_WAREHOUSE = 17;
	
	/** @var int Экспресс тяжеловесы дверь-дверь */
	public const EXPRESS_HEAVYWEIHTS_DOOR_DOOR = 18;
	
	/** @var int Супер-экспресс до 9 */
	public const SUPER_EXPRESS_UP_9 = 57;
	
	/** @var int Супер-экспресс до 10 */
	public const SUPER_EXPRESS_UP_10 = 58;
	
	/** @var int Супер-экспресс до 12 */
	public const SUPER_EXPRESS_UP_12 = 59;
	
	/** @var int Супер-экспресс до 14 */
	public const SUPER_EXPRESS_UP_14 = 60;
	
	/** @var int Супер-экспресс до 16 */
	public const SUPER_EXPRESS_UP_16 = 61;
	
	/** @var int Магистральный экспресс склад-склад */
	public const MAGISTRAL_EXPRESS_WAREHOUSE_WAREHOUSE = 62;
	
	/** @var int Магистральный супер-экспресс склад-склад */
	public const MAGISTRAL_SUPER_EXPRESS_WAREHOUSE_WAREHOUSE = 63;
	
	/** @var int Экономичный экспресс дверь-дверь */
	public const ECONOMIC_EXPRESS_DOOR_DOOR = 118;
	
	/** @var int Экономичный экспресс склад-дверь */
	public const ECONOMIC_EXPRESS_WAREHOUSE_DOOR = 119;
	
	/** @var int Экономичный экспресс дверь-склад */
	public const ECONOMIC_EXPRESS_DOOR_WAREHOUSE = 120;
	
	/** @var int Магистральный экспресс дверь-дверь */
	public const MAGISTRAL_EXPRESS_DOOR_DOOR = 121;
	
	/** @var int Магистральный экспресс склад-дверь */
	public const MAGISTRAL_EXPRESS_WAREHOUSE_DOOR = 122;
	
	/** @var int Магистральный экспресс дверь-склад */
	public const MAGISTRAL_EXPRESS_DOOR_WAREHOUSE = 123;
	
	/** @var int Магистральный супер-экспресс дверь-дверь */
	public const MAGISTRAL_SUPER_EXPRESS_DOOR_DOOR = 124;
	
	/** @var int Магистральный супер-экспресс склад-дверь */
	public const MAGISTRAL_SUPER_EXPRESS_WAREHOUSE_DOOR = 125;
	
	/** @var int Магистральный супер-экспресс дверь-склад */
	public const MAGISTRAL_SUPER_EXPRESS_DOOR_WAREHOUSE = 126;
}
