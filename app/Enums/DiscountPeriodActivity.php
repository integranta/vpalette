<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения периодов активности скидки.
 *
 * @method static static NO_LIMIT()
 * @method static static INTERVAL()
 */
final class DiscountPeriodActivity extends Enum implements LocalizedEnum
{
    /** @var string Без ограничений */
    public const NO_LIMIT = 'no_limit';
    
    /** @var string Интервал */
    public const INTERVAL = 'interval';
}
