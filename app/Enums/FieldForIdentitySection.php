<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Class FieldForIdentitySection
 *
 * Константы для выбора значения идентификации разделов
 *
 * @package App\Enums
 */
final class FieldForIdentitySection extends Enum implements LocalizedEnum
{
   public const NAME = 'name';
   public const SLUG = 'slug';
   public const ID = 'id';
}
