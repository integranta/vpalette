<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Class FrontendTypeAttributes
 *
 * Константы названии для определения отображения аттрибута в фильтре.
 *
 * @package App\Enums
 */
final class FrontendTypeAttributes extends Enum implements LocalizedEnum
{
    /** @var string Список */
    public const SELECT = 'select';
    
    /** @var string Чекбокс */
    public const CHECKBOX = 'checkbox';
    
    /** @var string Переключатель */
    public const RADIO = 'radio';
    
    /** @var string Строка */
    public const TEXT = 'text';
    
    /** @var string Текст */
    public const TEXTAREA = 'textarea';
    
    /** @var string Ползунок (от-до) */
    public const RANGE = 'range';
    
    /** @var string Цвет */
    public const COLOR = 'color';
    
    /** @var string Файл */
    public const FILE = 'file';
    
    /** @var string Число */
    public const NUMBER = 'number';
    
    /** @var string E-mail */
    public const EMAIL = 'email';
    
    /** @var string Дата */
    public const DATE = 'date';
}
