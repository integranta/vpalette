<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения тип добавляемого товара.
 *
 * @method static static PORTAL_PRODUCT()
 * @method static static PARTNER_PRODUCT()
 */
final class ProductType extends Enum implements LocalizedEnum
{
    /** @var string Товар от портала */
    public const PORTAL_PRODUCT = 'portal_product';
    
    /** @var string Товар от партнёра */
    public const PARTNER_PRODUCT = 'partner_product';
}
