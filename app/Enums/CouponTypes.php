<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения типов купонов.
 *
 * @method static static SINGLE_ITEM_ORDER()
 * @method static static SINGLE_ORDER()
 * @method static static REUSABLE()
 */
final class CouponTypes extends Enum implements LocalizedEnum
{
    /** @var string На один товар из заказа (самый дорогой в корзине) */
    public const SINGLE_ITEM_ORDER = 'single_item_order';
    
    /** @var string На один заказ */
    public const SINGLE_ORDER = 'single_order';
    
    /** @var string Переиспользуемый */
    public const REUSABLE = 'reusable';
}
