<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения статусов состояния импорта.
 *
 * @method static static FAIL()
 * @method static static SUCCESS()
 * @method static static DRAFT()
 */
final class ImportStatus extends Enum implements LocalizedEnum
{
    /** @var string Провал */
    public const FAIL = 'fail';
    
    /** @var string Успех */
    public const SUCCESS = 'success';
    
    /** @var string Черновик */
    public const DRAFT = 'draft';
}
