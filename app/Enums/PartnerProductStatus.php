<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения статусов состояния заявки от партнёра, на добавление его предложенного товара.
 *
 * @method static static QUEUE()
 * @method static static ADDED()
 * @method static static EXIST()
 * @method static static DONT_MATCH_SUBJECT
 * @method static static FALSE_PRODUCT
 */
final class PartnerProductStatus extends Enum implements LocalizedEnum
{
    /** @var string В очереди */
    public const QUEUE = 'queue';
    
    /** @var string Добавлен */
    public const ADDED = 'added';
    
    /** @var string Данный товар уже есть на сайте */
    public const EXIST = 'exist';
    
    /** @var string Не соответствует тематике сайта */
    public const DONT_MATCH_SUBJECT = 'dont_match_subject';
    
    /** @var string Ложный товар */
    public const FALSE_PRODUCT = 'false_product';
}
