<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения статусов состояния E-mail-рассылки.
 *
 * @method static static DRAFT()
 * @method static static DELETED()
 * @method static static SENDED()
 */
final class NewsletterStatus extends Enum implements LocalizedEnum
{
    /** @var string Черновик */
    public const DRAFT = 'draft';
    
    /** @var string Удалённый */
    public const DELETED = 'deleted';
    
    /** @var string Отправлен */
    public const SENDED = 'sended';
}
