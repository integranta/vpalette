<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Class FieldForIdentityElement
 *
 * Список констант для выбора идентификации элемента при импорте.
 *
 * @package App\Enums
 */
final class FieldForIdentityElement extends Enum implements LocalizedEnum
{
    public const NAME = 'name';
    public const ID = 'id';
    public const SLUG = 'slug';
    public const CREATED_AT = 'created_at';
    public const DELETED_AT = 'deleted_at';
}
