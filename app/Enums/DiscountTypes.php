<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения типов скидки.
 *
 * @method static static DISCOUNT()
 * @method static static SALE()
 */
final class DiscountTypes extends Enum implements LocalizedEnum
{
    /** @var string Скидка */
    public const DISCOUNT = 'discount';
    
    /** @var string Распродажа */
    public const SALE = 'sale';
}
