<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class OrderTypesCdek
 *
 * Константы для хранения типов заказа используемые в СДЭК.
 *
 * @package App\Enums
 */
final class OrderTypesCdek extends Enum
{
	/** @var int Интернет-магазин (только для договора типа "Договор с ИМ") */
	public const INTERNET_SHOP = 1;
	
	/** @var int "Доставка" (для любого договора) */
	public const DELIVERY = 2;
}
