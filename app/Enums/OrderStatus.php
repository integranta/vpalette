<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения статусов состояния заказа.
 *
 * @method static PENDING()
 * @method static PROCESSING()
 * @method static DELIVERED()
 * @method static COMPLETED()
 * @method static DECLINE
 *
 * @package App\Enums
 *
 */
final class OrderStatus extends Enum implements LocalizedEnum
{
    /** @var string Ожидает */
    public const PENDING = 'pending';
    
    /** @var string Выполняется */
    public const PROCESSING = 'processing';
    
    /** @var string Доставлен */
    public const DELIVERED = 'delivered';
    
    /** @var string Выполнен */
    public const COMPLETED = 'completed';
    
    /** @var string Отменён */
    public const DECLINE = 'decline';
}
