<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения типов баннеров.
 *
 * @method static static SLIDER()
 * @method static static BIG()
 * @method static static SMALL()
 */
final class BannerType extends Enum implements LocalizedEnum
{
    /** @var string Баннер для слайдера */
    public const SLIDER = 'slider';
    
    /** @var string Большой квадратный баннер */
    public const BIG = 'big';
    
    /** @var string Маленький квадратный баннер */
    public const SMALL = 'small';
}
