<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static CASH()
 * @method static static CARD()
 * @method static static CASH_ON_DELIVERY()
 */
final class PaymentMethods extends Enum implements LocalizedEnum
{
    const CASH =   'cash';
    const CARD =   'card';
    const CASH_ON_DELIVERY = 'cash_on_delivery';
}
