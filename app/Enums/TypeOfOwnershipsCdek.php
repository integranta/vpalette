<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class TypeOfOwnershipsCdek
 *
 * Константы хранящие коды форм собственности для ИМ (Интернет-магазинов) используемые СДЭК'ом.
 *
 * @package App\Enums
 */
final class TypeOfOwnershipsCdek extends Enum
{
	/** @var int Акционерное общество */
	public const JOINT_STOCK_COMPANY = 9;
	
	/** @var int Закрытое акционерное общество */
	public const CLOSED_JOINT_STOCK_COMPANY = 61;
	
	/** @var int Индивидуальный предприниматель */
	public const INDIVIDUAL_ENTREPRENEUR = 63;
	
	/** @var int Открытое акционерное общество */
	public const PUBLIC_CORPORATION = 119;
	
	/** @var int Общество с ограниченной ответственностью */
	public const LIMITED_LIABILITY_COMPANY = 137;
	
	/** @var int Публичное акционерное общество */
	public const PUBLIC_JOINT_STOCK_COMPANY = 147;
}
