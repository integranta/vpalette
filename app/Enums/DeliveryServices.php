<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static SELF_PICKUP()
 * @method static static CDEK()
 * @method static static BOXBERRY()
 */
final class DeliveryServices extends Enum implements LocalizedEnum
{
	const SELF_PICKUP = 'self_pickup';
	const CDEK = 'cdek';
	const BOXBERRY = 'boxberry';
}
