<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class AdditionalServicesCdek
 *
 * Константы хранящие коды дополнительных услуг для ИМ (Интернет-магазинов) используемые СДЭК'ом.
 *
 * @package App\Enums
 */
final class AdditionalServicesCdek extends Enum
{
	/** @var string Доставка в выходной день */
	public const DELIV_WEEKEND = 'DELIV_WEEKEND';
	
	/** @var string Забор в городе отправителя */
	public const TAKE_SENDER = 'TAKE_SENDER';
	
	/** @var string Доставка в городе получателя */
	public const DELIV_RECEIVER = 'DELIV_RECEIVER';
	
	/** @var string Примерка на дому */
	public const TRYING_ON = 'TRYING_ON';
	
	/** @var string Частичная доставка */
	public const PART_DELIV = 'PART_DELIV';
	
	/** @var string Осмотр вложения */
	public const INSPECTION_CARGO = 'INSPECTION_CARGO';
	
	/** @var string Реверс */
	public const REVERSE = 'REVERSE';
	
	/** @var string Опасный груз */
	public const DANGER_CARGO = 'DANGER_CARGO';
	
	/** @var string Упаковка 1 - Коробка размером 310*215*280 мм - 30 руб (для грузов до 10 кг) */
	public const PACKAGE_1 = 'PACKAGE_1';
}
