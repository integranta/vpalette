<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Константы для хранения статусов состояния платежа.
 *
 * @method static static NOT_PAID()
 * @method static static PAID()
 */
final class PaymentStatus extends Enum implements LocalizedEnum
{
    /** @var string Не оплачен */
    public const NOT_PAID = 'no_paid';
    
    /** @var string Оплачен */
    public const PAID = 'paid';
}
