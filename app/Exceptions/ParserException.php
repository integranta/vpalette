<?php

namespace App\Exceptions;

use Exception;
use Throwable;

/**
 * Class ParserException
 * @package App\Exceptions
 */
class ParserException extends Exception
{
    /**
     * ParserException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Class not extend from abstract driver class Parser', 500);
    }
}
