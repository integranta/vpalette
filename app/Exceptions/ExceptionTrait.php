<?php


namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * Trait ExceptionTrait
 * @package App\Exceptions
 */
trait ExceptionTrait
{
    /**
     * @param Request $request
     * @param Throwable $e
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function apiException(Request $request, Throwable $e): JsonResponse
    {

        if ($this->isModel($e))
        {
            return $this->ModelResponse($e);
        }

        if ($this->isHttp($e))
        {
            return $this->HttpResponse($e);
        }

        return parent::render($request, $e);

    }

    /**
     * @param Throwable $e
     * @return bool
     */
    private function isModel(Throwable $e): bool
    {
        return $e instanceof ModelNotFoundException;
    }

    /**
     * @param Throwable $e
     * @return JsonResponse
     */
    private function ModelResponse(Throwable $e): JsonResponse
    {
        return response()->json([
            'errors' => 'Model not found'
        ],Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Throwable $e
     * @return bool
     */
    private function isHttp(Throwable $e): bool
    {
        return $e instanceof NotFoundHttpException;
    }

    /**
     * @param Throwable $e
     * @return JsonResponse
     */
    private function HttpResponse(Throwable $e): JsonResponse
    {
        return response()->json([
            'errors' => 'Incorect route'
        ],Response::HTTP_NOT_FOUND);
    }

    /**
     * @param Throwable $exception
     * @return JsonResponse
     */
    private function ModelBannerResponse(Throwable $exception): JsonResponse
    {
        return response()->json([
            'errors' => 'Banner model not found'
        ],Response::HTTP_NOT_FOUND);
    }
}
