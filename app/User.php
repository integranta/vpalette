<?php

namespace App;

use App\Models\LinkedSocialAccount;
use App\Traits\UserTrait;
use Exception;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Joselfonseca\LighthouseGraphQLPassport\HasLoggedInTokens;
use Joselfonseca\LighthouseGraphQLPassport\HasSocialLogin;
use Laravel\Passport\HasApiTokens;
use Modules\Coupon\Models\Coupon;
use Modules\Discount\Models\Discount;
use Modules\Faq\Models\Faq;
use Modules\News\Models\News;
use Modules\Order\Models\Order;
use Modules\Partner\Models\Partner;
use Modules\Rating\Models\Rating;
use Modules\Reviews\Models\Review;
use Modules\Wishlist\Models\Wishlist;
use Rennokki\QueryCache\Traits\QueryCacheable;
use Spatie\Permission\Traits\HasRoles;


/**
 * Class User
 *
 * @package App
 * @method static create(array $getUserFields)
 * @property int $id
 * @property int $active
 * @property int $sort
 * @property string|null $name
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $emailVerifiedAt
 * @property string|null $password
 * @property string|null $phone
 * @property string $firstName
 * @property string $lastName
 * @property string|null $rememberToken
 * @property \Illuminate\Support\Carbon|null $createdAt
 * @property \Illuminate\Support\Carbon|null $updatedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clientsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Coupon\Models\Coupon[] $coupons
 * @property-read int|null $couponsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Discount\Models\Discount[] $discounts
 * @property-read int|null $discountsCount
 * @property-read array $allPermissions
 * @property-read string $avatarlink
 * @property-read bool $isme
 * @property-read string $profilelink
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LinkedSocialAccount[] $linkedSocialAccounts
 * @property-read int|null $linkedSocialAccountsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\News\Models\News[] $news
 * @property-read int|null $newsCount
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notificationsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Order\Models\Order[] $orders
 * @property-read int|null $ordersCount
 * @property-read \Modules\Partner\Models\Partner|null $partner
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissionsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Faq\Models\Faq[] $questions
 * @property-read int|null $questionsCount
 * @property-read \Modules\Rating\Models\Rating|null $ratings
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Reviews\Models\Review[] $reviews
 * @property-read int|null $reviewsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $rolesCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokensCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Wishlist\Models\Wishlist[] $wishlists
 * @property-read int|null $wishlistsCount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use UserTrait;
    use HasApiTokens;
    use HasSocialLogin;
    use HasLoggedInTokens;
    use QueryCacheable;
	
	/**
	 * Invalidate the cache automatically
	 * upon update in the database.
	 *
	 * @var bool
	 */
	protected static $flushCacheOnUpdate = true;
	
	public $cacheFor = 3600; // cache time, in seconds

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active',
        'sort',
        'first_name',
        'last_name',
        'name',
        'email',
        'password',
        'phone'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'allPermissions',
        'profilelink',
        'avatarlink',
        'isme'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Return name all permissions.
     *
     * @return array
     */
    public function getAllPermissionsAttribute(): array
    {
        $res = [];
        try {
            $allPermissions = $this->getAllPermissions();
            foreach ($allPermissions as $p) {
                $res[] = $p->name;
            }
        } catch (Exception $e) {
        }

        return $res;
    }

    /**
     * @return HasOne
     */
    public function partner(): HasOne
    {
        return $this->hasOne(Partner::class);
    }

    /**
     * @return HasMany
     */
    public function wishlists(): HasMany
    {
        return $this->hasMany(Wishlist::class);
    }

    /**
     * @return HasMany
     */
    public function news(): HasMany
    {
        return $this->hasMany(News::class);
    }

    /**
     * @return HasMany
     */
    public function linkedSocialAccounts(): HasMany
    {
        return $this->hasMany(LinkedSocialAccount::class);
    }

    /**
     * @return HasMany
     */
    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    /**
     * @return HasMany
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Faq::class);
    }

    /**
     * @return MorphOne
     */
    public function ratings(): MorphOne
    {
        return $this->morphOne(Rating::class, 'author');
    }

    /**
     * @return HasMany
     */
    public function coupons(): HasMany
    {
        return $this->hasMany(Coupon::class);
    }

    /**
     * @return BelongsToMany
     */
    public function discounts(): BelongsToMany
    {
        return $this->belongsToMany(Discount::class, 'user_discount');
    }

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
