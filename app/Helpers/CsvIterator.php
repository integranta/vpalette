<?php


namespace App\Helpers;


use Illuminate\Support\Str;

/**
 * Class CsvIterator
 *
 * @package App\Helpers
 */
class CsvIterator
{
    public $file;
    
    public $delimiter;
    
    public function __construct($file, $delimiter)
	{
		$this->file = fopen($file, 'rb');
		$this->delimiter = $delimiter;
	}
	
	/**
	 * @return \Generator
	 * @throws \Exception
	 */
	public function parse() {
    	$header = array_map(static function ($column) {
    		return Str::of($column)->trim()->slug('_')->__toString();
		}, fgetcsv($this->file, 4096, $this->delimiter));
    	
    	while (!feof($this->file)) {
    		$row = array_map(static function ($column) {
    			return htmlspecialchars_decode(Str::of($column)->trim()->__toString());
			}, (array) fgetcsv($this->file, 4096, $this->delimiter));
    		if (count($header) !== count($row)) {
    			throw new \Exception('Count column in header dont match with count column in row');
			}
    		$row = array_combine($header, $row);
    		yield $row;
		}
	}
	
	/**
	 * @param  \Iterator  $iterator
	 * @param  int        $size
	 *
	 * @return \Generator
	 */
	public function chunk(\Iterator $iterator, int $size)
	{
		$chunk = [];
		
		for ($i = 0; $iterator->valid(); $i++) {
			$chunk[] = $iterator->current();
			$iterator->next();
			if (count($chunk) === $size) {
				yield $chunk;
				$chunk = [];
			}
		}
		
		if (count($chunk)) {
			yield $chunk;
		}
	}
}