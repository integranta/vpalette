<?php


namespace App\Contracts;


/**
 * Interface DeliveryServiceContract
 *
 * @package App\Contracts
 */
interface DeliveryServiceContract
{
	/**
	 * Получить список населенных пунктов.
	 *
	 * @param  array  $params
	 *
	 * @return array|null
	 */
  	public function getListCities(array $params): ?array;
	
	
	/**
	 * Получить список регионов.
	 *
	 * @param  array  $params
	 *
	 * @return array|null
	 */
  	public function getListRegions(array $params): ?array;
}