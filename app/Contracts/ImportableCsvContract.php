<?php


namespace App\Contracts;

use App\User;

/**
 * Interface ImportableCsvContract
 *
 * @package App\Interfaces
 */
interface ImportableCsvContract
{
    /**
     * Получить успешно импортируемые строки.
     *
     * @return array
     */
    public function getSuccessfulImportRows(): array;
    
    /**
     * Добавить успешно импортируемые строки.
     *
     * @param  array  $rows
     */
    public function setSuccessfulImportRows(array $rows): void;
    
    /**
     * Обработчик считывания файла.
     *
     * @return void
     */
    public function parse(): void;
    
    /**
     * Обработчик импорта.
     *
     * @return array
     */
    public function process(): array;
    
    /**
     * Добавить считанные строки из импортируемого файла.
     *
     * @param  array  $rows
     */
    public function setRows(array $rows): void;
    
    /**
     * Установить разделитель используемый в файле.
     *
     * @param  string  $delimiter
     */
    public function setDelimiter(string $delimiter): void;
    
    /**
     * Получить символ разделитель, используемый в файле.
     *
     * @return string
     */
    public function getDelimiter(): string;
    
    /**
     * Установить ID партнёрского магазина, от которого будет происходить выгрузка остатков.
     *
     * @param  int  $id
     */
    public function setPartnerShopId(int $id): void;
    
    /**
     * Получить ID партнёрского магазина, от которого происходит выгрузка остатков.
     *
     * @return int
     */
    public function getPartnerShopId(): int;
    
    /**
     * Установить ID товара портала, для которого выгружаются остатки.
     *
     * @param  int  $id
     */
    public function setProductId(int $id): void;
    
    /**
     * Получить ID товара портала, для которого выгружаются остатки.
     *
     * @return int
     */
    public function getProductId(): int;
    
    /**
     * Получить пользователя - импортёра.
     *
     * @return User
     */
    public function getImportedBy(): User;
    
    /**
     * Установить пользователя - импортёра.
     *
     * @param  User  $importedBy
     */
    public function setImporterBy(User $importedBy): void;
    
}
