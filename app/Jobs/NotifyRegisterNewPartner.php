<?php

namespace App\Jobs;

use App\Notifications\RegisterNewPartner;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Partner\Models\Partner;

/**
 * Class NotifyRegisterNewPartner
 *
 * @package App\Jobs
 */
class NotifyRegisterNewPartner implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var User */
    public $user;

    /** @var Partner */
    public $partner;

    /**
     * Create a new job instance.
     *
     * @param  User     $user
     * @param  Partner  $partner
     */
    public function __construct(User $user, Partner $partner)
    {
        $this->user = $user;
        $this->partner = $partner;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->user->notify(new RegisterNewPartner($this->partner));
    }
}
