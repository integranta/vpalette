<?php

namespace App\Jobs;

use App\Notifications\RegisterNewPartnerShop;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\PartnerShop\Models\PartnerShop;

/**
 * Class NotifyRegisterNewPartnerShop
 *
 * @package App\Jobs
 */
class NotifyRegisterNewPartnerShop implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var User */
    public $user;

    /** @var PartnerShop */
    public $partnerShop;

    /**
     * Create a new job instance.
     *
     * @param  User         $user
     * @param  PartnerShop  $partnerShop
     */
    public function __construct(User $user, PartnerShop $partnerShop)
    {
        $this->user = $user;
        $this->partnerShop = $partnerShop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->user->notify(new RegisterNewPartnerShop($this->partnerShop));
    }
}
