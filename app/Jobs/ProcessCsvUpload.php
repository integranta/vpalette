<?php

namespace App\Jobs;

use App\Enums\ProductType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use Image;
use Modules\Brand\Models\Brand;
use Modules\Category\Models\Category;
use Modules\Product\Models\Product;
use Modules\Product\Models\ProductImage;
use Redis;

/**
 * Class ProcessCsvUpload
 *
 * @package App\Jobs
 */
class ProcessCsvUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	
	
	private $file;
	
	/**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file)
    {
		$this->file = $file;
	}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		
		
  
  
		Redis::throttle('upload-csv')
			->allow(1)
			->every(20)
			->then(function () {
			
			
		}, function () {
			// Unable to obtain lock...
			return $this->release(10);
		});
    }
	
	
}
