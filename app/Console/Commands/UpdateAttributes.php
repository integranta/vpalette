<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateAttributes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vpalette:update-attributes {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновляет товары, устанавливает значения атрибутов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
		$start = now();
		$this->output->title('Starting import');
		(new \App\Imports\UpdateAttributes())
			->withOutput($this->output)
			->import(base_path($this->argument('file')));
		$this->output->success('Import successful');
		$time = $start->diffInSeconds(now());
		$this->output->title("Processed in $time seconds");
    }
}
