<?php

namespace App\Console\Commands;

use Artisan;
use Cache;
use Illuminate\Support\Facades\File;
use Storage;
use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vpalette:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Установить все миграции и выгрузить все подготовленные данные.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
    	$this->info('Начал очистку кэша');
    	Cache::clear();
    	$this->info('Очистил кэш приложения');
    	
        $this->info('Начал копировать подготовленные картинки и текста.');
        if (File::missing(storage_path('app/public/uploads/faker'))) {
			File::makeDirectory(storage_path('app/public/uploads/faker'));
		}
        
        if (File::exists(storage_path('app/public/uploads/faker'))) {
			File::copyDirectory(public_path('faker'), storage_path('app/public/uploads/faker'));
		}
        $this->info('Скопировал подготовленные картинки и текста.');

        $this->info('Начал миграцию модулей');
        Artisan::call('module:migrate');
        $this->info('Закончил миграцию модулей');

        $this->info('Начал миграцию остальных таблиц');
        Artisan::call('migrate');
        $this->info('Закончил миграцию остальных таблиц');

        $this->info('Начал выгрузку подготовленных данных');
        Artisan::call('db:seed');
        $this->info('Закончил выгрузку подготовленных данных');

        $this->info('Всё готов !');
    }
}
