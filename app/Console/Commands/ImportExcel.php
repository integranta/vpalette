<?php

namespace App\Console\Commands;

use App\Imports\ImportCatalog;
use Illuminate\Console\Command;

class ImportExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vpalette:import-excel {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call import xlsx file via package Laravel Excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
    	$start = now();
		$this->output->title('Starting import');
		(new ImportCatalog)
			->withOutput($this->output)
			->import(base_path($this->argument('file')));
		$this->output->success('Import successful');
		$time = $start->diffInSeconds(now());
		$this->output->title("Processed in $time seconds");
    }
}
