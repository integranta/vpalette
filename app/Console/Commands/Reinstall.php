<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;
use Storage;

class Reinstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vpalette:reinstall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удаляет все ранее загруженные файлы и записи, запускает заново процесс установки.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
		$this->info('Начал очистку кэша');
		Artisan::call('optimize:clear');
		$this->info('Очистил кэш приложения');
    	
    	if (Storage::disk('uploads')->exists('banners')) {
			Storage::disk('uploads')->deleteDirectory('banners');
			$this->info('Удалил папку banners');
		}
		
    	if (Storage::disk('uploads')->exists('brands')) {
			Storage::disk('uploads')->deleteDirectory('brands');
			$this->info('Удалил папку brands');
		}
		
    	if (Storage::disk('uploads')->exists('categories')) {
			Storage::disk('uploads')->deleteDirectory('categories');
			$this->info('Удалил папку categories');
		}
		
    	
    	if (Storage::disk('uploads')->exists('news')) {
			Storage::disk('uploads')->deleteDirectory('news');
			$this->info('Удалил папку news');
		}
		
        if (Storage::disk('uploads')->exists('stocks')) {
			Storage::disk('uploads')->deleteDirectory('stocks');
			$this->info('Удалил папку stocks');
		}
	
		if (Storage::disk('uploads')->exists('products')) {
			Storage::disk('uploads')->deleteDirectory('products');
			$this->info('Удалил папку products');
		}

        $this->info('Начал откат миграции модулей');
        Artisan::call('module:migrate-rollback');
        $this->info('Сделал откат миграции модулей');

        $this->info('Запустил миграции модулей');
        Artisan::call('module:migrate');
        $this->info('Сделал миграцию модулей');

        $this->info('Начал выгрузку подготовленных данных');
        Artisan::call('db:seed');
        $this->info('Сделал выгрузку');
	
		if (!Storage::disk('uploads')->exists('products')) {
			Storage::disk('uploads')->makeDirectory('products');
			$this->info('Создал папку products');
		}
	
		$this->info('Начал кэширование данных');
		Artisan::call('optimize');
		$this->info('Закончил кэширование приложения');

        $this->info('Всё готово !');

    }
}
