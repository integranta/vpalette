<?php

namespace App\Imports;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Faq\Models\Faq;

/**
 * Class ImportFaq
 *
 * @package App\Imports
 */
class ImportFaq implements ToModel, WithHeadingRow
{
	use Importable;
	
	/**
	 * @param  array  $row
	 *
	 * @return Model|Model[]|null|Faq
	 */
	public function model(array $row)
	{
		return new Faq([
			'active' => true,
			'sort' => $row['sort'],
			'question' => $row['question'],
			'answer' => $row['answer']
		]);
	}
	
	public function headingRow(): int
	{
		return 1;
	}
}
