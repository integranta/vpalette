<?php

namespace App\Imports;

use Illuminate\Console\OutputStyle;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Row;
use Modules\Attribute\Models\Attribute;
use Modules\Attribute\Models\AttributeValue;
use Modules\Product\Models\Product;

/**
 * Class UpdateAttributes
 *
 * @package App\Imports
 */
class UpdateAttributes implements
	WithHeadingRow,
	OnEachRow,
	WithChunkReading,
	WithBatchInserts,
	WithProgressBar
{
	use Importable;
	
	/** @var array массив атрибутов */
	private $arProps = [];
	public $keys = [];
	public $values = [];
	
	public function onRow(Row $row): void
	{
		$_row = $row->toArray();
		
		if (isset($_row['arprops'])) {
			$this->setArProps($_row['arprops']);
		}
		
		$attributes = [];
		foreach ($this->getArProps() as $propName => $propValue) {
			if ($propName === 'Время, когда стала новинкой') {
				continue;
			}
			if ($propName === 'Картинки') {
				continue;
			}
			
			if ($propName === 'Количество комментариев к элементу') {
				continue;
			}
			
			if ($propName === 'Тема форума для комментариев') {
				continue;
			}
			
			$_prop = Attribute::whereName($propName)->first();
			if ($_prop !== null) {
				$attributes[$_prop->id] = $propValue;
			} else {
				$attributes[$propName] = $propValue;
			}
		}
		
		$product = Product::where('article', '=', $_row['article'], 'or')
			->where('barcode', '=', $_row['barcode'], 'or')
			->where('name', '=', $_row['name'])
			->first();
			
		if ($product !== null && count($attributes) > 0) {
			$product->load('attributes');
			
			foreach ($attributes as $attribute_id => $attributeValue) {
				$product->attributes()->create([
					'attribute_id' => $attribute_id,
					'product_id'   => $product->id,
					'value'        => $attributeValue
				]);
			}
		}
	}
	
	public function batchSize(): int
	{
		return 100;
	}
	
	public function chunkSize(): int
	{
		return 100;
	}
	
	/**
	 * @return array
	 */
	public function getArProps(): array
	{
		return $this->arProps;
	}
	
	/**
	 * Указываем какая строка является заголовка
	 *
	 * По умолчанию начинается с 1.
	 *
	 * @return int
	 */
	public function headingRow(): int
	{
		return 1;
	}
	
	/**
	 * @param  string  $arProps
	 */
	public function setArProps(string $arProps): void
	{
		$_arProps = Arr::collapse(Str::of($arProps)
			->trim()
			->explode(';')
			->map(function ($prop) {
				return Str::of($prop)
					->trim()
					->explode(': ')
					->toArray();
			})
			->toArray());
		
		foreach ($_arProps as $key => $arProp) {
			if ($key % 2 == 0) {
				$this->keys[] = $arProp;
				continue;
			}
			$this->values[] = $arProp;
		}
		
		$this->arProps = array_combine($this->keys, $this->values);
	}
}
