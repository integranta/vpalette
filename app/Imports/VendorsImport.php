<?php

namespace App\Imports;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Image;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Modules\Brand\Models\Brand;
use Storage;
use Str;

/**
 * Class VendorsImport
 *
 * @package App\Imports
 */
class VendorsImport implements ToModel, WithHeadingRow
{
	use Importable;
	
	public const VPALITRE_BASE_URL = 'https://vpalitre.ru';
	
	/**
	 * @param  array  $row
	 *
	 * @return \Illuminate\Database\Eloquent\Model|null|Brand
	 */
	public function model(array $row)
	{
		$slug = Str::of($row['name'])->lower()->trim()->slug('_', 'ru');
		
		$path = 'app/public/uploads/brands/'.$slug;
		
		if (!File::exists(storage_path($path))) {
			File::makeDirectory(storage_path($path));
		}
		
		$url = self::VPALITRE_BASE_URL.$row['logo'];
		
		
		$pathToSave = storage_path($path).'/'.Str::of(Str::random().'.jpg')->lower();
		
		
		$imageSrc = 'brands/'.$slug.'/';
		$contents = Image::make($url)->save($pathToSave, 95);
		$logoSrc = null;
		$logoSrc .= $imageSrc.$contents->basename;
		
		$arUploadedFiles = null;
		
		if ($row['file'] !== null) {
			$arFiles = explode(';', $row['file']);
			
			$fileSrc = 'brands/'.$slug.'/vendor_files';
			$arUploadedFiles = [];
			foreach ($arFiles as $key => $file) {
				$fileName = Str::of(Str::random().'.pdf')->lower();
				$pathToVendorFile = $fileSrc.'/'.$fileName;
				$response = Http::get(self::VPALITRE_BASE_URL.$file);
				$content = $response->body();
				Storage::disk('uploads')->put($pathToVendorFile, $content);
				$arUploadedFiles[$key] = $pathToVendorFile;
			}
		}
		
		$arBrand = [
			'active'       => $row['active'] === 'Y',
			'sort'         => (int) $row['sort'],
			'name'         => $row['name'],
			'slug'         => $slug,
			'logo'         => $logoSrc,
			'description'  => $row['description'],
			'url'          => null,
			'file'         => $arUploadedFiles,
			'preview_file' => null
		];
		
		return new Brand($arBrand);
	}
	
	public function headingRow(): int
	{
		return 1;
	}
}
