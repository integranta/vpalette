<?php


namespace App\Imports;


use App\Enums\ProductType;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use Image;
use Modules\Brand\Models\Brand;
use Modules\Category\Models\Category;
use Modules\Product\Models\Product;
use Modules\Product\Models\ProductImage;

/**
 * Class CatalogImport
 *
 * @package App\Imports
 */
final class CatalogImport
{
	/** @var string Базовый URL старого сайта ВПалитре */
	public const VPALITRE_BASE_URL = 'https://vpalitre.ru';
	
	/** @var string Базовый путь до картинок */
	public const FAKER_BASE_PATH_IMAGES = 'faker/products/from_site';
	
	/** @var int ID корневого раздела */
	public const SECTION_ROOT_ID = 1;
	
	/** @var string путь из названии разделов */
	public $sections;
	
	/** @var array массив из найденных/созданных разделов */
	public $categories = [];
	
	/** @var array массив из найденных/созданных ID разделов */
	public $categoryIds = [];
	
	public $images = [];
	
	private $successImportable = [];
	
	public function setUp(): void
	{
		$path = resource_path('pending-files/*.csv');
		$files = glob($path);
		
		foreach (array_slice($files, 0, 1) as $file) {
			$data = array_map(static function ($row) {
				return str_getcsv($row, ';');
			}, file($file));
			
			foreach ($data as $row) {
				
				$header = $data[0];
				
				
				dd(__METHOD__, $header);
				
				$this->sections = $row[17];
				
				$this->prepareImages($row);
				$this->getSections();
				$this->getCategoryIds();
				$brand = $this->getOrCreateBrand($row[42]);
				
				$productFields = [
					'article'      => $row[35],
					'barcode'      => empty($row[52]) ? null : $row[52],
					'name'         => $row[1],
					'sort'         => $row[7],
					'type_product' => ProductType::PORTAL_PRODUCT,
					'is_new'       => empty($row[27]) ? false : true,
					'weight'       => $row[18],
					'is_popular'   => false,
					'active'       => trim($row[6]) === 'Y',
					'sections'     => json_encode($this->categoryIds),
					'brand_id'     => $brand->id
				];
				
				
				$seoFields = [
					'title'          => $row[14],
					'description'    => $row[15],
					'keywords'       => $row[16],
					'og_title'       => $row[14],
					'og_description' => $row[15]
				];
				
				
				$images = null;
				
				if ($this->images !== null) {
					foreach ($this->images as $image) {
						$images[] = ProductImage::firstOrCreate($image);
					}
					$this->images = null;
				}
				
				$product = Product::firstOrCreate($productFields);
				$product->load(['seo', 'images', 'categories']);
				$product->seo()->firstOrCreate($seoFields);
				
				$categories = null;
				
				if (is_array($this->categories)) {
					$categories = $this->categories;
				} else {
					$categories[] = $this->categories;
				}
				
				$product->categories()->saveMany($categories);
				if ($images !== null) {
					$product->images()->saveMany($images);
					$images = null;
				}
				$this->setSuccessImportable($product->toArray());
			}
			
			unlink($file);
		}
	}
	
	/**
	 * Подготовляем картинки
	 *
	 * @param  array  $row
	 */
	private function prepareImages(array $row): void
	{
		
		$slug = $this->getSlug($row[1]);
		$path = Str::of('app\\public\\uploads\\products\\')
			->trim()
			->append($slug);
		
		if (!File::exists(storage_path($path))) {
			File::makeDirectory(storage_path($path));
		}
		
		$morePhotos = Str::of($row[39])->trim();
		$previewPicture = Str::of($row[2])
			->trim()
			->start(self::VPALITRE_BASE_URL);
		$detailPicture = Str::of($row[4])
			->trim()
			->start(self::VPALITRE_BASE_URL);
		
		if ($morePhotos->contains(';')) {
			
			$images = $morePhotos->explode(';')
				->map(static function ($image) {
					return Str::of($image)->trim()->start(self::VPALITRE_BASE_URL);
				})
				->toArray();
			
			$this->uploadImages($images, $path, $slug);
		} elseif ($previewPicture->isNotEmpty()) {
			$images[] = $previewPicture;
		} elseif ($detailPicture->isNotEmpty()) {
			$images[] = $detailPicture;
		} else {
			$images = null;
		}
	}
	
	/**
	 * Получить сгенерированный slug из названия.
	 *
	 * @param  string  $name
	 *
	 * @return \Illuminate\Support\Stringable
	 */
	private function getSlug(string $name): Stringable
	{
		return Str::of($name)->lower()->trim()->slug('_', 'ru');
	}
	
	/**
	 * Сохраняет подготовленные картинки.
	 *
	 * @param  array   $images
	 * @param  string  $path
	 * @param  string  $slug
	 */
	private function uploadImages(array $images, string $path, string $slug): void
	{
		foreach ($images as $image) {
			$ext = Str::of($image)->afterLast('.');
			
			$fileName = Str::of(Str::random())
				->append('.', $ext)
				->lower();
			
			$pathToSave = Str::of($path)
				->append('\\', $fileName);
			
			$content = base64_encode(Http::get($image)->body());
			
			$_image = Image::make($content)
				->encode($ext)
				->save(storage_path($pathToSave), 90, $ext);
			
			$this->images[]['full'] = Str::of('products/')
				->append($slug, '/', $_image->basename);
		}
	}
	
	/**
	 * Находит существующие разделы или создает новые.
	 *
	 * @return void
	 */
	public function getSections(): void
	{
		$categoryNames = $this->sections;
		$parentId = self::SECTION_ROOT_ID;
		
		if (Str::of($categoryNames)->contains(';')) {
			$categoryNames = Str::of($categoryNames)
				->trim()
				->explode(';')
				->map(static function ($item) {
					if (Str::of($item)->contains('/')) {
						return Str::of($item)
							->trim()
							->explode('/')
							->map(static function ($subItem) {
								return Str::of($subItem)
									->trim();
							});
					}
					
					return Str::of($item)->trim();
				})
				->toArray();
			
			foreach ($categoryNames as $key => $categories) {
				$parentId = self::SECTION_ROOT_ID;
				$_category = null;
				foreach ($categories as $index => $categoryName) {
					
					$slug = $this->getSlug($categoryName);
					
					$category = Category::whereParentId($parentId)
						->whereSlug($slug)
						->first();
					
					if ($category === null) {
						$category = Category::create([
							'parent_id' => $parentId,
							'name'      => $categoryName
						]);
					}
					
					$_category = $category;
					$parentId = $category->id;
				}
				
				$this->categories[$key] = $_category;
			}
		} elseif (Str::of($categoryNames)->contains('/')) {
			$categoryNames = Str::of($categoryNames)
				->trim()
				->explode('/')
				->map(static function ($item) {
					return Str::of($item)->trim();
				})
				->toArray();
			
			foreach ($categoryNames as $key => $categoryName) {
				
				$slug = $this->getSlug($categoryName);
				
				$category = Category::whereParentId($parentId)
					->whereSlug($slug)
					->first();
				
				if ($category === null) {
					$category = Category::create([
						'parent_id' => $parentId,
						'name'      => $categoryName
					]);
				} else {
					$parentId = $category->id;
				}
				
				$this->categories = $category;
			}
		} else {
			$category = Category::firstOrCreate(
				[
					'parent_id' => self::SECTION_ROOT_ID,
					'slug'      => $this->getSlug($categoryNames)
				],
				[
					'parent_id' => self::SECTION_ROOT_ID,
					'name'      => $categoryNames
				]);
			
			$this->categories = $category;
		}
	}
	
	public function getCategoryIds(): void
	{
		$this->categoryIds = null;
		if (is_array($this->categories)) {
			foreach ($this->categories as $category) {
				$this->categoryIds[] = $category['id'];
			}
		} else {
			/** @var Category $_category */
			$_category = $this->categories;
			$this->categoryIds[] = $_category->id;
		}
	}
	
	/**
	 * Вернёт или создаст бренд.
	 *
	 * @param $vendor
	 *
	 * @return \Illuminate\Database\Eloquent\Model|\Modules\Brand\Models\Brand
	 */
	private function getOrCreateBrand(string $vendor): Brand
	{
		$brandName = Str::of($vendor)->trim();
		
		if ($brandName->contains(',')) {
			$brandName = $brandName->before(',');
		}
		
		return Brand::firstOrCreate([
			'active' => true,
			'name'   => $brandName,
			'sort'   => 500
		]);
	}
	
	public function toModel(array $rows): void
	{
		foreach ($rows as $row) {
			$this->sections = $row[17];
			
			$this->prepareImages($row);
			$this->getSections();
			$this->getCategoryIds();
			$brand = $this->getOrCreateBrand($row[42]);
			
			$productFields = [
				'article'      => $row[35],
				'barcode'      => empty($row[52]) ? null : $row[52],
				'name'         => $row[1],
				'sort'         => (int) $row[7],
				'type_product' => ProductType::PORTAL_PRODUCT,
				'is_new'       => empty($row[27]) ? false : true,
				'weight'       => (float) $row[18],
				'is_popular'   => false,
				'active'       => trim($row[6]) === 'Y',
				'sections'     => json_encode($this->categoryIds),
				'brand_id'     => $brand->id
			];
			
			
			$seoFields = [
				'title'          => $row[14],
				'description'    => $row[15],
				'keywords'       => $row[16],
				'og_title'       => $row[14],
				'og_description' => $row[15]
			];
			
			
			$images = null;
			
			if ($this->images !== null) {
				foreach ($this->images as $image) {
					$images[] = ProductImage::firstOrCreate($image);
				}
				$this->images = null;
			}
			
			$product = Product::firstOrCreate([
				'slug' => Str::of($row[1])->lower()->trim()->slug('_', 'ru')
			], $productFields);
			$product->load(['seo', 'images', 'categories']);
			$product->seo()->firstOrCreate($seoFields);
			
			$categories = null;
			
			if (is_array($this->categories)) {
				$categories = $this->categories;
			} else {
				$categories[] = $this->categories;
			}
			
			$product->categories()->saveMany($categories);
			if ($images !== null) {
				$product->images()->saveMany($images);
				$images = null;
			}
			$this->setSuccessImportable($product->toArray());
		}
	}
	
	/**
	 * Получить массив успешно импортированных строк
	 *
	 * @return array
	 */
	public function getSuccessImportable(): array
	{
		return $this->successImportable;
	}
	
	/**
	 * Добавить в массив успеншо импортированные строки
	 *
	 * @param  array  $successImportable
	 */
	public function setSuccessImportable(array $successImportable): void
	{
		$this->successImportable[] = $successImportable;
	}
}