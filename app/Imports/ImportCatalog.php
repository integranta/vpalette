<?php

namespace App\Imports;

use App\Enums\ProductType;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Stringable;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Row;
use Modules\Brand\Models\Brand;
use Modules\Category\Models\Category;
use Modules\Product\Models\Product;
use Modules\Product\Models\ProductImage;
use Storage;
use Str;

/**
 * Class ImportCatalog
 *
 * @package App\Imports
 */
class ImportCatalog implements
	WithHeadingRow,
	OnEachRow,
	WithChunkReading,
	WithBatchInserts,
	WithProgressBar
{
	use Importable;
	
	/** @var string Базовый URL старого сайта ВПалитре */
	public const VPALITRE_BASE_URL = 'https://vpalitre.ru';
	
	/** @var string Базовый путь до картинок */
	public const FAKER_BASE_PATH_IMAGES = 'faker/products/from_site';
	
	/** @var int ID корневого раздела */
	public const SECTION_ROOT_ID = 1;
	
	/** @var string путь из названии разделов */
	public $sections;
	
	/** @var array массив из найденных/созданных разделов */
	public $categories = [];
	
	/** @var array массив из найденных/созданных ID разделов */
	public $categoryIds = [];
	
	public $images = [];
	
	private $successImportable = [];
	
	/**
	 * Указываем какая строка является заголовка
	 *
	 * По умолчанию начинается с 1.
	 *
	 * @return int
	 */
	public function headingRow(): int
	{
		return 1;
	}
	
	/**
	 * Получить массив успешно импортированных строк
	 *
	 * @return array
	 */
	public function getSuccessImportable(): array
	{
		return $this->successImportable;
	}
	
	/**
	 * Добавить в массив успеншо импортированные строки
	 *
	 * @param  array  $successImportable
	 */
	public function setSuccessImportable(array $successImportable): void
	{
		$this->successImportable[] = $successImportable;
	}
	
	/**
	 * @return int
	 */
	public function chunkSize(): int
	{
		return 1000;
	}
	
	/**
	 * @return int
	 */
	public function batchSize(): int
	{
		return 100;
	}
	
	/**
	 * @inheritDoc
	 */
	public function onRow(Row $row): void
	{
		$_row = $row->toArray();
		$this->sections = $_row['sections'];
		
		$this->prepareImages($_row);
		$this->prepareCategories($this->sections);
		$this->getCategoryIds();
		$brand = $this->getOrCreateBrand($_row['vendor']);
		
		
		$productFields = [
			'article'      => $_row['article'],
			'barcode'      => empty($_row['barcode']) ? null : $_row['barcode'],
			'name'         => $_row['name'],
			'sort'         => $_row['sort'],
			'type_product' => ProductType::PORTAL_PRODUCT,
			'is_new'       => empty($_row['is_new']) ? false : true,
			'weight'       => $_row['weight'] ?: 0.00,
			'is_popular'   => false,
			'active'       => trim($_row['active']) === 'Y',
			'sections'     => json_encode($this->categoryIds),
			'brand_id'     => $brand->id ?? null
		];
		
		
		$seoFields = [
			'title'          => $_row['seo_meta_title'],
			'description'    => $_row['seo_meta_description'],
			'keywords'       => $_row['seo_meta_keywords'],
			'og_title'       => $_row['seo_meta_title'],
			'og_description' => $_row['seo_meta_description']
		];
		
		
		$images = null;
		
		if ($this->images !== null) {
			foreach ($this->images as $image) {
				$images[] = ProductImage::firstOrCreate($image);
			}
			$this->images = null;
		}
		
		$product = Product::firstOrCreate($productFields);
		$product->load(['seo', 'images', 'categories']);
		$product->seo()->firstOrCreate($seoFields);
		
		$categories = null;
		
		if (is_array($this->categories)) {
			$categories = $this->categories;
		} else {
			$categories[] = $this->categories;
		}
		
		$product->categories()->saveMany($categories);
		if ($images !== null) {
			$product->images()->saveMany($images);
			$images = null;
		}
		$this->setSuccessImportable($product->toArray());
	}
	
	/**
	 * Подготовляем картинки
	 *
	 * @param  array  $row
	 */
	private function prepareImages(array $row): void
	{
		$slug = $this->getSlug($row['name']);
		$path = Str::of('app/public/uploads/products/')
			->trim()
			->append($slug);
		
		$pathToSave = Str::of('products/')->append($slug);
		
		if (File::exists(storage_path($path)) === false) {
			File::makeDirectory(storage_path($path));
		}
		
		$morePhotos = Str::of($row['more_photos'])->trim();
		$previewPicture = Str::of($row['preview_image'])
			->trim()
			->start(self::VPALITRE_BASE_URL);
		$detailPicture = Str::of($row['detail_image'])
			->trim()
			->start(self::VPALITRE_BASE_URL);
		
		if ($morePhotos->contains(';')) {
			
			$images = $morePhotos->explode(';')
				->map(static function ($image) {
					return Str::of($image)->trim()->start(self::VPALITRE_BASE_URL);
				})
				->toArray();
			foreach ($images as $image) {
				$this->saveImageFromURL($image, $pathToSave);
			}
		} elseif ($previewPicture->isNotEmpty()) {
			$this->saveImageFromURL($previewPicture, $pathToSave);
		} elseif ($detailPicture->isNotEmpty()) {
			$this->saveImageFromURL($detailPicture, $pathToSave);
		} else {
			$images = null;
		}
	}
	
	/**
	 * Получить сгенерированный slug из названия.
	 *
	 * @param  string  $name
	 *
	 * @return \Illuminate\Support\Stringable
	 */
	private function getSlug(string $name): Stringable
	{
		return Str::of($name)->lower()->trim()->slug('_', 'ru');
	}
	
	/**
	 * Сохраняет подготовленные картинки.
	 *
	 * @param  string   $uri
	 * @param  string  $path
	 *
	 * @return void
	 */
	private function saveImageFromURL(string $uri, string $path): void
	{
		$ext = Str::of($uri)->afterLast('.');
		
		if ($ext->contains(['jpg', 'png', 'gif', 'jpeg'])) {
			$fileName = Str::of(Str::random())
				->append('.', $ext)
				->lower();
			
			$pathToSave = Str::of($path)->append('/', $fileName);
			
			$content = base64_encode(Http::get($uri)->body());
			
			Storage::disk('uploads')->put($pathToSave, base64_decode($content));
			
			$this->images[]['full'] = $pathToSave;
		}
	}
	
	public function prepareCategories(string $sections): void
	{
		$categories = null;
		$parentId = self::SECTION_ROOT_ID;
		
		if (Str::of($sections)->contains(';')) {
			$categories = Str::of($sections)
				->trim()
				->explode(';')
				->map(static function ($section) {
					if (Str::of($section)->contains('/')) {
						return Str::of($section)
							->trim()
							->explode('/')
							->map(static function ($subSection) {
								return Str::of($subSection)->trim();
							});
					}
					return Str::of($section)->trim();
				})
				->toArray();
			
			$arCategories = null;
			
			foreach ($categories as $category) {
				$_category = null;
				$parentId = self::SECTION_ROOT_ID;
				
				if (is_array($category)) {
					foreach ($category as $item) {
						$ct = Category::whereName($item)
							->whereParentId($parentId)
							->orderBy('id', 'asc')
							->first();
						
						if ($ct === null) {
							$ct = $this->createNewCategory($item, $parentId);
						}
						
						$parentId = $ct->id;
						$_category = $ct;
					}
					$arCategories[] = $_category;
				} else {
					$ct = $this->getLastCategory($category, $parentId);
					
					if ($ct === null) {
						$ct = $this->createNewCategory($category, $parentId);
					}
					
					$arCategories[] = $ct;
				}
			}
			
			$this->categories = $arCategories;
		} elseif (Str::of($sections)->contains('/')) {
			$categories = Str::of($sections)
				->trim()
				->explode('/')
				->map(static function ($section) {
					return Str::of($section)->trim();
				})
				->toArray();
			
			foreach ($categories as $category) {
				
				$ct = $this->getLastCategory($category, $parentId);
				
				if ($ct === null) {
					$ct = $this->createNewCategory($category, $parentId);
				}
				$parentId = $ct->id;
				$this->categories = $ct;
			}
		} else {
			$categories = Str::of($sections)->trim();
			
			$ct = $this->getLastCategory($categories, $parentId);
			
			if ($ct === null) {
				$ct = $this->createNewCategory($categories, $parentId);
			}
			
			$this->categories = $ct;
		}
	}
	
	/**
	 * @param       $item
	 * @param  int  $parentId
	 *
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|\Modules\Category\Models\Category|object|null
	 */
	private function getLastCategory($item, int $parentId)
	{
		return Category::whereName($item)
			->whereParentId($parentId)
			->orderByDesc('id')
			->first();
	}
	
	public function getCategoryIds(): void
	{
		$this->categoryIds = [];
		if (is_array($this->categories)) {
			foreach ($this->categories as $category) {
				$this->categoryIds[] = $category['id'];
			}
		} else {
			/** @var Category $_category */
			$_category = $this->categories;
			$this->categoryIds[] = $_category->id;
		}
	}
	
	/**
	 * Вернёт или создаст бренд.
	 *
	 * @param  string|null  $vendor
	 *
	 * @return \Illuminate\Database\Eloquent\Model|\Modules\Brand\Models\Brand|null
	 */
	private function getOrCreateBrand(?string $vendor): ?Brand
	{
		if ($vendor === null) {
			return null;
		}
		
		$brandName = Str::of($vendor)->trim();
		
		if ($brandName->contains(',')) {
			$brandName = $brandName->before(',');
		}
		
		return Brand::firstOrCreate([
			'active' => true,
			'name'   => $brandName,
			'sort'   => 500
		]);
	}
	
	/**
	 * @param $item
	 * @param $parentId
	 *
	 * @return \Illuminate\Database\Eloquent\Model|\Modules\Category\Models\Category|null
	 */
	private function createNewCategory($item, $parentId)
	{
		return Category::create([
			'active'             => true,
			'sort'               => 500,
			'name'               => $item,
			'dropdown_text'      => null,
			'seo_text'           => null,
			'section_image_src'  => null,
			'menu_icon_src'      => null,
			'dropdown_image_src' => null,
			'parent_id'          => $parentId,
		])->fresh();
	}
}
