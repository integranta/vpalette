<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\{Importable,
    SkipsFailures,
    SkipsOnFailure,
    ToModel,
    WithBatchInserts,
    WithChunkReading,
    WithCustomCsvSettings,
    WithHeadingRow,
    WithValidation};
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Validators\Failure;
use Modules\ImportCatalog\Notifications\ImportHasFailedNotification;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\Product\Models\Product;

/**
 * Class PartnerOffersImport
 *
 * @package App\Imports
 */
class PartnerOffersImport implements
    ToModel,
    WithHeadingRow,
    WithValidation,
    WithCustomCsvSettings,
    WithChunkReading,
    WithBatchInserts,
    SkipsOnFailure
{
    use SkipsFailures;
    use Importable;

    /** @var string */
    public $delimiter = ',';
    /** @var int */
    private $partnerShopId;
    /** @var int */
    private $productId;
    /** @var int */
    private $rows = 0;
    /** @var array */
    private $successImportedRows = [];
    /** @var User */
    private $importerBy;

    /**
     * PartnerOffersImport constructor.
     *
     * @param  string  $delimiter
     * @param  int     $partnerShopId
     */
    public function __construct(string $delimiter, int $partnerShopId)
    {
        $this->delimiter = $delimiter;
        $this->partnerShopId = $partnerShopId;
    }

    /**
     * @return User
     */
    public function getImporterBy(): User
    {
        return $this->importerBy;
    }

    /**
     * @param  User  $importerBy
     */
    public function setImporterBy(User $importerBy): void
    {
        $this->importerBy = $importerBy;
    }

    /**
     * Return count success imported rows.
     *
     * @return int
     */
    public function getRowCount(): int
    {
        return $this->rows;
    }

    /**
     * Return array success imported rows.
     *
     * @return array
     */
    public function getSuccessImportedRows(): array
    {
        return $this->successImportedRows;
    }

    /**
     * @param  array  $successImportedRows
     *
     * @return void
     */
    public function setSuccessImportedRows(array $successImportedRows): void
    {
        $this->successImportedRows = $successImportedRows;
    }

    /**
     * @param  array  $row
     *
     * @return PartnerOffer
     */
    public function model(array $row): PartnerOffer
    {
        ++$this->rows;

        $data = [
            'article' => $row['article'],
            'barcode' => $row['barcode'],
            'amount' => $row['amount'],
            'price' => $row['price'],
            'product' => $this->getProductNameById($this->getProductId())
        ];

        $this->setSuccessImportedRows($data);

        return new PartnerOffer([
            'article' => $row['article'],
            'barcode' => $row['barcode'],
            'amount' => $row['amount'],
            'price' => $row['price'],
            'product_id' => $this->getProductId(),
            'partner_shop_id' => $this->getPartnerShopId()
        ]);
    }

    /**
     * Return name product for current imported item.
     *
     * @param  int  $id
     *
     * @return string
     */
    public function getProductNameById(int $id): string
    {
        return Product::find($id)->name;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param  int  $productId
     */
    public function setProductId(int $productId): void
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getPartnerShopId(): int
    {
        return $this->partnerShopId;
    }

    /**
     * @param  int  $partnerShopId
     *
     * @return void
     */
    public function setPartnerShopId(int $partnerShopId): void
    {
        $this->partnerShopId = $partnerShopId;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            '*.article' => function ($attribute, $value, $onFailure) {
                if ($product = Product::where('article', '=', $value)->first()) {
                    $this->setProductId($product->id);
                } else {
                    $onFailure('Product with this article not found');
                }
            },
            '*.barcode' => function ($attribute, $value, $onFailure) {
                if ($product = Product::where('barcode', '=', $value)->first()) {
                    $this->setProductId($product->id);
                } else {
                    $onFailure('Product with this barcode not found');
                }
            }
        ];
    }

    /**
     * @param  Failure[]  $failures
     *
     * @return Failure
     */
    public function onFailure(Failure ...$failures): Failure
    {
        // TODO: Implement onFailure() method.
    }

    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => $this->getDelimiter()
        ];
    }

    /**
     * @return string
     */
    public function getDelimiter(): string
    {
        return $this->delimiter;
    }

    /**
     * @param  string  $delimiter
     *
     * @return void
     */
    public function setDelimiter(string $delimiter): void
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function () {
                $this->importerBy->notify(new ImportHasFailedNotification);
            },
        ];
    }
}
