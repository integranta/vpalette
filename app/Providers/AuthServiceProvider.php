<?php

namespace App\Providers;

use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use Modules\Advantages\Models\Advantage;
use Modules\Advantages\Policies\AdvantagesPolicy;
use Modules\Attribute\Models\Attribute;
use Modules\Attribute\Policies\AttributePolicy;
use Modules\Banner\Models\Banner;
use Modules\Banner\Policies\BannerPolicy;
use Modules\Brand\Models\Brand;
use Modules\Brand\Policies\BrandPolicy;
use Modules\Category\Models\Category;
use Modules\Category\Policies\CategoryPolicy;
use Modules\Core\Models\Setting;
use Modules\Core\Policies\CorePolicy;
use Modules\Faq\Models\Faq;
use Modules\Faq\Policies\FaqPolicy;
use Modules\News\Models\News;
use Modules\News\Policies\NewsPolicy;
use Modules\Newsletters\Models\Newsletter;
use Modules\Newsletters\Policies\NewslettersPolicy;
use Modules\Page\Models\Page;
use Modules\Page\Policies\PagePolicy;
use Modules\Partner\Models\Partner;
use Modules\Partner\Policies\PartnerPolicy;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerOffer\Policies\PartnerOfferPolicy;
use Modules\PartnerShop\Models\PartnerShop;
use Modules\PartnerShop\Policies\PartnerShopPolicy;
use Modules\Product\Models\Product;
use Modules\Product\Policies\ProductPolicy;
use Modules\Reviews\Models\Review;
use Modules\Reviews\Policies\ReviewPolicy;
use Modules\Seo\Models\Seo;
use Modules\Seo\Policies\SeoPolicy;
use Modules\Stock\Models\Stock;
use Modules\Stock\Policies\StockPolicy;
use Modules\Subscriber\Models\Subscriber;
use Modules\Subscriber\Policies\SubscriberPolicy;

/**
 * Class AuthServiceProvider
 *
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Advantage::class => AdvantagesPolicy::class,
        Attribute::class => AttributePolicy::class,
        Banner::class => BannerPolicy::class,
        Brand::class => BrandPolicy::class,
        Category::class => CategoryPolicy::class,
        Setting::class => CorePolicy::class,
        Faq::class => FaqPolicy::class,
        News::class => NewsPolicy::class,
        Newsletter::class => NewslettersPolicy::class,
        Page::class => PagePolicy::class,
        Partner::class => PartnerPolicy::class,
        PartnerOffer::class => PartnerOfferPolicy::class,
        PartnerShop::class => PartnerShopPolicy::class,
        Product::class => ProductPolicy::class,
        Review::class => ReviewPolicy::class,
        Seo::class => SeoPolicy::class,
        Stock::class => StockPolicy::class,
        Subscriber::class => SubscriberPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::withCookieSerialization();

        Gate::before(static function ($user, $ability) {
            return $user->hasRole('Admin');
        });
    }
}
