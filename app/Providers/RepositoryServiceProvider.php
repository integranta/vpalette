<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Ads\Contracts\AdsContract;
use Modules\Ads\Repositories\AdsRepository;
use Modules\Advantages\Contracts\AdvantagesContract;
use Modules\Advantages\Repositories\AdvantagesRepository;
use Modules\Analytics\Contracts\ViewContract;
use Modules\Analytics\Repositories\ViewRepository;
use Modules\Attribute\Contracts\AttributeContract;
use Modules\Attribute\Contracts\AttributeValueContract;
use Modules\Attribute\Repositories\AttributeRepository;
use Modules\Attribute\Repositories\AttributeValueRepository;
use Modules\Banner\Contracts\BannerContract;
use Modules\Banner\Repositories\BannerRepository;
use Modules\Brand\Contracts\BrandContract;
use Modules\Brand\Repositories\BrandRepository;
use Modules\Cart\Contracts\CartContract;
use Modules\Cart\Contracts\CartCookieContract;
use Modules\Cart\Repositories\CartCookieRepository;
use Modules\Cart\Repositories\CartRepository;
use Modules\Category\Contracts\CategoryContract;
use Modules\Category\Repositories\CategoryRepository;
use Modules\Coupon\Contracts\CouponContract;
use Modules\Coupon\Repositories\CouponRepository;
use Modules\Delivery\Contracts\DeliveryContract;
use Modules\Delivery\Repositories\DeliveryRepository;
use Modules\Discount\Contracts\DiscountContract;
use Modules\Discount\Contracts\DiscountPartnerContract;
use Modules\Discount\Repositories\DiscountPartnerRepository;
use Modules\Discount\Repositories\DiscountRepository;
use Modules\Faq\Contracts\FaqContract;
use Modules\Faq\Repositories\FaqRepository;
use Modules\Finance\Contracts\FinanceContract;
use Modules\Finance\Repositories\FinanceRepository;
use Modules\Import\Contracts\ImportContract;
use Modules\Import\Contracts\ImportProfileContract;
use Modules\Import\Repositories\ImportProfileRepository;
use Modules\Import\Repositories\ImportRepository;
use Modules\ImportCatalog\Contracts\CsvDataContract;
use Modules\ImportCatalog\Repositories\CsvDataRepository;
use Modules\News\Contracts\NewsContract;
use Modules\News\Repositories\NewsRepository;
use Modules\Newsletters\Contracts\NewslettersContract;
use Modules\Newsletters\Repositories\NewslettersRepository;
use Modules\Order\Contracts\OrderContract;
use Modules\Order\Repositories\OrderRepository;
use Modules\Page\Contracts\PageContract;
use Modules\Page\Repositories\PageRepository;
use Modules\Partner\Contracts\PartnerContract;
use Modules\Partner\Repositories\PartnerRepository;
use Modules\PartnerOffer\Contracts\PartnerOfferContract;
use Modules\PartnerOffer\Repositories\PartnerOfferRepository;
use Modules\PartnerProduct\Contracts\PartnerProductContract;
use Modules\PartnerProduct\Contracts\PortalPartnerProductContract;
use Modules\PartnerProduct\Repositories\PartnerProductRepository;
use Modules\PartnerProduct\Repositories\PortalPartnerProductRepository;
use Modules\PartnerShop\Contracts\PartnerShopContract;
use Modules\PartnerShop\Repositories\PartnerShopRepository;
use Modules\Payment\Contracts\PaymentContract;
use Modules\Payment\Repositories\PaymentRepository;
use Modules\Product\Contracts\ProductAttributeContract;
use Modules\Product\Contracts\ProductContract;
use Modules\Product\Repositories\ProductAttributeRepository;
use Modules\Product\Repositories\ProductRepository;
use Modules\Reviews\Contracts\ReviewContract;
use Modules\Reviews\Repositories\ReviewRepository;
use Modules\Seo\Contracts\SeoContract;
use Modules\Seo\Repositories\SeoRepository;
use Modules\Stock\Contracts\StockContract;
use Modules\Stock\Repositories\StockRepository;
use Modules\Subscriber\Contracts\SubscriberContract;
use Modules\Subscriber\Repositories\SubscriberRepository;
use Modules\User\Contracts\UserContract;
use Modules\User\Repositories\UserRepository;
use Modules\Wishlist\Contracts\WishlistContract;
use Modules\Wishlist\Repositories\WishlistRepository;

/**
 * Class RepositoryServiceProvider
 *
 * @package App\Providers
 */
class RepositoryServiceProvider extends ServiceProvider
{
	/**
	 * @var array
	 */
	protected $repositories = [
		AdsContract::class                  => AdsRepository::class,
		BrandContract::class                => BrandRepository::class,
		BannerContract::class               => BannerRepository::class,
		ViewContract::class                 => ViewRepository::class,
		AttributeContract::class            => AttributeRepository::class,
		AttributeValueContract::class       => AttributeValueRepository::class,
		CategoryContract::class             => CategoryRepository::class,
		DeliveryContract::class             => DeliveryRepository::class,
		DiscountContract::class             => DiscountRepository::class,
		FaqContract::class                  => FaqRepository::class,
		FinanceContract::class              => FinanceRepository::class,
		NewsContract::class                 => NewsRepository::class,
		OrderContract::class                => OrderRepository::class,
		PartnerContract::class              => PartnerRepository::class,
		PartnerOfferContract::class         => PartnerOfferRepository::class,
		PaymentContract::class              => PaymentRepository::class,
		ProductContract::class              => ProductRepository::class,
		ReviewContract::class               => ReviewRepository::class,
		SeoContract::class                  => SeoRepository::class,
		StockContract::class                => StockRepository::class,
		AdvantagesContract::class           => AdvantagesRepository::class,
		PageContract::class                 => PageRepository::class,
		SubscriberContract::class           => SubscriberRepository::class,
		UserContract::class                 => UserRepository::class,
		NewslettersContract::class          => NewslettersRepository::class,
		PartnerShopContract::class          => PartnerShopRepository::class,
		ImportContract::class               => ImportRepository::class,
		ImportProfileContract::class        => ImportProfileRepository::class,
		CsvDataContract::class              => CsvDataRepository::class,
		CouponContract::class               => CouponRepository::class,
		WishlistContract::class             => WishlistRepository::class,
		CartContract::class                 => CartRepository::class,
		CartCookieContract::class           => CartCookieRepository::class,
		ProductAttributeContract::class     => ProductAttributeRepository::class,
		PartnerProductContract::class       => PartnerProductRepository::class,
		PortalPartnerProductContract::class => PortalPartnerProductRepository::class,
		DiscountPartnerContract::class      => DiscountPartnerRepository::class
	];
	
	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register(): void
	{
		foreach ($this->repositories as $interface => $implementation) {
			$this->app->bind($interface,
				$implementation);
		}
	}
	
	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot(): void
	{
		//
	}
}
