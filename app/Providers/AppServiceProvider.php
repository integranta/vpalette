<?php

namespace App\Providers;


use App\Resolvers\SocialUserResolver;
use App\User;
use Carbon\Carbon;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Lang;
use Modules\Advantages\Models\Advantage;
use Modules\Advantages\Observers\AdvantageObserver;
use Modules\Attribute\Models\Attribute;
use Modules\Attribute\Models\AttributeValue;
use Modules\Attribute\Observers\AttributeObserver;
use Modules\Attribute\Observers\AttributeValueObserver;
use Modules\Banner\Models\Banner;
use Modules\Banner\Observers\BannerObserver;
use Modules\Brand\Models\Brand;
use Modules\Brand\Observers\BrandObserver;
use Modules\Category\Models\Category;
use Modules\Category\Observers\CategoryObserver;
use Modules\Discount\Models\Discount;
use Modules\Discount\Observers\DiscountObserver;
use Modules\Discount\Observers\DiscountPartnerObserver;
use Modules\Faq\Models\Faq;
use Modules\Faq\Observers\FaqObserver;
use Modules\News\Models\News;
use Modules\News\Observers\NewsObserver;
use Modules\Newsletters\Models\Newsletter;
use Modules\Newsletters\Observers\NewsletterObserver;
use Modules\Order\Models\Order;
use Modules\Order\Models\OrderItem;
use Modules\Order\Observers\OrderItemObserver;
use Modules\Order\Observers\OrderObserver;
use Modules\Page\Models\Page;
use Modules\Page\Observers\PageObserver;
use Modules\Partner\Models\Partner;
use Modules\Partner\Observers\PartnerObserver;
use Modules\PartnerOffer\Models\PartnerOffer;
use Modules\PartnerOffer\Observers\PartnerOfferObserver;
use Modules\PartnerProduct\Models\PartnerProduct;
use Modules\PartnerProduct\Observers\PartnerProductObserver;
use Modules\PartnerShop\Models\PartnerShop;
use Modules\PartnerShop\Observers\PartnerShopObserver;
use Modules\Product\Models\Product;
use Modules\Product\Observers\ProductObserver;
use Modules\Reviews\Models\Review;
use Modules\Reviews\Observers\ReviewObserver;
use Modules\Seo\Models\Seo;
use Modules\Seo\Observers\SeoObserver;
use Modules\Stock\Models\Stock;
use Modules\Stock\Observers\StockObserver;
use Modules\Subscriber\Models\Subscriber;
use Modules\Subscriber\Observer\SubscriberObserver;
use Modules\User\Observers\UserObserver;


/**
 * Class AppServiceProvider
 *
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        SocialUserResolverInterface::class => SocialUserResolver::class,
    ];
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        // @see: https://laravel.com/docs/5.8/pagination#customizing-the-pagination-view
        Paginator::defaultView('shared.pagination.stisla');
        Paginator::defaultSimpleView('shared.pagination.stisla');
        
        // Set Russian localization for Carbon
        Lang::setLocale('ru');
        setlocale(LC_ALL, 'ru_RU.utf8');
        Carbon::setLocale(config('app.locale'));
        
        /**
         * Paginate a standard Laravel Collection.
         *
         * @param  int     $perPage
         * @param  int     $total
         * @param  int     $page
         * @param  string  $pageName
         *
         * @return array
         */
        Collection::macro('paginate', function ($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ? : LengthAwarePaginator::resolveCurrentPage($pageName);
            
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ? : $this->count(),
                $perPage,
                $page,
                [
                    'path'     => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
        
        
        // Observer's
        Advantage::observe(AdvantageObserver::class);
        Attribute::observe(AttributeObserver::class);
        AttributeValue::observe(AttributeValueObserver::class);
        Banner::observe(BannerObserver::class);
        Brand::observe(BrandObserver::class);
        User::observe(UserObserver::class);
        Category::observe(CategoryObserver::class);
        Discount::observe(DiscountObserver::class);
        if (Discount::has('partnerOffers')) {
            Discount::observe(DiscountPartnerObserver::class);
        }
        Faq::observe(FaqObserver::class);
        News::observe(NewsObserver::class);
        Newsletter::observe(NewsletterObserver::class);
        Order::observe(OrderObserver::class);
        OrderItem::observe(OrderItemObserver::class);
        Page::observe(PageObserver::class);
        Partner::observe(PartnerObserver::class);
        PartnerOffer::observe(PartnerOfferObserver::class);
        PartnerProduct::observe(PartnerProductObserver::class);
        PartnerShop::observe(PartnerShopObserver::class);
        Product::observe(ProductObserver::class);
        Review::observe(ReviewObserver::class);
        Seo::observe(SeoObserver::class);
        Stock::observe(StockObserver::class);
        Subscriber::observe(SubscriberObserver::class);
        User::observe(UserObserver::class);
    }
}
