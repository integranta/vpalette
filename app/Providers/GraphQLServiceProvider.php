<?php

namespace App\Providers;

use App\Enums\BannerType;
use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use App\Enums\FrontendTypeAttributes;
use Illuminate\Support\ServiceProvider;
use Nuwave\Lighthouse\Schema\TypeRegistry;
use Nuwave\Lighthouse\Schema\Types\LaravelEnumType;

/**
 * Class GraphQLServiceProvider
 *
 * @package App\Providers
 */
class GraphQLServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @see https://lighthouse-php.com/4.9/the-basics/types.html#enum
     *
     * @param  TypeRegistry  $typeRegistry
     *
     * @return void
     */
    public function boot(TypeRegistry $typeRegistry): void
    {
        $typeRegistry->register(
            new LaravelEnumType(BannerType::class)
        );

        $typeRegistry->register(
            new LaravelEnumType(CouponPeriodActivity::class)
        );

        $typeRegistry->register(
            new LaravelEnumType(CouponTypes::class)
        );

        $typeRegistry->register(
            new LaravelEnumType(DiscountAmountTypes::class)
        );

        $typeRegistry->register(
            new LaravelEnumType(DiscountPeriodActivity::class)
        );

        $typeRegistry->register(
            new LaravelEnumType(DiscountTypes::class)
        );
        
        $typeRegistry->register(
        	new LaravelEnumType(FrontendTypeAttributes::class)
		);
    }
}
