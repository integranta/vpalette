<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use JhaoDa\SocialiteProviders\Odnoklassniki\OdnoklassnikiExtendSocialite;
use Modules\Coupon\Events\CouponWasUsed;
use Modules\Coupon\Listeners\IncrementUsesCountUser;
use Modules\News\Events\NewsPostWasVisited;
use Modules\News\Listeners\IncrementNewsPostViewCount;
use Modules\Partner\Events\PartnerRegistered;
use Modules\Partner\Listeners\SendRegisterNotification;
use SocialiteProviders\Manager\SocialiteWasCalled;

/**
 * Class EventServiceProvider
 *
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NewsPostWasVisited::class => [
            IncrementNewsPostViewCount::class
        ],
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
            'SocialiteProviders\\Facebook\\FacebookExtendSocialite@handle',
            'SocialiteProviders\\Google\\GoogleExtendSocialite@handle',
            OdnoklassnikiExtendSocialite::class,
        ],
        PartnerRegistered::class => [
            SendRegisterNotification::class
        ],
        CouponWasUsed::class => [
            IncrementUsesCountUser::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();

        //
    }
}
