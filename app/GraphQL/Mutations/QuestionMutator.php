<?php

namespace App\GraphQL\Mutations;

use App\Repositories\BaseRepository;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Modules\Faq\Models\Faq;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class QuestionMutator
 *
 * @package App\GraphQL\Mutations
 */
class QuestionMutator extends BaseRepository
{
    /**
     * QuestionMutator constructor.
     *
     * @param  Faq  $model
     */
    public function __construct(Faq $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * Resolve for creating new Question.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function createQuestion($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        return $this->model->create([
        	'active' => false,
			'sort' => 500,
			'question' => $args['question'],
			'user_id' => $args['user_id'],
			'product_id' => $args['product_id'] ?? null,
			'answer' => null
		]);
    }

    /**
     * Resolve for updating issues question.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function updateQuestion($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $question = $this->model::find($args['id']);

        if (!$question) {
            return $question;
        }

        return $question->fill($args)->save();
    }


    /**
     * Resolve for deleting issues question.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     * @throws Exception
     */
    public function deleteQuestion($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $question = $this->find($args['id']);

        if (!$question) {
            return $question;
        }

        $this->delete($args['id']);
        return $question;
    }
}
