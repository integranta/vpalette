<?php

namespace App\GraphQL\Mutations;

use Event;
use GraphQL\Type\Definition\ResolveInfo;
use Modules\Coupon\Events\CouponWasUsed;
use Modules\Coupon\Models\Coupon;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class CouponMutator
 *
 * @package App\GraphQL\Mutations
 */

/**
 * Class CouponMutator
 *
 * @package App\GraphQL\Mutations
 */
class CouponMutator
{

    /**
     * Return a value for the field.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $coupon = (new Coupon)->where(['code' => $args['coupon']])->first();

        if (Event::dispatch(new CouponWasUsed($coupon))) {
            return true;
        }

        return false;
    }
}
