<?php

namespace App\GraphQL\Mutations;

use Arr;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Faq\Models\Faq;
use Modules\Reviews\Models\Review;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class ReactionMutator
 *
 * @package App\GraphQL\Mutations
 */
class ReactionMutator
{
    /**
     * Return a value for the field.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return array
     * @throws Exception
     *
     */
    public function setLike($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        $reactableType = Arr::get($args, 'likeable_type');
        $reactableId = Arr::get($args, 'likeable_id');
        $userId = Arr::get($args, 'user_id');

        $reactableModel = $this->getReactableModel($reactableType, $reactableId);
        
        // Если повторно пытаемся поставить like, убираем ранее поставленый.
        if ($reactableModel->liked($userId)) {
			$reactableModel->unlike($userId);
			return $this->getResult($reactableModel, $userId);
		}

        if ($reactableModel->disliked($userId)) {
        	$reactableModel->undislike($userId);
            return $this->getResult($reactableModel, $userId);
        }
        $reactableModel->like($userId);

        return $this->getResult($reactableModel, $userId);
    }

    /**
     * @param  string  $reactableType
     * @param  int     $reactableId
     *
     * @return Collection|Model|Faq|Faq[]|Review|Review[]|null
     */
    private function getReactableModel(string $reactableType, int $reactableId): Model
    {
        $reactableModel = null;

        if (!empty($reactableType)) {
            switch ($reactableType) {
                case 'review':
                    $reactableModel = Review::find($reactableId);
                    break;
                case 'question':
                    $reactableModel = Faq::find($reactableId);
                    break;
            }
        }
        return $reactableModel;
    }

    /**
     * @param  Model|Review|Faq  $reactableModel
     * @param  int               $userId
     *
     * @return array
     */
    private function getResult(Model $reactableModel, int $userId): array
    {
        return [
            'countLikes' => $reactableModel->likeCount,
            'countDislikes' => $reactableModel->dislikeCount,
            'hasLike' => $reactableModel->liked($userId),
            'hasDislike' => $reactableModel->disliked($userId)
        ];
    }
	
	/**
	 * @param                  $rootValue
	 * @param  array           $args
	 * @param  GraphQLContext  $context
	 * @param  ResolveInfo     $resolveInfo
	 *
	 * @return array
	 * @throws \Exception
	 */
    public function setDislike($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        $reactableType = Arr::get($args, 'dislikeable_type');
        $reactableId = Arr::get($args, 'dislikeable_id');
        $userId = Arr::get($args, 'user_id');

        $reactableModel = $this->getReactableModel($reactableType, $reactableId);
        
        // Если был ранее поставлен dislike, убираем его.
        if ($reactableModel->disliked($userId)) {
        	$reactableModel->undislike($userId);
        	return $this->getResult($reactableModel, $userId);
		}

        if ($reactableModel->liked($userId)) {
            return $this->getResult($reactableModel, $userId);
        }

        $reactableModel->dislike($userId);

        return $this->getResult($reactableModel, $userId);
    }
}
