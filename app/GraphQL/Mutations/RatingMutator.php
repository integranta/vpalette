<?php

namespace App\GraphQL\Mutations;

use App\User;
use Arr;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Model;
use Modules\Faq\Models\Faq;
use Modules\PartnerShop\Models\PartnerShop;
use Modules\Product\Models\Product;
use Modules\Reviews\Models\Review;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class RatingMutator
 *
 * @package App\GraphQL\Mutations
 */
class RatingMutator
{
    /**
     * Return a value for the field.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function set($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $ratingableType = Arr::get($args, 'ratingable_type');
        $ratingableId = Arr::get($args, 'ratingable_id');
        $user = $this->getUser(Arr::get($args, 'user_id'));
        $mark = (float) Arr::get($args, 'rating');

        $ratingableModel = $this->getRatingableModel($ratingableType, $ratingableId);

        if ($ratingableModel) {
            $ratingableModel->setRating($ratingableModel, $user, $mark);

            return $this->getResult($ratingableModel);
        }

        return $this->getResult($ratingableModel);
    }

    /**
     * @param  int  $userId
     *
     * @return mixed
     */
    private function getUser(int $userId)
    {
        return (new User)->find($userId);
    }

    /**
     * Return class ratingable model by ID
     *
     * @param  string  $ratingableType
     * @param  int     $ratingableId
     *
     * @return Model|Product|PartnerShop|Review|Faq
     */
    private function getRatingableModel(string $ratingableType, int $ratingableId): ?Model
    {
        $ratingableModel = null;

        if (!empty($ratingableType)) {
            switch ($ratingableType) {
                case 'product':
                    $ratingableModel = Product::find($ratingableId);
                    break;
                case 'shop':
                    $ratingableModel = PartnerShop::find($ratingableId);
                    break;
                case 'review':
                    $ratingableModel = Review::find($ratingableId);
                    break;
                case 'question':
                    $ratingableModel = Faq::find($ratingableId);
                    break;
            }
        }

        return $ratingableModel;
    }

    /**
     * @param  Model|Product|PartnerShop|Review|Faq  $ratingableModel
     *
     * @return array
     */
    private function getResult(Model $ratingableModel): array
    {
        return [
            'sumRating' => $ratingableModel->sumRating() ?? null,
            'minRating' => $ratingableModel->minRating() ?? null,
            'maxRating' => $ratingableModel->maxRating() ?? null,
            'avgRating' => $ratingableModel->avgRating() ?? null,
            'percentRating' => $ratingableModel->percentRating() ?? null
        ];
    }

    /**
     * @param  null            $rootValue
     * @param  mixed[]         $args
     * @param  GraphQLContext  $context
     * @param  ResolveInfo     $resolveInfo
     *
     * @return mixed
     */
    public function update($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $ratingableType = Arr::get($args, 'ratingable_type');
        $ratingableId = Arr::get($args, 'ratingable_id');
        $ratingId = Arr::get($args, 'ratingId');
        $mark = (float) Arr::get($args, 'rating');

        $ratingableModel = $this->getRatingableModel($ratingableType, $ratingableId);

        if ($ratingableModel) {

            $ratingableModel->updateRating($ratingId, ['rating' => $mark]);

            return $this->getResult($ratingableModel);
        }

        return $this->getResult($ratingableModel);
    }

    /**
     * @param                  $rootValue
     * @param  array           $args
     * @param  GraphQLContext  $context
     * @param  ResolveInfo     $resolveInfo
     *
     * @return array
     */
    public function delete($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        $ratingableType = Arr::get($args, 'ratingable_type');
        $ratingableId = Arr::get($args, 'ratingable_id');
        $ratingId = Arr::get($args, 'ratingId');

        $ratingableModel = $this->getRatingableModel($ratingableType, $ratingableId);

        if ($ratingableModel) {
            $ratingableModel->deleteRating($ratingId);

            return $this->getResult($ratingableModel);
        }

        return $this->getResult($ratingableModel);
    }
}
