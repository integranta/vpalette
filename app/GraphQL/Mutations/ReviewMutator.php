<?php

namespace App\GraphQL\Mutations;

use App\Repositories\BaseRepository;
use App\User;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Modules\Product\Models\Product;
use Modules\Reviews\Models\Review;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Storage;
use Str;

/**
 * Class ReviewMutator
 *
 * @package App\GraphQL\Mutations
 */
class ReviewMutator extends BaseRepository
{
    /**
     * ReviewMutator constructor.
     *
     * @param  Review  $model
     */
    public function __construct(Review $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    /**
     * Resolve for create review.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function createReview($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
    	$product = Product::find($args['product_id']);
    	$user = User::find($args['user_id']);
    	
        $args['photos'] = $this->uploadPicture($args);
        $args['username'] = $args['make_anonymous'] ? "Анонимно" : $user->name;

        $review = $this->create([
            'user_id' => $args['user_id'],
            'username' => $args['username'],
            'photos' => $args['photos'],
            'product_id' => $args['product_id'],
            'virtues' => $args['virtues'],
            'limitations' => $args['limitations'],
            'comments' => $args['comments'],
            'make_anonymous' => $args['make_anonymous'],
            'rating' => $args['rating']
        ]);
        
        $product->setRating($product, $user, $args['rating']);

        return $review;
    }

    /**
     * @param  array  $params
     *
     * @return array
     */
    private function uploadPicture(array $params): array
    {
        $photosPaths = array();
        $hash = Str::random(10);

		Storage::disk('uploads')
			->makeDirectory("reviews/{$params['product_id']}/{$params['user_id']}");
		
		if (isset($params['photos'])) {
            foreach ($params['photos'] as $key => $photo) {
                $photosPaths[$key]['src'] = Storage::disk('uploads')
					->put("reviews/{$params['product_id']}/{$params['user_id']}",
                    $photo
                );
            }
        } else {
            return [];
        }

        return $photosPaths;
    }

    /**
     * Resolve for update review.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function updateReview($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $args['photos'] = $this->uploadPicture($args);
        $args['username'] = $args['make_anonymous'] ? "Анонимно" : $this->model->find($args['id'])->user->name;

        $this->update([
            'user_id' => $args['user_id'],
            'username' => $args['username'],
            'photos' => $args['photos'],
            'product_id' => $args['product_id'],
            'virtues' => $args['virtues'],
            'limitations' => $args['limitations'],
            'comments' => $args['comments'],
            'make_anonymous' => $args['make_anonymous'],
            'rating' => $args['rating']
        ], $args['id']);

        // Обновляем рейтинг
        $ratingId = $this->find($args['id'])->ratings->where('ratingable_id', '=', $args['id'])->first()->id;
        $this->model->updateRating($ratingId, ['rating' => $args['rating']]);

        return $this->find($args['id']);
    }

    /**
     * Return a value for the field.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     * @throws Exception
     */
    public function deleteReview($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // Находим рейтинг и удаляем его.
        $ratingId = $this->find($args['id'])->ratings->where('ratingable_id', '=', $args['id'])->first()->id;
        $this->model->deleteRating($ratingId);
        $this->delete($args['id']);
        return $this->find($args['id']);
    }

}
