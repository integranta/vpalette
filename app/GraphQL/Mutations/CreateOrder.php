<?php

namespace App\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use Modules\Order\Models\Order;
use Modules\Order\Services\GraphQL\OrderGraphQLService;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class CreateOrder
 *
 * Обработчик создания заказа
 *
 * @package App\GraphQL\Mutations
 */
class CreateOrder
{
	private $service;
	
	/**
	 * CreateOrder constructor.
	 *
	 * @param $service
	 */
	public function __construct(OrderGraphQLService $service)
	{
		$this->service = $service;
	}
	
	
	/**
	 * Return a value for the field.
	 *
	 * @param  @param  null  $root Always null, since this field has no parent.
	 * @param  array<string, mixed>                                 $args         The field arguments passed by the client.
	 * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context      Shared between all fields.
	 * @param  \GraphQL\Type\Definition\ResolveInfo                 $resolveInfo  Metadata for advanced query resolution.
	 *
	 * @return \Modules\Order\Models\Order|null
	 */
    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): ?Order
    {
        return $this->service->createOrder($args);
    }
}
