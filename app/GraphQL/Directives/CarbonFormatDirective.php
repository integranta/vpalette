<?php

namespace App\GraphQL\Directives;

use Arr;
use Carbon\Carbon;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class CarbonFormatDirective
 *
 * @package App\GraphQL\Directives
 */
class CarbonFormatDirective extends BaseDirective implements FieldMiddleware
{
    /**
     * @return string
     */
    public static function definition(): string
    {
        return /* @lang GraphQL */ <<<'SDL'
"""
Get format of date
"""
directive @carbonFormat on FIELD_DEFINITION
SDL;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return 'carbonFormat';
    }

    /**
     * @param  FieldValue  $fieldValue
     * @param  Closure     $next
     *
     * @return FieldValue
     */
    public function handleField(FieldValue $fieldValue, Closure $next): FieldValue
    {
        $previousResolver = $fieldValue->getResolver();

        $wrappedResolver = static function ($root, array $args, GraphQLContext $context, ResolveInfo $info) use (
            $previousResolver
        ) {
            $result = $previousResolver($root, $args, $context, $info);

            $format = Arr::get($args, 'format');

            if (!$result instanceof Carbon) {
                return null;
            }

            if ($format === 'for_human') {
                return $result->isoFormat('DD MMMM YYYY');
            }

            return $format
                ? $result->format($format)
                : $result->toDateTimeString();
        };

        $fieldValue->setResolver($wrappedResolver);

        return $next($fieldValue);
    }
}
