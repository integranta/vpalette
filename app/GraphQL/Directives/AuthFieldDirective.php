<?php

namespace App\GraphQL\Directives;

use App\Traits\UserFromBearerToken;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Auth\AuthManager;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\DefinedDirective;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;


class AuthFieldDirective extends BaseDirective implements FieldMiddleware
{
	use UserFromBearerToken;
	
	public static function definition(): string
	{
		return /** @lang GraphQL */ <<<'GRAPHQL'
directive @authField on FIELD_DEFINITION
GRAPHQL;
	}
	
	public function handleField(FieldValue $fieldValue, Closure $next): FieldValue
	{
		$resolver = $fieldValue->getResolver();
		
		$fieldValue->setResolver(function ($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($resolver) {
			// Do something before the resolver, e.g. validate $args, check authentication
			
			// Call the actual resolver
			$result = $resolver($root, $args, $context, $resolveInfo);
			$authToken = Str::of($context->request()->headers->get('authorization'))->after('Bearer ');
			$user = $this->getUser($authToken);
			
			if ($user !== null) {
				auth()->login($user);
			}
			
			return $result;
		});
		
		// Keep the chain of adding field middleware going by calling the next handler.
		// Calling this before or after ->setResolver() allows you to control the
		// order in which middleware is wrapped around the field.
		return $next($fieldValue);
	}
}
