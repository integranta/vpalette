<?php

namespace App\GraphQL\Queries;

use App\Traits\GetAllProductsFromCategories;
use App\Traits\GetIdDescendentsParentCategory;
use GraphQL\Type\Definition\ResolveInfo;
use Modules\Reviews\Models\Review;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class FetchReviewsFromParentAndSubCategories
 *
 * @package App\GraphQL\Queries
 */
class FetchReviewsFromParentAndSubCategories
{
	use GetIdDescendentsParentCategory;
	use GetAllProductsFromCategories;
	
	/**
	 * Return a value for the field.
	 *
	 * @param  @param  null  $root Always null, since this field has no parent.
	 * @param  array<string, mixed>                                 $args         The field arguments passed by the
	 *                                                                            client.
	 * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context      Shared between all fields.
	 * @param  \GraphQL\Type\Definition\ResolveInfo                 $resolveInfo  Metadata for advanced query
	 *                                                                            resolution.
	 *
	 * @return mixed
	 */
	public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
	{
		$categoryIds = $this->getIdDescendentsParentCategory('slug', $args['slug']);
		
		$products = $this->getAllProductsFromCategories(
			$categoryIds,
			$args['orderBy']['field'],
			$args['orderBy']['order']
		);
		
		$productIds = $products->get(['id'])->pluck('id')->toArray();
		
		return Review::whereIn('id', $productIds)
			->orderBy($args['orderBy']['field'], $args['orderBy']['order'])
			->limit($args['countReview'])
			->get([
				'id',
				'active',
				'sort',
				'user_id',
				'product_id',
				'virtues',
				'limitations',
				'comments',
				'make_anonymous',
				'photos',
				'rating',
				'username',
				'created_at',
				'updated_at',
				'deleted_at'
			]);
	}
}
