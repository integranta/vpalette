<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Modules\Attribute\Models\Attribute;
use Modules\Category\Models\Category;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class FetchFilterParams
{
    /**
     * Return a value for the field.
     *
     * @param  @param  null  $root Always null, since this field has no parent.
     * @param  array<string, mixed>  $args The field arguments passed by the client.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Shared between all fields.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Metadata for advanced query resolution.
     * @return mixed
     */
    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
    	if (isset($args['fullPath']) === false)
		{
			$attributes = Attribute::with(['values'])
				->where('is_filterable', '=', true);
			return $attributes->get();
		}
    	
        $category = Category::whereFullPath($args['fullPath'])->first(['id']);
        
        if ($category === null)
		{
			$attributes = Attribute::with(['values'])
				->where('is_filterable', '=', true);
			return $attributes->get();
		}
		$category->load(['attributes', 'attributes.values']);
        return $category->attributes;
    }
}
