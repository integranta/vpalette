<?php

namespace App\GraphQL\Queries;

use Cache;
use GraphQL\Type\Definition\ResolveInfo;
use Modules\Category\Models\Category;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class CatalogTree
 *
 * @package App\GraphQL\Queries
 */
class CatalogTree
{
    /**
     * Return a value for the field.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
		$categories = Category::with([
			'children' => function($query) {
				return $query
					->cacheFor(60 * 60)
					->cacheTags(['category:children']);
			}/*,
			'children.products:product_id,category_id' => function($query) {
				return $query
					->cacheFor(60 * 60)
					->cacheTags(['category:children.products']);
			},
			'products:product_id,category_id' => function($query) {
				return $query
					->cacheFor(60 * 60)
					->cacheTags(['category:products']);
			}*/
		])
			->where('active', '=', $args['active'] ?? true)
			->orderBy('parent_id')
			->get()
			->nest();
		
		//dd(__METHOD__, $categories);
		
        return Cache::rememberForever(
            config('catalog-tree.cache_key'),
            static function () use ($categories) {
				return $categories;
            }
        );

        // return Cache::get(config('catalog-tree.cache_key'), 'default');
    }
}
