<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use Modules\Product\Models\Product;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class FetchPartnerOffersForProduct
{
	/**
	 * Return a value for the field.
	 *
	 * @param  @param  null  $root Always null, since this field has no parent.
	 * @param  array<string, mixed>                                 $args         The field arguments passed by the
	 *                                                              client.
	 * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context      Shared between all fields.
	 * @param  \GraphQL\Type\Definition\ResolveInfo                 $resolveInfo  Metadata for advanced query
	 *                                                                            resolution.
	 *
	 * @return mixed
	 */
	public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
	{
		$product = Product::find($args['productId']);
		
		if ($product === null) {
			return null;
		}
		
		$product->load('partnerOffers');
		
		$offers = $product->partnerOffers()->orderBy(
			$args['orderBy'][0]['field'],
			$args['orderBy'][0]['order']
		);
		
		return [
			'offers'  => $offers->get(),
			'min_price' => $offers->min('price'),
			'max_price' => $offers->max('price')
		];
	}
}
