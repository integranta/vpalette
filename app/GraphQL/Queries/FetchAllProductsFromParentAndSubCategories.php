<?php

namespace App\GraphQL\Queries;

use App\Traits\GetAllProductsFromCategories;
use App\Traits\GetIdDescendentsParentCategory;
use GraphQL\Type\Definition\ResolveInfo;
use Modules\Product\Models\Product;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class FetchAllProductsFromParentAndSubCategories
{
	use GetIdDescendentsParentCategory;
	use GetAllProductsFromCategories;
	
	/**
	 * Return a value for the field.
	 *
	 * @param  @param  null  $root Always null, since this field has no parent.
	 * @param  array<string, mixed>                                 $args         The field arguments passed by the
	 *                                                                            client.
	 * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context      Shared between all fields.
	 * @param  \GraphQL\Type\Definition\ResolveInfo                 $resolveInfo  Metadata for advanced query
	 *                                                                            resolution.
	 *
	 * @return mixed
	 */
	public function resolve($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
	{
		$categoryIds = $this->getIdDescendentsParentCategory('slug', $args['slug']);
		
		$products = $this->getAllProductsFromCategories(
			$categoryIds,
			$args['orderBy']['field'],
			$args['orderBy']['order']
		);
		
		$minPrice = $products->get()->map(function (Product $product) {
			return $product->partnerOffers()->min('price');
		})->min();
		
		$maxPrice = $products->get()->map(function (Product $product) {
			return $product->partnerOffers()->min('price');
		})->max();
		
		$paginateData = $products->paginate(
			$args['count'],
			[
				'id',
				'sections',
				'article',
				'barcode',
				'name',
				'slug',
				'type_product',
				'is_new',
				'full_path',
				'full_paths',
				'about',
				'weight',
				'min_price',
				'sort',
				'active',
				'is_popular',
				'brand_id',
				'created_at',
				'updated_at',
				'deleted_at'
			],
			'page',
			$args['page']
		);
		
		return [
			'paginatorInfo' => [
				'currentPage'  => $paginateData->currentPage(),
				'firstItem'    => $paginateData->firstItem(),
				'hasMorePages' => $paginateData->hasMorePages(),
				'lastItem'     => $paginateData->lastItem(),
				'lastPage'     => $paginateData->lastPage(),
				'perPage'      => $paginateData->perPage(),
				'total'        => $paginateData->total()
			],
			'data'          => $paginateData->items(),
			'min_price'     => $minPrice,
			'max_price'     => $maxPrice
		];
	}
}
