<?php

namespace App\GraphQL\Queries;

use App\Traits\GetAllProductsFromCategories;
use App\Traits\GetIdDescendentsParentCategory;
use App\Traits\GetMaxPrice;
use App\Traits\GetMinPrice;
use App\Traits\GetProductPaginateData;
use App\Traits\GetProductPaginateResponse;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Attribute\Models\Attribute;
use Modules\Category\Models\Category;
use Modules\Product\Models\Product;
use Modules\Product\Models\ProductAttribute;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

/**
 * Class FilterProduct
 *
 * @package App\GraphQL\Queries
 */
class FilterProduct
{
	use GetMinPrice;
	use GetMaxPrice;
	use GetAllProductsFromCategories;
	use GetProductPaginateData;
	use GetProductPaginateResponse;
	use GetIdDescendentsParentCategory;
	
    /**
     * Return a value for the field.
     *
     * @param  null            $rootValue    Usually contains the result returned from the parent field. In this case,
     *                                       it is always `null`.
     * @param  mixed[]         $args         The arguments that were passed into the field.
     * @param  GraphQLContext  $context      Arbitrary data that is shared between all fields of a single query.
     * @param  ResolveInfo     $resolveInfo  Information about the query itself, such as the execution state, the field
     *                                       name, path to the field from the root, and more.
     *
     * @return mixed
     */
    public function resolve($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
    	$arProps = collect($args['params'])->groupBy('code')->toArray();
    	$category = Category::whereFullPath($args['fullPath'])->first();
    	$products = null;
    	
    	if ($category === null) {
			return null;
		}
    	
    	$categories = $this->getIdDescendentsParentCategory('full_path', $args['fullPath']);
    	$products = $this->getAllProductsFromCategories($categories, 'id', 'asc');
    	
    	if(count($arProps) > 0) {
			$attributeIds = [];
		
			foreach ($arProps as $propCode => $propValues) {
				$attribute = Attribute::whereCode($propCode)->first();
				if ($attribute === null) {
					continue;
				}
				$attributeIds[] = $attribute->id;
			}
		
			$productAttributes = [];
		
			foreach ($arProps as $propCode => $propValues) {
				foreach ($propValues as $propValue) {
					foreach ($attributeIds as $attributeId) {
						$productAttribute = ProductAttribute::whereAttributeId($attributeId)
							->whereValue($propValue['value'])
							->first(['product_id']);
					
						if ($productAttribute === null) {
							continue;
						}
						$productAttributes[] = $productAttribute->toArray();
					}
				}
			}
		
			$productIds = Arr::pluck($productAttributes, 'product_id');
		
		
			$filterProducts = $products->whereIn('id', $productIds)
				->where('min_price', '>=', $args['minPrice'], 'and')
				->where('min_price', '<=', $args['maxPrice'])
				->orderBy('id', 'asc');
		
		
			$minPrice = $this->getMinPrice($filterProducts);
		
			$maxPrice = $this->getMaxPrice($filterProducts);
		
			$paginateData = $this->getProductPaginateData($filterProducts);
		
			return $this->getProductPaginateResponse($paginateData, $minPrice, $maxPrice);
		}
    	
    	$filterProducts = $products
			->where('min_price', '>=', $args['minPrice'], 'and')
			->where('min_price', '<=', $args['maxPrice'])
			->orderBy('id', 'asc');
	
		$minPrice = $this->getMinPrice($filterProducts);
	
		$maxPrice = $this->getMaxPrice($filterProducts);
	
		$paginateData = $this->getProductPaginateData($filterProducts);
	
		return $this->getProductPaginateResponse($paginateData, $minPrice, $maxPrice);
    }
}
