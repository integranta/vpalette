# ВПалитре

### ВПалитре — маркетплейс для художников и творческих людей.

Для запуска этого проекта склонируйте себе проект используя у себя в командной строке/терминале
```bash
git clone https://livevasiliy@bitbucket.org/konplus/vpalette.git
```

После того как проект будет успешно склонирован, перейдите в папку, где лежит копия этого проекта, для этого напишите
в командной строке/терминале
```bash
cd ./vpalette
```
Далее запустите команду для установки всех php-зависимостей указанных в composer.json

#### У вас должен быть установлен на компьютере composer и версия PHP не ниже 7.2
```bash
composer install
```

Скопируйте себе файл .env.example для этого напишите команду:

Если Windows:
```bash
copy .env.example .env
```

На Linux/macOS:
```bash
cp .env.example .env
```

Установите часовой пояс для этого укажите в скопированный новый файл .env в параметр
```text
APP_TIMEZONE=ЗНАЧЕНИЕ_ЧАСОВОГО_ПОЯСА
```
Значения можно найти [тут](https://www.php.net/manual/ru/timezones.europe.php)

По умолчанию используется ``Europe/Samara`` (UTC+4).

Сгенерируйте ключ

```bash
php artisan key:generate
```

Далее укажите в .env параметры для подключения к вашей базе данных для этого заполните параметры в файле:

```
DB_DATABASE=vpalette // Название созданной вами базы данных
DB_USERNAME=root // Имя пользователя базы данных
DB_PASSWORD= // Пароль используемого для пользователя
```


После успешной установки всех зависимостей composer, запустите миграции модулей, а потом остальные миграции в указанную базу данных,
для этого запустите команду:
```bash
php artisan module:migrate
php artisan migrate
```

Далее сгенерируйте ключ для Laravel Passport:
```bash
php artisan passport:install
```
Скопируйте полученный ключ (Client secret) для ``Password grand client``
и укажите его в ``.env`` в параметр ``PASSPORT_CLIENT_SECRET=`` и номер ID в параметр ``PASSPORT_CLIENT_ID=``
он нужен для корректной работы авторизации, через GraphQL.

Далее создайте в ``storage/app/public`` папку ``uploads`` и скопируйте папку ``faker``
из ``public/faker`` в созданную папку ``storage/uploads``.

Настройки связь между папкой ``storage`` и ``public`` для этого введите следующую команду:
```bash
php artisan storage:link
```

После запустите выгрузку подготовленных данных для моделей:
```bash
php artisan db:seed
```
