# PHP CircleCI 2.0 configuration file
#
# Check https://circleci.com/docs/2.0/language-php/ for more details
#
version: 2
jobs:
  build:
    docker:
      # Specify the version you desire here
      - image: circleci/php:7.3-node-browsers
      # Database
      - image: circleci/mysql:5.7-ram
        envoronment:
          MYSQL_DATABASE: "vpalette"
          MYSQL_DATATABASE_PASSWORD: ""
    steps:
      - add_ssh_keys:
          fignerprints:
            - "pU7C2fiKLZZriEtyNFSdF332Oi0wE5FWB0nq3+V9xyU"
      - checkout
      - run:
          name: "Prepare Environment"
          command: |
            sudo -E apt update
            sudo -E apt install zlib1g-dev libsqlite3-dev
            sudo -E apt-get install -y libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
                libfreetype6-dev
            sudo -E docker-php-ext-install zip
            sudo -E docker-php-ext-install mbstring
            sudo -E docker-php-ext-install pdo_mysql
            sudo -E docker-php-ext-configure gd --with-gd --with-webp-dir --with-jpeg-dir \
                --with-png-dir --with-zlib-dir --with-xpm-dir --with-freetype-dir
            sudo -E docker-php-ext-install gd
            sudo -E docker-php-ext-install bcmath
            sudo -E apt-get -yqq install exiftool
            sudo -E docker-php-ext-configure exif
            sudo -E docker-php-ext-install exif
            sudo -E docker-php-ext-enable exif
      - run:
          name: "Generate private OAuth key"
          command: |
            openssl genrsa -out storage/oauth-private.key 4096
            openssl rsa -in storage/oauth-private.key -pubout > storage/oauth-public.key
      # Download and cache dependencies
      - restore_cache:
          keys:
            # "composer.lock" can be used if it is committed to the repo
            - v1-dependencies-{{ checksum "composer.json" }}
            # fallback to using the latest cache if no exact match is found
            - v1-dependencies-
      - run:
          name: "Install Dependencies"
          command: composer install --prefer-dist --no-interaction --no-progress --no-scripts --no-suggest
      - save_cache:
          key: v1-dependencies-{{ checksum "composer.json" }}
          paths:
            - ./vendor
      - run:
          name: "Set up permissions"
          command: |
            sudo chmod -R 777 vendor storage bootstrap
      # prepare the database
      - run:
          name: "Unlimit memory for composer"
          command: |
            php -d memory_limit=-1
      - run:
          name: "Clear cache app"
          command: |
            php artisan config:clear
            php artisan config:cache
      - run:
          name: "Create database and run migration"
          command: |
            php artisan migrate --force --env=testing
      - run:
          name: "Generate Passport encryption keys"
          command: |
            php artisan passport:install
            php artisan passport:keys
