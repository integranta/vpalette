<?php

//Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');


Route::get('/', 'HomeController');
Route::get('home', 'HomeController');

Route::name('admin.')
	->prefix('admin')
	->middleware('auth')
	->group(function () {
		// Notifications
		Route::post('notify/read', 'NotificationController@markAsRead')
			->name('markAsRead');
		Route::post('notify/read/{id}', 'NotificationController@markAsReadById')
			->name('markAsReadById');
		Route::get('notify/list/unread', 'NotificationController@listAllUnReadNotifications')
			->name('listUnreadNotifications');
		Route::get('notify/list/read', 'NotificationController@listAllReadNotifications')
			->name('listReadNotifications');
		Route::get('notify/{id}/list', 'NotificationController@listAllNotifications')
			->name('listAllNotifications');
		Route::post('notify/{id}/remove', 'NotificationController@removeNotificationById');
	});

/*Route::middleware('auth')->get('logout', function() {

})->name('logout');*/

Route::get('logout', 'Auth\LogoutController')->middleware('auth');

Auth::routes(['verify' => true]);

Route::name('js.')->group(function () {
	Route::get('dynamic', 'JsController@dynamic')->name('dynamic');
});

// Get authenticated user
/*Route::get('users / auth', function() {
    return response()->json(['user' => Auth::check() ? Auth::user() : false]);
});*/


// Test Auth via VK
Route::get('login /{
		provider}', 'LoginController@redirectToProvider')
	->name('socialLogin');
Route::get('login/{provider}/callback', 'LoginController@handleProviderCallback')
	->name('socialLoginCallback');


Route::get('test/delivery/cdek/oauth', 'TestDeliveryController@cdekAuth');
Route::get('test/delivery/cdek/get/cities', 'TestDeliveryController@getListCities');
Route::get('test/delivery/cdek/get/regions', 'TestDeliveryController@getListRegions');
Route::get('test/delivery/cdek/get/delivery-points', 'TestDeliveryController@getListDeliveryPoints');
Route::get('test/delivery/cdek/get/reg-order', 'TestDeliveryController@regOrder');