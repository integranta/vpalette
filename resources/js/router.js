import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

import UsersComponent from './components/UsersComponent';

export default new Router({
    routes: [
        {
            path: 'admin/users',
            name: 'users.index',
            component: UsersComponent
        }
    ],
    mode: 'history'
})
