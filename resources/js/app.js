import './bootstrap';
import httpCodes from 'http-status-codes'
import Vue from 'vue';

import Permissions from "./mixins/Permissions";
import iosAlertView from 'vue-ios-alertview';
import VueSwal from 'vue-swal/dist/vue-swal.min';

// User
import UsersComponent from './components/UsersComponent';
import ProfileComponent from './components/ProfileComponent';
import AdduserComponent from './components/AdduserComponent';

// Partner
import RegisterPartnerComponent from "./components/Partner/PartnerCreate";
import EditPartnerComponent from "./components/Partner/PartnerEdit";
import ShowPartnerComponent from "./components/Partner/PartnerShow";
import TablePartners from "./components/Partner/PartnerTable";
import IndexPartner from "./components/Partner/PartnerIndex";

// Product Attributes
import ProductAttributes from "./components/Attribute/ProductAttributes";
import AttributeValues from "./components/Attribute/AttributeValues";

// Dashboard chart
import LineChartComponent from "./components/Dashboard/LineChartComponent";

// Common components
import LoaderComponent from "./components/Shared/Common/LoaderComponent";
import FlashMessage from "./components/Shared/Common/FlashMessage";
import Pagination from "./components/Shared/Common/Pagination";
import DataTable from "./components/Shared/Common/DataTable";
import SeoFields from "./components/Shared/Common/SeoFields";

// Newsletter
import NewsletterCreate from "./components/Newsletters/NewsletterCreate";
import NameStepsNewsletter from "./components/Newsletters/Shared/NameStepsNewsletter";
import NewsletterFirstStep from "./components/Newsletters/Shared/NewsletterFirstStep";
import NewsletterSecondStep from "./components/Newsletters/Shared/NewsletterSecondStep";
import NewsletterThreeStep from "./components/Newsletters/Shared/NewsletterThreeStep";
import NewsletterEdit from "./components/Newsletters/NewsletterEdit";
import NewsletterShow from "./components/Newsletters/NewsletterShow";

// Notification
import Notifications from "./components/Shared/Notification/Notifications";
import NotificationList from "./components/Shared/Notification/NotificationList";

// PartnerShop
import PartnerShopCreate from "./components/PartnerShop/PartnerShopCreate";
import PartnerShopEdit from "./components/PartnerShop/PartnerShopEdit";
import PartnerShopShow from "./components/PartnerShop/PartnerShopShow";
import PartnerShopIndex from "./components/PartnerShop/PartnerShopIndex";

// Coupons
import CouponCreate from "./components/Coupon/CouponCreate";
import CouponEdit from "./components/Coupon/CouponEdit";
import CouponShow from "./components/Coupon/CouponShow";

// Pages
import PageCreate from "./components/Page/PageCreate";
import PageEdit from "./components/Page/PageEdit";
import PageShow from "./components/Page/PageShow";
import PageIndex from "./components/Page/PageIndex";

// Order
import OrderEdit from "./components/Orders/OrderEdit";
import OrderCreate from "./components/Orders/OrderCreate";

// Discount from Partner
import DiscountPartnerCreate from "./components/Discount/DiscountPartner/DiscountPartnerCreate";
import DiscountPartnerEdit from "./components/Discount/DiscountPartner/DiscountPartnerEdit";
import DiscountPartnerShow from "./components/Discount/DiscountPartner/DiscountPartnerShow";

// Discount from Portal
import DiscountPortalCreate from "./components/Discount/DiscountPortal/DiscountPortalCreate";
import DiscountPortalEdit from "./components/Discount/DiscountPortal/DiscountPortalEdit";
import DiscountPortalShow from "./components/Discount/DiscountPortal/DiscountPortalShow";

Vue.use(iosAlertView);
Vue.use(VueSwal);
Vue.mixin(Permissions);

Vue.prototype.$http = window.axios;
Vue.prototype.$httpCode = httpCodes;


/**
 * USER MODULE COMPONENTS
 *
 */
Vue.component('users-component', UsersComponent);
Vue.component('profile-component', ProfileComponent);
Vue.component('adduser-component', AdduserComponent);

/**
 * PARTNER MODULE COMPONENTS
 */
Vue.component('register-partner-component', RegisterPartnerComponent);
Vue.component('edit-partner-component', EditPartnerComponent);
Vue.component('show-partner-component', ShowPartnerComponent);
Vue.component('index-partner-component', IndexPartner);
Vue.component('table-partners-component', TablePartners);

Vue.component('attribute-values', AttributeValues);
Vue.component('product-attributes', ProductAttributes);


// Линейный график
Vue.component('linechart-component', LineChartComponent);

/**
 * SHARED COMPONENTS
 */
Vue.component('loader-component', LoaderComponent);
Vue.component('flash-messages-component', FlashMessage);
Vue.component('DataTable', DataTable);
Vue.component('pagination', Pagination);
Vue.component('seo-fields', SeoFields);

// Email-Рассылка модуль
Vue.component('create-newsletter-component', NewsletterCreate);
Vue.component('edit-newsletter-component', NewsletterEdit);
Vue.component('show-newsletter-component', NewsletterShow);

// Email-Рассылка модуль Shared компоненты
Vue.component('name-steps-newsletter-component', NameStepsNewsletter);
Vue.component('newsletter-first-step-component', NewsletterFirstStep);
Vue.component('newsletter-second-step-component', NewsletterSecondStep);
Vue.component('newsletter-three-step-component', NewsletterThreeStep);

// Уведомления
Vue.component('notifications', Notifications);
Vue.component('notifications-list', NotificationList);

// Парнерские магазин модуль
Vue.component('partner-shop-create', PartnerShopCreate);
Vue.component('partner-shop-edit', PartnerShopEdit);
Vue.component('partner-shop-show', PartnerShopShow);
Vue.component('partner-shop-index', PartnerShopIndex);

// Скидки
Vue.component('discount-partner-create', DiscountPartnerCreate);
Vue.component('discount-partner-edit', DiscountPartnerEdit);
Vue.component('discount-partner-show', DiscountPartnerShow);

Vue.component('discount-portal-create', DiscountPortalCreate);
Vue.component('discount-portal-edit', DiscountPortalEdit);
Vue.component('discount-portal-show', DiscountPortalShow);

// Купоны
Vue.component('coupon-create', CouponCreate);
Vue.component('coupon-edit', CouponEdit);
Vue.component('coupon-show', CouponShow);




/**
 * Модуль Страницы
 */
Vue.component('page-index', PageIndex);
Vue.component('page-create', PageCreate);
Vue.component('page-edit', PageEdit);
Vue.component('page-show', PageShow);


/**
 * МОДУЛЬ Заказ
 */
Vue.component('order-create', OrderCreate);
Vue.component('order-edit', OrderEdit);

Vue.directive('uppercase', {
    update(el) {
        el.value = el.value.toUpperCase();
    }
});

Vue.directive('number', {
    update(el) {
        if (el.value === '') {
            el.value = 0;
        }
        el.value = Number.parseInt(el.value);
    }
});

new Vue({
    el: '#app',
    data() {
        return {
            user: AuthUser
        }
    },
    methods: {
        userCan(permission) {
            if (this.user && this.user.allPermissions.includes(permission)) {
                return true;
            }
            return false;
        },
        MakeUrl(path) {
            return BaseUrl(path);
        }
    }
});
