import './bootstrap';
import Vue from 'vue';
// Libraries
import 'bootstrap';
import { Form, HasError, AlertError } from 'vform';
import iosAlertView from 'vue-ios-alertview';

// Route information for Vue Router.
import Routes from './routes.js';

// Component File.
import App from './views/App';

Vue.use(
    iosAlertView
);

const app = new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App),
});

export default app;
