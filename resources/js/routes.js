import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './components/Home';

import UsersIndex from "./components/Users/UsersIndex";
import UsersEdit from "./components/Users/UsersEdit";
import UsersCreate from "./components/Users/UsersCreate";
import UsersShow from "./components/Users/UsersShow";

import SubscribersIndex from "./components/Subscribers/SubscribersIndex";

import PartnerIndex from "./components/Partner/PartnerIndex";
import PartnerCreate from "./components/Partner/PartnerCreate";
import PartnerEdit from "./components/Partner/PartnerEdit";
import PartnerShow from "./components/Partner/PartnerShow";

import ProductsIndex from "./components/Products/ProductsIndex";
import ProductsCreate from "./components/Products/ProductsCreate";
import ProductsEdit from "./components/Products/ProductsEdit";
import ProductsShow from "./components/Products/ProductsShow";

import OrdersIndex from "./components/Orders/OrdersIndex";
import OrdersCreate from "./components/Orders/OrdersCreate";
import OrdersEdit from "./components/Orders/OrdersEdit";
import OrdersShow from "./components/Orders/OrdersShow";

import ReviewsIndex from "./components/Reviews/ReviewsIndex";
import ReviewsCreate from "./components/Reviews/ReviewsCreate";
import ReviewsEdit from "./components/Reviews/ReviewsEdit";
import ReviewsShow from "./components/Reviews/ReviewsShow";

import NewsIndex from "./components/News/NewsIndex";
import NewsCreate from "./components/News/NewsCreate";
import NewsEdit from "./components/News/NewsEdit";
import NewsShow from "./components/News/NewsShow";


Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/admin/users',
            name: 'users-index',
            component: UsersIndex
        },
        {
            path: '/admin/users/:id/edit',
            name: 'user-edit',
            component: UsersEdit
        },
        {
            path: '/admin/users/new',
            name: 'user-create',
            component: UsersCreate
        },
        {
            path: '/admin/users/:id/',
            name: 'user-show',
            component: UsersShow
        },
        {
            path: '/admin/subscribers',
            name: 'subscribers-index',
            component: SubscribersIndex
        },
        {
            path: '/admin/users/partners',
            name: 'partners-index',
            component: PartnerIndex
        },
        {
            path: '/admin/users/partners/new',
            name: 'partner-create',
            component: PartnerCreate
        },
        {
            path: '/admin/users/partners/:id/edit',
            name: 'partner-edit',
            component: PartnerEdit
        },
        {
            path: '/admin/users/partner/:id',
            name: 'partner-show',
            component: PartnerShow
        },
        {
            path: '/admin/shop/products',
            name: 'products-index',
            component: ProductsIndex
        },
        {
            path: '/admin/shop/products/new',
            name: 'products-create',
            component: ProductsCreate
        },
        {
            path: '/admin/shop/products/:id/edit',
            name: 'products-edit',
            component: ProductsEdit
        },
        {
            path: '/admin/shop/products/:id',
            name: 'products-show',
            component: ProductsShow
        },
        {
            path: '/admin/shop/orders',
            name: 'orders-index',
            component: OrdersIndex
        },
        {
            path: '/admin/shop/orders/new',
            name: 'orders-create',
            component: OrdersCreate
        },
        {
            path: '/admin/shop/orders/:id/edit',
            name: 'orders-edit',
            component: OrdersEdit
        },
        {
            path: '/admin/shop/orders/:id/show',
            name: 'orders-show',
            component: OrdersShow
        },
        {
            path: '/admin/shop/reviews',
            name: 'reviews-index',
            component: ReviewsIndex,
        },
        {
            path: '/admin/shop/reviews/new',
            name: 'reviews-create',
            component: ReviewsCreate
        },
        {
            path: '/admin/shop/reviews/:id/edit',
            name: 'reviews-edit',
            component: ReviewsEdit
        },
        {
            path: '/admin/shop/reviews/:id/show',
            name: 'reviews-show',
            component: ReviewsShow
        },
        {
            path: '/admin/content/news',
            name: 'news-index',
            component: NewsIndex
        },
        {
            path: '/admin/shop/news/new',
            name: 'news-create',
            component: NewsCreate
        },
        {
            path: '/admin/shop/news/:id/edit',
            name: 'news-edit',
            component: NewsEdit
        },
        {
            path: '/admin/shop/news/:id/show',
            name: 'news-show',
            component: NewsShow
        },
    ]
});

export default router;
