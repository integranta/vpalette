export function asyncUpload(selector = '', url = '', message = 'Successfully Uploaded Files!') {
    const form = document.getElementById(`${selector}`);
    let request = new XMLHttpRequest();

    form.addEventListener('submit', function(e){
        e.preventDefault();
        let formData = new FormData(form);

        request.open('post', `${url}`);
        request.addEventListener("load", transferComplete);
        request.send(formData);
    });

    function transferComplete(data){
        let response = JSON.parse(data.currentTarget.response);
        if(response.success){
            document.getElementById('message').innerHTML = `${message}`;
        }
    }
}
