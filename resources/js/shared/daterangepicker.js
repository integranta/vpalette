import 'bootstrap-timepicker/js/bootstrap-timepicker.min'

/***
 * Wrapper for jQuery Data Range Picker
 * @param selector
 */
export default function setDateRangePicker(selector) {

    /**
     * @description Configure Date Range Picker
     * @see http://www.daterangepicker.com/#config
     */
    $(selector).daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        showSeconds: true,
        timePickerSeconds: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        }
    });

    $(selector).on('cancel.daterangepicker', function (ev, picker) {
        //do something, like clearing an input
        $(selector).val('');
    });
}
