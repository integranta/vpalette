export default function (title, message, type, icon) {
    $.notify({
        title: title + ' : ',
        message: message,
        icon: 'fa ' + icon
    }, {
        type: type,
        allow_dismiss: true,
        placement: {
            from: 'top',
            align: 'right'
        }
    });
}
