<?php

return [
    'alt_brand' => 'ВП',
    'control_panel_section' => [
        'title' => 'Панель управления',
        'control_panel_item' => 'Основные показатели'
    ],
    'users_section' => [
        'title' => 'Пользователи',
        'users_item' => 'Пользователи',
        'subscribers_item' => 'Подписчики',
        'partners_item' => 'Партнёры',
        'partner_shops_item' => 'Партнёрские магазины'
    ],
    'shop_section' => [
        'title' => 'Магазины',
        'products_item' => 'Товары',
        'partner_offers_item' => 'Партнёрские предложения',
        'partner_product_offers_item' => 'Товары от партнёров',
        'brands_item' => 'Бренды',
        'attributes_item' => 'Атрибуты',
        'categories_item' => 'Категории',
        'orders_item' => 'Заказы',
        'reviews_item' => 'Отзывы',
        'discount_item' => 'Скидки',
        'coupons_item' => 'Купоны'
    ],
    'content_section' => [
        'title' => 'Контент',
        'news_item' => 'Новости',
        'pages_item' => 'Страницы',
        'stocks_item' => 'Акции',
        'faq_item' => 'Вопросы и ответы'
    ],
    'ads_section' => [
        'title' => 'Реклама',
        'ads_rates_item' => 'Управление ставками',
        'finances_item' => 'Финансы',
        'banners_item' => 'Баннеры',
        'advantages_item' => 'Преимущества',
        'email_newsletters_item' => 'E-mail рассылки',
    ],
    'seo_section' => [
        'title' => 'Поисковая оптимизация',
        'control_elements_item' => 'Управление элементами'
    ],
    'settings_section' => [
        'title' => 'Настройки',
        'settings_item' => 'Настройки сайта'
    ]
];
