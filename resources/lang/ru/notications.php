<?php

return [
    'all_mark_as_readed' => 'Все уведомления были отмечаны как прочитаны.',
    'notification_mark_as_readed' => 'Уведомление было отмечано как прочитанное',
    'list_not_read_success_retrived' => 'Список не прочитанных уведомлений был успешно извлечен',
    'list_readed_success_retrived' => 'Список прочитанных уведомлений был успешно извлечён',
    'list_all_success_retrived' => 'Список всех уведомлений успешно извлечён',
    'notification_success_delete' => 'Уведомление успешно удалёно'
];
