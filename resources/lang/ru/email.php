<?php

return [
    'title' => 'Сбросить пароль',
    'email_title' => 'Email',
    'email_placeholder' => 'Введите свой Email',
    'send_button' => 'Отправить ссылку',
    'have_account' => 'Уже есть аккаунт?',
    'login_link' => 'Войти'
];
