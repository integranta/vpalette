<?php

use App\Enums\BannerType;
use App\Enums\CouponPeriodActivity;
use App\Enums\CouponTypes;
use App\Enums\DeliveryServices;
use App\Enums\DiscountAmountTypes;
use App\Enums\DiscountPeriodActivity;
use App\Enums\DiscountTypes;
use App\Enums\FieldForIdentityElement;
use App\Enums\FieldForIdentitySection;
use App\Enums\FrontendTypeAttributes;
use App\Enums\ImportStatus;
use App\Enums\NewsletterStatus;
use App\Enums\OrderStatus;
use App\Enums\PartnerProductStatus;
use App\Enums\PaymentMethods;
use App\Enums\PaymentStatus;
use App\Enums\ProductType;

return [
	NewsletterStatus::class        => [
		NewsletterStatus::DELETED => 'Удаленный',
		NewsletterStatus::DRAFT   => 'Черновик',
		NewsletterStatus::SENDED  => 'Отправленный'
	],
	ImportStatus::class            => [
		ImportStatus::DRAFT   => 'Черновик',
		ImportStatus::FAIL    => 'Не успех',
		ImportStatus::SUCCESS => 'Успех'
	],
	DiscountTypes::class           => [
		DiscountTypes::DISCOUNT => 'Скидка',
		DiscountTypes::SALE     => 'Распродажа'
	],
	DiscountAmountTypes::class     => [
		DiscountAmountTypes::PERCENT   => '%',
		DiscountAmountTypes::FIXED     => 'RUB фиксированная сумма',
		DiscountAmountTypes::SET_PRICE => 'RUB установить цену на товар'
	],
	CouponTypes::class             => [
		CouponTypes::SINGLE_ITEM_ORDER => 'На одну позицию заказ',
		CouponTypes::SINGLE_ORDER      => 'На один заказ',
		CouponTypes::REUSABLE          => 'Многоразовый'
	],
	CouponPeriodActivity::class    => [
		CouponPeriodActivity::INTERVAL => 'Интервал',
		CouponPeriodActivity::NO_LIMIT => 'Без ограничений'
	],
	DiscountPeriodActivity::class  => [
		DiscountPeriodActivity::INTERVAL => 'Интервал',
		DiscountPeriodActivity::NO_LIMIT => 'Без ограничений'
	],
	BannerType::class              => [
		BannerType::SLIDER => 'Слайдер',
		BannerType::BIG    => 'Большой',
		BannerType::SMALL  => 'Маленький'
	],
	OrderStatus::class             => [
		OrderStatus::PENDING    => 'Ожидание',
		OrderStatus::PROCESSING => 'Выполняется',
		OrderStatus::DELIVERED  => 'Доставлен',
		OrderStatus::COMPLETED  => 'Выполнен',
		OrderStatus::DECLINE    => 'Отменён'
	],
	PaymentStatus::class           => [
		PaymentStatus::NOT_PAID => 'Не оплачен',
		PaymentStatus::PAID     => 'Оплачен'
	],
	ProductType::class             => [
		ProductType::PORTAL_PRODUCT  => 'Товар от портала',
		ProductType::PARTNER_PRODUCT => 'Товар от партнёра'
	],
	PartnerProductStatus::class    => [
		PartnerProductStatus::QUEUE              => 'В очереди',
		PartnerProductStatus::ADDED              => 'Добавлен',
		PartnerProductStatus::EXIST              => 'Данный товар уже есть на сайте',
		PartnerProductStatus::DONT_MATCH_SUBJECT => 'Не соответствует тематике сайта',
		PartnerProductStatus::FALSE_PRODUCT      => 'Ложный товар'
	],
	FrontendTypeAttributes::class  => [
		FrontendTypeAttributes::SELECT   => 'Список',
		FrontendTypeAttributes::CHECKBOX => 'Чекбокс',
		FrontendTypeAttributes::RADIO    => 'Переключатель',
		FrontendTypeAttributes::TEXT     => 'Строка',
		FrontendTypeAttributes::TEXTAREA => 'Текст',
		FrontendTypeAttributes::RANGE    => 'Ползунок',
		FrontendTypeAttributes::COLOR    => 'Цвет',
		FrontendTypeAttributes::FILE     => 'Файл',
		FrontendTypeAttributes::NUMBER   => 'Число',
		FrontendTypeAttributes::EMAIL    => 'E-mail',
		FrontendTypeAttributes::DATE     => 'Дата'
	],
	FieldForIdentityElement::class => [
		FieldForIdentityElement::NAME       => 'Название элемента',
		FieldForIdentityElement::ID         => 'ID элемента',
		FieldForIdentityElement::SLUG       => 'Символьный код',
		FieldForIdentityElement::CREATED_AT => 'Дата создания',
		FieldForIdentityElement::DELETED_AT => 'Дата окончания (удаления)'
	],
	FieldForIdentitySection::class => [
		FieldForIdentitySection::ID   => 'ID раздел',
		FieldForIdentitySection::NAME => 'Название раздела',
		FieldForIdentitySection::SLUG => 'Символьный код'
	],
	DeliveryServices::class        => [
		DeliveryServices::SELF_PICKUP => 'Самовызов',
		DeliveryServices::CDEK        => 'СДЭК',
		DeliveryServices::BOXBERRY    => 'Boxberry'
	],
	PaymentMethods::class          => [
		PaymentMethods::CASH             => 'Наличными или картой в магазине',
		PaymentMethods::CARD             => 'Оплата онлайн',
		PaymentMethods::CASH_ON_DELIVERY => 'Наложенный платёж оплата при получении'
	]
];
