<?php

return [
    'title' => 'Авторизация',
    'email_title' => 'Email',
    'email_placeholder' => 'Введите свой Email',
    'password_title' => 'Пароль',
    'password_placeholder' => 'Введите свой пароль',
    'forget_password' => 'Забыли пароль?',
    'remember_me' => 'Запомнить меня',
    'login_button' => 'Войти',
    'dont_have_account' => 'Ещё не зарегистрированы?',
    'register_now' => 'Зарегистрироваться'
];
