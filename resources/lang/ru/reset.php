<?php

return [
    'title' => 'Установить новый пароль',
    'email_title' => 'Email',
    'email_placeholder' => 'Введите свой Email',
    'password_title' => 'Пароль',
    'password_placeholder' => 'Введите новый пароль',
    'confirm_password_title' => 'Подтвердите пароль',
    'confirm_password_placeholder' => 'Введите ещё раз пароль',
    'reset_button' => 'Сменить пароль',
    'recall_your_login' => 'Вспомнили данные?',
    'sing_in_link' => 'Войти'
];
