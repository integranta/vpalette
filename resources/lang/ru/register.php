<?php

return [
    'title' => 'Регистрация',
    'first_name_title' => 'Имя',
    'first_name_placeholder' => 'Введите своё имя',
    'last_name_title' => 'Фамилия',
    'last_name_placeholder' => 'Введите свою фамилию',
    'email_title' => 'Email',
    'email_placeholder' => 'Введите свой Email',
    'password_title' => 'Пароль',
    'password_placeholder' => 'Введите пароль',
    'password_confirm_title' => 'Подтвердите пароль',
    'password_confirm_placeholder' => 'Введите ещё раз пароль',
    'register_button' => 'Зарегистрироваться',
    'have_account' => 'Уже есть аккаунт?',
    'login_link' => 'Войти'
];
