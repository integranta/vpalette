<?php

return [
    'title' => 'Registration',
    'first_name_title' => 'Last name',
    'first_name_placeholder' => 'Input your first name',
    'last_name_title' => 'Last name',
    'last_name_placeholder' => 'Input your last name',
    'email_title' => 'Email',
    'email_placeholder' => 'Input your Email',
    'password_title' => 'Password',
    'password_placeholder' => 'Input password',
    'password_confirm_title' => 'Confirm password',
    'password_confirm_placeholder' => 'Re-enter password',
    'register_button' => 'Sign up',
    'have_account' => 'Already have an account?',
    'login_link' => 'Sign in'
];
