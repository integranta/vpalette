<?php

return [
    'title' => 'Set a New Password',
    'email_title' => 'Email',
    'email_placeholder' => 'Input your Email',
    'password_title' => 'Password',
    'password_placeholder' => 'Input new a password',
    'confirm_password_title' => 'Confirm password',
    'confirm_password_placeholder' => 'Re-enter new a password',
    'reset_button' => 'Set a new password',
    'recall_your_login' => 'Recalled your login info?',
    'sing_in_link' => 'Sign In'
];
