<?php

return [
    'title' => 'Authorization',
    'email_title' => 'Email',
    'email_placeholder' => 'Input your Email',
    'password_title' => 'Password',
    'password_placeholder' => 'Input password',
    'forget_password' => 'Forget password?',
    'remember_me' => 'Remember me',
    'login_button' => 'Sign in',
    'dont_have_account' => 'Dont have account?',
    'register_now' => 'Sign up'
];
