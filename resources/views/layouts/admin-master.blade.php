<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@yield('title', 'ВПалитре') &mdash; {{ env('APP_NAME') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ mix('css/app.min.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css')}}">
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ mix('/css/libs.min.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('libs/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ mix('libs/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ mix('libs/css/select2/dist/css/select2.min.css') }}">--}}
    @yield('plugin_css')

    <script>
        window.Laravel = @json(['csrfToken' => csrf_token()], JSON_PRETTY_PRINT);
        @auth
            window.Permissions = {!! json_encode(Auth::user()->allPermissions) !!};
        @else
            window.Permissions = [];
        @endauth
    </script>
</head>

<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar">
            @include('admin.partials.topnav')
        </nav>
        <div class="main-sidebar">
            @include('admin.partials.sidebar')
        </div>

        <!-- Main Content -->
        <div class="main-content">
            @yield('content')
        </div>
        <footer class="main-footer">
            @include('admin.partials.footer')
        </footer>
    </div>
</div>

<script src="{{ route('js.dynamic') }}"></script>
<script src="{{ mix('js/app.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>--}}
<script src="{{ asset('assets/js/stisla.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="{{ asset('libs/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('libs/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('libs/js/select.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
@stack('scripts')
</body>
</html>
