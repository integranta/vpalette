@extends('layouts.admin-master')

@section('title')
    Обновление профиля ({{ $user->name }})
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.users') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Обновление профиля ({{ $user->name }})</h1>
        </div>
        <div class="section-body">
            <profile-component user='{!! $user->toJson() !!}'></profile-component>
        </div>
    </section>
@endsection
