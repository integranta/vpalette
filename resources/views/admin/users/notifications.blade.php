@extends('layouts.admin-master')

@section('title')
    Список уведомлений
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.users') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Список уведомлений</h1>
        </div>
        <div class="section-body">
            <notifications-list :user-id="{{ $userId }}" />
        </div>
    </section>
@endsection
