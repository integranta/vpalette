@extends('layouts.admin-master')

@section('title')
    Добавить пользователя
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('admin.users') }}" class="btn btn-icon">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>
            <h1>Добавить пользователя</h1>
        </div>
        <div class="section-body">
            <adduser-component></adduser-component>
        </div>
    </section>
@endsection
