@extends('layouts.admin-master')

@section('title')
    Управление пользователями
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Управление пользователями</h1>
            <div class="section-header-button">
                @can('create-users')
                    <a href="{{ route('admin.users.create') }}" class="btn btn-primary">
                        Добавить
                    </a>
                @endcan
            </div>
        </div>
        <div class="section-body">
            <users-component></users-component>
        </div>
    </section>
@endsection
