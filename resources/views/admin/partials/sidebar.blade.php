<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="{{ route('admin.dashboard.index') }}">
            {{ env('APP_NAME') }}
        </a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="/">
            {{ __('sidebar.alt_brand') }}
        </a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">{{ __('sidebar.control_panel_section.title') }}</li>
        <li class="{{ Request::route()->getName() === 'admin.dashboard.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.dashboard.index') }}"
            >
                <i class="fa fa-tachometer-alt"></i>
                <span>{{ __('sidebar.control_panel_section.control_panel_item') }}</span>
            </a>
        </li>
        @can('manage-users')
        <li class="menu-header">
            {{ __('sidebar.users_section.title') }}
        </li>
        <li class="{{ Request::route()->getName() === 'admin.users.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.users.index') }}"
            >
                <i class="fa fa-users"></i>
                <span>
                    {{ __('sidebar.users_section.users_item') }}
                </span>
            </a>
        </li>
        @endcan
        @can('create subscribers')
            <li class="{{ Request::route()->getName() === 'admin.subscriber.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.subscriber.index') }}"
                >
                    <i class="fa fa-user-plus"></i>
                    <span>{{ __('sidebar.users_section.subscribers_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create partners')
            <li class="{{ Request::route()->getName() === 'admin.partners.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.partners.index') }}"
                >
                    <i class="fa fa-handshake"></i>
                    <span>{{ __('sidebar.users_section.partners_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create partner shops')
            <li class="{{ Request::route()->getName() === 'admin.partner-shop.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.partner-shop.index') }}"
                >
                    <i class="fa fa-store"></i>
                    <span>{{ __('sidebar.users_section.partner_shops_item') }}</span>
                </a>
            </li>
        @endcan
        <li class="menu-header">{{ __('sidebar.shop_section.title')  }}</li>
        @can('create products')
            <li class="{{ Request::route()->getName() === 'admin.products.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.products.index') }}"
                >
                    <i class="fa fa-people-carry"></i>
                    <span>
                    {{ __('sidebar.shop_section.products_item')  }}
                </span>
                </a>
            </li>
        @endcan
        @can('create partner offers')
            <li class="{{ Request::route()->getName() === 'admin.partner-offers.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.partner-offers.index') }}"
                >
                    <i class="fa fa-people-carry"></i>
                    <span>{{ __('sidebar.shop_section.partner_offers_item') }}</span>
                </a>
            </li>
        @endcan
        <li class="{{ Request::route()->getName() === 'admin.partner.products.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.partner.products.index', ['partner' => 1]) }}"
            >
                <i class="fa fa-people-carry"></i>
                <span>{{ __('sidebar.shop_section.partner_product_offers_item') }}</span>
            </a>
        </li>
        @can('create brands')
            <li class="{{ Request::route()->getName() === 'admin.brands.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.brands.index') }}"
                >
                    <i class="fa fa-trademark"></i>
                    <span>{{ __('sidebar.shop_section.brands_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create attributes')
            <li class="{{ Request::route()->getName() === 'admin.attributes.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.attributes.index') }}"
                >
                    <i class="fa fa-list-ul"></i>
                    <span>{{ __('sidebar.shop_section.attributes_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create categories')
            <li class="{{ Request::route()->getName() === 'admin.categories.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.categories.index') }}"
                >
                    <i class="fa fa-sitemap"></i>
                    <span>{{ __('sidebar.shop_section.categories_item') }}</span>
                </a>
            </li>
        @endcan
        <li class="{{ Request::route()->getName() === 'admin.orders.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.orders.index') }}"
            >
                <i class="fa fa-shopping-basket"></i>
                <span>{{ __('sidebar.shop_section.orders_item') }}</span>
            </a>
        </li>
        @can('create reviews')
            <li class="{{ Request::route()->getName() === 'admin.reviews.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.reviews.index') }}"
                >
                    <i class="fa fa-star-half-alt"></i>
                    <span>{{ __('sidebar.shop_section.reviews_item') }}</span>
                </a>
            </li>
        @endcan
        <li class="{{ Request::route()->getName() === 'admin.coupon.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.coupon.index') }}"
            >
                <i class="fas fa-ticket-alt"></i>
                <span>
                   {{ __('sidebar.shop_section.coupons_item') }}
                </span>
            </a>
        </li>
        <li class="{{ Request::route()->getName() === 'admin.discount.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.discount.index') }}"
            >
                <i class="fa fa-percent"></i>
                <span>
                   {{ __('sidebar.shop_section.discount_item') }}
                </span>
            </a>
        </li>

        <li class="menu-header">{{ __('sidebar.content_section.title') }}</li>
        @can('create news')
            <li class="{{ Request::route()->getName() === 'admin.posts.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.posts.index') }}"
                >
                    <i class="fa fa-newspaper"></i>
                    <span>{{ __('sidebar.content_section.news_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create pages')
            <li class="{{ Request::route()->getName() === 'admin.pages.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.pages.index') }}"
                >
                    <i class="fa fa-file"></i>
                    <span>{{ __('sidebar.content_section.pages_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create stocks')
            <li class="{{ Request::route()->getName() === 'admin.stocks.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.stocks.index') }}"
                >
                    <i class="fa fa-percent"></i>
                    <span>{{ __('sidebar.content_section.stocks_item') }}</span>
                </a>
            </li>
        @endcan
        {{--<li class="{{ Request::route()->getName() === 'admin.media' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.media') }}"
            >
                <i class="fa fa-image"></i>
                <span>Медиабиблиотека</span>
            </a>
        </li>--}}
        @can('create faqs')
            <li class="{{ Request::route()->getName() === 'admin.faqs.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.faqs.index') }}"
                >
                    <i class="fa fa-question-circle"></i>
                    <span>{{ __('sidebar.content_section.faq_item') }}</span>
                </a>
            </li>
        @endcan
        <li class="menu-header">{{ __('sidebar.ads_section.title') }}</li>
        <li class="{{ Request::route()->getName() === 'admin.ads.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.ads.index') }}"
            >
                <i class="fa fa-bullhorn"></i>
                <span>{{ __('sidebar.ads_section.ads_rates_item') }}</span>
            </a>
        </li>
        <li class="{{ Request::route()->getName() === 'admin.finance.index' ? ' active' : '' }}">
            <a
                class="nav-link"
                href="{{ route('admin.finance.index') }}"
            >
                <i class="fa fa-chart-line"></i>
                <span>{{ __('sidebar.ads_section.finances_item') }}</span>
            </a>
        </li>
        @can('create banners')
            <li class="{{ Request::route()->getName() === 'admin.banners.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.banners.index') }}"
                >
                    <i class="fa fa-image"></i>
                    <span>{{ __('sidebar.ads_section.banners_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create advantages')
            <li class="{{ Request::route()->getName() === 'admin.advantages.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.advantages.index') }}"
                >
                    <i class="fa fa-star"></i>
                    <span>{{ __('sidebar.ads_section.advantages_item') }}</span>
                </a>
            </li>
        @endcan
        @can('create newsletters')
            <li class="{{ Request::route()->getName() === 'admin.newsletters.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.newsletters.index') }}"
                >
                    <i class="fa fa-envelope-open-text"></i>
                    <span>{{ __('sidebar.ads_section.email_newsletters_item') }}</span>
                </a>
            </li>
        @endcan
        <li class="menu-header">{{ __('sidebar.seo_section.title') }}</li>
        @can('create seo')
            <li class="{{ Request::route()->getName() === 'admin.seo.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.seo.index') }}"
                >
                    <i class="fa fa-chart-bar"></i>
                    <span>{{ __('sidebar.seo_section.control_elements_item') }}</span>
                </a>
            </li>
        @endcan
        <li class="menu-header">{{ __('sidebar.settings_section.title') }}</li>
        @can('create settings')
            <li class="{{ Request::route()->getName() === 'admin.settings.index' ? ' active' : '' }}">
                <a
                    class="nav-link"
                    href="{{ route('admin.settings.index') }}"
                >
                    <i class="fa fa-cogs"></i>
                    <span>{{ __('sidebar.settings_section.settings_item') }}</span>
                </a>
            </li>
        @endcan
    </ul>
</aside>
