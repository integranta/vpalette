@php
    $errors = Session::get('error');
    $messages = Session::get('success');
    $info = Session::get('info');
    $warnings = Session::get('warning');
@endphp
@if ($errors) @foreach($errors as $key => $value)
    <div class="alert alert-danger alert-dismissible col-12" role="alert">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <strong>Ошибка!</strong> {{ $value }}
    </div>
@endforeach @endif

@if ($messages) @foreach($messages as $key => $value)
    <div class="alert alert-success alert-dismissible col-12" role="alert">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <strong>Успешно!</strong> {{ $value }}
    </div>
@endforeach @endif

@if ($info) @foreach($info as $key => $value)
    <div class="alert alert-info alert-dismissible col-12" role="alert">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <strong>Информация!</strong> {{ $value }}
    </div>
@endforeach @endif

@if ($warnings) @foreach($warnings as $key => $value)
    <div class="alert alert-warning alert-dismissible col-12" role="alert">
        <button class="close" type="button" data-dismiss="alert">×</button>
        <strong>Предупреждение!</strong> {{ $value }}
    </div>
@endforeach @endif
