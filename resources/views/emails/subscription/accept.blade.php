<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Потверждение подписки на Email-рассылку</title>
</head>
<body>
    <h1>Привет !</h1>
    <p>
        Вы, указали свой Email {{ $email }} для подписки на Email-рассылку в Интернет-портале ВПалитре.
        Для потверждения этого действия, нажмите на кнопку ниже.
        <a href="{{ route('admin.accept.subscription', ['email' => $email, 'active' => 1]) }}">Потвердить подписку</a>
    </p>
</body>
</html>
