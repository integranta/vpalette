@extends('layouts.auth-master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>{{ __('email.title') }}</h4>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="form-group">
                    <label for="email">{{ __('email.email_title') }}</label>
                    <input
                        id="email"
                        type="email"
                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        name="email"
                        tabindex="1"
                        value="{{ old('email') }}"
                        placeholder="{{ __('email.email_placeholder') }}"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="2">
                        {{ __('email.send_button') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="mt-5 text-muted text-center">
        {{ __('email.have_account') }} <a href="{{ route('login') }}">{{ __('email.login_link') }}</a>
    </div>
@endsection
