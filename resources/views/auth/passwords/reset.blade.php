@extends('layouts.auth-master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>{{ __('reset.title') }}</h4>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <label for="email">{{ __('reset.email_title') }}</label>
                    <input
                        id="email"
                        type="email"
                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        name="email"
                        tabindex="1"
                        value="{{ old('email') }}"
                        placeholder="{{ __('register.email_placeholder') }}"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label">{{ __('reset.password_title') }}</label>
                    <input
                        id="password"
                        type="password"
                        class="form-control{{ $errors->has('password') ? ' is-invalid': '' }}"
                        name="password"
                        tabindex="2"
                        placeholder="{{ __('reset.password_placeholder') }}"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirmation" class="control-label">
                        {{ __('reset.confirm_password_title') }}
                    </label>
                    <input
                        id="password_confirmation"
                        type="password"
                        class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid': '' }}"
                        name="password_confirmation"
                        tabindex="2"
                        placeholder="{{ __('reset.confirm_password_placeholder') }}"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('password_confirmation') }}
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                        {{ __('reset.reset_button') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="mt-5 text-muted text-center">
        {{ __('reset.recall_your_login') }} <a href="{{ route('login') }}">{{ __('reset.sing_in_link') }}</a>
    </div>
@endsection
