@extends('layouts.auth-master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>{{ __('login.title') }}</h4>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="email">{{ __('login.email_title') }}</label>
                    <input
                        id="email"
                        type="email"
                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        name="email"
                        placeholder="{{ __('login.email_placeholder') }}"
                        tabindex="1"
                        value="{{ old('email') }}"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="d-block">
                        <label for="password" class="control-label">{{ __('login.password_title') }}</label>
                        <div class="float-right">
                            <a href="{{ route('password.request') }}" class="text-small">
                                {{ __('login.forget_password') }}
                            </a>
                        </div>
                    </div>
                    <input
                        id="password"
                        type="password"
                        placeholder="{{ __('login.password_placeholder') }}"
                        class="form-control{{ $errors->has('password') ? ' is-invalid': '' }}"
                        name="password"
                        tabindex="2"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            name="remember"
                            class="custom-control-input"
                            tabindex="3"
                            id="remember"{{ old('remember') ? ' checked': '' }}
                        >
                        <label class="custom-control-label" for="remember">{{ __('login.remember_me') }}</label>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                        {{ __('login.login_button') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="mt-5 text-muted text-center">
        {{ __('login.dont_have_account') }}
        <a href="{{ route('register') }}">{{ __('login.register_now') }}</a>
    </div>
@endsection
