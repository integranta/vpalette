@extends('layouts.auth-master')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h4>{{ __('register.title') }}</h4>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group">
                    <label for="first_name">{{ __('register.first_name_title') }}</label>
                    <input
                        id="first_name"
                        type="text"
                        class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                        name="first_name"
                        tabindex="1"
                        placeholder="{{ __('register.first_name_placeholder') }}"
                        value="{{ old('first_name') }}"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('first_name') }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="last_name">{{ __('register.last_name_title') }}</label>
                    <input
                        id="last_name"
                        type="text"
                        class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                        name="last_name"
                        tabindex="2"
                        placeholder="{{ __('register.last_name_placeholder') }}"
                        value="{{ old('last_name') }}"
                        autofocus>
                    <div class="invalid-feedback">
                        {{ $errors->first('last_name') }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="email">{{ __('register.email_title') }}</label>
                    <input
                        id="email"
                        type="email"
                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        placeholder="{{ __('register.email_placeholder') }}"
                        name="email"
                        tabindex="3"
                        value="{{ old('email') }}"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="control-label">{{ __('register.password_title') }}</label>
                    <input
                        id="password"
                        type="password"
                        class="form-control{{ $errors->has('password') ? ' is-invalid': '' }}"
                        placeholder="{{ __('register.password_placeholder') }}"
                        name="password"
                        tabindex="4"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                </div>

                <div class="form-group">
                    <label for="password_confirmation" class="control-label">
                        {{ __('register.password_confirm_title') }}
                    </label>
                    <input
                        id="password_confirmation"
                        type="password"
                        placeholder="{{ __('register.password_confirm_placeholder') }}"
                        class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid': '' }}"
                        name="password_confirmation"
                        tabindex="5"
                        autofocus
                    >
                    <div class="invalid-feedback">
                        {{ $errors->first('password_confirmation') }}
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="6">
                        {{ __('register.register_button') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="mt-5 text-muted text-center">
        {{ __('register.have_account') }} <a href="{{ route('login') }}">{{ __('register.login_link') }}</a>
    </div>
@endsection
