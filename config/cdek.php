<?php

return [
	/** Включён ли тестовый режим */
	'test_mode_enable' => env('CDEK_TEST_MODE_ENABLE', true),
	
	/** Тип аутентификации */
	'grant_type'       => 'client_credentials',
	/*
	|--------------------------------------------------------------------------
	| Настройки для тестового режима СДЭК
	|--------------------------------------------------------------------------
	|
	| account - идентификатор клиента, равен Account;
	| password - секретный ключ клиента, равен Secure password.
	| base_uri - Базовый URL для отправки запросов в CДЭК
	|
	*/
	'test_mode'        => [
		'account'  => env('CDEK_TEST_ACCOUNT', 'EMscd6r9JnFiQ3bLoyjJY6eM78JrJceI'),
		'password' => env('CDEK_TEST_PASSWORD', 'PjLZkKBHEiLK3YsjtNrt3TGNG0ahs3kG'),
		'base_uri' => env('CDEK_TEST_URL', 'https://integration.edu.cdek.ru'),
		'timeout'  => env('CDEK_TEST_TIMEOUT', 60)
	],
	/*
	|--------------------------------------------------------------------------
	| Настройки для боевого режима СДЭК
	|--------------------------------------------------------------------------
	|
	| account - идентификатор клиента, равен Account;
	| password - секретный ключ клиента, равен Secure password.
	| base_uri - Базовый URL для отправки запросов в CДЭК
	|
	*/
	'prod_mode'        => [
		'account'  => env('CDEK_PROD_ACCOUNT', ''),
		'password' => env('CDEK_PROD_PASSWORD', ''),
		'base_uri' => env('CDEK_PROD_URL', 'https://integration.cdek.ru'),
		'timeout'  => env('CDEK_PROD_TIMEOUT', 60)
	]
];