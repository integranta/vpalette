<?php

return [
	'BOXBERRY_SDK_API_TOKEN' => env('BOXBERRY_SDK_API_TOKEN', null),
	'BOXBERRY_BASE_URI'      => env('BOXBERRY_BASE_URI', 'https://api.boxberry.ru/json.php'),
	'BOXBERRY_TIMEOUT'       => env('BOXBERRY_TIMEOUT', 300)
];